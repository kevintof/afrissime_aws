/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */
 /*fixed menu*/
 jQuery(document).ready(function($) {
   
   /*
   $("#payment-confirmation > .ps-shown-by-js > button").click(function(e) {
        var myPaymentMethodSelected = $(".payment-options").find("input[data-module-name='payexpresse']").is(':checked');
        
        if (myPaymentMethodSelected){
        }
        
    });
    */
    /*js pour le menu accordeon*/
    $('#accordion').find('.accordion-toggle').click(function(){

          //Expand or collapse this panel
          $(this).next().slideToggle();
            //Hide the other panels
          $(".accordion-content").not($(this).next()).slideUp();
          $(this).toggleClass('minus-cercle').siblings().removeClass("minus-cercle");;
        return false;
    });
    /*fin code js*/
		$('.jms-megamenu').jmsMegaMenu({    			
			event: jmmm_event,
			duration: jmmm_duration
		});	

	$('#mobile-vermegamenu').click(function(e) {
        e.stopPropagation();
    });
    $('.shoppingcart-box').click(function(e) {
        e.stopPropagation();
    });
    $('.search-wrapper').click(function(e) {
        e.stopPropagation();
    });
    $('#close_search').click(function(e) {
       $('#jms_ajax_search').removeClass('open');
    });

    $('.action-box > .product-btn').click(function(e) {
       var callerElement = $(this);
            callerElement.addClass('loading');
            setTimeout(function() {
            callerElement.removeClass('loading');
        }, 3000);
    });
});	

function validateForm() {
    var x = document.forms["emailForm"]["email"].value;
    if (x == "") {
        alert("Email must be filled out");
        return false;
    }
}

if($(".jms-vermegamenu").length) {
    jQuery('.jms-vermegamenu').jmsVerMegaMenu({       
        event: jmvmm_event,
        duration: jmvmm_duration
    });  
}

$('body').on('click', '.ajax-add-to-cart', function (event) {	
	event.preventDefault();
	var query = 'id_product=' + $(this).attr('data-id-product') + '&qty='+ $(this).attr('data-minimal-quantity') + '&token=' + $(this).attr('data-token') + '&add=1&action=update';
	var actionURL = prestashop['urls']['base_url'] +  'index.php?controller=cart';		
	$('.ajax-add-to-cart').removeClass('addtocart-selected');
	$(this).addClass('addtocart-selected');
	$(this).removeClass('checked');
	$(this).addClass('checking');
	var callerElement = $(this);
	$.post(actionURL, query, null, 'json').then(function (resp) {				
	    prestashop.emit('updateCart', {
	        reason: {
	          idProduct: resp.id_product,
	          idProductAttribute: resp.id_product_attribute,
	          linkAction: 'add-to-cart'
	        }
	    });
		
		$(callerElement).removeClass('checking');
		$(callerElement).addClass('checked');
		window.setTimeout( function() {$(callerElement).removeClass('checked');}, 3000 );
	}).fail(function (resp) {
	    prestashop.emit('handleError', { eventType: 'addProductToCart', resp: resp });
	});
});

$('body').on('click', '[data-button-action="add-to-cart"]', function (event) {
    $(this).removeClass('addtocart-selected');
    $(this).addClass('addtocart-selected');
    $(this).removeClass('checked');
    $(this).addClass('checking');
});

function view_as() { 
    var viewGrid = $(".view-grid"),
        viewList = $(".view-list"),
        productList = $(".product_list");
		viewGrid.click(function (e) {       
        productList.removeClass("products-list-in-row");
        productList.addClass("products-list-in-column");
        $(this).addClass('active');
        viewList.removeClass("active");
        e.preventDefault()
    });
    viewList.click(function (e) {       
        productList.removeClass("products-list-in-column");
        productList.addClass("products-list-in-row");
        viewGrid.removeClass("active");
        $(this).addClass('active');        
        e.preventDefault()
    })
}
jQuery(function ($) {
    "use strict";
    view_as();
    
});

jQuery(function ($) {
	"use strict";
	if($('.slider').length > 0) {
        $('.slider').fractionSlider({   
            'slideTransition' : jmsslider_trans,
            'slideEndAnimation' : jmsslider_end_animate,
            'transitionIn' : jmsslider_trans_in,
            'transitionOut' : jmsslider_trans_out,
            'fullWidth' : jmsslider_full_width,
            'delay' : jmsslider_delay,
            'timeout' : jmsslider_duration,
            'speedIn' : jmsslider_speed_in,
            'speedOut' : jmsslider_speed_out,
            'easeIn' : jmsslider_ease_in,
            'easeOut' : jmsslider_ease_out,
            'controls' : jmsslider_navigation,
            'pager' : jmsslider_pagination,
            'autoChange' : jmsslider_autoplay,
            'pauseOnHover' : jmsslider_pausehover,
            'backgroundAnimation' : jmsslider_bg_animate,
            'backgroundEase' : jmsslider_bg_ease,
            'responsive' : jmsslider_responsive,
            'dimensions' : jmsslider_dimensions,
            'fullscreen' : true
        });
	}
});
function back_to_top() {   
    $('#back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 500);
        return false;
    })
}

jQuery(function ($) {
    "use strict";
    $(window).scroll(function () {
     if ($(window).scrollTop() >= 30) {
      $("#back-to-top").stop().fadeIn(300);
     } else if ($(window).scrollTop() < $('header').outerHeight()) {
      $("#back-to-top").stop().fadeOut(300);
     }
    });
    $(window).scroll(function(){
    var aTop = 400;
    if($(".jms-popup-overlay").length > 0){
          if($(this).scrollTop()>=aTop){
              $('.jms-popup-overlay').removeClass('hidden'); 
          }
        }
    });
});

$(window).load(function () {     
    back_to_top(); 
});

var initialLoad = true;

$(document).ready(function() {	
	if(initialLoad){
		setTimeout(function() {
			jQuery('.preloader').fadeOut();
		}, 1000);		
		initialLoad = false;
	}
});

jQuery(function ($) {
    "use strict";
	var bodyEl = $("body"),
		content = $('.main-site'),
		openbtn = $('.top_menu p'),
		closebtn = $('.main-site' ),
		isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		openbtn.click(function(e) {		
			toggleMenu();
			e.stopPropagation();
		});
		if( closebtn ) {
			closebtn.click(function() {		
				toggleMenu();
			});
		}
		content.click(function(e) {		
			var target = e.target;
			if( isOpen && target !== openbtn ) {
				toggleMenu();
			}
		});		
	}
	function toggleMenu() {		
		if( isOpen ) {
			bodyEl.removeClass('open');
		}
		else {
			bodyEl.addClass('open');
		}
		isOpen = !isOpen;
	}
	init();

});
jQuery(function ($) {
    "use strict";
    if(typeof p_unique !== 'undefined') {
        for (var i = 0 ; i < p_unique.length ; i++) {
            var key = p_unique[i];
            if($(".product-carousel-" + key).length) {       
                var productCarousel = $(".product-carousel-" + key);          
                var rtl = false;
                if ($("body").hasClass("rtl")) rtl = true;              
                productCarousel.owlCarousel({
                    responsiveClass:true,
                    responsive:{            
                        1600:{
							items:p_itemsDesktop[key]
						},
						992:{
							items:p_itemsDesktopSmall[key]
						},
						768:{
							items:p_itemsTablet[key]
						},
						481:{
							items:p_itemsMobile[key]
						},
						0:{
							items:1
						}
                    },
                    rtl: rtl,
					margin: 30,
				    nav: p_nav[key],
                    dots: p_pag[key],
                    autoplay: p_auto[key],
                    slideBy: p_slideby[key],
                    rewindNav: p_rewind[key],
                    navigationText: ["", ""],
                    slideSpeed: 200	
                });
            }
        }
	}
	if(typeof blog_unique !== 'undefined') {
        for (var i = 0 ; i < blog_unique.length ; i++) {
            var key = blog_unique[i];
            if($(".blog-carousel-" + key).length) {       
                var blogsCarousel = $(".blog-carousel-" + key);          
                var rtl = false;
                if ($("body").hasClass("rtl")) rtl = true;              
                blogsCarousel.owlCarousel({
                    responsiveClass:true,
                    responsive:{            
                        1199:{
                            items:blog_itemsDesktop[key]
                        },
                        992:{
                            items:blog_itemsDesktopSmall[key]
                        },
                        768:{
                            items:blog_itemsTablet[key]
                        },
                        481:{
                            items:blog_itemsMobile[key]
                        },
                        0:{
                            items:1
                        },
                    },
                    rtl: rtl,
                    margin:blog_margin,
                    nav: blog_nav[key],
                    dots: blog_pag[key],
                    autoplay: blog_auto[key],
                    slideBy: blog_slideby[key],
                    rewindNav: blog_rewind[key],
                    navigationText: ["", ""],
                    slideSpeed: 800
                });
            }
        }
    }
    if($(".testimonial-carousel").length) {
        var testimonialCarousel = $(".testimonial-carousel");
        var rtl = false;
        if ($("body").hasClass("rtl")) rtl = true;
        testimonialCarousel.owlCarousel({
            responsiveClass:true,
            responsive:{            
                1199:{
                    items:testi_itemsDesktop
                },
                991:{
                    items:testi_itemsDesktopSmall
                },
                768:{
                    items:testi_itemsTablet
                },
                320:{
                    items:testi_itemsMobile
                },
                0:{
                    items:1
                }
            },
            rtl: rtl,
            margin: 20,
            nav: p_nav_testi,
            dots: p_pag_testi,
            autoplay: auto_play_testi,
            slideSpeed: 200,
            loop:false
        });
    }

    if($(".instagram-images").length) {
        var instagramCarousel = $(".instagram-images");
        var rtl = false;
        if ($("body").hasClass("rtl")) rtl = true;
        instagramCarousel.owlCarousel({
            responsiveClass:true,
            responsive:{            
                1199:{
                    items:inst_itemsDesktop
                },
                991:{
                    items:inst_itemsDesktopSmall
                },
                768:{
                    items:inst_itemsTablet
                },
                361:{
                    items:inst_itemsMobile
                },
                0:{
                    items:2
                }
            },
            rtl: rtl,
            margin: inst_space,
            nav: inst_nav,
            dots: inst_pag,
            autoplay: inst_autoplay,
            slideSpeed: 200,
            loop:false
        });
    }
    if($(".brand-carousel").length) {
        var brandCarousel = $(".brand-carousel");
        var rtl = false;
        if ($("body").hasClass("rtl")) rtl = true;
        brandCarousel.owlCarousel({
            responsiveClass:true,
            responsive:{            
                1600:{
                    items:brand_itemsDesktop
                },
                1200:{
                    items:brand_itemsDesktopSmall
                },
                768:{
                    items:brand_itemsTablet
                },
                481:{
                    items:brand_itemsMobile
                },
                361:{
                    items:3
                },
                0:{
                    items:2
                }
            },
            rtl: rtl,
            margin: 20,
            nav: p_nav_brand,
            dots: p_pag_brand,
            autoplay: auto_play_brand,
            slideSpeed: 200,
            loop:true
        });
    }
    if(typeof c1_unique !== 'undefined') {
            for (var i = 0 ; i < c1_unique.length ; i++) {
                var key = c1_unique[i];
                if($(".categories-carousel-" + key).length) {       
                    var categoriesCarousel = $(".categories-carousel-" + key);          
                    var rtl = false;
                    if ($("body").hasClass("rtl")) rtl = true;              
                    categoriesCarousel.owlCarousel({
                        responsiveClass:true,
                        responsive:{            
                            1367:{
                                items:c1_itemsDesktop[key]
                            },
                            1200:{
                                items:c1_itemsDesktopSmall[key]
                            },
                            768:{
                                items:c1_itemsTablet[key]
                            },
                            481:{
                                items:c1_itemsMobile[key]
                            },
                            0:{
                                items:1
                            }
                        },
                        rtl: rtl,
                        margin:c_space,
                        nav: c1_nav[key],
                        dots: c1_pag[key],
                        autoplay: c1_auto[key],
                        slideBy: c1_slideby[key],
                        rewindNav: c1_rewind[key],
                        navigationText: ["", ""],
                        loop:false,
                        slideSpeed: 200 
                    });
                }
            }
        }
    if(typeof cattab1_unique !== 'undefined') {
        for (var i = 0 ; i < cattab1_unique.length ; i++) {
            var key = cattab1_unique[i];
            if($(".categorytab-carousel-" + key).length) {       
                var categorytabCarousel = $(".categorytab-carousel-" + key);          
                var rtl = false;
                if ($("body").hasClass("rtl")) rtl = true;              
                categorytabCarousel.owlCarousel({
                    responsiveClass:true,
                    responsive:{            
                        1367:{
                            items:cattab1_itemsDesktop[key]
                        },
                        1200:{
                            items:cattab1_itemsDesktopSmall[key]
                        },
                        768:{
                            items:cattab1_itemsTablet[key]
                        },
                        481:{
                            items:cattab1_itemsMobile[key]
                        },
                        0:{
                            items:1
                        }
                    },
                    rtl: rtl,
                    margin: 30,
                    nav: cattab1_nav[key],
                    dots: cattab1_pag[key],
                    autoplay: cattab1_auto[key],
                    slideBy: cattab1_slideby[key],
                    rewindNav: cattab1_rewind[key],
                    navigationText: ["", ""],
                    slideSpeed: 200 
                });
            }
        }
    }
    if($(".customs-carousel-product").length) {
              var customsCarouselProduct = $(".customs-carousel-product");
            var rtl = false;
            if ($("body").hasClass("rtl")) rtl = true;
            customsCarouselProduct.owlCarousel({
                responsiveClass:true,
                responsive:{            
                    1200:{
                      items:4
                    },
                    1199:{
                        items:3
                    },
                    991:{
                        items:2
                    },
                    481:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                },
                rtl: rtl,
                margin: 15,
                nav: false,
                dots: false,
                autoplay: false,
                slideSpeed: 200,
                loop:false,
            });
        }
});

function initMap() {
		var coords = [
        {lat: 37.42006360000001, lng: -122.08215130000002, zoom: 12, maptype: 'roadmap', mapstyle:''},
        {lat: 40.7127837, lng: -74.00594130000002, zoom: 12 , maptype: 'terrain', mapstyle: ''},
        {lat: 40.7127837, lng: -74.00594130000002, zoom: 10 , maptype: 'roadmap', mapstyle:[{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]},
        {lat: -33.8688197, lng: 151.20929550000005, zoom: 15, maptype: 'roadmap', mapstyle:[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]},
        {lat: 40.7127837, lng: -74.00594130000002, zoom: 13, maptype: 'hybrid', mapstyle:''},
        {lat: 29.589869, lng: 102.04752600000006, zoom: 12 , maptype: 'satellite', mapstyle: ''},
		];
		var markers = [];
		var maps = [];
		for(var i = 0, length = coords.length; i < length; i++)
		{
			var point = coords[i];
			var latlng = new google.maps.LatLng(point.lat, point.lng);

			maps[i] = new google.maps.Map(document.getElementById('map_' + (i + 1)), {
				zoom: point.zoom,
				center: latlng,
				mapTypeId: point.maptype,
				styles: point.mapstyle  
			});

			markers[i] = new google.maps.Marker({
				position: latlng,
				map: maps[i]
			});
		}
}

/*fixed menu*/
    $(document).ready(function() {
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll > 200 && $("body").hasClass("page-index")) {
                $('.header-bottom').addClass('navbar-fixed-top');
                $('.top-bar').addClass('navbar-fixed-top');
                $('#header').addClass('change-style');
            }   
            else if (scroll > 0) {
                $('.header-bottom').addClass('navbar-fixed-top');
                $('.top-bar').addClass('navbar-fixed-top');
                $('#header').addClass('change-style');
            } 
            else {
                $('.header-bottom').removeClass('navbar-fixed-top');
                $('.top-bar').removeClass('navbar-fixed-top');
                $('#header').removeClass('change-style');
            }
        });
        
        $('#mobile-vermegamenu').click(function(e) {
            e.stopPropagation();
        });
    });
 jQuery(document).ready(function($) {
    if($(".product-carousel-wrapper").length) {
        if( $(window).width() > 991 ) {
            var c_height = $('.product-carousel-wrapper').outerHeight();
            var c_width = $('.product-carousel-wrapper .product-carousel').width();
            var c_show = c_height * 40 / 100;
            var c_hide = c_height * 35 / 100;
            var c_offset = $('.product-carousel-wrapper').offset().top;
            var sc_width = $( window ).width();
            var w_space = (sc_width - c_width) / 2;
        
            $('.product-carousel-wrapper .owl-controls > .owl-prev').css('left', (w_space - 60) );
            $('.product-carousel-wrapper .owl-controls > .owl-next').css('left', (w_space + c_width) );
            $( window ).scroll(function() {
                var scroll = $(this).scrollTop();
                if(scroll >= (c_offset - c_show) ) {
                    $('.product-carousel-wrapper').addClass('show-arrow');         
                } 
                if(scroll >= (c_offset + c_hide) ) {
                    $('.product-carousel-wrapper').removeClass('show-arrow');
                }   
               if(scroll < (c_offset - c_show) ) {
                    $('.product-carousel-wrapper').removeClass('show-arrow');
               }
            });   
        }
    }
 });

 jQuery(document).ready(function($) {
     //jQuery time code pour le formulaire pour la création de la boutique du vendeur
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $(".next").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();
        
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        
        //show the next fieldset
        next_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
            'transform': 'scale('+scale+')',
            'position': 'relative'
          });
                next_fs.css({'left': left, 'opacity': opacity});
            }, 
            duration: 100, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        
        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        
        //show the previous fieldset
        previous_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            }, 
            duration: 100, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".submit").click(function(){
        return false;
    })
});

 /*code pour afficher le bloc de l'option personnalisé au niveau du champ sexe*/
 /* code pour afficher l'image du logo de la boutique en preview*/
 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function() {
    $("#imageUpload").change(function() {
        console.log("fdfd");
        readURL(this);
    });
});
$(document).ready(function() {
    $("#inputGroupFile01").change(function(event) {  
      RecurFadeIn();
      readURL(this);    
    });
    $("#inputGroupFile01").on('click',function(event){
      RecurFadeIn();
    });
    function readURL(input) {    
      if (input.files && input.files[0]) {   
        var reader = new FileReader();
        var filename = $("#inputGroupFile01").val();
        filename = filename.substring(filename.lastIndexOf('\\')+1);
        reader.onload = function(e) {
          $('#cover_shop').css('background-image', 'url('+e.target.result +')');
          $('#cover_shop').hide();
          $('#cover_shop').fadeIn(500);      
          //$('.custom-file-label').text(filename);             
        }
        reader.readAsDataURL(input.files[0]);    
      } 
      $(".alert").removeClass("loading").hide();
    }
    function RecurFadeIn(){ 
      console.log('ran');
      FadeInAlert("Wait for it...");  
    }
    function FadeInAlert(text){
      $(".alert").show();
      $(".alert").text(text).addClass("loading");  
    }
});
 /*fin code*/
 /*js multi form edit profile*/
 ;(function($) {
    "use strict";  
    
    //* Form js
    function verificationForm(){
        //jQuery time
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $(".next").click(function () {
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'position': 'absolute'
                    });
                    next_fs.css({
                        'left': left,
                        'opacity': opacity
                    });
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".previous").click(function () {
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //de-activate current step on progressbar
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1 - now) * 50) + "%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'left': left
                    });
                    previous_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'opacity': opacity
                    });
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".submit").click(function () {
            return false;
        })
    }; 
    
    //* Add Phone no select
    function phoneNoselect(){
        if ( $('#msform').length ){   
            $("#phone").intlTelInput(); 
            $("#phone").intlTelInput("setNumber", "+880"); 
        };
    }; 
    //* Select js
    function nice_Select(){
        if ( $('.product_select').length ){ 
            $('select').niceSelect();
        };
    }; 
    /*Function Calls*/  
    verificationForm ();
    phoneNoselect ();
    nice_Select ();
})(jQuery); 
 /*fin code*/
  
