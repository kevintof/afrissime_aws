{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos.css" />
 <style>
   th{
    color: white,
    background-color: black,
   };
   table{
    width:50%,
   }

 </style>
{/block}
{block name='page_header_container'}{/block}

 {block name='page_content_top'}
    
 {/block}
  {block name='page_content'}
  <p class="MsoNormal" style="text-align:center;"><b><span style="font-size:16pt;line-height:107%;font-family:Cambria, serif;" xml:lang="fr" lang="fr">GUIDE DES TAILLES</span></b></p>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <p style="text-align:justify">
           Chez Afrissime.com, nous ne croyons pas à l’illusion de tailles prédéfinies. Chaque corps est différent et mérite une tenue tout aussi unique. Du coup, pour utiliser ce guide, nous vous recommandons de plutôt utiliser vos mensurations en cm (comme expliqué ci-dessous) plutôt que des tailles 38,44 ou XL ou autre.<br/> À partir de là, sélectionner une taille pour votre commande et chaque créatrice pourra alors s’assurer de faire une tenue PARFAITE pour VOUS ! 
        </p>
        </div>
      </div>
      <div class="row text-center container-fluid justify-content-center">
          <p>
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="#home" class="nav-link active" data-toggle="tab">Hommes</a>
                </li>
                <li class="nav-item">
                    <a href="#profile" class="nav-link" data-toggle="tab">Femmes</a>
                </li>
                <li class="nav-item">
                    <a href="#messages" class="nav-link" data-toggle="tab">Enfants</a>
                </li>
            </ul>
          </p>
          <div class="col-md-12">
            <div class="tab-content">
              <div class="tab-pane fade active" id="home">
                  <p>
                    <ul class="nav nav-tabs">
                      <li class="nav-item"><a href="#vetements" class="nav-link active" data-toggle="tab">Vêtements</a></li>
                      <li class="nav-item"><a href="#sous-vetements" class="nav-link" data-toggle="tab">Sous-vêtements</a></li>
                      <li class="nav-item"><a href="#chaussures" class="nav-link" data-toggle="tab">Chaussures</a></li>
                      <li class="nav-item"><a href="#accessoires" class="nav-link" data-toggle="tab">Accessoires</a></li>
                    </ul>
                    <div class="col-md-12"> 
                      <div class="tab-content">
                        <div id="vetements" class="tab-pane fade active in">
                          <p></p>
                            <h2></h2>
                            <div class="container">
                              <h1 style="text-align:left;margin-left:60px;"><strong>Chemises</strong></h1>
                              <table class="table table-bordered table-hover table-condensed" style="width:75%;text-align:center;margin-left:70px;margin-right:20px">
                                      <thead class="thead-dark" style="background-color:black;">
                                        <tr>
                                          <th style="color:white;font-weight:bold;"><strong>Int</strong></th>
                                          <th style="color:white;font-weight:bold;"><strong>Tour de poitrine (<span class="size-unit">cm</span>)</strong></th>
                                          <th style="color:white;font-weight:bold;"><strong>Tour de cou (<span class="size-unit">cm</span>)</strong></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td nowrap=""><strong>XXS</strong></td>
                                          <td nowrap=""><span class="s-inch hidden">32"-34"</span><span class="s-cm">81-86</span></td>
                                          <td nowrap=""><span class="s-inch hidden">14"</span><span class="s-cm">36</span></td>
                                        </tr>
                                        <tr>
                                          <td nowrap=""><strong>XS</strong></td>
                                          <td nowrap=""><span class="s-inch hidden">34"-36"</span><span class="s-cm">86-91</span></td>
                                          <td nowrap=""><span class="s-inch hidden">14.5"</span><span class="s-cm">37</span></td>
                                        </tr>
                                        <tr>
                                          <td nowrap=""><strong>S</strong></td>
                                          <td nowrap=""><span class="s-inch hidden">36"-38"</span><span class="s-cm">91-96</span></td>
                                          <td nowrap=""><span class="s-inch hidden">15"</span><span class="s-cm">38</span></td>
                                        </tr>
                                        <tr>
                                          <td nowrap=""><strong>M</strong></td>
                                          <td nowrap=""><span class="s-inch hidden">38"-40"</span><span class="s-cm">96-101</span></td>
                                          <td nowrap=""><span class="s-inch hidden">16"</span><span class="s-cm">41</span></td>
                                        </tr>
                                        <tr>
                                          <td nowrap=""><strong>L</strong></td>
                                          <td nowrap=""><span class="s-inch hidden">40"-42"</span><span class="s-cm">101-106</span></td>
                                          <td nowrap=""><span class="s-inch hidden">17"</span><span class="s-cm">43</span></td>
                                        </tr>
                                        <tr>
                                          <td nowrap=""><strong>XL</strong></td>
                                          <td nowrap=""><span class="s-inch hidden">42"-44"</span><span class="s-cm">106-111</span></td>
                                          <td nowrap=""><span class="s-inch hidden">17.5"</span><span class="s-cm">44</span></td>
                                        </tr>
                                        <tr>
                                          <td nowrap=""><strong>XXL</strong></td>
                                          <td nowrap=""><span class="s-inch hidden">44"-46"</span><span class="s-cm">111-116</span></td>
                                          <td nowrap=""><span class="s-inch hidden">18"</span><span class="s-cm">46</span></td>
                                        </tr>
                                        <tr>
                                          <td nowrap=""><strong>3XL</strong></td>
                                          <td nowrap=""><span class="s-inch hidden">46"-48"</span><span class="s-cm">116 - 121</span></td>
                                          <td nowrap=""><span class="s-inch hidden">18.5"</span><span class="s-cm">47</span></td>
                                        </tr>
                                      </tbody>
                               </table>

                            </div>
                            
                        </div>
                        <div id="sous-vetements" class="tab-pane fade">
                          <h3></h3>
                          <p></p>
                          
                            <div class="table-responsive text-center container-fluid justify-content-center">

                             <table class="table table-bordered table-hover table-condensed">
                            <thead class="thead-dark" style="background-color:black;">
                              <tr>
                                <th style="color:white;font-weight:bold;"><strong>Int</strong></th>
                                <th style="color:white;font-weight:bold;"><strong>Tour de poitrine (<span class="size-unit">cm</span>)</strong></th>
                                <th style="color:white;font-weight:bold;"><strong>Tour de taille (<span class="size-unit">cm</span>)</strong></th>
                                <th style="color:white;font-weight:bold;"><strong>Tour de hanche (<span class="size-unit">cm</span>)</strong></th>
                                <th style="color:white;font-weight:bold;"><strong>EU</strong></th>
                                <th style="color:white;font-weight:bold;"><strong>UK</strong></th>
                                <th style="color:white;font-weight:bold;"><strong>US</strong></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td nowrap=""><strong>XXS</strong></td>
                                <td nowrap=""><span class="s-inch hidden">30.7"</span><span class="s-cm">78</span></td>
                                <td nowrap=""><span class="s-inch hidden">23.6"</span><span class="s-cm">60</span></td>
                                <td nowrap=""><span class="s-inch hidden">33.1"</span><span class="s-cm">84</span></td>
                                <td nowrap="">32</td>
                                <td nowrap="">4</td>
                                <td nowrap="">0</td>
                              </tr>
                              <tr>
                                <td nowrap=""><strong>XS</strong></td>
                                <td nowrap=""><span class="s-inch hidden">31.5"</span><span class="s-cm">80</span></td>
                                <td nowrap=""><span class="s-inch hidden">24.4"</span><span class="s-cm">62</span></td>
                                <td nowrap=""><span class="s-inch hidden">33.9"</span><span class="s-cm">86</span></td>
                                <td nowrap="">34</td>
                                <td nowrap="">6</td>
                                <td nowrap="">2</td>
                              </tr>
                              <tr>
                                <td nowrap=""><strong>S</strong></td>
                                <td nowrap=""><span class="s-inch hidden">32.7"</span><span class="s-cm">83</span></td>
                                <td nowrap=""><span class="s-inch hidden">25.6"</span><span class="s-cm">65</span></td>
                                <td nowrap=""><span class="s-inch hidden">35"</span><span class="s-cm">89</span></td>
                                <td nowrap="">36</td>
                                <td nowrap="">8</td>
                                <td nowrap="">4</td>
                              </tr>
                              <tr>
                                <td nowrap=""><strong>M</strong></td>
                                <td nowrap=""><span class="s-inch hidden">34.6"</span><span class="s-cm">88</span></td>
                                <td nowrap=""><span class="s-inch hidden">27.6"</span><span class="s-cm">70</span></td>
                                <td nowrap=""><span class="s-inch hidden">37"</span><span class="s-cm">94</span></td>
                                <td nowrap="">38</td>
                                <td nowrap="">10</td>
                                <td nowrap="">6</td>
                              </tr>
                              <tr>
                                <td nowrap=""><strong>L</strong></td>
                                <td nowrap=""><span class="s-inch hidden">36.6"</span><span class="s-cm">93</span></td>
                                <td nowrap=""><span class="s-inch hidden">29.5"</span><span class="s-cm">75</span></td>
                                <td nowrap=""><span class="s-inch hidden">39"</span><span class="s-cm">99</span></td>
                                <td nowrap="">40</td>
                                <td nowrap="">12</td>
                                <td nowrap="">8</td>
                              </tr>
                              <tr>
                                <td nowrap=""><strong>XL</strong></td>
                                <td nowrap=""><span class="s-inch hidden">38.6"</span><span class="s-cm">98</span></td>
                                <td nowrap=""><span class="s-inch hidden">31.5"</span><span class="s-cm">80</span></td>
                                <td nowrap=""><span class="s-inch hidden">40.9"</span><span class="s-cm">104</span></td>
                                <td nowrap="">42</td>
                                <td nowrap="">14</td>
                                <td nowrap="">10</td>
                              </tr>
                              <tr>
                                <td nowrap=""><strong>XXL</strong></td>
                                <td nowrap=""><span class="s-inch hidden">40.5"</span><span class="s-cm">103</span></td>
                                <td nowrap=""><span class="s-inch hidden">33.5"</span><span class="s-cm">85</span></td>
                                <td nowrap=""><span class="s-inch hidden">42.9"</span><span class="s-cm">109</span></td>
                                <td nowrap="">44</td>
                                <td nowrap="">16</td>
                                <td nowrap="">12</td>
                              </tr>
                              <tr>
                                <td nowrap=""><strong>3XL</strong></td>
                                <td nowrap=""><span class="s-inch hidden">43.3"</span><span class="s-cm">110</span></td>
                                <td nowrap=""><span class="s-inch hidden">36.2"</span><span class="s-cm">92</span></td>
                                <td nowrap=""><span class="s-inch hidden">45.7"</span><span class="s-cm">116</span></td>
                                <td nowrap="">46</td>
                                <td nowrap="">18</td>
                                <td nowrap="">14</td>
                              </tr>
                            </tbody>
                          </table>
                            </div>
                        </div>
                        <div id="chaussures" class="tab-pane fade">
                          <h3></h3>
                          <p></p>
                            <table class="table table-bordered table-hover table-condensed">
                                  <thead class="thead-dark" style="background-color:black;">
                                    <tr>
                                      <th style="color:white;font-weight:bold;"><strong>EU</strong></th>
                                      <th style="color:white;font-weight:bold;"><strong>UK</strong></th>
                                      <th style="color:white;font-weight:bold;"><strong>US Women</strong></th>
                                      <th style="color:white;font-weight:bold;"><strong>US Men</strong></th>
                                      <th style="color:white;font-weight:bold;">
                                        <strong>
                                        <span class="size-unit">cm</span>
                                         </strong>
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td nowrap="">36</td>
                                      <td nowrap="">3.5</td>
                                      <td nowrap="">4</td>
                                      <td nowrap="">5</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">8.7"</span>
                                        <span class="s-cm">22.1</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">37</td>
                                      <td nowrap="">4.5</td>
                                      <td nowrap="">5</td>
                                      <td nowrap="">6</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">9"</span>
                                        <span class="s-cm">22.9</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">38</td>
                                      <td nowrap="">5</td>
                                      <td nowrap="">5.5</td>
                                      <td nowrap="">6.5</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">9.2"</span>
                                        <span class="s-cm">23.3</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">39</td>
                                      <td nowrap="">6</td>
                                      <td nowrap="">6.5</td>
                                      <td nowrap="">7.5</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">9.5"</span>
                                        <span class="s-cm">24.2</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">40</td>
                                      <td nowrap="">6.5</td>
                                      <td nowrap="">7</td>
                                      <td nowrap="">8</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">9.7"</span>
                                        <span class="s-cm">24.6</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">41</td>
                                      <td nowrap="">7.5</td>
                                      <td nowrap="">8</td>
                                      <td nowrap="">9</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">10"</span>
                                        <span class="s-cm">25.5</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">42</td>
                                      <td nowrap="">8</td>
                                      <td nowrap="">8.5</td>
                                      <td nowrap="">9.5</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">10.2"</span>
                                        <span class="s-cm">25.9</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">43</td>
                                      <td nowrap="">9</td>
                                      <td nowrap="">9.5</td>
                                      <td nowrap="">10.5</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">10.5"</span>
                                        <span class="s-cm">26.7</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">44</td>
                                      <td nowrap="">9.5</td>
                                      <td nowrap="">10</td>
                                      <td nowrap="">11</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">10.7"</span>
                                        <span class="s-cm">27.1</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">45</td>
                                      <td nowrap="">10.5</td>
                                      <td nowrap="">11</td>
                                      <td nowrap="">12</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">11"</span>
                                        <span class="s-cm">28</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">46</td>
                                      <td nowrap="">11</td>
                                      <td nowrap="">11.5</td>
                                      <td nowrap="">12.5</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">11.2"</span>
                                        <span class="s-cm">28.4</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">47</td>
                                      <td nowrap="">12</td>
                                      <td nowrap="">12.5</td>
                                      <td nowrap="">13.5</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">11.5"</span>
                                        <span class="s-cm">29.3</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td nowrap="">48</td>
                                      <td nowrap="">12.5</td>
                                      <td nowrap="">13</td>
                                      <td nowrap="">14</td>
                                      <td nowrap="">
                                        <span class="s-inch hidden">11.7"</span>
                                        <span class="s-cm">29.7</span>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                        </div>
                         <div id="accessoires" class="tab-pane fade">
                          <h3>accessoires</h3>
                          <p></p>
                        </div>
                      </div>
                    </div>
                  </p>
              </div>
              <div class="tab-pane fade" id="profile">
                <h1 style="text-align:left;margin-left:60px;"><strong>Soutiens gorge </strong></h1>
                <table class="table table-bordered table-hover table-condensed">
                <thead class="thead-dark" style="background-color:black;">
                  <tr>
                    <th style="color:white;font-weight:bold;"><strong>Mesure en-dessous des seins (<span class="size-unit">in</span>)</strong></th>
                    <th style="color:white;font-weight:bold;"><strong>Mesure au-dessus des seins (<span class="size-unit">in</span>)</strong></th>
                    <th style="color:white;font-weight:bold;"><strong>EU</strong></th>
                    <th style="color:white;font-weight:bold;"><strong>UK</strong></th>
                    <th style="color:white;font-weight:bold;"><strong>US</strong></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden"></span></td>
                    <td nowrap=""><span class="s-inch">27.6" - 28.3"</span><span class="s-cm hidden"></span></td>
                    <td nowrap="">75 (AA)</td>
                    <td nowrap=""></td>
                    <td nowrap=""></td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
                    <td nowrap=""><span class="s-inch">28.3" - 29.1"</span><span class="s-cm hidden">72 - 74</span></td>
                    <td nowrap="">75 (A)</td>
                    <td nowrap="">28A</td>
                    <td nowrap="">28A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
                    <td nowrap=""><span class="s-inch">29.1" - 29.9"</span><span class="s-cm hidden">74 - 76</span></td>
                    <td nowrap="">75 (B)</td>
                    <td nowrap="">28B</td>
                    <td nowrap="">28B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
                    <td nowrap=""><span class="s-inch">29.9" - 30.7"</span><span class="s-cm hidden">76 - 78</span></td>
                    <td nowrap="">75 (C)</td>
                    <td nowrap="">28C</td>
                    <td nowrap="">28C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
                    <td nowrap=""><span class="s-inch">30.7" - 31.5"</span><span class="s-cm hidden">78 - 80</span></td>
                    <td nowrap="">75 (D)</td>
                    <td nowrap="">28D</td>
                    <td nowrap="">28D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
                    <td nowrap=""><span class="s-inch">31.5" - 32.3"</span><span class="s-cm hidden">80 - 82</span></td>
                    <td nowrap="">75 (E)</td>
                    <td nowrap="">28DD</td>
                    <td nowrap="">28DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
                    <td nowrap=""><span class="s-inch">32.3" - 33.1"</span><span class="s-cm hidden">82 - 84</span></td>
                    <td nowrap="">75 (F)</td>
                    <td nowrap="">28E</td>
                    <td nowrap="">28DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
                    <td nowrap=""><span class="s-inch">33" - 33.9"</span><span class="s-cm hidden">84 - 86</span></td>
                    <td nowrap="">75 (G)</td>
                    <td nowrap="">28F</td>
                    <td nowrap="">28G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
                    <td nowrap=""><span class="s-inch">33.9" - 34.6"</span><span class="s-cm hidden">86 - 88</span></td>
                    <td nowrap="">75 (H)</td>
                    <td nowrap="">28FF</td>
                    <td nowrap="">28H</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">24.4" - 26.4"</span><span class="s-cm hidden">62 - 67</span></td>
                    <td nowrap=""><span class="s-inch">30.3" - 31.1"</span><span class="s-cm hidden">77 - 79</span></td>
                    <td nowrap="">80 (A)</td>
                    <td nowrap="">30A</td>
                    <td nowrap="">30A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">24.4" - 26.4"</span><span class="s-cm hidden">62 - 67</span></td>
                    <td nowrap=""><span class="s-inch">31.1" - 31.9"</span><span class="s-cm hidden">79 - 81</span></td>
                    <td nowrap="">80 (B)</td>
                    <td nowrap="">30B</td>
                    <td nowrap="">30B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">24.4" - 26.4"</span><span class="s-cm hidden">62 - 67</span></td>
                    <td nowrap=""><span class="s-inch">31.9" - 32.7"</span><span class="s-cm hidden">81 - 83</span></td>
                    <td nowrap="">80 (C)</td>
                    <td nowrap="">30C</td>
                    <td nowrap="">30C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">24.4" - 26.4"</span><span class="s-cm hidden">62 - 67</span></td>
                    <td nowrap=""><span class="s-inch">32.7" - 33.5"</span><span class="s-cm hidden">83 - 85</span></td>
                    <td nowrap="">80 (D)</td>
                    <td nowrap="">30D</td>
                    <td nowrap="">30D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">24.4" - 26.4"</span><span class="s-cm hidden">62 - 67</span></td>
                    <td nowrap=""><span class="s-inch">33.5" - 34.2"</span><span class="s-cm hidden">85 - 87</span></td>
                    <td nowrap="">80 (E)</td>
                    <td nowrap="">30DD</td>
                    <td nowrap="">30DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">24.4" - 26.4"</span><span class="s-cm hidden">62 - 67</span></td>
                    <td nowrap=""><span class="s-inch">34.2" - 35"</span><span class="s-cm hidden">87 - 89</span></td>
                    <td nowrap="">80 (F)</td>
                    <td nowrap="">30E</td>
                    <td nowrap="">30DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">24.4" - 26.4"</span><span class="s-cm hidden">62 - 67</span></td>
                    <td nowrap=""><span class="s-inch">35" - 35.8"</span><span class="s-cm hidden">89 - 91</span></td>
                    <td nowrap="">80 (G)</td>
                    <td nowrap="">30F</td>
                    <td nowrap="">30G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">24.4" - 26.4"</span><span class="s-cm hidden">62 - 67</span></td>
                    <td nowrap=""><span class="s-inch">35.8" - 36.6"</span><span class="s-cm hidden">91 - 93</span></td>
                    <td nowrap="">80 (H)</td>
                    <td nowrap="">30FF</td>
                    <td nowrap="">30H</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">26.4" - 28.3"</span><span class="s-cm hidden">67 - 72</span></td>
                    <td nowrap=""><span class="s-inch">32.3" - 33.1"</span><span class="s-cm hidden">82 - 84</span></td>
                    <td nowrap="">85 (A)</td>
                    <td nowrap="">32A</td>
                    <td nowrap="">32A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">26.4" - 28.3"</span><span class="s-cm hidden">67 - 72</span></td>
                    <td nowrap=""><span class="s-inch">33.1" - 33.9"</span><span class="s-cm hidden">84 - 86</span></td>
                    <td nowrap="">85 (B)</td>
                    <td nowrap="">32B</td>
                    <td nowrap="">32B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">26.4" - 28.3"</span><span class="s-cm hidden">67 - 72</span></td>
                    <td nowrap=""><span class="s-inch">33.9" - 34.6"</span><span class="s-cm hidden">86 - 88</span></td>
                    <td nowrap="">85 (C)</td>
                    <td nowrap="">32C</td>
                    <td nowrap="">32C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">26.4" - 28.3"</span><span class="s-cm hidden">67 - 72</span></td>
                    <td nowrap=""><span class="s-inch">34.6" - 35.4"</span><span class="s-cm hidden">88 - 90</span></td>
                    <td nowrap="">85 (D)</td>
                    <td nowrap="">32D</td>
                    <td nowrap="">32D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">26.4" - 28.3"</span><span class="s-cm hidden">67 - 72</span></td>
                    <td nowrap=""><span class="s-inch">35.4" - 36.2"</span><span class="s-cm hidden">90 - 92</span></td>
                    <td nowrap="">85 (E)</td>
                    <td nowrap="">32DD</td>
                    <td nowrap="">32DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">26.4" - 28.3"</span><span class="s-cm hidden">67 - 72</span></td>
                    <td nowrap=""><span class="s-inch">36.2" - 37"</span><span class="s-cm hidden">92 - 94</span></td>
                    <td nowrap="">85 (F)</td>
                    <td nowrap="">32E</td>
                    <td nowrap="">32DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">26.4" - 28.3"</span><span class="s-cm hidden">67 - 72</span></td>
                    <td nowrap=""><span class="s-inch">37" - 37.8"</span><span class="s-cm hidden">94 - 96</span></td>
                    <td nowrap="">85 (G)</td>
                    <td nowrap="">32F</td>
                    <td nowrap="">32G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">26.4" - 28.3"</span><span class="s-cm hidden">67 - 72</span></td>
                    <td nowrap=""><span class="s-inch">37.8" - 38.6"</span><span class="s-cm hidden">96 - 98</span></td>
                    <td nowrap="">85 (H)</td>
                    <td nowrap="">32FF</td>
                    <td nowrap="">32H</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">28.3" - 30.3"</span><span class="s-cm hidden">72 - 77</span></td>
                    <td nowrap=""><span class="s-inch">34.2" - 35"</span><span class="s-cm hidden">87 - 89</span></td>
                    <td nowrap="">90 (A)</td>
                    <td nowrap="">34A</td>
                    <td nowrap="">34A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">28.3" - 30.3"</span><span class="s-cm hidden">72 - 77</span></td>
                    <td nowrap=""><span class="s-inch">35" - 35.8"</span><span class="s-cm hidden">89 - 91</span></td>
                    <td nowrap="">90 (B)</td>
                    <td nowrap="">34B</td>
                    <td nowrap="">34B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">28.3" - 30.3"</span><span class="s-cm hidden">72 - 77</span></td>
                    <td nowrap=""><span class="s-inch">35.8" - 36.6"</span><span class="s-cm hidden">91 - 93</span></td>
                    <td nowrap="">90 (C)</td>
                    <td nowrap="">34C</td>
                    <td nowrap="">34C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">28.3" - 30.3"</span><span class="s-cm hidden">72 - 77</span></td>
                    <td nowrap=""><span class="s-inch">36.6" - 37.4"</span><span class="s-cm hidden">93 - 95</span></td>
                    <td nowrap="">90 (D)</td>
                    <td nowrap="">34D</td>
                    <td nowrap="">34D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">28.3" - 30.3"</span><span class="s-cm hidden">72 - 77</span></td>
                    <td nowrap=""><span class="s-inch">37.4" - 38.2"</span><span class="s-cm hidden">95 - 97</span></td>
                    <td nowrap="">90 (E)</td>
                    <td nowrap="">34DD</td>
                    <td nowrap="">34DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">28.3" - 30.3"</span><span class="s-cm hidden">72 - 77</span></td>
                    <td nowrap=""><span class="s-inch">38.2" - 39"</span><span class="s-cm hidden">97 - 99</span></td>
                    <td nowrap="">90 (F)</td>
                    <td nowrap="">34E</td>
                    <td nowrap="">34DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">28.3" - 30.3"</span><span class="s-cm hidden">72 - 77</span></td>
                    <td nowrap=""><span class="s-inch">39" - 39.8"</span><span class="s-cm hidden">99 - 101</span></td>
                    <td nowrap="">90 (G)</td>
                    <td nowrap="">34F</td>
                    <td nowrap="">34G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">28.3" - 30.3"</span><span class="s-cm hidden">72 - 77</span></td>
                    <td nowrap=""><span class="s-inch">39.8" - 40.5"</span><span class="s-cm hidden">101 - 103</span></td>
                    <td nowrap="">90 (H)</td>
                    <td nowrap="">34FF</td>
                    <td nowrap="">34H</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">30.3" - 32.3"</span><span class="s-cm hidden">77 - 82</span></td>
                    <td nowrap=""><span class="s-inch">36.2" - 37"</span><span class="s-cm hidden">92 - 94</span></td>
                    <td nowrap="">95 (A)</td>
                    <td nowrap="">36A</td>
                    <td nowrap="">36A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">30.3" - 32.3"</span><span class="s-cm hidden">77 - 82</span></td>
                    <td nowrap=""><span class="s-inch">37" - 37.8"</span><span class="s-cm hidden">94 - 96</span></td>
                    <td nowrap="">95 (B)</td>
                    <td nowrap="">36B</td>
                    <td nowrap="">36B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">30.3" - 32.3"</span><span class="s-cm hidden">77 - 82</span></td>
                    <td nowrap=""><span class="s-inch">37.8" - 38.6"</span><span class="s-cm hidden">96 - 98</span></td>
                    <td nowrap="">95 (C)</td>
                    <td nowrap="">36C</td>
                    <td nowrap="">36C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">30.3" - 32.3"</span><span class="s-cm hidden">77 - 82</span></td>
                    <td nowrap=""><span class="s-inch">38.6" - 39.4"</span><span class="s-cm hidden">98 - 100</span></td>
                    <td nowrap="">95 (D)</td>
                    <td nowrap="">36D</td>
                    <td nowrap="">36D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">30.3" - 32.3"</span><span class="s-cm hidden">77 - 82</span></td>
                    <td nowrap=""><span class="s-inch">39.4" - 40.2"</span><span class="s-cm hidden">100 - 102</span></td>
                    <td nowrap="">95 (E)</td>
                    <td nowrap="">36DD</td>
                    <td nowrap="">36DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">30.3" - 32.3"</span><span class="s-cm hidden">77 - 82</span></td>
                    <td nowrap=""><span class="s-inch">40.2" - 40.9"</span><span class="s-cm hidden">102 - 104</span></td>
                    <td nowrap="">95 (F)</td>
                    <td nowrap="">36E</td>
                    <td nowrap="">36DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">30.3" - 32.3"</span><span class="s-cm hidden">77 - 82</span></td>
                    <td nowrap=""><span class="s-inch">40.9" - 41.7"</span><span class="s-cm hidden">104 - 106</span></td>
                    <td nowrap="">95 (G)</td>
                    <td nowrap="">36F</td>
                    <td nowrap="">36G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">30.3" - 32.3"</span><span class="s-cm hidden">77 - 82</span></td>
                    <td nowrap=""><span class="s-inch">41.7" - 42.5"</span><span class="s-cm hidden">106 - 108</span></td>
                    <td nowrap="">95 (H)</td>
                    <td nowrap="">36FF</td>
                    <td nowrap="">36H</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch"></span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch"></span><span class="s-cm hidden">95 - 97</span></td>
                    <td nowrap="">100 (AA)</td>
                    <td nowrap=""></td>
                    <td nowrap=""></td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">32.3" - 34.2"</span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch">38.2" - 39"</span><span class="s-cm hidden">97 - 99</span></td>
                    <td nowrap="">100 (A)</td>
                    <td nowrap="">38A</td>
                    <td nowrap="">38A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">32.3" - 34.2"</span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch">39" - 39.8"</span><span class="s-cm hidden">99 - 101</span></td>
                    <td nowrap="">100 (B)</td>
                    <td nowrap="">38B</td>
                    <td nowrap="">38B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">32.3" - 34.2"</span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch">39.8" - 40.5"</span><span class="s-cm hidden"> - 103</span></td>
                    <td nowrap="">100 (C)</td>
                    <td nowrap="">38C</td>
                    <td nowrap="">38C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">32.3" - 34.2"</span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch">40.5" - 41.3"</span><span class="s-cm hidden">103 - 105</span></td>
                    <td nowrap="">100 (D)</td>
                    <td nowrap="">38D</td>
                    <td nowrap="">38D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">32.3" - 34.2"</span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch">41.3" - 42.1"</span><span class="s-cm hidden">105 - 107</span></td>
                    <td nowrap="">100 (E)</td>
                    <td nowrap="">38DD</td>
                    <td nowrap="">38DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">32.3" - 34.2"</span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch">42.1" - 42.9"</span><span class="s-cm hidden">107 - 109</span></td>
                    <td nowrap="">100 (F)</td>
                    <td nowrap="">38E</td>
                    <td nowrap="">38DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">32.3" - 34.2"</span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch">42.9" - 43.7"</span><span class="s-cm hidden">109 - 111</span></td>
                    <td nowrap="">100 (G)</td>
                    <td nowrap="">38F</td>
                    <td nowrap="">38G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">32.3" - 34.2"</span><span class="s-cm hidden">82 - 87</span></td>
                    <td nowrap=""><span class="s-inch">43.7" - 44.5"</span><span class="s-cm hidden">111 - 113</span></td>
                    <td nowrap="">100 (H)</td>
                    <td nowrap="">38FF</td>
                    <td nowrap="">38H</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">34.2" - 36.2"</span><span class="s-cm hidden">87 - 92</span></td>
                    <td nowrap=""><span class="s-inch">40.2" - 40.9"</span><span class="s-cm hidden">102 - 104</span></td>
                    <td nowrap="">105 (A)</td>
                    <td nowrap="">40A</td>
                    <td nowrap="">40A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">34.2" - 36.2"</span><span class="s-cm hidden">87 - 92</span></td>
                    <td nowrap=""><span class="s-inch">40.9" - 41.7"</span><span class="s-cm hidden">104 - 106</span></td>
                    <td nowrap="">105 (B)</td>
                    <td nowrap="">40B</td>
                    <td nowrap="">40B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">34.2" - 36.2"</span><span class="s-cm hidden">87 - 92</span></td>
                    <td nowrap=""><span class="s-inch">41.7" - 42.5"</span><span class="s-cm hidden">106 - 108</span></td>
                    <td nowrap="">105 (C)</td>
                    <td nowrap="">40C</td>
                    <td nowrap="">40C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">34.2" - 36.2"</span><span class="s-cm hidden">87 - 92</span></td>
                    <td nowrap=""><span class="s-inch">42.5" - 43.3"</span><span class="s-cm hidden">108 - 110</span></td>
                    <td nowrap="">105 (D)</td>
                    <td nowrap="">40D</td>
                    <td nowrap="">40D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">34.2" - 36.2"</span><span class="s-cm hidden">87 - 92</span></td>
                    <td nowrap=""><span class="s-inch">43.3" - 44.1"</span><span class="s-cm hidden">110 - 112</span></td>
                    <td nowrap="">105 (E)</td>
                    <td nowrap="">40DD</td>
                    <td nowrap="">40DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">34.2" - 36.2"</span><span class="s-cm hidden">87 - 92</span></td>
                    <td nowrap=""><span class="s-inch">44.1" - 44.9"</span><span class="s-cm hidden">112 - 114</span></td>
                    <td nowrap="">105 (F)</td>
                    <td nowrap="">40E</td>
                    <td nowrap="">40DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">34.2" - 36.2"</span><span class="s-cm hidden">87 - 92</span></td>
                    <td nowrap=""><span class="s-inch">44.9" - 45.7"</span><span class="s-cm hidden">114 - 116</span></td>
                    <td nowrap="">105 (G)</td>
                    <td nowrap="">40F</td>
                    <td nowrap="">40G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">34.2" - 36.2"</span><span class="s-cm hidden">87 - 92</span></td>
                    <td nowrap=""><span class="s-inch">45.7" - 46.5"</span><span class="s-cm hidden">116 - 118</span></td>
                    <td nowrap="">105 (H)</td>
                    <td nowrap="">40FF</td>
                    <td nowrap="">40H</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">36.2" - 38.2"</span><span class="s-cm hidden">92 - 97</span></td>
                    <td nowrap=""><span class="s-inch">42.1" - 42.9"</span><span class="s-cm hidden">107 - 109</span></td>
                    <td nowrap="">110 (A)</td>
                    <td nowrap="">42A</td>
                    <td nowrap="">42A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">36.2" - 38.2"</span><span class="s-cm hidden">92 - 97</span></td>
                    <td nowrap=""><span class="s-inch">42.9" - 43.7"</span><span class="s-cm hidden">109 - 111</span></td>
                    <td nowrap="">110 (B)</td>
                    <td nowrap="">42B</td>
                    <td nowrap="">42B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">36.2" - 38.2"</span><span class="s-cm hidden">92 - 97</span></td>
                    <td nowrap=""><span class="s-inch">43.7" - 44.5"</span><span class="s-cm hidden">111 - 113</span></td>
                    <td nowrap="">110 (C)</td>
                    <td nowrap="">42C</td>
                    <td nowrap="">42C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">36.2" - 38.2"</span><span class="s-cm hidden">92 - 97</span></td>
                    <td nowrap=""><span class="s-inch">44.5" - 45.3"</span><span class="s-cm hidden">113 - 115</span></td>
                    <td nowrap="">110 (D)</td>
                    <td nowrap="">42D</td>
                    <td nowrap="">42D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">36.2" - 38.2"</span><span class="s-cm hidden">92 - 97</span></td>
                    <td nowrap=""><span class="s-inch">45.3" - 46.1"</span><span class="s-cm hidden">115 - 117</span></td>
                    <td nowrap="">110 (E)</td>
                    <td nowrap="">42DD</td>
                    <td nowrap="">42DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">36.2" - 38.2"</span><span class="s-cm hidden">92 - 97</span></td>
                    <td nowrap=""><span class="s-inch">46.1" - 46.8"</span><span class="s-cm hidden">117 - 119</span></td>
                    <td nowrap="">110 (F)</td>
                    <td nowrap="">42E</td>
                    <td nowrap="">42DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">36.2" - 38.2"</span><span class="s-cm hidden">92 - 97</span></td>
                    <td nowrap=""><span class="s-inch">46.8" - 47.6"</span><span class="s-cm hidden">119 - 121</span></td>
                    <td nowrap="">110 (G)</td>
                    <td nowrap="">42F</td>
                    <td nowrap="">42G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">36.2" - 38.2"</span><span class="s-cm hidden">92 - 97</span></td>
                    <td nowrap=""><span class="s-inch">47.6" - 48.4"</span><span class="s-cm hidden">121 - 123</span></td>
                    <td nowrap="">110 (H)</td>
                    <td nowrap="">42FF</td>
                    <td nowrap="">42H</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">38.2" - 40.2"</span><span class="s-cm hidden">97 - 102</span></td>
                    <td nowrap=""><span class="s-inch">44.1" - 44.9"</span><span class="s-cm hidden">112 - 114</span></td>
                    <td nowrap="">115 (A)</td>
                    <td nowrap="">44A</td>
                    <td nowrap="">44A</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">38.2" - 40.2"</span><span class="s-cm hidden">97 - 102</span></td>
                    <td nowrap=""><span class="s-inch">44.9" - 45.7"</span><span class="s-cm hidden">114 - 116</span></td>
                    <td nowrap="">115 (B)</td>
                    <td nowrap="">44B</td>
                    <td nowrap="">44B</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">38.2" - 40.2"</span><span class="s-cm hidden">97 - 102</span></td>
                    <td nowrap=""><span class="s-inch">45.7" - 46.5"</span><span class="s-cm hidden">116 - 118</span></td>
                    <td nowrap="">115 (C)</td>
                    <td nowrap="">44C</td>
                    <td nowrap="">44C</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">38.2" - 40.2"</span><span class="s-cm hidden">97 - 102</span></td>
                    <td nowrap=""><span class="s-inch">46.5" - 47.2"</span><span class="s-cm hidden">118 - 120</span></td>
                    <td nowrap="">115 (D)</td>
                    <td nowrap="">44D</td>
                    <td nowrap="">44D</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">38.2" - 40.2"</span><span class="s-cm hidden">97 - 102</span></td>
                    <td nowrap=""><span class="s-inch">47.2" - 48"</span><span class="s-cm hidden">120 - 122</span></td>
                    <td nowrap="">115 (E)</td>
                    <td nowrap="">44DD</td>
                    <td nowrap="">44DD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">38.2" - 40.2"</span><span class="s-cm hidden">97 - 102</span></td>
                    <td nowrap=""><span class="s-inch">48" - 48.8"</span><span class="s-cm hidden">122 - 124</span></td>
                    <td nowrap="">115 (F)</td>
                    <td nowrap="">44E</td>
                    <td nowrap="">44DDD</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">38.2" - 40.2"</span><span class="s-cm hidden">97 - 102</span></td>
                    <td nowrap=""><span class="s-inch">48.8" - 49.6"</span><span class="s-cm hidden">124 - 126</span></td>
                    <td nowrap="">115 (G)</td>
                    <td nowrap="">44F</td>
                    <td nowrap="">44G</td>
                  </tr>
                  <tr>
                    <td nowrap=""><span class="s-inch">38.2" - 40.2"</span><span class="s-cm hidden">97 - 102</span></td>
                    <td nowrap=""><span class="s-inch">49.6" - 50.4"</span><span class="s-cm hidden">126 - 128</span></td>
                    <td nowrap="">115 (H)</td>
                    <td nowrap="">44FF</td>
                    <td nowrap="">44H</td>
                  </tr>
                </tbody>
        </table>
         <br/><br/>
        <h1 style="text-align:left;margin-left:60px;"><strong> Pantalons,jupes et shorts</strong></h1>
         <table class="table table-bordered table-hover table-condensed">
          <thead class="thead-dark" style="background-color:black;">
            <tr>
              <th style="color:white;font-weight:bold;"><strong>Int</strong></th>
              <th style="color:white;font-weight:bold;"><strong>Tour de taille (<span class="size-unit">in</span>)</strong></th>
              <th style="color:white;font-weight:bold;"><strong>EU</strong></th>
              <th style="color:white;font-weight:bold;"><strong>UK</strong></th>
              <th style="color:white;font-weight:bold;"><strong>US</strong></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td nowrap=""><strong>XXS</strong></td>
              <td nowrap=""><span class="s-inch">22.8" - 24.4"</span><span class="s-cm hidden">58 - 62</span></td>
              <td nowrap="">32</td>
              <td nowrap="">4</td>
              <td nowrap="">0</td>
            </tr>
            <tr>
              <td nowrap=""><strong>XS</strong></td>
              <td nowrap=""><span class="s-inch">24.4" - 26"</span><span class="s-cm hidden">62 - 66</span></td>
              <td nowrap="">34</td>
              <td nowrap="">6</td>
              <td nowrap="">2</td>
            </tr>
            <tr>
              <td nowrap=""><strong>S</strong></td>
              <td nowrap=""><span class="s-inch">26" - 27.6"</span><span class="s-cm hidden">66 - 70</span></td>
              <td nowrap="">36</td>
              <td nowrap="">8</td>
              <td nowrap="">4</td>
            </tr>
            <tr>
              <td nowrap=""><strong>M</strong></td>
              <td nowrap=""><span class="s-inch">27.6" - 29.1"</span><span class="s-cm hidden">70 - 74</span></td>
              <td nowrap="">38</td>
              <td nowrap="">10</td>
              <td nowrap="">6</td>
            </tr>
            <tr>
              <td nowrap=""><strong>L</strong></td>
              <td nowrap=""><span class="s-inch">29.1" - 30.7"</span><span class="s-cm hidden">74 - 78</span></td>
              <td nowrap="">40</td>
              <td nowrap="">12</td>
              <td nowrap="">8</td>
            </tr>
            <tr>
              <td nowrap=""><strong>XL</strong></td>
              <td nowrap=""><span class="s-inch">30.7" - 32.3"</span><span class="s-cm hidden">78 - 82</span></td>
              <td nowrap="">42</td>
              <td nowrap="">14</td>
              <td nowrap="">10</td>
            </tr>
            <tr>
              <td nowrap=""><strong>XXL</strong></td>
              <td nowrap=""><span class="s-inch">32.3" - 33.9"</span><span class="s-cm hidden">82 - 86</span></td>
              <td nowrap="">44</td>
              <td nowrap="">16</td>
              <td nowrap="">12</td>
            </tr>
            <tr>
              <td nowrap=""><strong>3XL</strong></td>
              <td nowrap=""><span class="s-inch">33.9" - 35.4"</span><span class="s-cm hidden">86 - 90</span></td>
              <td nowrap="">46</td>
              <td nowrap="">18</td>
              <td nowrap="">14</td>
            </tr>
            <tr>
              <td nowrap=""><strong>4XL</strong></td>
              <td nowrap=""><span class="s-inch">35.4" - 38.6"</span><span class="s-cm hidden">90 - 98</span></td>
              <td nowrap="">48</td>
              <td nowrap="">20</td>
              <td nowrap="">16</td>
            </tr>
            <tr>
              <td nowrap=""><strong>5XL</strong></td>
              <td nowrap=""><span class="s-inch">38.6" - 41.7"</span><span class="s-cm hidden">98 - 106</span></td>
              <td nowrap="">50</td>
              <td nowrap="">22</td>
              <td nowrap="">18</td>
            </tr>
            <tr>
              <td nowrap=""><strong>6XL</strong></td>
              <td nowrap=""><span class="s-inch">41.7" - 44.9"</span><span class="s-cm hidden">106 - 114</span></td>
              <td nowrap="">52</td>
              <td nowrap="">24</td>
              <td nowrap="">20</td>
            </tr>
            <tr>
              <td nowrap=""><strong>7XL</strong></td>
              <td nowrap=""><span class="s-inch">44.9" - 48"</span><span class="s-cm hidden">114 - 122</span></td>
              <td nowrap="">54</td>
              <td nowrap="">26</td>
              <td nowrap="">22</td>
            </tr>
          </tbody>
        </table>
                  
              </div>
              <div class="tab-pane fade" id="messages">
              <h1 style="text-align:left;margin-left:60px;"><strong>Bebe </strong></h1>

              <table class="table table-bordered table-hover table-condensed">
                    <thead class="thead-dark" style="background-color:black;">
                      <tr>
                        <th style="color:white;font-weight:bold;"><strong>Age</strong></th>
                        <th style="color:white;font-weight:bold;"><strong>Poids moyen (kg)</strong></th>
                        <th style="color:white;font-weight:bold;"><strong>Stature (<span class="size-unit">in</span>)</strong></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td nowrap="">Nouveau né</td>
                        <td nowrap="">3,3</td>
                        <td nowrap=""><span class="s-inch">19.7"</span><span class="s-cm hidden">50</span></td>
                      </tr>
                      <tr>
                        <td nowrap="">1 mois</td>
                        <td nowrap="">4</td>
                        <td nowrap=""><span class="s-inch">21.3"</span><span class="s-cm hidden">54</span></td>
                      </tr>
                      <tr>
                        <td nowrap="">3 mois</td>
                        <td nowrap="">5 - 6</td>
                        <td nowrap=""><span class="s-inch">23.6"</span><span class="s-cm hidden">60</span></td>
                      </tr>
                      <tr>
                        <td nowrap="">6 mois</td>
                        <td nowrap="">7 - 8</td>
                        <td nowrap=""><span class="s-inch">26.4"</span><span class="s-cm hidden">67</span></td>
                      </tr>
                      <tr>
                        <td nowrap="">9 mois</td>
                        <td nowrap="">8 - 9</td>
                        <td nowrap=""><span class="s-inch">27.9"</span><span class="s-cm hidden">71</span></td>
                      </tr>
                      <tr>
                        <td nowrap="">12 mois</td>
                        <td nowrap="">9 - 10</td>
                        <td nowrap=""><span class="s-inch">29.1"</span><span class="s-cm hidden">74</span></td>
                      </tr>
                      <tr>
                        <td nowrap="">18 mois</td>
                        <td nowrap="">11</td>
                        <td nowrap=""><span class="s-inch">31.9"</span><span class="s-cm hidden">81</span></td>
                      </tr>
                      <tr>
                        <td nowrap="">24 mois</td>
                        <td nowrap="">12</td>
                        <td nowrap=""><span class="s-inch">33.9"</span><span class="s-cm hidden">86</span></td>
                      </tr>
                      <tr>
                        <td nowrap="">36 mois</td>
                        <td nowrap="">14</td>
                        <td nowrap=""><span class="s-inch">37"</span><span class="s-cm hidden">94</span></td>
                      </tr>
                    </tbody>
                </table>
                  
                
                </div>
                  <p></p>
              </div>
            </div>
          </div>
      </div>
    </div>
    

  {/block}
