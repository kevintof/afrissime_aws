{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script type="text/javascript" src="{$urls.js_url}commercial.js"></script>
{literal}

<script>
// hack to get the plugin to work without loading jQuery
window.jQuery = window.$ = function() {
  return {
    on: function() {}
  };
};
window.$.fn = {};
</script>
<script type="text/javascript">
function show_box_gender(){
  $('#box_gender').show();
}
function hide_box_gender(){
  $('#box_gender').hide();
}
function show_payment_box_1(){
  if ($("#virement").prop('checked')==true){ 
    $('#payment_box_1').show();
    $('#payment_box_2').hide();
    $('#payment_box_3').hide();
    $('#payment_box_4').hide();
    $('#payment_box_5').hide();
    $('#payment_box_6').hide();

  }
}
function show_payment_box_2(){
  if ($("#carte").prop('checked')==true){ 
    $('#payment_box_1').hide();
    $('#payment_box_2').show();
    $('#payment_box_3').hide();
    $('#payment_box_4').hide();
    $('#payment_box_5').hide();
    $('#payment_box_6').hide();
  }
}
function show_payment_box_3(){
  if ($("#mobile_money").prop('checked')==true){ 
    $('#payment_box_1').hide();
    $('#payment_box_2').hide();
    $('#payment_box_3').show();
    $('#payment_box_4').hide();
    $('#payment_box_5').hide();
    $('#payment_box_6').hide();
  }
  
}
function show_payment_box_4(){
  if ($("#cash").prop('checked')==true){ 
    $('#payment_box_1').hide();
    $('#payment_box_2').hide();
    $('#payment_box_3').hide();
    $('#payment_box_4').show();
    $('#payment_box_5').hide();
    $('#payment_box_6').hide();
  }
}
function show_payment_box_5(){
  if ($("#bitcoin").prop('checked')==true){ 
    $('#payment_box_1').hide();
    $('#payment_box_2').hide();
    $('#payment_box_3').hide();
    $('#payment_box_4').hide();
    $('#payment_box_5').show();
    $('#payment_box_6').hide();
  }
}
function show_payment_box_6(){
  if ($("#wallet").prop('checked')==true){ 
    $('#payment_box_1').hide();
    $('#payment_box_2').hide();
    $('#payment_box_3').hide();
    $('#payment_box_4').hide();
    $('#payment_box_5').hide();
    $('#payment_box_6').show();
  }
}
</script>
{/literal}
{/block}
{block name='page_header_container'}{/block}

 {block name='page_content_top'}
    
 {/block}
  {block name='page_content'}
   
     <!-- MultiStep Form -->
     <div class="wk-mp-block" style="border:1px solid #d5d5d5;">
        <div style="background-color:#333;overflow: hidden;padding: 20px 25px;text-transform: uppercase;">
          <span style="font-size: 18px;font-weight: 400;color:#FFF;">{l s='Devenir commercial consultant' d='Shop.Theme.Global'}</span>
        </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="{$urls.current_url}" method="post" id="wk_mp_seller_form" enctype="multipart/form-data" style="margin-bottom: 30px;">
                 <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active">Renseignements personnels</li>
                    <li>Paramètres du compte</li>
                    <li>Méthodes de paiement</li>
                </ul>
                <!-- fieldsets -->
                <fieldset>
                    <h2 class="fs-title">{l s='Renseignements personnels' d='Shop.Theme.Global'}</h2>
                    <h3 class="fs-subtitle">{l s='Remplissez les champs suivants afin de créer votre compte' d='Shop.Theme.Global'}</h3>
                    <div class="">
                      <div class="row">
                        <label for="gender_id" class="control-label required">
                          {if $language.iso_code=='fr'}Sexe{/if} {if $language.iso_code=='en'}Sex{/if} 
                        </label>
                      </div>
                      <div class="row">
                            <label class="radio-inline">
                                <span class="custom-radio">
                                  <input name="gender_id" type="radio" value="1" checked onclick="hide_box_gender();">
                                  <span></span>
                                </span>
                                {if $language.iso_code=='fr'}Homme{/if} 
                                {if $language.iso_code=='en'}Man{/if} 
                            </label>
                            <label class="radio-inline">
                                <span class="custom-radio">
                                  <input name="gender_id" type="radio" value="2" onclick="hide_box_gender();">
                                  <span></span>
                                </span>
                                {if $language.iso_code=='fr'}Femme{/if} 
                                {if $language.iso_code=='en'}Woman{/if} 
                            </label>
                            <label class="radio-inline">
                                <span class="custom-radio">
                                  <input name="gender_id" type="radio" value="3" onclick="show_box_gender();">
                                  <span></span>
                                </span>
                                {if $language.iso_code=='fr'}Personnalisé{/if} 
                                {if $language.iso_code=='en'}Gender neutral{/if} 
                            </label>
                          </div>
                          <div id="box_gender" style="display:none">
                            <div class="">
                              {if $language.iso_code=='fr'}
                                <span>Indiquer votre sexe </span>
                                <p>Me désigner avec le pronom suivant:</p>
                              {/if} 
                              {if $language.iso_code=='en'}
                                <span>Please indicate your gender </span>
                                <p>Designate me with the following pronoun:</p>
                              {/if} 
                               <input class="form-control" name="title" type="text" value="" placeholder="{if $language.iso_code=='fr'}Pronom{/if}{if $language.iso_code=='en'}Pronoum{/if}">
                            </div>
                          </div>
                    </div>
                    <input type="text" name="fname" placeholder="{l s='Prénoms' d='Shop.Theme.Global'}"/>
                    <input type="text" name="lname" placeholder="{l s='Nom' d='Shop.Theme.Global'}"/>
                    <div class=''>
                      <input type="text" name="birthday" placeholder="{if $language.iso_code=='fr'} Date d'anniversaire (Ex: 11/04/1970){/if}{if $language.iso_code=='en'} Birthdate (Ex: 11/04/1970){/if}"/>
                    </div>
                    <input type="text" name="phone" placeholder="{l s='Téléphone' d='Shop.Theme.Global'}"/>
                    <select name='country'>
                      <option value="">{l s='Select Country' mod='marketplace'}</option>
                      {foreach $country as $countrydetail}
                        <option value="{$countrydetail.id_country}">
                          {$countrydetail.name}
                        </option>
                      {/foreach}
                    </select>
                    <input type="text" name="city" placeholder="{l s='Ville' d='Shop.Theme.Global'}"/>
                    {if $language.iso_code == 'fr'}
                      <label class="wk_formfield_required_notify">
                          Les champs qui sont marqués (<span class="required">*</span>) sont obligatoires à remplir. 
                      </label>
                    {/if}
                    {if $language.iso_code == 'en'}
                      <label class="wk_formfield_required_notify">
                          Fields marked (<span class="required">*</span>) are required to fill. 
                      </label>
                    {/if}
                    <input type="button" name="next" class="next action-button" value="Suivant"/>
                </fieldset>
                <fieldset>
                    <h2 class="fs-title">Paramètres du compte</h2>
                    <h3 class="fs-subtitle">Créer un compte pour devenir membre de la communauté</h3>
                    <input type="text" name="email" placeholder="{l s='Email' d='Shop.Theme.Global'}"/>
                    <input type="password" name="pass" placeholder="{l s='Mot de passe' d='Shop.Theme.Global'}"/>
                    <input type="password" name="cpass" placeholder="{l s='Mot de passe de confirmation' d='Shop.Theme.Global'}"/>
                    <input type="button" name="previous" class="previous action-button-previous" value="Précédent"/>
                    <input type="button" name="next" class="next action-button" value="Suivant"/>
                </fieldset>
                <fieldset>
                    <h2 class="fs-title">Méthodes de paiement </h2>
                    <h3 class="fs-subtitle">{if $language.iso_code == 'fr'} Complètez les informations afin de recevoir vos paiements {/if}  {if $language.iso_code == 'en'}Fill in the information to receive your payments {/if}</h3>
                    <div class="">
                      <div class="">
                        <input type="checkbox" name="payment_mode_id[]" value="1" style="width:10%; padding-right:10px" id="virement" onclick="show_payment_box_1();"/> Bank transfer
                      </div>
                      <div id="payment_box_1"style="display:none">
                        <p style="text-align:center; font-size:14px;">{l s='Informations bancaires' mod='marketplace'}</p>
                        <div class="col-md-6">
                          <label for="bank_name" class="control-label required">
                          Bank name
                          </label>
                          <input type="text" name="bank_name" class="form-control" value="" placeholder="Bank name"/>
                        </div>
                        <div class="col-md-6">
                          <label for="rib" class="control-label required">
                          IBAN / BIC
                          </label>
                          <input type="text" name="rib" class="form-control" value="" placeholder="IBAN / BIC"/>
                        </div>
                      </div>
                    </div>
                    <input type="checkbox" name="payment_mode_id[]" value="2" id="carte" onclick="show_payment_box_2();"/> {l s='Credit card' d='Shop.Theme.Global'} <br/>
                    <div id="payment_box_2" style="display:none" class="col-md-12">
                      <p style="text-align:center; font-size:14px;">Visa or MasterCard information</p>
                      <div class="col-md-12">
                        <label for="name_card" class="control-label required">
                        Name of the card holder
                        </label>
                        <input type="text" name="name_card" class="form-control" placeholder="Name of the card holder" value=""/>
                      </div>
                      <div class="col-md-12">
                        <div class="col-md-9">
                          <label for="num_card" class="control-label required">
                          Number of the credit card
                          </label>
                          <input type="text" name="num_card" class="form-control" placeholder="Ex: 4000 1234 5678 9101" value=""/>
                        </div>
                        <div class="col-md-3">
                          <label for="card_expiration" class="control-label required">
                          Expires end
                          </label>
                          <input type="text" name="card_expiration" class="form-control" placeholder=" Ex: 12/21" value=""/>
                        </div>
                      </div>
                    </div>
                    <div class="">
                      <div class="row">
                        <input type="checkbox" name="payment_mode_id[]" value="3" style="width:10%; padding-right:10px" id='mobile_money' onclick="show_payment_box_3();"/> Mobile money
                      </div>
                      <div id="payment_box_3" style="display:none" class="col-md-12">
                        <p style="text-align:center; font-size:14px;">Numéro pour le débit ou le crédit</p>
                        <div class="col-md-12">
                          <label for="num_mobile" class="control-label required">
                          Numéro
                          </label>
                          <input type="text" name="num_mobile" class="form-control" placeholder="Numéro" value=""/>
                        </div>
                      </div>
                    </div>
                    <div class="">
                      <div class="">
                        <input type="checkbox" name="payment_mode_id[]" value="4" style="width:10%; padding-right:10px;" id='cash' onclick="show_payment_box_4();"/> Cash 
                      </div>
                      <div id="payment_box_4" style="display:none" class="col-md-12">
                        <p style="text-align:center; font-size:14px;">Numéro pour vous communiquer les informations des transactions</p>
                        <div class='col-md-12'>
                          <div>
                            <label for="num_cash" class="control-label required">
                            Numéro
                            </label>
                            <input type="text" name="num_cash" class="form-control" placeholder="Numéro" value=""/>
                          </div>
                          <div>
                            <div class="col-md-12">
                              <label for="operator" class="control-label required">
                              Choisir un opérateur de transfert d'argent
                              </label>
                            </div>
                            <div class="col-md-4">
                              <input type="radio" name="operator" class='form-control'  value="1" checked/> <span style="text-align:center">Western Union</span>
                            </div>
                            <div class="col-md-4">
                              <input type="radio" name="operator" class='form-control'  value="2"/> <span style="text-align:center">Moneygram</span>
                            </div>
                            <div class='col-md-4'>
                              <input type="radio" name="operator" class='form-control' value="3"/> <span style="text-align:center">Express Union</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="">
                      <div class="">
                        <input type="checkbox" name="payment_mode_id[]" value="5" style="width:10%" id="bitcoin" onclick="show_payment_box_5();"/> Bitcoin 
                      </div>
                      <div id="payment_box_5" style="display:none" class="col-md-12">
                        <p style="text-align:center; font-size:14px;">Informations sur le portefeuille</p>
                        <div class="col-md-12">
                          <div class='col-md-6'>
                            <label for="bitcoin_address" class="control-label required">
                            Adresse
                            </label>
                            <input type="text" name="bitcoin_address" class="form-control" placeholder="Adresse" value=""/>
                          </div>
                          <div class='col-md-6'>
                            <label for="nickname_bitcoin" class="control-label required">
                            Pseudo
                            </label>
                            <input type="text" name="nickname_bitcoin" class="form-control" placeholder="Pseudo" value=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="">
                      <div class="">
                        <input type="checkbox" name="payment_mode_id[]" value="6" style="width:10%" id="wallet" onclick="show_payment_box_6();"/> Bank wallet 
                      </div>
                      <div id="payment_box_6" style="display:none" class="col-md-12">
                        <p style="text-align:center; font-size:14px;">Numéro du portefeuille mobile</p>
                        <div class="col-md-12">
                          <div class='col-md-12'>
                            <label for="wallet_numero" class="control-label required">
                            Numéro
                            </label>
                            <input type="text" name="wallet_number" class="form-control" placeholder="Numéro" value=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" style="width:10px;"/>
                                <span>
                                    {l s='I agree to the' mod='marketplace'}
                                    
                                        <a href="" class="wk_terms_link">
                                            {l s='terms and condition' mod='marketplace'}
                                        </a>
                                    {l s='and will adhere to them unconditionally.' mod='marketplace'}
                                </span>
                            </label>
                        </div>
                    </div>
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                    <img class="wk_product_loader" src="{$urls.img_url}loader.gif" width="25" />
                      <input type="submit" name="squadRequest" id="squadRequest" class="action-button" value="{if $language.iso_code=='fr'}Enregistrer{/if} {if $language.iso_code=='en'}Save{/if}"/>
                </fieldset>
            </form>
        </div>
      </div>
     </div>
<!-- /.MultiStep Form -->
        
        
  {/block}