{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos.css" />
{/block}
{block name='page_header_container'}{/block}

 {block name='page_content_top'}

 {/block}
  {block name='page_content'}
  <div id="pageshop">
    	<div id="transcroller-body" class="aos-all">

        <div class="main">
          <div class="section landing-section section-white">
            <div class="container">
              <div class="row text-center">
                <div class="col-md-12">
                  <h3 style="font-weight:700">Comment effectuer un retour ?</h3>
                  <br>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                 <div class="info aos-item" data-aos="fade-up">
                    <div class="icon icon-primary">
                      <img src="https://www.afrissime.com/themes/jms_basel/assets/img/retour/effectuer_retour.png" alt="effectuer un retour" width="80px" height="80px">
                    </div>
                    <div class="description">
                      <h4>Effectuer un retour</h4>
                      <p>Retourner, remplacer, échanger ou faire réparer des articles.</p>
                    </div>
                 </div>
                </div>
                <div class="col-md-4">
                   <div class="info aos-item" data-aos="fade-down">
                      <div class="icon icon-warning">
                        <img src="https://www.afrissime.com/themes/jms_basel/assets/img/retour/retour_cadeau.png" alt="Retourner un cadeau" width="80px" height="80px">
                      </div>
                      <div class="description">
                        <h4>Retourner un cadeau</h4>
                        <p>Retourner ou remplacer des articles que vous avez reçu en cadeau.</p>
                      </div>
                   </div>
                </div>
                <div class="col-md-4">
                 <div class="info aos-item" data-aos="zoom-out-down">
                  <div class="icon icon-danger">
                    <img src="https://www.afrissime.com/themes/jms_basel/assets/img/retour/statut.png" alt="Statut de retour produit" width="80px" height="80px">
                  </div>
                  <div class="description">
                    <h4>Voir le statut de vos retours</h4>
                    <p>Imprimer l'étiquette de retour et suivre l'état de vos retours et remboursements</p>
                  </div>
                 </div>
                </div>
              </div>
            </div>
          </div>
          <div class="section landing-section">
            <div class="container">
              <div class="row">
                <div class="md-12 text-center">
                  <h3>Comment procéder ?</h3>
                  <p>
                      <strong>Il est facile de nous retourner un article</strong><br>
                    </p>
                </div>
              </div>
              <div class="row stepper">
                <div class="col-md-3">
                 <div class="info aos-item" data-aos="fade-in">
                    <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png" alt="Next" style="opacity: .1;position: absolute;right: -54px;top:15px">
                    <div class="icon icon-primary">
                      <img src="https://www.afrissime.com/themes/jms_basel/assets/img/retour/impression.png" alt="Impression etiquette" width="80px" height="80px">
                    </div>
                    <div class="description">
                      <p><strong>1. Imprimer l'étiquette et l'autorisation de retour</strong></p>
                     
                    </div>
                 </div>
                </div>
                <div class="col-md-3">
                   <div class="info aos-item" data-aos="fade-in">
                      <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png" alt="Next" style="opacity: .1;position: absolute;right: -40px;top:15px">
                      <div class="icon icon-warning">
                        <!--i class="ti-light-bulb"></i-->
                        <img src="https://www.afrissime.com/themes/jms_basel/assets/img/retour/preparer_colis.png" alt="Préparation du colis" width="80px" height="80px">
                      </div>
                      <div class="description">
                        <p><strong>2.Préparer votre colis en y incluant l'autorisation de retour.</strong></p>
                      </div>
                   </div>
                </div>
                <div class="col-md-3">
                  <div class="info aos-item" data-aos="fade-in">
                    <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png" alt="Next" style="opacity: .1;position: absolute;right: -80px;top:15px">
                    <div class="icon icon-danger">
                      <!--i class="ti-pie-chart"></i-->
                      <img src="https://www.afrissime.com/themes/jms_basel/assets/img/retour/collage_etiquette.png" alt="Collage étiquette" width="80px" height="80px">
                    </div>
                    <div class="description" style="position:relative; left:14px;">
                      <p><strong>3. Coller l'étiquette de retour</strong></p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="info aos-item" data-aos="fade-in">
                    <div class="icon icon-danger">
                      <img src="https://www.afrissime.com/themes/jms_basel/assets/img/retour/expedition.png" alt="Expédition du colis" width="80px" height="80px">
                    </div>
                    <div class="description">
                      <p><strong>4. Expédiez-le</strong></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="section text-center landing-section section-white">
            <div class="container aos-item" data-aos="fade-up">
              <h3>Retours & remboursements</h3>
              <div class="md-12 text-center" style="text-align:justify">
                <p>Avec Afrissime, vous faites vos courses et profitez de notre politique de retour et remboursement en toute confiance.</p>
                <h4>Satisfait ou remboursé</h4>
                <p>
                A compter de la date de livraison vous disposez de 15 jours effectuer le retour de votre produit.
                </p>
                <h4>Retour rapide et facile</h4>
                <p>
                Effectuer un retour d'article en toute simplicité sans <strong>AUCUN </strong> frais supplémentaire
                </p>
                <h4>Authenticité Garantie</h4>
                <p>
                  Nous vous offrons la garantie Afrissime, Nous nous engageons à vous offrir 100% de produits authentiques et originaux. Achetez avec confiance, vous êtes protégés quoi qu'il arrive.
                </p>
                 <h4>Remboursement</h4>
                <p>
                  Vous êtes totalement remboursés si vous recevez un article défectueux ou non conforme.
                </p>
              </div>
                	
            </div>
          </div>
        </div>
      </div>
  </div>
    <script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
  {/block}
