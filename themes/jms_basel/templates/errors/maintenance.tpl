{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='layouts/layout-error.tpl'}
{block name='head_seo' prepend}
  <script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/vendor/bootstrap/js/popper.js"></script>
  <script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/vendor/select2/select2.min.js"></script>

   <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/vendor/bootstrap/css/bootstrap.min.css" />
   <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
   <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/vendor/animate/animate.css" />
   <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/vendor/select2/select2.min.css" />
   <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/util.css" />
   <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/main.css" />
{/block}
{block name='content'}

  <section id="main">

    {block name='page_header_container'}
    {/block}

    {block name='page_content_container'}

        {block name='page_content'}
          <!--  -->
          <div class="simpleslide100">
            <div class="simpleslide100-item bg-img1" style="background-image: url('https://www.afrissime.com/themes/jms_basel/assets/img/bg01.jpg');"></div>
          </div>

          <div class="size1 overlay1">
            <!--  -->
              <div class="size1 flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-50">
                <h3 class="l1-txt1 txt-center p-b-25">
                  <img src="{$shop.logo}" alt="logo">
                </h3>
                <div class="row">
                    <div class="">
                      <iframe width="740" height="350" src="https://www.youtube.com/embed/eqc9g1u9bxA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div id="evermaintenance_block">
                    <p class="m2-txt1 txt-center p-b-48">
                      Restez connectés, nous nous dévoilerons à vous dans : 
                    </p>
                  <div class="flex-w flex-c-m cd100 p-b-33">
                    <div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
                      <span class="l2-txt1 p-b-9 days">35</span>
                      <span class="s2-txt1">Jours</span>
                    </div>

                    <div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
                      <span class="l2-txt1 p-b-9 hours">17</span>
                      <span class="s2-txt1">Heures</span>
                    </div>

                    <div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
                      <span class="l2-txt1 p-b-9 minutes">50</span>
                      <span class="s2-txt1">Minutes</span>
                    </div>

                    <div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
                      <span class="l2-txt1 p-b-9 seconds">39</span>
                      <span class="s2-txt1">Secondes</span>
                    </div>
                  </div>

                  <form class="w-full flex-w flex-c-m validate-form" action="" method="post" id="evermaintenance">
                    <div class="row">
                      <div class="col-md-9">
                        <div class="wrap-input100 validate-input where1" data-validate = "Valid email is required: ex@abc.xyz">
                          <input class="input100 placeholder0 s2-txt2" type="text" name="email" placeholder="Pour plus d'informations, renseignez votre email" required>
                          <input type="hidden" id="ip" name="ip" value="" />
                          <span class="focus-input100"></span>
                        </div>
                        <div id='social_block' style="text-align: center;">
                          <ul class="find-us">

                               <li class="divider"><a href="https://www.facebook.com/afrissime" class="facebook" target="_blank"><span class="fa fa-facebook"></span></a></li>
                               <li class="divider"><a href="https://twitter.com/Afrissime" class="twitter" target="_blank"><span class="fa fa-twitter"></span></a></li>
                               <li class="divider"><a href="https://linkedin.com/company/afrissime" class="linkedin" target="_blank"><span class="fa fa-linkedin"></span></a></li>
                               <li class="divider"><a href="https://www.youtube.com/channel/UCvV8Ta0UA8XhhDK7l2TIrww" class="youtube" target="_blank"><span class="fa fa-youtube"></span></a></li>
                                <li class="divider"><a href="https://www.pinterest.com/afrissime" class="pinterest" target="_blank"><span class="fa fa-pinterest"></span></a></li>            
                                <li class="divider">

                                    <a href="https://www.instagram.com/afrissime" class="instagram" target="_blank">

                                    <span class="fa fa-instagram"></span>

                                    </a>

                                </li>

                                        <li class="divider"><a href="https://wa.me/22892626943" class="whatsapp" target="_blank"><span class="fa fa-whatsapp"></span></a></li>
                                <li class="divider"><a href="https://t.me/Afrissime" class="telegram" target="_blank"><span class="fa fa-telegram"></span></a></li>
                                <li class="divider"><a href="https://www.pscp.tv/afrissime" target="_blank"><span class="periscope"></span></a></li>
                                <li class="divider"><a href="https://soundcloud.com/afrissime" target="_blank"><span class="soundcloud"></span></a></li>
                                <li class="divider"><a href="https://www.flickr.com/photos/184895339@N06" target="_blank"><span class="flickr"></span></a></li>
                            </ul>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <input type="submit" class="flex-c-m size3 s2-txt3 how-btn1 trans-04 where1" value="Inscrivez-vous"/>
                      </div>
                    </div>
                  </form>
                </div>
                <div id="evermaintenance_success" class="col-md-12" style="display:none;">
                  <div class="row">
                      <div class="alert alert-success" style="text-align:center">
                          {l s='Votre adresse email a bien été enregistrée avec succès.Vous serez informés en tant réel de nos actualités.'}
                      </div>
                  </div>
                </div>
            </div>
          </div>
        {/block}
    {/block}

    {block name='page_footer_container'}
      <script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/vendor/countdowntime/moment.min.js"></script>
      <script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/vendor/countdowntime/moment-timezone.min.js"></script>
      <script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/vendor/countdowntime/moment-timezone-with-data.min.js"></script>
      <script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/vendor/countdowntime/countdowntime.js"></script>
      <script>
          $('.cd100').countdown100({
            /*Set Endtime here*/
            /*Endtime must be > current time*/
            endtimeYear: 0,
            endtimeMonth: 0,
            endtimeDate: {$days_remaining},
            endtimeHours: {$hours_remaining},
            endtimeMinutes: {$min_remaining},
            endtimeSeconds: {$sec_remaining},
            timeZone: "" 
            // ex:  timeZone: "America/New_York"
            //go to " http://momentjs.com/timezone/ " to get timezone
          });
        </script>
        <script src="https://www.afrissime.com/themes/jms_basel/assets/vendor/tilt/tilt.jquery.min.js"></script>
        <script >
          $('.js-tilt').tilt({
            scale: 1.1
          })
        </script>
        <script src="https://www.afrissime.com/themes/jms_basel/assets/js/main.js"></script>
        <script type="text/javascript">
          $( document ).ready(function() {
           
              $.getJSON("https://jsonip.com/?callback=?", function (data) {
                  $('#ip').val(data.ip);
              });
              var form = $('#evermaintenance');
                form.submit(function(e) {
                      $.ajax({
                          type: 'POST',
                          url: '../maintenance/newsletter.php',
                          data: $(this).serialize(),
                          dataType: 'json',
                          success: function(jsonData) {
                              if (jsonData == 'success') {
                                  $('#evermaintenance_block').slideUp();
                                  $('#evermaintenance_success').slideDown();
                              };
                          }
                      });
                      e.preventDefault();
            });
          });
          </script>
    {/block}

  </section>

{/block}
