{**
 * 2007-2016 afrissime
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@afrissime.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade afrissime to newer
 * versions in the future. If you wish to customize afrissime for your
 * needs please refer to http://www.afrissime.com for more information.
 *
 * @author    afrissime SA <contact@afrissime.com>
 * @copyright 2007-2016 afrissime SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of afrissime SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="{$urls.base_url}themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="{$urls.base_url}themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="{$urls.base_url}themes/jms_basel/assets/css/aos.css" />
{/block}
{block name='page_header_container'}{/block}

 {block name='page_content_top'}

 {/block}
  {block name='page_content'}
  <div id="pageshop">
    	<div id="transcroller-body" class="aos-all">

        <div class="main">
          <div class="section landing-section section-white">
            <div class="container">
              <div class="row text-center">
                <div class="col-md-12">
                  <h3 style="font-weight:700">{l s='Comment devenir un commercial consultant?' d='Shop.Theme.Global'} </h3>
                  <br>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                 <div class="info aos-item" data-aos="fade-up">
                    <div class="icon icon-primary">
                      <img src="{$urls.base_url}themes/jms_basel/assets/img/create_account.png" alt="{l s='Créer un compte Afrissime Squad' d='Shop.Theme.Global'}" width="80px" height="80px">
                    </div>
                    <div class="description">
                      <h4>1. {l s='Créer un compte Afrissime Squad' d='Shop.Theme.Global'}</h4>
                      <p>Inscrivez vous en vous cliquant sur le lien <a href="#">DEVENIR CONSULTANT INDEPENDANT"</a>, rentrez vos coordonnées avec une adresse mail valide et définissez votre mot de passe, un compte client AFRISSIME et un compte AFRISSIME Squad vous sont créés automatiquement.</p>
                    </div>
                 </div>
                </div>
                <div class="col-md-4">
                   <div class="info aos-item" data-aos="fade-down">
                      <div class="icon icon-warning">
                        <img src="{$urls.base_url}themes/jms_basel/assets/img/buy.png" alt="{l s='Effectuer des achats pour des tiers' d='Shop.Theme.Global'}" width="80px" height="80px">
                      </div>
                      <div class="description">
                        <h4>2. {l s='Effectuer des achats pour des tiers' d='Shop.Theme.Global'}</h4>
                        <p>Proposez les produits AFRISSIME  autour de vous.Passez les commandes de vos clients, en utilisant votre compte AFRISSIME.</p>
                      </div>
                   </div>
                </div>
                <div class="col-md-4">
                 <div class="info aos-item" data-aos="zoom-out-down">
                  <div class="icon icon-danger">
                    <img src="{$urls.base_url}themes/jms_basel/assets/img/earn_cash.png" alt="{l s='Gagner de l\'argent' d='Shop.Theme.Global'}" width="80px" height="80px">
                  </div>
                  <div class="description">
                    <h4>3. {l s='Gagner de l\'argent' d='Shop.Theme.Global'}</h4>
                    <p>Touchez des commissions sur vos ventes. Plus vous vendez, plus vous gagnez.</p>
                  </div>
                 </div>
                </div>
              </div>
              <div class="row text-center">
                  <div class="col-md-12" style="margin-bottom:30px">
                    <a href="{$urls.base_url}fr/devenir-commercial" class="btn  btn-effect">{l s='Devenir commercial consultant' d='Shop.Theme.Global'}</a>
                  </div>              
              </div>
            </div>
          </div>

          <div class="section landing-section">
            <div class="container">
              <div class="row">
                <div class="md-12 text-center">
                  <h3 style="font-weight:700">{l s='Avantages de rejoindre Afrissime Squad' d='Shop.Theme.Global'}</h3>
                </div>
              </div>
                <div class="row">
                  <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
                    <div class="col-md-3">
                      <img src="{$urls.base_url}themes/jms_basel/assets/img/earn_cash2.png" alt="{l s='Vous gagnez de l\'argent' d='Shop.Theme.Global'}" width="200px">
                    </div>
                    <div class="col-md-9">
                        <h4 style="font-weight:600">{l s='Vous gagnez de l\'argent' d='Shop.Theme.Global'}</h4>
                        <p>
                          Vous faites des commissions en vendant des articles fournis par Afrissime.
Vos efforts sont récompensés : plus vous travaillez, plus vous gagnez !
40% de nos vendeurs gagnent plus de 200 000 Fcfa par mois
Gagnez de l’argent depuis la maison

                        </p>
                    </div>
                  </div>
               </div>
                <div class="row">
                  <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
                    <div class="col-md-9">
                        <h4 style="font-weight:600">Vous êtes votre propre patron</h4>
                        <p>
                           En tant que commercial consultant indépendant, vous disposez d'une totale liberté et contrôle sur votre activité.
Construisez votre propre entreprise prospère. Le ciel est la limite !
Vous formez également votre propre équipe de conseillers commerciaux !

                        </p>
                    </div>
                    <div class="col-md-3">
                      <img src="{$urls.base_url}themes/jms_basel/assets/img/boss.png" alt="paiement" width="200px">
                    </div>
                  </div>
               </div>
               <div class="row">
                    <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
                      <div class="col-md-3">
                        <img src="{$urls.base_url}themes/jms_basel/assets/img/fun.png" width="200px" alt="Livraison">
                      </div>
                      <div class="col-md-9">
                          <h4 style="font-weight:600">Vous vous amusez</h4>
                          <p>
                              Vendez et soyez récompensés pour cela. Gagner de nouvelles compétences et développer votre confiance en soi.Rencontrer de nouvelles personnes & faire de nouveaux amis.Assistez à nos événements sociaux (récompenses, petits déjeuners, sorties, conférence de presse ...)

                          </p>
                      </div>
                      
                    </div>
               </div>
               <div class="row">
                    <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
                      <div class="col-md-9">
                          <h4 style="font-weight:600">Vous êtes formé</h4>
                          <p>
                              Nous vous fournissons d'excellentes connaissances et compétences.Nous nous assurons que vous devenez un homme d'affaires et une femme d'affaires pleinement autonomes.


                          </p>
                      </div>
                      <div class="col-md-3">
                        <img src="{$urls.base_url}themes/jms_basel/assets/img/formation.png" width="200px" alt="Livraison">
                      </div>
                    </div>
               </div>
               <div class="row">
                    <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
                      <div class="col-md-3">
                        <img src="{$urls.base_url}themes/jms_basel/assets/img/delivery.png" width="200px" alt="Livraison">
                      </div>
                      <div class="col-md-9">
                          <h4 style="font-weight:600">Accédez aux promotions quotidiennes.</h4>
                          <p>
                              Gagnez de l'argent avec des commissions et amusez-vous à le faire ! Obtenez un accès gratuit à toutes nos formations et obtenez des connaissances sur votre travail !<br/>
                              Vous n'avez pas besoin d'une expérience préalable, seulement une forte motivation et d’enthousiasme !
                          </p>
                      </div>
                      
                    </div>
               </div>
               <div class="row">
                    <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
                      <div class="col-md-12">
                          <p style="text-align:center">
                              Afrissime vous forme, livre aux clients, collecte, reverse l'argent, et dispose d’un service client et vendeurs.<br/>Avec Afrissime, le paradis est sur terre !

                          </p>
                      </div>
                      <div class="row text-center">
                          <div class="col-md-12" style="margin-bottom:30px">
                            <a href="{$urls.base_url}fr/devenir-commercial" class="btn  btn-effect">{l s='Devenir commercial consultant' d='Shop.Theme.Global'}</a>
                          </div>              
                      </div>
                    </div>
               </div>
              </div>
            </div>
               
            </div>
          </div>
      </div>
  </div>
    <script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
  {/block}
