{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_title'}
  {l s='Afrissime squad rewards' d='Shop.Theme.Global'}
{/block}

{block name='page_content'}
  <h6>{l s='Here are the orders you\'ve placed since your account was created.' d='Shop.Theme.Global'}</h6>
    <div id="rewards_account" class="rewards">
      <ul class="idTabs">
        <li class="col-xs-12 col-sm-4"><a href="#idTab1" {if $activeTab!='history'}class="selected"{/if}>{l s='My account' d='Shop.Theme.Global'}</a></li>
        <li class="col-xs-12 col-sm-4"><a href="#idTab2" {if $activeTab=='history'}class="selected"{/if}>{l s='Rewards history' d='Shop.Theme.Global'}</a></li>
        <li class="col-xs-12 col-sm-4"><a href="#idTab3">{l s='Vouchers history' d='Shop.Theme.Global'}</a></li>
      </ul>
    </div>
    <div class="sheets table-responsive">
      <div id="idTab1" class="rewardsBlock">
        {if $return_days > 0}
        <p>{l s='Rewards will be available %s days after the validation of each order.'  sprintf=[$return_days|intval] d='Shop.Theme.Global'}</p>
          {/if}
      </div>
    </div>
{/block}
