{**
 * 2007-2016 afrissime
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@afrissime.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade afrissime to newer
 * versions in the future. If you wish to customize afrissime for your
 * needs please refer to http://www.afrissime.com for more information.
 *
 * @author    afrissime SA <contact@afrissime.com>
 * @copyright 2007-2016 afrissime SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of afrissime SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="{$urls.base_url}themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="{$urls.base_url}themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="{$urls.base_url}themes/jms_basel/assets/css/aos.css" />
{/block}
{block name='page_content'}
	<div id="pageshop">
      <div id="transcroller-body" class="aos-all">
      <div class="landing-header" style="background-image: url('{$urls.base_url}themes/jms_basel/assets/img/cover_pageshop.jpg');height:600px">
        <div class="container">
          <div class="intro_shop">
            {if $language.iso_code == 'fr'}
            <h2>Vivez de votre savoir faire</h2>
            <h3>Vendez simplement, maintenant et sans engagement.</h3>
            {/if}
            {if $language.iso_code == 'en'}
            <h2>Live your expertise</h2>
            <h3>Sell ​​simply, now and without commitment.</h3>
            {/if}
            <br>
            	{if $language.iso_code == 'fr'}
              		<a href="{$urls.base_url}demande-du-vendeur" class="btn-default btn-effect">J'ouvre ma boutique gratuitement</a>
              	{else}
              		<a href="{$urls.base_url}seller-request" class="btn-default btn-effect">I create my shop</a>
              	{/if}
          </div>
        </div>
      </div>

    <div class="main">
      <div class="section landing-section section-white">
    <div class="container">
      <div class="row text-center">
        <div class="col-md-12">
          <h3 style="font-weight:700">{if $language.iso_code == 'fr'}Avantages de la vente sur Afrissime{else}Benefits of the sale on Afrissime{/if}</h3>
          <br>
          {if $language.iso_code == 'fr'}
          <p>
          Notre offre évolue constamment mais certaines choses ne changeront jamais : L’inscription sur notre plateforme est gratuite et le restera toujours.
          </p>
          {/if}
          {if $language.iso_code == 'en'}
          <p>
          Our offer is constantly evolving but some things will never change: Registration on our platform is free and will remain so forever.
          </p>
          {/if}
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-up">
            <div class="icon icon-primary">
              <img src="{$urls.base_url}themes/jms_basel/assets/img/visibilite.png" alt="Visibilité" width="80px" height="80px">
            </div>
            <div class="description">
              {if $language.iso_code == 'fr'}<h4> Visibilité</h4>

              <p style="text-align:justify">Lorsque vous vendez en ligne sur Afrissime, vos produits sont plus faciles à trouver et plus faciles à acheter. Ouvrir votre boutique sur Afrissime, vous permet en tant que vendeur de toucher des millions d’acheteurs à travers le monde que vous ayez un article à vendre ou des millions.</p>
              {/if}
              {if $language.iso_code == 'en'}<h4> Visibility</h4>
              <p style="text-align:justify">When you sell online on Afrissime, your products are easier to find and easier to buy. Opening your shop on Afrissime, allows you as a salesman to reach millions of buyers around the world whether you have an item for sale or millions.</p>
              {/if}
            </div>
         </div>
        </div>
        <div class="col-md-4">
           <div class="info aos-item" data-aos="fade-up">
              <div class="icon icon-warning">
                <img src="{$urls.base_url}themes/jms_basel/assets/img/croissance.png" alt="Croissance" width="80px" height="80px">
              </div>
              <div class="description">
                {if $language.iso_code == 'fr'}
                <h4>Croissance</h4>
                <p style="text-align:justify">Vendre sur Afrissime vous permet de développer votre activité. Afrissime vous aide à gérer les tâches fastidieuses associées à votre activité. Profitez d’autres services spécialement conçus pour vous aider à expédier les commandes, stocker les produits, gérer les annonces, etc.</p>
                {/if}
                {if $language.iso_code == 'en'}
                <h4>Growth</h4>
                <p style="text-align:justify">Selling on Afrissime allows you to grow your business. Afrissime helps you manage the tedious tasks associated with your business. Take advantage of other services specifically designed to help you ship orders, store products, manage ads, and more.</p>
                {/if}
              </div>
           </div>
        </div>
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-up">
          <div class="icon icon-danger">
            <img src="{$urls.base_url}themes/jms_basel/assets/img/accompagnement.png" alt="Accompagnement" width="80px" height="80px">
          </div>
          <div class="description">
          {if $language.iso_code == 'fr'}
            <h4>Accompagnement</h4>
            <input type="checkbox" class="read-more-state" id="post-1" />
            <p class="read-more-wrap" style="text-align:justify">En vendant sur Afrissime, vous bénéficiez de prestations offertes par Afrissime à l’instar de shooting photo de vos produits avec ou sans mannequins, une réalisation de spot publicitaire vidéo, des séances de formation et de renforcement de capacité, une publicité et une prospection <span class="read-more-target">auprès de particuliers et professionnels, conceptions d’affiches et autre support à volonté, un service de traduction pour tous vos produits, pour vos acheteurs un service client disponible 24h/24 dans la langue locale.</span></p>
            {/if}
            {if $language.iso_code == 'en'}
            <h4>Guidance</h4>
            <input type="checkbox" class="read-more-state" id="post-1" />
            <p class="read-more-wrap" style="text-align:justify">By selling on Afrissime, you benefit from services offered by Afrissime like photo shooting of your products with or without mannequins, a realization of video advertising, training sessions and capacity building, advertising and prospecting.</p>
            {/if}
          </div>
         </div>
        </div>
      </div>
        <div class="row">
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-up">
            <div class="icon icon-primary">
              <img src="{$urls.base_url}themes/jms_basel/assets/img/logistique.png" alt="Logistique" width="80px" height="80px">
            </div>
            <div class="description">
            {if $language.iso_code == 'fr'}
              <h4>Logistique</h4>
              <p style="text-align:justify">Afrissime s’occupe de l’expédition de vos produits au client où qu’il se trouve dans le monde.</p>
            {/if}
            {if $language.iso_code == 'en'}
              <h4>Logistics</h4>
              <p style="text-align:justify">Afrissime takes care of shipping your products to the customer wherever he is in the world.</p>
            {/if}
            </div>
         </div>
        </div>
        <div class="col-md-4">
           <div class="info aos-item" data-aos="fade-up">
              <div class="icon icon-warning">
                <img src="{$urls.base_url}themes/jms_basel/assets/img/performance.png" alt="Outils performants" width="80px" height="80px">
              </div>
              <div class="description">
                {if $language.iso_code == 'fr'}
                <h4>Outils performants</h4>
                <input type="checkbox" class="read-more-state" id="post-2" />
                <p class="read-more-wrap" style="text-align:justify">En tant que vendeur, vous découvrirez qu’Afrissime propose des outils et fonctionnalités uniques capables de vous aider à <span class="read-more-target">développer votre entreprise. Ils sont gratuits et peuvent être utilisés pour gérer en souplesse votre activité. Bénéficiez notamment de rapports de commandes et d’informations relatives à vos commandes.</span></p>
                {/if}
                {if $language.iso_code == 'en'}
                <h4>High-performance tools</h4>
                <input type="checkbox" class="read-more-state" id="post-2" />
                <p class="read-more-wrap" style="text-align:justify">As a salesman, you will discover that Afrifime offers unique tools and features that can help you <span class="read-more-target">grow your business. They are free and can be used to flexibly manage your business. In particular, you benefit from order reports and information relating to your orders.</span></p>
                {/if}
                <label for="post-2" class="read-more-trigger"></label>
              </div>
           </div>
        </div>
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-up">
          <div class="icon icon-danger">
            <img src="{$urls.base_url}themes/jms_basel/assets/img/sans_engagement.png" alt="sans engagement" width="80px" height="80px">
          </div>
          <div class="description">
            {if $language.iso_code == 'fr'}
            <h4>Sans engagment</h4>
            <p style="text-align:justify">En tant que vendeur sur Afrissime, vous avez l'assurance de pouvoir fermer votre boutique à tout moment.</p>
            {/if}
            {if $language.iso_code == 'en'}
            <h4>Without engagement</h4>
            <p style="text-align:justify">As a seller on Afrissime, you can rest assured that you can close your shop at any time.</p>
            {/if}
          </div>
         </div>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-12">
        		{if $language.iso_code == 'fr'}
            <a href="{$urls.base_url}demande-du-vendeur" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
          {else}
            <a href="{$urls.base_url}seller-request" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">I create my shop</a>
          {/if}
        </div>
      </div>
    </div>
  </div>

      <div class="section landing-section">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          {if $language.iso_code == 'fr'}
          <h3 style="font-weight:700">Vendez simplement en quelques étapes</h3>
          <p class="text-center">Accédez à des millions de clients et mettez l’expertise de la vente en ligne et les technologies de recherche et de paiement d’Afrissime au service de votre entreprise. Vendre sur Afrissime est un processus simple en 5 étapes.</p>
            <h4 style="font-weight:600">Avant de vous inscrire</h4>
          {/if}
          {if $language.iso_code == 'en'}
          <h3 style="font-weight:700">Just sell in a few steps.</h3>
          <p class="text-center">Access millions of customers and put Afrissime's online sales expertise and search and payment technologies to the service of your business. Selling on Afrissime is a simple process in 5 steps.</p>
            <h4 style="font-weight:600">Before you register</h4>
          {/if}
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-6">
         <div class="info aos-item" data-aos="fade-in">
            <div>
              <div class="icon icon-primary">
                <img src="{$urls.base_url}themes/jms_basel/assets/img/rech.png" alt="Recherche produit" width="80px" height="80px">
              </div>
              <div class="description">
                {if $language.iso_code == 'fr'}
                <p><strong>1. Décidez de ce que vous voulez vendre</strong></p>
                <p class="text-center">Plus de 15 catégories sont ouvertes à tous les vendeurs. </p>
                {/if}
                {if $language.iso_code == 'en'}
                <p><strong>1. Decide what you want to sell</strong></p>
                <p class="text-center">More than 15 categories are open to all sellers. </p>
                {/if}
              </div>
            </div>
            
         </div>
         <div style="float:right;">
                <img class="next hidden-sm hidden-xs" src="{$urls.base_url}themes/jms_basel/assets/img/next.png"  alt="Next" style="top: 75px;">
            </div>
        </div>
        <div class="col-md-6">
           <div class="info aos-item" data-aos="fade-in">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="{$urls.base_url}themes/jms_basel/assets/img/ins.png" alt="Inscription" width="80px" height="80px">
              </div>
              <div class="description">
                {if $language.iso_code == 'fr'}
                <p><strong>2. S’inscrire et commencer à répertorier des produits</strong></p>
                <p class="text-center">Créez votre compte sur Afrissime, et accéder à l’interface web sur laquelle vous pourrez gérer votre compte vendeur.</p>
                {/if}
                {if $language.iso_code == 'en'}
                <p><strong>2. Register and start listing products</strong></p>
                <p class="text-center">Create your account on Afrissime, and access the web interface on which you can manage your seller account.</p>
                {/if}
              </div>
           </div>
        </div>
      </div>
      <div class="row">
        <div class="md-12 text-center">
          {if $language.iso_code == 'fr'}
            <h4 style="font-weight:600">Une fois inscrit</h4>
          {/if}
          {if $language.iso_code == 'en'}
            <h4 style="font-weight:600">Once registered</h4>
          {/if}
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-3">
         <div class="info aos-item" data-aos="zoom-out">
            <img class="next hidden-sm hidden-xs" src="{$urls.base_url}themes/jms_basel/assets/img/next.png" alt="Next" style="right:-40px; top:0px">
            <div class="icon icon-primary">
              <img src="{$urls.base_url}themes/jms_basel/assets/img/loupe.png"  alt="Répertorier les articles sur Afrissime" width="80px" height="80px">
            </div>
            <div class="description">
              {if $language.iso_code == 'fr'}
              <p><strong>1. Répertorier</strong></p>
              <p class="text-center">Vous pouvez ajouter des produits au catalogue produits Afrissime Marketplace autant que vous le désirez.</p>
              {/if}
              {if $language.iso_code == 'en'}
              <p><strong>1. List</strong></p>
              <p class="text-center">You can add products to the Afrissime Marketplace product catalog as much as you like.</p>
              {/if}
            </div>
         </div>
        </div>
        <div class="col-md-3">
           <div class="info aos-item" data-aos="zoom-out">
              <img class="next hidden-sm hidden-xs" src="{$urls.base_url}themes/jms_basel/assets/img/next.png" alt="Next" style="right:-40px; top:0px">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="{$urls.base_url}themes/jms_basel/assets/img/vente.png" width="80px" height="80px" alt="Vendre sur Afrissime">
              </div>
              <div class="description">
                {if $language.iso_code == 'fr'}
                <p><strong>2. Vendre</strong></p>
                <p class="text-center">Vous disposez de différentes possibilités pour gérer vos commandes, en fonction du nombre de commandes que vous recevez :</p>
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <ul class="list-unstyled">
                      <li>&nbsp;</li>
                      <li style="text-align:center"><strong>•  Outil de gestion des commandes: </strong> </br>visualisez une liste de vos commandes et les détails concernant une commande sélectionnée.</li>
                      <li style="text-align:center"><strong>•  Rapports de commande: </strong> </br>recevez des informations sur le traitement de plusieurs commandes en exécutant un seul rapport.</li>
                    </ul>
                  </div>
                </div>
                {/if}
                {if $language.iso_code == 'en'}
                <p><strong>2. Sell</strong></p>
                <p class="text-center">You have different options to manage your orders, depending on the number of orders you receive:</p>
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <ul class="list-unstyled">
                      <li>&nbsp;</li>
                      <li style="text-align:center"><strong>• Order Management Tool: </strong> </br>view a list of your orders and details about a selected order.</li>
                      <li style="text-align:center"><strong>• Order reports: </strong> </br>receive information about processing multiple orders by running a single report.</li>
                    </ul>
                  </div>
                </div>
                {/if}
              </div>
           </div>
        </div>
         <div class="col-md-3">
         <div class="info aos-item" data-aos="zoom-out">
            <img class="next hidden-sm hidden-xs" src="{$urls.base_url}themes/jms_basel/assets/img/next.png" alt="Next" style="right:-40px; top:0px">
            <div class="icon icon-primary">
              <img src="{$urls.base_url}themes/jms_basel/assets/img/expedition.png" width="80px" height="80px" alt="Expédition">
            </div>
            <div class="description">
              {if $language.iso_code == 'fr'}
              <p><strong>3. Expédition</strong></p>
              <p class="text-center">Afrissime vous alerte quand un client passe une commande. Dès lors vous disposez d’un délai de 48h pour traiter (emballage et étiquetage) et expédier le produit.</p>
              {/if}
              {if $language.iso_code == 'en'}
              <p><strong>3. Expedition</strong></p>
              <p class="text-center">Afrissime alerts you when a customer places an order. Therefore you have 48 hours to process (packaging and labeling) and ship the product.</p>
              {/if}
            </div>
         </div>
        </div>
         <div class="col-md-3">
         <div class="info aos-item" data-aos="zoom-out">
            <div class="icon icon-primary">
              <img src="{$urls.base_url}themes/jms_basel/assets/img/encaissement.png" width="80px" height="80px" alt="Encaissement">
            </div>
            <div class="description">
              {if $language.iso_code == 'fr'}
              <p><strong>4. Encaissement</strong></p>
              <p class="text-center">Afrissime vire régulièrement les paiements sur votre compte bancaire (ou tout autre moyen de paiement choisi) et vous informe que le règlement a bien été effectué. Afrissime accepte les paiements dans de nombreuses monnaies et vous paie dans la monnaie de votre pays, afin de simplifier le traitement du paiement.</p>
              {/if}
              {if $language.iso_code == 'en'}
                <p><strong>4. Cashing</strong></p>
                <p class="text-center">Afrissime frankly transfers payments to your bank account (or any other means of payment chosen) and informs you that the payment has been made. Afribusiness payments in many currencies and countries in the currency of your country, to simplify the payment process.</p>
              {/if}
            </div>
         </div>
        </div>
      </div>
       <div class="row text-center">
        <div class="col-md-12">
            {if $language.iso_code == 'fr'}
            <a href="{$urls.base_url}demande-du-vendeur" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
          {else}
            <a href="{$urls.base_url}seller-request" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">I create my shop</a>
          {/if}
        </div>
      </div>
    </div>
  </div>

  <div class="section landing-section section-white">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          {if $language.iso_code == 'fr'}
          <h3 style="font-weight:700">Avec Afrissime, vendre dans le monde est à la fois simple et facile</h3>
          {/if}
          {if $language.iso_code == 'en'}
          <h3 style="font-weight:700">With Afrissime, selling in the world is both simple and easy
</h3>
          {/if}
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-12 text-center">
          <div class="description">
            <p class="text-center">
            {if $language.iso_code == 'fr'}
              Comprendre les codes TVA et les réglementations sur les produits, créer des listings produits efficaces et fournir un service client localisé peut s'avérer complexe lorsque vous développez vos activités au-delà des frontières. Tirez parti de notre logistique, de nos outils performants et de notre service après-vente afin de jouir de notre base de clients en constante croissance.
              {/if}
              {if $language.iso_code == 'en'}
                Understanding VAT codes and product regulations, creating effective product listings, and providing localized customer service can be complex when you grow your business across borders. Leverage our logistics, powerful tools and after-sales service to enjoy our ever-growing customer base.
              {/if}
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
          <div class="col-md-3">
            <img src="{$urls.base_url}themes/jms_basel/assets/img/compte_vendeur.png" width="200px" height="200px" alt="un seul compte vendeur">
          </div>
          <div class="col-md-9">
              {if $language.iso_code == 'fr'}
              <h4 style="font-weight:600">Un seul compte vendeur pour le monde entier</h4>
              <p>
                  Avec un seul compte, vous toucherez des clients dans les 200 pays du monde, et vous serez en mesure de gérer toutes vos offres ainsi que vos stocks depuis votre interface vendeur.
              </p>
              {/if}
              {if $language.iso_code == 'en'}
              <h4 style="font-weight:600">One seller account for the whole world</h4>
              <p>
                 With just one account, you'll reach customers in 200 countries around the world, and you'll be able to manage all your offers and inventory from your sales interface.
              </p>
              {/if}
          </div>
        </div>
      </div>
      <div class="row">
        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
          <div class="col-md-9">
            {if $language.iso_code == 'fr'}
              <h4 style="font-weight:600">Développez vos ventes grâce à de nouvelles opportunités</h4>
              <p>
                  Avec plus de 15 catégories de produits disponibles, vous pourrez vendre de nombreux produits sur Afrissime.
              </p>
              {/if}
              {if $language.iso_code == 'en'}
              <h4 style="font-weight:600">Grow your sales with new opportunities</h4>
              <p>
                  With more than 15 categories of products available, you will be able to sell many products on Afrissime.
              </p>
              {/if}
          </div>
          <div class="col-md-3">
            <img src="{$urls.base_url}themes/jms_basel/assets/img/opportunites.png" width="200px" height="200px" alt="Opportunités">
          </div>
        </div>
      </div>
       <div class="row">
        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
          <div class="col-md-3">
              <img src="{$urls.base_url}themes/jms_basel/assets/img/taxes.png" width="200px" height="200px" alt="Taxes et paiements">
          </div>
          <div class="col-md-9">
            {if $language.iso_code == 'fr'}
              <h4 style="font-weight:600">Gérez les taxes et les paiements</h4>
              <p>
                  Si vous vendez des produits dans un pays de l’UE, vous devrez probablement demander un numéro de taxe sur la valeur ajoutée (TVA). La TVA est une taxe européenne sur les dépenses de consommation. Afrissime accepte les paiements dans de nombreuses monnaies et vous paie dans la monnaie de votre pays, afin de simplifier le traitement du paiement.  
              </p>
            {/if}
            {if $language.iso_code == 'en'}
              <h4 style="font-weight:600">Manage taxes and payments</h4>
              <p>
                  If you sell products in an EU country, you will probably need to apply for a Value Added Tax (VAT) number. VAT is a European tax on consumer spending. Afrissime accepts payments in many currencies and pays you in the currency of your country, to simplify the payment process. 
              </p>
            {/if}
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-12">
              {if $language.iso_code == 'fr'}
              <a href="{$urls.base_url}demande-du-vendeur" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
            {else}
              <a href="{$urls.base_url}seller-request" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">I create my shop</a>
            {/if}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section landing-section section-white">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          {if $language.iso_code == 'fr'}
          <h3 style="font-weight:700">Questions - Réponses</h3>
          {/if}
          {if $language.iso_code == 'en'}
          <h3 style="font-weight:700">Questions answers</h3>
          {/if}
        </div>
      </div>
      <div id="accordion" aos-item" data-aos="fade-down">
      {if $language.iso_code == 'fr'}
      <p class="accordion-toggle minus-cercle">Qu'est-ce que Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p class="accordion-toggle minus-cercle">What is Afrissime?</p>
      {/if}
      <div class="accordion-content default">
      {if $language.iso_code == 'fr'}
      <p>Afrissime vient de la combinaison de Afrique et Assime (marché en langue mina). Afrissime est une boutique en ligne de vente de tout produit fait par des africains du continent ou de la diaspora.</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p>Afrissime comes from the combination of Africa and Assime (market in Mina language). Afrissime is an online shop selling any product made by African continent or diaspora.</p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
      <p class="accordion-toggle">Quels produits puis-je vendre sur Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p class="accordion-toggle">What products can I sell on Afrissime?</p>
      {/if}
      <div class="accordion-content">
        {if $language.iso_code == 'fr'}
          <p>Vous pouvez vendre tout type de produits à condition que le produit réponde aux critères suivants :</p>
          <ul class="list-unstyled"> 
            <li>-Avoir été fait par un africain</li> 
            <li>-Être un produit de qualité</li> 
          </ul>
        {/if}
        {if $language.iso_code == 'en'}
          <p>You can sell any type of product provided that the product meets the following criteria:</p>
          <ul class="list-unstyled"> 
            <li>-Have been done by an African</li> 
            <li>-Being a quality product</li> 
          </ul>
        {/if}
      </div>
      {if $language.iso_code == 'fr'}
        <p class="accordion-toggle">Quels types de produits ne puis-je pas vendre sur Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
        <p class="accordion-toggle">What types of products can not I sell on Afrissime?</p>
      {/if}
      <div class="accordion-content">
        {if $language.iso_code == 'fr'}
          <p>Vous ne pouvez pas vendre de produits illégaux, fabriqués de façon illégale ou qui sont prohibés par les politiques d’Afrissime. Cela inclut les médicaments sur ordonnance, les drogues illicites, les armes à feu, les munitions, les produits interdits de vente par le commerce international, etc.</p>
        {/if}
        {if $language.iso_code == 'en'}
          <p>You can not sell illegal products that are illegally manufactured or that are prohibited by Afrissime policies. This includes prescription drugs, illegal drugs, firearms, ammunition, prohibited goods for sale by international trade, etc.</p>
        {/if}
      </div>
      {if $language.iso_code == 'fr'}
      <p class="accordion-toggle">Quelles informations dois-je fournir pour m'inscrire comme vendeur sur Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p class="accordion-toggle">What information must I provide to register as a seller on Afrissime?</p>
      {/if}
      <div class="accordion-content">
      {if $language.iso_code == 'fr'}
      <p>Toutes les informations à fournir sont renseignées dans le formulaire de création de boutique disponible ici.</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p>All information to be provided is filled in the form of shop creation available here.</p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
      <p class="accordion-toggle">Comment mettre mes produits en vente sur Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p class="accordion-toggle">How to put my products on sale on Afrissime?</p>
      {/if}
      <div class="accordion-content">
      {if $language.iso_code == 'fr'}
      <p>Vous pouvez utiliser notre interface Web ou un outil de téléchargement des offres. La procédure et les informations requises varient selon vos produits.</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p>You can use our web interface or a tool for downloading offers. The procedure and information required vary depending on your products.</p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
      <p class="accordion-toggle">Combien coûte la vente sur Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p class="accordion-toggle">How much does it cost to sell on Afrissime?</p>
      {/if}
      <div class="accordion-content">
      {if $language.iso_code == 'fr'}
      <p>Vendre sur Afrissime est gratuit. Nous prenons seulement 15% de commission sur chaque vente effectuée.</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p>Selling on Afrissime is free. We only take 15% commission on each sale made.</p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
      <p class="accordion-toggle">Comment gérer les commandes ?</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p class="accordion-toggle">How to manage orders?</p>
      {/if}
      <div class="accordion-content">
      {if $language.iso_code == 'fr'}
      <p>Vous avez deux options pour gérer vos commandes.Vous pouvez utiliser l'interface Web. Vous pouvez télécharger le rapport de commande quotidien, fichier tabulé qui fournit un résumé des commandes reçues et comprend les informations des clients et des expéditions pour que vous puissiez traiter ces commandes.
      </p>
      {/if}
      {if $language.iso_code == 'en'}
      <p>You have two options for managing your orders. You can use the web interface. You can download the daily order report, tabular file that provides a summary of orders received and includes customer information and shipments for you to process these orders.
      </p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
      <p class="accordion-toggle">Comment suis-je payé ?</p>
      {/if}
      {if $language.iso_code == 'en'}
      <p class="accordion-toggle">How do I get paid?</p>
      {/if}
      <div class="accordion-content">
      {if $language.iso_code == 'fr'}
      <p>Afrissime transfère le solde qui vous est dû sur votre compte bancaire (ou tout autre canal de paiement choisi) 7 jours après la réception du produit par le client. Les frais de retours, les réclamations clients ainsi que les remboursements affectent votre solde.
      </p>
      {/if}
      {if $language.iso_code == 'en'}
        <p>Afrissime transfers the balance due to you to your bank account (or any other chosen payment channel) 7 days after receipt of the product by the customer. Return charges, customer complaints and refunds affect your balance.
      </p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
        <p class="accordion-toggle">Proposez-vous la protection contre la fraude ?</p>
      {/if}
      {if $language.iso_code == 'en'}
        <p class="accordion-toggle">Do you offer protection against fraud?</p>
      {/if}
      <div class="accordion-content">
        {if $language.iso_code == 'fr'}
          <p>Oui. La protection contre la fraude de paiement d'Afrissime vous aide à éliminer les commandes frauduleuses passées sur vos produits.
          </p>
        {/if}
        {if $language.iso_code == 'en'}
          <p>Yes. Afrissime Payment Fraud Protection helps you eliminate fraudulent orders placed on your products.
          </p>
        {/if}
      </div>
      {if $language.iso_code == 'fr'}
        <p class="accordion-toggle">Qu'est-ce que la garantie Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
        <p class="accordion-toggle">What is the Afrissime guarantee?</p>
      {/if}
      <div class="accordion-content">
      {if $language.iso_code == 'fr'}
        <p>Le programme de garantie Afrissime est conçu pour gérer des situations dans lesquelles un client n'a jamais reçu un produit ou a reçu un produit qui est substantiellement différent de celui commandé ou attendu. Nous demandons aux clients de commencer par contacter le vendeur en cas de problème. Si le vendeur n'est pas en mesure de résoudre le problème, le client peut exercer un recours en garantie. À la réception de la réclamation, Afrissime envoie au vendeur un message automatique détaillant la réclamation et sollicitant des informations sur la commande et le processus de traitement. Afrissime détermine alors le règlement du recours en garantie, qui peut inclure un remboursement de la commande au client, aux frais du vendeur.
      </p>
      {/if}
      {if $language.iso_code == 'en'}
        <p>The Afrissime Guarantee Program is designed to handle situations in which a customer has never received a product or received a product that is substantially different from that ordered or expected. We ask customers to start by contacting the seller in case of problems. If the seller is not able to solve the problem, the customer can exercise a warranty claim. Upon receipt of the claim, Afrissime sends the seller an automatic message detailing the claim and requesting information about the order and the processing process. Afrissime then determines the settlement of the warranty claim, which may include a refund of the order to the customer, at the expense of the seller.
      </p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
        <p class="accordion-toggle">Les clients peuvent-ils laisser des évaluations ?</p>
      {/if}
      {if $language.iso_code == 'en'}
        <p class="accordion-toggle">Can customers leave ratings?</p>
      {/if}
      <div class="accordion-content">
      {if $language.iso_code == 'fr'}
        <p>Oui, les clients peuvent laisser des commentaires. Un pourcentage élevé d'avis positifs est un facteur essentiel de réussite sur Afrissime. C'est le meilleur moyen pour les acheteurs de savoir si vous êtes digne de leur confiance. Votre pourcentage d'avis positifs s'affiche à la page des offres et est l'une des premières informations visibles. Les clients sont plus enclins à acheter des produits de vendeurs ayant un pourcentage élevé d'avis positifs. Votre pourcentage d'avis positifs est l'une des statistiques clés utilisées sur Afrissime pour évaluer votre performance.
      </p>
      {/if}
      {if $language.iso_code == 'en'}
        <p>Yes, customers can leave comments. A high percentage of positive reviews is a critical success factor on Afrissime. This is the best way for buyers to know if you are worthy of their trust. Your percentage of positive reviews is displayed on the offers page and is one of the first visible information. Customers are more likely to buy products from vendors with a high percentage of positive reviews. Your percentage of positive reviews is one of the key statistics used on Afrissime to evaluate your performance.
      </p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
       <p class="accordion-toggle">Puis-je proposer des services cadeaux (emballage et messages) à mes clients sur Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
       <p class="accordion-toggle">Can I offer gift services (packaging and messages) to my clients on Afrissime?</p>
      {/if}
      <div class="accordion-content">
      {if $language.iso_code == 'fr'}
        <p>Oui. Notre service de messages cadeaux permet aux clients de joindre des messages à des articles individuels ou à des commandes complètes. Notre service d'emballage cadeau permet aux clients de sélectionner un emballage cadeau pour chaque article de leur commande. En tant que vendeur, vous pouvez mettre l'un de ces services, voire les deux, à la disposition des acheteurs de vos produits.
        </p>
      {/if}
      {if $language.iso_code == 'en'}
        <p>Yes. Our gift messaging service allows customers to attach messages to individual items or to complete orders. Our gift wrapping service allows customers to select a gift wrap for each item of their order. As a seller, you can put one or both of these services at the disposal of the buyers of your products.
        </p>
      {/if}
      </div>
      {if $language.iso_code == 'fr'}
        <p class="accordion-toggle">A qui puis-je m'adresser si j'ai des questions concernant le programme Vendre sur Afrissime ?</p>
      {/if}
      {if $language.iso_code == 'en'}
        <p class="accordion-toggle">Who can I contact if I have questions about the Sell on Afrissime program?</p>
      {/if}
      <div class="accordion-content">
        {if $language.iso_code == 'fr'}
        <p>Notre équipe commerciale sera heureuse de détailler avec vous les avantages du programme. Remplissez le formulaire Contactez-nous.
        </p>
        {/if}
        {if $language.iso_code == 'en'}
        <p>Our sales team will be happy to detail with you the benefits of the program. Fill out the Contact Us form.
        </p>
        {/if}
      </div>
    </div>
  </div>
      <div class="section text-center landing-section section-white">
    <div class="container">
      {if $language.iso_code == 'fr'}
        <h3 style="font-weight:700">Ils vendent déjà sur Afrissime</h3>
      {/if}
      {if $language.iso_code == 'en'}
        <h3 style="font-weight:700">They already sell on Afrissime</h3>
      {/if}
      <div class="video-container">
        <div class="col-md-8 col-md-offset-2">
      		<iframe width="849" height="534" src="https://www.youtube.com/embed/6ZfMa7h4vbo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
        	{if $language.iso_code == 'fr'}
            <a href="{$urls.base_url}demande-du-vendeur" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
          {else}
            <a href="{$urls.base_url}seller-request" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">I create my shop</a>
          {/if}
    </div>
  </div>

      <div class="section landing-section">
        <div class="container">
          <div class="row" style="margin-top:20px">
            <div class="col-md-8 col-md-offset-2">
              {if $language.iso_code == 'fr'}
                <h3 style="font-weight:700; text-align:center">Contactez nous</h3>
              {/if}
              {if $language.iso_code == 'en'}
                <h3 style="font-weight:700; text-align:center">Contact us</h3>
              {/if}
              <form data-behaviour="disable-btn-on-submit" class="new_contact_form" id="new_contact_form" action="{$urls.pages.contact}" accept-charset="UTF-8" data-remote="true" method="post"><input name="utf8" type="hidden" value="✓">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="required" for="contact_form_email">Email</label>
                      <input class="form-control" placeholder="Email" type="text" value="" name="contact_form[email]" id="contact_form_email">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="required" for="contact_form_name">Nom</label>
                      <input class="form-control" placeholder="{if $language.iso_code == 'fr'}Prénom /Nom{/if}{if $language.iso_code == 'en'}Firstname / Lastname{/if}" type="text" name="contact_form[name]" id="contact_form_name">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="required" for="contact_form_message">Message</label>
                  <textarea class="form-control" rows="4" placeholder="{if $language.iso_code == 'fr'}Dites nous tout!{/if}{if $language.iso_code == 'en'}Tell us everything {/if}" name="contact_form[message]" id="contact_form_message"></textarea>
                </div>
                
                  <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                      <button name="button" type="submit" class="btn btn-warning btn-block btn-lg btn-fill" data-action="loadbtn" data-disable-with="Contactez nous…" style="border-radius: 2em;min-width: 7em;">{if $language.iso_code == 'fr'}Contactez nous{/if}{if $language.iso_code == 'en'}Contact us{/if}</button>
                    </div>
                  </div>

      </form>      </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
{/block}
