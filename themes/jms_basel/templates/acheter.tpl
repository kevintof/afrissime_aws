{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="{$urls.base_url}themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="{$urls.base_url}themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="{$urls.base_url}themes/jms_basel/assets/css/aos.css" />
{/block}
{block name='page_content'}
<div id="pageshop">
  <div id="transcroller-body" class="aos-all">
      <div class="landing-header" style="background-image: url('{$urls.base_url}themes/jms_basel/assets/img/seller_cover.jpg');height:600px">
        <div class="container">
          <div class="intro_shop">
            {if $language.iso_code == 'fr'}
              <h2>Achetez des articles authentiques</h2>
              <h3>en consommant des produits fabriqués par des africains</h3>
            {/if}
            {if $language.iso_code == 'en'}
              <h2>Buy authentic items</h2>
              <h3>by consuming products made by Africans</h3>
            {/if}
            <br>
          </div>
        </div>
      </div>
      <div class="main">
       <div class="section landing-section">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          {if $language.iso_code == 'fr'}
            <h3 style="font-weight:700">Comment acheter sur Afrissime?</h3>
          {/if}
          {if $language.iso_code == 'en'}
            <h3 style="font-weight:700">How to buy on Afrissime?</h3>
          {/if}
          <p class="text-center">
            {if $language.iso_code == 'fr'}
              L'achat sur Afrissime se fait en 4 étapes:
            {/if}
            {if $language.iso_code == 'en'}
              The purchase on Afrissime is done in 4 steps:
            {/if}
          </p>
          {if $language.iso_code == 'fr'}
            <h4 style="font-weight:600">1. Rechercher votre article</h4>
            <p>Nous avons plusieurs façons de rechercher un produit sur Afrissime</p>
          {/if}
          {if $language.iso_code == 'en'}
            <h4 style="font-weight:600">1. Search your article</h4>
            <p>We have several ways to search for a product on Afrissime</p>
          {/if}
        </div>
      </div>
      <div class="row stepper">
      	<div class="col-md-6">
           <div class="info aos-item" data-aos="fade-in">
            {if $language.iso_code == 'fr'}
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="{$urls.base_url}themes/jms_basel/assets/img/search_step_2.jpg" alt="Recherche à partir du moteur de recherche" >
              </div>
              <div class="description">
                <p><strong>Rechercher à partir du moteur de recherche</strong></p>
                <p class="text-center">Cette méthode est plus rapide et adapté lorsque vous connaissez le nom du produit que vous recherchez. </p>
              </div>
            {/if}
            {if $language.iso_code == 'en'}
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="{$urls.base_url}themes/jms_basel/assets/img/search_step_2.jpg" alt="Search from the search engine" >
              </div>
              <div class="description">
                <p><strong>Search from the search engine</strong></p>
                <p class="text-center">This method is faster and better when you know the name of the product you are looking for.</p>
              </div>
            {/if}
           </div>
        </div>
        <div class="col-md-6">
         <div class="info aos-item" data-aos="fade-in">
          {if $language.iso_code == 'fr'}
            <div>
              <div class="icon icon-primary">
                <img src="{$urls.base_url}themes/jms_basel/assets/img/search_step_1.jpg" alt="Recherche par catégorie" >
              </div>
              <div class="description">
                <p><strong>Rechercher en parcourant les catégories  </strong></p>
                <p class="text-center">Si vous n'avez pas beaucoup d'information sur le produit que vous recherchez, l'idéal serait de chercher à partir de la catégorie appropriée. </p>
              </div>
            </div>
          {/if}
          {if $language.iso_code == 'en'}
            <div>
              <div class="icon icon-primary">
                <img src="{$urls.base_url}themes/jms_basel/assets/img/search_step_1.jpg" alt="Search by category" >
              </div>
              <div class="description">
                <p><strong>Search by browsing categories  </strong></p>
                <p class="text-center">if you do not have a lot of information about the product you are looking for, the ideal would be to search from the appropriate category.</p>
              </div>
            </div>
          {/if}
         </div>
        </div>
        
      </div>
      	<div class="row">
	        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
	          <div class="col-md-3">
	            <img src="{$urls.base_url}themes/jms_basel/assets/img/add_to_cart.png" alt="{if $language.iso_code == 'fr'}Ajouter au panier {/if} {if $language.iso_code == 'en'}Add to cart {/if}" width="200px">
	          </div>
	          <div class="col-md-9">
	              <h4 style="font-weight:600">2. {if $language.iso_code == 'fr'}Ajouter votre article au panier {/if} {if $language.iso_code == 'en'}Add your item to cart {/if}</h4>
	              <p>
	              	{if $language.iso_code == 'fr'}
                  En ajout votre article au panier, vous êtes certains de le retrouver sur la page récapitulatif de votre commande.<br/>
                  Une fois l'article ajouté au panier, vous pourrez passer soit continuer les achats ou soit procéder au paiement.
                  {/if} {if $language.iso_code == 'en'}
                    Add your item to the cart, you are certain to find it on the summary page of your order.<br/>
                    Once the item has been added to the cart, you can either continue shopping or make the payment
                  {/if} 
	              </p>
	          </div>
	        </div>
	     </div>
       	<div class="row">
	        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
	          <div class="col-md-9">
	              <h4 style="font-weight:600">3. {if $language.iso_code == 'fr'}Procéder ensuite au paiement {/if} {if $language.iso_code == 'en'}Then proceed to payment {/if} </h4>
	              <p>
	                {if $language.iso_code == 'fr'}
                    Afrissime offre plusieurs méthodes de paiement à savoir :
	                  <ul class="list-unstyled">
	                  	<li>Le paiement par chèque</li>
	                  	<li>Le paiement par virement bancaire</li>
	                  	<li>Le paiement par Paypal et carte bancaire VISA ou Mastercard</li>
	                  	<li>Le paiement en espèce</li>
                      <li>Le paiement par mobile money et porteuille mobile</li>
	                  	<li>Le paiement à la livraison</li>
                      <li>Le paiement par bitcoin</li>
	                  </ul>
                  {/if}
                  {if $language.iso_code == 'en'}
                    Afrissime offers several methods of payment namely:
                    <ul class="list-unstyled">
                      <li>Payment by check</li>
                      <li>Payment by bank transfer</li>
                      <li>Payment by Paypal and VISA or Mastercard credit card</li>
                      <li>Payment by cash</li>
                      <li>Payment by mobile money and wallet</li>
                      <li>Payment on delivery</li>
                      <li>Payment by bitcoin</li>
                    </ul>
                  {/if}
	              </p>
	          </div>
	          <div class="col-md-3">
	            <img src="{$urls.base_url}themes/jms_basel/assets/img/cash-payment.jpg" alt="paiement" width="200px">
	          </div>
	        </div>
	     </div>
	     <div class="row">
		        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
		       	  <div class="col-md-3">
		            <img src="{$urls.base_url}themes/jms_basel/assets/img/delivery.png" width="200px" alt="Livraison">
		          </div>
		          <div class="col-md-9">
                  {if $language.iso_code == 'fr'}
		                <h4 style="font-weight:600">4. Faîtes vous livrer</h4>
                  {/if}
                  {if $language.iso_code == 'en'}
                    <h4 style="font-weight:600">4. You are delivered</h4>
                  {/if}
		              <p>
		                  Nous vous livrons vos produits, quelque soit votre lieu de résidence avec la plus rapidité possible et dans les plus brefs délais.
		              </p>
		          </div>
		          
		        </div>
		      </div>
		  </div>
    </div>
	     
    </div>
</div>
<script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
{/block}
