{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="{$urls.base_url}themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="{$urls.base_url}themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="{$urls.base_url}themes/jms_basel/assets/css/aos.css" />
{/block}
{block name='page_content'}
<div id="pageshop">
  <div id="transcroller-body" class="aos-all">
      <div class="landing-header" style="background-image: url('{$urls.base_url}themes/jms_basel/assets/img/seller_cover.jpg');height:600px">
        <div class="container">
          <div class="intro_shop">
            {if $language.iso_code == 'fr'} 
              <h2>Gagnez de l'argent</h2>
              <h3>en touchant des clients partout dans le monde</h3>
            {/if}
            {if $language.iso_code == 'en'} 
              <h2>Earn money</h2>
              <h3>Reaching customers around the world</h3>
            {/if}
            <br>
              {if $language.iso_code == 'fr'}
                  <a href="{$urls.base_url}demande-du-vendeur" class="btn  btn-effect" style="color: #000;background-color: #fff;border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
                {else}
                  <a href="{$urls.base_url}seller-request" class="btn  btn-effect" style="color: #000;background-color: #fff;border-radius: 2em;min-width: 7em;">I create my shop</a>
                {/if}
          </div>
        </div>
      </div>
      <div class="main">
       <div class="section landing-section">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          {if $language.iso_code == 'fr'} 
            <h3 style="font-weight:700">Comment vendre sur Afrissime?</h3>
            <p class="text-center">
            La vente sur Afrissime se fait en 3 étapes :
            </p>
              <h4 style="font-weight:600">Création de votre boutique</h4>
          {/if}
          {if $language.iso_code == 'en'} 
            <h3 style="font-weight:700">How to sell on Afrissime?</h3>
            <p class="text-center">
            The sale on Afrissime is done in 3 steps::
            </p>
              <h4 style="font-weight:600">Let's create your shop</h4>
          {/if}
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-in">
            <div>
              <div class="icon icon-primary">
                <img src="{$urls.base_url}themes/jms_basel/assets/img/shop_step_1.jpg" alt="{if $language.iso_code == 'en'} Your personal information {/if} {if $language.iso_code == 'fr'} Vos informations personnelles {/if}" >
              </div>
              <div class="description">
                {if $language.iso_code == 'fr'} 
                  <p><strong>1. Renseignez vos informations personnels </strong></p>
                  <p class="text-center">Ces informations serviront à vous créer en même temps un espace personnel sur Afrissime </p>
                {/if}
                {if $language.iso_code == 'en'} 
                  <p><strong>1. Fill in your personal information </strong></p>
                  <p class="text-center">This information will serve to create at the same time a personal space on Afrissime.</p>
                {/if}
              </div>
            </div>
            
         </div>
         <div style="float:right;">
                <img class="next hidden-sm hidden-xs" src="{$urls.base_url}themes/jms_basel/assets/img/next.png"  alt="Next" >
         </div>
        </div>
        <div class="col-md-4">
           <div class="info aos-item" data-aos="fade-in">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="{$urls.base_url}themes/jms_basel/assets/img/shop_step_2.jpg" alt="Inscription" >
              </div>
              <div class="description">
                {if $language.iso_code == 'fr'}
                  <p><strong>2. Renseignez les informations à propos de la boutique</strong></p>
                  <p class="text-center">Donnez plus d'information pour votre boutique afin de permettre aux acheteurs d'en savoir plus vos conditions de livraison et options de paiement</p>
                {/if}
                {if $language.iso_code == 'en'}
                  <p><strong>2. Information about the shop</strong></p>
                  <p class="text-center">Provide more information for your shop to allow buyers to find out more about your delivery terms and payment options</p>
                {/if}
              </div>
           </div>
           <div style="float:right;">
                <img class="next hidden-sm hidden-xs" src="{$urls.base_url}themes/jms_basel/assets/img/next.png"  alt="Next" >
           </div>
        </div>
        <div class="col-md-4">
           <div class="info aos-item" data-aos="fade-in">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="{$urls.base_url}themes/jms_basel/assets/img/shop_step_3.jpg" alt="{if $language.iso_code == 'fr'} Conditions générales de vente {/if} {if $language.iso_code == 'en'} Check the terms of sale {/if}" >
              </div>
              <div class="description">
                <p><strong>3.{if $language.iso_code == 'fr'} Chochez les conditions générales de vente {/if} {if $language.iso_code == 'en'} Check the terms of sale {/if}</strong></p>
                <p class="text-center">{if $language.iso_code == 'fr'} Après lecture des conditions générales de vente vous devez cliquer afin de donner votre accord. {/if} {if $language.iso_code == 'en'} After reading the terms of sale you must click to give your agreement. {/if}</p>
              </div>
           </div>
           
        </div>
      </div>
      <div class="row">
        <div class="md-12 text-center">
            <h4 style="font-weight:600">{if $language.iso_code == 'fr'} Gestion de vos produits {/if} {if $language.iso_code == 'en'}Managment of your products{/if}</h4>
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-6">
         <div class="info aos-item" data-aos="zoom-out">
          
            <div class="icon icon-primary">
              <img src="{$urls.base_url}themes/jms_basel/assets/img/account_step_1.jpg"  alt="{if $language.iso_code == 'fr'} Mon compte {/if} {if $language.iso_code == 'en'} My account {/if}" >
            </div>
            <div class="description">
              <p><strong>1. {if $language.iso_code == 'fr'} Connectez vous sur Afrissime {/if} {if $language.iso_code == 'en'} Sign in to Afrissime {/if}</strong></p>
              <p class="text-center"> 
              {if $language.iso_code == 'fr'} Vous devez vous connecter à votre espace personnel avant de pouvoir gérer votre boutique.Une fois connecté, cliquez sur le menu "Produit" pour accéder à la liste de vos produits. {/if}
              {if $language.iso_code == 'en'} You must log in to your personal space before you can manage your shop. Once logged in, click on the "Product" menu to access the list of your products {/if}
              </p>
            </div>
         </div>
          <div style="float:right;">
                <img class="next hidden-sm hidden-xs" src="{$urls.base_url}themes/jms_basel/assets/img/next.png"  alt="Next" >
           </div>
        </div>
        <div class="col-md-6">
           <div class="info aos-item" data-aos="zoom-out">
             
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="{$urls.base_url}themes/jms_basel/assets/img/account_step_2.jpg"  alt="{if $language.iso_code == 'fr'} Liste de vos produits {/if} {if $language.iso_code == 'en'} List of your products {/if}">
              </div>
              <div class="description">
                <p><strong>2. {if $language.iso_code == 'fr'} Gestion de vos articles {/if} {if $language.iso_code == 'en'} Managment of your products {/if} </strong></p>
                <p class="text-center">
                {if $language.iso_code == 'fr'} Vous pouvez gérer vos produits (ajouter, modifier les informations) à partir de cette page. {/if}
                {if $language.iso_code == 'en'} You can manage your products (add, edit information) from this page.{/if}
                </p>
              </div>
           </div>
        </div>
      </div>
       <div class="row">
        <div class="md-12 text-center">
            <h4 style="font-weight:600">{if $language.iso_code == 'fr'}Faîtes connaitre votre boutique {/if} {if $language.iso_code == 'en'} Let’s know your shop{/if}</h4>
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-12">
         <div class="info aos-item" data-aos="zoom-out">
          
            <div class="icon icon-primary">
              <img src="{$urls.base_url}themes/jms_basel/assets/img/shop_step_4.jpg" alt="Ma boutique" >
            </div>
            <div class="description">
              <p><strong>{if $language.iso_code == 'fr'} Partagez le lien de votre boutique sur vos réseaux sociaux {/if} {if $language.iso_code == 'en'} Share the link of your shop on your social networks {/if}</strong></p>
              <p class="text-center">
                {if $language.iso_code == 'fr'} Faire connaître votre boutique auprès de vos contacts et auprès de la communauté d'Afrissime, vous permettra de gagner en visibilité.{/if} 
                {if $language.iso_code == 'en'} Make your store known to your contacts and the community of Afrissime, you will gain visibility.{/if}
              </p>
            </div>
         </div>
        </div>
      </div>
    </div>
  </div>
    </div>
</div>
<script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
{/block}
