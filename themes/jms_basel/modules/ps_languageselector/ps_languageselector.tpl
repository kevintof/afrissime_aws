{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block languages module -->
{if $jpb_homepage == 1 || $jpb_homepage == 3 || $jpb_homepage == 6}
	{if count($languages) > 1}
		<div class="top-list">
			<form action="{$urls.base_url}" method="get">
				<ul>
					<li>
						{if $language.iso_code=='fr'}
						<a href="{$urls.base_url}ouvrir-une-boutique">Vendre sur Afrissime</a>
						{else}
						<a href="{$urls.base_url}create-your-shop">Sell on Afrissime</a>

						{/if}
					</li>
					<li><a href="#" class="dropdown-btn">{if $language.iso_code=='fr'}Aide {else} Help {/if}<i class="fa fa-caret-down"></i></a>
						<ul id="dropdown-container">
							<li style="padding-left: 20px;"><a href="#"> {if $language.iso_code=='fr'}Service client {else} Customer service {/if}</a></li>
							<li><a href="#"> Litige </a></li>
							<li><a href="#"> {if $language.iso_code=='fr'}Effectuer un retour Produit {else} Return a product {/if}</a></li>
							<li><a href="#">Protection de l'acheteur</a></li>
							<li>
						<a href="#" style="border-right:none;">{l s='Litiges / réclamations:' d='Shop.Theme.Global'}</a>
					</li>
						</ul>
					</li>
				</ul>
				
					<ul class="switcher">
						<li>
						<a href="#" class="dropdown-btn"> <img src="{$urls.base_url}themes/jms_basel/assets/img/icon/countries/{$pays_visiteur}.png"/> / <span>{$current_language.name_simple|truncate:3:''}</span> / <span>{$currency.iso_code}</span> <i class="fa fa-caret-down"></i></a>
						
								<ul id="dropdown-switcher">
									<li>
										<span style="font-size:12px; color:#000;padding-left:20px;"> {l s='Localisation:' d='Shop.Theme.Global'} </span>
										<div style="overflow:hidden; padding-left:20px;padding-bottom:20px;">
												<select name="id_country" id="id_country" class="form-control form-control-select" style="width:200px">
												<option value="">{l s='Select Country' mod='marketplace'}</option>
												{foreach $country as $countrydetail}
													<option value="{$countrydetail.id_country}">
														{$countrydetail.name}
													</option>
												{/foreach}
											</select>
										</div>
									</li>
									<li>
										<span style="color:#000; font-size:12px">{l s='Language' d='Shop.Theme.Global'}</span>
										<div style="overflow:hidden;padding-left:20px">
										<select name="id_lang" class="form-control form-control-select" style="width:200px">
											<option value="">{l s='Select Language' mod='Shop.Theme.Global'}</option>
											{foreach from=$languages item=language}
												<option value="{$language.id_lang}">{$language.name_simple}
											    </option>
											{/foreach}
										</select>
											
										</div>
									</li>
									<li>
										<span style="font-size:12px; color:#000"> {l s='Monnaie:' d='Shop.Theme.Global'} </span>
										
											<div style="overflow:hidden; padding-left:20px;padding-bottom:20px;">
												<select name="id_currency" id="currency" class="form-control form-control-select" style="width:200px;margin-bottom:20px;">
													<option value="1">€ Euro - EUR</option>
													<option value="2">$ Dollar US - USD</option>
													<option value="3">CFA XOF</option>
													<option value="5">FCFA XAF</option>
													<option value="6">£ GBP</option>
												</select>
												<div>
													<button name="button" type="submit" class="btn btn-active btn-effect" style="width:200px;">{l s='ENREGISTRER' d='Shop.Theme.Global'}</button>
												</div>
												<input type="hidden" name="SubmitCurrency" value="1"/>
											</div>	
										
									</li>
								</ul>
							
						</li>
					</ul>
				
				<ul style="padding-left:10px">
					<li>
						{if $language.iso_code=='fr'} 
							<a href="{$urls.base_url}articles" style="border:none">Blog</a>
						{/if}
						{if $language.iso_code=='en'} 
							<a href="{$urls.base_url}news" style="border:none">News</a>
						{/if}
					</li>
				</ul>
			</form>
		</div>
		<script>
			//* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
			var dropdown = document.getElementsByClassName("dropdown-btn");
			var i;

			for (i = 0; i < dropdown.length; i++) {
			  dropdown[i].addEventListener("click", function() {
			    this.classList.toggle("active");
			    var dropdownContent = this.nextElementSibling;
			    if (dropdownContent.style.display === "block") {
			      dropdownContent.style.display = "none";
			    } else {
			      dropdownContent.style.display = "block";
			      dropdownContent.style.width = "250px";
			      dropdownContent.style.top = "42px";
			      dropdownContent.style.right = "0px";
			      dropdownContent.style.position = "absolute";
			    }
			  });
			} 
		</script>
	{/if}	
{elseif $jpb_homepage == 4 || $jpb_homepage == 7}
	{if count($languages) > 1}
		<div class="btn-group compact-hidden languages-info">
			<a href="#"  class="btn-xs dropdown-toggle" data-toggle="dropdown">
				<span class="btn-name">{$current_language.name_simple|truncate:3:''}</span>
			
			</a>
			<ul class="dropdown-menu" role="menu">
				{foreach from=$languages key=k item=language name="languages"}
					<li {if $language.id_lang == $current_language.id_lang} class="current" {/if}>
						<a href="{url entity='language' id=$language.id_lang}" class="dropdown-item">
							{$language.name_simple|truncate:3:''}
						</a>
					</li>
				{/foreach}		
			</ul>
		</div>
		<div class="btn-group compact-hidden languages-info">
			<a href="#"  class="btn-xs dropdown-toggle" data-toggle="dropdown">
				<span class="btn-name">{$current_language.name_simple|truncate:3:''}</span>
			
			</a>
			<ul class="dropdown-menu" role="menu">
				{foreach from=$languages key=k item=language name="languages"}
					<li {if $language.id_lang == $current_language.id_lang} class="current" {/if}>
						<a href="{url entity='language' id=$language.id_lang}" class="dropdown-item">
							{$language.name_simple|truncate:3:''}
						</a>
					</li>
				{/foreach}		
			</ul>
		</div>
	{/if}
{else}
{if count($languages) > 1}
		<div class="btn-group compact-hidden languages-info">
			<a href="#"  class="btn-xs dropdown-toggle" data-toggle="dropdown">
				<span class="btn-name">{l s='Languages:' d='Shop.Theme.Global'}</span>			
			</a>
			<ul>
				{foreach from=$languages key=k item=language name="languages"}
					<li {if $language.id_lang == $current_language.id_lang} class="current" {/if}>
						<a href="{url entity='language' id=$language.id_lang}" class="dropdown-item">
							{$language.name_simple}
						</a>
					</li>
				{/foreach}		
			</ul>
		</div>
	{/if}
{/if}
<!-- /Block languages module -->
