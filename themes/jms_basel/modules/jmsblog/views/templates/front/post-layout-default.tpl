{*

* 2007-2014 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*}

{if $sidebar == 'left'} 

	{assign var="layout" value="layouts/layout-left-column.tpl"}

{elseif $sidebar == 'right'}

	{assign var="layout" value="layouts/layout-right-column.tpl"}

{elseif $sidebar == 'no'}	

	{assign var="layout" value="layouts/layout-full-width.tpl"}

{/if}
{assign var="layout" value="layouts/layout-left-column.tpl"}
{extends file='page.tpl'}

{block name="page_content"}

{capture name=path}{$post.title|escape:'html':'UTF-8'}{/capture}

<div class="single-blog">

<div class="blog-post">
		{assign var=params value=['post_id' => $post.post_id, 'category_slug' => $post.category_alias, 'slug' => $post.alias]}

		{assign var=catparams value=['category_id' => $post.category_id, 'slug' => $post.category_alias]}

		{if $post.link_video && $jmsblog_setting.JMSBLOG_SHOW_MEDIA}

			<div class="post-video">

				{$post.link_video}

			</div>

		{elseif $post.image && $jmsblog_setting.JMSBLOG_SHOW_MEDIA}

			<div class="post-thumb">

				<img src="{$image_baseurl|escape:'html':'UTF-8'}{$post.image|escape:'html':'UTF-8'}" alt="{l s='Image Blog' d='Modules.JmsBlog'}" />

			</div>

		{/if}

		<h1 class="title">{$post.title|escape:'html':'UTF-8'}</h1>

		<ul class="post-meta">

			{if $jmsblog_setting.JMSBLOG_SHOW_CATEGORY}

				<li>

					<span>

						{l s='In:' d='Modules.JmsBlog'} 

						<a href="{jmsblog::getPageLink('jmsblog-category', $catparams)}">

							{$post.category_name|escape:'html':'UTF-8'}

						</a>

					</span>

				</li>

			{/if}

			<li>

				<span>{$post.created|escape:'html':'UTF-8'|date_format:"%B %e, %Y"}</span>

			</li>

			{if $jmsblog_setting.JMSBLOG_SHOW_COMMENTS}

				<li>

					<span>{$comments|@count}{l s=' comments' d='Modules.JmsBlog'}</span>

				</li>

			{/if}

			{if $jmsblog_setting.JMSBLOG_SHOW_VIEWS}

				<li>

					<span>{$post.views|escape:'html':'UTF-8'} {l s='view(s)' d='Modules.JmsBlog'}</span>

				</li>

			{/if}

		</ul>

		<div class="post-fulltext">

			{$post.fulltext nofilter}	

		</div>

	</div>

<div>


<ul class="social-btn">
	<li>
	  <div class="fb-share-button" 
	    data-href="{jmsblog::getPageLink('jmsblog-post', $params)|replace:'&amp;':'&'}" 
	    data-layout="button_count">
	  </div>
	</li>
	<li style="margin-top:5px;">
	<a href="https://twitter.com/share?ref_src={jmsblog::getPageLink('jmsblog-post', $params)|replace:'&amp;':'&'}" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	</li>
	<li>
		{literal}
<script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
<script type="IN/Share" data-url="{jmsblog::getPageLink('jmsblog-post', $params)|replace:'&amp;':'&'}"></script>
{/literal}
	</li>
	<li class="pin">
	{literal}
		<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
	{/literal}
		<a href="http://pinterest.com/pin/create/button/?url={jmsblog::getPageLink('jmsblog-post', $params)|replace:'&amp;':'&'}&media={$image_baseurl|escape:'html':'UTF-8'}{$post.image|escape:'html':'UTF-8'}&description={$post.title|escape:'html':'UTF-8'}" class="pin-it-button" count-layout="horizontal">
		    <img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" />
		</a>
	</li>
	<li class="wa">
		<a href="https://web.whatsapp.com/send?text={jmsblog::getPageLink('jmsblog-post', $params)|replace:'&amp;':'&'}" data-action="share/whatsapp/share"><span style="background-color:#1bd741; width:85px; height:20px; font-size: 11px;padding: 5px;border-radius:3px;color:#fff"><i class="fa fa-whatsapp"></i> Partager</span></a>
	</li>
	<li class="tg">
		<a class="tgme_action_button" href="https://telegram.me/share/url?url={jmsblog::getPageLink('jmsblog-post', $params)|replace:'&amp;':'&'}"><span style="background-color:#29b6f6; width:85px; height:20px; font-size: 11px;padding: 5px;border-radius:3px;color:#fff"><i class="fa fa-telegram"></i> Partager</span></a>
	</li>
</ul>

</div>


</div>


		<div id="comments">

			

				{include file="modules/jmsblog/views/templates/front/comment_default.tpl"}		

		

				{include file="modules/jmsblog/views/templates/front/comment_facebook.tpl"}		

		

		</div>


{/block}





