** Message from online shop {shop_name} **

Dear {firstname} {lastname},

Thank you for placing the order (n°{orderid}) in our shop.

In order to help us constantly improve our offer, and to help other customers in their purchase decision process, we invite you to rate the our shop. Simply follow the link below.

{link_to_form}

Thanks again !

-The {shop_name} Team