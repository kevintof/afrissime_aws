<section class="featured-products clearfix">
	<div class="custom-title">
	    <h3>
	        {l s='Popular Products' d='Shop.Theme.Catalog'}
	    </h3>
	    <span class="b-title_separator"><span></span></span>
	  </div>
  
  <div class="products customs-carousel-product">
    {foreach from=$products item="product"}
      {include file="catalog/_partials/miniatures/product.tpl" product=$product}
    {/foreach}
  </div>
</section>