{assign var=unique_id value=1|mt_rand:1000}
<script type="text/javascript">
	if(c1_unique)
		c1_unique.push({$unique_id});
	else 
		var c1_unique = [];
		c1_unique.push({$unique_id});

	if(c1_items)
		c1_items[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	else
		var c1_items = [];
		c1_items[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	if(c1_itemsDesktop)
		c1_itemsDesktop[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	else
		var c1_itemsDesktop = [];
		c1_itemsDesktop[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};

	if(c1_itemsDesktopSmall)
		c1_itemsDesktopSmall[{$unique_id}]={if $cols_md}{$cols_md|escape:'htmlall':'UTF-8'}{else}3{/if};
	else
		var c1_itemsDesktopSmall = [];
		c1_itemsDesktopSmall[{$unique_id}]={if $cols_md}{$cols_md|escape:'htmlall':'UTF-8'}{else}3{/if};

	if(c1_itemsTablet)
		c1_itemsTablet[{$unique_id}]={if $cols_sm}{$cols_sm|escape:'htmlall':'UTF-8'}{else}2{/if};
	else
		var c1_itemsTablet = [];
		c1_itemsTablet[{$unique_id}]={if $cols_sm}{$cols_sm|escape:'htmlall':'UTF-8'}{else}2{/if};
	if(c1_itemsMobile)
		c1_itemsMobile[{$unique_id}]={if $cols_xs}{$cols_xs|escape:'htmlall':'UTF-8'}{else}1{/if};
	else
		var c1_itemsMobile = [];
		c1_itemsMobile[{$unique_id}]={if $cols_xs}{$cols_xs|escape:'htmlall':'UTF-8'}{else}1{/if};

	if(c1_nav)
		c1_nav[{$unique_id}]={if $navigation ==1}true{else}false{/if};
	else
		var c1_nav = [];
		c1_nav[{$unique_id}]={if $navigation ==1}true{else}false{/if};

	if(c1_pag)
		c1_pag[{$unique_id}]={if $pagination ==1}true{else}false{/if};
	else
		var c1_pag = [];
		c1_pag[{$unique_id}]={if $pagination ==1}true{else}false{/if};
	if(c1_auto)
		c1_auto[{$unique_id}]={if $autoplay ==1}true{else}false{/if};
	else
		var c1_auto = [];
		c1_auto[{$unique_id}]={if $autoplay ==1}true{else}false{/if};
	if(c1_rewind)
		c1_rewind[{$unique_id}]={if $rewind ==1}true{else}false{/if};
	else
		var c1_rewind = [];
		c1_rewind[{$unique_id}]={if $rewind ==1}true{else}false{/if};
	if(c1_slideby)
		c1_slideby[{$unique_id}]={if $slidebypage ==1}'page'{else}1{/if};
	else
		var c1_slideby = [];
		c1_slideby[{$unique_id}]={if $slidebypage ==1}'page'{else}1{/if};	

	var c_space = 30;
</script>
<div class="home_categories4">
	{if $addon_title || $addon_desc}
		<div class="addon-title custom-title5">
			{if $addon_title}
				<h3>
					{$addon_title|escape:'htmlall':'UTF-8'}	
				</h3>		
			{/if}
			<span class="text-center">
                <span style="background-color: #e49a9e; opacity: : 0.7; height: 2px;width: 50px;" class="d-inline-block"></span>
            </span>
			{if $addon_desc}
				<p class="addon-desc">{$addon_desc|escape:'htmlall':'UTF-8'}</p>
			{/if}
		</div>
	{/if}
    {if isset($categories) AND $categories}
            <div class="categories-carousel categories-carousel-{$unique_id}">
            {foreach from=$categories item=category key=k}
                {assign var='categoryLink' value=$link->getcategoryLink($category.id_category, $category.link_rewrite)}
					<div class="categories-wrapper">
						{if $show_img == 1}
							<div class="categoy-image img-zoom">
								<a href="{$categoryLink}">
									<img src="{$img_cat_dir}{$category.id_category}_thumb.jpg" alt="{$category.name}" title="{$category.name}" class="img-responsive"/>
									<div class="product-count">
										{$category.product_count}{l s=' Products' d='Modules.JmsPagebuilder'}
									</div>
								</a>
							</div>
						{/if}
						
							{if $show_product == 1}
								<div class="category-info">
									<div class="cat-name">
										<span>{$category.name}</span>
									</div>
									<a href="{$categoryLink}" class="view-product">
										{l s='View products' d='Modules.JmsPagebuilder'}
									</a>
								</div>
							{/if}
						
					</div>
            {/foreach}
            </div>
	    {else}
	        <p>{l s='No categories' d='Modules.JmsPagebuilder'}</p>
  	{/if}
</div>