{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{assign var=unique_id value=1|mt_rand:1000}
<script type="text/javascript">
	if(p_unique)
		p_unique.push({$unique_id});
	else 
		var p_unique = [];
		p_unique.push({$unique_id});

	if(p_items)
		p_items[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	else
		var p_items = [];
		p_items[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	if(p_itemsDesktop)
		p_itemsDesktop[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	else
		var p_itemsDesktop = [];
		p_itemsDesktop[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};

	if(p_itemsDesktopSmall)
		p_itemsDesktopSmall[{$unique_id}]={if $cols_md}{$cols_md|escape:'htmlall':'UTF-8'}{else}3{/if};
	else
		var p_itemsDesktopSmall = [];
		p_itemsDesktopSmall[{$unique_id}]={if $cols_md}{$cols_md|escape:'htmlall':'UTF-8'}{else}3{/if};

	if(p_itemsTablet)
		p_itemsTablet[{$unique_id}]={if $cols_sm}{$cols_sm|escape:'htmlall':'UTF-8'}{else}2{/if};
	else
		var p_itemsTablet = [];
		p_itemsTablet[{$unique_id}]={if $cols_sm}{$cols_sm|escape:'htmlall':'UTF-8'}{else}2{/if};
	if(p_itemsMobile)
		p_itemsMobile[{$unique_id}]={if $cols_xs}{$cols_xs|escape:'htmlall':'UTF-8'}{else}1{/if};
	else
		var p_itemsMobile = [];
		p_itemsMobile[{$unique_id}]={if $cols_xs}{$cols_xs|escape:'htmlall':'UTF-8'}{else}1{/if};

	if(p_nav)
		p_nav[{$unique_id}]={if $navigation ==1}true{else}false{/if};
	else
		var p_nav = [];
		p_nav[{$unique_id}]={if $navigation ==1}true{else}false{/if};

	if(p_pag)
		p_pag[{$unique_id}]={if $pagination ==1}true{else}false{/if};
	else
		var p_pag = [];
		p_pag[{$unique_id}]={if $pagination ==1}true{else}false{/if};
	if(p_auto)
		p_auto[{$unique_id}]={if $autoplay ==1}true{else}false{/if};
	else
		var p_auto = [];
		p_auto[{$unique_id}]={if $autoplay ==1}true{else}false{/if};
	if(p_rewind)
		p_rewind[{$unique_id}]={if $rewind ==1}true{else}false{/if};
	else
		var p_rewind = [];
		p_rewind[{$unique_id}]={if $rewind ==1}true{else}false{/if};
	if(p_slideby)
		p_slideby[{$unique_id}]={if $slidebypage ==1}'page'{else}1{/if};
	else
		var p_slideby = [];
		p_slideby[{$unique_id}]={if $slidebypage ==1}'page'{else}1{/if};
</script>
<div class="product-carousel-wrapper default-style">
	{if $addon_title || $addon_desc}
		<div class="addon-title custom-title">
			{if $addon_title}
				<h3>
					{$addon_title|escape:'htmlall':'UTF-8'}	
				</h3>		
			{/if}
			{if $addon_desc}
				<p class="addon-desc">{$addon_desc|escape:'htmlall':'UTF-8'}</p>
			{/if}
			<span class="b-title_separator"><span></span></span>
		</div>
	{/if}
	
	
	<div class="product-carousel product-carousel-{$unique_id}">	
		{foreach from = $products_slides item = products_slide}
			<div class="item ajax_block_product">
				{foreach from = $products_slide item = product}
					{include file="catalog/_partials/miniatures/product.tpl" product=$product}
				{/foreach}
			</div>
		{/foreach}
	</div>
</div>