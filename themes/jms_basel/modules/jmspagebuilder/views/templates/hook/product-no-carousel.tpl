<div class="product-no-carousel">
	{if $addon_title || $addon_desc}
		<div class="addon-title custom-title2">
			{if $addon_title}
				<h3>
					{$addon_title|escape:'htmlall':'UTF-8'}	
				</h3>		
			{/if}
			{if $addon_desc}
				<p class="addon-desc">{$addon_desc|escape:'htmlall':'UTF-8'}</p>
			{/if}
		</div>
	{/if}
	<div class="row">
		{foreach from = $products_slides item = products_slide key = k}
			<div class="moreBox ajax_block_product col-xs-6 col-sm-4 col-md-3" {if $k > 7} style="display: none;" {/if}>
				{foreach from = $products_slide item = product}
					{include file="catalog/_partials/miniatures/product.tpl" product=$product}
				{/foreach}
			</div>
		{/foreach}
	</div>
	{if $products_slides|@count >8 }
		<div id="loadMore" style="">
         	<a href="#" class="btn">{l s='LOAD MORE PORTFOLIO' d='Modules.JmsPagebuilder'}</a>
      	</div>
  	{/if}
</div>