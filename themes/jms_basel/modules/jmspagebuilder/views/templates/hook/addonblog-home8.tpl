{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{assign var=unique_id value=1|mt_rand:1000}
<script type="text/javascript">	
	if(blog_unique)
		blog_unique.push({$unique_id});
	else 
		var blog_unique = [];
		blog_unique.push({$unique_id});

		if(blog_items)
			blog_items[{$unique_id}]={if $items_show}{$items_show|escape:'htmlall':'UTF-8'}{else}4{/if};
		else
			var blog_items = [];
			blog_items[{$unique_id}]={if $items_show}{$items_show|escape:'htmlall':'UTF-8'}{else}4{/if};
		if(blog_itemsDesktop)
			blog_itemsDesktop[{$unique_id}]={if $items_show}{$items_show|escape:'htmlall':'UTF-8'}{else}4{/if};
		else
			var blog_itemsDesktop = [];
			blog_itemsDesktop[{$unique_id}]={if $items_show}{$items_show|escape:'htmlall':'UTF-8'}{else}4{/if};

		if(blog_itemsDesktopSmall)
			blog_itemsDesktopSmall[{$unique_id}]={if $items_show_md}{$items_show_md|escape:'htmlall':'UTF-8'}{else}3{/if};
		else
			var blog_itemsDesktopSmall = [];
			blog_itemsDesktopSmall[{$unique_id}]={if $items_show_md}{$items_show_md|escape:'htmlall':'UTF-8'}{else}3{/if};

		if(blog_itemsTablet)
			blog_itemsTablet[{$unique_id}]={if $items_show_sm}{$items_show_sm|escape:'htmlall':'UTF-8'}{else}2{/if};
		else
			var blog_itemsTablet = [];
			blog_itemsTablet[{$unique_id}]={if $items_show_sm}{$items_show_sm|escape:'htmlall':'UTF-8'}{else}2{/if};
		if(blog_itemsMobile)
			blog_itemsMobile[{$unique_id}]={if $items_show_xs}{$items_show_xs|escape:'htmlall':'UTF-8'}{else}1{/if};
		else
			var blog_itemsMobile = [];
			blog_itemsMobile[{$unique_id}]={if $items_show_xs}{$items_show_xs|escape:'htmlall':'UTF-8'}{else}1{/if};

		if(blog_nav)
			blog_nav[{$unique_id}]={if $navigation ==1}true{else}false{/if};
		else
			var blog_nav = [];
			blog_nav[{$unique_id}]={if $navigation ==1}true{else}false{/if};

		if(blog_pag)
			blog_pag[{$unique_id}]={if $pagination ==1}true{else}false{/if};
		else
			var blog_pag = [];
			blog_pag[{$unique_id}]={if $pagination ==1}true{else}false{/if};
		if(blog_auto)
			blog_auto[{$unique_id}]={if $autoplay ==1}true{else}false{/if};
		else
			var blog_auto = [];
			blog_auto[{$unique_id}]={if $autoplay ==1}true{else}false{/if};
		if(blog_rewind)
			blog_rewind[{$unique_id}]={if $rewind ==1}true{else}false{/if};
		else
			var blog_rewind = [];
			blog_rewind[{$unique_id}]={if $rewind ==1}true{else}false{/if};
		if(blog_slideby)
			blog_slideby[{$unique_id}]={if $slidebypage ==1}'page'{else}1{/if};
		else {
			var blog_slideby = [];
			blog_slideby[{$unique_id}]={if $slidebypage ==1}'page'{else}1{/if};
		}
		var blog_margin = 15;
</script>
<div class="blog-box">
	{if $addon_title || $addon_desc}
		<div class="addon-title custom-title4">
			{if $addon_title}
				<h3>
					{$addon_title|escape:'htmlall':'UTF-8'}	
					<span class="b-title_separator"><span></span></span>
				</h3>		
			{/if}
			
			{if $addon_desc}
				<p class="addon-desc">{$addon_desc|escape:'htmlall':'UTF-8'}</p>
			{/if}
		</div>
	{/if}
	{if $adPosts|@count gt 0}
		<div class="blog-carousel-{$unique_id} blog-carousel">	
			{foreach from=$adPosts item=arr_post}	
				<div class="item">
				{foreach from=$arr_post item=post}
					
						{assign var=params value=['post_id' => $post.post_id, 'category_slug' => $post.category_alias, 'slug' => $post.alias]}
						{assign var=catparams value=['category_id' => $post.category_id, 'slug' => $post.category_alias]}
						<div class="blog-item">
							{if $post.link_video && ($show_media == '1')}
								<div class="post-thumb">
									{$post.link_video|escape:'htmlall':'UTF-8'}
								</div>
							{elseif $post.image && ($show_media == '1')}
								<div class="post-thumb">
									<a href="{jmsblog::getPageLink('jmsblog-post', $params)|escape:'htmlall':'UTF-8'|replace:'&amp;':'&'}">
										<img src="{$image_url|escape:'html':'UTF-8'}{$post.image|escape:'html':'UTF-8'}" alt="{$post.title|escape:'htmlall':'UTF-8'}" class="img-responsive" />
									</a>
									{if $show_category == '1'}
										<div class="cat-box">
											<a href="{jmsblog::getPageLink('jmsblog-category', $catparams)|escape:'htmlall':'UTF-8'|replace:'&amp;':'&'}">
												{$post.category_name|escape:'html':'UTF-8'}
											</a>
										</div>
									{/if}
									{if $show_time == '1'}
										<div class="post-created">
											<span class="date">{$post.created|escape:'html':'UTF-8'|date_format:'%e'}</span>
											<span class="month">{$post.created|escape:'html':'UTF-8'|date_format:'%b'}</span>
										</div>
									{/if}
								</div>
							{/if}	
							<div class="post-info">
									<a href="{jmsblog::getPageLink('jmsblog-post', $params)|escape:'htmlall':'UTF-8'|replace:'&amp;':'&'}" class="post-title">	
										{$post.title}
									</a>
									<div class="view-comment">
										{if $show_nviews == '1'}
											<span class="view">
												{$post.views|escape:'html':'UTF-8'} {l s='views' d='Modules.JmsPagebuilder'}
											</span>
										{/if}
										<span class="space">/</span>
										{if $show_ncomments == '1'}		
											<span class="comment">
												{$post.comment_count|escape:'html':'UTF-8'} {l s='comments' d='Modules.JmsPagebuilder'}
											</span>
										{/if}
									</div>
								{if $show_introtext == '1'}	
									<div class="post-intro">{$post.introtext nofilter}</div>	
								{/if}
							</div>
						</div>
					
				{/foreach}	
				</div>		
			{/foreach}	
		</div>	
{/if}
</div>


	