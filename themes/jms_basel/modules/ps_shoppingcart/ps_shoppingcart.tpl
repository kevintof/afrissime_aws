<div class="btn-group compact-hidden blockcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if} dropdown js-dropdown {if isset($jpb_addtocart) && $jpb_addtocart == 'ajax_cartbottom'}shoppingcart-bottom{/if}" id="cart_block" data-refresh-url="{$refresh_url}">
			<a href="#" class="dropdown-toggle icon cart-icon" data-toggle="dropdown">	
				<span class="icon-basket"></span>
				<span class="box-cart ajax_cart_quantity">{$cart.products_count}</span>
				<span class="space">/</span>
				<span class="cart_block_total ajax_block_cart_total">{$cart.totals.total.value}</span>
			</a>
			<a href="#" class="btn-xs tab-title dropdown-toggle" data-toggle="dropdown">		 
				<span>{l s='Shoppinh cart' d='Shop.Theme.Actions'}</span>
				<span id="b-close_cart" class="b-close_search"></span>
			</a>
	<div class="dropdown-menu shoppingcart-box">
	   <div class="shoppingcart-content">
        <span class="ajax_cart_no_product" {if $cart.products_count != 0}style="display:none"{/if}>{l s='There is no product' d='Shop.Theme.Actions'}</span>
			<ul class="list products cart_block_list" id="cart_block_list">
				{foreach from=$cart.products item=product}
					<li>{include 'module:ps_shoppingcart/ps_shoppingcart-product-line.tpl' product=$product}</li>
				{/foreach}
			</ul>
			</div>
			<div class="cart-prices">
				<div class="checkout-info">
						{foreach from=$cart.subtotals item="subtotal"}
							<div class="{$subtotal.type} cart-prices-line"  >
								<span class="cart-label">{$subtotal.label}</span>
								<span class="price">{$subtotal.value}</span>
							</div>
						{/foreach}
				</div>
				<div class="checkout-info">
					<a id="button_order_cart" class="btn-default" href="{$cart_url}" title="{l s='Check out' d='Shop.Theme.Actions'}" rel="nofollow">
						<span>
							{l s='Check out' d='Shop.Theme.Actions'}
						</span>
					</a> 
				</div>
			</div>
	</div>
</div>
