<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{jmswishlist}prestashop>jmswishlist_9ae79c1fccd231ac7fbbf3235dbf6326'] = 'Ma liste d\'envies';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_56ee3495a32081ccb6d2376eab391bfa'] = 'Affichage';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_6bee4060e5e05246d4bcbb720736417c'] = 'Clients :';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_c515b215b9c6be251c924cc6d1324c41'] = 'Choisir un client';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_c440899c1d6f8c8271b9b1d171c7e665'] = 'Liste d\'envies';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_12246cb491c807e85279b8aed74ea3cf'] = 'Choisir une liste d\'envies';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_655d20c1ca69519ca647684edbb2db35'] = 'Plus récent';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_87f8a6ab85c9ced3702b4ea641ad4bb5'] = 'Moyen';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_28d0edd045e05cf5af64e35ae0c4c6ef'] = 'Ancien';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_81355310011c137fdd21cf9a1394ed6a'] = 'Liste des produits';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_8a6830ac12924a940aa0a83d5e7c76ff'] = 'Le produit a été ajouté avec succès à votre liste d\'envies';
$_MODULE['<{jmswishlist}prestashop>jmswishlist_16a23698e7cf5188ce1c07df74298076'] = 'Vous devez vous connecter pour pouvoir ajouter une wishlist.';
$_MODULE['<{jmswishlist}prestashop>mywishlist_24e059bb957ed4c27e1b92f16f7cf710'] = 'Mes listes d\'envies';
$_MODULE['<{jmswishlist}prestashop>mywishlist_b30545c7b2d429352b9afdd85be810c7'] = 'Vous devez saisir un nom pour la liste.';
$_MODULE['<{jmswishlist}prestashop>mywishlist_b74c118d823d908d653cfbf1c877ae55'] = 'Le nom de cette liste existe déjà';
$_MODULE['<{jmswishlist}prestashop>mywishlist_7098d49878bbd102b13038a748125e27'] = 'Vous pouvez pas supprimer cette liste d\'envies';
$_MODULE['<{jmswishlist}prestashop>mywishlist_ce7510c007e56bccd6b16af6c40a03de'] = 'Vous n\'êtes pas connecté';
$_MODULE['<{jmswishlist}prestashop>mywishlist_3c924eebbd7c3447336bbec3b325d3da'] = 'Erreur rencontrée lors du changement de produit d\'une liste à l\'autre';
$_MODULE['<{jmswishlist}prestashop>mywishlist_3540aa14bffcdfbbfc3aafbbcb028a1f'] = 'Le produit a été correctement déplacé dans la liste.';
