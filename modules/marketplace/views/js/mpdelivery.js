$(document).ready(function () {
    var counter = 1;

    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        cols += '<td><input type="text" class="form-control" name="transporter[]"/></td>';
        cols += '<td><select name="delivery_way[]" class="form-control form-control-select">'
                                                +'<option value="1">Maritime</option>'
                                                +'<option value="2">Aerienne</option>'
                                                +'<option value="3">Terrestre</option>'
                                            +'</select></td>';
        cols += '<td><input type="text" class="form-control" name="duration[]"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});