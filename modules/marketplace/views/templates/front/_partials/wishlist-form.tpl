<div class="modal fade" id="mk_wishlistmodal" tabindex="-1" role="dialog" aria-labelledby="mk_wishlistmodalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="text-align:left;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="mk_wishlistmodalLabel">{l s='Ajouter aux favoris' mod='marketplace'}</h3>
      </div>
      <form id="wk_wishlist_seller_form" action="#">
        <div class="modal-body">
          <fieldset>
            <legend>{l s='Ajouter à une liste existante' mod='marketplace'} </legend>
            <div class="form-group">
              <select onchange="OldListAdd(this.value)" name="list_id" style="width: 100%" class="form-control">
                {if $wishlistsQt == true}
                  {foreach from=$wishlists item=wishlist}
                    <option value="{$wishlist.id_marketplace_wishlist}">{$wishlist.wording}</option>
                  {/foreach}
                {/if}
              </select>
            </div>
          </fieldset>
          <fieldset>
            <legend>Ajouter à une nouvelle liste</legend>
            <div class="input-group">
                <span class="input-group-btn">
                  <button onclick="addToNewList()" class="btn btn-default" type="button">OK</button>
                </span>
              <input id="list_name" name="list_name" type="text" class="form-control"
                     placeholder="Saisissez le nom de la liste">
            </div>
          </fieldset>
          <input type="number" name="seller_id" hidden value="{$seller_id}"/>
          <input type="number" name="customer_id" hidden value="{$id_customer}"/>
          <div id="wishlist_error" style="text-align: left" class="modal-footer">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
  function addToNewList() {
    var html = $('<div id="wishlist_error" class="alert"></div>');
    var url = prestashop['urls']['base_url'] + '?fc=module&module=marketplace&controller=addtowishlist';
    $.ajax({
      type: 'POST',
      url: url,
      data: {
        type: "new_list_add",
        list_name: $("input[name='list_name']").val(),
        seller_id: {$seller_id},
        customer_id:{$id_customer}
      },
      success: function (json) {
        var results = JSON.parse(json);
        if (results['result'] == 'OK') {
          $("#wishlist_error").append(
            html.addClass('alert-success').append('<strong>OK</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>')
          );
        }
      }
    }).fail(function (result) {
      $("#wishlist_error").append(
        html.addClass('alert-warning')
          .append('<strong>Une erreur est survenue, veuillez réessayer !</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>')
      );

    });
  }

  function OldListAdd(list_id) {
    var html = $('<div id="wishlist_error" class="alert"></div>');

    console.log(list_id);
    var url = prestashop['urls']['base_url'] + '?fc=module&module=marketplace&controller=addtowishlist';
    $.ajax({
      type: 'POST',
      url: url,
      data: {
        type: "old_list_add",
        list_id: list_id,
        seller_id: {$seller_id},
        customer_id:{$id_customer}
      },
      success: function (json) {
        var results = JSON.parse(json);
        console.log(json);

        if (results['result'] == 'OK') {
          $("#wishlist_error").append(
            html.addClass('alert-success')
              .append('<strong>OK</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>')
          );
        }
        if (results['result'] == 'KO') {
          $("#wishlist_error").append(
            html.addClass('alert-warning')
              .append('<strong>' + results.message + '</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>')
          );
        }
      }
    }).fail(function (result) {
      $("#wishlist_error").append(
        html.addClass('alert-warning')
          .append('<strong>Une erreur est survenue, veuillez réessayer !</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'));

    });
  }
</script>
