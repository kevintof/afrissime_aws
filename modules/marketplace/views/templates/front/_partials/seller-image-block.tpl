{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}
<div class="wk_profile_container">
	<div class="avatar-upload" style="margin:0 auto">
		<div class="avatar-preview">
				 <div id="imagePreview" {if isset($seller_img_exist)} class="mp-img-preview" style="background-image: url({$seller_img_path});" {else} style="background-image: url({$seller_img_path}?time={$timestamp})"{/if}></div>
		</div>
	</div>
</div>
<div class="wk_profile_img_belowlink">
    {hook h='displayMpSellerImageBlockFooter'}
</div>
{block name='mp_image_preview'}
	{include file='module:marketplace/views/templates/front/product/_partials/mp-image-preview.tpl'}
{/block}