{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<label class="wk_formfield_required_notify">
	{if $language.iso_code=='fr'}
		Les champs qui sont marqués (<span class="required">*</span>) sont obligatoires à remplir. 
	{/if}
	{if $language.iso_code=='en'}
		Those fields which has been marked (<span class="required">*</span>) are compulsory to be filled by you.
	{/if}
	
</label>