{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{extends file='page.tpl'}
{block name='page_content_container'}
    <div id="mywishlist">
        <h2>{l s='My favorites shop, Page ' d='Shop.Theme'} {$currentPage}</h2>

        {include file='_partials/form-errors.tpl' errors=$errors}
        {if $id_customer|intval neq 0}
            {if $wishlists}
                <div id="block-history" class="block-center">
                    <table class="std">
                        <thead>
                        <tr>
                            <th scope="col" class="first_item">{l s='Nom de la liste' d='Shop.Theme'}</th>
{*                            <th scope="col" class="item mywishlist_first">{l s='Date de création' d='Shop.Theme'}</th>*}
                            <th scope="col" class="last_item mywishlist_first">{l s='Voir les boutiques' d='Shop.Theme'}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$wishlists item=wishlist}
                            <tr>
                                <td>{$wishlist.wording}</td>
{*                                <td>{date("d-M-Y à H:i:s")}</td>*}
                                <td>
                                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#{$wishlist.id_marketplace_wishlist}">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div id="{$wishlist.id_marketplace_wishlist}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">{l s='Voici les boutiques de la
                                                liste ' d='Shop.Theme'}  {$wishlist.wording}</h4>
                                        </div>

                                        <div class="modal-body">
                                            {foreach from=$wishlist.shop item=shop}
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="row text-center">
                                                            <div class="col-md-4">
                                                                {if isset($shop.shop_image)}
                                                                    <div class="mp-img-preview" style='border-radius: 100%;'>
                                                                        <img src="{$urls.base_url}/modules/marketplace/views/img/shop_img/{$shop.shop_image}" height='100px'/>
                                                                    </div>
                                                                {/if}
                                                            </div>
                                                            <div class="col-md-8">
                                                                <h4> {$shop.shop_name_unique}</h4>
                                                                <a href="{$link->getModuleLink('marketplace', 'shopstore', ['mp_shop_name'=>$shop.shop_name_unique])}" class="btn btn-link" title="{l s='Visit shop' mod='marketplace'} {$shop.shop_name_unique}">{l s='Visit shop' mod='marketplace'}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            {/foreach}
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        {/foreach}
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            {$paginationCode|unescape: 'html' nofilter}
                        </div>
                    </div>
                </div>
                <div id="block-order-detail">&nbsp;</div>
            {else}
                <div class="alert alert-info">
                    Vous n'avez aucune liste de boutiques pour le moment !
                </div>
            {/if}
        {/if}
        <ul class="footer_links">
            <li>
                <a href="{$link->getPageLink('my-account', true)}"></a>
                <a class="btn btn-default button button-small" href="{$link->getPageLink('my-account', true)|escape:'html'}">{l s='Back to Your Account' d='Shop.Theme'}</a>
            </li>
            <li class="f_right">
                <a href="#"></a>
                <a href="{$urls.pages.index}" class="btn btn-default button button-small">{l s='Home' d='Shop.Theme'}</a>
            </li>
        </ul>
    </div>
{/block}
