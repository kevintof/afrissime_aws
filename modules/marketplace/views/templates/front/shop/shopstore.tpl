{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{extends file=$layout}
{block name='content'}
    <div class="wk-mp-block">
        <div class="wk_shop_cover_container">
            {if isset($shop_banner_path)}
                <img class="wk_banner_image" src="{$shop_banner_path}" alt="{l s='Banner' mod='marketplace'}"/>
            {/if}
        </div>
        <div class="wk_profile_container">
            <div class="wk_shop_left">
                {block name='seller_image_block'}
                    {include file='module:marketplace/views/templates/front/_partials/seller-image-block.tpl'}
                {/block}
                {hook h='displayMpShopLeftColumn'}
            </div>

            <div class="wk-mp-contenu">
                {if Configuration::get('WK_MP_SHOW_SELLER_DETAILS')}
                    <div class="wk_profile_seller_name">
                        <h1 class="col-md-12 text-uppercase">
                            {$mp_seller_info.shop_name_unique}
                        </h1>
                        <div class="clearfix"></div>
                        {block name='mp_product_slider'}
                            {include file='module:marketplace/views/templates/front/seller/_partials/seller-details.tpl'}
                        {/block}
                    </div>
                    <div class="box-account">
                        <div class="box-content">
                            {if isset($WK_MP_SELLER_DETAILS_ACCESS_5) && isset($mp_seller_info.about_shop) && $mp_seller_info.about_shop}
                                <div class="wk_row">
                                    <label class="collection_label">{l s='About Shop -' mod='marketplace'}</label>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="wk_about_shop">
                                    {$mp_seller_info.about_shop nofilter}
                                </div>
                                <div calss='wk_row wk_seller_localisation'>
                                    <i class="fa fa-map-marker"></i><span> {if isset($mp_seller_info.country)}{$mp_seller_info.country} {/if} - {if isset($mp_seller_info.seller_town)} {$mp_seller_info.seller_town} {/if}</span>
                                </div>
                                {hook h="displayMpShopDetailsBottom"}
                            {/if}
                            <a href="#wk_question_form" class="wk_anchor_links open-question-form" data-toggle="modal"
                               data-target="#myModal" title="{l s='Contact Seller' mod='marketplace'}">
                                <div class="wk_profile_left_display btn btn-default btn-effect">
                                    <span>
                                        <i class="material-icons">&#xE0D0;</i> {l s='Contact Seller' mod='marketplace'}
                                    </span>
                                </div>
                            </a>
                            {block name='product_images_modal'}
                                {include file='module:marketplace/views/templates/front/_partials/contact-seller-form.tpl'}
                            {/block}
                            {hook h="displayExtraShopDetails"}
                            {if $isCustomerIsLogged}
                                <button class="wk_anchor_links open-question-form"
                                        data-toggle="modal"
                                        data-target="#mk_wishlistmodal"
                                        title="{l s='Ajouter à mes favoris' mod='marketplace'}">
                                    <div class="btn btn-primary btn-effect" style="border:none">
                                    <span>
                                        <i class="fa fa-heart-o"></i> {l s='Ajouter à mes favoris' mod='marketplace'}
                                    </span>
                                    </div>
                                </button>
                                {block name='mk_wishlist_form_block'}
                                    {include file='module:marketplace/views/templates/front/_partials/wishlist-form.tpl'}
                                {/block}
                            {/if}
                            {if isset($WK_MP_SELLER_DETAILS_ACCESS_1)}
                                <div class="wk_profile_seller_name">
                                    <h1 class="col-md-8 text-uppercase">
                                        {$mp_seller_info.seller_firstname} {$mp_seller_info.seller_lastname}
                                    </h1>
                                    <div class="clearfix"></div>
                                </div>
                            {/if}


                        </div>
                    </div>
                {/if}
            </div>

            <div class="clearfix"></div>
        </div>
        {if isset($WK_MP_SELLER_DETAILS_ACCESS_8)}
            {block name='shopcollection'}
                {include file='module:marketplace/views/templates/front/shop/_partials/shopcollection.tpl'}
            {/block}
        {/if}
    </div>
{/block}
