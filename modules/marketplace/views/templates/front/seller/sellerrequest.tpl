{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{extends file=$layout}
{block name="head" prepend}

{literal}
 <link href="{$smarty.const._MODULE_DIR_}marketplace/views/css/passwordscheck.css" rel="stylesheet" type="text/css"/>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/css/intlTelInput.css" rel="stylesheet" type="text/css"/>
<script>
// hack to get the plugin to work without loading jQuery
window.jQuery = window.$ = function() {
  return {
    on: function() {}
  };
};
window.$.fn = {};
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/js/intlTelInput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/js/utils.js"></script>
<script>
    $("#phone").intlTelInput({
	 initialCountry: "auto",
          geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
          utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.1.0/js/utils.js" // just for formatting/placeholders etc
    });
  </script> 

{/literal}
{/block}
{block name='content'}
<script type="text/javascript" src="{$smarty.const._MODULE_DIR_}marketplace/views/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="{$smarty.const._MODULE_DIR_}marketplace/views/js/tinymce/tinymce_wk_setup.js"></script>
<script type="text/javascript" src="{$smarty.const._MODULE_DIR_}marketplace/views/js/mpdelivery.js">
</script>
<script type="text/javascript" src="{$smarty.const._MODULE_DIR_}marketplace/views/js/passwordscheck.js">
</script>
{literal}
<script type="text/javascript">
function show_box_gender(){
  $('#box_gender').show();
}
function hide_box_gender(){
  $('#box_gender').hide();
}
function show_payment_box_1(){
	if ($("#virement").prop('checked')==true){ 
		$('#payment_box_1').show();
		$('#payment_box_2').hide();
		$('#payment_box_3').hide();
		$('#payment_box_4').hide();
		$('#payment_box_5').hide();
		$('#payment_box_6').hide();

	}
}
function show_payment_box_2(){
	if ($("#carte").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').show();
		$('#payment_box_3').hide();
		$('#payment_box_4').hide();
		$('#payment_box_5').hide();
		$('#payment_box_6').hide();
	}
}
function show_payment_box_3(){
	if ($("#mobile_money").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').hide();
		$('#payment_box_3').show();
		$('#payment_box_4').hide();
		$('#payment_box_5').hide();
		$('#payment_box_6').hide();
	}
	
}
function show_payment_box_4(){
	if ($("#cash").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').hide();
		$('#payment_box_3').hide();
		$('#payment_box_4').show();
		$('#payment_box_5').hide();
		$('#payment_box_6').hide();
	}
}
function show_payment_box_5(){
	if ($("#bitcoin").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').hide();
		$('#payment_box_3').hide();
		$('#payment_box_4').hide();
		$('#payment_box_5').show();
		$('#payment_box_6').hide();
	}
}
function show_payment_box_6(){
	if ($("#wallet").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').hide();
		$('#payment_box_3').hide();
		$('#payment_box_4').hide();
		$('#payment_box_5').hide();
		$('#payment_box_6').show();
	}
}
</script>
{/literal}
<input type="hidden" name="token" id="wk-static-token" value="{$static_token}">
{if isset($is_seller)}
	
		{if $is_seller == 0}
			<div class="alert alert-info">
			</div>
		{else}
			<div class="alert alert-info">
				{l s='Your request has been approved by admin. ' mod='marketplace'}
				<a href="{$link->getModuleLink('marketplace','addproduct')}">
					<button class="btn btn-primary">
					{if $language.iso_code=='fr'} Ajouter votre premier produit{/if}
					{if $language.iso_code=='en'} Add Your First Product{/if}
					</button>
				</a>
			</div>
		{/if}
{else}
	<div class="wk-mp-block" style="border:1px solid #d5d5d5;">
		<div class="page-title" style="background-color:{$title_bg_color};">
			<span style="color:{$title_text_color};">{l s='Seller Request' mod='marketplace'}</span>
		</div>
		{if isset($signup)}
			{if $language.iso_code=='fr'}
				<p class="alert alert-success" style="margin:30px;">Votre boutique a été crée avec succès. Vous pouvez vous connecter à votre espace personnel pour administrer votre boutique.</p>
			{/if}
			{if $language.iso_code=='en'}
				<p class="alert alert-success" style="margin:30px;">Your shop has been successfully created. You can login to your personal space to administer your shop.</p>
			{/if}
		{/if}
		<form action="{$link->getModuleLink('marketplace', 'sellerrequest')}" method="post" id="wk_mp_seller_form" enctype="multipart/form-data">
			<div class="wk-mp-right-column">
				<div class="alert alert-danger wk_display_none" id="wk_mp_form_error"></div>
				<!-- progressbar -->
				{if $language.iso_code=='fr'}
		            <ul id="progressbar">
		                <li class="active">Renseignements personnels</li>
		                <li>Renseignements boutique</li>
		                <li>Conditions générales de vente</li>
		            </ul>
	            {/if}
	            {if $language.iso_code=='en'}
		            <ul id="progressbar">
		                <li class="active">Personal information</li>
		                <li>Shop information</li>
		                <li>Terms of Sales</li>
		            </ul>
	            {/if}
				<fieldset>
					{if $language.iso_code=='fr'}
						<h2 class="fs-title">Renseignements personnels</h2>
                		<h3 class="fs-subtitle"> Parlez nous un peu de vous</h3>
					{/if}
					{if $language.iso_code=='en'}
						<h2 class="fs-title">Personal information</h2>
                		<h3 class="fs-subtitle"> Tell us a little about yourself</h3>
					{/if}
					<input type="hidden" name="current_lang" id="current_lang" value="{$current_lang.id_lang}">
				
					<input type="hidden" name="default_lang" value="{$context_language}" />
					
					<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_lastname" class="control-label required">
								{l s='Last Name' mod='marketplace'}
							</label>
							<input class="form-control"
							type="text"
							value="{if isset($smarty.post.seller_lastname)}{$smarty.post.seller_lastname}{else}{$customer_lastname}{/if}"
							name="seller_lastname"
							id="seller_lastname" />
						</div>
						<div class="col-md-6">
							<label for="seller_firstname" class="control-label required">
								{l s='First Name' mod='marketplace'}
							</label>
							<input class="form-control"
							type="text"
							value="{if isset($smarty.post.seller_firstname)}{$smarty.post.seller_firstname}{else}{$customer_firstname}{/if}"
							name="seller_firstname"
							id="seller_firstname" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<div class="row">
								<label for="seller_gender" class="control-label required">
									{if $language.iso_code=='fr'}Sexe{/if} {if $language.iso_code=='en'}Sex{/if} 
								</label>
							</div>
							<div class="row">
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="1" checked onclick="hide_box_gender();">
						              <span></span>
						            </span>
						            {if $language.iso_code=='fr'}Homme{/if} 
						          	{if $language.iso_code=='en'}Man{/if} 
						        </label>
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="2" onclick="hide_box_gender();">
						              <span></span>
						            </span>
						            {if $language.iso_code=='fr'}Femme{/if} 
						          	{if $language.iso_code=='en'}Woman{/if} 
						        </label>
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="3" onclick="show_box_gender();">
						              <span></span>
						            </span>
						            {if $language.iso_code=='fr'}Personnalisé{/if} 
						          	{if $language.iso_code=='en'}Gender neutral{/if} 
						        </label>
					        </div>
					        <div id="box_gender" style="display:none">
					        	<div class="row">
					        		{if $language.iso_code=='fr'}
					        			<span>Indiquer votre sexe </span>
					        			<p>Me désigner avec le pronom suivant:</p>
					        		{/if} 
					        		{if $language.iso_code=='en'}
					        			<span>Please indicate your gender </span>
					        			<p>Designate me with the following pronoun:</p>
					        		{/if} 
					        		 <input class="form-control" name="seller_title" type="text" value="" placeholder="{if $language.iso_code=='fr'}Pronom{/if}{if $language.iso_code=='en'}Pronoum{/if}">
					        	</div>
					        </div>
					    </div>
					    <div class="col-md-6">
					    	<label for="seller_birthday" class="control-label">
								{if $language.iso_code=='fr'}Date d'anniversaire{/if} 
								{if $language.iso_code=='en'}Bithdate{/if} 
							</label>
					        <input class="form-control" name="seller_birthday" type="text" value="" placeholder="{if $language.iso_code=='fr'}DD/MM/YYYY{/if} {if $language.iso_code=='en'}YYYY/MM/DD{/if}" >
					        <span class="form-control-comment">
					            {if $language.iso_code=='fr'} (Ex. : 31/05/1970){/if}{if $language.iso_code=='en'} (Ex. : 1970/05/31){/if}
					        </span>
					    </div>
				    </div>
			    	<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_nationality" class="control-label required">
								{if $language.iso_code=='fr'}Nationalité{/if}
								{if $language.iso_code=='en'}Nationality{/if}
							</label>
							<select name="seller_nationality" id="seller_nationality" class="form-control form-control-select"  style="height:40px; width:100%">
								<option value="">{l s='Select Country' mod='marketplace'}</option>
								{foreach $african_country as $countrydetail}
									<option value="{$countrydetail.id_country}">
										{$countrydetail.name}
									</option>
								{/foreach}
							</select>
						</div>
						<div class="col-md-6">
							<label for="residence" class="control-label required">
								{if $language.iso_code=='fr'}Pays de résidence{/if}
								{if $language.iso_code=='en'}Country of residence{/if}
							</label>
							<select name="residence" id="residence"  class="form-control form-control-select" style="height:40px; width:100%">
								<option value="">{l s='Select Country' mod='marketplace'}</option>
								{foreach $country as $countrydetail}
									<option value="{$countrydetail.id_country}">
										{$countrydetail.name}
									</option>
								{/foreach}
							</select>
						</div>
					</div>
				    <div class="form-group row">
						<div class="col-md-6">
							<label for="customer_email" class="control-label required">
								{l s='Email' mod='marketplace'}
							</label>
							<input class="form-control"
							type="email"
							value="{$customer_email}"
							name="customer_email"
							id="customer_email"
							/>
							<span class="wk-msg-customer_email"></span>
						</div>
						<div class="col-md-6">
							<label for="phone" class="control-label required">
								{if $language.iso_code=='fr'}Numéro Téléphone{/if}
								{if $language.iso_code=='en'}Phone number{/if}
							</label>
							<input class="form-control"
							type="text"
							value=""
							name="phone"
							id="phone"
							maxlength="{$max_phone_digit}"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_password" class="control-label required">
								{if $language.iso_code=='fr'}Mot de passe{/if}
								{if $language.iso_code=='en'}Password{/if}
								
							</label>
							<input class="form-control"
							type="password"
							value=""
							name="seller_password"
							id="seller_password"
							data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true"
							/>
							<div id="popover-password-top" class="hide  block-help"><i class="fa fa-info-circle text-danger" aria-hidden="true"></i> Saisissez un mot de passe avec un niveau de sécurité élevé</div>
							<div id="popover-password">
                                <p>Niveau de sécurité du mot de passe : <span id="result"> </span></p>
                                <div class="progress">
                                    <div id="password-strength" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                    </div>
                                </div>
                            </div>
							<span  class="wk-msg-customerpassword"></span>
						</div>
						<div class="col-md-6">
							<label for="customer_confpassword" class="control-label required">
								{if $language.iso_code=='fr'}Confirmation du mot de passe{/if}
								{if $language.iso_code=='en'}Password confirmation{/if}
							</label>
							<input class="form-control"
							type="password"
							value=""
							name="seller_confpassword"
							id="seller_confpassword"
							 />
							 <div id="popover-cpassword" class="hide block-help"><i class="fa fa-info-circle text-danger" aria-hidden="true"></i> Les mots de passe ne sont pas identiques</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<label for="seller_address" class="control-label required">
								{if $language.iso_code=='fr'}Adresse{/if}
								{if $language.iso_code=='en'}Address{/if}

							</label>
						</div>
						<textarea class="form-control" name="address"></textarea>
					</div>
					{if $seller_country_need}
						<div class="form-group row">
							<div class="col-md-6" id="seller_zipcode">
								<label for="postcode" class="control-label">
									{l s='Zip/Postal Code' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.postcode)}{$smarty.post.postcode|escape:'htmlall':'UTF-8'}{/if}"
								name="postcode"
								id="postcode" />
							</div>
							<div class="col-md-6">
								<label for="city" class="control-label required">
									{l s='City' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.city)}{$smarty.post.city|escape:'htmlall':'UTF-8'}{/if}"
								name="city"
								id="city"
								maxlength="64" />
							</div>
						</div>
					{/if}
					{if $tax_identification_number}
						<div class="form-group row">
							<div class="col-md-6" id="seller_tax_identification_number">
								<label for="tax_identification_number" class="control-label">
									{l s='Tax Identification Number/VAT' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.tax_identification_number)}{$smarty.post.tax_identification_number|escape:'htmlall':'UTF-8'}{/if}"
								name="tax_identification_number"
								id="tax_identification_number"
								maxlength="64" />
							</div>
						</div>
					{/if}
					

					{hook h="displayMpSellerRequestFooter"}
					{hook h='displayGDPRConsent' mod='psgdpr' id_module=$id_module}

					{block name='mp-form-fields-notification'}
						{include file='module:marketplace/views/templates/front/_partials/mp-form-fields-notification.tpl'}
					{/block}
					<input type="button" name="next" class="next action-button" value="{if $language.iso_code=='fr'}Suivant{/if} {if $language.iso_code=='en'}Next{/if}"/>
				</fieldset>
				<fieldset>
					  <h2 class="fs-title">{if $language.iso_code=='fr'}Renseignements boutique{/if} {if $language.iso_code=='en'}Shop information{/if}</h2>
					  	{if $language.iso_code=='fr'}
                			<h3 class="fs-subtitle">Donnez des informations sur votre boutique</h3>
                		{/if}
                		{if $language.iso_code=='en'}
                			<h3 class="fs-subtitle">Give information about your shop</h3>
                		{/if}
                		<div class="form-group row">
                			 <div class="avatar-upload">
						        <div class="avatar-edit">
						            <input type='file' id="imageUpload" name="shopimage[]" accept=".png, .jpg, .jpeg" />
						            <label for="imageUpload"></label>
						        </div>
						        <div class="avatar-preview">
						            <div id="imagePreview" style="background-image: url(https://www.afrissime.com/modules/marketplace/views/img/shop_img/defaultshopimage.jpg?time=1570007517);">
						            </div>
						        </div>
						    </div>
                		</div>
                		
	                	<div class="form-group row">
		                	<div class="col-md-6">
		                		<div class="form-group seller_shop_name_uniq">
									<label for="shop_name_unique" class="control-label required">
										{l s='Shop Name' mod='marketplace'}
										<div class="wk_tooltip">
											{if $language.iso_code=='fr'}
												<span class="wk_tooltiptext">Ce nom sera utilisé dans l'URL de votre boutique.</span>
											{/if}
											{if $language.iso_code=='en'}
												<span class="wk_tooltiptext">This name will be used in your shop URL</span>
											{/if}
										</div>
									</label>
									<img style="display: none;" width="25" src="{$modules_dir}marketplace/views/img/loading-small.gif" class="seller-loading-img"/>
									<input class="form-control"
										type="text"
										value="{if isset($smarty.post.shop_name_unique)}{$smarty.post.shop_name_unique}{/if}"
										id="shop_name_unique"
										name="shop_name_unique"
										onblur="onblurCheckUniqueshop();"
										autocomplete="off" />
									<span class="wk-msg-shopnameunique"></span>
									<p><span class="wk-msg-domaineunique"></span><p>
								</div>
							</div>
							<div class="col-md-6">
								<span style="position:relative; right:210px;top:30px;">.afrissime.com</span>
							</div>
						</div>
						
						<div class="form-group row">
							<div class="col-md-12">
								<label for="about_shop" class="control-label required">
									{if $language.iso_code=='fr'}Description de la boutique{/if}
									{if $language.iso_code=='en'}Description of the shop{/if}
								</label>
							</div>
							<textarea name="about_shop_1" class="form-control" placeholder=""></textarea>
						</div>
						<div style="width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;height:300px;background-image: url(https://www.afrissime.com/modules/marketplace/views/img/shop_banner/default_cover_pageshop.jpg);" id="cover_shop">
							<div style="position:relative; top:160px;">
								<h3 style="text-align:center;padding: 10px;background-color: #fff;opacity: 0.7;">{if $language.iso_code=='fr'}Image de couverture boutique{/if} {if $language.iso_code=='en'}Cover image shop{/if}</h3>
								<div class="input-group" style="margin: 0 auto;"> 
								    <div class="custom-file">
									    <input type="file" name="profilebannerimage[]" id="inputGroupFile01" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
									 </div>
						      	</div>
					      	</div>
				      	</div>
				      <div class="form-group row">
							<div class="col-md-6">
								<label for="business_email" class="control-label required">
								{if $language.iso_code=='fr'}Email boutique{/if}
								{if $language.iso_code=='en'}Business Email{/if}
								</label>
								<input class="form-control"
								type="email"
								value="{if isset($smarty.post.business_email)}{$smarty.post.business_email}{else}{$customer_email}{/if}"
								name="business_email"
								id="business_email"
								onblur="onblurCheckUniqueSellerEmail();" />
								<span class="wk-msg-selleremail"></span>
								<span>{if $language.iso_code=='fr'}Pour recevoir un email à chaque vente{/if}{if $language.iso_code=='en'}To receive an email with each sale{/if}</span>
							</div>
							<div class="col-md-6">
								<label for="phone_shop" class="control-label required">
									{l s='Phone' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.phone)}{$smarty.post.phone}{/if}"
								name="phone_shop"
								id="phone_shop"
								maxlength="{$max_phone_digit}" />
								<span>{if $language.iso_code=='fr'}Pour recevoir un SMS à chaque vente{/if} {if $language.iso_code=='en'}To receive an SMS for every sale{/if}</span>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="id_card" class="control-label required">
									{if $language.iso_code=='fr'}
										Pièce d'identité du titulaire de la boutique
									{/if}
									{if $language.iso_code=='en'}
										ID card of the shop owner
									{/if}
								</label>
								<input type="file" name="id_card"/>
							</div>
							<div class="col-md-6">
								<label for="payment_method" class="control-label">
									{if $language.iso_code=='fr'}
										Canal de versement des paiements
									{/if}
									{if $language.iso_code=='en'}
										How do you want to be paid?
									{/if}
								</label>
								{if $language.iso_code=='fr'}
									<p>Choisissez parmi les moyens de paiement proposés et renseignez les différentes informations.</p>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="1" style="width:10%; padding-right:10px" id="virement" onclick="show_payment_box_1();"/> Virement bancaire
										</div>
										<div id="payment_box_1"style="display:none">
											<p style="text-align:center; font-size:14px;">{l s='Informations bancaires' mod='marketplace'}</p>
											<div class="col-md-6">
												<label for="bank_name" class="control-label required">
												Nom de la banque
												</label>
												<input type="text" name="bank_name" class="form-control" value="" placeholder="Nom de la banque"/>
											</div>
											<div class="col-md-6">
												<label for="rib" class="control-label required">
												RIB
												</label>
												<input type="text" name="rib" class="form-control" value="" placeholder="RIB"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="2" style="width:10%; padding-right:10px" id="carte" onclick="show_payment_box_2();"/> Carte de débit ou de crédit
										</div>
										<div id="payment_box_2" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Informations de la carte Visa ou Mastercard </p>
											<div class="col-md-12">
												<label for="name_card" class="control-label required">
												Nom du titulaire de la carte
												</label>
												<input type="text" name="name_card" class="form-control" placeholder="Nom du titulaire de la carte" value=""/>
											</div>
											<div class="col-md-12">
												<div class="col-md-9">
													<label for="num_card" class="control-label required">
													Numéro de la carte
													</label>
													<input type="text" name="num_card" class="form-control" placeholder="Ex: 4000 1234 5678 9101" value=""/>
												</div>
												<div class="col-md-3">
													<label for="card_expiration" class="control-label required">
													Expire le
													</label>
													<input type="text" name="card_expiration" class="form-control" placeholder=" Ex: 12/21" value=""/>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="3" style="width:10%; padding-right:10px" id='mobile_money' onclick="show_payment_box_3();"/> Mobile money
										</div>
										<div id="payment_box_3" {if isset($edit) && ($seller_payment_details.payment_mode_id == 3)} style="" {else} style="display:none" {/if} class="col-md-12">
											<p style="text-align:center; font-size:14px;">Numéro pour le débit ou le crédit</p>
											<div class="col-md-6">
												<label for="num_mobile" class="control-label required">
												Numéro
												</label>
												<input type="text" name="num_mobile" class="form-control" placeholder="Numéro" value=""/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="4" style="width:10%; padding-right:10px;" id='cash' onclick="show_payment_box_4();"/> Cash 
										</div>
										<div id="payment_box_4" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Numéro pour vous communiquer les informations des transactions</p>
											<div class='col-md-12'>
												<div class="col-md-6">
													<label for="num_cash" class="control-label required">
													Numéro
													</label>
													<input type="text" name="num_cash" class="form-control" placeholder="Numéro" value=""/>
												</div>
												<div class="col-md-6">
													<div class="col-md-12">
														<label for="operator" class="control-label required">
														Choisir un opérateur de transfert d'argent
														</label>
													</div>
													<div class="col-md-4">
														<input type="radio" name="operator" class='form-control'  value="1" checked/> <span style="text-align:center">Western Union</span>
													</div>
													<div class="col-md-4">
														<input type="radio" name="operator" class='form-control'  value="2"/> <span style="text-align:center">Moneygram</span>
													</div>
													<div class='col-md-4'>
														<input type="radio" name="operator" class='form-control' value="3"/> <span style="text-align:center">Express Union</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="5" style="width:10%" id="bitcoin" onclick="show_payment_box_5();"/> Bitcoin 
										</div>
										<div id="payment_box_5" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Informations sur le portefeuille</p>
											<div class="col-md-12">
												<div class='col-md-6'>
													<label for="bitcoin_address" class="control-label required">
													Adresse
													</label>
													<input type="text" name="bitcoin_address" class="form-control" placeholder="Adresse" value=""/>
												</div>
												<div class='col-md-6'>
													<label for="nickname_bitcoin" class="control-label required">
													Pseudo
													</label>
													<input type="text" name="nickname_bitcoin" class="form-control" placeholder="Pseudo" value=""/>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="6" style="width:10%" id="wallet" onclick="show_payment_box_6();"/> Bank wallet 
										</div>
										<div id="payment_box_6" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Numéro du portefeuille mobile</p>
											<div class="col-md-12">
												<div class='col-md-6'>
													<label for="wallet_numero" class="control-label required">
													Numéro
													</label>
													<input type="text" name="wallet_number" class="form-control" placeholder="Numéro" value=""/>
												</div>
											</div>
										</div>
									</div>
								{/if}
								{if $language.iso_code=='en'}
									<p>Choose from the payment methods offered and fill in the various information.</p>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="1" style="width:10%; padding-right:10px" id="virement" onclick="show_payment_box_1();"/> Bank transfer
										</div>
										<div id="payment_box_1"style="display:none">
											<p style="text-align:center; font-size:14px;">{l s='Informations bancaires' mod='marketplace'}</p>
											<div class="col-md-6">
												<label for="bank_name" class="control-label required">
												Bank name
												</label>
												<input type="text" name="bank_name" class="form-control" value="" placeholder="Bank name"/>
											</div>
											<div class="col-md-6">
												<label for="rib" class="control-label required">
												IBAN / BIC
												</label>
												<input type="text" name="rib" class="form-control" value="" placeholder="IBAN / BIC"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="2" style="width:10%; padding-right:10px" id="carte" onclick="show_payment_box_2();"/> Debit or credit card
										</div>
										<div id="payment_box_2" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Visa or MasterCard information</p>
											<div class="col-md-12">
												<label for="name_card" class="control-label required">
												Name of the card holder
												</label>
												<input type="text" name="name_card" class="form-control" placeholder="Name of the card holder" value=""/>
											</div>
											<div class="col-md-12">
												<div class="col-md-9">
													<label for="num_card" class="control-label required">
													Number of the credit card
													</label>
													<input type="text" name="num_card" class="form-control" placeholder="Ex: 4000 1234 5678 9101" value=""/>
												</div>
												<div class="col-md-3">
													<label for="card_expiration" class="control-label required">
													Expires end
													</label>
													<input type="text" name="card_expiration" class="form-control" placeholder=" Ex: 12/21" value=""/>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="3" style="width:10%; padding-right:10px" id='mobile_money' onclick="show_payment_box_3();"/> Mobile money
										</div>
										<div id="payment_box_3" {if isset($edit) && ($seller_payment_details.payment_mode_id == 3)} style="" {else} style="display:none" {/if} class="col-md-12">
											<p style="text-align:center; font-size:14px;">Numéro pour le débit ou le crédit</p>
											<div class="col-md-6">
												<label for="num_mobile" class="control-label required">
												Phone number
												</label>
												<input type="text" name="num_mobile" class="form-control" placeholder="Phone number" value=""/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="4" style="width:10%; padding-right:10px;" id='cash' onclick="show_payment_box_4();"/> Cash 
										</div>
										<div id="payment_box_4" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Phone number to communicate transaction information</p>
											<div class='col-md-12'>
												<div class="col-md-6">
													<label for="num_cash" class="control-label required">
													Phone number
													</label>
													<input type="text" name="num_cash" class="form-control" placeholder="Phone number" value=""/>
												</div>
												<div class="col-md-6">
													<div class="col-md-12">
														<label for="operator" class="control-label required">
														Choose a money transfer operator
														</label>
													</div>
													<div class="col-md-4">
														<input type="radio" name="operator" class='form-control'  value="1" checked/> <span style="text-align:center">Western Union</span>
													</div>
													<div class="col-md-4">
														<input type="radio" name="operator" class='form-control'  value="2"/> <span style="text-align:center">Moneygram</span>
													</div>
													<div class='col-md-4'>
														<input type="radio" name="operator" class='form-control' value="3"/> <span style="text-align:center">Express Union</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="5" style="width:10%" id="bitcoin" onclick="show_payment_box_5();"/> Bitcoin 
										</div>
										<div id="payment_box_5" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Wallet Information</p>
											<div class="col-md-12">
												<div class='col-md-6'>
													<label for="bitcoin_address" class="control-label required">
													Adresse
													</label>
													<input type="text" name="bitcoin_address" class="form-control" placeholder="Address" value=""/>
												</div>
												<div class='col-md-6'>
													<label for="nickname_bitcoin" class="control-label required">
													Nickname
													</label>
													<input type="text" name="nickname_bitcoin" class="form-control" placeholder="Nickname" value=""/>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="6" style="width:10%" id="wallet" onclick="show_payment_box_6();"/> Bank wallet 
										</div>
										<div id="payment_box_6" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Mobile wallet number</p>
											<div class="col-md-12">
												<div class='col-md-6'>
													<label for="wallet_numero" class="control-label required">
													Numéro
													</label>
													<input type="text" name="wallet_number" class="form-control" placeholder="Mobile wallet number" value=""/>
												</div>
											</div>
										</div>
									</div>
								{/if}
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="id_country" class="control-label required">
								{if $language.iso_code=='fr'}Localisation de votre boutique {/if}
								{if $language.iso_code=='en'}Location of your shop {/if}
								</label>
								<select name="id_country" id="id_country" class="form-control form-control-select"  style="height:40px; width:100%">
									<option value="">{l s='Select Country' mod='marketplace'}</option>
									{foreach $country as $countrydetail}
										<option value="{$countrydetail.id_country}">
											{$countrydetail.name}
										</option>
									{/foreach}
								</select>
								<span class="wk-msg-sellerlocalisation"></span>
							</div>
							<div class="col-md-6">
								<div id="wk_seller_state_div" class="col-md-6">
									<label for="id_state" class="control-label required">
										{l s='State' mod='marketplace'}
									</label>
									<select name="id_state" id="id_state" class="form-control form-control-select">
										<option value="">{l s='Select State' mod='marketplace'}</option>
									</select>
									<input type="hidden" name="state_available" id="state_available" value="0" />
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="seller_town" class="control-label required">
									{if $language.iso_code=='fr'}Ville{/if}
									{if $language.iso_code=='en'}City{/if}
								</label>
								<input class="form-control"
								type="text"
								value=""
								name="seller_town"
								id="seller_town"
								maxlength="" />
								<span class="wk-msg-sellertown"></span>
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="seller_district" class="control-label required">
										{if $language.iso_code=='fr'}Quartier{/if}
										{if $language.iso_code=='en'}Street/ district{/if}
									</label>
									<input class="form-control"
									type="text"
									value=""
									name="seller_district"
									id="seller_district"
									maxlength="" />
									<span class="wk-msg-sellerdistrict"></span>
								</div>
								<div class="col-md-6">
									<label for="seller_postalcode" class="control-label required">
										{if $language.iso_code=='fr'}Adresse postale{/if}
										{if $language.iso_code=='en'}PO Box{/if}
									</label>
									<input class="form-control"
									type="text"
									value=""
									name="seller_postalcode"
									id="seller_postalcode"
									maxlength="" />
									<span class="wk-msg-sellerpostalcode"></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<h3>{if $language.iso_code=='fr'}Détails de livraison{/if} {if $language.iso_code=='en'}Delivery Details{/if}</h3>
						</div>
						<div class="container">
							<div class="col-md-3" style="float:right;">
				                <input type="button" class="" id="addrow" value="{l s='Ajouter' mod='marketplace'}" />
				            </div>
						    <table id="myTable" class=" table order-list">
							    <thead>
							        <tr>
							            <td>{if $language.iso_code=='fr'}Transporteur{/if} {if $language.iso_code=='en'}Carrier{/if}</td>
							            <td>{if $language.iso_code=='fr'}Voie{/if} {if $language.iso_code=='en'}Delivery method{/if} </td>
							            <td>{if $language.iso_code=='fr'}Durée{/if} {if $language.iso_code=='en'}Duration{/if}</td>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <td class="col-sm-4">
							                <input class="form-control"
											type="text"
											value=""
											name="transporter[]"
											id="transporter"
											maxlength="" />
											<span class="wk-msg-transporter"></span>
							            </td>
							            <td class="col-sm-4">
							            	{if $language.iso_code=='fr'}
								                <select name="delivery_way[]" class="form-control form-control-select">
								                	<option value="0">Choisir une voie</option>
													<option value="1">Maritime</option>
													<option value="2">Aérienne</option>
													<option value="3">Terrestre</option>
												</select>
											{/if} 
											{if $language.iso_code=='en'}
								                <select name="delivery_way[]" class="form-control form-control-select">
								                	<option value="0">Choose delivery method</option>
													<option value="1">Shipping</option>
													<option value="2">Air transport</option>
													<option value="3">By road</option>
												</select>
											{/if}
											<span class="wk-msg-delivery_way"></span>
							            </td>
							            <td class="col-sm-4">
							               <input class="form-control"
											type="text"
											value=""
											name="duration[]"
											id="duration"
											placeholder="{if $language.iso_code=='fr'}Ex: 2 à 4 jours{/if}{if $language.iso_code=='en'}Ex: from 2 to 4 days{/if}" />
							            </td>
							            <td class="col-sm-2"><a class="deleteRow"></a>

							            </td>
							        </tr>
							    </tbody>
							</table>
						</div>
						 <input type="button" name="previous" class="previous action-button-previous" value="{if $language.iso_code=='fr'}Précédent{/if} {if $language.iso_code=='en'}Previous{/if}"/>
                		<input type="button" name="next" class="next action-button" value="{if $language.iso_code=='fr'}Suivant{/if} {if $language.iso_code=='en'}Next{/if}"/>
				</fieldset>
				<fieldset>
					<h2 class="fs-title">{if $language.iso_code=='fr'} Conditions générales de vente{/if} {if $language.iso_code=='en'} Termes of sale{/if}</h2>
						{if $language.iso_code=='fr'}
                			<h3 class="fs-subtitle">En savoir plus sur les conditions générales de vente</h3>
                		{/if}
                		{if $language.iso_code=='en'}
                			<h3 class="fs-subtitle">Learn more about the terms of sale</h3>
                		{/if}
                		<p style="text-align:justify">{if $language.iso_code=='fr'}Cliquez sur le lien "Termes et conditions" afin de prendre connaissance des conditions générales de vente. En cochant l'option "Je suis d'accord avec les termes et conditions", vous confirmez avoir lu et approuvé le contrat. {/if}
                		{if $language.iso_code=='en'}Click on the link "Terms and conditions" to read the terms and conditions of sale. By checking "I agree with the terms and conditions", you confirm that you have read and approved the contract. {/if}</p>
                		{if $terms_and_condition_active}
							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" style="width:10px;"/>
										<span>
										{if $language.iso_code=='fr'}
											Je suis d'accord avec les
										{/if}
										{if $language.iso_code=='en'}
											I agree with the
										{/if}
											{if isset($linkCmsPageContent)}
												<a href="{$linkCmsPageContent}" class="wk_terms_link">
													{l s='terms and condition' mod='marketplace'}
												</a>
											{else}
												{l s='terms and condition' mod='marketplace'}
											{/if}
											{if $language.iso_code=='fr'}
												et j'y adhère inconditionnellement.
											{/if}
											{if $language.iso_code=='en'}
												and will adhere to them unconditionally.
											{/if}
										</span>
									</label>
								</div>
							</div>
							{if isset($linkCmsPageContent)}
								<div class="modal fade" id="wk_terms_condtion_div" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div id="wk_terms_condtion_content" class="modal-body"></div>
										</div>
									</div>
								</div>
							{/if}
						{/if}
								<input type="button" name="previous" class="previous action-button-previous" value="{if $language.iso_code=='fr'}Précédent{/if} {if $language.iso_code=='en'}Previous{/if}"/>
							<div data-action="{l s='Register' mod='marketplace'}" id="wk-seller-submit" style="width: 120px;display:inline">
								<img class="wk_product_loader" src="{$modules_dir}marketplace/views/img/loader.gif" width="25" />
								<input type="submit" name="sellerRequest" id="sellerRequest" class="action-button" value="{if $language.iso_code=='fr'}Enregistrer{/if} {if $language.iso_code=='en'}Save{/if}"/>
							</div>
				</fieldset>
			</div>
		</form>
	</div>
{/if}
{/block}