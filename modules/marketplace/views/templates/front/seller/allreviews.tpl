{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{extends file=$layout}
{block name='content'}
{if Configuration::get('WK_MP_REVIEW_SETTINGS')}
	<div class="wk-mp-block">
		<div class="wk_shop_cover_container">
			{if isset($shop_banner_path)}
				<img class="wk_banner_image" src="{$shop_banner_path}" alt="{l s='Banner' mod='marketplace'}"/>
			{/if}
		</div>
		<div class="wk_profile_container">
			<div class="wk_shop_left">
				{block name='seller_image_block'}
					{include file='module:marketplace/views/templates/front/_partials/seller-image-block.tpl'}
				{/block}
				{hook h='displayMpShopLeftColumn'}
			</div>
			<div class="wk-mp-contenu">
				{if Configuration::get('WK_MP_SHOW_SELLER_DETAILS')}
					<div class="wk_profile_seller_name">
						<h1 class="col-md-12 text-uppercase">
							{$mp_seller_info.shop_name}
						</h1>
						<div class="clearfix"></div>
						{block name='mp_product_slider'}
							{include file='module:marketplace/views/templates/front/seller/_partials/seller-details.tpl'}
						{/block}
					</div>
				{/if}
				<div class="box-account">
					<div class="box-content" style="margin-top:0px;">
						{if isset($WK_MP_SELLER_DETAILS_ACCESS_5) && isset($mp_seller_info.about_shop) && $mp_seller_info.about_shop}
							<div class="wk_row">
								<label class="collection_label">{l s='About Shop -' mod='marketplace'}</label>
								<div class="clearfix"></div>
							</div>
							<div class="wk_about_shop">
								{$mp_seller_info.about_shop nofilter}
							</div>

							{hook h="displayMpShopDetailsBottom"}
						{/if}
						{hook h="displayExtraShopDetails"}
						{if isset($WK_MP_SELLER_DETAILS_ACCESS_7)}
								<a href="#wk_question_form" class="wk_anchor_links open-question-form" data-toggle="modal" data-target="#myModal" title="{l s='Contact Seller' mod='marketplace'}">
									<div class="wk_profile_left_display btn btn-default btn-effect">
										<span>
											<i class="material-icons">&#xE0D0;</i> {l s='Contact Seller' mod='marketplace'}
										</span>
									</div>
								</a>
							</div>
							<div class="clearfix"></div>
							{block name='product_images_modal'}
								{include file='module:marketplace/views/templates/front/_partials/contact-seller-form.tpl'}
						    {/block}
						{/if}
						{if isset($WK_MP_SELLER_DETAILS_ACCESS_1)}
							<div class="wk_profile_seller_name">
								<h1 class="col-md-8 text-uppercase">
									{$mp_seller_info.seller_firstname} {$mp_seller_info.seller_lastname}
								</h1>
								<div class="clearfix"></div>
							</div>
						{/if}

						
					</div>
					<div class="wk_profile_container" style="padding:20px;">
						{if isset($reviews_info)}
							<div class="wk-allreviews-page-seller-rating">
								{block name='mp-seller-rating-summary'}
									{include file='module:marketplace/views/templates/front/seller/_partials/seller-rating-summary.tpl'}
								{/block}
							</div>
							<h3 style="color:{$title_text_color};">{l s='All Reviews' mod='marketplace'} {if isset($nbReviews)}({$nbReviews}){/if}</h3>
							{foreach from=$reviews_info item=review}
								{block name='mp-seller-review-list'}
									{include file='module:marketplace/views/templates/front/seller/_partials/seller-review-list.tpl'}
								{/block}
							{/foreach}
							<div class="wk-pagination-right">
								<div class="col-sm-12">
									<ul class="pagination pagination-sm">
										<li class="page-item">
											<a class="page-link {if $p == 1}wk-disabled{/if}" {if $p > 1}href="{$link->getModuleLink('marketplace', 'allreviews', ['mp_shop_name' => $shopLinkRewrite, 'p' => $p-1])}"{/if} aria-label="Previous">
												<span aria-hidden="true">{l s='Previous' mod='marketplace'}</span>
											</a>
										</li>
										{for $i = 1 to $page_count}
											<li class="page-item">
												<a class="page-link {if $p == $i}wk-page-active{/if}" {if $p != $i}href="{$link->getModuleLink('marketplace', 'allreviews', ['mp_shop_name' => $shopLinkRewrite, 'p' => $i])}"{/if}>{$i}</a>
											</li>
										{/for}
										<li class="page-item">
											<a class="page-link {if $p == $page_count}wk-disabled{/if}" {if $p < $page_count}href="{$link->getModuleLink('marketplace', 'allreviews', ['mp_shop_name' => $shopLinkRewrite, 'p' => $p+1])}"{/if} aria-label="Next">
												<span aria-hidden="true">{l s='Next' mod='marketplace'}</span>
											</a>
										</li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						{else}
							<p>{l s='No reviews available' mod='marketplace'}</p>
						{/if}
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
{/if}
{/block}