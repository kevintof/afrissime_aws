{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<input type="hidden" name="default_lang" value="{$mp_seller_info.default_lang}" />
<div class="form-group row">
	<div class="col-md-6">
		<label for="seller_lastname" class="control-label required">
			{l s='Last Name' mod='marketplace'}
		</label>
		<input class="form-control"
		type="text"
		value="{if isset($smarty.post.seller_lastname)}{$smarty.post.seller_lastname}{else}{$mp_seller_info.seller_lastname}{/if}"
		name="seller_lastname"
		id="seller_lastname" />
	</div>
	<div class="col-md-6">
		<label for="seller_firstname" class="control-label required">
			{l s='First Name' mod='marketplace'}
		</label>
		<input class="form-control"
		type="text"
		value="{if isset($smarty.post.seller_firstname)}{$smarty.post.seller_firstname}{else}{$mp_seller_info.seller_firstname}{/if}"
		name="seller_firstname"
		id="seller_firstname" />
	</div>
</div>
<div class="form-group row">
	<div class="col-md-6">
		<div class="row">
			<label for="seller_gender" class="control-label required">
				{l s='Sexe' mod='marketplace'}
			</label>
		</div>
		<div class="row">
	        <label class="radio-inline">
	            <span class="custom-radio">
	              <input name="seller_gender" type="radio" value="1" {if $mp_customer_id_gender==1}checked {/if} onclick="hide_box_gender();">
	              <span></span>
	            </span>
	          Homme
	        </label>
	        <label class="radio-inline">
	            <span class="custom-radio">
	              <input name="seller_gender" type="radio" value="2" {if $mp_customer_id_gender==2}checked {/if} onclick="hide_box_gender();">
	              <span></span>
	            </span>
	           Femme
	        </label>
	        <label class="radio-inline">
	            <span class="custom-radio">
	              <input name="seller_gender" type="radio" value="3" {if $mp_customer_id_gender==3}checked {/if} onclick="show_box_gender();">
	              <span></span>
	            </span>
	           Personnalisé
	        </label>
        </div>
        <div id="box_gender" {if $mp_customer_id_gender==3}checked {else} style="display:none" {/if} >
        	<div class="row">
        		<span>{l s='Indiquer votre sexe' mod='marketplace'}</span>
        		<p>{l s='Me désigner avec le pronom suivant:' mod='marketplace'}</p>
        		 <input class="form-control" name="seller_title" type="text" value="{$mp_seller_info.seller_title}" placeholder="{l s='Pronom' mod='marketplace'}">
        	</div>
        </div>
    </div>
    <div class="col-md-6">
    	<label for="seller_birthday" class="control-label">
			{if $language.iso_code=='fr'}Date d'anniversaire{/if} 
			{if $language.iso_code=='en'}Bithdate{/if} 
		</label>
        <input class="form-control" name="seller_birthday" type="text" value="{$customer.birthday|date_format:"%D"}" placeholder="{if $language.iso_code=='fr'}DD/MM/YYYY{/if} {if $language.iso_code=='en'}YYYY/MM/DD {/if}" >
        <span class="form-control-comment">
            {if $language.iso_code=='fr'} (Ex. : 31/05/1970){/if}{if $language.iso_code=='en'} (Ex. : 1970/05/31){/if}
        </span>
    </div>
</div>
<div class="form-group row">
	<div class="col-md-6">
		<label for="seller_nationality" class="control-label required">
			{l s='Nationalité' mod='marketplace'}
		</label>
		<select name="seller_nationality" id="seller_nationality" class="form-control form-control-select" required="required" style="height:40px; width:100%">
			<option value="">{l s='Select Country' mod='marketplace'}</option>
			{foreach $african_country as $countrydetail}
				<option value="{$countrydetail.id_country}" {if $mp_seller_info.seller_nationality==$countrydetail.id_country} selected{/if}>
					{$countrydetail.name}
				</option>
			{/foreach}
		</select>
	</div>
	<div class="col-md-6">
		<label for="residence" class="control-label required">
			{l s='Pays de résidence' mod='marketplace'}
		</label>
		<select name="residence" id="residence" required="required" class="form-control form-control-select" style="height:40px; width:100%">
			<option value="">{l s='Select Country' mod='marketplace'}</option>
			{foreach $country as $countrydetail}
				<option value="{$countrydetail.id_country}" {if $mp_seller_info.residence==$countrydetail.id_country} selected{/if}>
					{$countrydetail.name}
				</option>
			{/foreach}
		</select>
	</div>
</div>
<div class="form-group row">
	<div class="col-md-6">
		<label for="customer_email" class="control-label required">
			{l s='Email' mod='marketplace'}
		</label>
		<input class="form-control"
		type="email"
		value="{$customer.email}"
		name="customer_email"
		id="customer_email"
		required="required"
		/>
		<span class="wk-msg-customer_email"></span>
	</div>
	<div class="col-md-6">
		<label for="phone" class="control-label required">
			{l s='Numéro Téléphone' mod='marketplace'}
		</label>
		<input class="form-control"
		type="text"
		value="{if isset($customer.phone)}{$customer.phone}{/if}"
		name="seller_phone"
		id="phone"
		maxlength="" required="required"/>
	</div>
</div>
<div class="form-group row">
	<div class="col-md-6">
		<label for="seller_password" class="control-label required">
			{l s='Mot de passe' mod='marketplace'}
		</label>
		<input class="form-control"
		type="password"
		value=""
		name="seller_password"
		id="seller_password"
		/>
		<span class="wk-msg-customeremail"></span>
	</div>
	<div class="col-md-6">
		<label for="customer_confpassword" class="control-label required">
			{l s='Confirmation du mot de passe' mod='marketplace'}
		</label>
		<input class="form-control"
		type="text"
		value=""
		name="seller_confpassword"
		id="seller_confpassword"
		 />
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label for="address" class="control-label required">
			{l s='Adresse' mod='marketplace'}
		</label>
	</div>
	<textarea class="form-control" name="address"> {if isset($smarty.post.address)}{$smarty.post.address}{else}{$mp_seller_info.address}{/if}</textarea>
</div>
{if Configuration::get('WK_MP_SELLER_FAX') || Configuration::get('WK_MP_SELLER_TAX_IDENTIFICATION_NUMBER')}
	<div class="form-group row">
		{if Configuration::get('WK_MP_SELLER_FAX')}
			<div class="col-md-6">
				<label for="fax" class="control-label">{l s='Fax' mod='marketplace'}</label>
				<input class="form-control"
				type="text"
				value="{if isset($smarty.post.fax)}{$smarty.post.fax}{else}{$mp_seller_info.fax}{/if}"
				name="fax"
				id="fax" />
			</div>
		{/if}
		{if Configuration::get('WK_MP_SELLER_TAX_IDENTIFICATION_NUMBER')}
			<div class="col-md-6">
				<label for="fax" class="control-label">{l s='Tax Identification Number' mod='marketplace'}</label>
				<input class="form-control"
				type="text"
				value="{if isset($smarty.post.tax_identification_number)}{$smarty.post.tax_identification_number}{else}{$mp_seller_info.tax_identification_number}{/if}"
				name="tax_identification_number"
				id="tax_identification_number" />
			</div>
		{/if}
	</div>
{/if}

<div class="form-group row">
	{if $seller_country_need}
		<div class="col-md-6" id="seller_zipcode">
			<label for="postcode" class="control-label required">
				{l s='Zip/Postal Code' mod='marketplace'}
			</label>
			<input class="form-control"
			type="text"
			value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{$mp_seller_info.postcode}{/if}"
			name="postcode"
			id="postcode" />
		</div>
		<div class="col-md-6">
			<label for="city" class="control-label required">
				{l s='City' mod='marketplace'}
			</label>
			<input class="form-control"
			type="text"
			value="{if isset($smarty.post.city)}{$smarty.post.city}{else}{$mp_seller_info.city}{/if}"
			name="city"
			id="city"
			maxlength="64" />
		</div>
	{/if}
</div>

{hook h="displayMpEditProfileInformationBottom"}