{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{extends file=$layout}
{block name="head" prepend}
 <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/css/intlTelInput.css" rel="stylesheet" type="text/css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script type="text/javascript" src="{$smarty.const._MODULE_DIR_}marketplace/views/js/mpdelivery.js"></script>
{/block}
{block name='content'}
{if isset($smarty.get.deleted)}
	<p class="alert alert-success">
		<button data-dismiss="alert" class="close" type="button">×</button>
		{l s='Delivery method deleted successfully' mod='marketplace'}
	</p>
{else if isset($smarty.get.edited)}
	<p class="alert alert-success">
		<button data-dismiss="alert" class="close" type="button">×</button>
		{l s='Delivery method updated successfully' mod='marketplace'}
	</p>
{else if isset($smarty.get.created)}
	<p class="alert alert-success">
		<button data-dismiss="alert" class="close" type="button">×</button>
		{l s='Delivery method created successfully' mod='marketplace'}
	</p>
{/if}

<div class="wk-mp-block">
	{hook h="displayMpMenu"}
	<div class="wk-mp-content">
		<div class="page-title" style="background-color:{$title_bg_color};">
			<span style="color:{$title_text_color};">{if $language.iso_code=='fr'}Détails de livraison{/if} {if $language.iso_code=='en'}Delivery Details{/if}</span>
		</div>
		<div class="wk-mp-right-column">
			{if isset($seller_delivery_details) && !isset($edit)}
				{if !empty($seller_delivery_details)}
					<div class="wk_product_list">
					{foreach from=$seller_delivery_details key=k item=delivery}
						<div>
							<div style='col-md-3'>
								<label class="wk_payment_mode_heading">
								{if $language.iso_code == 'fr'}Transporteur :
								{/if}
								{if $language.iso_code == 'en'}Carrier name :
								{/if}
								</label>
							</div>
							<div class='col-md-9'>
								<span class="wk_payment_mode_details">
									{$delivery.carrier_name}
								</span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div>
							<div style='col-md-3'>
								<label class="wk_payment_mode_heading">
								{if $language.iso_code == 'fr'}Voie de livraison :
								{/if}
								{if $language.iso_code == 'en'}Delivery method :
								{/if}
							</label>
							</div>
							<div class='col-md-9'>
								{if $delivery.delivery_way==1}
									<p>
										<span class="wk_payment_mode_details">{if $language.iso_code == 'fr'}Maritime {/if} {if $language.iso_code == 'en'} Shipping{/if}</span>
									</p>
								{/if}
								{if $delivery.delivery_way==2}
									<p>
										<span class="wk_payment_mode_details">{if $language.iso_code == 'fr'}Aérienne{/if} {if $language.iso_code == 'en'} Air transport{/if}</span>
									</p>
								{/if}
								{if $delivery.delivery_way==3}
									<p>
										<span class="wk_payment_mode_details">{if $language.iso_code == 'fr'}Terrestre{/if} {if $language.iso_code == 'en'} By road{/if}</span>
									</p>
								{/if}
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							<p class="wk_btn_delivery_mode_edit">
								<ul class='pull-right'>
									<li style="float:left; padding-right:10px">
										<a href="{$link->getModuleLink('marketplace', 'mpdelivery', ['id' => $delivery.id_customer_delivery])}" class="">
											<button class="btn btn-primary btn-sm">
												<i class="material-icons">&#xE254;</i> {l s='Edit' mod='marketplace'}
											</button>
										</a>
									</li>
									<li style="padding-right:10px; float:left;">
										<a href="{$link->getModuleLink('marketplace', 'mpdelivery', ['id' => $delivery.id_customer_delivery, 'delete_delivery' => 1])}" class="">
											<button class="btn btn-danger btn-sm delete_mp_data">
												<i class="material-icons">&#xE872;</i> {l s='Delete' mod='marketplace'}
											</button>
										</a>
									</li>
								</ul>
							</p>
						</div>
					{/foreach}
					
				</div>
				{else}
					<form action="{$link->getModuleLink('marketplace', 'mpdelivery')}" method="post" class="form-horizontal" enctype="multipart/form-data" role="form" accept-charset="UTF-8,ISO-8859-1,UTF-16">
						<div class="form-wrapper">
							<div class="container">
								{if !isset($edit)}
									<div class="col-md-3" style="float:right;">
						                <input type="button" class="btn btn-primary" id="addrow" value="{l s='Ajouter' mod='marketplace'}" />
						            </div>
						        {/if}
							    <table id="myTable" class=" table order-list">
								    <thead>
								        <tr>
								            <td>{if $language.iso_code=='fr'}Transporteur{/if} {if $language.iso_code=='en'}Carrier{/if}</td>
								            <td>{if $language.iso_code=='fr'}Voie{/if} {if $language.iso_code=='en'}Delivery method{/if} </td>
								            <td>{if $language.iso_code=='fr'}Durée{/if} {if $language.iso_code=='en'}Duration{/if}</td>
								        </tr>
								    </thead>
								    <tbody>
								        <tr>
								            <td class="col-sm-4">
								                <input class="form-control"
												type="text"
												value=""
												name="transporter[]"
												id="transporter"
												maxlength="" />
												<span class="wk-msg-transporter"></span>
								            </td>
								            <td class="col-sm-4">
								            	{if $language.iso_code=='fr'}
									                <select name="delivery_way[]" class="form-control form-control-select">
									                	<option value="0">Choisir une voie</option>
														<option value="1" {if isset($edit) && ($seller_delivery_details.delivery_way == 1)} selected {/if}>Maritime</option>
														<option value="2" {if isset($edit) && ($seller_delivery_details.delivery_way == 2)} selected {/if}>Aérienne</option>
														<option value="3" {if isset($edit) && ($seller_delivery_details.delivery_way == 3)} selected {/if}>Terrestre</option>
													</select>
												{/if} 
												{if $language.iso_code=='en'}
									                <select name="delivery_way[]" class="form-control form-control-select">
									                	<option value="0">Choose delivery method</option>
														<option value="1" {if isset($edit) && ($seller_delivery_details.delivery_way == 1)} selected {/if}>Shipping</option>
														<option value="2"  {if isset($edit) && ($seller_delivery_details.delivery_way == 2)} selected {/if}>Air transport</option>
														<option value="3"  {if isset($edit) && ($seller_delivery_details.delivery_way == 3)} selected {/if}>By road</option>
													</select>
												{/if}
												<span class="wk-msg-delivery_way"></span>
								            </td>
								            <td class="col-sm-4">
								               <input class="form-control"
												type="text"
												value=""
												name="duration[]"
												id="duration"
												placeholder="{if $language.iso_code=='fr'}Ex: 2 à 4 jours{/if}{if $language.iso_code=='en'}Ex: from 2 to 4 days{/if}" />
								            </td>
								            <td class="col-sm-2"><a class="deleteRow"></a>

								            </td>
								        </tr>
								    </tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12 wk_text_right">
									<input type="hidden" id="customer_id" name="customer_id" value="{$customer_id}" />
									<button type="submit" name="submit_delivery_details" id="submit_delivery_details" class="btn btn-success wk_btn_extra form-control-submit">
										<span>{l s='Save' mod='marketplace'}</span>
									</button>
								</div>
							</div>
						</div>
					</form>
				{/if}
			{else}
					<div class="alert alert-info">
						{if $language.iso_code=='fr'}
							Fournissez les détails de livraison de vos commandes.
						{/if}
						{if $language.iso_code=='en'}
							Provide the delivery details of your orders.
						{/if}
					</div>
					<form action="{if isset($edit)}{$link->getModuleLink('marketplace', 'mpdelivery', ['id' => $seller_delivery_details.id_customer_delivery])}{else}{$link->getModuleLink('marketplace', 'mpdelivery')}{/if}" method="post" class="form-horizontal" enctype="multipart/form-data" role="form" accept-charset="UTF-8,ISO-8859-1,UTF-16">
						<div class="form-wrapper">
							<div class="container">
								{if !isset($edit)}
									<div class="col-md-3" style="float:right;">
						                <input type="button" class="btn btn-primary" id="addrow" value="{l s='Ajouter' mod='marketplace'}" />
						            </div>
						        {/if}
							    <table id="myTable" class=" table order-list">
								    <thead>
								        <tr>
								            <td>{if $language.iso_code=='fr'}Transporteur{/if} {if $language.iso_code=='en'}Carrier{/if}</td>
								            <td>{if $language.iso_code=='fr'}Voie{/if} {if $language.iso_code=='en'}Delivery method{/if} </td>
								            <td>{if $language.iso_code=='fr'}Durée{/if} {if $language.iso_code=='en'}Duration{/if}</td>
								        </tr>
								    </thead>
								    <tbody>
								        <tr>
								            <td class="col-sm-4">
								                <input class="form-control"
												type="text"
												value="{if isset($edit)} {$seller_delivery_details.carrier_name} {/if}"
												name="transporter[]"
												id="transporter"
												maxlength="" />
												<span class="wk-msg-transporter"></span>
								            </td>
								            <td class="col-sm-4">
								            	{if $language.iso_code=='fr'}
									                <select name="delivery_way[]" class="form-control form-control-select">
									                	<option value="0">Choisir une voie</option>
														<option value="1" {if isset($edit) && ($seller_delivery_details.delivery_way == 1)} selected {/if}>Maritime</option>
														<option value="2" {if isset($edit) && ($seller_delivery_details.delivery_way == 2)} selected {/if}>Aérienne</option>
														<option value="3" {if isset($edit) && ($seller_delivery_details.delivery_way == 3)} selected {/if}>Terrestre</option>
													</select>
												{/if} 
												{if $language.iso_code=='en'}
									                <select name="delivery_way[]" class="form-control form-control-select">
									                	<option value="0">Choose delivery method</option>
														<option value="1" {if isset($edit) && ($seller_delivery_details.delivery_way == 1)} selected {/if}>Shipping</option>
														<option value="2"  {if isset($edit) && ($seller_delivery_details.delivery_way == 2)} selected {/if}>Air transport</option>
														<option value="3"  {if isset($edit) && ($seller_delivery_details.delivery_way == 3)} selected {/if}>By road</option>
													</select>
												{/if}
												<span class="wk-msg-delivery_way"></span>
								            </td>
								            <td class="col-sm-4">
								               <input class="form-control"
												type="text"
												value="{if isset($edit)} {$seller_delivery_details.duration} {/if}"
												name="duration[]"
												id="duration"
												placeholder="{if $language.iso_code=='fr'}Ex: 2 à 4 jours{/if}{if $language.iso_code=='en'}Ex: from 2 to 4 days{/if}" />
								            </td>
								            <td class="col-sm-2"><a class="deleteRow"></a>

								            </td>
								        </tr>
								    </tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12 wk_text_right">
									<input type="hidden" id="customer_id" name="customer_id" value="{$customer_id}" />
									<button type="submit" name="submit_delivery_details" id="submit_delivery_details" class="btn btn-success wk_btn_extra form-control-submit">
										<span>{l s='Save' mod='marketplace'}</span>
									</button>
								</div>
							</div>
						</div>
					</form>
			{/if}
		</div>
	</div>
</div>
{/block}