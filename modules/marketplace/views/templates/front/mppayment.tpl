{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{extends file=$layout}
{block name='content'}
{literal}
<script type="text/javascript">
function show_box_gender(){
  $('#box_gender').show();
}
function hide_box_gender(){
  $('#box_gender').hide();
}
function show_payment_box_1(){
	if ($("#virement").prop('checked')==true){ 
		$('#payment_box_1').show();
		$('#payment_box_2').hide();
		$('#payment_box_3').hide();
		$('#payment_box_4').hide();
		$('#payment_box_5').hide();
		$('#payment_box_6').hide();

	}
}
function show_payment_box_2(){
	if ($("#carte").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').show();
		$('#payment_box_3').hide();
		$('#payment_box_4').hide();
		$('#payment_box_5').hide();
		$('#payment_box_6').hide();
	}
}
function show_payment_box_3(){
	if ($("#mobile_money").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').hide();
		$('#payment_box_3').show();
		$('#payment_box_4').hide();
		$('#payment_box_5').hide();
		$('#payment_box_6').hide();
	}
	
}
function show_payment_box_4(){
	if ($("#cash").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').hide();
		$('#payment_box_3').hide();
		$('#payment_box_4').show();
		$('#payment_box_5').hide();
		$('#payment_box_6').hide();
	}
}
function show_payment_box_5(){
	if ($("#bitcoin").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').hide();
		$('#payment_box_3').hide();
		$('#payment_box_4').hide();
		$('#payment_box_5').show();
		$('#payment_box_6').hide();
	}
}
function show_payment_box_6(){
	if ($("#wallet").prop('checked')==true){ 
		$('#payment_box_1').hide();
		$('#payment_box_2').hide();
		$('#payment_box_3').hide();
		$('#payment_box_4').hide();
		$('#payment_box_5').hide();
		$('#payment_box_6').show();
	}
}
</script>
{/literal}
{if isset($smarty.get.deleted)}
	<p class="alert alert-success">
		<button data-dismiss="alert" class="close" type="button">×</button>
		{l s='Payment method deleted successfully' mod='marketplace'}
	</p>
{else if isset($smarty.get.edited)}
	<p class="alert alert-success">
		<button data-dismiss="alert" class="close" type="button">×</button>
		{l s='Payment method updated successfully' mod='marketplace'}
	</p>
{else if isset($smarty.get.created)}
	<p class="alert alert-success">
		<button data-dismiss="alert" class="close" type="button">×</button>
		{l s='Payment method created successfully' mod='marketplace'}
	</p>
{/if}

<div class="wk-mp-block">
	{hook h="displayMpMenu"}
	<div class="wk-mp-content">
		<div class="page-title" style="background-color:{$title_bg_color};">
			<span style="color:{$title_text_color};">{l s='Payment Details' mod='marketplace'}</span>
		</div>
		<div class="wk-mp-right-column">
			{if isset($seller_payment_details) && !isset($edit)}
				<div class="wk_product_list">
					{if !empty($seller_payment_details)}
						{foreach from=$seller_payment_details key=k item=payment}
							<div>
								<div style='col-md-3'>
									<label class="wk_payment_mode_heading">{if $language.iso_code=='fr'}Mode de paiement :{/if} {if $language.iso_code=='en'}Payment mode :{/if}</label>
								</div>
								<div class='col-md-9'>
									<span id="label_payment_mode_{$k}" class="wk_payment_mode_details">
										{$payment.payment_mode}
									</span>
								</div>
								<div class="clearfix"></div>
							</div>
							<div>
								<div style='col-md-3'>
									<label class="wk_payment_mode_heading">
									{if $language.iso_code=='fr'}Détails{/if} {if $language.iso_code=='en'}Account Details{/if}
								</label>
								</div>
								<div class='col-md-9'>
									{if $payment.payment_mode_id==1}
										{if !empty($payment.bank_name)}
											<p>
												<span class="">{l s='Nom de la banque :' mod='marketplace'}</span>
												<span id="label_bank_name" class="">
													{if !empty($payment.bank_name)}
														{$payment.bank_name}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
										{if $payment.rib != ''}
											<p>
												<span class="">{l s='RIB :' mod='marketplace'}</span>
												<span id="label_rib" class="">
													{if $payment.rib != ''}
														{$payment.rib}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
									{/if}
									{if $payment.payment_mode_id==2}
										{if !empty($payment.name_card)}
											<p>
												<span class="">{l s='Nom du titulaire de la carte :' mod='marketplace'}</span>
												<span id="label_name_card" class="">
													{if !empty($payment.name_card)}
														{$payment.name_card}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
										{if !empty($payment.num_card)}
											<p>
												<span class="">{l s='Numéro de la carte :' mod='marketplace'}</span>
												<span id="label_num_card" class="">
													{if !empty($payment.num_card)}
														{$payment.num_card}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
										{if !empty($payment.card_expiration)}
											<p>
												<span class="">{l s='Expire le :' mod='marketplace'}</span>
												<span id="label_num_card" class="">
													{if !empty($payment.card_expiration)}
														{$payment.card_expiration}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
									{/if}
									{if $payment.payment_mode_id==3}
										{if !empty($payment.num_mobile)}
											<p>
												<span class="">{l s='Numéro :' mod='marketplace'}</span>
												<span id="label_num_mobile" class="">
													{if !empty($payment.num_mobile)}
														{$payment.num_mobile}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
									{/if}
									{if $payment.payment_mode_id==4}
										{if !empty($payment.num_cash)}
											<p>
												<span class="">{l s='Numéro :' mod='marketplace'}</span>
												<span id="label_num_cash" class="">
													{if !empty($payment.num_cash)}
														{$payment.num_cash}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
										{if !empty($payment.cash_operator)}
											<p>
												<div class="">{l s='Opérateur de transfert :' mod='marketplace'}</div>
												<div class="row">
													{if ($payment.cash_operator==1)}
														<span style="padding-right:10px; float: left;"><i class="icon-fllist_wu"> </i> </span> Western Union
													{/if}
													{if ($payment.cash_operator==2) }
														<span style="padding-right:10px; float: left;"><i class="icon-fllist_mg"> </i> </span>Moneygram
													{/if}
													{if ($payment.cash_operator==3) }
														<span style="padding-right:10px; float: left;"> </span> Express Union
													{/if}
												</div>
											</p>
										{/if}
									{/if}
									{if $payment.payment_mode_id==5}
										{if !empty($payment.bitcoin_address)}
											<p>
												<span class="">{l s='Adresse :' mod='marketplace'}</span>
												<span id="label_bitcoin_address" class="">
													{if !empty($payment.bitcoin_address)}
														{$payment.bitcoin_address}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
										{if !empty($payment.nickname_bitcoin)}
											<p>
												<span class="">{l s='Pseudo :' mod='marketplace'}</span>
												<span id="label_cash_operator" class="">
													{if !empty($payment.nickname_bitcoin)}
														{$payment.nickname_bitcoin}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
									{/if}
									{if $payment.payment_mode_id==6}
										{if !empty($payment.wallet_number)}
											<p>
												<span class="">{l s='Numéro :' mod='marketplace'}</span>
												<span id="label_wallet_number" class="">
													{if !empty($payment.wallet_number)}
														{$payment.wallet_number}
													{else}
														<i>{l s='Aucun pour le moment' mod='marketplace'}</i>
													{/if}
												</span>
											</p>
										{/if}
									{/if}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<p class="wk_btn_payment_mode_edit">
									<ul class='pull-right'>
										<li style="float:left; padding-right:10px">
											<a href="{$link->getModuleLink('marketplace', 'mppayment', ['id' => $payment.id_customer_payment])}" class="">
												<button class="btn btn-primary btn-sm">
													<i class="material-icons">&#xE254;</i> {l s='Edit' mod='marketplace'}
												</button>
											</a>
										</li>
										<li style="padding-right:10px; float:left;">
											<a href="{$link->getModuleLink('marketplace', 'mppayment', ['id' => $payment.id_customer_payment, 'delete_payment' => 1])}" class="">
												<button class="btn btn-danger btn-sm delete_mp_data">
													<i class="material-icons">&#xE872;</i> {l s='Delete' mod='marketplace'}
												</button>
											</a>
										</li>
									</ul>
								</p>
							</div>
						{/foreach}
					{else}
						<div class="alert alert-info">
						{l s='Provide your account details to obtain payment from admin for your orders.' mod='marketplace'}
					</div>
					<form action="{$link->getModuleLink('marketplace', 'mppayment')}" method="post" class="form-horizontal" enctype="multipart/form-data" role="form" accept-charset="UTF-8,ISO-8859-1,UTF-16">
						<div class="form-wrapper">
							<div class="">
								<label for="payment_method" class="control-label">
									{l s='Canal de versement des paiements' mod='marketplace'}
								</label>
								<p>{l s='Choisissez parmi les moyens de paiement proposés et renseignez les différentes informations.' mod='marketplace'}</p>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="1" style="width:10%; padding-right:10px" id="virement" onclick="show_payment_box_1();"/> {l s='Virement bancaire' mod='marketplace'}
										</div>
										<div id="payment_box_1" style="display:none">
											<p style="text-align:center; font-size:14px;">{l s='Informations bancaires' mod='marketplace'}</p>
											<div class="col-md-6">
												<label for="bank_name" class="control-label required">
												{l s='Nom de la banque' mod='marketplace'}
												</label>
												<input type="text" name="bank_name" class="form-control" value="" placeholder="{l s='Nom de la banque' mod='marketplace'}"/>
											</div>
											<div class="col-md-6">
												<label for="rib" class="control-label required">
												{l s='RIB' mod='marketplace'}
												</label>
												<input type="text" name="rib" class="form-control" value="" placeholder="{l s='RIB' mod='marketplace'}"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="2" style="width:10%; padding-right:10px" id="carte" onclick="show_payment_box_2();" /> {l s='Carte de débit ou de crédit' mod='marketplace'}
										</div>
										<div id="payment_box_2" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Informations de la carte Visa ou Mastercard</p>
											<div class="col-md-4">
												<label for="name_card" class="control-label required">
												{l s='Nom du titulaire de la carte' mod='marketplace'}
												</label>
												<input type="text" name="name_card" class="form-control" placeholder="Nom" value=""/>
											</div>
											<div class="col-md-8">
												<div class="col-md-6">
													<label for="num_card" class="control-label required">
													{l s='Numéro de la carte' mod='marketplace'}
													</label>
													<input type="text" name="num_card" class="form-control" placeholder="Ex: 4000 1234 5678 9101" value=""/>
												</div>
												<div class="col-md-6">
													<label for="card_expiration" class="control-label required">
													{l s='Expire le' mod='marketplace'}
													</label>
													<input type="text" name="card_expiration" class="form-control" placeholder=" Ex: 12/21" value=""/>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="3" style="width:10%; padding-right:10px" id='mobile_money' onclick="show_payment_box_3();"/> Mobile money 
										</div>
										<div id="payment_box_3" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Numéro pour le débit ou le crédit</p>
											<div class="col-md-6">
												<label for="num_mobile" class="control-label required">
												{l s='Numéro' mod='marketplace'}
												</label>
												<input type="text" name="num_mobile" class="form-control" placeholder="Numéro" value=""/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="4" style="width:10%; padding-right:10px;" id='cash' onclick="show_payment_box_4();"/> Cash 
										</div>
										<div id="payment_box_4" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">Numéro pour vous communiquer les informations des transactions</p>
											<div class='col-md-12'>
												<div class="col-md-6">
													<label for="num_cash" class="control-label required">
													{l s='Numéro' mod='marketplace'}
													</label>
													<input type="text" name="num_cash" class="form-control" placeholder="Numéro" value=""/>
												</div>
												<div class="col-md-6">
													<div class="col-md-12">
														<label for="operator" class="control-label required">
														{l s='Choisir un opérateur de transfert d\'argent' mod='marketplace'}
														</label>
													</div>
													<div class="col-md-4">
														<input type="radio" name="cash_operator" class='form-control'  value="1" checked/> <span style="text-align:center">Western Union</span>
													</div>
													<div class="col-md-4">
														<input type="radio" name="cash_operator" class='form-control'  value="2"/> <span style="text-align:center">Moneygram</span>
													</div>
													<div class='col-md-4'>
														<input type="radio" name="cash_operator" class='form-control' value="3"/> <span style="text-align:center">Express Union</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="5" style="width:10%" id="bitcoin" onclick="show_payment_box_5();"/> Bitcoin 
										</div>
										<div id="payment_box_5" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">{l s='Informations sur le portefeuille' mod='marketplace'}</p>
											<div class="col-md-12">
												<div class='col-md-6'>
													<label for="bitcoin_address" class="control-label required">
													{l s='Adresse' mod='marketplace'}
													</label>
													<input type="text" name="bitcoin_address" class="form-control" placeholder="{l s='Adresse' mod='marketplace'}" value=""/>
												</div>
												<div class='col-md-6'>
													<label for="nickname_bitcoin" class="control-label required">
													{l s='Pseudo' mod='marketplace'}
													</label>
													<input type="text" name="nickname_bitcoin" class="form-control" placeholder="{l s='Pseudo' mod='marketplace'}" value=""/>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="6" style="width:10%" id="wallet" onclick="show_payment_box_6();"/> Bank wallet 
										</div>
										<div id="payment_box_6" style="display:none" class="col-md-12">
											<p style="text-align:center; font-size:14px;">{l s='Numéro du portefeuille mobile' mod='marketplace'}</p>
											<div class="col-md-12">
												<div class='col-md-6'>
													<label for="wallet_numero" class="control-label required">
													{l s='Numéro' mod='marketplace'}
													</label>
													<input type="text" name="wallet_number" class="form-control" placeholder="{l s='Numéro' mod='marketplace'}" value=""/>
												</div>
											</div>
										</div>
									</div>
							</div>
							<div class="row">
								<div class="col-md-12 wk_text_right">
									<input type="hidden" id="customer_id" name="customer_id" value="{$customer_id}" />
									<button type="submit" name="submit_payment_details" id="submit_payment_details" class="btn btn-success wk_btn_extra form-control-submit">
										<span>{l s='Save' mod='marketplace'}</span>
									</button>
								</div>
							</div>
						</div>
					</form>
					{/if}
				</div>
			{else}
				{if isset($mp_payment_option)}
					<div class="alert alert-info">
						{l s='Provide your account details to obtain payment from admin for your orders.' mod='marketplace'}
					</div>
					<form action="{if isset($edit)}{$link->getModuleLink('marketplace', 'mppayment', ['id' => $seller_payment_details.id_customer_payment])}{else}{$link->getModuleLink('marketplace', 'mppayment')}{/if}" method="post" class="form-horizontal" enctype="multipart/form-data" role="form" accept-charset="UTF-8,ISO-8859-1,UTF-16">
						<div class="form-wrapper">
							<div class="">
								<label for="payment_method" class="control-label">
									{l s='Canal de versement des paiements' mod='marketplace'}
								</label>
								<p>{l s='Choisissez parmi les moyens de paiement proposés et renseignez les différentes informations.' mod='marketplace'}</p>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="1" style="width:10%; padding-right:10px" id="virement" onclick="show_payment_box_1();" {if isset($edit) && ($seller_payment_details.payment_mode_id == 1)}checked {/if}/> {l s='Virement bancaire' mod='marketplace'}
										</div>
										<div id="payment_box_1" {if isset($edit) && ($seller_payment_details.payment_mode_id == 1)} style="" {else} style="display:none" {/if}>
											<p style="text-align:center; font-size:14px;">{l s='Informations bancaires' mod='marketplace'}</p>
											<div class="col-md-6">
												<label for="bank_name" class="control-label required">
												{l s='Nom de la banque' mod='marketplace'}
												</label>
												<input type="text" name="bank_name" class="form-control" value="{if isset($edit)}{$seller_payment_details.bank_name}{/if}" placeholder="{l s='Nom de la banque' mod='marketplace'}"/>
											</div>
											<div class="col-md-6">
												<label for="rib" class="control-label required">
												{l s='RIB' mod='marketplace'}
												</label>
												<input type="text" name="rib" class="form-control" value="{if isset($edit)}{$seller_payment_details.rib}{/if}" placeholder="{l s='RIB' mod='marketplace'}"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="2" style="width:10%; padding-right:10px" id="carte" onclick="show_payment_box_2();" {if isset($edit) && ($seller_payment_details.payment_mode_id == 2)}checked {/if}/> {l s='Carte de débit ou de crédit' mod='marketplace'}
										</div>
										<div id="payment_box_2" {if isset($edit)  && ($seller_payment_details.payment_mode_id == 2)} style="" {else} style="display:none" {/if} class="col-md-12">
											<p style="text-align:center; font-size:14px;">Informations de la carte Visa ou Mastercard</p>
											<div class="col-md-4">
												<label for="name_card" class="control-label required">
												{l s='Nom du titulaire de la carte' mod='marketplace'}
												</label>
												<input type="text" name="name_card" class="form-control" placeholder="Nom" value="{if isset($edit)} {$seller_payment_details.name_card} {/if}"/>
											</div>
											<div class="col-md-8">
												<div class="col-md-6">
													<label for="num_card" class="control-label required">
													{l s='Numéro de la carte' mod='marketplace'}
													</label>
													<input type="text" name="num_card" class="form-control" placeholder="Ex: 4000 1234 5678 9101" value="{if isset($edit)}{$seller_payment_details.num_card} {/if}"/>
												</div>
												<div class="col-md-6">
													<label for="card_expiration" class="control-label required">
													{l s='Expire le' mod='marketplace'}
													</label>
													<input type="text" name="card_expiration" class="form-control" placeholder=" Ex: 12/21" value="{if isset($edit)}{$seller_payment_details.card_expiration}{/if}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="3" style="width:10%; padding-right:10px" id='mobile_money' onclick="show_payment_box_3();" {if isset($edit)  && ($seller_payment_details.payment_mode_id == 3)}checked {/if}/> Mobile money 
										</div>
										<div id="payment_box_3" {if isset($edit) && ($seller_payment_details.payment_mode_id == 3)} style="" {else} style="display:none" {/if} class="col-md-12">
											<p style="text-align:center; font-size:14px;">Numéro pour le débit ou le crédit</p>
											<div class="col-md-6">
												<label for="num_mobile" class="control-label required">
												{l s='Numéro' mod='marketplace'}
												</label>
												<input type="text" name="num_mobile" class="form-control" placeholder="Numéro" value="{if isset($edit)}{$seller_payment_details.num_mobile}{/if}"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="4" style="width:10%; padding-right:10px;" id='cash' onclick="show_payment_box_4();" {if isset($edit) && ($seller_payment_details.payment_mode_id == 4)}checked {/if}"/> Cash 
										</div>
										<div id="payment_box_4" {if isset($edit) && ($seller_payment_details.payment_mode_id == 4)} style="" {else} style="display:none" {/if} class="col-md-12">
											<p style="text-align:center; font-size:14px;">Numéro pour vous communiquer les informations des transactions</p>
											<div class='col-md-12'>
												<div class="col-md-6">
													<label for="num_cash" class="control-label required">
													{l s='Numéro' mod='marketplace'}
													</label>
													<input type="text" name="num_cash" class="form-control" placeholder="Numéro" value="{if isset($edit)}{$seller_payment_details.num_mobile}{/if}"/>
												</div>
												<div class="col-md-6">
													<div class="col-md-12">
														<label for="operator" class="control-label required">
														{l s='Choisir un opérateur de transfert d\'argent' mod='marketplace'}
														</label>
													</div>
													<div class="col-md-4">
														<input type="radio" name="cash_operator" class='form-control'  value="1" checked/> <span style="text-align:center">Western Union</span>
													</div>
													<div class="col-md-4">
														<input type="radio" name="cash_operator" class='form-control'  value="2"/> <span style="text-align:center">Moneygram</span>
													</div>
													<div class='col-md-4'>
														<input type="radio" name="cash_operator" class='form-control' value="3"/> <span style="text-align:center">Express Union</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="5" style="width:10%" id="bitcoin" onclick="show_payment_box_5();" {if isset($edit) && ($seller_payment_details.payment_mode_id == 5)}checked {/if}"/> Bitcoin 
										</div>
										<div id="payment_box_5" {if isset($edit) && ($seller_payment_details.payment_mode_id == 5)} style="" {else} style="display:none" {/if} class="col-md-12">
											<p style="text-align:center; font-size:14px;">{l s='Informations sur le portefeuille' mod='marketplace'}</p>
											<div class="col-md-12">
												<div class='col-md-6'>
													<label for="bitcoin_address" class="control-label required">
													{l s='Adresse' mod='marketplace'}
													</label>
													<input type="text" name="bitcoin_address" class="form-control" placeholder="{l s='Adresse' mod='marketplace'}" value=""/>
												</div>
												<div class='col-md-6'>
													<label for="nickname_bitcoin" class="control-label required">
													{l s='Pseudo' mod='marketplace'}
													</label>
													<input type="text" name="nickname_bitcoin" class="form-control" placeholder="{l s='Pseudo' mod='marketplace'}" value=""/>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="row">
											<input type="checkbox" name="payment_mode_id[]" value="6" style="width:10%" id="wallet" onclick="show_payment_box_6();" {if isset($edit) && ($seller_payment_details.payment_mode_id == 6)}checked {/if}"/> Bank wallet 
										</div>
										<div id="payment_box_6" {if isset($edit) && ($seller_payment_details.payment_mode_id == 6)} style="" {else} style="display:none" {/if} class="col-md-12">
											<p style="text-align:center; font-size:14px;">{l s='Numéro du portefeuille mobile' mod='marketplace'}</p>
											<div class="col-md-12">
												<div class='col-md-6'>
													<label for="wallet_numero" class="control-label required">
													{l s='Numéro' mod='marketplace'}
													</label>
													<input type="text" name="wallet_number" class="form-control" placeholder="{l s='Numéro' mod='marketplace'}" value=""/>
												</div>
											</div>
										</div>
									</div>
							</div>
							<div class="row">
								<div class="col-md-12 wk_text_right">
									<input type="hidden" id="customer_id" name="customer_id" value="{$customer_id}" />
									<button type="submit" name="submit_payment_details" id="submit_payment_details" class="btn btn-success wk_btn_extra form-control-submit">
										<span>{l s='Save' mod='marketplace'}</span>
									</button>
								</div>
							</div>
						</div>
					</form>
				{else}
					<div class="alert alert-info">
						{l s='Admin has not created any payment method yet' mod='marketplace'}
					</div>
				{/if}
			{/if}
			<div class="left full">
				{hook h="displayMpPaymentDetailBottom"}
			</div>
		</div>
	</div>
</div>
{/block}