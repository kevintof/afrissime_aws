<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once dirname(__FILE__).'/classes/WkMpRequiredClasses.php';
if (Module::isEnabled('mpsellerstaff')) {
    include_once dirname(__FILE__).'/../mpsellerstaff/classes/WkMpStaffRequiredClasses.php';
}
class Marketplace extends Module
{
    public static $mpController = true;
    public $sellerDetailsView = array(
        array('id_group' => 1),
        array('id_group' => 2),
        array('id_group' => 3),
        array('id_group' => 4),
        array('id_group' => 5),
        array('id_group' => 6),
        array('id_group' => 7),
        array('id_group' => 8),
        array('id_group' => 9),
    );

    public function __construct()
    {
        $this->name = 'marketplace';
        $this->tab = 'market_place';
        $this->version = '5.2.0';
        $this->author = 'Webkul';
        $this->need_instance = 0;
        $this->secure_key = Tools::hash($this->name); //encrypt() deprecated in PS 1.7, use hash()
        $this->module_key = '92e753c36c07c56867a9169292c239e5';
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->controllers = array(
            'addproduct',
            'allreviews',
            'dashboard',
            'editprofile',
            'managecombination',
            'mporder',
            'mporderdetails',
            'mppayment',
            'mptransaction',
            'productdetails',
            'productlist',
            'sellerprofile',
            'sellerrequest',
            'shopstore',
            'updateproduct',
            'createattribute',
            'createattributevalue',
            'productattribute',
            'viewattributegroupvalue',
            'addfeaturevalue',
            'createfeature',
            'productfeature',
            'viewfeaturevalue',
            'generatecombination',
        );
        parent::__construct();
        $this->displayName = $this->l('Marketplace');
        $this->description = $this->l('Turn your Prestashop store into a marketplace where sellers can add products, manage orders, manage profile, shop, product name and descriptions in multi-language.');
        $this->confirmUninstall = $this->l('Are you sure? All module data will be lost after uninstalling the module');

        $this->sellerDetailsView[0]['name'] = $this->l('Seller Name');
        $this->sellerDetailsView[1]['name'] = $this->l('Seller Email');
        $this->sellerDetailsView[2]['name'] = $this->l('Seller Phone & Fax');
        $this->sellerDetailsView[3]['name'] = $this->l('Address');
        $this->sellerDetailsView[4]['name'] = $this->l('About Shop');
        $this->sellerDetailsView[5]['name'] = $this->l('Social Profile');
        $this->sellerDetailsView[6]['name'] = $this->l('Contact Seller Link');
        $this->sellerDetailsView[7]['name'] = $this->l('Shop Products');
        $this->sellerDetailsView[8]['name'] = $this->l('Shop Name On Product Page');
    }

    public function getContent()
    {
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminMarketplaceGeneralSettings'));
    }

    // If customer is getting delete then we are updating seller with anonymous information
    public function hookActionDeleteGDPRCustomer($customer)
    {
        $objSeller = new WkMpSeller();
        $sellerInfo = WkMpSeller::getSellerByCustomerId($customer['id']);
        if ($sellerInfo) {
            $result = $objSeller->updateSellerInformation($sellerInfo, $customer['email']);
            if (!$result) {
                return json_encode($this->l('Unable to delete seller information.'));
            }
        }
    }

    // Showing seller information based on customer ID
    public function hookActionExportGDPRData($customer)
    {
        if (!Tools::isEmpty($customer['email']) && Validate::isEmail($customer['email'])) {
            $objSeller = new WkMpSeller();
            if ($res = $objSeller->exportSellerInformation($customer['id'])) {
                return json_encode($res);
            }
            return json_encode($this->l('Seller information not exist.'));
        }
    }

    public function sellersOrderMail(
        $mpOrderDetail,
        $customer,
        $address,
        $addressState,
        $idCurrency,
        $idSeller,
        $idOrder
    ) {
        $order = new Order($idOrder);
        $idCurrency = (int) $order->id_currency;

        // Format price
        foreach ($mpOrderDetail['product_list'] as &$list) {
            foreach ($list as &$product) {
                $product['unit_price_tax_excl'] = Tools::displayPrice($product['unit_price_tax_excl']);
                $product['unit_price_tax_incl'] = Tools::displayPrice($product['unit_price_tax_incl']);
                $product['total_price_tax_incl'] = Tools::displayPrice($product['total_price_tax_incl']);
            }
        }

        $objMpSeller = new WkMpSeller($idSeller);
        $productHTML = $objMpSeller->getMpEmailTemplateContent(
            'mp_order_product_list.tpl',
            Mail::TYPE_HTML,
            $mpOrderDetail['product_list']
        );

        $currency = new Currency($idCurrency);
        $totalTax = $mpOrderDetail['total_seller_tax'];

        $templateVars = array(
            '{order_reference}' => $order->reference,
            '{seller_name}' => $mpOrderDetail['seller_name'],
            '{customer_name}' => $customer->firstname.' '.$customer->lastname,
            '{customer_email}' => $customer->email,
            '{ship_address_name}' => $address->firstname.' '.$address->lastname,
            '{ship_address}' => $address->address1.' '.$address->address2,
            '{city}' => $address->city,
            '{state}' => $addressState,
            '{country}' => $address->country,
            '{zipcode}' => $address->postcode,
            '{phone}' => $address->phone_mobile,
            '{seller_product_total}' => Tools::displayPrice($mpOrderDetail['total_earn_te'], $currency, false),
            '{seller_shipping}' => '',
            '{seller_tax}' => Tools::displayPrice($totalTax, $currency, false),
            '{final_total_price}' => Tools::displayPrice($mpOrderDetail['total_price_tax_incl'], $currency, false),
            '{product_html}' => $productHTML,
            '{voucher_html}' => '',
        );

        $voucherInfo = WkMpSellerOrderDetail::setVoucherDetails($idOrder, $idSeller, $idCurrency);
        if ($voucherInfo) {
            $voucherHTML = $objMpSeller->getMpEmailTemplateContent(
                'mp_order_voucher_detail.tpl',
                Mail::TYPE_HTML,
                $voucherInfo
            );
            $templateVars['{voucher_html}'] = $voucherHTML;
        }

        if ($sellerShipping = WkMpAdminShipping::getSellerShippingByIdOrder(
            $idOrder,
            $objMpSeller->seller_customer_id
        )) {
            $shippingInfo = $objMpSeller->getMpEmailTemplateContent(
                'mp_shipping_detail.tpl',
                Mail::TYPE_HTML,
                Tools::displayPrice($sellerShipping, $currency, false)
            );
            $templateVars['{seller_shipping}'] = $shippingInfo;
            $templateVars['{final_total_price}'] = Tools::displayPrice(
                $mpOrderDetail['total_price_tax_incl'] + $sellerShipping,
                $currency,
                false
            );
        }

        if (Configuration::get('WK_MP_SUPERADMIN_EMAIL')) {
            $adminEmail = Configuration::get('WK_MP_SUPERADMIN_EMAIL');
        } else {
            $idEmployee = WkMpHelper::getSupperAdmin();
            $employee = new Employee($idEmployee);
            $adminEmail = $employee->email;
        }

        $fromTitle = Configuration::get('WK_MP_FROM_MAIL_TITLE');
        $to = $mpOrderDetail['seller_email'];
        Mail::Send(
            $mpOrderDetail['seller_default_lang_id'],
            'mp_order',
            Mail::l('Order Created', $mpOrderDetail['seller_default_lang_id']),
            $templateVars,
            $to,
            $mpOrderDetail['seller_name'],
            $adminEmail,
            $fromTitle,
            null,
            null,
            _PS_MODULE_DIR_.'marketplace/mails/',
            false,
            null,
            null
        );
    }

    public function hookDisplayAdminOrder()
    {
        $idOrder = Tools::getValue('id_order');
        $order = new Order($idOrder);
        $order->getCurrentState();
        $state = new OrderState($order->getCurrentState(), Configuration::get('PS_LANG_DEFAULT'));
        $mpOrderDetails = new WkMpSellerOrderDetail();
        if ($mpOrders = $mpOrderDetails->getProductsFromOrder($idOrder)) {
            $sellerOrderDetail = array();
            foreach ($mpOrders as $detail) {
                $sellerProduct = WkMpSellerProduct::getSellerProductByPsIdProduct($detail['product_id']);
                if ($sellerProduct) {
                    $detail['id_mp_product'] = $sellerProduct['id_mp_product'];
                }
                $sellerOrderDetail[$detail['id_seller']][] = $detail;
            }
            $this->context->smarty->assign(array(
                'seller_order_details' => $mpOrders,
                'mp_seller_order_details' => $sellerOrderDetail,
                'link' => $this->context->link,
                'currentState' => $state,
            ));

            return $this->display(__FILE__, 'admin-order-view-seller-details.tpl');
        }
    }

    public function hookDisplayMpMenu()
    {
        $idCustomer = $this->context->customer->id;
        $seller = WkMpSeller::getSellerDetailByCustomerId($idCustomer);
        if ($seller) {
            if ($seller['active']) {
                //Get Seller total products
                if ($sellerProduct = WkMpSellerProduct::getSellerProduct($seller['id_seller'], 'all', $this->context->language->id)) {
                    $totalSellerProducts = count($sellerProduct);
                } else {
                    $totalSellerProducts = 0;
                }

                $this->context->smarty->assign(array(
                    'name_shop' => $seller['link_rewrite'],
                    'totalSellerProducts' => $totalSellerProducts,
                ));
            }

            $this->context->smarty->assign('is_seller', $seller['active']);
        } else {
            $this->context->smarty->assign('is_seller', -1); // Not a seller
        }

        $this->context->smarty->assign('link', $this->context->link);
        return $this->fetch('module:marketplace/views/templates/hook/mpmenu.tpl');
    }

    public function hookDisplayMPMyAccountMenu()
    {
        $seller = WkMpSeller::getSellerDetailByCustomerId($this->context->customer->id);
        if ($seller) {
            if ($seller['active']) {
                $this->context->smarty->assign(array(
                    'id_customer' => $this->context->customer->id,
                    'name_shop' => $seller['link_rewrite'],
                ));
            }
            $this->context->smarty->assign(array(
                'mpSellerShopSettings' => Configuration::get('WK_MP_SELLER_SHOP_SETTINGS'),
                'shop_approved' => $seller['shop_approved'],
                'is_seller' => $seller['active'],

            ));
        } else {
            $this->context->smarty->assign('is_seller', -1); // Not a seller
        }

        $this->context->smarty->assign('link', $this->context->link);
        return $this->fetch('module:marketplace/views/templates/hook/mpmyaccountmenu.tpl');
    }

    /**
     * Display Sell on link on navigation bar.
     *
     * @return html An link with text Sell on shop name
     */
    public function hookDisplayNav1()
    {
        if (Configuration::get('WK_MP_LINK_ON_NAV_BAR')) {
            $this->context->smarty->assign('wk_ad_nav', 1);

            return $this->displayAdvertisementLink();
        }
    }

    /**
     * Display Sell on link on footer.
     *
     * @return html An link with text Sell on shop name
     */
    public function hookDisplayMyAccountBlock()
    {
        if (Configuration::get('WK_MP_LINK_ON_FOOTER_BAR')) {
            $this->context->smarty->assign('wk_ad_footer', 1);

            return $this->displayAdvertisementLink();
        }
    }

    public function displayAdvertisementLink()
    {
        if ($this->context->customer->id) {
            $isSellerExist = WkMpSeller::getSellerDetailByCustomerId($this->context->customer->id);
            if ($isSellerExist && $isSellerExist['active']) {
                return;
            } else {
                if (Module::isEnabled('mpsellerstaff')) {
                    $staffDetails = WkMpSellerStaff::getStaffInfoByIdCustomer($this->context->customer->id);
                    if ($staffDetails) {
                        return;  //If current customer is any seller's staff, can't register as seller
                    }
                }
            }
        }
        $this->context->smarty->assign(
            'sellerLink',
            $this->context->link->getModuleLink('marketplace', 'sellerrequest')
        );
        return $this->fetch('module:marketplace/views/templates/hook/advertisement.tpl');
    }

    /**
     * Add custom CSS and Ad link on pages.
     *
     * If you dont want custom CSS, Define Marketplace::$mpController = false,
     * in init() function in your front controller,
     */
    public function hookDisplayHeader()
    {
        // Apply custome CSS for all MP and MP addons controllers
        if (isset($this->context->controller->module)) {
            if (($this->context->controller->module->name == $this->name
                || in_array($this->name, $this->context->controller->module->dependencies))
                && self::$mpController) {
                if (Configuration::get('WK_MP_ALLOW_CUSTOM_CSS')) {
                    $this->context->controller->registerStylesheet(
                        'module-marketplace-custom-style-css',
                    