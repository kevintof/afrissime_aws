<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkMpCustomerDelivery extends ObjectModel
{
    public $seller_customer_id;
    public $carrier_id;
    public $carrier_name;
    public $duration;
    public $delivery_way;

    public static $definition = array(
        'table' => 'wk_mp_customer_delivery_detail',
        'primary' => 'id_customer_delivery',
        'fields' => array(
            'seller_customer_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'carrier_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'carrier_name' => array('type' => self::TYPE_STRING),
            'duration' => array('type' => self::TYPE_STRING),
            'delivery_way' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
        ),
    );

    /**
     * Get Delivery Details by using Customer ID
     *
     * @param  int $id_customer Customer ID
     * @return array
     */
    public function getDeliveryDetailByIdCustomer($idCustomer)
    {
        return Db::getInstance()->getRow(
            'SELECT mcpd.*
            FROM `'._DB_PREFIX_.'wk_mp_customer_delivery_detail` mcpd
            WHERE mcpd.`seller_customer_id` = '.(int) $idCustomer
        );
    }
    /**
     * Get Delivery Details by using Customer ID
     *
     * @param  int $id_customer Customer ID
     * @return array
     */
    public function getAlldeliveryDetailByIdCustomer($idCustomer)
    {
        return Db::getInstance()->executeS(
            'SELECT mcpd.*
            FROM `'._DB_PREFIX_.'wk_mp_customer_delivery_detail` mcpd
            WHERE mcpd.`seller_customer_id` = '.(int) $idCustomer
        );
    }
    /**
     * Get Delivery Detail by using primary ID, we can also create object of this class by using ID instead
     *
     * @param  int $id Primary ID
     * @return array
     */
    public static function getDeliveryDetailById($id)
    {
        return Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'wk_mp_customer_delivery_detail` WHERE `id_customer_delivery` = '.(int) $id);
    }
}
