<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkMpCustomerPayment extends ObjectModel
{
    public $seller_customer_id;
    public $payment_mode_id;
    public $payment_detail;
    //ajout des nouveaux champs
    public $bank_name;
    public $rib;
    public $name_card;
    public $num_card;
    public $card_expiration;
    public $num_mobile;
    public $cash_operator;
    public $num_cash;
    public $bitcoin_address;
    public $nickname_bitcoin;
    public $wallet_number;
    //fin

    public static $definition = array(
        'table' => 'wk_mp_customer_payment_detail',
        'primary' => 'id_customer_payment',
        'fields' => array(
            'seller_customer_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'payment_mode_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'payment_detail' => array('type' => self::TYPE_STRING),
            // ajout des nouveaux champs
            'bank_name' => array('type' => self::TYPE_STRING),
            'rib' => array('type' => self::TYPE_STRING),
            'name_card' => array('type' => self::TYPE_STRING),
            'num_card' => array('type' => self::TYPE_STRING),
            'card_expiration' => array('type' => self::TYPE_STRING),
            'num_mobile' => array('type' => self::TYPE_STRING),
            'bitcoin_address' => array('type' => self::TYPE_STRING),
            'wallet_number' => array('type' => self::TYPE_STRING),
            'cash_operator' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            //fin 
        ),
    );

    /**
     * Get Payment Details by using Customer ID
     *
     * @param  int $id_customer Customer ID
     * @return array
     */
    public function getPaymentDetailByIdCustomer($idCustomer)
    {
        return Db::getInstance()->getRow(
            'SELECT mcpd.*, mpm.`payment_mode`
            FROM `'._DB_PREFIX_.'wk_mp_customer_payment_detail` mcpd
            LEFT JOIN  `'._DB_PREFIX_.'wk_mp_payment_mode` mpm ON (mcpd.`payment_mode_id`= mpm.`id_mp_payment`)
            WHERE mcpd.`seller_customer_id` = '.(int) $idCustomer
        );
    }

    /**
     * Get Payment Details by using Customer ID
     *
     * @param  int $id_customer Customer ID
     * @return array
     */
    public function getAllPaymentDetailByIdCustomer($idCustomer)
    {
        return Db::getInstance()->executeS(
            'SELECT mcpd.*, mpm.`payment_mode`
            FROM `'._DB_PREFIX_.'wk_mp_customer_payment_detail` mcpd
            LEFT JOIN  `'._DB_PREFIX_.'wk_mp_payment_mode` mpm ON (mcpd.`payment_mode_id`= mpm.`id_mp_payment`)
            WHERE mcpd.`seller_customer_id` = '.(int) $idCustomer
        );
    }
    /**
     * Get Payment Detail by using primary ID, we can also create object of this class by using ID instead
     *
     * @param  int $id Primary ID
     * @return array
     */
    public static function getPaymentDetailById($id)
    {
        return Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'wk_mp_customer_payment_detail` WHERE `id_customer_payment` = '.(int) $id);
    }
}
