<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkMpWishlist extends ObjectModel
{
    public $id_customer ;
    public $wording;

    public static $definition = [
        'table' => 'wk_marketplace_wishlist',
        'primary' => 'id_marketplace_wishlist',
        'fields' => [
            'id_customer' => ['type' => self::TYPE_STRING, 'required' => true],
            'wording' => ['type' => self::TYPE_STRING, 'required' => true],
        ],
        'associations'=> [
            'customer' => [ 'type'=>self::HAS_ONE, 'required'=>true, 'field' => 'id_customer', 'object' => 'Customer' ]
        ]
    ];

    public static function printPaginationLink(Link $link, int $current, int $n_pages)
    {
        $current_minus_1 = $current - 1 ;
        $current_plus_1 = $current + 1 ;
        $limit_inf = 2 ;
        $limit_sup = $n_pages ;

        $pagination = '<ul class="pagination">' ;
        $pagination .= '<li> <a title="allez à la page une" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>1]).'">Première</a> </li>' ;
        if($current <= 1)
            $pagination .= '<li class="disabled"><a>Précedent</a></li>' ;
        else
            $pagination .= '<li><a class="" title="allez à la page précédente" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>$current_minus_1]).'">Précedent</a></li>' ;

        // Si on a moins de 8 pages, afficher toute le pagination de 1 à n
        if($n_pages <= 15)
        {
            for($i = 1; $i<=$n_pages; $i++)
                $pagination .= '<li><a class="" title="allez à la page '.$i.'" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>$i]).'">'.$i.'</a></li>' ;
        }
        else
        {
            $pagination .= '<li><a class="to-hide " title="allez à la page 1" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>1]).'">1</a></li>' ;
            $pagination .= '<li><a class="to-hide " title="allez à la page 2" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>2]).'">2</a></li>' ;
            $pagination .= '<li class="disabled"><a class="to-hide">...</a></li>' ;

            if($current > $limit_inf+1 && $current < $limit_sup-1)
            {
                $pagination .= '<li><a class="to-hide " title="allez à la page '.$current_minus_1.'" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>$current_minus_1]).'">'.$current_minus_1.'</a></li>' ;
                $pagination .= '<li class="active"><a class="to-hide" title="allez à la page '.$current.'">'.$current.'</a></li>' ;
                $pagination .= '<li><a class="to-hide " title="allez à la page '.$current_plus_1.'" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>$current_plus_1]).'">'.$current_plus_1.'</a></li>' ;
                $pagination .= '<li class="disabled"><a class="to-hide">...</a></li>' ;
            }
            $pagination .= '<li><a class="to-hide " title="allez à la page '.($n_pages-1).'" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>($n_pages-1)]).'">'.($n_pages-1).'</a></li>' ;
            $pagination .= '<li><a class="to-hide " title="allez à la page '.$n_pages.'" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>($n_pages)]).'">'.$n_pages.'</a></li>' ;
        }
        if($current == $n_pages)
            $pagination .= '<li class="disabled to-hide"><a>Suivant</a></li>' ;
        else
            $pagination .= '<li><a class="" title="allez à la page suivante" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>$current_plus_1]).'">Suivant</a></li>' ;

        $pagination .= '<li><a class="to-hide " title="allez à la dernière page" href="'.$link->getModuleLink('marketplace', 'mywishlist', ['offset'=>($n_pages)]).'">Dernière</a></li>' ;

        $pagination .= '</ul>' ;

        return $pagination ;
    }

    public static function customerWishlistSize(int $customer_id)
    {
        $db = Db::getInstance() ;
        $count = $db->executeS(
            (new DbQuery())
                ->select("COUNT(id_customer) as n")
                ->from('marketplace_wishlist')
                ->where("id_customer = {$customer_id}")
        ) ;

        return $count[0]['n'] ;
    }

    public static function getCustomerWishlists(int $cutomer_id, int $limit = 0, int $offset = 0)
    {
        $db = Db::getInstance() ;
        // récupération des wishlists
        $wishlists = $db->executeS(
            (new DbQuery())->from('marketplace_wishlist')
            ->where("id_customer = {$cutomer_id}")
            ->limit($limit, $limit*$offset)
        ) ;

        for($i = 0; $i<count($wishlists); $i++)
        {
            $wishlists[$i]['shop'] = $db->executeS(
                (new DbQuery())->select("shop_name_unique, id_marketplace_wishlist, s.id_seller, s.shop_image,s.id_country, s.seller_town")->from('marketplace_wishlist_seller_assoc')
                    ->leftJoin('wk_mp_seller', 's', 's.id_seller=ps_marketplace_wishlist_seller_assoc.id_seller')
                    ->where("id_marketplace_wishlist = {$wishlists[$i]['id_marketplace_wishlist']}")
            ) ;
        }

        return $wishlists ;
    }
}
