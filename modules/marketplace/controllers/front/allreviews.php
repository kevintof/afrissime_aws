<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class MarketplaceAllReviewsModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $shopLinkRewrite = Tools::getValue('mp_shop_name');
        $mpSeller = WkMpSeller::getSellerByLinkRewrite($shopLinkRewrite, $this->context->language->id);
        if ($mpSeller) {
            $idSeller = $mpSeller['id_seller'];

            //Rating Information
            if ($avgRating = WkMpSellerReview::getSellerAvgRating($idSeller)) {
                Media::addJsDef(array(
                    'avg_rating' => $avgRating,
                    'module_dir' => _MODULE_DIR_,
                ));
            }

            $n = Configuration::get('PS_PRODUCTS_PER_PAGE');
            $p = Tools::getValue('p');
            if (!$p) {
                $p = 1; // default page number
            }
            // Set left Image column
            $this->setLeftImageBlock($mpSeller['shop_image']);
            //Check if shop banner exist
            $shopBannerPath = WkMpSeller::getShopBannerLink($mpSeller);
            if ($shopBannerPath) {
                $this->context->smarty->assign('shop_banner_path', $shopBannerPath);
            }
            if ($mpSeller['id_country']) {
                $mpSeller['country'] = Country::getNameById($this->context->language->id, $mpSeller['id_country']);
            }
            if ($mpSeller['id_state']) {
                $mpSeller['state'] = State::getNameById($mpSeller['id_state']);
            }
            //Review details
            $objReview = new WkMpSellerReview();
            $sellerReview = $objReview->getSellerReviewByIdSeller($idSeller);
            if ($sellerReview) {
                $nbReviews = count($sellerReview);
                $sellerReview = $this->filterReviewsByPage($sellerReview, $p, $n);

                foreach ($sellerReview as &$review) {
                    //get name from prestashop customer table
                    $customer = new Customer($review['id_customer']);
                    $review['customer_name'] = $customer->firstname.' '.$customer->lastname;

                    //Get Customer review record - Is helpful or not
                    if ($this->context->customer->id) {
                        $customerReviewDetails = $objReview->isReviewHelpfulForCustomer(
                            $this->context->customer->id,
                            $review['id_review']
                        );
                        if ($customerReviewDetails) {
                            $review['like'] = $customerReviewDetails['like'];
                        }
                    }

                    //Get Total likes(helpful) or dislikes (not helpful) on particular review
                    $reviewDetails = $objReview->getReviewHelpfulSummary($review['id_review']);
                    if ($reviewDetails) {
                        $review['total_likes'] = $reviewDetails['total_likes'];
                        $review['total_dislikes'] = $reviewDetails['total_dislikes'];
                    }
                }

                //Sort review list according to admin configuration (By default it will display sort by recent review)
                if (Configuration::get('WK_MP_REVIEW_DISPLAY_SORT') == '2') { // 2 for most helpful
                    $sellerReview = $objReview->sortingReviewList($sellerReview);
                }

                $this->context->smarty->assign(array(
                    'reviews_info' => $sellerReview,
                    'shopLinkRewrite' => $shopLinkRewrite,
                    'shopLinkReviews' => $this->context->link->getModuleLink('marketplace', 'allreviews', array('mp_shop_name' => $shopLinkRewrite)),
                    'nbReviews' => $nbReviews,
                    'p' => $p,
                    'n' => $n,
                    'page_count' => (int) ceil($nbReviews/$n),
                    'myAccount' => 'index.php?controller=authentication&back='.urlencode($this->context->link->getModuleLink('marketplace', 'allreviews', array('mp_shop_name' => $shopLinkRewrite))),
                ));

                // Assign the seller details view vars
                WkMpSeller::checkSellerAccessPermission($mpSeller['seller_details_access']);

                //Display seller rating summary
                if ($sellerRating = WkMpSellerReview::getSellerAvgRating($idSeller)) {
                    $totalReview = $nbReviews;

                    //Get seller rating full summary
                    $sellerRatingDetail = WkMpSellerReview::getSellerRatingSummary($idSeller, $totalReview);

                    $this->context->smarty->assign(
                        array(
                            'sellerRating' => $sellerRating,
                            'sellerRatingDetail' => $sellerRatingDetail,
                            'totalReview' => $totalReview,
                        )
                    );

                    Media::addJsDef(array(
                        'sellerRating' => $sellerRating,
                        'rating_start_path' => _MODULE_DIR_.$this->module->name.'/views/img/',
                        'totalReview' => $totalReview,
                    ));
                }
            }

            $this->context->smarty->assign(
                array(
                'mp_seller_info' => $mpSeller,
                'customer_email' => $this->context->customer->email,
                'seller_id' => $idSeller
                )
            );
            $this->defineJSVars();
            $this->setTemplate('module:marketplace/views/templates/front/seller/allreviews.tpl');
        } else {
            //echo "test"; exit;
            Tools::redirect($this->context->link->getPageLink('pagenotfound'));
        }
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array(
            'title' => $this->module->l('Marketplace', 'shopstore'),
            'url' => $this->context->link->getModuleLink('marketplace', 'dashboard')
        );

        $breadcrumb['links'][] = array(
            'title' => $this->module->l('Shop', 'shopstore'),
            'url' => ''
        );

        return $breadcrumb;
    }
    public function setLeftImageBlock($mpShopImage)
    {
        if ($mpShopImage && file_exists(_PS_MODULE_DIR_.'marketplace/views/img/shop_img/'.$mpShopImage)) {
            $this->context->smarty->assign('seller_img_path', _MODULE_DIR_.'marketplace/views/img/shop_img/'.$mpShopImage);
            $this->context->smarty->assign('seller_img_exist', 1);
        } else {
            $this->context->smarty->assign('seller_img_path', _MODULE_DIR_.'marketplace/views/img/shop_img/defaultshopimage.jpg');
        }
    }
    public function defineJSVars()
    {
        $jsVars = array(
                'contact_seller_ajax_link' => $this->context->link->getModuleLink('marketplace', 'contactsellerprocess'),
                'logged' => $this->context->customer->isLogged(),
                'some_error' => $this->module->l('Some error occured...', 'allreviews'),
            );

        Media::addJsDef($jsVars);
    }

    public function filterReviewsByPage($sellerReview, $p, $n)
    {
        $result = array();
        if ($sellerReview) {
            $start = ($p - 1) * $n;
            $end = $start + $n;
            for ($i = $start; $i < $end; $i++) {
                if (array_key_exists($i, $sellerReview)) {
                    $result[] = $sellerReview[$i];
                }
            }
        }

        return $result;
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->registerStylesheet(
            'mp_store_profile',
            'modules/'.$this->module->name.'/views/css/mp_store_profile.css'
        );

        $this->registerStylesheet(
            'marketplace_account',
            'modules/'.$this->module->name.'/views/css/marketplace_account.css'
        );

        $this->registerStylesheet(
            'mp_seller_rating-css',
            'modules/'.$this->module->name.'/views/css/mp_seller_rating.css'
        );

        $this->registerJavascript(
            'contactseller-js',
            'modules/'.$this->module->name.'/views/js/contactseller.js'
        );

        $this->registerJavascript(
            'mp-jquery-raty-min',
            'modules/'.$this->module->name.'/views/js/libs/jquery.raty.min.js'
        );

        $this->registerJavascript(
            'mp_review_like-js',
            'modules/'.$this->module->name.'/views/js/mp_review_like.js'
        );
    }
}
