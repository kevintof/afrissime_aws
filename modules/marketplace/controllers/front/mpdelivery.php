<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class MarketplaceMpDeliveryModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        if (isset($this->context->customer->id)) {
            $seller = WkMpSeller::getSellerDetailByCustomerId($this->context->customer->id);
            if ($seller && $seller['active']) {
                $mpDelivery = new WkMpCustomerDelivery();

                //if seller edit or delete delivery details
                if ($idmpDelivery = Tools::getValue('id')) {
                    if (WkMpCustomerDelivery::getDeliveryDetailById($idmpDelivery)) {
                        if (Tools::getValue('delete_delivery')) {
                            //if seller delete delivery mode
                            $objDeliveryDetail = new WkMpCustomerDelivery($idmpDelivery);
                            if ($objDeliveryDetail->delete()) {
                                Tools::redirect($this->context->link->getModuleLink('marketplace', 'mpdelivery', array('deleted' => 1)));
                            }
                        } else {
                            $this->context->smarty->assign('edit', 1);
                        }
                    } else {
                        Tools::redirect($this->context->link->getModuleLink('marketplace', 'mpdelivery'));
                    }
                }
                //get seller all selected carriers
                if($idmpDelivery = Tools::getValue('id')){
                    $sellerDelivery = $mpDelivery->getDeliveryDetailById($idmpDelivery);
                    $this->context->smarty->assign('seller_delivery_details', $sellerDelivery);
                }
                else{
                    $sellerDelivery = $mpDelivery->getAllDeliveryDetailByIdCustomer($this->context->customer->id);
                    $this->context->smarty->assign('seller_delivery_details', $sellerDelivery);
                }
                 
                
                $this->context->smarty->assign(array(
                        'customer_id' => $this->context->customer->id,
                        'is_seller' => $seller['active'],
                        'logic' => 7,
                    ));

                Media::addJsDef(array(
                        'required_payment' => $this->module->l('Delivery mode is required field.', 'mpdelivery'),
                        'confirm_msg' => $this->module->l('Are you sure want to delete?', 'mpdelivery'),
                    ));

                $this->setTemplate('module:marketplace/views/templates/front/mpdelivery.tpl');
            } else {
                Tools::redirect($this->context->link->getModuleLink('marketplace', 'sellerrequest'));
            }
        } else {
            Tools::redirect('index.php?controller=authentication&back='.urlencode($this->context->link->getModuleLink('marketplace', 'mpdelivery')));
        }
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submit_delivery_details') && $this->context->customer->id) {
            $seller = WkMpSeller::getSellerDetailByCustomerId($this->context->customer->id);
            if ($seller && $seller['active']) {
                $idmpDelivery = Tools::getValue('id');
                $tranporter = Tools::getValue('transporter');
                $delivery_way = Tools::getValue('delivery_way');
                $duration = Tools::getValue('duration');
                if (empty($tranporter)) {
                    $this->errors[] = $this->module->l('Carrier name is required field.', 'mpDelivery');
                }
                if (empty($delivery_way)) {
                    $this->errors[] = $this->module->l('Delivery method is required field.', 'mpDelivery');
                }
                if (empty($delivery_way)) {
                    $this->errors[] = $this->module->l('Duration is required field.', 'mpDelivery');
                }
                if (empty($this->errors)) {
                    if ($idmpDelivery) {
                        $mpdelivery = new WkMpCustomerDelivery($idmpDelivery);
                        $mpdelivery->seller_customer_id = $this->context->customer->id;
                        $mpdelivery->carrier_name=$tranporter[0];
                        $mpdelivery->delivery_way= $delivery_way[0];
                        $mpdelivery->duration=$duration[0];
                        $mpdelivery->save();
                    } 
                    else {
                        for($i=0; $i<count($tranporter); $i++){
                            $mpdelivery = new WkMpCustomerDelivery();
                            $mpdelivery->seller_customer_id = $this->context->customer->id;
                            $mpdelivery->carrier_name=$tranporter[$i];
                            $mpdelivery->delivery_way= $delivery_way[$i];
                            $mpdelivery->duration=$duration[$i];
                            $mpdelivery->save();
                        }
                    }
                    if ($idmpDelivery) {
                        Tools::redirect($this->context->link->getModuleLink('marketplace', 'mpdelivery', array('edited' => 1)));
                    } else {
                        Tools::redirect($this->context->link->getModuleLink('marketplace', 'mpdelivery', array('created' => 1)));
                    }
                }
            }
        }
    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array(
            'title' => $this->module->l('Marketplace', 'mpdelivery'),
            'url' => $this->context->link->getModuleLink('marketplace', 'dashboard')
        );

        $breadcrumb['links'][] = array(
            'title' => $this->module->l('Delivery details', 'mpdelivery'),
            'url' => ''
        );
        return $breadcrumb;
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->registerStylesheet('marketplace_account-css', 'modules/'.$this->module->name.'/views/css/marketplace_account.css');
        $this->registerJavascript('mp_form_validation-js', 'modules/'.$this->module->name.'/views/js/mp_form_validation.js');
    }
}
