<?php

/**
 * @author fatigba IN <fatigba72@gmail.com>
 */
class MarketplaceAddToWishlistModuleFrontController extends ModuleFrontController
{
    /**
     * @var Db
     */
    protected $db ;

    public function initContent()
    {
        parent::initContent();
        $this->db = Db::getInstance() ;
        $this->ajax = TRUE ;
    }

    public function displayAjax()
    {
        // do better after
        $type = Tools::getValue("type", null);
        if (is_null($type)) {
            throw new \InvalidArgumentException("type value nust be 'new_list_add' or 'old_list_add'");
        }
        $flag = false ;

        if (strcmp($type, "new_list_add") == 0)
        {
            $customerId = strip_tags(Tools::getValue("customer_id", 0)) ;
            $sellerId = strip_tags(Tools::getValue("seller_id", 0)) ;
            $listName = strip_tags(Tools::getValue("list_name", "")) ;

            if(strlen($listName) == 0) {
                throw new \InvalidArgumentException("The new list name is empty") ;
            }
            if($customerId <= 0) {
                throw new \InvalidArgumentException("Vous devez etre connecté !") ;
            }

            if($customerId <= 0 || $sellerId <= 0) {
                throw new \InvalidArgumentException("customer_id or seller_id is not set") ;
            }

            $flag = $this->newListAdd($customerId, $sellerId, $listName) ;
        }
        else if (strcmp($type, "old_list_add") == 0)
        {
            $customerId = strip_tags(Tools::getValue("customer_id", 0)) ;
            $sellerId = strip_tags(Tools::getValue("seller_id", 0)) ;
            $listId = strip_tags(Tools::getValue("list_id", 0)) ;

            if(strlen($listId) <= 0) {
                throw new \InvalidArgumentException("list_id is not set") ;
            }

            if($customerId <= 0 || $sellerId <= 0) {
                throw new \InvalidArgumentException("customer_id or seller_id is not set") ;
            }

            try {
                $flag = $this->OldListAdd($listId, $sellerId);
            } catch (WishlistInsertionException $e) {
                die(json_encode(['result' => "KO", "message"=>$e->getMessage(), "httpCode"=>403]));
            }
        }
        if($flag) {
            die(json_encode(['result' => "OK"]));
        }
        die(json_encode(['result' => "KO", "message"=>"Une erreur inconnue est survenue, veuillez reprendre.", "httpCode"=>500]));
    }

    /**
     * ok
     * @param int $customerId
     * @param int $sellerId
     * @param string $listName
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    private function newListAdd(int $customerId, int $sellerId, string $listName)
    {
        // Insertion de la liste
        $state = $this->db->insert("marketplace_wishlist", [
                "id_customer" => $customerId,
                "wording" => $listName,
            ]);;
        if (!$state)
            return false;

        $idWishlist = $this->db->Insert_ID();
        // Insertion dans le table d'association
        $state = $state && $this->db->insert("marketplace_wishlist_seller_assoc", [
                        "id_marketplace_wishlist" => $idWishlist,
                        "id_seller" => $sellerId,
                    ]
                );
        return $state;
    }

    private function OldListAdd(int $listId, int $sellerId)
    {
        // insertion pour une liste existante
        $flag = count($this->db->executeS((new DbQuery())->from('marketplace_wishlist_seller_assoc')
            ->where("id_marketplace_wishlist = {$listId} AND id_seller = {$sellerId}"))) ;
        if($flag > 0) {
            throw new WishlistInsertionException("La boutique est déjà dans la liste", $flag) ;
        }

        return $this->db->insert("marketplace_wishlist_seller_assoc", [
                    "id_marketplace_wishlist" => $listId,
                    "id_seller" => $sellerId,
                ]
            );
    }
}

class WishlistInsertionException extends Exception
{
    public function __construct($message = "", $code = 0) {
        parent::__construct($message, $code, null) ;
    }
}
