<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


/**
 * This controller is responsible to display favourite shop list page
 * Class MarketplaceMyWishListModuleFrontController
 */
class MarketplaceMyWishListModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	private $perPage ;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
		$this->perPage = 15 ;
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
//        echo '<pre>' . var_export(WkMpWishlist::getCustomerWishlists($this->context->customer->id), true) . '</pre>';
//        die() ;
        $errors = array();

        if ($this->context->customer->isLogged())
        {
            $offset = Tools::getValue("offset", 1) ;
            $count = WkMpWishlist::customerWishlistSize($this->context->customer->id) ;
            $this->context->smarty->assign([
                    "wishlists"=>WkMpWishlist::getCustomerWishlists($this->context->customer->id, $this->perPage, $offset-1),
                    "wishlistTotalSize"=>$count,
                    "paginationCode"=>WkMpWishlist::printPaginationLink($this->context->link, $offset,  ceil($count/$this->perPage)),
                    "currentPage"=>$offset
                ]
            ) ;
            //var_dump(WkMpWishlist::getCustomerWishlists($this->context->customer->id, $this->perPage, $offset-1)); exit;
        }
        else
            Tools::redirect('index.php?controller=authentication&back='.urlencode($this->context->link->getModuleLink('marketplace', 'mywishlist')));

        $this->context->smarty->assign([
            'id_customer' => $this->context->customer->isLogged() ? (int)$this->context->customer->id : -1,
            'errors' => $errors,
            'navigationPipe' => Configuration::get('PS_NAVIGATION_PIPE'),
            'form_link' => $errors,
            'link' => $this->context->link,
            'perPage'=>$this->perPage
        ]);

        $this->setTemplate('module:marketplace/views/templates/front/mywishlist.tpl');
	}
     public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array(
            'title' => $this->getTranslator()->trans('Account', [], 'Breadcrumb'),
            'url' => $this->context->link->getPageLink('my-account', true)
        );
        $breadcrumb['links'][] = array(
            'title' => $this->getTranslator()->trans('My favorites shop', [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('marketplace', 'mywishlist')
        );

        return $breadcrumb;
    }
}
