<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class MarketplaceSellerRequestModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $smartyVars = array();
        //if (isset($this->context->customer->id)) {
            if (Module::isEnabled('mpsellerstaff')) {
                //If customer is a staff then restrict Staff to use seller request page
                WkMpSellerStaff::overrideMpSellerCustomerId($this->context->customer->id);
            }
            $mpSeller = WkMpSeller::getSellerDetailByCustomerId($this->context->customer->id);
            if ($mpSeller = WkMpSeller::getSellerDetailByCustomerId($this->context->customer->id)) {
                $smartyVars['is_seller'] = $mpSeller['active'];

                // check if seller product exist and seller is active then redirect to dashboard page
                $mpProduct = WkMpSellerProduct::getSellerProduct($mpSeller['id_seller']);
                if ($mpProduct && $mpSeller['active']) {
                    Tools::redirect($this->context->link->getModulelink('marketplace', 'dashboard'));
                }
            }

            if (Configuration::get('WK_MP_TERMS_AND_CONDITIONS_STATUS')) {
                //Display CMS page link
                if (Configuration::get('WK_MP_TERMS_AND_CONDITIONS_CMS')) {
                    $objCMS = new CMS(Configuration::get('WK_MP_TERMS_AND_CONDITIONS_CMS'), $this->context->language->id);

                    $linkCmsPageContent = $this->context->link->getCMSLink($objCMS, $objCMS->link_rewrite, Configuration::get('PS_SSL_ENABLED'));
                    if (!strpos($linkCmsPageContent, '?')) {
                        $linkCmsPageContent .= '?content_only=1';
                    } else {
                        $linkCmsPageContent .= '&content_only=1';
                    }
                    $smartyVars['linkCmsPageContent'] = $linkCmsPageContent;
                }
            }

            $smartyVars['terms_and_condition_active'] = Configuration::get('WK_MP_TERMS_AND_CONDITIONS_STATUS');
            $smartyVars['max_phone_digit'] = Configuration::get('WK_MP_PHONE_DIGIT');

            $customer = new Customer($this->context->customer->id);
            $smartyVars['customer_firstname'] = $customer->firstname;
            $smartyVars['customer_lastname'] = $customer->lastname;
            $smartyVars['customer_email'] = $customer->email;
            if (Tools::getValue('signup')) {
                        $smartyVars['signup'] = 1;
            }

            if (Configuration::get('WK_MP_MULTILANG_ADMIN_APPROVE')) {
                $this->context->smarty->assign('allow_multilang', 1);
                $currentLang = $this->context->language->id;
            } else {
                $this->context->smarty->assign('allow_multilang', 0);
                if (Configuration::get('WK_MP_MULTILANG_DEFAULT_LANG') == '1') {//Admin default lang
                    $currentLang = Configuration::get('PS_LANG_DEFAULT');
                } elseif (Configuration::get('WK_MP_MULTILANG_DEFAULT_LANG') == '2') {//Seller default lang
                    $currentLang = $this->context->language->id;
                }
            }

            $this->context->smarty->assign($smartyVars);
            $this->context->smarty->assign(array(
                'id_module' => $this->module->id,
                'myaccount' => $this->context->link->getPageLink('my-account', true),
                'modules_dir' => _MODULE_DIR_,
                'static_token' => Tools::getToken(false),
                'ps_img_dir' => _PS_IMG_.'l/',
                'country' => Country::getCountries($this->context->language->id, true),
                'african_country' => Country::getAfricansCountries($this->context->language->id, true),
                'seller_country_need' => Configuration::get('WK_MP_SELLER_COUNTRY_NEED'),
                'tax_identification_number' => Configuration::get('WK_MP_SELLER_TAX_IDENTIFICATION_NUMBER'),
                'languages' => Language::getLanguages(),
                'total_languages' => count(Language::getLanguages()),
                'current_lang' => Language::getLanguage((int) $currentLang),
                'context_language' => $this->context->language->id,
            ));
            $this->jsDefVars();
            $this->setTemplate('module:marketplace/views/templates/front/seller/sellerrequest.tpl');
        /*
        } else {
            Tools::redirect('index.php?controller=authentication&back='.urlencode($this->context->link->getModuleLink('marketplace', 'sellerrequest')));
        }
        */
        parent::initContent();
    }

    public function jsDefVars()
    {
        $jsDef = array(
            'id_country' => 0,
            'id_state' => 0,
            'iso' => $this->context->language->iso_code,
            'multi_lang' => Configuration::get('WK_MP_MULTILANG_ADMIN_APPROVE'),
            'mp_tinymce_path' => _MODULE_DIR_.$this->module->name.'/libs',
            'img_module_dir' => _MODULE_DIR_.$this->module->name.'/views/img/',
            'path_sellerdetails' => $this->context->link->getModuleLink('marketplace', 'sellerrequest'),
            'seller_country_need' => Configuration::get('WK_MP_SELLER_COUNTRY_NEED'),
            'terms_and_condition_active' => Configuration::get('WK_MP_TERMS_AND_CONDITIONS_STATUS'),
            'selectstate' => $this->module->l('Select State', 'sellerrequest'),
            'req_shop_name_lang' => $this->module->l('Shop name is required in Default Language -', 'sellerrequest'),
           'shop_name_exist_msg' => $this->module->l('Shop Unique name already taken. Try another.', 'sellerrequest'),
           /*'shop_domainname_exist_msg' => $this->module->l("Le domaine n'est pas disponible .", 'sellerrequest'),*/
            'shop_name_error_msg' => $this->module->l('Shop name can not contain any special character except underscore. Try another.', 'sellerrequest'),
            'seller_email_exist_msg' => $this->module->l('Email Id already exist.', 'sellerrequest'),
        );

        Media::addJsDef($jsDef);
    }
    
    public function postProcess()
    {
        if (Tools::isSubmit('sellerRequest')) {
            $shopNameUnique = strtolower(trim(Tools::getValue('shop_name_unique')));
            $sellerFirstName = trim(Tools::getValue('seller_firstname'));
            $sellerLastName = trim(Tools::getValue('seller_lastname'));
            $sellerPhone = trim(Tools::getValue('phone'));
            $phoneShop = trim(Tools::getValue('phone_shop'));
            $customerEmail = trim(Tools::getValue('customer_email'));
            $businessEmail = trim(Tools::getValue('business_email'));
            $sellerAddress = trim(Tools::getValue('address'));
            $sellerPostalcode = trim(Tools::getValue('seller_postalcode'));


            if (Tools::getValue('seller_gender')) {
                $sellerGender = Tools::getValue('seller_gender');
            } else {
                $sellerGender = 1;
            }
            if (Tools::getValue('seller_birthday')) {
                $sellerBirthday = Tools::getValue('seller_birthday');
                $selBirthday = explode("/",$sellerBirthday);
                if(Tools::getValue('default_lang')==1){
                    $sellerBirthday =$selBirthday[2]."-".$selBirthday[1]."-".$selBirthday[0];
                }
                if(Tools::getValue('default_lang')==2){
                    $sellerBirthday =$selBirthday[0]."-".$selBirthday[1]."-".$selBirthday[2];
                }
            } else {
                $sellerBirthday = "";
            }
            if (Tools::getValue('seller_title')) {
                $sellerTitle = Tools::getValue('seller_title');
            } else {
                $sellerTitle = "";
            }
            if (Tools::getValue('postcode')) {
                $sellerPostalCode = trim(Tools::getValue('postcode'));
            } else {
                $sellerPostalCode = '';
            }

            if (Tools::getValue('city')) {
                $sellerCity = trim(Tools::getValue('city'));
            } else {
                $sellerCity = '';
            }
            if (Tools::getValue('seller_town')) {
                $sellerTown = trim(Tools::getValue('seller_town'));
            } else {
                $sellerTown = '';
            }
            if (Tools::getValue('seller_district')) {
                $sellerDistrict = trim(Tools::getValue('seller_district'));
            } else {
                $sellerDistrict = '';
            }

            if (Tools::getValue('id_country')) {
                $sellerCountryId = Tools::getValue('id_country');
            } else {
                $sellerCountryId = 0;
            }

            if (Tools::getValue('seller_nationality')) {
                $sellerNationality = Tools::getValue('seller_nationality');
            } else {
                $sellerNationality = 0;
            }

            if (Tools::getValue('id_state')) {
                $sellerStateId = Tools::getValue('id_state');
            } else {
                $sellerStateId = 0;
            }

            if (Tools::getValue('tax_identification_number')) {
                $taxIdentificationNumber = Tools::getValue('tax_identification_number');
            } else {
                $taxIdentificationNumber = '';
            }

            if (Configuration::get('WK_MP_SELLER_ADMIN_APPROVE') == 0) {
                $active = 1;
            } else {
                $active = 0;
            }
            //If multi-lang is OFF then PS default lang will be default lang for seller
            if (Configuration::get('WK_MP_MULTILANG_ADMIN_APPROVE')) {
                $defaultLang = Tools::getValue('default_lang');
            } else {
                if (Configuration::get('WK_MP_MULTILANG_DEFAULT_LANG') == '1') {
                    $defaultLang = Configuration::get('PS_LANG_DEFAULT');
                } elseif (Configuration::get('WK_MP_MULTILANG_DEFAULT_LANG') == '2') {
                    $defaultLang = Tools::getValue('current_lang');
                }
            }

            //$shopName = trim(Tools::getValue('shop_name_'.$defaultLang));
            $shopName = trim(Tools::getValue('shop_name_unique'));
            //$shopNameUnique = strtolower(trim(Tools::getValue('shop_name_'.$defaultLang)));
            // validation des champs du formulaire
            $this->validateSellerRegistrationForm($defaultLang);

            /*code pour l'inscription du vendeur*/
                if (empty($this->context->customer->id)) {
                    $customer = new Customer();
                    $customer->lastname=$sellerLastName;
                    $customer->firstname =$sellerFirstName;
                    $customer->id_gender =$sellerGender;
                    $customer->birthday =$sellerBirthday;
                    $customer->phone =$sellerPhone;
                    $customer->seller_title =$sellerTitle;
                    $pwd=Tools::getValue('seller_password');
                    $customer->passwd=md5(_COOKIE_KEY_.$pwd);
                    $customer->email=$customerEmail;


                    $customer->firstname = Tools::ucwords($customer->firstname);
                    $customer->active = 1;
                    $customer->add();
                    $this->context->customer->id= $customer->id;
                }
                //Saving seller payment
                $idMpPayment = $this->context->customer->id;
                $paymentMode = Tools::getValue('payment_mode_id');
                if(!empty($paymentMode)){
                    if ($idMpPayment) {
                        $mpPayment = new WkMpCustomerPayment($idMpPayment);
                    } 
                    for($i=0;$i<count($paymentMode); $i++){
                        $mpPayment = new WkMpCustomerPayment();
                        $mpPayment->seller_customer_id = $this->context->customer->id;
                        $mpPayment->payment_mode_id = $paymentMode[$i];
                        switch ($paymentMode[$i]) {
                            case '1':
                                $mpPayment->bank_name = Tools::getValue('bank_name');
                                $mpPayment->rib = Tools::getValue('rib');
                                break;
                            
                            case '2':
                                $mpPayment->name_card = Tools::getValue('name_card');
                                $mpPayment->num_card = Tools::getValue('num_card');
                                $mpPayment->card_expiration = Tools::getValue('card_expiration');
                                break;
                            case '3':
                                $mpPayment->num_mobile = Tools::getValue('num_mobile');
                                break;
                            case '4':
                                $mpPayment->num_cash = Tools::getValue('num_cash');
                                $mpPayment->cash_operator = Tools::getValue('cash_operator');
                                break;
                            case '5':
                                $mpPayment->bitcoin_address = Tools::getValue('bitcoin_address');
                                $mpPayment->nickname_bitcoin = Tools::getValue('nickname_bitcoin');
                                break;
                            case '6':
                                $mpPayment->wallet_number = Tools::getValue('wallet_number');
                                break;
                        }
                    $mpPayment->save();
                    }
                }
                // Saving seller delivery details
                
                $tranporter = Tools::getValue('transporter');
                $delivery_way = Tools::getValue('delivery_way');
                $duration = Tools::getValue('duration');
               
                if(!empty($tranporter) && !empty($duration) && !empty($delivery_way)){
                    for($i=0; $i<count($tranporter); $i++){
                        $mpdelivery = new WkMpCustomerDelivery();
                        $mpdelivery->seller_customer_id = $this->context->customer->id;
                        $mpdelivery->carrier_name=$tranporter[$i];
                        $mpdelivery->delivery_way= $delivery_way[$i];
                        $mpdelivery->duration=$duration[$i];
                        $mpdelivery->save();
                    }
                }    
           
            /*fin code*/
            Hook::exec('actionBeforeAddSeller', array('id_customer' => $this->context->customer->id));
            //Saving seller details
            //if  (empty($this->errors)) {
                $objMpSeller = new WkMpSeller();
                $objMpSeller->shop_name_unique = $shopNameUnique;
                $objMpSeller->shop_name = $shopName;
                $objMpSeller->link_rewrite = Tools::link_rewrite($shopNameUnique);
                $objMpSeller->seller_firstname = $sellerFirstName;
                $objMpSeller->seller_lastname = $sellerLastName;
                $objMpSeller->business_email = $businessEmail;
                $objMpSeller->phone = $sellerPhone;
                //ajout de nouveaux champs
                $objMpSeller->seller_gender = $sellerGender;
                $objMpSeller->seller_nationality = $sellerNationality;
                $objMpSeller->seller_email = $customerEmail;
                $objMpSeller->phone_shop = $phoneShop;
                $objMpSeller->address = Tools::getValue('address');
                $objMpSeller->seller_town = $sellerTown;
                $objMpSeller->seller_district = $sellerDistrict;
                $objMpSeller->postcode = $sellerPostalCode;
                // fin ajout
                
                $objMpSeller->city = $sellerCity;
                $objMpSeller->id_country = $sellerCountryId;
                $objMpSeller->id_state = $sellerStateId;
                $objMpSeller->tax_identification_number = $taxIdentificationNumber;
                $objMpSeller->default_lang = Tools::getValue('default_lang');
                $objMpSeller->active = $active;
                $objMpSeller->shop_approved = $active;

                $objMpSeller->seller_customer_id = $this->context->customer->id;

                if (Configuration::get('WK_MP_SHOW_SELLER_DETAILS')) {
                    //display all seller details for new seller
                    $objMpSeller->seller_details_access = Configuration::get('WK_MP_SELLER_DETAILS_ACCESS');
                }

                foreach (Language::getLanguages(false) as $language) {
                    $shopIdLang = $language['id_lang'];

                    if (Configuration::get('WK_MP_MULTILANG_ADMIN_APPROVE')) {
                        //if shop name in other language is not available then fill with seller language same for others
                        if (!Tools::getValue('shop_name_'.$language['id_lang'])) {
                            $shopIdLang = $defaultLang;
                        }
                    } else {
                        //if multilang is OFF then all fields will be filled as default lang content
                        $shopIdLang = $defaultLang;
                    }

                    //$objMpSeller->shop_name[$language['id_lang']] = Tools::getValue('shop_name_'.$shopIdLang);
                    $objMpSeller->shop_name[$language['id_lang']] = Tools::getValue('shop_name_unique');
                }

                $objMpSeller->save();
                $idSeller = $objMpSeller->id;
                if ($idSeller) {
                    //If seller default active approval is ON then mail to seller of account activation
                    if ($objMpSeller->active) {
                        WkMpSeller::sendMail($idSeller, 1, 1);
                    }

                    //If mpsellerstaff module is installed but currently disabled and current customer was a staff then delete this customer as staff from mpsellerstaff module table. Because a customer can not be a seller and a staff both in same time.
                    if (Module::isInstalled('mpsellerstaff') && !Module::isEnabled('mpsellerstaff')) {
                        WkMpSeller::deleteStaffDataIfBecomeSeller($objMpSeller->seller_customer_id);
                    }

                    if (Configuration::get('WK_MP_MAIL_ADMIN_SELLER_REQUEST')) {
                        //Mail to Admin on seller request
                        $sellerName = $sellerFirstName.' '.$sellerLastName;
                        $objMpSeller->mailToAdminWhenSellerRequest($sellerName, $shopName, $businessEmail, $sellerPhone);
                    }

                    Hook::exec('actionAfterAddSeller', array('id_seller' => $idSeller));
                    Tools::redirect($this->context->link->getModuleLink('marketplace', 'sellerrequest',array('signup' => 1)));
                } else {
                    $this->errors[] = $this->module->l('Something wrong while creating seller.', 'sellerrequest');
                }
            //}
        }
    }

    public function validateSellerRegistrationForm($defaultLang)
    {
        $shopNameUnique = trim(Tools::getValue('shop_name_unique'));
        //$shopNameUnique = strtolower(trim(Tools::getValue('shop_name_'.$defaultLang)));
        $sellerFirstName = trim(Tools::getValue('seller_firstname'));
        $sellerLastName = trim(Tools::getValue('seller_lastname'));
        $sellerPhone = trim(Tools::getValue('phone'));
        $phoneShop = trim(Tools::getValue('phone_shop'));
        //nouveau champs ajoutés
        $sellerGender = trim(Tools::getValue('seller_gender'));
        $sellerTitle = trim(Tools::getValue('seller_title'));
        $sellerBirthday = trim(Tools::getValue('seller_birthday'));
        $sellerNationality = trim(Tools::getValue('seller_nationality'));
        $customerEmail = trim(Tools::getValue('customer_email'));
        //$paymentModeId = trim(Tools::getValue('paymentModeId'));
        //$sellerPhone = trim(Tools::getValue('seller_phone'));
        $sellerPassword = trim(Tools::getValue('seller_password'));
        //fin ajout 
        $sellerAddress = trim(Tools::getValue('address'));
        $businessEmail = trim(Tools::getValue('business_email'));
        //$shopName = trim(Tools::getValue('shop_name_'.$defaultLang));
        $shopName = trim(Tools::getValue('shop_name_unique'));
        $sellerLang = Language::getLanguage((int) $defaultLang);

        //$languages = Language::getLanguages();
        //foreach ($languages as $language) {
            if (!Validate::isCatalogName(Tools::getValue('shop_name_unique'))) {
                $invalidShopName = 1;
            }
        //}
        //Validation du nom de la boutique pour éviter les noms en doublon
        
        if (!$shopNameUnique) {
            $this->errors[] = $this->module->l('Unique name for shop is required field.', 'sellerrequest');
        } elseif (!Validate::isCatalogName($shopNameUnique) || !Tools::link_rewrite($shopNameUnique)) {
            $this->errors[] = $this->module->l('Invalid Unique name for shop', 'sellerrequest');
        } elseif (WkMpSeller::isShopNameExist(Tools::link_rewrite($shopNameUnique))) {
            $this->errors[] = $this->module->l('Unique name for shop is already taken. Try another.', 'sellerrequest');
        }
        if (!$sellerLastName) {
            $this->errors[] = $this->module->l('Seller last name is required field.', 'sellerrequest');
        } elseif (!Validate::isName($sellerLastName)) {
            $this->errors[] = $this->module->l('Invalid seller last name.', 'sellerrequest');
        }
        if (!$sellerFirstName) {
            $this->errors[] = $this->module->l('Seller first name is required field.', 'sellerrequest');
        } elseif (!Validate::isName($sellerFirstName)) {
            $this->errors[] = $this->module->l('Invalid seller first name.', 'sellerrequest');
        }
        /*
        if (!$sellerBirthday) {
            $this->errors[] = $this->module->l('birthdate is required field.', 'sellerrequest');
        } elseif (!Validate::isDate($sellerPhone)) {
            $this->errors[] = $this->module->l('Invalid birthdate format.', 'sellerrequest');
        }
        */
        /*if (!$paymentModeId) {
            $this->errors[] = $this->module->l('Payment mode is required field.', 'mppayment');
        }
        */
        if (!$sellerGender) {
            $this->errors[] = $this->module->l('Seller gender is required field.', 'sellerrequest');
        } elseif (!Validate::isName($sellerGender)) {
            $this->errors[] = $this->module->l('Invalid seller gender.', 'sellerrequest');
        }
        
        if (!$sellerPhone) {
            $this->errors[] = $this->module->l('Phone is required field.', 'sellerrequest');
        } elseif (!Validate::isPhoneNumber($sellerPhone)) {
            $this->errors[] = $this->module->l('Invalid phone number.', 'sellerrequest');
        }
        if (!$phoneShop) {
            $this->errors[] = $this->module->l('Phone is required field.', 'sellerrequest');
        } elseif (!Validate::isPhoneNumber($phoneShop)) {
            $this->errors[] = $this->module->l('Invalid phone number.', 'sellerrequest');
        }
        if (!$sellerNationality) {
                $this->errors[] = $this->module->l('Nationality is required field.', 'sellerrequest');
        }
        if (!Tools::getValue('residence')) {
                $this->errors[] = $this->module->l('Pays de résidence est un champ requis.', 'sellerrequest');
        }
        if (!$customerEmail) {
            $this->errors[] = $this->module->l('Email is required field.', 'sellerrequest');
        } elseif (!Validate::isEmail($customerEmail)) {
            $this->errors[] = $this->module->l('Invalid Email', 'sellerrequest');
        } elseif (WkMpSeller::isSellerEmailExist($customerEmail)) {
            $this->errors[] = $this->module->l('Email already exist.', 'sellerrequest');
        }
        if (!$sellerAddress) {
            $this->errors[] = $this->module->l('Seller address is required field.', 'sellerrequest');
        } 
        if (!$shopName) {
            if (Configuration::get('WK_MP_MULTILANG_ADMIN_APPROVE')) {
                $this->errors[] = sprintf($this->module->l('Shop name is required in %s', 'sellerrequest'), $sellerLang['name']);
            } else {
                $this->errors[] = $this->module->l('Shop name is required', 'sellerrequest');
            }
        } elseif (isset($invalidShopName)) {
            $this->errors[] = $this->module->l('Invalid Shop name', 'sellerrequest');
        }
       //var_dump($this->errors);exit;
        //Validate data
        $languages = Language::getLanguages();
        foreach ($languages as $language) {
            if (Tools::getValue('about_shop_'.$language['id_lang'])) {
                if (!Validate::isCleanHtml(Tools::getValue('about_shop_'.$language['id_lang']))) {
                    $invalidAboutShop = 1;
                }
            }
        }

        if (isset($invalidAboutShop)) {
            $this->errors[] = $this->module->l('Shop description does not have valid data.', 'editprofile');
        }

        if (!$businessEmail) {
            $this->errors[] = $this->module->l('Email ID is required field.', 'sellerrequest');
        } elseif (!Validate::isEmail($businessEmail)) {
            $this->errors[] = $this->module->l('Invalid Email ID.', 'sellerrequest');
        } elseif (WkMpSeller::isSellerEmailExist($businessEmail)) {
            $this->errors[] = $this->module->l('Email ID already exist.', 'sellerrequest');
        }

        if (Configuration::get('WK_MP_SELLER_TAX_IDENTIFICATION_NUMBER')) {
            $TINnumber = Tools::getValue('tax_identification_number');
            if ($TINnumber && !Validate::isGenericName($TINnumber)) {
                $this->errors[] = $this->module->l('Tax Identification Number must be valid.', 'sellerrequest');
            }
        }

        if (Configuration::get('WK_MP_SELLER_COUNTRY_NEED')) {
            $postcode = Tools::getValue('postcode');
            $countryNeedZipCode = true;
            $countryZipCodeFormat = false;
            if (Tools::getValue('id_country')) {
                $country = new Country(Tools::getValue('id_country'));
                $countryNeedZipCode = $country->need_zip_code;
                $countryZipCodeFormat = $country->zip_code_format;
            }
            // code pour vérifier la saisie au niveau du code postal
            /*
            if (!$postcode && $countryNeedZipCode) {
                $this->errors[] = $this->module->l('Zip/Postal Code is required field.', 'sellerrequest');
            } elseif ($countryZipCodeFormat) {
                if (!$country->checkZipCode($postcode)) {
                    $this->errors[] = sprintf($this->module->l('The Zip/Postal code you\'ve entered is invalid. It must follow this format: %s'), str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $countryZipCodeFormat))));
                }
            } elseif (!Validate::isPostCode($postcode)) {
                $this->errors[] = $this->module->l('Invalid Zip/Postal code', 'sellerrequest');
            }
            */
            $sellerCity = Tools::getValue('city');
            if (!$sellerCity) {
                $this->errors[] = $this->module->l('City is required field.', 'sellerrequest');
            } elseif (!Validate::isName($sellerCity)) {
                $this->errors[] = $this->module->l('Invalid city name.', 'sellerrequest');
            }
            if (!Tools::getValue('id_country')) {
                $this->errors[] = $this->module->l('Country is required field.', 'sellerrequest');
            }

            

            //if state available in selected country
            /*
            if (Tools::getValue('state_available')) {
                if (!Tools::getValue('id_state')) {
                    $this->errors[] = $this->module->l('State is required field.', 'sellerrequest');
                }
            }
            */

        }

        if (Configuration::get('WK_MP_TERMS_AND_CONDITIONS_STATUS') && !Tools::getValue('terms_and_conditions')) {
            $this->errors[] = $this->module->l('Please agree the terms and condition.', 'sellerrequest');
        }
        //var_dump($this->errors); exit;
    }

    public function displayAjaxCheckUniqueShopName()
    {
        if (!$this->isTokenValid()) {
            die('Something went wrong!');
        }
        //check unique shop name and compare to other existing shop name unique
        WkMpSeller::validateSellerUniqueShopName();
    }

    public function displayAjaxCheckUniqueSellerEmail()
    {
        if (!$this->isTokenValid()) {
            die('Something went wrong!');
        }
        //check seller email and compare to other existing seller email
        WkMpSeller::validateSellerEmail();
    }
    /*
    public function displayAjaxCheckUniqueCustomerEmail()
    {
        if (!$this->isTokenValid()) {
            die('Something went wrong!');
        }
        //check personal email and compare to other existing personal email
        WkMpSeller::validateCustomerEmail();
    }
    */
    public function displayAjaxCheckZipCodeByCountry()
    {
        if (!$this->isTokenValid()) {
            die('Something went wrong!');
        }

        //Display zip code field on the basis of country
        $countryNeedZipCode = true;
        if (Tools::getValue('id_country')) {
            $country = new Country(Tools::getValue('id_country'));
            $countryNeedZipCode = $country->need_zip_code;
        }

        if ($countryNeedZipCode) {
            die('1');
        } else {
            die('0');
        }
    }

    public function displayAjaxGetSellerState()
    {
        if (!$this->isTokenValid()) {
            die('Something went wrong!');
        }
        //Get state by choosing country
        WkMpSeller::displayStateByCountryId();
    }

    public function displayAjaxValidateMpSellerForm()
    {
        if (!$this->isTokenValid()) {
            die('Something went wrong!');
        }

        $params = array();
        parse_str(Tools::getValue('formData'), $params);
        if (!empty($params)) {
            WkMpSeller::validationSellerFormField($params);
        } else {
            die('1');
        }
    }

    public function getBreadcrumbLinks()
    {
        $mpURL = 'javascript:void(0)';
        if (isset($this->context->customer->id)) {
            if ($mpSeller = WkMpSeller::getSellerDetailByCustomerId($this->context->customer->id)) {
                if ($mpSeller['active']) {
                    $mpURL = $this->context->link->getModuleLink('marketplace', 'dashboard');
                }
            }
        }

        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array(
            'title' => $this->module->l('Marketplace', 'sellerrequest'),
            'url' => $mpURL
        );

        $breadcrumb['links'][] = array(
            'title' => $this->module->l('Seller Request', 'sellerrequest'),
            'url' => 'javascript:void(0)'
        );
        return $breadcrumb;
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->registerStylesheet('mp-marketplace_account', 'modules/'.$this->module->name.'/views/css/marketplace_account.css');
        $this->registerStylesheet('mp-marketplace_global', 'modules/'.$this->module->name.'/views/css/mp_global_style.css');

        $this->registerJavascript('mp-mp_form_validation', 'modules/'.$this->module->name.'/views/js/mp_form_validation.js');
        $this->registerJavascript('mp-change_multilang', 'modules/'.$this->module->name.'/views/js/change_multilang.js');
        $this->registerJavascript('mp-getstate', 'modules/'.$this->module->name.'/views/js/getstate.js');
    }
}
