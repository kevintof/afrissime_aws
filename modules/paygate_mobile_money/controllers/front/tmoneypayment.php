<?php

use GuzzleHttp\Client;

/**
 * Cette classe est est charge de faire une demande de paiement en utilisant l'api flooz
 * de paygateglobal, la version RESTful.
 * @author jordy
 * Class PaygateMobileMoneyFloozPaymentModuleFrontController
 */
class paygate_mobile_moneytmoneypaymentModuleFrontController extends ModuleFrontController
{
    protected $apiToken = "1bf2275a-b56d-444a-81f1-644eccd59ae3" ;

    public function postProcess()
    {
        $client = $this->context->customer ;

        if($this->module->active)
        {
//            if(Context::getContext()->currency->iso_code != "XOF")
//            {
//                Tools::redirect(
//                    'index.php?controller=order&error='.urlencode('Le paiement par flooz, n\'est disponible qu\'en Franc CFA')
//                ) ;
//            }
//
//            if(Context::getContext()->country->iso_code != "TG")
//            {
//                // redirection vers la page de commande en disant qu'il faut être au togo
//                Tools::redirect(
//                    'index.php?controller=order&error='.urlencode('Le paiement par flooz, n\'est disponible qu\'au Togo')
//                ) ;
//            }

            if($client->isLogged())
            {
                // Récupération des variables
                $panier = new Cart(Context::getContext()->cart->id) ;
                $currency = $this->context->currency;
                // validation du panier
                $validitePanier = $this->module->validateOrder(
                    $panier->id,
                    3,
                    $panier->getOrderTotal(true, Cart::BOTH),
                    Tools::getValue('paymentName', $this->module->displayName),
                    null,
                    null,
                    (int)$currency->id,
                    true,
                    $client->secure_key
                ) ;
                //Récupération des informations des articles
                $name = '';
                $products = $this->context->cart->getProducts();
                foreach ($products as $product) {
                    $name .= $product['name'].', ';
                }

                if($validitePanier)
                {
                    $id_commande = $this->module->currentOrder ;
                    $confirmationUrl = Context::getContext()->shop->getBaseURL(true).'index.php?controller=order-confirmation&id_cart='.$panier->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$client->secure_key;

                    Tools::redirect(
                        "https://paygateglobal.com/v1/page?url={$confirmationUrl}&token={$this->apiToken}&amount={$panier->getOrderTotal()}&description=".urlencode($name)."&identifier={$id_commande}&url=".$this->context->link->getModuleLink($this->module->displayName, 'validation',array('success'=>1))
                    ) ;
                }
                else
                {
                    Tools::redirect(
                        'index.php?controller=order&step=3&error='.urlencode("Il est impossible de valider votre commande")
                    ) ;
                }
            }
        }
        else
        {
            Tools::redirect(
                'index.php?controller=order&step=3&error='.urlencode('Le paiement par flooz, n\'est pas actif')
            ) ;
        }
    }
}
