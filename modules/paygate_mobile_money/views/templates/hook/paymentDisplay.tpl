<section>
  <p>
  	<img src="{$urls.base_url}modules/paygate_mobile_money/views/img/paygate_logo.png"
                     alt="{l s='Payer avec TMoney (mobile money par Togocom)' mod='paygateglobal'}" width="190px" height="104px"/>
  </p>
  <p>{l s='You will be redirected to the paygateglobal website to make the payment. Once the payment is made, you will be automatically brought back to our site' mod='paygateglobal'}</p>
</section>