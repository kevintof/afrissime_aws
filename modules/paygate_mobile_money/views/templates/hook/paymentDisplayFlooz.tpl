<div class="row">
    <div class="col-xs-12 col-md-6">
        <p class="payment_module" id="PaygateMobileMoney_payment_button">
            <a
              href="{$link->getModuleLink('paygateglobal', 'floozpayment', ['paymentName'=>'Flooz/Moov tg'], true)|escape:'htmlall':'UTF-8'}"
               title="{l s='Payer avec flooz (mobile money by Moov)'
               mod='PaygateMobileMoney'}">
                <img src="{$module_dir|escape:'htmlall':'UTF-8'}/logo.png" alt="{l s='Pay with my payment module' mod='PaygateMobileMoney'}" width="32" height="32"/>
                {l s='Payer avec flooz (mobile money by Moov)' mod='paygateglobal'}
            </a>
        </p>
    </div>
</div>
