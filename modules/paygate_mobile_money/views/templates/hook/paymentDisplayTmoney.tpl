<div class="row">
    <div class="col-xs-12 col-md-6">
        <p class="payment_module" id="PaygateMobileMoney_payment_button">
            <a
              href="{$link->getModuleLink('paygateglobal', 'floozpayment', ['paymentName'=>'Tmoney/Togocom'], true)|escape:'htmlall':'UTF-8'}"
              title="{l s='Pay with my payment module' mod='PaygateMobileMoney'}">
                <img src="{$module_dir|escape:'htmlall':'UTF-8'}/logo.png"
                     alt="{l s='Payer avec TMoney (mobile money par Togocom)' mod='paygateglobal'}" width="32" height="32"/>
                {l s='Payer avec TMoney (mobile money par Togocom)' mod='paygateglobal'}
            </a>
        </p>
    </div>
</div>
