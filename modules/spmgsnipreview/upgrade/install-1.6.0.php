<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function upgrade_module_1_6_0($module)
{
    $name_module = 'spmgsnipreview';

    if(version_compare(_PS_VERSION_, '1.7', '>')) {
        $tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewreview");
        if ($tab_id) {
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        $tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewreviews");
        if ($tab_id) {
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        @unlink(_PS_ROOT_DIR_ . "/img/t/AdminSpmgsnipreviewreview.gif");


        $tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewstorereview");
        if ($tab_id) {
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        $tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewstorereviews");
        if ($tab_id) {
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        @unlink(_PS_ROOT_DIR_ . "/img/t/AdminSpmgsnipreviewstorereview.gif");


        $module->createAdminTabs15();
    }



    $module->registerHook('lastReviewsSPM');




    $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview`');
    if (is_array($list_fields))
    {
        foreach ($list_fields as $k => $field)
            $list_fields[$k] = $field['Field'];
        if (!in_array('is_new', $list_fields)) {
            if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview` ADD `is_new` int(11) NOT NULL default \'0\'')) {
                return false;
            }

        }
    }


    $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_storereviews`');
    if (is_array($list_fields))
    {
        foreach ($list_fields as $k => $field)
            $list_fields[$k] = $field['Field'];
        if (!in_array('is_new', $list_fields)) {
            if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_storereviews` ADD `is_new` int(11) NOT NULL default \'0\'')) {
                return false;
            }

        }
    }


    #### posts api ###
    $languages = Language::getLanguages(false);
    foreach ($languages as $language){
        $i = $language['id_lang'];
        $iso = Tools::strtoupper(Language::getIsoById($i));
        Configuration::updateValue($name_module.'idesc_'.$i, 'rated product in our shop' . $iso);
        Configuration::updateValue($name_module.'pdesc_'.$i, 'rated product in our shop' . $iso);
    }
    #### posts api ###


    return true;
}
?>