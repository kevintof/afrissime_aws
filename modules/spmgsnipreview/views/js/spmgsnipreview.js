/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

document.addEventListener("DOMContentLoaded", function(event) {
    $(document).ready(function(){
        $('#searchr').val('');


        // sorting
        $( "#select_spmgsnipreview_sort" ).change(function() {


            var data_action = $('select#select_spmgsnipreview_sort').attr('data-action');

            if(data_action == "userpagereviews"){
                var id_product_spmgsnipreview = $('select#select_spmgsnipreview_sort').attr('data-id-customer');
            } else {
                var id_product_spmgsnipreview = $('#id_product_spmgsnipreview').val();
            }
            go_page_spmgsnipreviewr( 0, data_action , '', '' , id_product_spmgsnipreview, 1);
        });
        // sorting

    });
});



function go_page_spmgsnipreviewr(page,type,rating,search,id_product,is_sort){


    if(is_sort !== undefined){
      var sort_condition = $( "#select_spmgsnipreview_sort option:selected" ).val();
    } else {
        var sort_condition = '';
    }


    if(type == "userpagereviews"){
        var id_customer = id_product;
    } else {
        var id_customer = 0;
    }

    if(search !== undefined && search !== ''){

        var search = $('#searchr').val();


    } else {
        var search = '';

    }

    if(rating !== undefined){

        var frat = rating;
    } else {
        var frat = '';

    }

    for (var rating_i = 1; rating_i < 6; rating_i++) {
        $('#reviews-rating-'+rating_i).parent().removeClass('active-items-block');
    }


    if(id_product === undefined){
        var id_product = 0;
    }

    $('#reviews-list').css('opacity',0.5);
    $.post(ajax_productreviews_url_spmgsnipreview, {
            action:type,
            page : page,
            frat: frat,
            search: search,
            id_product:id_product,
            uid:id_customer,
            sort_condition: sort_condition,
            is_sort:is_sort

        },
        function (data) {
            if (data.status == 'success') {

                $('#reviews-list').css('opacity',1);

                $('#reviews-list').html('');
                $('#reviews-list').prepend(data.params.content);

                $('#page_navr').html('');
                $('#page_navr').prepend(data.params.page_nav);


                if(rating !== undefined && rating !== '' && rating !== 0){
                    $('#reviews-rating-'+rating).parent().addClass('active-items-block');
                    $('#clear-rating-reviews').removeClass('display-none');
                } else {
                    $('#clear-rating-reviews').addClass('display-none');
                }

                if(search !== undefined && search !== '' && search !== 0){
                    $('#clear-search-reviews').removeClass('display-none');
                } else {
                    $('#clear-search-reviews').addClass('display-none');
                }

                if (document.getElementById('spr-content') instanceof Object){
                    var id_scroll_to = "spr-content";

                } else {

                    if (document.getElementById('center_column') instanceof Object) {
                        // for ps 1.6
                        var id_scroll_to = "center_column";
                    } else {
                        // for ps 1.7
                        var id_scroll_to = "content";
                    }
                }


                if(type == "userpagereviews" || type == "productpagereviews" || type == "allpagereviews") {
                    FB.XFBML.parse(); // for facebook
                }





                $("html, body").animate({ scrollTop: $('#'+id_scroll_to).offset().top }, "slow");
                return false;


            } else {
                $('#reviews-list').css('opacity',1);
                alert(data.message);
            }

        }, 'json');

}


function report_helpfull_spmgsnipreview(rid,val){
    $.post(ajax_productreviews_url_spmgsnipreview,
        { rid:rid,
          val:val,
          action:'helpfull'
        },
        function (data) {
            if (data.status == 'success') {


                var data_status = data.message;

                $("#block-helpful"+rid).html('');
                $("#block-helpful"+rid).html(data_status);
                $("#block-helpful"+rid).css('opacity',0.5);
                $("#block-helpful"+rid).addClass('helpfull-success');

                var yes = data.params.yes;
                $("#block-helpful-yes"+rid).html('');
                $("#block-helpful-yes"+rid).html(yes);


                var all = data.params.all;
                $("#block-helpful-all"+rid).html('');
                $("#block-helpful-all"+rid).html(all);

                $('#people-folowing-reviews'+rid).css('opacity',0.5);

                setTimeout(function(){
                    $("#block-helpful"+rid).css('opacity',1);
                    $('#people-folowing-reviews'+rid).css('opacity',1);
                }, 300);


            } else {
                $("#block-helpful"+rid).html('');
                $("#block-helpful"+rid).html(data.message);
                $("#block-helpful"+rid).css('opacity',0.5);
                $("#block-helpful"+rid).addClass('helpfull-error');
                setTimeout(function(){
                    $("#block-helpful"+rid).css('opacity',1);
                }, 300);


            }
        }, 'json');
}

function report_abuse_spmgsnipreview(rid){

    $.post(ajax_productreviews_url_spmgsnipreview,
        { rid:rid,
            action:'abuse'
        },
        function (data) {
            if (data.status == 'success') {


                var data = data.params.content;
                //alert(data);

                if ($('div#fb-con-wrapper').length == 0)
                {
                    conwrapper = '<div id="fb-con-wrapper" class="popup-form-box"><\/div>';
                    $('body').append(conwrapper);
                } else {
                    $('#fb-con-wrapper').html('');
                }

                if ($('div#fb-con').length == 0)
                {
                    condom = '<div id="fb-con"><\/div>';
                    $('body').append(condom);
                }

                $('div#fb-con').fadeIn(function(){

                    $(this).css('filter', 'alpha(opacity=70)');
                    $(this).bind('click dblclick', function(){
                        $('div#fb-con-wrapper').hide();
                        $(this).fadeOut();
                        window.location.reload();
                    });
                });


                $('div#fb-con-wrapper').html('<a id="button-close" style="display: inline;"><\/a>'+data).fadeIn();

                $("a#button-close").click(function() {
                    $('div#fb-con-wrapper').hide();
                    $('div#fb-con').fadeOut();
                    window.location.reload();
                });

                $("button#cancel-report").click(function() {
                    $('div#fb-con-wrapper').hide();
                    $('div#fb-con').fadeOut();
                    window.location.reload();
                });

            } else {
                alert(data.message);

            }
        }, 'json');
}





function field_state_change(field, state, err_text)
{

    if(typeof field_gdpr_change_spmgsnipreview === 'function') {
        //gdpr
        field_gdpr_change_spmgsnipreview();
        //gdpr
    }

    var field_label = $('label[for="'+field+'"]');
    var field_div_error = $('#'+field);

    if (state == 'success')
    {
        field_label.removeClass('error-label');
        field_div_error.removeClass('error-current-input');
    }
    else
    {
        field_label.addClass('error-label');
        field_div_error.addClass('error-current-input');
    }
    document.getElementById('error_'+field).innerHTML = err_text;

}


function addRemoveDiscountShareReview(data_type,rid){
    $('#facebook-share-review-block').css('opacity',0.5);
    $.post(ajax_productreviews_url_spmgsnipreview,
        { rid:rid,
            action:data_type
        },
        function (data) {
            if (data.status == 'success') {


                var voucher_html_share_review = data.params.content;

                $('#facebook-share-review-block').css('opacity',1);
                if(data.length==0) return;
                if ($('div#fb-con-wrapper').length == 0)
                {
                    conwrapper = '<div id="fb-con-wrapper" class="voucher-data"><\/div>';
                    $('body').append(conwrapper);
                } else {
                    $('#fb-con-wrapper').html('');
                }

                if ($('div#fb-con').length == 0)
                {
                    condom = '<div id="fb-con"><\/div>';
                    $('body').append(condom);
                }


                $('div#fb-con').fadeIn(function(){

                    $(this).css('filter', 'alpha(opacity=70)');
                    $(this).bind('click dblclick', function(){
                        $('div#fb-con-wrapper').hide();
                        $(this).fadeOut();
                    });
                });


                $('div#fb-con-wrapper').html('<a id="button-close" style="display: inline;"><\/a>'+voucher_html_share_review).fadeIn();

                $("a#button-close").click(function() {
                    $('div#fb-con-wrapper').hide();
                    $('div#fb-con').fadeOut();
                });

            } else {
                alert(data.message);

            }
        }, 'json');

}


function trim(str) {
    str = str.replace(/(^ *)|( *$)/,"");
    return str;
}



function show_form_review(par){

    $('#add-review-block').toggle();
    $('#no-customers-reviews').toggle();

    if(par == 1){
        $('.spr-summary-actions .btn-spmgsnipreview').parent().hide();
    } else {
        $('.spr-summary-actions .btn-spmgsnipreview').parent().show();
    }
}




function spmgsnipreview_open_tab(){
    // for first tab style //
    if($('#more_info_tabs li').length>0) {
        $.each($('#more_info_tabs li'), function (key, val) {
            $(this).children().removeClass("selected");
        });

        $('#idTab777-my').addClass('selected');

        for (i = 0; i < $('#more_info_sheets').children().length; i++) {
            $('#more_info_sheets').children(i).addClass("block_hidden_only_for_screen");
        }
        $('#idTab777').removeClass('block_hidden_only_for_screen');
    }

    // for first tab style //


    // for second tab style //
    if($('.nav-tabs').length>0) {

        $.each($('.nav-tabs li'), function (key, val) {
            $(this).removeClass("active");
            $(this).children().removeClass("active"); // for ps 1.7
        });

        //$('#idTab777-my').parent().addClass('active');

        for (i = 0; i < $('.tab-content').children().length; i++) {
            $('.tab-content').children(i).removeClass("active");
            $('.tab-content').children(i).removeClass("in");
        }
        $('#idTab777').addClass('in');
        $('#idTab777').addClass('active');
        $('#idTab777-my').addClass('active'); // for ps 1.7

    }
    // for second tab style //
}


$(document).ready(function() {

    var is_bug_spmgsnipreview = 0;

    if(is_bug_spmgsnipreview) {
        $('a.btn-spmgsnipreview').click(function () {
            setTimeout(function () {
                $('#availability_statut').css('display', 'none');
                $('#add_to_cart').css('display', 'block');


            }, 1000);
        });

        $('a[href="#idTab777"]').click(function () {
            setTimeout(function () {
                $('#availability_statut').css('display', 'none');
                $('#add_to_cart').css('display', 'block');


            }, 1000);
        });
    }

    $('a.btn-default-spmgsnipreview[href="#idTab777"]').click(function(){

        spmgsnipreview_open_tab();

    });


    $('a#idTab777-my-click[href="#idTab777"]').click(function(){

        spmgsnipreview_open_tab();

        $('#add-review-block').show();
        $('#no-customers-reviews').hide();
        $('.spr-summary-actions .btn-spmgsnipreview').parent().hide();

    });

});


$(document).ready(function() {
    var is_func_spmgsnipreview = $.fancybox;

    if (is_func_spmgsnipreview !== undefined) {
        $("a.fancybox").fancybox();
    }

});


$(document).ready(function() {
    /* if page contains #idTab777, then go to id="idTab777" */
    if(window.location.href.indexOf('#') != -1){
        var vars = [], hash_spmgsnipreview = '';
        var hashes_spmgsnipreview = window.location.href.slice(window.location.href.indexOf('#') + 1);

        for(var i = 0; i < hashes_spmgsnipreview.length; i++)
        {
            hash_spmgsnipreview += hashes_spmgsnipreview[i];
        }

        if(hash_spmgsnipreview == "idTab777") {
            setTimeout(function(){
                $("html, body").animate({scrollTop: $('#' + hash_spmgsnipreview).offset().top}, "slow");

                spmgsnipreview_open_tab();
                $('#add-review-block').toggle();
                $('#no-customers-reviews').toggle();
                $('.spr-summary-actions .btn-spmgsnipreview').parent().hide();

            }, 500);
        }

    }
    /* if page contains #idTab777, then go to id="idTab777" */


    if ($('.owl_reviews_home_type_carousel ul').length > 0) {

        if ($('.owl_reviews_home_type_carousel ul').length > 0) {

            if (typeof $('.owl_reviews_home_type_carousel ul').owlCarousel === 'function') {

                $('.owl_reviews_home_type_carousel ul').owlCarousel({
                    items: spmgsnipreview_number_reviews_home_slider,

                    loop: true,
                    responsive: true,
                    nav: true,
                    navRewind: false,
                    margin: 20,
                    dots: true,

                    lazyLoad: true,
                    lazyFollow: true,
                    lazyEffect: "fade",
                });

            }
        }

    }

    if ($('.owl_reviews_type_carousel ul').length > 0) {

        if ($('.owl_reviews_type_carousel ul').length > 0) {

            if (typeof $('.owl_reviews_type_carousel ul').owlCarousel === 'function') {

                $('.owl_reviews_type_carousel ul').owlCarousel({
                    items: 1,

                    loop: true,
                    responsive: true,
                    nav: true,
                    navRewind: false,
                    margin: 20,
                    dots: true,
                    navText: [,],

                    lazyLoad: true,
                    lazyFollow: true,
                    lazyEffect: "fade",
                });
            }
        }

    }


});