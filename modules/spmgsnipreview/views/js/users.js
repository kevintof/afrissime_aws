/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

document.addEventListener("DOMContentLoaded", function(event) {
    $(document).ready(function(){
        $('#searchu').val('');
    });
});


function go_page_spmgsnipreviewu(page,type,rating,search,id_customer){


    if(search !== undefined && search !== ''){

        var searchu = $('#searchu').val();


    } else {
        var searchu = '';

    }



    if(id_customer === undefined){
        var id_customer = 0;
    }

    $('#users-list').css('opacity',0.5);
    $.post(ajax_users_url_spmgsnipreview, {
            action:type,
            page : page,
            search: searchu,
            uid:id_customer

        },
        function (data) {
            if (data.status == 'success') {

                $('#users-list').css('opacity',1);

                $('#users-list').html('');
                $('#users-list').prepend(data.params.content);

                $('#page_navu').html('');
                $('#page_navu').prepend(data.params.page_nav);




                if(search !== undefined && search !== '' && search !== 0){
                    $('#clear-search-users').removeClass('display-none');
                } else {
                    $('#clear-search-users').addClass('display-none');
                }

                if (document.getElementById('center_column') instanceof Object){
                    var id_scroll_to = "center_column";
                } else {
                    var id_scroll_to = "content";
                }



                $("html, body").animate({ scrollTop: $('#'+id_scroll_to).offset().top }, "slow");
                return false;


            } else {
                $('#users-list').css('opacity',1);
                alert(data.message);
            }

        }, 'json');

}



$(document).ready(function(){

    if ($('.owl_users_type_carousel ul').length > 0) {

        if ($('.owl_users_type_carousel ul').length > 0) {

            if (typeof $('.owl_users_type_carousel ul').owlCarousel === 'function') {

                $('.owl_users_type_carousel ul').owlCarousel({
                    items: 1,

                    loop: true,
                    responsive: true,
                    nav: true,
                    navRewind: false,
                    margin: 20,
                    dots: true,
                    navText: [,],

                    lazyLoad: true,
                    lazyFollow: true,
                    lazyEffect: "fade",
                });
            }
        }

    }


    if ($('.owl_users_home_type_carousel ul').length > 0) {

        if ($('.owl_users_home_type_carousel ul').length > 0) {

            if (typeof $('.owl_users_home_type_carousel ul').owlCarousel === 'function') {

                $('.owl_users_home_type_carousel ul').owlCarousel({
                    items: spmgsnipreview_number_users_home_slider,

                    loop: true,
                    responsive: true,
                    nav: true,
                    navRewind: false,
                    margin: 20,
                    dots: true,

                    lazyLoad: true,
                    lazyFollow: true,
                    lazyEffect: "fade",
                });

            }
        }

    }

});