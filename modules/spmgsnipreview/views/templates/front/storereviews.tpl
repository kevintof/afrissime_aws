{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{capture name=path}
{l s='Store Reviews' mod='spmgsnipreview'}
{/capture}


<h1 class="page-heading">{$meta_title|escape:'htmlall':'UTF-8'}</h1>


<div id="succes-review">
{l s='Your review  has been successfully sent our team. Thanks for review!' mod='spmgsnipreview'}
</div>



        <div class="text-align-center margin-bottom-20" id="add_testimonials">
            <input type="button" onclick="show_testimonial_form(1)" value="{l s='Write a Review' mod='spmgsnipreview'}"
                   class="btn-custom btn-primary-spmgsnipreview testimonials-add-btn" />


        </div>


        {if $spmgsnipreviewid_customerti == 0 && $spmgsnipreviewwhocanaddti == 'reg'}
            <div class="no-registered-ti"  id="text-before-add-testimonial-form">
                <div class="text-no-reg">
                    {l s='You cannot post a review because you are not logged as a customer' mod='spmgsnipreview'}
                </div>
                <br/>

                <div class="no-reg-button">
                    <a href="{$spmgsnipreviewmy_account nofilter}"
                       class="btn-custom btn-primary-spmgsnipreview testimonials-add-btn" >{l s='Log in / sign up' mod='spmgsnipreview'}</a>
                </div>

            </div>
        {*{elseif $spmgsnipreviewis_buyti == 0 && $spmgsnipreviewwhocanaddti == 'buy'}
            <div class="no-registered-ti"  id="text-before-add-testimonial-form">
                <div class="text-no-reg">
                    {l s='Only registered users who already bought something in shop can add review.' mod='spmgsnipreview'}
                </div>
            </div>
            *}
        {else}

            <div id="text-before-add-testimonial-form">

                <b class="margin-5">{l s='Send us Your review about an order or about our products & services.' mod='spmgsnipreview'}</b>

                {if $spmgsnipreviewwhocanaddti == 'buy'}
                    <br/><br/>
                    <div class="text-no-reg alert alert-warning">
                        {l s='Only customers that have purchased a product can give a review.' mod='spmgsnipreview'}
                    </div>
                {else}
                    <br/>
                {/if}

                {* voucher suggestions *}

                {if $spmgsnipreviewvis_onti && $spmgsnipreviewis_show_voucherti == 1}
                    <br/><br/>
                    <div class="advertise-text-review">
                                <span>
                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}"
                                         alt="{l s='Write a review and get voucher for discount' mod='spmgsnipreview'}" />
                                    {l s='Write a review and get voucher for discount' mod='spmgsnipreview'}
                                    <b>{$spmgsnipreviewdiscountti|escape:'htmlall':'UTF-8'}</b> {if $spmgsnipreviewvalutati != '%'}<b>({if $spmgsnipreviewtaxti == 1}{l s='Tax Included' mod='spmgsnipreview'}{else}{l s='Tax Excluded' mod='spmgsnipreview'}{/if})</b>{/if}
                                    {if $spmgsnipreviewis_show_minti == 1 && $spmgsnipreviewisminamountti}
                                        <b>({l s='Minimum amount' mod='spmgsnipreview'} : {$spmgsnipreviewminamountti|escape:'htmlall':'UTF-8'} {$spmgsnipreviewcurtxtti|escape:'htmlall':'UTF-8'})</b>
                                    {/if}
                                    ,
                                    {l s='valid for' mod='spmgsnipreview'} {$spmgsnipreviewsdvvalidti|escape:'htmlall':'UTF-8'} {$spmgsnipreviewdaysti|escape:'htmlall':'UTF-8'}
                                </span>
                    </div>
                {/if}

                {* voucher suggestions *}

            </div>




            <div id="add-testimonial-form" {if $spmgsnipreviewis17 == 1}class="block-categories"{/if}>



                <form method="post" enctype="multipart/form-data" id="spmgsnipreview_form" name="spmgsnipreview_form">
                    <input type="hidden" name="action" value="addreview" />

                    <div class="title-rev" id="idTab666-my">
                        <div class="float-left title-form-text-left">
                            {l s='Write a Review' mod='spmgsnipreview'}
                        </div>

                        <input type="button" value="{l s='close' mod='spmgsnipreview'}"
                               class="btn-spmgsnipreview btn-primary-spmgsnipreview title-form-text-right" onclick="show_testimonial_form(0)">

                        <div class="clear"></div>
                    </div>

                    <div id="body-add-storereview-form-storereview">


                        {if $spmgsnipreviewcriterions|@count > 0}

                            {foreach from=$spmgsnipreviewcriterions item='criterion'}

                                <label for="rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}"
                                       class="float-left">{$criterion.name|escape:'htmlall':'UTF-8'}<sup class="testimonials-req">*</sup></label>

                                <div class="rat testimonials-stars rating-stars-dynamic-storereview">
                                                        <span onmouseout="read_rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}');">

                                                            <img  onmouseover="_rating_efect_rev(1,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(1,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',1); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true; "
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="1" id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_1" />

                                                            <img  onmouseover="_rating_efect_rev(2,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(2,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',2); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true;"
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="2" id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_2" />

                                                            <img  onmouseover="_rating_efect_rev(3,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(3,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',3); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true;"
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="3"  id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_3" />
                                                            <img  onmouseover="_rating_efect_rev(4,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(4,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',4); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true;"
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="4"  id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_4" />
                                                            <img  onmouseover="_rating_efect_rev(5,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(5,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',5); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true;"
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="5"  id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_5" />
                                                        </span>
                                    {if strlen($criterion.description)>0}
                                        <div class="tip-criterion-description">{$criterion.description|escape:'htmlall':'UTF-8'}</div>
                                    {/if}
                                </div>
                                <input type="hidden" id="rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}" name="rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}" value="0"/>
                                <div class="clr"></div>
                                <div class="errorTxtAdd" id="error_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}"></div>

                            {/foreach}
                        {else}
                            <label for="rat_rel" class="float-left">{l s='Rating' mod='spmgsnipreview'}<sup class="testimonials-req">*</sup></label>

                            <div class="rat testimonials-stars rating-stars-dynamic-storereview">
                                                        <span onmouseout="read_rating_review_shop('rat_rel');">
                                                            <img  onmouseover="_rating_efect_rev(1,0,'rat_rel')" onmouseout="_rating_efect_rev(1,1,'rat_rel')"
                                                                  onclick = "rating_review_shop('rat_rel',1); rating_checked=true; "
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="1"
                                                                  id="img_rat_rel_1" />
                                                            <img  onmouseover="_rating_efect_rev(2,0,'rat_rel')" onmouseout="_rating_efect_rev(2,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',2); rating_checked=true;" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="2"  id="img_rat_rel_2" />
                                                            <img  onmouseover="_rating_efect_rev(3,0,'rat_rel')" onmouseout="_rating_efect_rev(3,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',3); rating_checked=true;" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="3"  id="img_rat_rel_3" />
                                                            <img  onmouseover="_rating_efect_rev(4,0,'rat_rel')" onmouseout="_rating_efect_rev(4,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',4); rating_checked=true;" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="4"  id="img_rat_rel_4" />
                                                            <img  onmouseover="_rating_efect_rev(5,0,'rat_rel')" onmouseout="_rating_efect_rev(5,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',5); rating_checked=true;" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="5"  id="img_rat_rel_5" />
                                                        </span>
                            </div>
                            <input type="hidden" id="rat_rel" name="rat_rel" value="0"/>
                            <div class="clr"></div>
                            <div class="errorTxtAdd" id="error_rat_rel"></div>
                        {/if}




                        <br/>


                        <label for="name-review">{l s='Name' mod='spmgsnipreview'}<span class="testimonials-req">*</span></label>
                        <input type="text" name="name-review" id="name-review"
                               {if strlen($spmgsnipreviewname_cti)>0}value="{$spmgsnipreviewname_cti|escape:'htmlall':'UTF-8'}"{/if}
                               class="testimonials-input" onkeyup="check_inpNameReview();" onblur="check_inpNameReview();" />
                        <div class="errorTxtAdd" id="error_name-review"></div>

                        <label for="email-review">{l s='Email' mod='spmgsnipreview'}<sup class="testimonials-req">*</sup></label>
                        <input type="text" name="email-review"
                               id="email-review"
                               class="testimonials-input"
                               {if strlen($spmgsnipreviewemail_cti)>0}value="{$spmgsnipreviewemail_cti|escape:'htmlall':'UTF-8'}"{/if}
                               onkeyup="check_inpEmailReview();" onblur="check_inpEmailReview();"
                                />
                        <div class="errorTxtAdd" id="error_email-review"></div>

                        {if $spmgsnipreviewis_avatar == 1}
                        <label for="avatar-review">{l s='Avatar' mod='spmgsnipreview'}</label>
                            {if strlen($spmgsnipreviewc_avatarti)>0}
                                <div class="avatar-block-rev-form">
                                    <input type="radio" name="post_images" checked="" style="display: none">
                                    <img src="{$spmgsnipreviewc_avatarti|escape:'htmlall':'UTF-8'}" alt="{$spmgsnipreviewname_cti|escape:'htmlall':'UTF-8'}" />
                                </div>
                            {/if}

                            <input type="file" name="avatar-review" id="avatar-review" class="testimonials-input" />
                            <div class="b-guide">
                                {l s='Allow formats' mod='spmgsnipreview'}: *.jpg; *.jpeg; *.png; *.gif.
                            </div>
                            <div class="errorTxtAdd" id="error_avatar-review"></div>
                        {/if}

                        {if $spmgsnipreviewis_web == 1}
                        <label>{l s='Web address:' mod='spmgsnipreview'}</label>
                        <input type="text" name="web-review"
                               id="web-review"
                               class="testimonials-input"
                                />
                        {/if}

                        {if $spmgsnipreviewis_company == 1}
                            <label>{l s='Company' mod='spmgsnipreview'}</label>
                            <input type="text" name="company-review"
                                   id="company-review"
                                   class="testimonials-input"
                                    />
                        {/if}


                        {if $spmgsnipreviewis_addr == 1}
                            <label>{l s='Address' mod='spmgsnipreview'}</label>
                            <input type="text" name="address-review"
                                   id="address-review"
                                   class="testimonials-input"
                                    />
                        {/if}


                        {if $spmgsnipreviewis_country == 1}
                            <label>{l s='Country' mod='spmgsnipreview'}</label>
                            <input type="text" name="country-review"
                                   id="country-review"
                                   class="testimonials-input"
                                    />
                        {/if}

                        {if $spmgsnipreviewis_city == 1}
                            <label>{l s='City' mod='spmgsnipreview'}</label>
                            <input type="text" name="city-review"
                                   id="city-review"
                                   class="testimonials-input"
                                    />
                        {/if}

                        <label for="text-review">{l s='Message:' mod='spmgsnipreview'}<span class="testimonials-req">*</span></label>
                        <textarea class="testimonials-textarea"
                                  id="text-review"
                                  name="text-review" onkeyup="check_inpMsgReview();" onblur="check_inpMsgReview();"></textarea>
                        <div class="errorTxtAdd" id="error_text-review"></div>


                        {if $spmgsnipreviewis_filesti == 1}
                            <label for="text-files">{l s='Files' mod='spmgsnipreview'}</label>
                            <span class="file-upload-rev" id="file-upload-rev">
                                            <input type="file" name="files[]" multiple />
                                            <div class="progress-files-bar">
                                                <div class="progress-files"></div>
                                            </div>
                                            <div id="file-files-list"></div>
                                        </span>


                            <div class="avatar-guid">
                                {l s='Maximum files to upload' mod='spmgsnipreview'} - <b>{$spmgsnipreviewruploadfilesti|escape:'htmlall':'UTF-8'}</b><br/>{l s='Allow formats' mod='spmgsnipreview'}: *.jpg; *.jpeg; *.png; *.gif.
                            </div>
                            <div id="error_text-files" class="errorTxtAdd"></div>
                        {/if}


                        {* gdpr *}
                        {hook h='displayGDPRConsent' mod='psgdpr' id_module=$id_module}
                        {* gdpr *}


                        {if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customerti == 0}
                            <label for="inpCaptchaReview">{l s='Captcha' mod='spmgsnipreview'}<span class="testimonials-req">*</span></label>
                            <div class="clr"></div>
                            <img width="100" height="26" class="float-left" id="secureCodReview" src="{$spmgsnipreviewcaptchastore nofilter}"
                                 alt="Captcha"/>
                            <input type="text" class="inpCaptchaReview float-left" id="inpCaptchaReview" size="6" name="captcha"
                                   onkeyup="check_inpCaptchaReview();" onblur="check_inpCaptchaReview();"/>
                            <div class="clr"></div>

                            <div id="error_inpCaptchaReview" class="errorTxtAdd"></div>
                        {/if}

                    </div>

                    <div id="footer-add-storereview-form-storereview">
                       <input type="submit" name="submit_spmgsnipreview" value="{l s='Submit your Review' mod='spmgsnipreview'}" class="btn-custom btn-success-custom" />
                        &nbsp;
                        <button onclick="show_testimonial_form(0)" value="{l s='Cancel' mod='spmgsnipreview'}" class="btn btn-danger">{l s='Cancel' mod='spmgsnipreview'}</button>
                    </div>






                </form>
            </div>

        {literal}
            <script type="text/javascript">
                {/literal}{if $spmgsnipreviewis17 == 1}{literal}
                var baseDir = '{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}';
                {/literal}{/if}{literal}
                var module_dir = '{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/{literal}';
                var spmgsnipreview_star_active = '{/literal}{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}{literal}';
                var spmgsnipreview_star_noactive = '{/literal}{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}{literal}';
            </script>
        {/literal}


        {literal}
            <script type="text/javascript">
                document.addEventListener("DOMContentLoaded", function(event) {
                    jQuery(document).ready(init_rating);
                });
            </script>
        {/literal}



        {literal}
            <script type="text/javascript">


                setTimeout(function() {
                    $('#footer-add-storereview-form-storereview').find('input[name="submit_spmgsnipreview"]').removeAttr('disabled');
                }, 1000);


                function field_gdpr_change_store_spmgsnipreview(){
                    // gdpr
                    var gdpr_spmgsnipreview = $('#psgdpr_consent_checkbox_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal}');

                    var is_gdpr_spmgsnipreview = 1;

                    if(gdpr_spmgsnipreview.length>0){

                        if(gdpr_spmgsnipreview.prop('checked') == true) {
                            $('.gdpr_module_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal} .psgdpr_consent_message').removeClass('error-label');
                        } else {
                            $('.gdpr_module_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal} .psgdpr_consent_message').addClass('error-label');
                            is_gdpr_spmgsnipreview = 0;
                        }

                        $('#psgdpr_consent_checkbox_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal}').on('click', function(){
                            if(gdpr_spmgsnipreview.prop('checked') == true) {
                                $('.gdpr_module_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal} .psgdpr_consent_message').removeClass('error-label');
                            } else {
                                $('.gdpr_module_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal} .psgdpr_consent_message').addClass('error-label');
                            }
                        });

                    }

                    //gdpr

                    return is_gdpr_spmgsnipreview;
                }


                {/literal}{if $spmgsnipreviewis17 == 1}{literal}
                var baseDir = '{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}';
                {/literal}{/if}{literal}

                {/literal}{if $spmgsnipreviewis_filesti == 1}{literal}

                var file_shopreviews_upload_url_spmgsnipreview = '{/literal}{$spmgsnipreviewupload_shopreviews_url nofilter}{literal}';
                var file_shopreviews_max_files_spmgsnipreview = {/literal}{$spmgsnipreviewruploadfilesti|escape:'htmlall':'UTF-8'}{literal};
                var file_shopreviews_max_message_spmgsnipreview = '{/literal}{$spmgsnipreviewptc_msg13_1|escape:'htmlall':'UTF-8'}{literal} '+file_shopreviews_max_files_spmgsnipreview+' {/literal}{$spmgsnipreviewptc_msg13_2|escape:'htmlall':'UTF-8'}{literal}';
                var file_shopreviews_path_upload_url_spmgsnipreview = baseDir + '{/literal}{$spmgsnipreviewfpathti|escape:'htmlall':'UTF-8'}{literal}tmpshopreviews/';

                {/literal}{/if}{literal}


                /* clear form fields */
                {/literal}{if $spmgsnipreviewcriterions|@count > 0}

                        {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                        $('#rat_rel{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}').val(0);

                        {/literal}{/foreach}

                        {else}{literal}

                        $('#rat_rel').val(0);

                        {/literal}{/if}
                /* clear form fields */


                {if $spmgsnipreviewcriterions|@count > 0}

                {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                var rating_checked{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal} = false;

                {/literal}{/foreach}

                {else}{literal}

                var rating_checked = false;

                {/literal}{/if}{literal}





                {/literal}{if $spmgsnipreviewcriterions|@count > 0}



                {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                function check_inpRatingReview{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}()
                {

                    if(!rating_checked{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}){
                        field_state_change('rat_rel{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}','failed', '{/literal}{$spmgsnipreviewmsg1_2|escape:'htmlall':'UTF-8'} {$criterion.name|escape:'htmlall':'UTF-8'}{literal}');
                        return false;
                    }
                    field_state_change('rat_rel{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}','success', '');
                    return true;


                }
                {/literal}{/foreach}

                {else}{literal}
                function check_inpRatingReview()
                {
                    if(!rating_checked){
                        field_state_change('rat_rel','failed', '{/literal}{$spmgsnipreviewmsg1|escape:'htmlall':'UTF-8'}{literal}');
                        return false;
                    }
                    field_state_change('rat_rel', 'success', '');
                    return true;

                }


                {/literal}{/if}{literal}



                function check_inpNameReview()
                {

                    var name_review = trim(document.getElementById('name-review').value);

                    if (name_review.length == 0)
                    {
                        field_state_change_store('name-review','failed', '{/literal}{$spmgsnipreviewmsg2|escape:'htmlall':'UTF-8'}{literal}');
                        return false;
                    }
                    field_state_change_store('name-review','success', '');
                    return true;
                }


                function check_inpEmailReview()
                {

                    var email_review = trim(document.getElementById('email-review').value);

                    if (email_review.length == 0)
                    {
                        field_state_change_store('email-review','failed', '{/literal}{$spmgsnipreviewmsg3|escape:'htmlall':'UTF-8'}{literal}');
                        return false;
                    }
                    field_state_change_store('email-review','success', '');
                    return true;
                }

                function check_inpMsgReview()
                {

                    var subject_review = trim(document.getElementById('text-review').value);

                    if (subject_review.length == 0)
                    {
                        field_state_change_store('text-review','failed', '{/literal}{$spmgsnipreviewmsg4|escape:'htmlall':'UTF-8'}{literal}');
                        return false;
                    }
                    field_state_change_store('text-review','success', '');
                    return true;
                }


                {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customerti == 0}{literal}
                function check_inpCaptchaReview()
                {

                    var inpCaptchaReview = trim(document.getElementById('inpCaptchaReview').value);

                    if (inpCaptchaReview.length != 6)
                    {
                        field_state_change_store('inpCaptchaReview','failed', '{/literal}{$spmgsnipreviewmsg5|escape:'htmlall':'UTF-8'}{literal}');
                        return false;
                    }
                    field_state_change_store('inpCaptchaReview','success', '');
                    return true;
                }
                {/literal}{/if}{literal}

                document.addEventListener("DOMContentLoaded", function(event) {
                $(document).ready(function (e) {
                    $("#spmgsnipreview_form").on('submit',(function(e) {

                        {/literal}



                        {if $spmgsnipreviewis_avatar == 1}{literal}
                        field_state_change_store('avatar-review','success', '');
                        {/literal}{/if}{literal}




                        {/literal}{if $spmgsnipreviewcriterions|@count > 0}

                        {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                        var is_rating{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal} = check_inpRatingReview{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}();

                        {/literal}{/foreach}{literal}

                        {/literal}{else}{literal}

                        var is_rating = check_inpRatingReview();

                        {/literal}{/if}{literal}


                        var is_name_review = check_inpNameReview();
                        var is_email_review = check_inpEmailReview();
                        var is_msg_review =check_inpMsgReview();
                        {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customerti == 0}{literal}
                        var is_captcha_review = check_inpCaptchaReview();
                        {/literal}{/if}{literal}


                        // gdpr
                        var is_gdpr_spmgsnipreview = field_gdpr_change_store_spmgsnipreview();
                        // gdpr

                        if(
                                is_gdpr_spmgsnipreview && //gdpr

                                {/literal}{if $spmgsnipreviewcriterions|@count > 0}

                                {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                                is_rating{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal} &&

                                {/literal}{/foreach}{literal}

                                {/literal}{else}{literal}

                                is_rating &&

                                {/literal}{/if}{literal}

                                is_name_review && is_email_review && is_msg_review

                                {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customerti == 0}{literal}
                                && is_captcha_review
                                {/literal}{/if}{literal}
                        ){

                            $('#add-testimonial-form').css('opacity','0.5');
                            $('#footer-add-review-form-review input').attr('disabled','disabled');

                            e.preventDefault();
                            $.ajax({
                                url: '{/literal}{$spmgsnipreviewajax_url nofilter}{literal}',
                                type: "POST",
                                data:  new FormData(this),
                                contentType: false,
                                cache: false,
                                processData:false,
                                dataType: 'json',
                                success: function(data)
                                {
                                    if (data.status == 'success') {

                                        $('#rat_rel').val('');
                                        $('#name-review').val('');
                                        $('#email-review').val('');
                                        $('#web-review').val('');

                                        $('#country-review').val('');
                                        $('#city-review').val('');

                                        $('#company-review').val('');
                                        $('#address-review').val('');
                                        $('#text-review').val('');
                                        $('#inpCaptchaReview').val('');

                                        $('#text-before-add-testimonial-form').hide();
                                        $('#add-testimonial-form').hide();

                                        $('#succes-review').show();



                                        {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customerti == 0}{literal}
                                        var count = Math.random();
                                        document.getElementById('secureCodReview').src = "";
                                        document.getElementById('secureCodReview').src = "{/literal}{$spmgsnipreviewcaptchastore nofilter}{literal}?re=" + count;
                                        {/literal}{/if}{literal}





                                        {/literal}{if $spmgsnipreviewvis_onti == 1 && $spmgsnipreviewis_show_voucherti}{literal}
                                        var voucher_html = data.params.voucher_html;


                                        if ($('div#fb-con-wrapper').length == 0)
                                        {
                                            conwrapper = '<div id="fb-con-wrapper" class="voucher-data"><\/div>';
                                            $('body').append(conwrapper);
                                        } else {
                                            $('#fb-con-wrapper').html('');
                                        }

                                        if ($('div#fb-con').length == 0)
                                        {
                                            condom = '<div id="fb-con"><\/div>';
                                            $('body').append(condom);
                                        }

                                        $('div#fb-con').fadeIn(function(){

                                            $(this).css('filter', 'alpha(opacity=70)');
                                            $(this).bind('click dblclick', function(){
                                                $('div#fb-con-wrapper').hide();
                                                $(this).fadeOut();
                                                //window.location.reload();
                                            });
                                        });



                                        $('div#fb-con-wrapper').html('<a id="button-close" style="display: inline;"><\/a>'+voucher_html).fadeIn();



                                        $("a#button-close").click(function() {
                                            $('div#fb-con-wrapper').hide();
                                            $('div#fb-con').fadeOut();
                                            //window.location.reload();
                                        });

                                        {/literal}{/if}{literal}


                                        $('#add-testimonial-form').css('opacity','1');


                                    } else {
                                        $('#add-testimonial-form').css('opacity','1');
                                        $('#footer-add-review-form-review input').removeAttr('disabled');
                                        var error_type = data.params.error_type;

                                        if(error_type == 1){
                                            field_state_change_store('name-review','failed', '{/literal}{$spmgsnipreviewmsg2|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        } else if(error_type == 2){
                                            field_state_change_store('email-review','failed', '{/literal}{$spmgsnipreviewmsg6|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        } else if(error_type == 3){
                                            field_state_change_store('text-review','failed', '{/literal}{$spmgsnipreviewmsg4|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        } else if(error_type == 8){
                                            field_state_change_store('avatar-review','failed', '{/literal}{$spmgsnipreviewmsg8|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        } else if(error_type == 9){
                                            field_state_change_store('avatar-review','failed', '{/literal}{$spmgsnipreviewmsg9|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        } else if(error_type == 10){
                                            field_state_change_store('email-review','failed', '{/literal}{$spmgsnipreviewmsg10|escape:'htmlall':'UTF-8'}{literal}');
                                            field_state_change_store('name-review','failed', '{/literal}{$spmgsnipreviewmsg10|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        }
                                                {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customerti == 0}{literal}
                                        else if(error_type == 4){
                                            field_state_change_store('inpCaptchaReview','failed', '{/literal}{$spmgsnipreviewmsg7|escape:'htmlall':'UTF-8'}{literal}');
                                            var count = Math.random();
                                            document.getElementById('secureCodReview').src = "";
                                            document.getElementById('secureCodReview').src = "{/literal}{$spmgsnipreviewcaptchastore nofilter}{literal}?re=" + count;
                                            return false;
                                        }
                                                {/literal}{/if}{literal}
                                        else {
                                            alert(data.message);
                                            return false;
                                        }

                                        {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customerti == 0}{literal}
                                        var count = Math.random();
                                        document.getElementById('secureCodReview').src = "";
                                        document.getElementById('secureCodReview').src = "{/literal}{$spmgsnipreviewcaptchastore nofilter}{literal}?re=" + count;
                                        {/literal}{/if}{literal}



                                    }
                                }

                            });

                        } else {
                            return false;
                        }

                    }));


                });
                });





            </script>
        {/literal}


        {/if}






<div class="testimonials-items {if $spmgsnipreviewis17 == 1}block-categories{/if}">








			


    <div class="row-custom total-info-tool">
            <div class="col-sm-6-custom first-block-ti">



					<strong class="float-left">
                        <span class="testimonials-count-items">{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}</span>
                        {l s='Store Reviews' mod='spmgsnipreview'}
                     </strong>

                <span class="separator-items-block float-left">-</span>


                <div {if $spmgsnipreviewt_tpages == 1}itemscope itemtype="http://schema.org/corporation"{/if} class="float-left total-rating-items-block">

                    {if $spmgsnipreviewt_tpages == 1}
                        <meta itemprop="name" content="{$spmgsnipreviewsh_nameti|escape:'htmlall':'UTF-8'}">
                        <meta itemprop="url" content="{$spmgsnipreviewsh_urlti|escape:'htmlall':'UTF-8'}">
                    {/if}


                <div {if $spmgsnipreviewt_tpages == 1}itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating"{/if}>

                    {if $spmgsnipreviewt_tpages == 1}
                        <meta itemprop="reviewCount" content="{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}">
                    {/if}


                    {assign var='test_rating_store' value=$spmgsnipreviewavg_decimalti|replace:',':'.'}

                    {section name=ratid loop=5 start=0}
                        {if $smarty.section.ratid.index < $spmgsnipreviewavg_ratingti}

                            {if $test_rating_store <= $spmgsnipreviewmax_star_par && $test_rating_store >= $spmgsnipreviewmin_star_par}
                                <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"/>
                            {else}
                                <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                            {/if}

                        {else}
                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                        {/if}
                        {$test_rating_store = $test_rating_store - 1}
                    {/section}


                        <span {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}>
                        (<span {if $spmgsnipreviewt_tpages == 1}itemprop="ratingValue"{/if} {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}
                                    >{$spmgsnipreviewavg_decimalti|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewt_tpages == 1}itemprop="bestRating"{/if} {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}
                                    >5</span>)
                        </span>

                </div>

                </div>



               </div>
        <div class="col-sm-5-custom b-search-items">

                <form onsubmit="return false;" method="post" action="#">

                    <fieldset>
                        <input type="submit" value="{l s='go' mod='spmgsnipreview'}" class="button_mini_custom" onclick="go_page_spmgsnipreviewti(0,'allstorereviews','',1)">
                        <input type="text" class="txt" name="searchti" id="searchti"
                               onfocus="{literal}if(this.value == '{/literal}{l s='Search' mod='spmgsnipreview'}{literal}') {this.value='';};{/literal}"
                               onblur="{literal}if(this.value == '') {this.value='{/literal}{l s='Search' mod='spmgsnipreview'}{literal}';};{/literal}"
                               value="{l s='Search' mod='spmgsnipreview'}"
                                />



                            <a rel="nofollow" href="javascript:void(0)" id="clear-search-storereviews" class="clear-search-items display-none"
                               onclick="go_page_spmgsnipreviewti(0,'allstorereviews')"
                                    >
                                {l s='Clear search' mod='spmgsnipreview'}
                            </a>

                    </fieldset>
                </form>


        </div>

        </div>



{if $spmgsnipreviewis_filterallti}
    <div class="row-custom filter-testimonials">

            <div class="col-sm-1-custom">
                <b class="filter-txt-items-block">{l s='Filter' mod='spmgsnipreview'}:</b>
            </div>


            <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 5}active-items-block{/if}">
                {if $spmgsnipreviewfiveti>0}
                <a rel="nofollow"
                   onclick="go_page_spmgsnipreviewti(0,'allstorereviews',5,'{if $spmgsnipreviewis_searchti == 1}1{/if}')"
                   href="javascript:void(0)"
                   id="storereviews-rating-5"
                   >
                {/if}
                {section name="test" loop=5}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                {/section}
                <span class="count-items-block {if $spmgsnipreviewfiveti==0}text-decoration-none{/if}">({$spmgsnipreviewfiveti|escape:'htmlall':'UTF-8'})</span>

                {if $spmgsnipreviewfiveti>0}
                </a>
                {/if}
            </div>
            <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 4}active-items-block{/if}">
                {if $spmgsnipreviewfourti>0}
                <a rel="nofollow"
                   onclick="go_page_spmgsnipreviewti(0,'allstorereviews',4,'{if $spmgsnipreviewis_searchti == 1}1{/if}')"
                   href="javascript:void(0)"
                   id="storereviews-rating-4"
                   >
                {/if}
                    {section name="test" loop=4}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                    {/section}
                    {section name="test" loop=1}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                    {/section}

                <span class="count-items-block {if $spmgsnipreviewfourti==0}text-decoration-none{/if}">({$spmgsnipreviewfourti|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewfourti>0}
                </a>
                {/if}
            </div>
            <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 3}active-items-block{/if}">
                {if $spmgsnipreviewthreeti>0}
                <a rel="nofollow"

                   onclick="go_page_spmgsnipreviewti(0,'allstorereviews',3,'{if $spmgsnipreviewis_searchti == 1}1{/if}')"
                   href="javascript:void(0)"
                   id="storereviews-rating-3"

                   >
                {/if}
                    {section name="test" loop=3}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                    {/section}
                    {section name="test" loop=2}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                    {/section}
                    <span class="count-items-block {if $spmgsnipreviewthreeti==0}text-decoration-none{/if}">({$spmgsnipreviewthreeti|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewthreeti>0}
                </a>
                {/if}
            </div>
            <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 2}active-items-block{/if}">
                {if $spmgsnipreviewtwoti>0}
                <a rel="nofollow"

                   onclick="go_page_spmgsnipreviewti(0,'allstorereviews',2,'{if $spmgsnipreviewis_searchti == 1}1{/if}')"
                   href="javascript:void(0)"
                   id="storereviews-rating-2"


                        >
                {/if}
                    {section name="test" loop=2}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                    {/section}
                    {section name="test" loop=3}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                    {/section}

                    <span class="count-items-block {if $spmgsnipreviewtwoti==0}text-decoration-none{/if}">({$spmgsnipreviewtwoti|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewtwoti>0}
                </a>
                {/if}
            </div>
            <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 1}active-items-block{/if}">
                {if $spmgsnipreviewoneti>0}
                <a rel="nofollow"

                   onclick="go_page_spmgsnipreviewti(0,'allstorereviews',1,'{if $spmgsnipreviewis_searchti == 1}1{/if}')"
                   href="javascript:void(0)"
                   id="storereviews-rating-1"

                   >
                {/if}
                    {section name="test" loop=1}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                    {/section}
                    {section name="test" loop=4}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                    {/section}
                    <span class="count-items-block {if $spmgsnipreviewoneti==0}text-decoration-none{/if}">({$spmgsnipreviewoneti|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewoneti>0}
                </a>
                {/if}
            </div>


            <div class="col-sm-1-custom">
                <a rel="nofollow" href="javascript:void(0)"
                   onclick="go_page_spmgsnipreviewti(0,'allstorereviews')"
                   id="clear-rating-storereviews" class="reset-items-block display-none">
                    <i class="fa fa-refresh"></i>{l s='Reset' mod='spmgsnipreview'}
                </a>
            </div>



    </div>
{/if}

    {if $spmgsnipreviewis_searchti == 1}
        <h3 class="search-result-item">{l s='Results for' mod='spmgsnipreview'} <b>"{$spmgsnipreviewsearchti|escape:'quotes':'UTF-8'}"</b></h3>
        <br/>
    {/if}


    {if $count_all_reviewsti>0 && $spmgsnipreviewis_sortfti == 1}
        <div class="search-result-item float-right">

            <b>
                {l s='Sort by' mod='spmgsnipreview'}:
            </b>
            <select id="select_spmgsnipreview_sortti" data-actionti="allstorereviews">
                <option value="date_add:desc" selected="selected">--</option>
                <option value="rating:asc">Rating: Lowest first</option>
                <option value="rating:desc">Rating: Highest first</option>
                <option value="date_add:asc">Date: Oldest first</option>
                <option value="date_add:desc">Date: Newest first</option>
            </select>

        </div>
        <div class="clear"></div>
        <br/>
    {/if}



    {if $count_all_reviewsti > 0}

<div id="storereviews-list">


    {assign var="list_storereviews" value="modules/spmgsnipreview/views/templates/front/list_storereviews.tpl"}

    {if file_exists("{$tpl_dir}{$list_storereviews}")}
        {include file="{$tpl_dir}{$list_storereviews}"}
    {else}
        {include file="{$list_storereviews}"}
    {/if}


</div>

        <div class="text-align-center toolbar-paging" id="page_nav">
            {$pagingti nofilter}
        </div>

        </div>
{else}
	<div class="testimonials-no-items">
	{l s='There are not store reviews yet' mod='spmgsnipreview'}
	</div>
{/if}

</div>