{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}

{if count($reviewsti)>0}
{foreach from=$reviewsti item=review name=myLoop}
    <div {if $spmgsnipreviewt_tpages == 1}itemprop="review" itemscope itemtype="http://schema.org/Review"{/if}
         class="pl-animate {$spmgsnipreviewd_eff_shopti|escape:'htmlall':'UTF-8'}">


        <div class="row-custom shopreviews-items">


            {if $spmgsnipreviewis_avatar == 1 && $review.is_show_ava}
                <div class="col-sm-2-custom post_avatar">

                    <img
                            {if strlen($review.avatar)>0}
                                src="{$review.avatar|escape:'htmlall':'UTF-8'}"
                            {else}
                                src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/avatar_m.gif"
                            {/if}
                            alt="{$review.name|escape:'htmlall':'UTF-8'}"
                            />


                </div>
            {/if}
            <div class="col-sm-{if $spmgsnipreviewis_avatar != 1 && !$review.is_show_ava}12{else}10{/if}-custom info">

                {if $spmgsnipreviewt_tpages == 1}
                    <meta itemprop="itemReviewed" content="{$shop_name_snippetti|escape:'htmlall':'UTF-8'}"/>
                {/if}
                <span class="commentbody_center" {if $spmgsnipreviewt_tpages == 1}itemprop="description"{/if}>
				{$review.message|nl2br nofilter}

                    {if $review.is_show == 1 && strlen($review.response)>0}
                        <div class="admin-reply-on-testimonial">
                            <div class="owner-date-reply">{l s='Administrator' mod='spmgsnipreview'}: </div>
                            {$review.response|nl2br nofilter}
                        </div>
                    {/if}
                </span>

                <div class="clear"></div>


                {if $spmgsnipreviewis_filesti == 1}
                    {if count($review.files)>0}
                        <div  class="{if $spmgsnipreviewis16 == 1}row-custom{else}row-list-reviews{/if}">
                            {foreach from=$review.files item=file}
                                <div class="col-sm-{if $spmgsnipreviewis16 == 1}2{else}{if $review.criterions|@count>0}4{else}6{/if}{/if}-custom files-review-spmgsnipreview">
                                    <a class="fancybox shown" data-fancybox-group="other-views" href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$file.full_path|escape:'htmlall':'UTF-8'}">
                                        <img class="img-responsive" width="105" height="105" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$file.small_path|escape:'htmlall':'UTF-8'}" alt="{$file.id|escape:'htmlall':'UTF-8'}"

                                                >
                                    </a>

                                </div>
                            {/foreach}
                        </div>
                        <div class="clear"></div>
                        <br/><br/>

                    {/if}
                {/if}


                <div class="row-custom">



                    {if $review.criterions|@count>0}
                        <div class="col-sm-6-custom">
                            <div class="spr-review-content">

                                {foreach from=$review.criterions item=criterion}
                                    <div class="criterion-item-block">
                                        {if strlen(trim($criterion.name))>0}
                                            {$criterion.name|escape:'htmlall':'UTF-8'}:
                                        {/if}

                                        {section name=ratid loop=5}
                                            {if $smarty.section.ratid.index < $criterion.rating}
                                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                            {else}
                                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list"  alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                            {/if}
                                        {/section}

                                    </div>
                                {/foreach}

                            </div>
                        </div>
                    {/if}



                    <div class="col-sm-{if $review.criterions|@count>0}6{else}12{/if}-custom">

                        <span class="foot_center margin-top-10">{l s='Posted by' mod='spmgsnipreview'} <b {if $spmgsnipreviewt_tpages == 1}itemprop="author"{/if}
                                    >{if $spmgsnipreviewis_uprof  && $review.is_show_ava && $review.id_customer > 0}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}{$review.name}{if $spmgsnipreviewis_uprof && $review.id_customer > 0}</a>{/if}</b>{if $spmgsnipreviewt_tpages == 1}<meta
                            itemprop="name" content="{$review.name}"/>{/if}{if $spmgsnipreviewis_country == 1}{if strlen($review.country)>0}, <span
                                    class="fs-12">{$review.country}</span>{/if}{/if}{if $spmgsnipreviewis_city == 1}{if strlen($review.city)>0}, <span class="fs-12">{$review.city}</span>{/if}{/if}

                            {if $review.rating != 0}

                                {section name=bar loop=5 start=0}
                                    {if $smarty.section.bar.index < $review.rating}
                                        <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                    {else}
                                    <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                    {/if}
                                {/section}
                                <span {if $spmgsnipreviewt_tpages == 1}itemtype="http://schema.org/Rating" itemscope="" itemprop="reviewRating"{/if}>
                                    (<span {if $spmgsnipreviewt_tpages == 1}itemprop="ratingValue"{/if}>{$review.rating|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewt_tpages == 1}itemprop="bestRating"{/if}>5</span>)&nbsp;
                                </span>

                        {else}
                            {section name=bar loop=5 start=0}
                                <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            <span {if $spmgsnipreviewt_tpages == 1}itemtype="http://schema.org/Rating" itemscope="" itemprop="reviewRating"{/if}>
                                    (<span {if $spmgsnipreviewt_tpages == 1}itemprop="ratingValue"{/if}>{$review.rating|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewt_tpages == 1}itemprop="bestRating"{/if}>5</span>)&nbsp;
                            </span>
                            {/if}


                        </span>
                        <div class="clear"></div>

                        <span class="foot_center">{$review.date_add|date_format|escape:'htmlall':'UTF-8'}</span>
                        <br/>
                        {if $spmgsnipreviewt_tpages == 1}
                            <meta itemprop="datePublished" content="{$review.date_add|date_format:"%Y-%m-%d"|escape:'htmlall':'UTF-8'}"/>
                        {/if}

                        <span class="foot_center">
                        {if $spmgsnipreviewis_company == 1}
                            <b>{$review.company nofilter}</b>
                        {/if}

                            {if $spmgsnipreviewis_addr == 1}
                                <b>{$review.address nofilter}</b>
                            {/if}

                            {if $spmgsnipreviewis_web == 1}
                                {if strlen($review.web)>0}
                                    <a title="http://{$review.web|escape:'htmlall':'UTF-8'}" rel="nofollow"
                                       href="http://{$review.web|escape:'htmlall':'UTF-8'}">http://{$review.web|escape:'htmlall':'UTF-8'}</a>
                                {/if}
                            {/if}
                        </span>

                        <div class="clear"></div>
                        <span class="foot_center">{if $review.is_buy != 0}<span class="is_buy">{l s='Verified Purchase' mod='spmgsnipreview'}</span>{/if}</span>


                    </div>
                </div>


            </div>


        </div>


    </div>
{/foreach}


{if $spmgsnipreviewd_eff_shopti != "disable_all_effects"}
{literal}
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            $(document).ready(function(){
                spmgsnipreview_init_effects();
            });
        });
    </script>
{/literal}
{/if}

<div class="clear"></div>
{else}
    <div class="testimonials-no-items">
        {l s='There are not store reviews yet' mod='spmgsnipreview'}

    </div>
{/if}