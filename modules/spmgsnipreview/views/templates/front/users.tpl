{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewis17 == 1}
        <h2>{l s='All Users' mod='spmgsnipreview'}</h2>
    {else}
        {capture name=path}
            {l s='All Users' mod='spmgsnipreview'}
        {/capture}
    {/if}





<div class="b-inside-pages {if $spmgsnipreviewis17 == 1}block-categories{/if}">

<div id="top" class="b-column-c">
				
				<div class="b-wrapper">
					
					<div class="b-tab {if $spmgsnipreviewis16 == 1}b-tab-16-profile-page{/if}">
						<ul>
							<li class="current"><a href="#">{l s='Users' mod='spmgsnipreview'} ({$spmgsnipreviewdata_count_customers|escape:'htmlall':'UTF-8'})</a></li>
						</ul>					
					</div>
					
					<div class="b-search-friends">



                        <form onsubmit="return false;" method="post" action="#">

                            <fieldset>
                                <input type="submit" value="go" class="button_mini_custom" onclick="go_page_spmgsnipreviewu(0,'allusers','',1)">
                                <input type="text" class="txt"  name="searchu" id="searchu"
                                       onfocus="{literal}if(this.value == '{/literal}{l s='Find in Users List' mod='spmgsnipreview'}{literal}') {this.value='';};{/literal}" onblur="{literal}if(this.value == '') {this.value='{/literal}{l s='Find in Users List' mod='spmgsnipreview'}{literal}';};{/literal}"
                                       onblur="{literal}if(this.value == '{/literal}{l s='Find in Users List' mod='spmgsnipreview'}{literal}') {this.value='';};{/literal}" onblur="{literal}if(this.value == '') {this.value='{/literal}{l s='Find in Users List' mod='spmgsnipreview'}{literal}';};{/literal}"
                                       value="{l s='Find in Users List' mod='spmgsnipreview'}" />

                                    <a rel="nofollow" href="javascript:void(0)" id="clear-search-users" class="clear-search-items display-none"
                                       onclick="go_page_spmgsnipreviewu(0,'allusers','','')">
                                        {l s='Clear search' mod='spmgsnipreview'}
                                    </a>


                            </fieldset>
                        </form>

					</div>

                    {if $spmgsnipreviewis_search == 1}
                        <h3 class="search-result-item-user">{l s='Results for' mod='spmgsnipreview'} <b>"{$spmgsnipreviewsearch|escape:'quotes':'UTF-8'}"</b></h3>
                        <br/>
                    {/if}
					
					<div class="b-friends-list">
					{if count($spmgsnipreviewcustomers)>0}
					<ul id="users-list">

                        {assign var="list_users" value="modules/spmgsnipreview/views/templates/front/list_users.tpl"}

                        {if file_exists("{$tpl_dir}{$list_users}")}
                            {include file="{$tpl_dir}{$list_users}"}
                        {else}
                            {include file="{$list_users}"}
                        {/if}



					</ul>
					{else}
						<div class="spmgsnipreview-not-found">
							{l s='Users not found' mod='spmgsnipreview'}
						</div>
					{/if}	
						
					</div>

                    {if count($spmgsnipreviewcustomers)>0}
                    <div class="paging-users-custom" id="page_navu">
                        {$spmgsnipreviewpaging nofilter}
                    </div>
                    {/if}


				</div>
				

			</div>
			
</div>

