{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}

{if count($reviews_all)>0}
{foreach from=$reviews_all item=review}
    <div class="spr-review pl-animater {$spmgsnipreviewd_eff_rev|escape:'htmlall':'UTF-8'}">
        <div class="spr-review-header">
            {if $review.is_active == 1}
                {if $spmgsnipreviewratings_on == 1 && $review.rating!=0}
                    <span class="spr-starratings spr-review-header-starratings">

                                      {section name=ratid loop=5}
                                          {if $smarty.section.ratid.index < $review.rating}
                                              <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                        {else}
                                            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"  alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                          {/if}
                                      {/section}

                                    </span>
                    <div itemtype="http://schema.org/Rating" itemscope="" itemprop="reviewRating" class="rating-stars-total">
                        (<span itemprop="ratingValue">{$review.rating|escape:'htmlall':'UTF-8'}</span>/<span itemprop="bestRating">5</span>)&nbsp;
                    </div>

                {/if}

                {if $spmgsnipreviewtitle_on == 1 && strlen($review.title_review)>0}
                    <h3 class="spr-review-header-title">
                        <a href="{$spmgsnipreviewrev_url|escape:'htmlall':'UTF-8'}{if $spmgsnipreviewis_rewrite == 1}?{else}&{/if}rid={$review.id|escape:'htmlall':'UTF-8'}"
                           title="{$review.title_review|escape:'htmlall':'UTF-8' nofilter}" class="title-review-all">
                            {$review.title_review|escape:'htmlall':'UTF-8' nofilter}
                        </a>
                    </h3>
                {/if}

            {/if}
            <div class="clear-spmgsnipreview"></div>

                            <span class="spr-review-header-byline float-left">
                                {l s='By' mod='spmgsnipreview'}
                                {if $spmgsnipreviewis_avatarr == 1 && strlen($review.avatar)>0 && $review.is_show_ava}

                                    <span class="avatar-block-rev">
                                        <img alt="{$review.customer_name|escape:'htmlall':'UTF-8' nofilter}"
                                             src="{$review.avatar|escape:'htmlall':'UTF-8'}">
                                     </span>

                                {/if}

                                {if strlen($review.customer_name)>0}
                                    {if $spmgsnipreviewis_uprof  && $review.is_show_ava && $review.id_customer > 0}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.customer_name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}<strong
                                            >{$review.customer_name|escape:'htmlall':'UTF-8' nofilter}</strong>{if $spmgsnipreviewis_uprof && $review.id_customer > 0}</a>{/if}
                                {/if}
                                {if strlen($review.customer_name)>0}{l s='on' mod='spmgsnipreview'}{/if}&nbsp;<strong>{$review.time_add|date_format|escape:'htmlall':'UTF-8'}</strong>
                                {if $spmgsnipreviewip_on == 1 && strlen($review.ip)>0}
                                    ({if $review.is_no_ip == 0}<b>{l s='IP' mod='spmgsnipreview'}:</b>&nbsp;{/if}{$review.ip|escape:'htmlall':'UTF-8'})
                                {/if}
                                <span itemprop="itemReviewed" class="font-size-10">
                                    <a href="{$review.product_link|escape:'quotes':'UTF-8'}"
                                       title="{$review.product_name|escape:'quotes':'UTF-8'}"
                                       class="title-review-all">
                                        {$review.product_name|escape:'quotes':'UTF-8'}
                                    </a>
                                </span>

                            </span>

            {if $review.is_active == 1}
                {if $spmgsnipreviewis_helpfulf == 1}
                    <span class="float-right people-folowing-reviews" id="people-folowing-reviews{$review.id|escape:'htmlall':'UTF-8'}"><span class="first-helpful" id="block-helpful-yes{$review.id|escape:'htmlall':'UTF-8'}">{$review.helpfull_yes|escape:'quotes':'UTF-8'}</span> {l s='of' mod='spmgsnipreview'} <span id="block-helpful-all{$review.id|escape:'htmlall':'UTF-8'}">{$review.helpfull_all|escape:'quotes':'UTF-8'}</span> {l s='people found the following review helpful' mod='spmgsnipreview'}</span>
                {/if}
            {/if}
            <div class="clear-spmgsnipreview"></div>

            {if $review.is_buy != 0}
                <span class="spr-review-header-byline float-left">
                                        <span class="is_buy_product">{l s='Verified Purchase' mod='spmgsnipreview'}</span>
                                    </span>
                <div class="clear-spmgsnipreview"></div>
            {/if}

        </div>

        <div class="{if $spmgsnipreviewis16 == 1}row-custom{else}row-list-reviews{/if}">


            {if $review.product_img}
                <div class="img-block-spmgsnipreview col-sm-2-custom">
                    <a href="{$spmgsnipreviewrev_url|escape:'htmlall':'UTF-8'}{if $spmgsnipreviewis_rewrite == 1}?{else}&{/if}rid={$review.id|escape:'htmlall':'UTF-8'}"
                       title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                            >
                        <img src="{$review.product_img|escape:'htmlall':'UTF-8'}" title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                             alt = "{$review.product_name|escape:'htmlall':'UTF-8'}" class="border-image-review img-responsive" />
                    </a>
                </div>
            {/if}


            {if $review.is_active == 1}
                {if $review.criterions|@count>0}
                    <div class="spr-review-content {if $spmgsnipreviewis16 == 1}col-sm-3-custom{else}col-sm-3-list-reviews{/if}">

                        {foreach from=$review.criterions item=criterion}
                            <div class="criterion-item-block">
                                {$criterion.name|escape:'htmlall':'UTF-8'}:

                                {section name=ratid loop=5}
                                    {if $smarty.section.ratid.index < $criterion.rating}
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                    {else}
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list"  alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                    {/if}
                                {/section}

                            </div>
                        {/foreach}

                    </div>
                {/if}
            {/if}

            <div class="spr-review-content {if $spmgsnipreviewis16 == 1}col-sm-{if $review.criterions|@count>0 && $review.is_active == 1}7{else}10{/if}-custom{else}col-sm-{if $review.criterions|@count>0 && $review.is_active == 1}7{else}10{/if}-list-reviews{/if}">

                {if $review.is_active == 1}
                    {if $spmgsnipreviewtext_on == 1 && strlen($review.text_review)>0}

                        <p class="spr-review-content-body">
                            {if strlen($review.text_review)>150}

                                {if $spmgsnipreviewtitle_on == 0 || strlen($review.title_review)==0}
                                    <a href="{$spmgsnipreviewrev_url|escape:'htmlall':'UTF-8'}{if $spmgsnipreviewis_rewrite == 1}?{else}&{/if}rid={$review.id|escape:'htmlall':'UTF-8'}"
                                       title="{$review.title_review|escape:'htmlall':'UTF-8'}" class="title-review-all">
                                        {$review.text_review|escape:'htmlall':'UTF-8'|nl2br nofilter}</a>
                                {else}

                                    {$review.text_review|substr:0:150|escape:'htmlall':'UTF-8'|nl2br nofilter}...&nbsp;
                                    <a href="{$spmgsnipreviewrev_url|escape:'htmlall':'UTF-8'}{if $spmgsnipreviewis_rewrite == 1}?{else}&{/if}rid={$review.id|escape:'htmlall':'UTF-8'}"
                                       title="{$review.title_review|escape:'htmlall':'UTF-8'}" class="title-review-all">
                                        {l s='more' mod='spmgsnipreview'}</a>

                                {/if}

                            {else}

                                {if $spmgsnipreviewtitle_on == 0 || strlen($review.title_review)==0}
                                    <a href="{$spmgsnipreviewrev_url|escape:'htmlall':'UTF-8'}{if $spmgsnipreviewis_rewrite == 1}?{else}&{/if}rid={$review.id|escape:'htmlall':'UTF-8'}"
                                       title="{$review.title_review|escape:'htmlall':'UTF-8'}" class="title-review-all">
                                        {$review.text_review|escape:'htmlall':'UTF-8'|nl2br nofilter}</a>
                                {else}
                                    {$review.text_review|escape:'htmlall':'UTF-8'|nl2br nofilter}
                                {/if}
                            {/if}
                        </p>
                        <br/>

                    {/if}


                    {if $spmgsnipreviewis_filesr == 1}
                        {if count($review.files)>0}
                            <div  class="{if $spmgsnipreviewis16 == 1}row-custom{else}row-list-reviews{/if}">
                                {foreach from=$review.files item=file}
                                    <div class="col-sm-{if $spmgsnipreviewis16 == 1}2{else}3{/if}-custom files-review-spmgsnipreview">
                                        <a class="fancybox shown" data-fancybox-group="other-views" href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$file.full_path|escape:'htmlall':'UTF-8'}">
                                            <img class="img-responsive" width="105" height="105" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$file.small_path|escape:'htmlall':'UTF-8'}" alt="{$file.id|escape:'htmlall':'UTF-8'}"

                                                    >
                                        </a>

                                    </div>
                                {/foreach}
                            </div>
                            <div class="clear-spmgsnipreview"></div>
                            {if $spmgsnipreviewis17 == 1}<br/><br/>{/if}
                        {/if}
                    {/if}

                {else}
                    <p class="spr-review-content-body">{l s='The customer has rated the product but has not posted a review, or the review is pending moderation' mod='spmgsnipreview'}</p>
                {/if}

                <div class="clear-spmgsnipreview"></div>




                {if $review.is_active == 1}
                {if strlen($review.admin_response)>0 && $review.is_display_old == 1}
                    <div class="clear-spmgsnipreview"></div>
                    <div class="shop-owner-reply-on-review">
                        <div class="owner-date-reply">{l s='Shop owner reply' mod='spmgsnipreview'} ({$review.review_date_update|date_format|escape:'htmlall':'UTF-8'}): </div>
                        {$review.admin_response|nl2br nofilter}
                    </div>

                {/if}

                    <div class="clear-spmgsnipreview"></div>
                    <div class="spr-review-footer row-custom">

                        {if $spmgsnipreviewis_helpfulf == 1}
                            <div class="col-sm-{if $spmgsnipreviewis_abusef == 0}12{else}9{/if}-custom margin-bottom-10" id="block-helpful{$review.id|escape:'htmlall':'UTF-8'}">
                                {l s='Was this review helpful to you?' mod='spmgsnipreview'}
                                <a class="btn-success button_padding_spmgsnipreview" title="{l s='Yes' mod='spmgsnipreview'}"
                                   href="javascript:void(0)" onclick="report_helpfull_spmgsnipreview({$review.id|escape:'htmlall':'UTF-8'},1)" ><b>{l s='Yes' mod='spmgsnipreview'}</b></a>
                                <a class="btn-danger button_padding_spmgsnipreview" title="{l s='No' mod='spmgsnipreview'}"
                                   href="javascript:void(0)" onclick="report_helpfull_spmgsnipreview({$review.id|escape:'htmlall':'UTF-8'},0)"><b>{l s='No' mod='spmgsnipreview'}</b></a>
                            </div>
                        {/if}

                        {if $spmgsnipreviewis_abusef == 1}
                            <div class="col-sm-{if $spmgsnipreviewis_helpfulf == 0}12{else}3{/if}-custom margin-bottom-10">
                                <a class="button_padding_spmgsnipreview spr-review-reportreview"
                                   title="{l s='Report abuse' mod='spmgsnipreview'}"
                                   href="javascript:void(0)" onclick="report_abuse_spmgsnipreview({$review.id|escape:'htmlall':'UTF-8'})"
                                        ><b><i class="fa fa-ban text-primary"></i>&nbsp;{l s='Report abuse' mod='spmgsnipreview'}</b></a>
                            </div>
                        {/if}


                        <div class="clear-spmgsnipreview"></div>

                    </div>

                {if $spmgsnipreviewrsoc_on == 1}
                    <div class="fb-like valign-top" data-href="{$spmgsnipreviewrev_url|escape:'htmlall':'UTF-8'}{if $spmgsnipreviewis_rewrite == 1}?{else}&{/if}rid={$review.id|escape:'htmlall':'UTF-8'}"
                         data-show-faces="false" data-width="60" data-send="false" data-layout="{if $spmgsnipreviewrsoccount_on == 1}button_count{else}button{/if}"></div>

                {literal}
                    <script type="text/javascript">

                        document.addEventListener("DOMContentLoaded", function(event) {
                            $(document).ready(function(){

                                /* Voucher, when a user share review on the Facebook */
                                // like

                                FB.Event.subscribe("edge.create", function(targetUrlReview) {

                                    //alert(targetUrlReview);
                                    if(targetUrlReview == '{/literal}{$spmgsnipreviewrev_url|escape:'htmlall':'UTF-8' nofilter}{if $spmgsnipreviewis_rewrite == 1}?{else}&{/if}rid={$review.id|escape:'htmlall':'UTF-8'}{literal}'){

                                        addRemoveDiscountShareReview('facebook',{/literal}{$review.id|escape:'htmlall':'UTF-8'}{literal});

                                    }
                                });
                                /* Voucher, when a user share review on the Facebook */

                            });
                        });
                    </script>
                {/literal}


                {/if}

                {/if}

            </div>
            <div class="clear-spmgsnipreview"></div>

        </div>






    </div>
{/foreach}




    {if $spmgsnipreviewd_eff_rev != "disable_all_effects"}
    {literal}
        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function(event) {
                $(document).ready(function(){
                    spmgsnipreview_init_effects_review();
                });
            });
        </script>
    {/literal}
    {/if}

{else}
    <div class="advertise-text-review advertise-text-review-text-align">
        {l s='There are not Product Reviews yet.' mod='spmgsnipreview'}

    </div>

{/if}