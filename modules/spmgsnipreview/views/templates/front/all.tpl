{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}

{capture name=path}{l s='All Reviews' mod='spmgsnipreview'}{/capture}


<h2>{l s='All Reviews' mod='spmgsnipreview'}</h2>



<div class="block-last-spmgsnipreviews block blockmanufacturer margin-top-10 {if $spmgsnipreviewis17 == 1}block-categories{/if}">


    <div class="row-custom total-info-tool">
        <div class="col-sm-6-custom first-block-ti">



            <strong class="float-left">
                <span class="testimonials-count-items">{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}</span>
                {l s='Reviews' mod='spmgsnipreview'}
            </strong>

            <span class="separator-items-block float-left">-</span>


            <div itemscope itemtype="http://schema.org/corporation" class="float-left total-rating-items-block">


                <meta itemprop="name" content="{$spmgsnipreviewsh_name|escape:'htmlall':'UTF-8'}">
                <meta itemprop="url" content="{$spmgsnipreviewallr_url|escape:'htmlall':'UTF-8'}">



                <div itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating">


                    <meta itemprop="reviewCount" content="{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}">

                    {assign var='test_rating' value=$spmgsnipreviewavg_rating}
                    {section name=ratid loop=5 start=0}
                        {if $smarty.section.ratid.index < $spmgsnipreviewavg_rating}

                            {if $test_rating <= $spmgsnipreviewmax_star_par && $test_rating >= $spmgsnipreviewmin_star_par}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                            {else}
                                <img src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                            {/if}

                        {else}
                            <img src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                        {/if}
                        {$test_rating = $test_rating - 1}
                    {/section}

                    <span {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}>
                        (<span itemprop="ratingValue" {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}
                                >{$spmgsnipreviewavg_decimal|escape:'htmlall':'UTF-8'}</span>/<span itemprop="bestRating" {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}
                                >5</span>)
                        </span>

                </div>

            </div>



        </div>
        <div class="col-sm-5-custom b-search-items">

            <form onsubmit="return false;" method="post" action="#">

                <fieldset>
                    <input type="submit" value="go" class="button_mini_custom" onclick="go_page_spmgsnipreviewr(0,'allpagereviews','',1)">
                    <input type="text" class="txt" name="searchr" id="searchr"
                           onfocus="{literal}if(this.value == '{/literal}{l s='Search' mod='spmgsnipreview'}{literal}') {this.value='';};{/literal}"
                           onblur="{literal}if(this.value == '') {this.value='{/literal}{l s='Search' mod='spmgsnipreview'}{literal}';};{/literal}"
                           value="{l s='Search' mod='spmgsnipreview'}" />

                        <a rel="nofollow" href="javascript:void(0)" id="clear-search-reviews" class="clear-search-items display-none"
                           onclick="go_page_spmgsnipreviewr(0,'allpagereviews','','')">
                            {l s='Clear search' mod='spmgsnipreview'}
                        </a>


                </fieldset>
            </form>


        </div>

    </div>

    {if $spmgsnipreviewis_filterall == 1}

    <div class="row-custom filter-reviews-spmgsnipreview">

        <div class="col-sm-1-custom">
            <b class="filter-txt-items-block">{l s='Filter' mod='spmgsnipreview'}:</b>
        </div>
        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 5}active-items-block{/if}">
            {if $spmgsnipreviewfive>0}
            <a rel="nofollow"

               onclick="go_page_spmgsnipreviewr(0,'allpagereviews',5,'{if $spmgsnipreviewis_search == 1}1{/if}')"
               href="javascript:void(0)"
               id="reviews-rating-5"

                    >
                {/if}
                {section name="test" loop=5}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                {/section}
                <span class="count-items-block {if $spmgsnipreviewfive==0}text-decoration-none{/if}">({$spmgsnipreviewfive|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewfive>0}
            </a>
            {/if}
        </div>
        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 4}active-items-block{/if}">
            {if $spmgsnipreviewfour>0}
            <a rel="nofollow"

               onclick="go_page_spmgsnipreviewr(0,'allpagereviews',4,'{if $spmgsnipreviewis_search == 1}1{/if}')"
               href="javascript:void(0)"
               id="reviews-rating-4"

                    >
                {/if}
                {section name="test" loop=4}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                {/section}
                {section name="test" loop=1}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                {/section}

                <span class="count-items-block {if $spmgsnipreviewfour==0}text-decoration-none{/if}">({$spmgsnipreviewfour|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewfour>0}
            </a>
            {/if}
        </div>
        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 3}active-items-block{/if}">
            {if $spmgsnipreviewthree>0}
            <a rel="nofollow"

               onclick="go_page_spmgsnipreviewr(0,'allpagereviews',3,'{if $spmgsnipreviewis_search == 1}1{/if}')"
               href="javascript:void(0)"
               id="reviews-rating-3"

                    >
                {/if}
                {section name="test" loop=3}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                {/section}
                {section name="test" loop=2}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                {/section}
                <span class="count-items-block {if $spmgsnipreviewthree==0}text-decoration-none{/if}">({$spmgsnipreviewthree|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewthree>0}
            </a>
            {/if}
        </div>
        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 2}active-items-block{/if}">
            {if $spmgsnipreviewtwo>0}
            <a rel="nofollow"

               onclick="go_page_spmgsnipreviewr(0,'allpagereviews',2,'{if $spmgsnipreviewis_search == 1}1{/if}')"
               href="javascript:void(0)"
               id="reviews-rating-2"

                    >
                {/if}
                {section name="test" loop=2}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                {/section}
                {section name="test" loop=3}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                {/section}

                <span class="count-items-block {if $spmgsnipreviewtwo==0}text-decoration-none{/if}">({$spmgsnipreviewtwo|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewtwo>0}
            </a>
            {/if}
        </div>
        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 1}active-items-block{/if}">
            {if $spmgsnipreviewone>0}
            <a rel="nofollow"

               onclick="go_page_spmgsnipreviewr(0,'allpagereviews',1,'{if $spmgsnipreviewis_search == 1}1{/if}')"
               href="javascript:void(0)"
               id="reviews-rating-1"

                    >
                {/if}
                {section name="test" loop=1}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                {/section}
                {section name="test" loop=4}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                {/section}
                <span class="count-items-block {if $spmgsnipreviewone==0}text-decoration-none{/if}">({$spmgsnipreviewone|escape:'htmlall':'UTF-8'})</span>
                {if $spmgsnipreviewone>0}
            </a>
            {/if}
        </div>


            <div class="col-sm-1-custom">
                <a rel="nofollow" href="javascript:void(0)"
                   onclick="go_page_spmgsnipreviewr(0,'allpagereviews','','')"
                   id="clear-rating-reviews" class="reset-items-block display-none">
                    <i class="fa fa-refresh"></i>{l s='Reset' mod='spmgsnipreview'}
                </a>
            </div>



    </div>


    {/if}

    {if $spmgsnipreviewis_search == 1}
        <h3 class="search-result-item">{l s='Results for' mod='spmgsnipreview'} <b>"{$spmgsnipreviewsearch|escape:'quotes':'UTF-8'}"</b></h3>
        <br/>
    {/if}


    {if count($reviews_all)>0 && $spmgsnipreviewis_sortallf == 1}
    <div class="search-result-item float-right">

        <b>
            {l s='Sort by' mod='spmgsnipreview'}:
        </b>
        <select id="select_spmgsnipreview_sort" data-action="allpagereviews">
            <option value="time_add:desc" selected="selected">--</option>
            {if $spmgsnipreviewratings_on == 1}
            <option value="rating:asc">Rating: Lowest first</option>
            <option value="rating:desc">Rating: Highest first</option>
            {/if}
            <option value="time_add:asc">Date: Oldest first</option>
            <option value="time_add:desc">Date: Newest first</option>
        </select>

    </div>
    <div class="clear"></div>
    <br/>
    {/if}



		<div class="block_content" id="shopify-product-reviews">
			{if count($reviews_all)>0}

                <div class="spr-reviews" id="reviews-list">



                        {assign var="list_allreviews" value="modules/spmgsnipreview/views/templates/front/list_allreviews.tpl"}

                        {if file_exists("{$tpl_dir}{$list_allreviews}")}
                            {include file="{$tpl_dir}{$list_allreviews}"}
                        {else}
                            {include file="{$list_allreviews}"}
                        {/if}


                </div>



	    	{else}
	    		<div class="gsniprev-block-noreviews-list">
					{l s='There are not Product Reviews yet.' mod='spmgsnipreview'}
				</div>
	    	{/if}
	    </div>
	    {if count($reviews_all)>0}

	        <div id="page_navr">{$spmgsnipreviewpaging nofilter}</div>

	    {/if}
</div>


