{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewis16 == 1 && $spmgsnipreviewis17 ==0}
{capture name=path}<a href="{$spmgsnipreviewmy_a_link|escape:'htmlall':'UTF-8'}">{l s='My account' mod='spmgsnipreview'}</a>
<span class="navigation-pipe">{$navigationPipe}</span>{l s='Product Reviews' mod='spmgsnipreview'}{/capture}
{/if}


{if $spmgsnipreviewis17 ==1}
    <a href="{$spmgsnipreviewmy_a_link|escape:'htmlall':'UTF-8'}">{l s='My account' mod='spmgsnipreview'}</a>
        <span class="navigation-pipe"> > </span>{l s='Product Reviews' mod='spmgsnipreview'}
{/if}

    <h3 class="page-product-heading">{l s='Product Reviews' mod='spmgsnipreview'}</h3>



{if $spmgsnipreviewis_reminder}
<div class="checkbox-my-acc" id="reminder_status_block">

    <span class="spmgsnipreviews-float-left">
        <input type="checkbox" {if $spmgsnipreviewrem_status == 1}checked="checked"{/if} name="reminder_status" id="reminder_status" onclick="change_reminder_status()">
    </span>
    <span class="spmgsnipreviews-float-left">
    <i class="fa fa-bell-o fa-lg"></i>
     </span>
    <span class="spmgsnipreviews-float-left">
    {l s='Send me a reminder e-mail to rate purchased products after each my order' mod='spmgsnipreview'}
    </span>
    <div class="spmgsnipreviews-clear"></div>

</div>
<br/>
{/if}


{if count($spmgsnipreviewmy_reviews)>0}
<div class="block-center {if $spmgsnipreviewis17 == 1}block-categories{/if}" id="block-history">
	<div id="gsniprev-list">
	<table id="block-history" class="table table-bordered {if $spmgsnipreviewis16 == 1}responsive-table-custom{/if}">
        <thead>
			<tr>
				<th class="first_item">
						{l s='Product' mod='spmgsnipreview'}
				</th>
				{if $spmgsnipreviewratings_on == 1}
				<th class="item" style="width:200px">
						{l s='Total Rating' mod='spmgsnipreview'}
				</th>
				{/if}
				{if $spmgsnipreviewtitle_on == 1}
				<th class="item">
						{l s='Title' mod='spmgsnipreview'}
				</th>
				{/if}
				<th class="item">
						{l s='Purchased' mod='spmgsnipreview'}?
				</th>
				<th class="item">
						{l s='Date Add' mod='spmgsnipreview'}
				</th>
                <th class="item">
                    {l s='Suggestion to modify review' mod='spmgsnipreview'}?
                </th>
				<th class="last_item">
						{l s='Status' mod='spmgsnipreview'}
				</th>
			</tr>
        </thead>
        <tbody id="reviews-list">

            {assign var="list_myreviews" value="modules/spmgsnipreview/views/templates/front/list_myreviews.tpl"}

            {if file_exists("{$tpl_dir}{$list_myreviews}")}
                {include file="{$tpl_dir}{$list_myreviews}"}
            {else}
                {include file="{$list_myreviews}"}
            {/if}

		</tbody>
	</table>
	
	<div id="page_navr">
        {$spmgsnipreviewpaging nofilter}
    </div>
	
	</div>


</div>

    {literal}

        <script type="text/javascript">



        function modify_review(id_review,action,id_customer){

                $.post('{/literal}{$spmgsnipreviewreviews_url nofilter}{literal}',
                        { id_review:id_review,
                            action:action,
                            id_customer: id_customer
                        },
                        function (data) {
                            if (data.status == 'success') {


                                var data = data.params.content;


                                    if ($('div#fb-con-wrapper').length == 0) {
                                        conwrapper = '<div id="fb-con-wrapper"><\/div>';
                                        $('body').append(conwrapper);
                                    } else {
                                        $('#fb-con-wrapper').html('');
                                    }

                                    if ($('div#fb-con').length == 0) {
                                        condom = '<div id="fb-con"><\/div>';
                                        $('body').append(condom);
                                    }

                                    $('div#fb-con').fadeIn(function () {

                                        $(this).css('filter', 'alpha(opacity=70)');
                                        $(this).bind('click dblclick', function () {
                                            $('div#fb-con-wrapper').hide();
                                            $(this).fadeOut();
                                            //window.location.reload();
                                        });
                                    });


                                    $('div#fb-con-wrapper').html('<a id="button-close" style="display: inline;"><\/a>' + data).fadeIn();

                                    $("a#button-close").click(function () {
                                        $('div#fb-con-wrapper').hide();
                                        $('div#fb-con').fadeOut();
                                    });

                                    $("button#cancel-report").click(function () {
                                        $('div#fb-con-wrapper').hide();
                                        $('div#fb-con').fadeOut();
                                    });

                                  





                            } else {
                                alert(data.message);

                            }
                        }, 'json');
            }

        </script>
        {/literal}


{else}
<div class="advertise-text-review advertise-text-review-text-align">
	{l s='There are not Product Reviews yet.' mod='spmgsnipreview'}
</div>
{/if}


    <br/>
    <ul class="footer_links clearfix">

        <li class="float-left margin-right-10">
            <a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}" class="btn btn-default button button-small-spmgsnipreview">
                <span><i class="icon-chevron-left"></i> {l s='Home' mod='spmgsnipreview'}</span>
            </a>
        </li>

        <li class="float-left">
            <a href="{$spmgsnipreviewmy_a_link|escape:'htmlall':'UTF-8'}" class="btn btn-default button button-small-spmgsnipreview">
			<span>
				<i class="icon-chevron-left"></i> {l s='Back to Your Account' mod='spmgsnipreview'}
			</span>
            </a>
        </li>

    </ul>



{literal}
<style type="text/css">
table.std th, table.table_block th{padding:5px!important}
</style>
{/literal}

{literal}

<script type="text/javascript">

    {/literal}{if $spmgsnipreviewis_reminder}{literal}
    function change_reminder_status(){
        if($("input#reminder_status").is(":checked")) {
            var reminder_status = 1;
        } else {
            var reminder_status = 0;
        }
        $.post('{/literal}{$spmgsnipreviewreviews_url nofilter}{literal}',
                {action:'change-reminder',
                    reminder_status: reminder_status
                },
                function (data) {

                    if (data.status == 'success') {



                        $("#confirm-reminder").remove();

                        if(reminder_status == 1){
                            var status_text = '{/literal}{$spmgsnipreviewmyr_msg1|escape:'htmlall':'UTF-8'}{literal}';
                        } else {
                            var status_text = '{/literal}{$spmgsnipreviewmyr_msg2|escape:'htmlall':'UTF-8'}{literal}';
                        }

                        var html = '<div id="confirm-reminder" class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+
                                '{/literal}{$spmgsnipreviewmyr_msg3|escape:'htmlall':'UTF-8'}{literal}'+
                                '&nbsp;({/literal}{$spmgsnipreviewmyr_msg4|escape:'htmlall':'UTF-8'}{literal}'+
                                '&nbsp;<strong>'+status_text+'</strong>)</div>';

                        $("#reminder_status_block").after(html);


                    } else {

                        alert(data.message);

                    }
                }, 'json');
    }
    {/literal}{/if}{literal}



</script>

<style type="text/css">


    @media
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {

        /* Force table to not be like tables anymore */
        table.responsive-table-custom, table.responsive-table-custom thead,
        table.responsive-table-custom tbody,
        table.responsive-table-custom th,
        table.responsive-table-custom td,
        table.responsive-table-custom tr {
            display: block!important;
        }

        /* Hide table headers (but not display: none;, for accessibility) */
        table.responsive-table-custom thead tr {
            position: absolute!important;;
            top: -9999px!important;;
            left: -9999px!important;;
        }

        table.responsive-table-custom tr { border: 1px solid #ccc; }

        table.responsive-table-custom td {
            /* Behave  like a "row" */
            border: none!important;;
            border-bottom: 1px solid #eee!important;;
            position: relative!important;;
            padding-left: 50%!important;;

        }

        table.responsive-table-custom td:before {
            /* Now like a table header */
            position: absolute!important;;
            /* Top/left values mimic padding */
            top: 6px!important;;
            left: 6px!important;;
            width: 45%!important;;
            padding-right: 10px!important;;
            white-space: nowrap!important;;
        }

        /*
        Label the data
        */
        table.responsive-table-custom td:nth-of-type(1):before { content: "{/literal}{l s='Product' mod='spmgsnipreview'}{literal}"; }
        table.responsive-table-custom td:nth-of-type(2):before { content: "{/literal}{l s='Total Rating' mod='spmgsnipreview'}{literal}"; }
        table.responsive-table-custom td:nth-of-type(3):before { content: "{/literal}{l s='Title' mod='spmgsnipreview'}{literal}"; }
        table.responsive-table-custom td:nth-of-type(4):before { content: "{/literal}{l s='Purchased' mod='spmgsnipreview'}{literal}?"; }
        table.responsive-table-custom td:nth-of-type(5):before { content: "{/literal}{l s='Date Add' mod='spmgsnipreview'}{literal}"; }
        table.responsive-table-custom td:nth-of-type(6):before { content: "{/literal}{l s='Suggestion to modify review' mod='spmgsnipreview'}{literal}?"; }
        table.responsive-table-custom td:nth-of-type(7):before { content: "{/literal}{l s='Status' mod='spmgsnipreview'}{literal}"; }

    }



</style>

{/literal}


