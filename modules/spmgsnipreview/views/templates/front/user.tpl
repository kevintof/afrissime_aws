{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{foreach from=$spmgsnipreviewcustomer item=customer name=myLoop}

{if $spmgsnipreviewis16 == 0}



    <a href="{$spmgsnipreviewshoppers_url|escape:'htmlall':'UTF-8'}">{l s='Shoppers' mod='spmgsnipreview'}</a>
    <span class="navigation-pipe">></span>
    {$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}

{else}

    {if $spmgsnipreviewis17 == 1}
        <a href="{$spmgsnipreviewshoppers_url|escape:'htmlall':'UTF-8'}">{l s='Users' mod='spmgsnipreview'}</a>
        <span class="navigation-pipe">></span>
        {$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}
    {else}
    {capture name=path}
<a href="{$spmgsnipreviewshoppers_url|escape:'htmlall':'UTF-8'}">{l s='Users' mod='spmgsnipreview'}</a>
	<span class="navigation-pipe">></span>
	{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}
    {/capture}
    {/if}

{/if}




<div class="b-product-item {if $spmgsnipreviewis17 == 1}block-categories{/if}">
					<div class="b-photo">
						<div class="block-photo">
							<img title="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}" alt="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}"
								 src="{$customer.avatar_thumb|escape:'htmlall':'UTF-8'}"
								 {if $customer.exist_avatar == 0}class="photo"{else}class="border-none"{/if}>
							<div class="data">
								<div>
									<strong>{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}</strong>
								</div>
								<div class="margin-top-5">
									<i>
									   {if strlen($customer.gender_txt)>0}{$customer.gender_txt|escape:'htmlall':'UTF-8'}{/if}
									   {if $customer.stats.age != "--"}
									   {if strlen($customer.stats.age)>0}{$customer.stats.age|escape:'htmlall':'UTF-8'} {l s='years' mod='spmgsnipreview'}{/if}
									   {/if}
									</i>
								</div>
							</div>
						</div>
					</div>
					
					
					
	<div class="b-description">
					
		<h1 class="fn">{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}</h1>
					

		<div class="data-of-item">
            <br/>
			<b>{l s='Registration date:' mod='spmgsnipreview'}</b> {$customer.date_add|escape:'htmlall':'UTF-8'}<br/><br/>
			<b>{l s='Last visit:' mod='spmgsnipreview'}</b> {$customer.stats.last_visit|escape:'htmlall':'UTF-8'}
		</div>


	</div>
				
<div class="clr"><!-- --></div>

<!--  tab -->
<div class="b-tab b-tab-16-profile-page" id="tabs-custom">
	<ul>
        {if $spmgsnipreviewis_proftab == 1}
		<li class="current">
			<a href="javascript:void(0)" data="spmgsnipreviewprofile">
			   	{l s='Profile' mod='spmgsnipreview'}</a>
		</li>
        {/if}
        {if $spmgsnipreviewrvis_on == 1}
        <li {if $spmgsnipreviewis_proftab == 0}class="current"{/if}>
            <a href="javascript:void(0)" data="spmgsnipreviewreviews">
                {l s='Product Reviews' mod='spmgsnipreview'} ({$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'})</a>
        </li>
        {/if}
        {if $spmgsnipreviewis_storerev == 1}
        <li {if $spmgsnipreviewis_proftab == 0 && $spmgsnipreviewrvis_on == 0}class="current"{/if}>
            <a href="javascript:void(0)" data="spmgsnipreviewstorereviews">
                {l s='Store Reviews' mod='spmgsnipreview'} ({$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'})</a>
        </li>
        {/if}
		
	</ul>
</div>

<!--  end tab  -->
				
<div id="tabs-custom-content">
    {if $spmgsnipreviewis_proftab == 1}
<div class="b-tab-wrapper current-tab-content" id="spmgsnipreviewprofile">
	<div class="b-details">
		<div class="items">

			<table class="title-first">
				<tr class="odd">
					<td>
						<b>{l s='Addresses for' mod='spmgsnipreview'}
						{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}:
						</b>
					</td>
				</tr>
			</table>
			{if count($customer.addresses)>0}
			{foreach from=$customer.addresses item=address name=ItemMyLoop}
			<table class="margin-top-10 title-first">
				<tr class="even">
					<td class="width-33">
						<b class="font-size-12">
							{l s='Location #' mod='spmgsnipreview'}{$smarty.foreach.ItemMyLoop.index+1|escape:'htmlall':'UTF-8'}:
						</b>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table class="title-first border-none">
				<tr class="even">
					<td>
						{if strlen($address.country)>0}{$address.country|escape:'htmlall':'UTF-8'}, <br>{/if}
						{if strlen($address.city)>0}{$address.city|escape:'htmlall':'UTF-8'}, <br/>{/if}
						{if strlen($address.postcode)>0}{$address.postcode|escape:'htmlall':'UTF-8'}{/if}
					</td>
				</tr>
			</table>
			
			
			{/foreach}
			{else}
			<table class="title-first border-none margin-top-10">
				<tr class="even">
					<td>
						<b class="font-size-12">{l s='Address Not Found.' mod='spmgsnipreview'}</b>
					</td>
				</tr>
			</table>
			{/if}

		</div>	
	</div>
</div>
    {/if}


{if $spmgsnipreviewrvis_on == 1}
<div class="b-tab-wrapper {if $spmgsnipreviewis_proftab == 0}current-tab-content{else}current-tab-content-hide{/if}" id="spmgsnipreviewreviews">
        <div class="b-details">
            <div class="items">


                <div class="row-custom total-info-tool">
                    <div class="col-sm-6-custom first-block-ti">



                        <strong class="float-left">
                            <span class="testimonials-count-items">{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}</span>
                            {l s='Product Reviews' mod='spmgsnipreview'}
                        </strong>

                        <span class="separator-items-block float-left">-</span>


                        <div itemscope itemtype="http://schema.org/corporation" class="float-left total-rating-items-block">


                            <meta itemprop="name" content="{$customer.firstname|escape:'htmlall':'UTF-8'}{$customer.lastname|escape:'htmlall':'UTF-8'}">
                            <meta itemprop="url" content="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'}">



                            <div itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating">


                                <meta itemprop="reviewCount" content="{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}">



                                {assign var='test_rating_product' value=$spmgsnipreviewavg_decimal|replace:',':'.'}

                                {section name=ratid loop=5 start=0}
                                    {if $smarty.section.ratid.index < $spmgsnipreviewavg_rating}

                                        {if $test_rating_product <= $spmgsnipreviewmax_star_par && $test_rating_product >= $spmgsnipreviewmin_star_par}
                                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"/>
                                        {else}
                                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/if}

                                    {else}
                                        <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                    {/if}
                                    {$test_rating_product = $test_rating_product - 1}
                                {/section}




                                <span {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}>
                        (<span itemprop="ratingValue" {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}
                                            >{$spmgsnipreviewavg_decimal|escape:'htmlall':'UTF-8'}</span>/<span itemprop="bestRating" {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}
                                            >5</span>)
                        </span>

                            </div>

                        </div>



                    </div>
                    <div class="col-sm-5-custom b-search-items">

                        <form onsubmit="return false;" method="post" action="#">
                            <input type="hidden" name="uid" value="{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'}"/>
                            <fieldset>
                                <input type="submit" value="go" class="button_mini_custom" onclick="go_page_spmgsnipreviewr(0,'userpagereviews','',1,{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})">
                                <input type="text" class="txt" name="searchr" id="searchr"
                                       onfocus="{literal}if(this.value == '{/literal}{l s='Search' mod='spmgsnipreview'}{literal}') {this.value='';};{/literal}"
                                       onblur="{literal}if(this.value == '') {this.value='{/literal}{l s='Search' mod='spmgsnipreview'}{literal}';};{/literal}"
                                       value="{l s='Search' mod='spmgsnipreview'}" />

                                    <a rel="nofollow" href="javascript:void(0)" id="clear-search-reviews" class="clear-search-items display-none"
                                       onclick="go_page_spmgsnipreviewr(0,'userpagereviews','','',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                                            >
                                        {l s='Clear search' mod='spmgsnipreview'}
                                    </a>


                            </fieldset>
                        </form>


                    </div>

                </div>

                <div class="row-custom filter-reviews-spmgsnipreview">

                    <div class="col-sm-1-custom">
                        <b class="filter-txt-items-block">{l s='Filter' mod='spmgsnipreview'}:</b>
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 5}active-items-block{/if}">
                        {if $spmgsnipreviewfive>0}
                        <a rel="nofollow"

                           onclick="go_page_spmgsnipreviewr(0,'userpagereviews',5,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="reviews-rating-5"
                                >
                            {/if}
                            {section name="test" loop=5}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            <span class="count-items-block {if $spmgsnipreviewfive==0}text-decoration-none{/if}">({$spmgsnipreviewfive|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewfive>0}
                        </a>
                        {/if}
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 4}active-items-block{/if}">
                        {if $spmgsnipreviewfour>0}
                        <a rel="nofollow"

                           onclick="go_page_spmgsnipreviewr(0,'userpagereviews',4,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="reviews-rating-4"

                                >
                            {/if}
                            {section name="test" loop=4}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            {section name="test" loop=1}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}

                            <span class="count-items-block {if $spmgsnipreviewfour==0}text-decoration-none{/if}">({$spmgsnipreviewfour|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewfour>0}
                        </a>
                        {/if}
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 3}active-items-block{/if}">
                        {if $spmgsnipreviewthree>0}
                        <a rel="nofollow"

                           onclick="go_page_spmgsnipreviewr(0,'userpagereviews',3,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="reviews-rating-3"

                                >
                            {/if}
                            {section name="test" loop=3}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}
                            {section name="test" loop=2}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            <span class="count-items-block {if $spmgsnipreviewthree==0}text-decoration-none{/if}">({$spmgsnipreviewthree|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewthree>0}
                        </a>
                        {/if}
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 2}active-items-block{/if}">
                        {if $spmgsnipreviewtwo>0}
                        <a rel="nofollow"

                           onclick="go_page_spmgsnipreviewr(0,'userpagereviews',2,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="reviews-rating-2"

                                >
                            {/if}
                            {section name="test" loop=2}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}
                            {section name="test" loop=3}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}

                            <span class="count-items-block {if $spmgsnipreviewtwo==0}text-decoration-none{/if}">({$spmgsnipreviewtwo|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewtwo>0}
                        </a>
                        {/if}
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 1}active-items-block{/if}">
                        {if $spmgsnipreviewone>0}
                        <a rel="nofollow"


                           onclick="go_page_spmgsnipreviewr(0,'userpagereviews',1,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="reviews-rating-1"

                                >
                            {/if}
                            {section name="test" loop=1}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}
                            {section name="test" loop=4}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}
                            <span class="count-items-block {if $spmgsnipreviewone==0}text-decoration-none{/if}">({$spmgsnipreviewone|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewone>0}
                        </a>
                        {/if}
                    </div>


                        <div class="col-sm-1-custom">
                            <a rel="nofollow" href="javascript:void(0)"
                               onclick="go_page_spmgsnipreviewr(0,'userpagereviews','','',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                               id="clear-rating-reviews" class="reset-items-block display-none"
                               >
                                <i class="fa fa-refresh"></i>{l s='Reset' mod='spmgsnipreview'}
                            </a>
                        </div>



                </div>

                {if $spmgsnipreviewis_search == 1}
                    <h3 class="search-result-item">{l s='Results for' mod='spmgsnipreview'} <b>"{$spmgsnipreviewsearch|escape:'quotes':'UTF-8'}"</b></h3>
                    <br/>
                {/if}



                {if count($reviews_all)>0 && $spmgsnipreviewis_sortfu == 1}
                    <div class="search-result-item float-right">

                        <b>
                            {l s='Sort by' mod='spmgsnipreview'}:
                        </b>
                        <select id="select_spmgsnipreview_sort" data-action="userpagereviews" data-id-customer="{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'}">
                            <option value="time_add:desc" selected="selected">--</option>
                            {if $spmgsnipreviewratings_on == 1}
                                <option value="rating:asc">Rating: Lowest first</option>
                                <option value="rating:desc">Rating: Highest first</option>
                            {/if}
                            <option value="time_add:asc">Date: Oldest first</option>
                            <option value="time_add:desc">Date: Newest first</option>
                        </select>

                    </div>
                    <div class="clear"></div>
                    <br/>
                {/if}



                <div class="block_content" id="shopify-product-reviews">
                    {if count($reviews_all)>0}

                        <div class="spr-reviews" id="reviews-list">

                            {assign var="list_allreviews" value="modules/spmgsnipreview/views/templates/front/list_allreviews.tpl"}


                            {if file_exists("{$tpl_dir}{$list_allreviews}")}
                                {include file="{$tpl_dir}{$list_allreviews}"}
                            {else}
                                {include file="{$list_allreviews}"}
                            {/if}


                        </div>



                    {else}
                        <div class="gsniprev-block-noreviews-list">
                            {l s='There are not Product Reviews yet.' mod='spmgsnipreview'}
                        </div>
                    {/if}
                </div>
                {if count($reviews_all)>0}
                    <div class="text-align-center" id="page_navr">
                        {$spmgsnipreviewpaging nofilter}
                    </div>
                {/if}




            </div>
        </div>
        </div>
{/if}




    {if $spmgsnipreviewis_storerev == 1}
        <div class="b-tab-wrapper {if $spmgsnipreviewis_proftab == 0 && $spmgsnipreviewrvis_on == 0}current-tab-content{else}current-tab-content-hide{/if} padding-10" id="spmgsnipreviewstorereviews">






                <div class="row-custom total-info-tool">
                    <div class="col-sm-6-custom first-block-ti">



                        <strong class="float-left">
                            <span class="testimonials-count-items">{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}</span>
                            {l s='Store Reviews' mod='spmgsnipreview'}
                        </strong>

                        <span class="separator-items-block float-left">-</span>


                        <div {if $spmgsnipreviewt_tpages == 1}itemscope itemtype="http://schema.org/corporation"{/if} class="float-left total-rating-items-block">

                            {if $spmgsnipreviewt_tpages == 1}

                                <meta itemprop="name" content="{$customer.firstname|escape:'htmlall':'UTF-8'}{$customer.lastname|escape:'htmlall':'UTF-8'}">
                                <meta itemprop="url" content="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'}">

                            {/if}


                            <div {if $spmgsnipreviewt_tpages == 1}itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating"{/if}>

                                {if $spmgsnipreviewt_tpages == 1}
                                    <meta itemprop="reviewCount" content="{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}">
                                {/if}



                                {assign var='test_rating_store' value=$spmgsnipreviewavg_decimalti|replace:',':'.'}

                                {section name=ratid loop=5 start=0}
                                    {if $smarty.section.ratid.index < $spmgsnipreviewavg_ratingti}

                                        {if $test_rating_store <= $spmgsnipreviewmax_star_par && $test_rating_store >= $spmgsnipreviewmin_star_par}
                                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"/>
                                        {else}
                                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/if}

                                    {else}
                                        <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                    {/if}
                                    {$test_rating_store = $test_rating_store - 1}
                                {/section}

                                <span {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}>
                        (<span {if $spmgsnipreviewt_tpages == 1}itemprop="ratingValue"{/if} {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}
                                            >{$spmgsnipreviewavg_decimalti|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewt_tpages == 1}itemprop="bestRating"{/if} {if $spmgsnipreviewis16 == 0}class="vertical-align-top"{/if}
                                            >5</span>)
                        </span>

                            </div>

                        </div>



                    </div>
                    <div class="col-sm-5-custom b-search-items">

                        <form onsubmit="return false;" method="post" action="#">
                            <input type="hidden" name="uid" value="{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'}"/>
                            <fieldset>
                                <input type="submit" value="go" class="button_mini_custom" onclick="go_page_spmgsnipreviewti(0,'userstorereviews','',1,{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})">
                                <input type="text" class="txt" name="searchti" id="searchti"
                                       onfocus="{literal}if(this.value == '{/literal}{l s='Search' mod='spmgsnipreview'}{literal}') {this.value='';};{/literal}"
                                       onblur="{literal}if(this.value == '') {this.value='{/literal}{l s='Search' mod='spmgsnipreview'}{literal}';};{/literal}"
                                       value="{l s='Search' mod='spmgsnipreview'}" />

                                    <a rel="nofollow" href="javascript:void(0)" id="clear-search-storereviews" class="clear-search-items display-none"
                                       onclick="go_page_spmgsnipreviewti(0,'userstorereviews','','',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})">
                                        {l s='Clear search' mod='spmgsnipreview'}
                                    </a>


                            </fieldset>
                        </form>


                    </div>

                </div>




                <div class="row-custom filter-testimonials {if $spmgsnipreviewis16 == 0}filter-testimonials-14{/if}">

                    <div class="col-sm-1-custom">
                        <b class="filter-txt-items-block">{l s='Filter' mod='spmgsnipreview'}:</b>
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 5}active-items-block{/if}">
                        {if $spmgsnipreviewfiveti>0}
                        <a rel="nofollow"

                           rel="nofollow"
                           onclick="go_page_spmgsnipreviewti(0,'userstorereviews',5,'{if $spmgsnipreviewis_searchti == 1}1{/if}',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="storereviews-rating-5"

                                >
                            {/if}
                            {section name="test" loop=5}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            <span class="count-items-block {if $spmgsnipreviewfiveti==0}text-decoration-none{/if}">({$spmgsnipreviewfiveti|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewfiveti>0}
                        </a>
                        {/if}
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 4}active-items-block{/if}">
                        {if $spmgsnipreviewfourti>0}
                        <a rel="nofollow"
                           onclick="go_page_spmgsnipreviewti(0,'userstorereviews',4,'{if $spmgsnipreviewis_searchti == 1}1{/if}',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="storereviews-rating-4"
                                >
                            {/if}
                            {section name="test" loop=4}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            {section name="test" loop=1}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}

                            <span class="count-items-block {if $spmgsnipreviewfourti==0}text-decoration-none{/if}">({$spmgsnipreviewfourti|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewfourti>0}
                        </a>
                        {/if}
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 3}active-items-block{/if}">
                        {if $spmgsnipreviewthreeti>0}
                        <a rel="nofollow"
                           onclick="go_page_spmgsnipreviewti(0,'userstorereviews',3,'{if $spmgsnipreviewis_searchti == 1}1{/if}',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="storereviews-rating-3"
                                >
                            {/if}
                            {section name="test" loop=3}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}
                            {section name="test" loop=2}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            <span class="count-items-block {if $spmgsnipreviewthreeti==0}text-decoration-none{/if}">({$spmgsnipreviewthreeti|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewthreeti>0}
                        </a>
                        {/if}
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 2}active-items-block{/if}">
                        {if $spmgsnipreviewtwoti>0}
                        <a rel="nofollow"
                           onclick="go_page_spmgsnipreviewti(0,'userstorereviews',2,'{if $spmgsnipreviewis_searchti == 1}1{/if}',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="storereviews-rating-2"
                                >
                            {/if}
                            {section name="test" loop=2}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}
                            {section name="test" loop=3}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}

                            <span class="count-items-block {if $spmgsnipreviewtwoti==0}text-decoration-none{/if}">({$spmgsnipreviewtwoti|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewtwoti>0}
                        </a>
                        {/if}
                    </div>
                    <div class="col-sm-2-custom {if isset($spmgsnipreviewfratti) && $spmgsnipreviewfratti == 1}active-items-block{/if}">
                        {if $spmgsnipreviewoneti>0}
                        <a rel="nofollow"

                           onclick="go_page_spmgsnipreviewti(0,'userstorereviews',1,'{if $spmgsnipreviewis_searchti == 1}1{/if}',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                           href="javascript:void(0)"
                           id="storereviews-rating-1"
                                >
                            {/if}
                            {section name="test" loop=1}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}
                            {section name="test" loop=4}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                            {/section}
                            <span class="count-items-block {if $spmgsnipreviewoneti==0}text-decoration-none{/if}">({$spmgsnipreviewoneti|escape:'htmlall':'UTF-8'})</span>
                            {if $spmgsnipreviewoneti>0}
                        </a>
                        {/if}
                    </div>


                        <div class="col-sm-1-custom">
                            <a rel="nofollow" href="javascript:void(0)"
                               onclick="go_page_spmgsnipreviewti(0,'userstorereviews','','',{$spmgsnipreviewuser_id|escape:'htmlall':'UTF-8'})"
                               id="clear-rating-storereviews" class="reset-items-block display-none">
                                <i class="fa fa-refresh"></i>{l s='Reset' mod='spmgsnipreview'}
                            </a>
                        </div>



                </div>

                {if $spmgsnipreviewis_searchti == 1}
                    <h3 class="search-result-item">{l s='Results for' mod='spmgsnipreview'} <b>"{$spmgsnipreviewsearchti|escape:'quotes':'UTF-8'}"</b></h3>
                    <br/>
                {/if}


            {if $count_all_reviewsti>0 && $spmgsnipreviewis_sortfuti == 1}
                <div class="search-result-item float-right">

                    <b>
                        {l s='Sort by' mod='spmgsnipreview'}:
                    </b>
                    <select id="select_spmgsnipreview_sortti" data-actionti="userstorereviews">
                        <option value="date_add:desc" selected="selected">--</option>
                        <option value="rating:asc">Rating: Lowest first</option>
                        <option value="rating:desc">Rating: Highest first</option>
                        <option value="date_add:asc">Date: Oldest first</option>
                        <option value="date_add:desc">Date: Newest first</option>
                    </select>

                </div>
                <div class="clear"></div>
                <br/>
            {/if}


            {if $count_all_reviewsti > 0}
                <div id="storereviews-list">

                    {assign var="list_storereviews" value="modules/spmgsnipreview/views/templates/front/list_storereviews.tpl"}

                    {if file_exists("{$tpl_dir}{$list_storereviews}")}
                        {include file="{$tpl_dir}{$list_storereviews}"}
                    {else}
                        {include file="{$list_storereviews}"}
                    {/if}

                </div>


                <div class="text-align-center"  id="page_nav">
                    {$spmgsnipreviewpagingti nofilter}

                </div>

            {else}
                <div class="testimonials-no-items">
                    {l s='There are not store reviews yet' mod='spmgsnipreview'}
                </div>
            {/if}



        </div>

    {/if}

</div>

</div>

{/foreach}


{literal}
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
    $( "#tabs-custom ul li a" ).click(function() {

        var tab_custom_spmgsnipreview = $(this).attr('data').slice($(this).attr('data').indexOf('#') + 1);


        $.each($('#tabs-custom ul li'), function(key, val) {

            $(this).removeClass("current");

            if($(this).children(0).attr('data') == tab_custom_spmgsnipreview){
                $(this).addClass("current");
            }


        });


        $.each($('#tabs-custom-content div.b-tab-wrapper'), function(key, val) {
            $(this).removeClass("current-tab-content");
            $(this).removeClass("current-tab-content-hide");

            if($(this).attr('id').slice($(this).attr('id').indexOf('#') + 1) == tab_custom_spmgsnipreview) {
                $(this).addClass("current-tab-content");
            } else {
                $(this).addClass("current-tab-content-hide");
            }

        });

        {/literal}{if $spmgsnipreviewd_eff_rev != "disable_all_effects"}{literal}
        //spmgsnipreview_init_effects_review();
        {/literal}{/if}{literal}

        {/literal}{if $spmgsnipreviewd_eff_shopti != "disable_all_effects"}{literal}
        //spmgsnipreview_init_effects();
        {/literal}{/if}{literal}




    });

    });


    {/literal}{if $spmgsnipreviewis_search == 1 || $spmgsnipreviewfrat > 0 || $spmgsnipreviewisgp}{literal}
    document.addEventListener("DOMContentLoaded", function(event) {
    $(document).ready(function(){

        $.each($('#tabs-custom ul li'), function(key, val) {
            $(this).removeClass("current");
        });

        $('#tabs-custom ul li a[data="spmgsnipreviewreviews"]').parent().addClass("current");


        $.each($('#tabs-custom-content div.b-tab-wrapper'), function(key, val) {
            $(this).removeClass("current-tab-content");
            $(this).addClass("current-tab-content-hide");
        });

        $('#tabs-custom-content #spmgsnipreviewreviews').removeClass("current-tab-content-hide");
        $('#tabs-custom-content #spmgsnipreviewreviews').addClass("current-tab-content");


    });
    });
    {/literal}{/if}{literal}


    {/literal}{if $spmgsnipreviewis_searchti == 1 || $spmgsnipreviewfratti > 0 || $spmgsnipreviewispti}{literal}
    document.addEventListener("DOMContentLoaded", function(event) {
    $(document).ready(function(){

        $.each($('#tabs-custom ul li'), function(key, val) {
            $(this).removeClass("current");
        });

        $('#tabs-custom ul li a[data="spmgsnipreviewstorereviews"]').parent().addClass("current");


        $.each($('#tabs-custom-content div.b-tab-wrapper'), function(key, val) {
            $(this).removeClass("current-tab-content");
            $(this).addClass("current-tab-content-hide");
        });

        $('#tabs-custom-content #spmgsnipreviewstorereviews').removeClass("current-tab-content-hide");
        $('#tabs-custom-content #spmgsnipreviewstorereviews').addClass("current-tab-content");


    });
    });
    {/literal}{/if}{literal}

</script>
{/literal}
