{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewptabs_type == 2 || $spmgsnipreviewptabs_type == 3}

{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}

<li {if $spmgsnipreviewptabs_type == 3}class="nav-item"{/if}>

<a id="idTab777-my" href="#idTab777" data-toggle="tab"{if $spmgsnipreviewptabs_type == 3}class="nav-link"{/if}
        ><img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}"
                                            class="{if $spmgsnipreviewis16 == 0}fix-width-ps15{/if} title-rating-one-star"
                                           alt="{l s='Reviews' mod='spmgsnipreview'}" />&nbsp;{l s='Reviews' mod='spmgsnipreview'} <span id="count-review-tab">({$spmgsnipreviewcount_reviews|escape:'html':'UTF-8'})</span></a>

</li>

{/if}

{/if}

{/if}
