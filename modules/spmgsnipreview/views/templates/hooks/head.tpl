{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewis_r_p != 0}
    <meta property="og:title" content="{$spmgsnipreviewname|escape:'htmlall':'UTF-8'}"/>
    <meta property="og:image" content="{$spmgsnipreviewimg|escape:'htmlall':'UTF-8'}"/>
    <meta property="og:description" content="{$spmgsnipreviewdescr|escape:'htmlall':'UTF-8'}" />
    <meta property="og:url" content="{$spmgsnipreviewreview_url|escape:'htmlall':'UTF-8'}"/>
    <meta property="og:type" content="product"/>
{/if}

{if $spmgsnipreviewpinvis_on == 1 && $spmgsnipreviewis_product_page != 0}

<meta property="og:title" content="{$product_name|escape:'htmlall':'UTF-8'}" />
<meta property="og:description" content="{$spmgsnipreviewpindesc|escape:'htmlall':'UTF-8'}" />
<meta property="og:type" content="product" />
<meta property="og:url" content="{if $spmgsnipreviewis_ssl == 1}https{else}http{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}" />
<meta property="og:site_name" content="{$shop_name|escape:'htmlall':'UTF-8'}" />
<meta property="og:price:amount" content="{$product_price_custom|escape:'htmlall':'UTF-8'}" />
<meta property="og:price:currency" content="{$currency_custom|escape:'htmlall':'UTF-8'}" />
<meta property="og:availability" content="{if $stock_string=='in_stock'}instock{else}{$stock_string|escape:'htmlall':'UTF-8'}{/if}" />

{/if}

{literal}
<script type="text/javascript">
    var is_mobile_spmgsnipreview = '{/literal}{$spmgsnipreviewis_mobile nofilter}{literal}';
</script>
{/literal}

{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewrsson == 1}
<link rel="alternate" type="application/rss+xml" href="{$spmgsnipreviewrss_url nofilter}" />
{/if}

{literal}
    <script type="text/javascript">

        {/literal}{if $spmgsnipreviewis17 == 1}{literal}
            var baseDir = '{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}';
        {/literal}{/if}{literal}

        var spmgsnipreview_is_rewrite = '{/literal}{$spmgsnipreviewis_rewrite nofilter}{literal}';

        var ajax_productreviews_url_spmgsnipreview = '{/literal}{$spmgsnipreviewreviews_url nofilter}{literal}';

    </script>
{/literal}


{if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}


{literal}
<style type="text/css">
.page-item.active .page-link, .page-item.active .page-link:focus, .page-item.active .page-link:hover
{
    background-color:{/literal}{$spmgsnipreviewstylecolor|escape:'htmlall':'UTF-8'}{literal};
    border-color:{/literal}{$spmgsnipreviewstylecolor|escape:'htmlall':'UTF-8'}{literal};
}
a.page-link:hover {
    background-color:{/literal}{$spmgsnipreviewstylecolor|escape:'htmlall':'UTF-8'}{literal}!important  ;
    color:#fff;
    border-color:{/literal}{$spmgsnipreviewstylecolor|escape:'htmlall':'UTF-8'}{literal};
}

    </style>
{/literal}
{/if}

{/if}

{if $spmgsnipreviewrsoc_on == 1}
<!-- facebook button -->
{literal}
    <script type="text/javascript" src="{/literal}{$spmgsnipreviewfbliburl|escape:'htmlall':'UTF-8'}{literal}"></script>
{/literal}
<!-- facebook button -->
{/if}







{if $spmgsnipreviewis_uprof == 1}

{literal}
    <script type="text/javascript">

        var ajax_users_url_spmgsnipreview = '{/literal}{$spmgsnipreviewajax_users_url nofilter}{literal}';

        {/literal}
{if $spmgsnipreviewis_show == 1}

    {if $spmgsnipreviewislogged != 0}{literal}
    document.addEventListener("DOMContentLoaded", function(event) {
        $('document').ready( function() {
            var count1 = Math.random();
            var ph =  '<img class="avatar-header-spmgsnipreview" '+
                    ' src="{/literal}{$spmgsnipreviewavatar_thumb|escape:'htmlall':'UTF-8'}?re=' + count1+'{literal}"'+
                    ' />';



            if($('#header_user_info span'))
                $('#header_user_info span:last').append(ph);

            // for PS 1.6 >
            if($('.header_user_info')){
                $('.header_user_info .account:last').append(ph);

            }

            // for ps 1.7 >
            if($('.user-info')){
                $('.user-info .account:last').append(ph);

            }


        });
    });
    {/literal}{/if}
{/if}
        {literal}
    </script>
{/literal}
{/if}





{if $spmgsnipreviewis_storerev == 1}



    {if $spmgsnipreviewrssontestim == 1}
        <link rel="alternate" type="application/rss+xml" href="{$spmgsnipreviewrss_testimonials_url nofilter}" />
    {/if}


{literal}
    <style type="text/css">
        .ps15-color-background{background-color:{/literal}{$spmgsnipreviewBGCOLOR_T|escape:'htmlall':'UTF-8'}{literal};}


        {/literal}{if $spmgsnipreviewt_leftside == 1 || $spmgsnipreviewt_rightside == 1}{literal}




        /* testimonials widget */
        {/literal}{if $spmgsnipreviewt_leftside == 1}{literal}


        div#spmgsnipreview-box.left_shopreviews .belt {

            border-radius: 5px;


        {/literal}{if $spmgsnipreviewis_mobile == 0}{literal}
            background-color: {/literal}{$spmgsnipreviewBGCOLOR_TIT|escape:'htmlall':'UTF-8'}{literal};
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
            color: white;
            font-size: 15px;
            padding: 5px;
            right: 67px;
            text-align: center;
            top: 68px;
            box-sizing:border-box;
            width: 171px;
            height: 33px;
        {/literal}{else}{literal}
            background: {/literal}{$spmgsnipreviewBGCOLOR_TIT|escape:'htmlall':'UTF-8'}{literal} url("{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/spmgsnipreview/views/img/t-left{/literal}{$spmgsnipreviewlang|escape:'htmlall':'UTF-8'}{literal}.png") repeat scroll 0 0;
            width: 33px;
            height: 151px;
        {/literal}{/if}{literal}
        }

        table.spmgsnipreview-widgets td.facebook_block{
            height: 151px;
        }

        {/literal}{/if}{literal}


        {/literal}{if $spmgsnipreviewt_rightside == 1}{literal}
        #spmgsnipreview-box.right_shopreviews .belt{

            border-radius: 5px;


        {/literal}{if $spmgsnipreviewis_mobile == 0}{literal}

            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            -o-transform: rotate(270deg);
            transform: rotate(270deg);
            background-color: {/literal}{$spmgsnipreviewBGCOLOR_TIT|escape:'htmlall':'UTF-8'}{literal};
            color: white;
            font-size: 15px;
            padding: 5px;
            right: -67px;
            text-align: center;
            top: 68px;
            box-sizing:border-box;
            width: 171px;
            height: 33px;

        {/literal}{else}{literal}
            background: {/literal}{$spmgsnipreviewBGCOLOR_TIT|escape:'htmlall':'UTF-8'}{literal} url("{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/spmgsnipreview/views/img/t-right{/literal}{$spmgsnipreviewlang|escape:'htmlall':'UTF-8'}{literal}.png") repeat scroll 0 0;
            width: 33px;
            height: 151px;
        {/literal}{/if}{literal}
        }

        table.spmgsnipreview-widgets td.facebook_block{
            height: 151px;
        }

        {/literal}{/if}{literal}



        {/literal}{if $spmgsnipreviewt_leftside == 1}{literal}
        .spmgsnipreview-widgets .left_shopreviews{
            right: auto;

        }
        {/literal}{/if}{if $spmgsnipreviewt_rightside == 1}{literal}
        .spmgsnipreview-widgets .right_shopreviews {
            {/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:111px;{/literal}{/if}{literal}
            right: -{/literal}{if $spmgsnipreviewis16 == 1}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{else}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 23}{/if}{literal}px;
        }
        {/literal}{/if}{literal}

        div#spmgsnipreview-box.left_shopreviews{
            {/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:111px;{/literal}{/if}{literal}
            left: -{/literal}{if $spmgsnipreviewis16 == 1}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{else}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 23}{/if}{literal}px;
        }
        #spmgsnipreview-box .outside{
        {/literal}{if $spmgsnipreviewt_leftside == 1}{literal}
            float: right;
        {/literal}{else}{literal}
            float: right;
        {/literal}{/if}{literal}
        }

        /* testimonials widget */

        {/literal}

        {/if}
        {literal}

    </style>
{/literal}





{literal}
    <script type="text/javascript">

        var ajax_storereviews_url_spmgsnipreview = '{/literal}{$spmgsnipreviewajax_url nofilter}{literal}';

        document.addEventListener("DOMContentLoaded", function(event) {
        $(document).ready(function() {


            {/literal}

            {if $spmgsnipreviewt_leftside == 1 || $spmgsnipreviewt_rightside == 1}{literal}
            /* testimonials widget */

            {/literal}{if $spmgsnipreviewt_leftside == 1}{literal}

            {/literal}{if ($spmgsnipreviewis_mobile == 1 && $spmgsnipreviewmt_leftside == 1) || (!$spmgsnipreviewis_mobile == 1 && $spmgsnipreviewst_leftside == 1)}{literal}

            $(".spmgsnipreview-widgets .left_shopreviews .outside").hover(
                    function () {
                        $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({{/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:'auto',{/literal}{/if}{literal}left:'0px'}, 500);
                    },
                    function () {
                        $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({{/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:'111px',{/literal}{/if}{literal}left:'-{/literal}{if $spmgsnipreviewis16 == 1}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{else}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 23}{/if}{literal}px'}, 500);

                    }
            );
            $(".spmgsnipreview-widgets .left_shopreviews .belt").hover(

                    function () {
                        $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({{/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:'auto',{/literal}{/if}{literal}left:'0px'}, 500);
                    },
                    function () {

                        $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({{/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:'111px',{/literal}{/if}{literal}left:'-{/literal}{if $spmgsnipreviewis16 == 1}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{else}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 23}{/if}{literal}px'}, 500);

                    }
            );


            {/literal}{if $spmgsnipreviewis_mobile == 1}{literal}


            $( ".spmgsnipreview-widgets .left_shopreviews .belt" ).click(function() {
                var style_belt = $(".spmgsnipreview-widgets .left_shopreviews").attr("style");

                if(style_belt == "left: 0px; height: 111px; overflow: hidden;"
                        ||
                        style_belt == "left: 0px; height: 111px;"){
                    $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({left:'-{/literal}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{literal}px'}, 500);

                }

                if(style_belt == "left: -{/literal}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{literal}px; height: 111px; overflow: hidden;"
                        ||
                        style_belt == "left: -{/literal}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{literal}px; height: 111px;"
                ){
                    $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({left:'0px'}, 500);

                }
            });

            {/literal}{/if}{literal}





            {/literal}{/if}{literal}

            {/literal}{/if}{literal}

            {/literal}{if $spmgsnipreviewt_rightside == 1}{literal}

            {/literal}{if ($spmgsnipreviewis_mobile == 1 && $spmgsnipreviewmt_rightside == 1) || (!$spmgsnipreviewis_mobile == 1 && $spmgsnipreviewst_rightside == 1)}{literal}

            $(".spmgsnipreview-widgets .right_shopreviews .outside").hover(
                    function () {
                        $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({{/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:'auto',{/literal}{/if}{literal}right:'0px'}, 500);
                    },
                    function () {
                        $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({{/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:'111px',{/literal}{/if}{literal}right:'-{/literal}{if $spmgsnipreviewis16 == 1}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{else}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 23}{/if}{literal}px'}, 500);

                    }
            );
            $(".spmgsnipreview-widgets .right_shopreviews .belt").hover(
                    function () {
                        $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({{/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:'auto',{/literal}{/if}{literal}right:'0px'}, 500);
                    },
                    function () {
                        $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({{/literal}{if $spmgsnipreviewis_mobile  == 1}{literal}height:'111px',{/literal}{/if}{literal}right:'-{/literal}{if $spmgsnipreviewis16 == 1}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{else}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 23}{/if}{literal}px'}, 500);

                    }
            );



            {/literal}{if $spmgsnipreviewis_mobile == 1}{literal}

            $( ".spmgsnipreview-widgets .right_shopreviews .belt" ).click(function() {
                var style_belt = $(".spmgsnipreview-widgets .right_shopreviews").attr("style");

                if(style_belt == "right: 0px; height: 111px; overflow: hidden;"
                        ||
                        style_belt == "right: 0px; height: 111px;"){
                    $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({right:'-{/literal}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{literal}px'}, 500);

                }

                if(style_belt == "right: -{/literal}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{literal}px; height: 111px; overflow: hidden;"
                        ||
                        style_belt == "right: -{/literal}{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8' + 5}{literal}px; height: 111px;"
                ){
                    $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({right:'0px'}, 500);

                }
            });

            {/literal}{/if}{literal}

            



            {/literal}{/if}{literal}

            {/literal}{/if}{literal}

            /* testimonials widget */
            {/literal}{/if}{literal}




        });
        });
    </script>
{/literal}

{/if}