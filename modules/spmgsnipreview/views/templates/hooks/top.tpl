{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewallinfo_on == 1 && $spmgsnipreviewsvis_on == 1 && ($spmgsnipreviewis_home_b_top == 1 || $spmgsnipreviewis_cat_b_top == 1 || $spmgsnipreviewis_man_b_top == 1)}

{if count($spmgsnipreviewdata_badges)>0}


    <div class="clear-spmgsnipreview"></div>
    <div class="badges" style="width:{$spmgsnipreviewallinfoh_w|escape:'htmlall':'UTF-8'}%">

        <strong class="title-badges {if $spmgsnipreviewis_rtl == 1}badges-rtl{/if}">{l s='Review(s) and rating(s)' mod='spmgsnipreview'}</strong>
		<span itemscope itemtype="http://schema.org/Product" {if $spmgsnipreviewis_rtl == 1}class="badges-rtl"{/if}>
			<meta content="{$spmgsnipreviewbadges_name|escape:'htmlall':'UTF-8'}" itemprop="name">
            <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<span>

                {assign var='test_rating' value=$spmgsnipreviewdata_badges.total_rating|replace:',':'.'}
                {section name=ratid loop=5}

                    {if $smarty.section.ratid.index <= $spmgsnipreviewdata_badges.total_rating}

                        {if $test_rating <= $spmgsnipreviewmax_star_par && $test_rating >= $spmgsnipreviewmin_star_par}

                            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                        {else}
                            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                        {/if}
                    {else}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                    {/if}
                    {$test_rating = $test_rating - 1}

                {/section}
			</span>

			<meta content="1" itemprop="worstRating" />
			(<span itemprop="ratingValue">{$spmgsnipreviewdata_badges.total_rating|escape:'htmlall':'UTF-8'}</span>/<span itemprop="bestRating">5</span>)


			<strong>{if $spmgsnipreviewis_home_b_top != 0}{l s='Shop' mod='spmgsnipreview'}{/if}
                    {if $spmgsnipreviewis_cat_b_top != 0}{l s='Category' mod='spmgsnipreview'}{/if}
                    {if $spmgsnipreviewis_man_b_top != 0}{l s='Brand' mod='spmgsnipreview'}{/if} :</strong>
            <span itemprop="itemReviewed">{$spmgsnipreviewbadges_name|escape:'htmlall':'UTF-8'}</span> -
            {l s='Based on' mod='spmgsnipreview'} <span itemprop="ratingCount">{$spmgsnipreviewdata_badges.total_reviews|escape:'htmlall':'UTF-8'}</span> {l s='rating(s)' mod='spmgsnipreview'}
            {l s='and' mod='spmgsnipreview'} <span itemprop="reviewCount">{$spmgsnipreviewdata_badges.total_reviews|escape:'htmlall':'UTF-8'}</span> {l s='review(s)' mod='spmgsnipreview'}
		</span>
            </span>

        &nbsp;<span {if $spmgsnipreviewis_rtl == 1}class="badges-rtl"{/if}>-</span>&nbsp;<a href="{$spmgsnipreviewrev_all|escape:'htmlall':'UTF-8'}" {if $spmgsnipreviewis_rtl == 1}class="badges-rtl"{/if}
                          title="{l s='View All Reviews' mod='spmgsnipreview'}"><span>{l s='View All Reviews' mod='spmgsnipreview'}</span></a>


    </div>

{/if}

{/if}

{/if}


{*
<div class="score">8<span>,7</span></div>
<div class="starholder">
    <div class="stars" style="width:87%;">
    </div>
</div>
<div class="klantenvertellen">
    klanten <strong>vertellen</strong>
</div>
<div class="beoordelingen">
    <span class="number">522</span>
    Beoordelingen
</div>
<a rel="nofollow" href="https://klantenvertellen.nl/enquete/voetbalreizenxl" class="btn" target="_blank">Zelf beoordelen</a>
<a rel="nofollow" href="https://klantenvertellen.nl/referenties/voetbalreizenxl/" class="btn bekijk" target="_blank">Bekijk beoordelingen</a>
<a rel="nofollow" href="https://klantenvertellen.nl/referenties/voetbalreizenxl/" class="fullbtn" target="_blank"></a>
{literal}
<style>@font-face {font-family:'Avenir-Book';src:url('/widgets/fonts/Avenir-Book.eot');src:url('/widgets/fonts/Avenir-Book.eot?#iefix') format('embedded-opentype'), url('/widgets/fonts/Avenir-Book.woff') format('woff'), url('/widgets/fonts/Avenir-Book.ttf') format('truetype'), url('/widgets/fonts/Avenir-Book.svg#Avenir-Book') format('svg');font-weight:normal;font-style:normal;}html {box-sizing:border-box;}*, *:before, *:after {box-sizing:inherit;}html, body {margin:0px;padding:0px;width:100%;height:100%;}body {font-family:'Avenir-Book', sans-serif;background-color:#fff;border:1px solid #494545;}.score {width:62px;height:62px;text-align:center;background-image:url(/widgets/images/logo-big.png);background-size:cover;font-size:31px;font-weight:700;padding-top:10px;color:#444444;position:absolute;top:7px;left:2%;}.score span {font-size:.5em;font-weight:300;position:relative;top:-.6em;}.starholder {background-image:url(/widgets/images/stars-big-inactive.png);width:90px;height:15px;position:absolute;top:5px;left:25%;}.stars {background-image:url(/widgets/images/stars-big.png);height:15px;}.klantenvertellen {color:#444444;font-size:14px;font-weight:300;position:absolute;left:60%;}.klantenvertellen strong {font-weight:700;}.beoordelingen {font-size:12px;line-height:1.6em;text-align:center;color:#444444;font-weight:300;position:absolute;top:30px;left:25%;width:90px;}.beoordelingen .number {font-size:31px;display:block;}.btn {width:113px;font-size:10px;color:#fff;font-weight:300;background-color:#8ab327;border-radius:3px;border:1px solid #789d1e;text-decoration:none;padding:4px 0px 4px 9px;display:block;position:absolute;top:20px;left:60%;}.btn.bekijk {background-color:#2dc0ff;border-color:#32b5f0;top:45px;}.btn:hover {background-color:#789d1e;}.btn.bekijk:hover {background-color:#32b5f0;}.fullbtn {display:none;}@media all and (min-width:199px) and (max-width:299px) {.beoordelingen {font-size:9px;line-height:1em;left:39%;width:113px;text-align:left;top:25px;}.beoordelingen .number {font-size:9px;display:inline;}.btn {top:40px;left:39%;}.btn.bekijk { top:65px;}.klantenvertellen {left:39%;top:5px;}.starholder {background-image:url(/widgets/images/stars-small-inactive.png);width:60px;left:2%;top:75px;height:10px;}.stars {background-image:url(/widgets/images/stars-small.png);height:10px;}}@media all and (max-width:199px) {.score {width:99%;height:84px;text-align:center;background-image:url(/widgets/images/logo-small.png);background-repeat:no-repeat;background-position:center;background-size:contain;background-size:auto;font-size:42px;font-weight:700;padding-top:15px;color:#444444;position:absolute;top:auto;left:auto;margin:5px auto;}.score span {font-size:.5em;font-weight:300;position:relative;top:-.6em;}.btn, .beoordelingen, .starholder {display:none;}.klantenvertellen {position:relative;width:97px;margin:0 auto;left:auto;top:auto;margin-top:95px;font-size:18px;line-height:1em;text-align:center;}.fullbtn {display:block;position:absolute;top:0px;right:0px;bottom:0px;left:0px;}}@media all and (max-width:100px) {.klantenvertellen {width:100%;}}@media (-webkit-min-device-pixel-ratio:1.25), (min-resolution:120dpi){ .score {background-image:url(/widgets/images/logo-small@2.png);background-size:contain;}.starholder {background-image:url(/widgets/images/stars-big-inactive@2.png);background-size:cover;}.stars {background-image:url(/widgets/images/stars-big@2.png);background-size:cover;}}@media screen and (min-width:132px) and (max-width:132px) and (max-height:81px) {.score {width:62px;height:62px;text-align:center;background-image:url(/widgets/images/logo-big.png);background-size:cover;font-size:31px;font-weight:700;padding-top:10px;color:#444444;position:relative;top:10px;margin:0 auto;}}</style>
{/literal}
*}

{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}


{if $spmgsnipreviewis_blocklr == 1 && ($spmgsnipreviewis_home_top == 1 || $spmgsnipreviewis_cat_top == 1 || $spmgsnipreviewis_man_top == 1 || $spmgsnipreviewis_prod_top == 1 || $spmgsnipreviewis_oth_top == 1)}





<div class="clear-spmgsnipreview"></div>
<div id="spmgsnipreview_block_left" class="block-last-spmgsnipreviews block blockmanufacturer" style="width:{$spmgsnipreviewblocklr_w|escape:'htmlall':'UTF-8'}%">

    	<h4 class="title_block">
			<div class="spmgsnipreviews-float-left">
			{l s='Last Product Reviews' mod='spmgsnipreview'}
			</div>
			<div class="spmgsnipreviews-float-left margin-left-5">
			{if $spmgsnipreviewrsson == 1}
				<a href="{$spmgsnipreviewrss_url nofilter}" target="_blank" title="{l s='RSS Feed' mod='spmgsnipreview'}">
					<img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/feed.png" alt="{l s='RSS Feed' mod='spmgsnipreview'}" />
				</a>
			{/if}
			</div>
			<div class="spmgsnipreviews-clear"></div>
		</h4>
		<div class="block_content block-items-data row">
			{if count($spmgsnipreviewreviews_block)>0}


			{foreach from=$spmgsnipreviewreviews_block item=review name=myLoop}

                <div class="items-last-spmgsnipreviews ">
                {if $review.product_img}
                    <div class="img-block-spmgsnipreview col-sm-1">
                        <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                           title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                >
                            <img src="{$review.product_img|escape:'htmlall':'UTF-8'}" title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                 alt = "{$review.product_name|escape:'htmlall':'UTF-8'}" class="border-image-review img-responsive" />
                        </a>
                    </div>
                {/if}
                <div class="body-block-spmgsnipreview col-sm-11 {if !$review.product_img}body-block-spmgsnipreview-100{/if}">
                    <div class="title-block-last-spmgsnipreview">


                    <div class="r-product">
                        {l s='By' mod='spmgsnipreview'}
                        {if $spmgsnipreviewis_avatarr == 1 && strlen($review.avatar)>0 && $review.is_show_ava == 1}

                            <span class="avatar-block-rev">
                                        <img alt="{$review.customer_name|escape:'htmlall':'UTF-8'}"
                                             src="{$review.avatar|escape:'htmlall':'UTF-8'}">
                            </span>

                        {/if}

                        {if strlen($review.customer_name)>0}
                            {if $spmgsnipreviewis_uprof && $review.id_customer > 0 && $review.is_show_ava == 1}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.customer_name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}<strong
                                    >{$review.customer_name|escape:'htmlall':'UTF-8'}</strong>{if $spmgsnipreviewis_uprof && $review.id_customer > 0 && $review.is_show_ava == 1}</a>{/if}
                        {/if}
                        {if strlen($review.customer_name)>0}{l s='on' mod='spmgsnipreview'}{/if}&nbsp;<strong>{$review.time_add|date_format|escape:'htmlall':'UTF-8'}</strong>


                    </div>

                    {if $review.is_active == 1}
                    {if $spmgsnipreviewratings_on == 1 && $review.rating != 0}
                    <div  class="rating-stars-total-block">
                       ({$review.rating|escape:'htmlall':'UTF-8'}/5)
                    </div>
                    <div class="r-rating">
                                {section name=ratid loop=5}
                                    {if $smarty.section.ratid.index < $review.rating}
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                    {else}
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                    {/if}

                                {/section}
                    </div>
                    {/if}
                     {/if}
                        <div class="clear-spmgsnipreview"></div>
                        {if $review.is_buy != 0}
                            <span class="spmgsnipreview-block-date float-left">
                                            <span class="is_buy_product is_buy_product_block">{l s='Verified Purchase' mod='spmgsnipreview'}</span>
                                        </span>
                            <div class="clear-spmgsnipreview"></div>
                        {/if}
                    </div>

                    <div class="title-block-r">
                        <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                           title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                >
                            {$review.product_name|escape:'htmlall':'UTF-8'}
                        </a>
                    </div>


                    {if $review.is_active == 1}

                    {if $spmgsnipreviewtext_on == 1 && strlen($review.text_review)>0}
                                <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                                   title="{$review.text_review|escape:'quotes':'UTF-8':strip_tags|substr:0:$spmgsnipreviewblocklr_tr|escape:'htmlall':'UTF-8'}"
                                   >
                                    {$review.text_review|escape:'quotes':'UTF-8':strip_tags|substr:0:$spmgsnipreviewblocklr_tr|escape:'htmlall':'UTF-8'}{if strlen($review.text_review)>$spmgsnipreviewblocklr_tr}...{/if}
                                </a>
                    {/if}

                    {else}

                        {l s='The customer has rated the product but has not posted a review, or the review is pending moderation' mod='spmgsnipreview'}
	    			{/if}

                </div>
                    <div class="clear-spmgsnipreview"></div>
                </div>
	    	{/foreach}
                <div class="gsniprev-view-all float-right">
                    <a href="{$spmgsnipreviewallr_url|escape:'htmlall':'UTF-8'}"
                       class="btn btn-default button button-small-spmgsnipreview"
                            >
                        <span>{l s='View All Reviews' mod='spmgsnipreview'}</span>
                    </a>
                </div>
                <div class="clear-spmgsnipreview"></div>

	    	{else}
	    		<div class="gsniprev-block-noreviews">
					{l s='There are not Product Reviews yet.' mod='spmgsnipreview'}
				</div>
	    	{/if}
	    </div>
</div>




{/if}

{/if}

{/if}
