{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}

{if $spmgsnipreviewis_blocklr == 1 && $spmgsnipreviewblocklr_chook == "blocklr_chook"}





<div class="clear-spmgsnipreview"></div>
<div id="spmgsnipreview_block_left" class="block-last-spmgsnipreviews block blockmanufacturer" style="width:{$spmgsnipreviewblocklr_w|escape:'htmlall':'UTF-8'}%">

    	<h4 class="title_block">
			<div class="spmgsnipreviews-float-left">
			{l s='Last Product Reviews' mod='spmgsnipreview'}
			</div>
			<div class="spmgsnipreviews-float-left margin-left-5">
			{if $spmgsnipreviewrsson == 1}
				<a href="{$spmgsnipreviewrss_url nofilter}" target="_blank" title="{l s='RSS Feed' mod='spmgsnipreview'}">
					<img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/feed.png" alt="{l s='RSS Feed' mod='spmgsnipreview'}" />
				</a>
			{/if}
			</div>
			<div class="spmgsnipreviews-clear"></div>
		</h4>
		<div class="block_content block-items-data row-custom">
			{if count($spmgsnipreviewreviews_block_chook)>0}


			{foreach from=$spmgsnipreviewreviews_block_chook item=review name=myLoop}

                <div class="items-last-spmgsnipreviews ">
                {if $review.product_img}
                    <div class="img-block-spmgsnipreview col-sm-3-custom">
                        <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                           title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                >
                            <img src="{$review.product_img|escape:'htmlall':'UTF-8'}" title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                 alt = "{$review.product_name|escape:'htmlall':'UTF-8'}" class="border-image-review img-responsive" />
                        </a>
                    </div>
                {/if}
                <div class="body-block-spmgsnipreview col-sm-8-custom {if !$review.product_img}body-block-spmgsnipreview-100{/if}">
                    <div class="title-block-last-spmgsnipreview">


                    <div class="r-product">
                        {if strlen($review.customer_name)>0}
                            {l s='By' mod='spmgsnipreview'}&nbsp;<strong>{$review.customer_name|escape:'html':'UTF-8'}</strong>
                        {/if}
                        {if strlen($review.customer_name)>0}{l s='on' mod='spmgsnipreview'}{/if}&nbsp;<strong>{$review.time_add|date_format|escape:'html':'UTF-8'}</strong>


                    </div>

                        {if $review.is_active == 1}
                    {if $spmgsnipreviewratings_on == 1 && $review.rating != 0}
                    <div  class="rating-stars-total-block">
                       ({$review.rating|escape:'html':'UTF-8'}/5)
                    </div>
                    <div class="r-rating">
                                {section name=ratid loop=5}
                                    {if $smarty.section.ratid.index < $review.rating}
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                    {else}
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                    {/if}

                                {/section}
                    </div>
                    {/if}
                        {/if}
                        <div class="clear-spmgsnipreview"></div>
                    </div>

                    <div class="title-block-r">
                        <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                           title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                >
                            {$review.product_name|escape:'htmlall':'UTF-8'}
                        </a>
                    </div>


                    {if $review.is_active == 1}

                    {if $spmgsnipreviewtext_on == 1 && strlen($review.text_review)>0}

                                    {$review.text_review|escape:'quotes':'UTF-8':strip_tags|substr:0:$spmgsnipreviewblocklr_tr|escape:'htmlall':'UTF-8'}{if strlen($review.text_review)>$spmgsnipreviewblocklr_tr}...{/if}

                    {/if}

                    {else}

                        {l s='The customer has rated the product but has not posted a review, or the review is pending moderation' mod='spmgsnipreview'}
	    			{/if}

                </div>
                    <div class="clear-spmgsnipreview"></div>
                </div>
	    	{/foreach}
                <div class="gsniprev-view-all float-right">
                    <a href="{$spmgsnipreviewallr_url|escape:'html':'UTF-8'}"
                       class="btn btn-default button button-small-spmgsnipreview"
                            >
                        <span>{l s='View All Reviews' mod='spmgsnipreview'}</span>
                    </a>
                </div>
                <div class="clear-spmgsnipreview"></div>

	    	{else}
	    		<div class="gsniprev-block-noreviews">
					{l s='There are not Product Reviews yet.' mod='spmgsnipreview'}
				</div>
	    	{/if}
	    </div>
</div>




{/if}

{/if}

{/if}
