{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

<div>
     <div class="alert alert-success">
         {if $spmgsnipreviewis17 == 1}
             {$spmgsnipreviewmsg nofilter}
         {else}
             {$spmgsnipreviewmsg nofilter}
         {/if}
        </div>
</div>