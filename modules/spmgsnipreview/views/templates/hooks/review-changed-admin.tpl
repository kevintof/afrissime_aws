{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

<div id="add-review-form-review" class="popup-form font-family-custom">


    <div class="title-rev">
        <div class="title-form-text-left">
            <b class="title-form-custom">{l s='Review' mod='spmgsnipreview'}:</b>

        </div>

        <div class="clear-spmgsnipreview"></div>
    </div>

    <div id="body-add-review-form-review" class="text-align-left">
        <span class="label-span">{l s='Review URL' mod='spmgsnipreview'}:</span>

        <span class="badge">
            <a href="{$spmgsnipreviewdatareview.review_url|escape:'htmlall':'UTF-8'}" target="_blank">{$spmgsnipreviewdatareview.review_url|escape:'htmlall':'UTF-8'}</a>
        </span>
        <div class="clear-spmgsnipreview"></div>

        <span class="label-span">{l s='Review Language' mod='spmgsnipreview'}:</span>

        <span class="badge">
            {$spmgsnipreviewdatareview.name_lang|escape:'htmlall':'UTF-8'}
        </span>
        <div class="clear-spmgsnipreview"></div>


        {if $spmgsnipreviewdatareview.id_customer == 0}

            <label for="name-abuse" >{l s='Guest Name' mod='spmgsnipreview'}:</label>
            <input type="text" name="name-abuse" id="name-abuse" class="form-control disabled-values" disabled value="{$spmgsnipreviewdatareview.customer_name|escape:'htmlall':'UTF-8'}" />
        {if $spmgsnipreviewdatareview.email}
            <label for="email-abuse" >{l s='Guest Email' mod='spmgsnipreview'}:</label>
            <input type="text" name="email-abuse" id="email-abuse" class="form-control disabled-values" disabled  value="{$spmgsnipreviewdatareview.email|escape:'htmlall':'UTF-8'}" />

         {/if}

            <div class="clear-spmgsnipreview"></div>
        {else}
            <span class="label-span">{l s='Customer' mod='spmgsnipreview'}:</span>
            <span class="badge">
            <a href="{$spmgsnipreviewdatareview.url_to_customer|escape:'htmlall':'UTF-8'}" target="_blank">{$spmgsnipreviewdatareview.customer_name_full|escape:'htmlall':'UTF-8'}</a>
        </span>
            <div class="clear-spmgsnipreview"></div>

        {/if}



        {if $spmgsnipreviewdatareview.criterions|@count>0}

                {foreach from=$spmgsnipreviewdatareview.criterions item=criterion}
                        <label for="rating-review" >{$criterion.name|escape:'htmlall':'UTF-8'}:</label>

                        <span class="stars-section">
                        {section name=ratid loop=5}
                            {if $smarty.section.ratid.index < $criterion.rating}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list1"
                                     alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                            {else}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list1"
                                     alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                            {/if}
                        {/section}
                        </span>
                    <div style="clear:both"></div>

                {/foreach}

            <br/>
         {else}
            <label for="rating-review" >{l s='Rating' mod='spmgsnipreview'}:</label>
            <span class="stars-section">
            {section name=ratid loop=5}
                {if $smarty.section.ratid.index < $spmgsnipreviewdatareview.rating}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list1"
                         alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                {else}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list1"
                         alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                {/if}
            {/section}
             </span>
            <br/>
        {/if}
        <div style="clear: both;"></div>


        {if strlen($spmgsnipreviewdatareview.title_review)>0}
        <label for="subject-review" >{l s='Review Title' mod='spmgsnipreview'}:</label>
        <input disabled class="form-control disabled-values" id="disabledInput" type="text" value="{$spmgsnipreviewdatareview.title_review|escape:'htmlall':'UTF-8' nofilter}"  />
        {/if}

        {if strlen($spmgsnipreviewdatareview.text_review)>0}
        <label for="text-review" >{l s='Review Text' mod='spmgsnipreview'}:</label>
        <textarea disabled class="form-control disabled-values" id="disabledInput" cols="42" rows="7">{$spmgsnipreviewdatareview.text_review|escape:'htmlall':'UTF-8' nofilter}</textarea>
        {/if}
    </div>



    {if $spmgsnipreviewdatareview.is_changed == 2}
    <div id="body-add-review-form-review">

        <span class="badge">
            <a href="javascript:void(0)" onclick="$('.old-review-item').toggle();" >{l s='Click to see old review' mod='spmgsnipreview'}</a>
        </span>



    </div>
    <div id="body-add-review-form-review" class="text-align-left old-review-item" style="display: none">

        {if $spmgsnipreviewdatareview.criterions_old|@count>0}

            {foreach from=$spmgsnipreviewdatareview.criterions_old item=criterion}
                <label for="rating-review" >{$criterion.name|escape:'htmlall':'UTF-8'}:</label>

                <span class="stars-section">
                        {section name=ratid loop=5}
                            {if $smarty.section.ratid.index < $criterion.rating}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list"
                                     alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                            {else}
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list"
                                     alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                            {/if}
                        {/section}
                        </span>
                <br/>

            {/foreach}

            <br/>
        {else}
            <label for="rating-review" >{l s='Rating' mod='spmgsnipreview'}:</label>
            <span class="stars-section">
            {section name=ratid loop=5}
                {if $smarty.section.ratid.index < $spmgsnipreviewdatareview.rating}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list1"
                         alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                {else}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-list1"
                         alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                {/if}
            {/section}
             </span>
            <br/>
        {/if}



        {if strlen($spmgsnipreviewdatareview.title_review_old)>0}
            <label for="subject-review" >{l s='Review Title' mod='spmgsnipreview'}:</label>
            <input disabled class="form-control disabled-values" id="disabledInput" type="text" value="{$spmgsnipreviewdatareview.title_review_old|escape:'htmlall':'UTF-8'}"  />
        {/if}

        {if strlen($spmgsnipreviewdatareview.text_review_old)>0}
            <label for="text-review" >{l s='Review Text' mod='spmgsnipreview'}:</label>
            <textarea disabled class="form-control disabled-values" id="disabledInput" cols="42" rows="7">{$spmgsnipreviewdatareview.text_review_old|escape:'htmlall':'UTF-8'}</textarea>
        {/if}


    </div>
    {/if}


    {if $spmgsnipreviewdatareview.id_customer != 0}

    <div class="title-rev">
        <div class="title-form-text-left">
            <b class="title-form-custom">{l s='Your suggest for change the review' mod='spmgsnipreview'}:</b>
        </div>

        <div class="clear-spmgsnipreview"></div>
    </div>
    <div id="body-add-review-form-review" class="text-align-left">




        <label for="text-admin_response" >{l s='Your suggestion text' mod='spmgsnipreview'}:</label>
        <textarea id="text-admin_response" name="text-admin_response" cols="42" rows="7" class="form-control cursor-active"
                  onkeyup="check_inpReponseReview();" onblur="check_inpReponseReview();"  {*{if $spmgsnipreviewdatareview.is_changed == 2}disabled{/if}*}
                >{$spmgsnipreviewdatareview.suggest_text|escape:'htmlall':'UTF-8'}</textarea>
        <div id="error_text-admin_response" class="errorTxtAdd"></div>

        <label for="is_display_old" >{l s='Display your suggestion on product page' mod='spmgsnipreview'}:</label>
        <input type="checkbox"  class="spmgsnipreview-checkbox" {if $spmgsnipreviewdatareview.is_display_old == 1}checked="checked"{/if} value="1" id="is_display_old" name="is_display_old">


        

        {if $spmgsnipreviewdatareview.is_count_sending_suggestion != 0}
            <div style="clear: both;"></div>
        <label for="is_send_again" >{l s='Send your suggestion by email again' mod='spmgsnipreview'}:</label>
        <input type="checkbox"  class="spmgsnipreview-checkbox" value="1" id="is_send_again" name="is_send_again"/>

            <div class="tip-times">
                {l s='Your response has already been sent' mod='spmgsnipreview'} <strong>{$spmgsnipreviewdatareview.is_count_sending_suggestion|escape:'htmlall':'UTF-8'}</strong> {l s='time(s)' mod='spmgsnipreview'}<br>

            </div>
        {/if}


    </div>
    <div id="footer-add-review-form-review">
        <button onclick="update_chaged()"  value="{l s='Send suggestion to user change the review' mod='spmgsnipreview'}" class="btn btn-success">{l s='Send suggestion to user change the review' mod='spmgsnipreview'}</button>
    </div>

    {else}
        <div class="tip-times">
            {l s='You cannot suggest a guest the change own review. Only for registered customers' mod='spmgsnipreview'}
        </div>

    {/if}



    </div>


{if $spmgsnipreviewdatareview.id_customer != 0}
{literal}
<script type="text/javascript">

    function check_inpReponseReview()
    {

        var text_review = trim(document.getElementById('text-admin_response').value);

        if (text_review.length == 0)
        {
            field_state_change('text-admin_response','failed', '{/literal}{$spmgsnipreviewrca_msg1|escape:'htmlall':'UTF-8'}{literal}');
            return false;
        }
        field_state_change('text-admin_response','success', '');
        return true;
    }


    function update_chaged(){

            var is_text_response =  check_inpReponseReview();

            if(is_text_response){

            var id_review = {/literal}{$spmgsnipreviewdatareview.id|escape:'htmlall':'UTF-8'}{literal};

            $('#changeditem'+id_review).html('<img src="../img/admin/../../modules/spmgsnipreview/views/img/loader.gif" />');

                if($("input#is_display_old").is(":checked")) {
                    var is_display_old = 1;
                } else {
                    var is_display_old = 0;
                }

                if($("input#is_send_again").is(":checked")) {
                    var is_send_again = 1;
                } else {
                    var is_send_again = 0;
                }

            $.post('{/literal}{$spmgsnipreviewreviews_admin_url nofilter}{literal}',
                    {action:'change-wait',
                    review_id:{/literal}{$spmgsnipreviewreview_id|escape:'htmlall':'UTF-8'}{literal},
                    is_display_old: is_display_old,
                    is_send_again:is_send_again,
                    admin_response: $('#text-admin_response').val(),
                    },
                    function (data) {

                        if (data.status == 'success') {

                            $('#changeditem'+id_review).html('');
                            var html = '<span class="label-tooltip" data-original-title="{/literal}{$spmgsnipreviewrca_msg2|escape:'htmlall':'UTF-8'}{literal}" data-toggle="tooltip">'+
                                '<a style="text-decoration:none" onclick="spmgsnipreview_list({/literal}{$spmgsnipreviewdatareview.id|escape:'htmlall':'UTF-8'}{literal},\'changed\',1,\'{/literal}{$spmgsnipreviewtoken|escape:'htmlall':'UTF-8'}{literal}\');" href="javascript:void(0)">'+
                                '<img src="../img/admin/../../modules/spmgsnipreview/views/img/time.gif" />'+
                            '</a>'+
                                    '</span>';
                            $('#changeditem'+id_review).html(html);

                             $('#fb-con-wrapper-admin').remove();
                             $('#fb-con').remove();



                        } else {

                            alert(data.message);

                        }
                    }, 'json');
            }

        }


</script>
{/literal}

{/if}