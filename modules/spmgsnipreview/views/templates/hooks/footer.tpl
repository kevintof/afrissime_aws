{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}


{if $spmgsnipreviewrvis_on == 1}
{if $spmgsnipreviewallinfo_on == 1 && ($spmgsnipreviewis_home_b_bottom == 1 || $spmgsnipreviewis_cat_b_bottom == 1 || $spmgsnipreviewis_man_b_bottom == 1)}

    {if count($spmgsnipreviewdata_badges)>0}

        <div class="clear-spmgsnipreview"></div>
        <div class="badges badges-footer" {if isset($spmgsnipreviewallinfoh_w)}style="width:{$spmgsnipreviewallinfoh_w|escape:'htmlall':'UTF-8'}%"{/if}>

            <strong class="title-badges">{l s='Review(s) and rating(s)' mod='spmgsnipreview'}</strong>
		<span itemscope itemtype="http://schema.org/Product">
			<meta content="{$spmgsnipreviewbadges_name|escape:'htmlall':'UTF-8'}" itemprop="name">
            <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<span>
                {assign var='test_rating' value=$spmgsnipreviewdata_badges.total_rating|replace:',':'.'}
                {section name=ratid loop=5}
                    {if $smarty.section.ratid.index <= $spmgsnipreviewdata_badges.total_rating}

                        {if $test_rating <= $spmgsnipreviewmax_star_par && $test_rating >= $spmgsnipreviewmin_star_par}

                            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                        {else}
                            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                        {/if}
                    {else}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                    {/if}
                    {$test_rating = $test_rating - 1}

                {/section}
			</span>

			<meta content="1" itemprop="worstRating" />
			(<span itemprop="ratingValue">{$spmgsnipreviewdata_badges.total_rating|escape:'htmlall':'UTF-8'}</span>/<span itemprop="bestRating">5</span>)




			<strong>{if $spmgsnipreviewis_home_b_bottom != 0}{l s='Shop' mod='spmgsnipreview'}{/if}
                {if $spmgsnipreviewis_cat_b_bottom != 0}{l s='Category' mod='spmgsnipreview'}{/if}
                {if $spmgsnipreviewis_man_b_bottom != 0}{l s='Brand' mod='spmgsnipreview'}{/if} :</strong>
             <span itemprop="itemReviewed">{$spmgsnipreviewbadges_name|escape:'htmlall':'UTF-8'}</span> -
                {l s='Based on' mod='spmgsnipreview'} <span itemprop="ratingCount">{$spmgsnipreviewdata_badges.total_reviews|escape:'htmlall':'UTF-8'}</span> {l s='rating(s)' mod='spmgsnipreview'}
                {l s='and' mod='spmgsnipreview'} <span itemprop="reviewCount">{$spmgsnipreviewdata_badges.total_reviews|escape:'htmlall':'UTF-8'}</span> {l s='review(s)' mod='spmgsnipreview'}
		</span>
            </span>

            &nbsp; - &nbsp;<a href="{$spmgsnipreviewrev_all|escape:'htmlall':'UTF-8'}"
                              title="{l s='View All Reviews' mod='spmgsnipreview'}"><span>{l s='View All Reviews' mod='spmgsnipreview'}</span></a>

        </div>

    {/if}

{/if}
{/if}




{if $spmgsnipreviewrvis_on == 1}

    {if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}


        {if $spmgsnipreviewis_blocklr == 1 && ($spmgsnipreviewis_home_bottom == 1 || $spmgsnipreviewis_cat_bottom == 1 || $spmgsnipreviewis_man_bottom == 1 || $spmgsnipreviewis_prod_bottom == 1 || $spmgsnipreviewis_oth_bottom == 1)}



        {if $spmgsnipreviewis16 == 1}

            {if $spmgsnipreviewis17 == 1}
                <div class="col-xs-12 col-sm-3 wrapper links {if $spmgsnipreviewsr_sliderr == 1}owl_storereviews_type_carousel{/if}" style="width:{$spmgsnipreviewblocklr_w|escape:'htmlall':'UTF-8'}%">
            {else}
                <section class="footer-block col-xs-12 col-sm-3 {if $spmgsnipreviewsr_sliderr == 1}owl_storereviews_type_carousel{/if}" style="width:{$spmgsnipreviewblocklr_w|escape:'htmlall':'UTF-8'}%">
            {/if}
        {else}
                <div class="clear-spmgsnipreview"></div>
                <div id="spmgsnipreview_block_footer" class="block-last-spmgsnipreviews footer-block blockmanufacturer" style="width:{$spmgsnipreviewblocklr_w|escape:'htmlall':'UTF-8'}%">
        {/if}




                <h4 {if $spmgsnipreviewis17 == 1}class="h3 hidden-sm-down"{/if}>

                    <div class="spmgsnipreviews-float-left">
                        {l s='Last Product Reviews' mod='spmgsnipreview'}
                    </div>
                    <div class="spmgsnipreviews-float-left margin-left-5">
                        {if $spmgsnipreviewrsson == 1}
                            <a href="{$spmgsnipreviewrss_url nofilter}" target="_blank" title="{l s='RSS Feed' mod='spmgsnipreview'}">
                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/feed.png" alt="{l s='RSS Feed' mod='spmgsnipreview'}" />
                            </a>
                        {/if}
                    </div>
                    <div class="spmgsnipreviews-clear"></div>

                </h4>


                {if $spmgsnipreviewis17 == 1}
                <div data-toggle="collapse" data-target="#last_reviews_block_footer" class="title clearfix hidden-md-up">
                    <span class="h3">{l s='Last Product Reviews' mod='spmgsnipreview'}</span>
                    <span class="pull-xs-right">
                      <span class="navbar-toggler collapse-icons">
                        <i class="material-icons add">&#xE313;</i>
                         <i class="material-icons remove">&#xE316;</i>
                      </span>
                    </span>
                </div>
                {/if}


                <div class="block_content block-items-data toggle-footer {if $spmgsnipreviewis17 == 1}collapse{/if}" {if $spmgsnipreviewis17 == 1}id="last_reviews_block_footer"{/if}>

                    {if count($spmgsnipreviewreviews_block)>0}


                         {if $spmgsnipreviewsr_sliderr == 1 && (count($spmgsnipreviewreviews_block) > $spmgsnipreviewsr_slr)}<ul class="owl-carousel owl-theme">{/if}



                        {foreach from=$spmgsnipreviewreviews_block item=review name=myLoop}


                            {if $spmgsnipreviewsr_sliderr == 1}

                                {if ($smarty.foreach.myLoop.index % $spmgsnipreviewsr_slr == 0) || $smarty.foreach.myLoop.first}
                                    <div>
                                {/if}

                            {/if}


                            <div class="items-last-spmgsnipreviews ">

                                <div class="row-custom">
                                {if $review.product_img}
                                    <div class="img-block-spmgsnipreview col-xs-{if $spmgsnipreviewsr_sliderr == 1}3{else}2{/if}-custom">
                                        <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                                           title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                                >
                                            <img src="{$review.product_img|escape:'htmlall':'UTF-8'}" title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                                 alt = "{$review.product_name|escape:'htmlall':'UTF-8'}" class="border-image-review img-responsive" />
                                        </a>
                                    </div>
                                {/if}
                                <div class="body-block-spmgsnipreview col-xs-{if $spmgsnipreviewsr_sliderr == 1}9{else}10{/if}-custom {if !$review.product_img}body-block-spmgsnipreview-100{/if}">
                                    <div class="title-block-last-spmgsnipreview">


                                        <div class="r-product">
                                            {l s='By' mod='spmgsnipreview'}
                                            {if $spmgsnipreviewis_avatarr == 1 && strlen($review.avatar)>0 && $review.is_show_ava == 1}

                                                <span class="avatar-block-rev">
                                                    <img alt="{$review.customer_name|escape:'htmlall':'UTF-8' nofilter}"
                                                         src="{$review.avatar|escape:'htmlall':'UTF-8'}">
                                                </span>

                                            {/if}

                                            {if strlen($review.customer_name)>0}
                                                {if $spmgsnipreviewis_uprof && $review.id_customer > 0 && $review.is_show_ava == 1}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.customer_name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}<strong
                                                        >{$review.customer_name|escape:'htmlall':'UTF-8' nofilter}</strong>{if $spmgsnipreviewis_uprof && $review.id_customer > 0 && $review.is_show_ava == 1}</a>{/if}
                                            {/if}
                                            {if strlen($review.customer_name)>0}{l s='on' mod='spmgsnipreview'}{/if}&nbsp;<strong>{$review.time_add|date_format|escape:'htmlall':'UTF-8'}</strong>


                                        </div>

                                        {if $review.is_active == 1}
                                        {if $spmgsnipreviewratings_on == 1 && $review.rating != 0}
                                            <div  class="rating-stars-total-block">
                                                ({$review.rating|escape:'htmlall':'UTF-8'}/5)
                                            </div>
                                            <div class="r-rating">
                                                {section name=ratid loop=5}
                                                    {if $smarty.section.ratid.index < $review.rating}
                                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                                    {else}
                                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                                    {/if}

                                                {/section}
                                            </div>
                                        {/if}
                                        {/if}
                                        <div class="clear-spmgsnipreview"></div>
                                        {if $review.is_buy != 0}
                                            <span class="spmgsnipreview-block-date float-left">
                                            <span class="is_buy_product is_buy_product_block">{l s='Verified Purchase' mod='spmgsnipreview'}</span>
                                        </span>
                                            <div class="clear-spmgsnipreview"></div>
                                        {/if}
                                    </div>

                                    <div class="title-block-r">
                                        <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                                           title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                                >
                                            {$review.product_name|escape:'htmlall':'UTF-8'}
                                        </a>
                                    </div>


                                    {if $review.is_active == 1}

                                        {if $spmgsnipreviewtext_on == 1 && strlen($review.text_review)>0}
                                            <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                                               title="{$review.text_review|escape:'quotes':'UTF-8':strip_tags|substr:0:$spmgsnipreviewblocklr_tr|escape:'htmlall':'UTF-8'}"
                                                    >
                                                {$review.text_review|strip_tags|substr:0:$spmgsnipreviewblocklr_tr|escape:'htmlall':'UTF-8' nofilter}{if strlen($review.text_review)>$spmgsnipreviewblocklr_tr}...{/if}
                                            </a>
                                        {/if}

                                    {else}

                                        {l s='The customer has rated the product but has not posted a review, or the review is pending moderation' mod='spmgsnipreview'}
                                    {/if}

                                </div>
                                </div>
                                <div class="clear-spmgsnipreview"></div>
                            </div>

                            {if $spmgsnipreviewsr_sliderr == 1}

                                {if ($smarty.foreach.myLoop.index % $spmgsnipreviewsr_slr == $spmgsnipreviewsr_slr - 1) || $smarty.foreach.myLoop.last}
                                    </div>
                                {/if}
                            {/if}


                        {/foreach}


                        {if $spmgsnipreviewsr_sliderr == 1 && (count($spmgsnipreviewreviews_block) > $spmgsnipreviewsr_slr)}</ul>{/if}


                        <div class="gsniprev-view-all float-right">
                            <a href="{$spmgsnipreviewallr_url|escape:'htmlall':'UTF-8'}"
                               class="btn btn-default button button-small-spmgsnipreview"
                                    >
                                <span>{l s='View All Reviews' mod='spmgsnipreview'}</span>
                            </a>
                        </div>
                        <div class="clear-spmgsnipreview"></div>

                    {else}
                        <div class="gsniprev-block-noreviews">
                            {l s='There are not Product Reviews yet.' mod='spmgsnipreview'}
                        </div>
                    {/if}
                </div>


        {if $spmgsnipreviewis16 == 1}

            {if $spmgsnipreviewis17 == 1}
                </div>
            {else}
                </section>
            {/if}
        {else}
            </div>
        {/if}




        {/if}

    {/if}

{/if}


{if $spmgsnipreviewis_uprof == 1}
{if $spmgsnipreviewradv_footer == 1}

    {if $spmgsnipreviewis16 == 1}

            {if $spmgsnipreviewis17 == 1}
                <div class="col-xs-12 col-sm-12 wrapper links {if $spmgsnipreviewsr_slideru == 1 && (count($spmgsnipreviewcustomers_block) > $spmgsnipreviewsr_slu)}owl_users_type_carousel{/if}">
            {else}
                <section class="footer-block col-xs-12 col-sm-3 {if $spmgsnipreviewsr_slideru == 1 && (count($spmgsnipreviewcustomers_block) > $spmgsnipreviewsr_slu)}owl_users_type_carousel{/if}">
            {/if}


    {else}
        <div class="clear"></div>
        <div id="spmgsnipreview_block_footer_users" class="block footer-block {if $spmgsnipreviewis16 == 1}blockmanufacturer16-footer{else}blockmanufacturer{/if}"
        >
    {/if}


		<h4 {if $spmgsnipreviewis17 == 1}class="h3 hidden-sm-down"{/if}>
			<a href="{$spmgsnipreviewshoppers_url|escape:'htmlall':'UTF-8'}"
			   title="{l s='Users' mod='spmgsnipreview'}"
				>{l s='Users' mod='spmgsnipreview'}</a>
		</h4>

        {if $spmgsnipreviewis17 == 1}
            <div data-toggle="collapse" data-target="#all_users_block_footer" class="title clearfix hidden-md-up">
                <span class="h3">{l s='Users' mod='spmgsnipreview'}</span>
                        <span class="pull-xs-right">
                          <span class="navbar-toggler collapse-icons">
                            <i class="material-icons add">&#xE313;</i>
                            <i class="material-icons remove">&#xE316;</i>
                          </span>
                        </span>
            </div>
        {/if}

		<div class="block_content block-items-data toggle-footer {if $spmgsnipreviewis17 == 1}collapse{/if}" {if $spmgsnipreviewis17 == 1}id="all_users_block_footer"{/if}>
			{if count($spmgsnipreviewcustomers_block)>0}



			<ul class="users-block-items {if $spmgsnipreviewsr_slideru == 1 && (count($spmgsnipreviewcustomers_block) > $spmgsnipreviewsr_slu)}owl-carousel owl-theme{/if}">
			{foreach from=$spmgsnipreviewcustomers_block item=customer name=myLoop}


                {if $spmgsnipreviewsr_slideru == 1}

                    {if ($smarty.foreach.myLoop.index % $spmgsnipreviewsr_slu == 0) || $smarty.foreach.myLoop.first}
                        <div>
                    {/if}

                {/if}


	    		<li>
	    			<img src="{$customer.avatar_thumb|escape:'htmlall':'UTF-8'}"
						class="user-img-spmgsnipreview"
	    		   	   		  title="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}"
	    		   	   		  alt = "{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}" />
	    			<a href="{$spmgsnipreviewshopper_url|escape:'htmlall':'UTF-8'}{$customer.id_customer|escape:'htmlall':'UTF-8'}"
	    		   	   title="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}">
	    		   	   	 {$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}
	    		   	   </a>
	    		   	 <div class="clr"></div>
	    		</li>


                {if $spmgsnipreviewsr_slideru == 1}

                    {if ($smarty.foreach.myLoop.index % $spmgsnipreviewsr_slu == $spmgsnipreviewsr_slu - 1) || $smarty.foreach.myLoop.last}
                        </div>
                    {/if}
                {/if}

	    	{/foreach}
	    	</ul>
	    	{else}
	    		<div class="padding-10">
					{l s='There are not users yet.' mod='spmgsnipreview'}
				</div>
	    	{/if}
	    	<div class="gsniprev-view-all">
                    <a class="btn btn-default button button-small-spmgsnipreview" href="{$spmgsnipreviewshoppers_url|escape:'htmlall':'UTF-8'}" title="{l s='View all users' mod='spmgsnipreview'}">
                        <span>{l s='View all users' mod='spmgsnipreview'}</span>
                    </a>
                </div>
		</div>
    {if $spmgsnipreviewis16 == 1}

        {if $spmgsnipreviewis17 == 1}
            </div>
        {else}
            </section>
        {/if}

    {else}
	    </div>
    {/if}

{/if}
{/if}




{if $spmgsnipreviewis_storerev == 1}
{if $spmgsnipreviewt_footer == 1}

    {if ($spmgsnipreviewis_mobile == 1 && $spmgsnipreviewmt_footer == 1) || (!$spmgsnipreviewis_mobile == 1 && $spmgsnipreviewst_footer == 1)}


        {if $spmgsnipreviewis17 == 1}
            <div class="col-xs-12 col-sm-12 wrapper links {if $spmgsnipreviewsr_slider == 1}owl_storereviews_type_carousel{/if}">
        {else}
            <section class="footer-block col-xs-12 col-sm-3 {if $spmgsnipreviewsr_slider == 1}owl_storereviews_type_carousel{/if}">
        {/if}



		<h4 {if $spmgsnipreviewis17 == 1}class="h3 hidden-sm-down"{/if}>



		<div class="float-left">
					<a href="{$spmgsnipreviewstorereviews_url|escape:'htmlall':'UTF-8'}"
					>{l s='Store Reviews' mod='spmgsnipreview'}&nbsp;(&nbsp;{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}&nbsp;)</a>

		</div>
		<div class="float-left margin-left-5">
		{if $spmgsnipreviewrssontestim == 1}
			<a href="{$spmgsnipreviewrss_testimonials_url nofilter}" title="{l s='RSS Feed' mod='spmgsnipreview'}" target="_blank">
				<img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/feed.png" alt="{l s='RSS Feed' mod='spmgsnipreview'}" />
			</a>
		{/if}
		</div>
		<div class="clear"></div>


		<div {if $spmgsnipreviewt_footers == 1}itemscope itemtype="http://schema.org/corporation"{/if} class="total-rating-items-block-footer margin-top-5">

            {if $spmgsnipreviewt_footers == 1}
                <meta itemprop="name" content="{$spmgsnipreviewsh_nameti|escape:'htmlall':'UTF-8'}">
                <meta itemprop="url" content="{$spmgsnipreviewsh_urlti|escape:'htmlall':'UTF-8'}">
            {/if}

                <div {if $spmgsnipreviewt_footers == 1}itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating"{/if}>


            {if $spmgsnipreviewt_footers == 1}
                <meta itemprop="worstRating" content="1">
                <meta itemprop="ratingCount" content="{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}">
            {/if}


                    {assign var='test_rating_store' value=$spmgsnipreviewavg_decimalti|replace:',':'.'}

                    {section name=ratid loop=5 start=0}
                        {if $smarty.section.ratid.index < $spmgsnipreviewavg_ratingti}

                            {if $test_rating_store <= $spmgsnipreviewmax_star_par && $test_rating_store >= $spmgsnipreviewmin_star_par}
                                <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"/>
                            {else}
                                <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                            {/if}

                        {else}
                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                        {/if}
                        {$test_rating_store = $test_rating_store - 1}
                    {/section}



                  <span {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}>
                        (<span {if $spmgsnipreviewt_footers == 1}itemprop="ratingValue"{/if} {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}
                                >{$spmgsnipreviewavg_decimalti|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewt_footers == 1}itemprop="bestRating"{/if} {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}
                                >5</span>)
                        </span>

            </div>

            </div>

		</h4>


    {if $spmgsnipreviewis17 == 1}
        <div data-toggle="collapse" data-target="#storereviews_block_footer" class="title clearfix hidden-md-up">
            <span class="h3">{l s='Store Reviews' mod='spmgsnipreview'}</span>
                        <span class="pull-xs-right">
                          <span class="navbar-toggler collapse-icons">
                            <i class="material-icons add">&#xE313;</i>
                            <i class="material-icons remove">&#xE316;</i>
                          </span>
                        </span>
        </div>
    {/if}


		<div class="block_content block-items-data toggle-footer {if $spmgsnipreviewis17 == 1}collapse{/if}" {if $spmgsnipreviewis17 == 1}id="storereviews_block_footer"{/if}>
	    {if count($spmgsnipreviewreviews_f) > 0}


        {if $spmgsnipreviewsr_slider == 1 && (count($spmgsnipreviewreviews_f) > $spmgsnipreviewsr_sl)}<ul class="owl-carousel owl-theme">{/if}

	    {foreach from=$spmgsnipreviewreviews_f item=review name=myLoop}

            {if $spmgsnipreviewsr_slider == 1}

                {if ($smarty.foreach.myLoop.index % $spmgsnipreviewsr_sl == 0) || $smarty.foreach.myLoop.first}
                    <div>
                {/if}

            {/if}


	    <div class="rItem {if $spmgsnipreviewis_ps15 == 1 && $spmgsnipreviewis16 == 0}padding-0{/if}">
			<div class="ratingBox">
				<small>{l s='Review By' mod='spmgsnipreview'} <b>{if $spmgsnipreviewis_uprof && $review.is_show_ava && $review.id_customer > 0}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}{$review.name|escape:'htmlall':'UTF-8' nofilter}{if $spmgsnipreviewis_uprof && $review.id_customer > 0}</a>{/if}</b></small>
                <br/>

                <div class="float-right">
				{if $review.rating != 0}
                    {section name=bar loop=5 start=0}
                        {if $smarty.section.bar.index < $review.rating}
                            <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                        {else}
                            <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                        {/if}
                    {/section}
                {else}
                    {section name=bar loop=5 start=0}
                        <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/rating_star2.png" />
                    {/section}
                {/if}
                </div>

			</div>
			<div class="clear"></div>
            <div class="margin-bottom-5">
            {if $spmgsnipreviewis_avatar == 1 && $review.is_show_ava}
                <div class="float-left {if $spmgsnipreviewis16 == 1}avatar-block{else}avatar-block15{/if}">
                    <img
                    {if strlen($review.avatar)>0}
                        src="{$review.avatar|escape:'htmlall':'UTF-8'}"
                    {else}
                        src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/avatar_m.gif"
                    {/if}
                    alt="{$review.name|escape:'htmlall':'UTF-8'}"
                    />
                </div>
             {/if}
                <div class="{if $spmgsnipreviewis17 == 0}font-size-11{/if} float-left {if $spmgsnipreviewis16 == 1}{if $spmgsnipreviewis_avatar == 1}testimonial-block-text{else}testimonial-block-text-100{/if}{else}{if $spmgsnipreviewis_avatar == 1}testimonial-block-text15{else}testimonial-block-text-100{/if}{/if}">
                    {$review.message|substr:0:100|escape:'htmlall':'UTF-8' nofilter}
                    {if strlen($review.message)>100}...{/if}

                </div>
                <div class="clear"></div>
            </div>
			{if $spmgsnipreviewis_web == 1}
            {if strlen($review.web)>0}
                <small class="float-right">
                    <a title="http://{$review.web|escape:'htmlall':'UTF-8'}" rel="nofollow" href="http://{$review.web|escape:'htmlall':'UTF-8'}"
                       target="_blank" class="testimonials-link-web"
                            >http://{$review.web|escape:'htmlall':'UTF-8'}</a>
                </small>
            {/if}
            {/if}
            <small class="float-left">{$review.date_add|date_format|escape:'htmlall':'UTF-8'}</small>

            <div class="clear"></div>

            <span class="float-right">{if $review.is_buy != 0}<span class="is_buy">{l s='Verified Purchase' mod='spmgsnipreview'}</span>{/if}</span>
            <div class="clear"></div>
		</div>

            {if $spmgsnipreviewsr_slider == 1}

                {if ($smarty.foreach.myLoop.index % $spmgsnipreviewsr_sl == $spmgsnipreviewsr_sl - 1) || $smarty.foreach.myLoop.last}
                    </div>
                {/if}
            {/if}


		{/foreach}


        {if $spmgsnipreviewsr_slider == 1 && (count($spmgsnipreviewreviews_f) > $spmgsnipreviewsr_sl)}</ul>{/if}


		{else}
		<div class="rItem no-items-shopreviews">
			{l s='There are not store reviews yet.' mod='spmgsnipreview'}
		</div>
		{/if}




	   <div class="submit_testimonal" align="center">
	   <a title="{l s='See all Store Reviews' mod='spmgsnipreview'}" class="btn btn-default button button-small-spmgsnipreview"
	  		   href="{$spmgsnipreviewstorereviews_url|escape:'htmlall':'UTF-8'}"><span>{l s='See all Store Reviews' mod='spmgsnipreview'}</span></a>

		</div>

		</div>


	{if $spmgsnipreviewis16 == 1}
        </section>
    {else}
	    </div>
    {/if}
{/if}
{/if}





<!-- left column testimonials -->
{if $spmgsnipreviewt_leftside == 1}

        {if ($spmgsnipreviewis_mobile == 1 && $spmgsnipreviewmt_leftside == 1) || (!$spmgsnipreviewis_mobile == 1 && $spmgsnipreviewst_leftside == 1)}
<table class="spmgsnipreview-widgets">


            <tr><td class="facebook_block">
                    <div id="spmgsnipreview-box" class="left_shopreviews" >
                        <div class="outside">
                            <div class="inside">


                                <!-- code block testimonials -->
                                <div id="spmgsnipreview_block_footer"  class="myaccount ps15-color-background {if $spmgsnipreviewis_ps15 == 1}color-black{/if}"
                                     style="width:{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8'}px">



                                    <h4 class="text-align-center block-side-item {if $spmgsnipreviewis_ps15 == 0}testimonials-block-14{/if}
                                    {if $spmgsnipreviewis_ps15 == 1 && $spmgsnipreviewis16 == 0}testimonials-block-15-footer{/if}">

                                        <div>
                                            <a class="color-black"
                                               href="{$spmgsnipreviewstorereviews_url|escape:'htmlall':'UTF-8'}"
                                                    >{l s='Store Reviews' mod='spmgsnipreview'}&nbsp;(&nbsp;{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}&nbsp;)</a>

                                            {if $spmgsnipreviewrssontestim == 1}
                                                <a href="{$spmgsnipreviewrss_testimonials_url nofilter}" title="{l s='RSS Feed' mod='spmgsnipreview'}" target="_blank">
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/feed.png" alt="{l s='RSS Feed' mod='spmgsnipreview'}" />
                                                </a>
                                            {/if}

                                        </div>

                                        <div {if $spmgsnipreviewt_leftsides == 1}itemscope itemtype="http://schema.org/corporation"{/if} class="total-rating-items-block margin-top-3">

                                            {if $spmgsnipreviewt_leftsides == 1}
                                            <meta itemprop="name" content="{$spmgsnipreviewsh_nameti|escape:'htmlall':'UTF-8'}">
                                            <meta itemprop="url" content="{$spmgsnipreviewsh_urlti|escape:'htmlall':'UTF-8'}">
                                            {/if}


                                            <div {if $spmgsnipreviewt_leftsides == 1}itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating"{/if}>

                                                {if $spmgsnipreviewt_leftsides == 1}
                                            <meta itemprop="worstRating" content="1">
                                            <meta itemprop="ratingCount" content="{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}">
                                                {/if}


                                                {assign var='test_rating_store' value=$spmgsnipreviewavg_decimalti|replace:',':'.'}

                                                {section name=ratid loop=5 start=0}
                                                    {if $smarty.section.ratid.index < $spmgsnipreviewavg_ratingti}

                                                        {if $test_rating_store <= $spmgsnipreviewmax_star_par && $test_rating_store >= $spmgsnipreviewmin_star_par}
                                                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"/>
                                                        {else}
                                                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                                        {/if}

                                                    {else}
                                                        <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                                    {/if}
                                                    {$test_rating_store = $test_rating_store - 1}
                                                {/section}


                                                <span {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}>
                        (<span {if $spmgsnipreviewt_leftsides == 1}itemprop="ratingValue"{/if} {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}
                                                            >{$spmgsnipreviewavg_decimalti|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewt_leftsides == 1}itemprop="bestRating"{/if} {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}
                                                            >5</span>)
                        </span>

                                        </div>

                                        </div>

                                    </h4>

                                    <div class="block_content block-items-data">
                                        {if $spmgsnipreviewcount_all_reviews > 0}

                                            {foreach from=$spmgsnipreviewreviews_ls item=review name=myLoop}
                                                <div class="rItem {if $spmgsnipreviewis_ps15 == 1 && $spmgsnipreviewis16 == 0}padding-0{/if}">
                                                    <div class="ratingBox">
                                                        <small>{l s='Review By' mod='spmgsnipreview'} <b>{if $spmgsnipreviewis_uprof && $review.is_show_ava && $review.id_customer > 0}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}{$review.name|escape:'htmlall':'UTF-8' nofilter}{if $spmgsnipreviewis_uprof && $review.id_customer > 0}</a>{/if}</b></small>
                                                        <br/>



                                                        <div class="float-right">
                                                        {if $review.rating != 0}
                                                            {*{for $foo=0 to 4}*}
                                                            {section name=bar loop=5 start=0}
                                                                {if $smarty.section.bar.index < $review.rating}
                                                                    <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                                                {else}
                                                                    <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                                                {/if}
                                                            {/section}
                                                            {*{/for}*}
                                                        {else}
                                                            {*{for $foo=0 to 4}*}
                                                            {section name=bar loop=5 start=0}
                                                                <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/rating_star2.png" />
                                                            {/section}
                                                            {*{/for}*}
                                                        {/if}
                                                        </div>

                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="margin-bottom-5">
                                                        {if $spmgsnipreviewis_avatar == 1 && $review.is_show_ava}
                                                        <div class="float-left avatar-block-popup">
                                                            <img
                                                                    {if strlen($review.avatar)>0}
                                                                        src="{$review.avatar|escape:'htmlall':'UTF-8'}"
                                                                    {else}
                                                                        src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/avatar_m.gif"
                                                                    {/if}
                                                                    alt="{$review.name|escape:'htmlall':'UTF-8'}"
                                                                    />
                                                        </div>
                                                        {/if}

                                                        <div class="font-size-11 float-left {if $spmgsnipreviewis_avatar == 1}testimonial-block-text-popup{else}testimonial-block-text-popup-100{/if}">
                                                            {$review.message|substr:0:100|escape:'htmlall':'UTF-8' nofilter}
                                                            {if strlen($review.message)>100}...{/if}

                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    {if $spmgsnipreviewis_web == 1}
                                                        {if strlen($review.web)>0}
                                                            <small class="float-right">
                                                                <a title="http://{$review.web|escape:'htmlall':'UTF-8'}" rel="nofollow" href="http://{$review.web|escape:'htmlall':'UTF-8'}"
                                                                   target="_blank" class="testimonials-link-web"
                                                                        >http://{$review.web|escape:'htmlall':'UTF-8'}</a>
                                                            </small>
                                                        {/if}
                                                    {/if}
                                                    <small class="float-left">{$review.date_add|date_format|escape:'htmlall':'UTF-8'}</small>

                                                    <div class="clear"></div>

                                                    <span class="float-right">{if $review.is_buy != 0}<span class="is_buy">{l s='Verified Purchase' mod='spmgsnipreview'}</span>{/if}</span>
                                                    <div class="clear"></div>
                                                </div>
                                            {/foreach}
                                        {else}
                                            <div class="rItem no-items-shopreviews">
                                                {l s='There are not store reviews yet.' mod='spmgsnipreview'}
                                            </div>
                                        {/if}




                                        <div class="submit_testimonal" align="center">
                                                <a title="{l s='See all Store Reviews' mod='spmgsnipreview'}" class="btn btn-default button button-small-spmgsnipreview"
                                                   href="{$spmgsnipreviewstorereviews_url|escape:'htmlall':'UTF-8'}"><span>{l s='See all Store Reviews' mod='spmgsnipreview'}</span></a>

                                        </div>

                                    </div>


                                </div>
                                <!-- code block testimonials -->



                            </div>
                        </div>
                        <div class="belt">{if $spmgsnipreviewis_mobile == 0}{l s='Store Reviews' mod='spmgsnipreview'}{/if}</div>
                    </div>
                </td></tr>



</table>
{/if}
{/if}
<!-- left column testimonials -->






<!-- right column testimonials -->

{if $spmgsnipreviewt_rightside == 1}
    {if ($spmgsnipreviewis_mobile == 1 && $spmgsnipreviewmt_rightside == 1) || (!$spmgsnipreviewis_mobile == 1 && $spmgsnipreviewst_rightside == 1)}
<table class="spmgsnipreview-widgets">


            <tr><td class="facebook_block">
                    <div id="spmgsnipreview-box" class="right_shopreviews">
                        <div class="outside">
                            <div class="inside">

                                <!-- code block testimonials -->
                                <div id="spmgsnipreview_block_footer"  class="myaccount ps15-color-background {if $spmgsnipreviewis_ps15 == 1}color-black{/if}"
                                     style="width:{$spmgsnipreviewt_width|escape:'htmlall':'UTF-8'}px">


                                    <h4 class="text-align-center block-side-item {if $spmgsnipreviewis_ps15 == 0}testimonials-block-14{/if}
                                     {if $spmgsnipreviewis_ps15 == 1 && $spmgsnipreviewis16 == 0}testimonials-block-15-footer{/if}">

                                        <div>
                                            <a class="color-black"
                                               href="{$spmgsnipreviewstorereviews_url|escape:'htmlall':'UTF-8'}"
                                                    >{l s='Store Reviews' mod='spmgsnipreview'}&nbsp;(&nbsp;{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}&nbsp;)</a>

                                            {if $spmgsnipreviewrssontestim == 1}
                                                <a href="{$spmgsnipreviewrss_testimonials_url nofilter}" title="{l s='RSS Feed' mod='spmgsnipreview'}" target="_blank">
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/feed.png" alt="{l s='RSS Feed' mod='spmgsnipreview'}" />
                                                </a>
                                            {/if}

                                        </div>

                                        <div {if $spmgsnipreviewt_rightsides == 1}itemscope itemtype="http://schema.org/corporation"{/if} class="total-rating-items-block margin-top-3">

                                            {if $spmgsnipreviewt_rightsides == 1}
                                            <meta itemprop="name" content="{$spmgsnipreviewsh_nameti|escape:'htmlall':'UTF-8'}">
                                            <meta itemprop="url" content="{$spmgsnipreviewsh_urlti|escape:'htmlall':'UTF-8'}">
                                            {/if}


                                            <div {if $spmgsnipreviewt_rightsides == 1}itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating"{/if}>

                                                {if $spmgsnipreviewt_rightsides == 1}
                                            <meta itemprop="worstRating" content="1">
                                            <meta itemprop="ratingCount" content="{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}">
                                                {/if}


                                                {assign var='test_rating_store' value=$spmgsnipreviewavg_decimalti|replace:',':'.'}

                                                {section name=ratid loop=5 start=0}
                                                    {if $smarty.section.ratid.index < $spmgsnipreviewavg_ratingti}

                                                        {if $test_rating_store <= $spmgsnipreviewmax_star_par && $test_rating_store >= $spmgsnipreviewmin_star_par}
                                                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"/>
                                                        {else}
                                                            <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                                        {/if}

                                                    {else}
                                                        <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                                    {/if}
                                                    {$test_rating_store = $test_rating_store - 1}
                                                {/section}


                                                <span {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}>
                                                    (<span {if $spmgsnipreviewt_rightsides == 1}itemprop="ratingValue"{/if} {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}
                                                                                        >{$spmgsnipreviewavg_decimalti|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewt_rightsides == 1}itemprop="bestRating"{/if} {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}
                                                                                        >5</span>)
                                                    </span>

                                        </div>

                                        </div>

                                    </h4>


                                    <div class="block_content block-items-data">
                                        {if $spmgsnipreviewcount_all_reviews > 0}

                                            {foreach from=$spmgsnipreviewreviews_rs item=review name=myLoop}
                                                <div class="rItem {if $spmgsnipreviewis_ps15 == 1 && $spmgsnipreviewis16 == 0}padding-0{/if}">
                                                    <div class="ratingBox">
                                                        <small>{l s='Review By' mod='spmgsnipreview'} <b>{if $spmgsnipreviewis_uprof && $review.is_show_ava && $review.id_customer > 0}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}{$review.name|escape:'htmlall':'UTF-8' nofilter}{if $spmgsnipreviewis_uprof && $review.id_customer > 0}</a>{/if}</b></small>
                                                        <br/>


                                                        <div class="float-right">

                                                        {if $review.rating != 0}
                                                            {*{for $foo=0 to 4}*}
                                                            {section name=bar loop=5 start=0}
                                                                {if $smarty.section.bar.index < $review.rating}
                                                                    <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                                                {else}
                                                                    <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                                                {/if}
                                                            {/section}
                                                            {*{/for}*}
                                                        {else}
                                                            {*{for $foo=0 to 4}*}
                                                            {section name=bar loop=5 start=0}
                                                                <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/rating_star2.png" />
                                                            {/section}
                                                            {*{/for}*}
                                                        {/if}
                                                        </div>


                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="margin-bottom-5">
                                                        {if $spmgsnipreviewis_avatar == 1 && $review.is_show_ava}
                                                        <div class="float-left avatar-block-popup">
                                                            <img
                                                                    {if strlen($review.avatar)>0}
                                                                        src="{$review.avatar|escape:'htmlall':'UTF-8'}"
                                                                    {else}
                                                                        src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/avatar_m.gif"
                                                                    {/if}
                                                                    alt="{$review.name|escape:'htmlall':'UTF-8'}"
                                                                    />
                                                        </div>
                                                        {/if}

                                                        <div class="font-size-11 float-left {if $spmgsnipreviewis_avatar == 1}testimonial-block-text-popup{else}testimonial-block-text-popup-100{/if}">
                                                            {$review.message|substr:0:100|escape:'htmlall':'UTF-8' nofilter}
                                                            {if strlen($review.message)>100}...{/if}

                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    {if $spmgsnipreviewis_web == 1}
                                                        {if strlen($review.web)>0}
                                                            <small class="float-right">
                                                                <a title="http://{$review.web|escape:'htmlall':'UTF-8'}" rel="nofollow" href="http://{$review.web|escape:'htmlall':'UTF-8'}"
                                                                   target="_blank" class="testimonials-link-web"
                                                                        >http://{$review.web|escape:'htmlall':'UTF-8'}</a>
                                                            </small>
                                                        {/if}
                                                    {/if}
                                                    <small class="float-left">{$review.date_add|date_format|escape:'htmlall':'UTF-8'}</small>

                                                    <div class="clear"></div>

                                                    <span class="float-right">{if $review.is_buy != 0}<span class="is_buy">{l s='Verified Purchase' mod='spmgsnipreview'}</span>{/if}</span>
                                                    <div class="clear"></div>
                                                </div>
                                            {/foreach}
                                        {else}
                                            <div class="rItem no-items-shopreviews">
                                                {l s='There are not store reviews yet.' mod='spmgsnipreview'}
                                            </div>
                                        {/if}




                                        <div class="submit_testimonal" align="center">
                                                <a title="{l s='See all Store Reviews' mod='spmgsnipreview'}" class="btn btn-default button button-small-spmgsnipreview"
                                                   href="{$spmgsnipreviewstorereviews_url|escape:'htmlall':'UTF-8'}"><span>{l s='See all Store Reviews' mod='spmgsnipreview'}</span></a>

                                        </div>

                                    </div>


                                </div>
                                <!-- code block testimonials -->

                            </div>
                        </div>
                        {*<div class="belt"><i class="icon-facebook"></i></div>*}
                        <div class="belt">{if $spmgsnipreviewis_mobile == 0}{l s='Store Reviews' mod='spmgsnipreview'}{/if}</div>
                    </div>
                </td></tr>



</table>
{/if}
{/if}
<!-- right column testimonials -->



{/if}












{if $spmgsnipreviewpinvis_on == 1 && $spmgsnipreviewis_product_page != 0}

{literal}
    <script defer="defer" src="//assets.pinterest.com/js/pinit.js" data-pin-build="parsePins"></script>
{/literal}

{/if}


{$spmgsnipreviewbreadcustom|escape:'quotes':'UTF-8' nofilter}




