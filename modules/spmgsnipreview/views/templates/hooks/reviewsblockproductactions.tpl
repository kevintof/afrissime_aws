{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}



{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}

{if $spmgsnipreviewhooktodisplay == "product_actions"}

    <div class="clear-spmgsnipreview"></div>

    <div class="{if $spmgsnipreviewis16 == 1}gsniprev-block-16{else}gsniprev-block{/if}">
        <b class="title-rating-block">
            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{l s='Total Rating' mod='spmgsnipreview'}" />&nbsp;{l s='Total Rating' mod='spmgsnipreview'}</b><span class="ratings-block-punct">:</span>

        <br/><br/>


        {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">{/if}
            {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}
                <meta content="1" itemprop="worstRating">
                <meta content="{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}" itemprop="ratingCount">
            {/if}

            {*<div class="rating">{$spmgsnipreviewavg_rating|escape:'htmlall':'UTF-8'}</div>*}

            <div class="float-left">
            {assign var='test_rating_block' value=$spmgsnipreviewavg_rating}
            {section name=ratid loop=5}

                {if $smarty.section.ratid.index < $spmgsnipreviewavg_rating}

                    {if $test_rating_block <= $spmgsnipreviewmax_star_par && $test_rating_block >= $spmgsnipreviewmin_star_par}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"
                             alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                    {else}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}"
                             alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                    {/if}

                {else}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                         alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                {/if}
                {$test_rating_block = $test_rating_block - 1}
            {/section}
            </div>

            <div class="gsniprev-block-reviews-text">
                <span {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}itemprop="ratingValue"{/if}>{$spmgsnipreviewavg_decimal|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}itemprop="bestRating"{/if}>5</span> - <span id="count_review_block" {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}itemprop="reviewCount"{/if}>{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}</span> <span id="reviews_text_block">{$spmgsnipreviewtext_reviews|escape:'htmlall':'UTF-8'}</span>
            </div>
            <div class="clear-spmgsnipreview"></div>
            {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}</div>{/if}

        <br/>


        {if $spmgsnipreviewstarratingon == 1}

            <a href="javascript:void(0)" onclick="$('.gsniprev-rating-block').toggle();" class="view-ratings">{l s='View ratings' mod='spmgsnipreview'}</a>
            <br/>
            <div class="gsniprev-rating-block">
                <table class="gsniprev-rating-block-table">
                    <tr>
                        <td class="gsniprev-rating-block-left">
                            {section name="test" loop=5}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                        </td>
                        <td class="gsniprev-rating-block-right"><b id="five-blockreview">{$spmgsnipreviewfive|escape:'htmlall':'UTF-8'}</b></td>
                    </tr>
                    <tr>
                        <td class="gsniprev-rating-block-left">
                            {section name="test" loop=4}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            {section name="test" loop=1}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                        </td>
                        <td class="gsniprev-rating-block-right"><b id="four-blockreview">{$spmgsnipreviewfour|escape:'htmlall':'UTF-8'}</b></td>
                    </tr>
                    <tr>
                        <td class="gsniprev-rating-block-left">
                            {section name="test" loop=3}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            {section name="test" loop=2}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                        </td>
                        <td class="gsniprev-rating-block-right"><b id="three-blockreview">{$spmgsnipreviewthree|escape:'htmlall':'UTF-8'}</b></td>
                    </tr>
                    <tr>
                        <td class="gsniprev-rating-block-left">
                            {section name="test" loop=2}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            {section name="test" loop=3}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                        </td>
                        <td class="gsniprev-rating-block-right"><b id="two-blockreview">{$spmgsnipreviewtwo|escape:'htmlall':'UTF-8'}</b></td>
                    </tr>
                    <tr>
                        <td class="gsniprev-rating-block-left">
                            {section name="test" loop=1}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                            {section name="test" loop=4}
                                <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                            {/section}
                        </td>
                        <td class="gsniprev-rating-block-right"><b id="one-blockreview">{$spmgsnipreviewone|escape:'htmlall':'UTF-8'}</b></td>
                    </tr>
                </table>
            </div>

            <br/>
        {/if}


        {if $spmgsnipreviewis_add != 1}
            <a class="btn-spmgsnipreview btn-primary-spmgsnipreview"
               href="{if $spmgsnipreviewajax == 1}{$spmgsnipreviewsharing_url|escape:'htmlall':'UTF-8'}{/if}#idTab777"
               id="idTab777-my-click"
               {if $spmgsnipreviewis_bug == 1 && $spmgsnipreviewajax == 0}onclick="$.scrollTo('#idTab777');return false;"{/if}>
        <span>
            <i class="icon-pencil"></i>&nbsp;

            {l s='Add Review' mod='spmgsnipreview'}

        </span>
            </a>
        {/if}



        <a class="btn-spmgsnipreview btn-default-spmgsnipreview"
           href="{if $spmgsnipreviewajax == 1}{$spmgsnipreviewsharing_url|escape:'htmlall':'UTF-8'}{/if}#idTab777"
           {if $spmgsnipreviewis_bug == 1 && $spmgsnipreviewajax == 0}onclick="$.scrollTo('#idTab777');return false;"{/if}>
        <span>
            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="title-rating-one-star" alt="{l s='View Reviews' mod='spmgsnipreview'}"/>
            {l s='View Reviews' mod='spmgsnipreview'}
        </span>
        </a>


        {literal}
        <script type="text/javascript">
            var module_dir = '{/literal}{$module_dir|escape:'htmlall':'UTF-8'}{literal}';
            var spmgsnipreview_star_active = '{/literal}{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}{literal}';
            var spmgsnipreview_star_noactive = '{/literal}{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}{literal}';
            var spmgsnipreview_star_half_active = '{/literal}{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}{literal}';
            var spmgsnipreview_min_star_par = '{/literal}{$spmgsnipreviewmin_star_par|escape:'htmlall':'UTF-8'}{literal}';
            var spmgsnipreview_max_star_par = '{/literal}{$spmgsnipreviewmax_star_par|escape:'htmlall':'UTF-8'}{literal}';


        </script>
        {/literal}



    </div>




{/if}

{/if}

{/if}



{if $spmgsnipreviewpinvis_on == 1 && $spmgsnipreview_productActions == 'productActions'}
<a href="//www.pinterest.com/pin/create/button/?
		url=http://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}
		&media={$product_image|escape:'htmlall':'UTF-8'}
		&description={$meta_description|escape:'htmlall':'UTF-8'}" 
  data-pin-do="buttonPin" data-pin-config="{if $spmgsnipreviewpinterestbuttons == 'firston'}above{/if}{if $spmgsnipreviewpinterestbuttons == 'secondon'}beside{/if}{if $spmgsnipreviewpinterestbuttons == 'threeon'}none{/if}">
  <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" alt="Pinterest" />
</a>

    {if $spmgsnipreviewis17 == 1 && $spmgsnipreviewajax == 1}
    {literal}
        <script defer="defer" src="//assets.pinterest.com/js/pinit.js" data-pin-build="parsePins"></script>
        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function(event) {
                window.parsePins();
            });
        </script>
    {/literal}
    {/if}
{/if}