{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewptabs_type == 1}

    {if $spmgsnipreviewrvis_on == 1}

        {if $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1 || $spmgsnipreviewratings_on == 1}

            <h3 class="page-product-heading" id="#idTab777"><img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="title-rating-one-star" alt="{l s='Reviews' mod='spmgsnipreview'}" />&nbsp;{l s='Reviews' mod='spmgsnipreview'} <span id="count-review-tab">({$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'})</span></h3>

        {/if}

    {/if}

{/if}



{if $spmgsnipreviewrvis_on == 1}

    {if $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1 || $spmgsnipreviewratings_on == 1}

        <div id="idTab777" {if $spmgsnipreviewis17 == 1}class="block-categories {if $spmgsnipreviewptabs_type == 3}tab-pane fade in{/if}"{else}class="tab-pane"{/if}>


            <!-- reviews template -->

            <div id="shopify-product-reviews">

                <div class="spr-container row-custom">




                    {if $spmgsnipreviewis_add == 0}
                        <div class="text-align-center margin-bottom-20">
                            <span class="spr-summary-actions">

                              <a class="btn-spmgsnipreview btn-primary-spmgsnipreview" href="javascript:void(0)"  onclick="show_form_review(1)">
                                <span>
                                    <i class="icon-pencil"></i>
                                    {l s='Write a Review' mod='spmgsnipreview'}
                                </span>
                              </a>

                            </span>
                        </div>
                    {/if}


                    <div class="spr-content col-sm-12-custom" id="spr-content">




                        {literal}
                        <script type="text/javascript">
                            var module_dir = '{/literal}{$module_dir|escape:'htmlall':'UTF-8'}{literal}';
                            var spmgsnipreview_star_active = '{/literal}{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}{literal}';
                            var spmgsnipreview_star_noactive = '{/literal}{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}{literal}';
                            var spmgsnipreview_star_half_active = '{/literal}{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}{literal}';

                            var spmgsnipreview_min_star_par = '{/literal}{$spmgsnipreviewmin_star_par|escape:'htmlall':'UTF-8'}{literal}';
                            var spmgsnipreview_max_star_par = '{/literal}{$spmgsnipreviewmax_star_par|escape:'htmlall':'UTF-8'}{literal}';
                        </script>
                        {/literal}



                        {literal}
                        <script type="text/javascript">
                            {/literal}{if $spmgsnipreviewis17 == 1}{literal}document.addEventListener("DOMContentLoaded", function(event) { {/literal}{/if}{literal}
                                jQuery(document).ready(init_rating);

                                $("#idTab777-my-click").click(function() {
                                    $('.total-info-tool-product-page .btn-spmgsnipreview').parent().hide();
                                });

                                {/literal}{if $spmgsnipreviewis17 == 1}{literal}}); {/literal}{/if}{literal}






                        </script>
                        {/literal}


                        <div id="add-review-block" style="display: none">
                            {if $spmgsnipreviewid_customer == 0 && $spmgsnipreviewwhocanadd == 'reg'}
                                <div class="no-registered">
                                    <div class="text-no-reg">
                                        {l s='You cannot post a review because you are not logged as a customer' mod='spmgsnipreview'}
                                    </div>
                                    <br/>
                                    <div class="no-reg-button">
                                        <a href="{$spmgsnipreviewm_acc nofilter}"
                                           class="btn-spmgsnipreview btn-primary-spmgsnipreview" >{l s='Log in / sign up' mod='spmgsnipreview'}</a>
                                    </div>

                                </div>
                            {*{elseif $spmgsnipreviewis_buy == 0 && $spmgsnipreviewwhocanadd == 'buy'}
                                <div class="no-registered">
                                    <div class="text-no-reg">
                                        {l s='Only users who already bought the product can add review.' mod='spmgsnipreview'}
                                    </div>
                                </div>*}
                            {else}

                            {if $spmgsnipreviewis_add == 1}

                                <div class="advertise-text-review">
                                    {l s='You have already add review for this product' mod='spmgsnipreview'}
                                </div>

                            {else}


                            {if $spmgsnipreviewwhocanadd == 'buy'}

                                <div class="text-no-reg alert alert-warning">
                                    {l s='Only customers that have purchased a product can give a review.' mod='spmgsnipreview'}
                                </div>
                            {/if}


                                {* voucher suggestions *}
                            {if $spmgsnipreviewvis_on && $spmgsnipreviewis_show_voucher == 1}
                                <div class="advertise-text-review">
                                <span>
                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}"
                                         alt="{l s='Write a review and get voucher for discount' mod='spmgsnipreview'}" />
                                    {l s='Write a review and get voucher for discount' mod='spmgsnipreview'}
                                    <b>{$spmgsnipreviewdiscount|escape:'htmlall':'UTF-8'}</b> {if $spmgsnipreviewvaluta != '%'}<b>({if $spmgsnipreviewtax == 1}{l s='Tax Included' mod='spmgsnipreview'}{else}{l s='Tax Excluded' mod='spmgsnipreview'}{/if})</b>{/if}
                                    {if $spmgsnipreviewis_show_min == 1 && $spmgsnipreviewisminamount}
                                        <b>({l s='Minimum amount' mod='spmgsnipreview'} : {$spmgsnipreviewminamount|escape:'htmlall':'UTF-8'} {$spmgsnipreviewcurtxt|escape:'htmlall':'UTF-8'})</b>
                                    {/if}
                                    ,
                                    {l s='valid for' mod='spmgsnipreview'} {$spmgsnipreviewsdvvalid|escape:'htmlall':'UTF-8'} {$spmgsnipreviewdays|escape:'htmlall':'UTF-8'}
                                </span>
                                </div>
                            {/if}


                            {if $spmgsnipreviewvis_onfb == 1 && $spmgsnipreviewis_show_fb_voucher == 1}
                            <br/>
                                <div class="advertise-text-review" id="facebook-share-review-block">
                                <span>
                                    <img width="16" height="16" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/btn/ico-facebook.png"
                                         alt="{l s='Share your review on Facebook and get voucher for discount' mod='spmgsnipreview'}"/>
                                    {l s='Share your review on Facebook and get voucher for discount' mod='spmgsnipreview'}
                                    <b>{$spmgsnipreviewdiscountfb|escape:'htmlall':'UTF-8'}</b> {if $spmgsnipreviewvalutafb != '%'}<b>({if $spmgsnipreviewtaxfb == 1}{l s='Tax Included' mod='spmgsnipreview'}{else}{l s='Tax Excluded' mod='spmgsnipreview'}{/if})</b>{/if}

                                    {if $spmgsnipreviewis_show_minfb == 1 && $spmgsnipreviewisminamountfb}
                                        <b>({l s='Minimum amount' mod='spmgsnipreview'} : {$spmgsnipreviewminamountfb|escape:'htmlall':'UTF-8'} {$spmgsnipreviewcurtxtfb|escape:'htmlall':'UTF-8'})</b>
                                    {/if}

                                    ,
                                    {l s='valid for' mod='spmgsnipreview'} {$spmgsnipreviewsdvvalidfb|escape:'htmlall':'UTF-8'} {$spmgsnipreviewdaysfb|escape:'htmlall':'UTF-8'}
                                </span>
                                </div>
                            {/if}
                                {* voucher suggestions *}








                                {** **}
                                <div id="add-review-form-review">
                                    <form method="post" action="{$spmgsnipreviewupload nofilter}" enctype="multipart/form-data"
                                          id="add_review_item_form" name="add_review_item_form">

                                        <input type="hidden" name="action" value="add" />
                                        <input type="hidden" name="id_product" id="id_product_spmgsnipreview" value="{$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'}" />
                                        <input type="hidden" name="id_category_spmgsnipreview" id="id_category_spmgsnipreview" value="{$spmgsnipreviewid_category|escape:'htmlall':'UTF-8'}" />
                                        <input type="hidden" name="id_customer" value="{$spmgsnipreviewid_customer|escape:'htmlall':'UTF-8'}" />


                                        <div class="title-rev">
                                            <div class="title-form-text-left">
                                                <b>{l s='Write Your Review' mod='spmgsnipreview'}</b>
                                            </div>

                                            <input type="button" value="{l s='close' mod='spmgsnipreview'}" class="btn-spmgsnipreview btn-primary-spmgsnipreview title-form-text-right" onclick="show_form_review(0)">
                                            <div class="clear-spmgsnipreview"></div>
                                        </div>

                                        <div id="body-add-review-form-review">


                                            {if $spmgsnipreviewratings_on == 1}
                                                <br/>

                                                {if $spmgsnipreviewcriterions|@count > 0}

                                                    {foreach from=$spmgsnipreviewcriterions item='criterion'}

                                                        <label for="rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}"
                                                               class="float-left">{$criterion.name|escape:'htmlall':'UTF-8'}<sup class="required">*</sup></label>

                                                        <div class="rat rating-stars-dynamic">
                                                        <span onmouseout="read_rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}');">

                                                            <img  onmouseover="_rating_efect_rev(1,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(1,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',1); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true; "
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="1" id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_1" />

                                                            <img  onmouseover="_rating_efect_rev(2,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(2,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',2); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true;"
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="2" id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_2" />

                                                            <img  onmouseover="_rating_efect_rev(3,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(3,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',3); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true;"
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="3"  id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_3" />
                                                            <img  onmouseover="_rating_efect_rev(4,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(4,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',4); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true;"
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="4"  id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_4" />
                                                            <img  onmouseover="_rating_efect_rev(5,0,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onmouseout="_rating_efect_rev(5,1,'rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}')"
                                                                  onclick = "rating_review_shop('rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}',5); rating_checked{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}=true;"
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="5"  id="img_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}_5" />
                                                        </span>
                                                            {if strlen($criterion.description)>0}
                                                                <div class="clear"></div>
                                                                <div class="tip-criterion-description">{$criterion.description nofilter}</div>
                                                            {/if}
                                                        </div>
                                                        <input type="hidden" id="rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}" name="rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}" value="0"/>
                                                        <div class="clr"></div>
                                                        <div class="errorTxtAdd" id="error_rat_rel{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}"></div>

                                                    {/foreach}
                                                {else}
                                                    <label for="rat_rel" class="float-left">{l s='Rating' mod='spmgsnipreview'}<sup class="required">*</sup></label>

                                                    <div class="rat rating-stars-dynamic">
                                                        <span onmouseout="read_rating_review_shop('rat_rel');">
                                                            <img  onmouseover="_rating_efect_rev(1,0,'rat_rel')" onmouseout="_rating_efect_rev(1,1,'rat_rel')"
                                                                  onclick = "rating_review_shop('rat_rel',1); rating_checked=true; "
                                                                  src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                                                  alt="1"
                                                                  id="img_rat_rel_1" />
                                                            <img  onmouseover="_rating_efect_rev(2,0,'rat_rel')" onmouseout="_rating_efect_rev(2,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',2); rating_checked=true;" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="2"  id="img_rat_rel_2" />
                                                            <img  onmouseover="_rating_efect_rev(3,0,'rat_rel')" onmouseout="_rating_efect_rev(3,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',3); rating_checked=true;" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="3"  id="img_rat_rel_3" />
                                                            <img  onmouseover="_rating_efect_rev(4,0,'rat_rel')" onmouseout="_rating_efect_rev(4,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',4); rating_checked=true;" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="4"  id="img_rat_rel_4" />
                                                            <img  onmouseover="_rating_efect_rev(5,0,'rat_rel')" onmouseout="_rating_efect_rev(5,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',5); rating_checked=true;" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="5"  id="img_rat_rel_5" />
                                                        </span>
                                                    </div>
                                                    <input type="hidden" id="rat_rel" name="rat_rel" value="0"/>
                                                    <div class="clr"></div>
                                                    <div class="errorTxtAdd" id="error_rat_rel"></div>
                                                {/if}
                                                <br/>
                                            {/if}

                                            <label for="name-review">{l s='Name' mod='spmgsnipreview'}<sup class="required">*</sup></label>
                                            <input type="text" name="name-review" id="name-review" value="{$spmgsnipreviewc_name|escape:'htmlall':'UTF-8'}"  onkeyup="check_inpNameReview();" onblur="check_inpNameReview();" />
                                            <div class="errorTxtAdd" id="error_name-review"></div>

                                            <label for="email-review">{l s='Email' mod='spmgsnipreview'}<sup class="required">*</sup></label>
                                            <input type="text" name="email-review" id="email-review" value="{$spmgsnipreviewc_email|escape:'htmlall':'UTF-8'}" onkeyup="check_inpEmailReview();" onblur="check_inpEmailReview();"  />
                                            <div id="error_email-review" class="errorTxtAdd"></div>

                                            {if $spmgsnipreviewis_avatarr == 1}
                                                <label for="avatar-review">{l s='Avatar' mod='spmgsnipreview'}</label>

                                                {if strlen($spmgsnipreviewc_avatar)>0}
                                                    <div class="avatar-block-rev-form">
                                                        <input type="radio" name="post_images" checked="" style="display: none">
                                                        <img src="{$spmgsnipreviewc_avatar|escape:'htmlall':'UTF-8'}" alt="{$spmgsnipreviewc_name|escape:'htmlall':'UTF-8'}" />
                                                    </div>
                                                {/if}

                                                <input type="file" name="avatar-review"
                                                       id="avatar-review"
                                                       class="testimonials-input"
                                                        />
                                                <div class="avatar-guid">
                                                    {l s='Allow formats' mod='spmgsnipreview'}: *.jpg; *.jpeg; *.png; *.gif.
                                                </div>
                                                <div class="errorTxtAdd" id="error_avatar-review"></div>
                                            {/if}



                                            {if $spmgsnipreviewtitle_on == 1}
                                                <label for="subject-review">{l s='Title' mod='spmgsnipreview'}<sup class="required">*</sup></label>
                                                <input type="text" name="subject-review" id="subject-review" onkeyup="check_inpSubjectReview();" onblur="check_inpSubjectReview();" />
                                                <div id="error_subject-review" class="errorTxtAdd"></div>
                                            {/if}

                                            {if $spmgsnipreviewtext_on == 1}
                                                <label for="text-review">{l s='Text' mod='spmgsnipreview'}<sup class="required">*</sup></label>
                                                <textarea id="text-review" name="text-review" cols="42" rows="7" onkeyup="check_inpTextReview();" onblur="check_inpTextReview();"></textarea>
                                                <div id="textarea_feedback"></div>
                                                <div id="error_text-review" class="errorTxtAdd"></div>
                                            {/if}


                                            {if $spmgsnipreviewis_filesr == 1}
                                                <label for="text-files">{l s='Files' mod='spmgsnipreview'}</label>
                                                <span class="file-upload-rev" id="file-upload-rev">
                                            <input type="file" name="files[]" multiple />
                                            <div class="progress-files-bar">
                                                <div class="progress-files"></div>
                                            </div>
                                            <div id="file-files-list"></div>
                                        </span>


                                                <div class="avatar-guid">
                                                    {l s='Maximum files to upload' mod='spmgsnipreview'} - <b>{$spmgsnipreviewruploadfiles|escape:'htmlall':'UTF-8'}</b><br/> {l s='Allow formats' mod='spmgsnipreview'}: *.jpg; *.jpeg; *.png; *.gif.
                                                </div>
                                                <div id="error_text-files" class="errorTxtAdd"></div>
                                            {/if}


                                            {* gdpr *}
                                            {hook h='displayGDPRConsent' mod='psgdpr' id_module=$id_module}
                                            {* gdpr *}

                                            {if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}
                                                <label for="inpCaptchaReview">{l s='Captcha' mod='spmgsnipreview'}<sup class="required">*</sup></label>
                                                <div class="clr"></div>
                                                <img width="100" height="26" class="float-left" id="secureCodReview" src="{$spmgsnipreviewcaptcha_url nofilter}" alt="Captcha"/>
                                                <input type="text" class="inpCaptchaReview float-left" id="inpCaptchaReview" size="6" name="captcha"
                                                       onkeyup="check_inpCaptchaReview();" onblur="check_inpCaptchaReview();"/>
                                                <div class="clr"></div>

                                                <div id="error_inpCaptchaReview" class="errorTxtAdd"></div>
                                            {/if}



                                        </div>

                                        <div id="footer-add-review-form-review">
                                            <button type="submit" value="{l s='Add review' mod='spmgsnipreview'}" class="btn btn-success">{l s='Add review' mod='spmgsnipreview'}</button>
                                            &nbsp;
                                            <button onclick="show_form_review(0)" value="{l s='Cancel' mod='spmgsnipreview'}" class="btn btn-danger">{l s='Cancel' mod='spmgsnipreview'}</button>
                                        </div>







                                    </form>
                                </div>


                            {literal}
                                <script type="text/javascript">


                                    // gdpr
                                    setTimeout(function() {
                                        $('#footer-add-review-form-review').find('button[type="submit"]').removeAttr('disabled');
                                    }, 1000);
                                    // gdpr


                                    function field_gdpr_change_spmgsnipreview(){
                                        // gdpr
                                        var gdpr_spmgsnipreview = $('#psgdpr_consent_checkbox_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal}');

                                        var is_gdpr_spmgsnipreview = 1;

                                        if(gdpr_spmgsnipreview.length>0){

                                            if(gdpr_spmgsnipreview.prop('checked') == true) {
                                                $('.gdpr_module_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal} .psgdpr_consent_message').removeClass('error-label');
                                            } else {
                                                $('.gdpr_module_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal} .psgdpr_consent_message').addClass('error-label');
                                                is_gdpr_spmgsnipreview = 0;
                                            }

                                            $('#psgdpr_consent_checkbox_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal}').on('click', function(){
                                                if(gdpr_spmgsnipreview.prop('checked') == true) {
                                                    $('.gdpr_module_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal} .psgdpr_consent_message').removeClass('error-label');
                                                } else {
                                                    $('.gdpr_module_{/literal}{$id_module|escape:'htmlall':'UTF-8'}{literal} .psgdpr_consent_message').addClass('error-label');
                                                }
                                            });

                                        }

                                        //gdpr

                                        return is_gdpr_spmgsnipreview;
                                    }
                                    

                                    {/literal}{if $spmgsnipreviewis17 == 1}{literal}
                                    var baseDir = '{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}';
                                    {/literal}{/if}{literal}


                                    {/literal}{if $spmgsnipreviewis_filesr == 1}{literal}

                                    var file_upload_url_spmgsnipreview = '{/literal}{$spmgsnipreviewupload nofilter}{literal}';
                                    var file_max_files_spmgsnipreview = {/literal}{$spmgsnipreviewruploadfiles|escape:'htmlall':'UTF-8'}{literal};
                                    var file_max_message_spmgsnipreview = '{/literal}{$spmgsnipreviewptc_msg13_1|escape:'htmlall':'UTF-8'}{literal} '+file_max_files_spmgsnipreview+' {/literal}{$spmgsnipreviewptc_msg13_2|escape:'htmlall':'UTF-8'}{literal}';
                                    var file_path_upload_url_spmgsnipreview = baseDir + '{/literal}{$spmgsnipreviewfpath|escape:'htmlall':'UTF-8'}{literal}tmp/';

                                    {/literal}{/if}{literal}


                                    var text_min = {/literal}{$spmgsnipreviewrminc|escape:'htmlall':'UTF-8'}{literal};

                                    {/literal}{if $spmgsnipreviewis17 == 1}{literal}document.addEventListener("DOMContentLoaded", function(event) { {/literal}{/if}{literal}
                                        $(document).ready(function(){


                                            {/literal}{if $spmgsnipreviewtext_on == 1}{literal}
                                            $('#textarea_feedback').html($('#text-review').val().length + ' {/literal}{$spmgsnipreviewptc_msg11|escape:'htmlall':'UTF-8'}{literal}. {/literal}{$spmgsnipreviewptc_msg12|escape:'htmlall':'UTF-8'}{literal} '+text_min+' {/literal}{$spmgsnipreviewptc_msg11|escape:'htmlall':'UTF-8'}{literal}');

                                            $('#text-review').keyup(function() {
                                                var text_length_val = trim(document.getElementById('text-review').value);
                                                var text_length = text_length_val.length;

                                                if(text_length<text_min)
                                                    $('#textarea_feedback').css('color','red');
                                                else
                                                    $('#textarea_feedback').css('color','green');

                                                $('#textarea_feedback').html(text_length + ' {/literal}{$spmgsnipreviewptc_msg11|escape:'htmlall':'UTF-8'}{literal}. {/literal}{$spmgsnipreviewptc_msg12|escape:'htmlall':'UTF-8'}{literal} '+text_min+' {/literal}{$spmgsnipreviewptc_msg11|escape:'htmlall':'UTF-8'}{literal}');
                                            });

                                            {/literal}{/if}{literal}

                                            /* clear form fields */

                                            {/literal}{if $spmgsnipreviewratings_on == 1}{literal}

                                            {/literal}{if $spmgsnipreviewcriterions|@count > 0}

                                            {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                                            $('#rat_rel{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}').val(0);

                                            {/literal}{/foreach}

                                            {else}{literal}

                                            $('#rat_rel').val(0);

                                            {/literal}{/if}{literal}

                                            {/literal}{/if}{literal}

                                            {/literal}{if $spmgsnipreviewid_customer == 0}{literal}
                                            $('#name-review').val('');
                                            $('#email-review').val('');
                                            {/literal}{/if}{literal}

                                            {/literal}{if $spmgsnipreviewtitle_on == 1}{literal}
                                            $('#subject-review').val('');
                                            {/literal}{/if}{literal}
                                            {/literal}{if $spmgsnipreviewtext_on == 1}{literal}
                                            $('#text-review').val('');
                                            {/literal}{/if}{literal}
                                            {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}
                                            $('#inpCaptchaReview').val('');
                                            {/literal}{/if}{literal}

                                            /* clear form fields */
                                        });

                                        {/literal}{if $spmgsnipreviewis17 == 1}{literal}}); {/literal}{/if}{literal}




                                    {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}
                                    function check_inpCaptchaReview()
                                    {

                                        var inpCaptchaReview = trim(document.getElementById('inpCaptchaReview').value);

                                        if (inpCaptchaReview.length != 6)
                                        {
                                            field_state_change('inpCaptchaReview','failed', '{/literal}{$spmgsnipreviewptc_msg1|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        }
                                        field_state_change('inpCaptchaReview','success', '');
                                        return true;
                                    }
                                    {/literal}{/if}{literal}


                                    {/literal}{if $spmgsnipreviewtext_on == 1}{literal}
                                    function check_inpTextReview()
                                    {

                                        var text_review = trim(document.getElementById('text-review').value);

                                        if (text_review.length == 0 || text_review.length<text_min)
                                        {
                                            field_state_change('text-review','failed', '{/literal}{$spmgsnipreviewptc_msg2|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        }
                                        field_state_change('text-review','success', '');
                                        return true;
                                    }
                                    {/literal}{/if}{literal}


                                    function check_inpNameReview()
                                    {

                                        var name_review = trim(document.getElementById('name-review').value);

                                        if (name_review.length == 0)
                                        {
                                            field_state_change('name-review','failed', '{/literal}{$spmgsnipreviewptc_msg3|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        }
                                        field_state_change('name-review','success', '');
                                        return true;
                                    }


                                    function check_inpEmailReview()
                                    {

                                        var email_review = trim(document.getElementById('email-review').value);

                                        if (email_review.length == 0)
                                        {
                                            field_state_change('email-review','failed', '{/literal}{$spmgsnipreviewptc_msg4|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        }
                                        field_state_change('email-review','success', '');
                                        return true;
                                    }


                                    {/literal}{if $spmgsnipreviewtitle_on == 1}{literal}
                                    function check_inpSubjectReview()
                                    {

                                        var subject_review = trim(document.getElementById('subject-review').value);

                                        if (subject_review.length == 0)
                                        {
                                            field_state_change('subject-review','failed', '{/literal}{$spmgsnipreviewptc_msg5|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        }
                                        field_state_change('subject-review','success', '');
                                        return true;
                                    }
                                    {/literal}{/if}{literal}





                                    {/literal}{if $spmgsnipreviewratings_on == 1}{literal}



                                    {/literal}{if $spmgsnipreviewcriterions|@count > 0}

                                    {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                                    var rating_checked{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal} = false;

                                    {/literal}{/foreach}

                                    {else}{literal}

                                    var rating_checked = false;

                                    {/literal}{/if}{literal}





                                    {/literal}{if $spmgsnipreviewcriterions|@count > 0}



                                    {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                                    function check_inpRatingReview{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}()
                                    {

                                        if(!rating_checked{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}){
                                            field_state_change('rat_rel{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}','failed', '{/literal}{$spmgsnipreviewptc_msg6|escape:'htmlall':'UTF-8'} {$criterion.name|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        }
                                        field_state_change('rat_rel{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}','success', '');
                                        return true;


                                    }
                                    {/literal}{/foreach}

                                    {else}{literal}
                                    function check_inpRatingReview()
                                    {
                                        if(!rating_checked){
                                            field_state_change('rat_rel','failed', '{/literal}{$spmgsnipreviewptc_msg7|escape:'htmlall':'UTF-8'}{literal}');
                                            return false;
                                        }
                                        field_state_change('rat_rel', 'success', '');
                                        return true;

                                    }


                                    {/literal}{/if}{literal}

                                    {/literal}{/if}{literal}


                                    {/literal}{if $spmgsnipreviewis17 == 1}{literal}document.addEventListener("DOMContentLoaded", function(event) { {/literal}{/if}{literal}
                                        $(document).ready(function (e) {
                                            $("#add_review_item_form").on('submit',(function(e) {



                                                {/literal}{if $spmgsnipreviewis_avatarr == 1}{literal}
                                                field_state_change('avatar-review','success', '');
                                                {/literal}{/if}{literal}




                                                {/literal}{if $spmgsnipreviewratings_on == 1}{literal}
                                                {/literal}{if $spmgsnipreviewcriterions|@count > 0}

                                                {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                                                var is_rating{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal} = check_inpRatingReview{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal}();

                                                {/literal}{/foreach}{literal}

                                                {/literal}{else}{literal}

                                                var is_rating = check_inpRatingReview();

                                                {/literal}{/if}{literal}
                                                {/literal}{/if}{literal}

                                                {/literal}{if $spmgsnipreviewtitle_on == 1}{literal}
                                                var is_subject = check_inpSubjectReview();
                                                {/literal}{/if}{literal}

                                                var is_name = check_inpNameReview();
                                                var is_email = check_inpEmailReview();

                                                {/literal}{if $spmgsnipreviewtext_on == 1}{literal}
                                                var is_text =  check_inpTextReview();
                                                {/literal}{/if}{literal}
                                                {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}
                                                var is_captcha = check_inpCaptchaReview();
                                                {/literal}{/if}{literal}


                                                // gdpr
                                                var is_gdpr_spmgsnipreview = field_gdpr_change_spmgsnipreview();


                                                if(is_gdpr_spmgsnipreview && //gdpr
                                                        {/literal}{if $spmgsnipreviewratings_on == 1}{literal}
                                                        {/literal}{if $spmgsnipreviewcriterions|@count > 0}

                                                        {foreach from=$spmgsnipreviewcriterions item='criterion'}{literal}

                                                is_rating{/literal}{$criterion.id_spmgsnipreview_review_criterion|escape:'htmlall':'UTF-8'}{literal} &&

                                                {/literal}{/foreach}{literal}

                                                {/literal}{else}{literal}

                                                is_rating &&

                                                {/literal}{/if}{literal}
                                                {/literal}{/if}{literal}

                                                {/literal}{if $spmgsnipreviewtitle_on == 1}{literal}
                                                is_subject &&
                                                {/literal}{/if}{literal}
                                                is_name &&
                                                is_email &&
                                                {/literal}{if $spmgsnipreviewtext_on == 1}{literal}
                                                is_text &&
                                                {/literal}{/if}{literal}
                                                {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}
                                                is_captcha &&
                                                {/literal}{/if}{literal}
                                                true
                                                ){

                                                    $('#reviews-list').css('opacity',0.5);
                                                    $('#add-review-form-review').css('opacity',0.5);
                                                    $('#footer-add-review-form-review button').attr('disabled','disabled');

                                                    e.preventDefault();
                                                    $.ajax({
                                                        url: '{/literal}{$spmgsnipreviewreviews_url nofilter}{literal}',
                                                        type: "POST",
                                                        data:  new FormData(this),
                                                        contentType: false,
                                                        cache: false,
                                                        processData:false,
                                                        dataType: 'json',
                                                        success: function(data)
                                                        {


                                                            $('#reviews-list').css('opacity',1);
                                                            $('#add-review-form-review').css('opacity',1);

                                                            if (data.status == 'success') {




                                                                $('#gsniprev-list').html('');
                                                                var paging = $('#gsniprev-list').prepend(data.params.content);
                                                                $(paging).hide();
                                                                $(paging).fadeIn('slow');

                                                                $('#gsniprev-nav').html('');
                                                                var paging = $('#gsniprev-nav').prepend(data.params.paging);
                                                                $(paging).hide();
                                                                $(paging).fadeIn('slow');


                                                                var count_review = data.params.count_reviews;

                                                                $('#count-review-tab').html('');
                                                                $('#count-review-tab').html('('+count_review+')');





                                                                {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}

                                                                var count = Math.random();
                                                                document.getElementById('secureCodReview').src = "";
                                                                document.getElementById('secureCodReview').src = "{/literal}{$spmgsnipreviewcaptcha_url nofilter}{literal}?re=" + count;
                                                                $('#inpCaptchaReview').val('');

                                                                {/literal}{/if}{literal}

                                                                jQuery(document).ready(init_rating);



                                                                $('.advertise-text-review').css('opacity','0.2');
                                                                $('#add-review-block').css('opacity','0.2');





                                                                var voucher_html_suggestion = data.params.voucher_html_suggestion;



                                                                {/literal}{if $spmgsnipreviewvis_on == 1 && $spmgsnipreviewis_show_voucher}{literal}
                                                                /* voucher */

                                                                var voucher_html = data.params.voucher_html;



                                                                if ($('div#fb-con-wrapper').length == 0)
                                                                {
                                                                    conwrapper = '<div id="fb-con-wrapper" class="voucher-data"><\/div>';
                                                                    $('body').append(conwrapper);
                                                                } else {
                                                                    $('#fb-con-wrapper').html('');
                                                                }

                                                                if ($('div#fb-con').length == 0)
                                                                {
                                                                    condom = '<div id="fb-con"><\/div>';
                                                                    $('body').append(condom);
                                                                }



                                                                $('div#fb-con').fadeIn(function(){

                                                                    $(this).css('filter', 'alpha(opacity=70)');
                                                                    $(this).bind('click dblclick', function(){
                                                                        $('div#fb-con-wrapper').hide();
                                                                        $(this).fadeOut();

                                                                        showSocialSuggestion(voucher_html_suggestion);


                                                                    });
                                                                });



                                                                $('div#fb-con-wrapper').html('<a id="button-close" style="display: inline;"><\/a>'+voucher_html).fadeIn();



                                                                $("a#button-close").click(function() {
                                                                    $('div#fb-con-wrapper').hide();
                                                                    $('div#fb-con').fadeOut();
                                                                    showSocialSuggestion(voucher_html_suggestion);


                                                                });



                                                                /* voucher */
                                                                {/literal}{else}{literal}


                                                                showSocialSuggestion(voucher_html_suggestion);


                                                                {/literal}{/if}{literal}




                                                            } else {

                                                                var error_type = data.params.error_type;
                                                                $('#footer-add-review-form-review button').removeAttr('disabled');


                                                                {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}
                                                                if(error_type == 3){
                                                                    field_state_change('inpCaptchaReview','failed', '{/literal}{$spmgsnipreviewptc_msg8|escape:'htmlall':'UTF-8'}{literal}');

                                                                    {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}
                                                                    var count = Math.random();
                                                                    document.getElementById('secureCodReview').src = "";
                                                                    document.getElementById('secureCodReview').src = "{/literal}{$spmgsnipreviewcaptcha_url nofilter}{literal}?re=" + count;
                                                                    $('#inpCaptchaReview').val('');
                                                                    {/literal}{/if}{literal}

                                                                    return false;

                                                                }
                                                                {/literal}{/if}{literal}

                                                                if(error_type == 2){
                                                                    field_state_change('email-review','failed', '{/literal}{$spmgsnipreviewptc_msg9|escape:'htmlall':'UTF-8'}{literal}');
                                                                    field_state_change('inpCaptchaReview','failed', '{/literal}{$spmgsnipreviewptc_msg1|escape:'htmlall':'UTF-8'}{literal}');

                                                                    {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}
                                                                    var count = Math.random();
                                                                    document.getElementById('secureCodReview').src = "";
                                                                    document.getElementById('secureCodReview').src = "{/literal}{$spmgsnipreviewcaptcha_url nofilter}{literal}?re=" + count;
                                                                    $('#inpCaptchaReview').val('');
                                                                    {/literal}{/if}{literal}

                                                                    return false;
                                                                }

                                                                if(error_type == 1){
                                                                    alert("{/literal}{$spmgsnipreviewptc_msg10|escape:'htmlall':'UTF-8'}{literal}");
                                                                    window.location.reload();
                                                                }

                                                                if(error_type == 8){
                                                                    field_state_change('avatar-review','failed', '{/literal}{$spmgsnipreviewava_msg8|escape:'htmlall':'UTF-8'}{literal}');
                                                                    return false;
                                                                } else if(error_type == 9){
                                                                    field_state_change('avatar-review','failed', '{/literal}{$spmgsnipreviewava_msg9|escape:'htmlall':'UTF-8'}{literal}');
                                                                    return false;
                                                                } else if(error_type == 10){
                                                                    field_state_change_store('email-review','failed', '{/literal}{$spmgsnipreviewptc_msg14|escape:'htmlall':'UTF-8'}{literal}');
                                                                    field_state_change_store('name-review','failed', '{/literal}{$spmgsnipreviewptc_msg14|escape:'htmlall':'UTF-8'}{literal}');
                                                                    return false;
                                                                }

                                                                {/literal}{if $spmgsnipreviewis_captcha == 1 && $spmgsnipreviewid_customer == 0}{literal}
                                                                var count = Math.random();
                                                                document.getElementById('secureCodReview').src = "";
                                                                document.getElementById('secureCodReview').src = "{/literal}{$spmgsnipreviewcaptcha_url nofilter}{literal}?re=" + count;
                                                                $('#inpCaptchaReview').val('');
                                                                {/literal}{/if}{literal}


                                                            }

                                                        }
                                                    });





                                                } else {
                                                    return false;
                                                }

                                            }));

                                        });
                                        {/literal}{if $spmgsnipreviewis17 == 1}{literal}}); {/literal}{/if}{literal}



                                    //}


                                    function showSocialSuggestion(voucher_html){
                                        {/literal}{if $spmgsnipreviewvis_onfb == 1 && $spmgsnipreviewis_show_fb_voucher == 1}{literal}
                                        if ($('div#fb-con-wrapper').length == 0)
                                        {
                                            conwrapper = '<div id="fb-con-wrapper"><\/div>';
                                            $('body').append(conwrapper);
                                        } else {
                                            $('#fb-con-wrapper').html('');
                                        }

                                        if ($('div#fb-con').length == 0)
                                        {
                                            condom = '<div id="fb-con"><\/div>';
                                            $('body').append(condom);
                                        }

                                        $('div#fb-con').fadeIn(function(){

                                            $(this).css('filter', 'alpha(opacity=70)');
                                            $(this).bind('click dblclick', function(){
                                                $('div#fb-con-wrapper').hide();
                                                $(this).fadeOut();
                                                window.location.reload();
                                            });
                                        });

                                        $('div#fb-con-wrapper').html('<a id="button-close" style="display: inline;"><\/a>'+voucher_html).fadeIn();

                                        $("a#button-close").click(function() {
                                            $('div#fb-con-wrapper').hide();
                                            $('div#fb-con').fadeOut();
                                            window.location.reload();
                                        });
                                        {/literal}{else}{literal}
                                        window.location.reload();
                                        {/literal}{/if}{literal}
                                    }


                                </script>
                            {/literal}

                            {/if}



                            {/if}

                        </div>








                        <div class="row-custom total-info-tool-product-page">
                            <div class="col-sm-5-custom first-block-ti">



                                {*{if $spmgsnipreviewis_add == 0}
                                    <span class="spr-summary-actions">

                                          <a class="btn-spmgsnipreview btn-primary-spmgsnipreview" href="javascript:void(0)"  onclick="show_form_review(1)">
                                            <span>
                                                <i class="icon-pencil"></i>
                                                {l s='Write a Review' mod='spmgsnipreview'}
                                            </span>
                                          </a>

                                      </span>
                                {/if}*}


                 <span class="spr-starrating spr-summary-starrating">

                                    {*<b class="total-rating-review">{l s='Total rating' mod='spmgsnipreview'}:</b>*}


                                    {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1 && $spmgsnipreviewhooktodisplay == "none"}<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">{/if}
                                        {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1 && $spmgsnipreviewhooktodisplay == "none"}
                                            <meta content="1" itemprop="worstRating">
                                            <meta content="{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}" itemprop="ratingCount">
                                        {/if}


                                    <b class="spr-summary-actions-togglereviews gsniprev-block-ratings-text">
                                        {l s='Based on' mod='spmgsnipreview'} <span class="font-weight-bold" {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1 && $spmgsnipreviewhooktodisplay == "none"}itemprop="reviewCount"{/if}>{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}</span> {$spmgsnipreviewtext_reviews|escape:'htmlall':'UTF-8'}
                                    </b>



                    -

                    <span>
                    {assign var='test_rating' value=$spmgsnipreviewavg_rating}
                        {section name=ratid loop=5}

                            {if $smarty.section.ratid.index < $spmgsnipreviewavg_rating}

                                {if $test_rating <= $spmgsnipreviewmax_star_par && $test_rating >= $spmgsnipreviewmin_star_par}
                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"
                                         alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                  {else}
                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}"
                                         alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                {/if}

                              {else}
                                  <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"
                                       alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                            {/if}
                            {$test_rating = $test_rating - 1}
                        {/section}
                    </span>

                    <span class="gsniprev-block-ratings-text">
                        <span {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1 && $spmgsnipreviewhooktodisplay == "none"}itemprop="ratingValue"{/if}>{$spmgsnipreviewavg_decimal|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1 && $spmgsnipreviewhooktodisplay == "none"}itemprop="bestRating"{/if}>5</span>
                    </span>

                    {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1 && $spmgsnipreviewhooktodisplay == "none"}</div>{/if}

                </span>





                            </div>
                            <div class="col-sm-6-custom b-search-items">

                                <form onsubmit="return false;" method="post" action="#">

                                    <fieldset>
                                        <input type="submit" value="{l s='go' mod='spmgsnipreview'}" class="button_mini_custom" onclick="go_page_spmgsnipreviewr(0,'productpagereviews','',1,{$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'})">
                                        <input type="text" class="txt" name="searchr" id="searchr"
                                               onfocus="{literal}if(this.value == '{/literal}{l s='Search' mod='spmgsnipreview'}{literal}') {this.value='';};{/literal}"
                                               onblur="{literal}if(this.value == '') {this.value='{/literal}{l s='Search' mod='spmgsnipreview'}{literal}';};{/literal}"
                                               value="{l s='Search' mod='spmgsnipreview'}" />

                                        <a rel="nofollow" href="javascript:void(0)" id="clear-search-reviews" class="clear-search-items display-none"
                                           onclick="go_page_spmgsnipreviewr(0,'productpagereviews','','',{$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'})"
                                                >
                                            {l s='Clear search' mod='spmgsnipreview'}
                                        </a>


                                    </fieldset>
                                </form>


                            </div>

                        </div>





                        {if $spmgsnipreviewis_filterp == 1}
                            <!-- filters and ratings -->

                            <div class="spr-header spr-summary row-custom">



                                <div class="col-sm-12-custom">

                                    <div class="row-custom filter-reviews-spmgsnipreview product-reviews-filter-block">

                                        <div class="col-sm-1-custom">
                                            <b class="filter-txt-items-block">{l s='Filter' mod='spmgsnipreview'}:</b>
                                        </div>
                                        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 5}active-items-block{/if}">
                                            {if $spmgsnipreviewfive>0}
                                            <a rel="nofollow"

                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',5,'{if $spmgsnipreviewis_search == 1}1{/if}',{$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'})"
                                               href="javascript:void(0)"
                                               id="reviews-rating-5"

                                                    >
                                                {/if}
                                                {section name="test" loop=5}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                                                {/section}
                                                <span class="count-items-block {if $spmgsnipreviewfive==0}text-decoration-none{/if}">({$spmgsnipreviewfive|escape:'htmlall':'UTF-8'})</span>
                                                {if $spmgsnipreviewfive>0}
                                            </a>
                                            {/if}
                                        </div>
                                        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 4}active-items-block{/if}">
                                            {if $spmgsnipreviewfour>0}
                                            <a rel="nofollow"


                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',4,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'})"
                                               href="javascript:void(0)"
                                               id="reviews-rating-4"

                                                    >
                                                {/if}
                                                {section name="test" loop=4}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                                                {/section}
                                                {section name="test" loop=1}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                                                {/section}

                                                <span class="count-items-block {if $spmgsnipreviewfour==0}text-decoration-none{/if}">({$spmgsnipreviewfour|escape:'htmlall':'UTF-8'})</span>
                                                {if $spmgsnipreviewfour>0}
                                            </a>
                                            {/if}
                                        </div>
                                        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 3}active-items-block{/if}">
                                            {if $spmgsnipreviewthree>0}
                                            <a rel="nofollow"

                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',3,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'})"
                                               href="javascript:void(0)"
                                               id="reviews-rating-3"

                                                    >
                                                {/if}
                                                {section name="test" loop=3}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                                                {/section}
                                                {section name="test" loop=2}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" />
                                                {/section}
                                                <span class="count-items-block {if $spmgsnipreviewthree==0}text-decoration-none{/if}">({$spmgsnipreviewthree|escape:'htmlall':'UTF-8'})</span>
                                                {if $spmgsnipreviewthree>0}
                                            </a>
                                            {/if}
                                        </div>
                                        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 2}active-items-block{/if}">
                                            {if $spmgsnipreviewtwo>0}
                                            <a rel="nofollow"

                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',2,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'})"
                                               href="javascript:void(0)"
                                               id="reviews-rating-2"
                                                    >
                                                {/if}
                                                {section name="test" loop=2}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                                                {/section}
                                                {section name="test" loop=3}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                                                {/section}

                                                <span class="count-items-block {if $spmgsnipreviewtwo==0}text-decoration-none{/if}">({$spmgsnipreviewtwo|escape:'htmlall':'UTF-8'})</span>
                                                {if $spmgsnipreviewtwo>0}
                                            </a>
                                            {/if}
                                        </div>
                                        <div class="col-sm-2-custom {if isset($spmgsnipreviewfrat) && $spmgsnipreviewfrat == 1}active-items-block{/if}">
                                            {if $spmgsnipreviewone>0}
                                            <a rel="nofollow"

                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',1,'{if $spmgsnipreviewis_search == 1}1{/if}', {$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'})"
                                               href="javascript:void(0)"
                                               id="reviews-rating-1"
                                                    >
                                                {/if}
                                                {section name="test" loop=1}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                                                {/section}
                                                {section name="test" loop=4}
                                                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}"/>
                                                {/section}
                                                <span class="count-items-block {if $spmgsnipreviewone==0}text-decoration-none{/if}">({$spmgsnipreviewone|escape:'htmlall':'UTF-8'})</span>
                                                {if $spmgsnipreviewone>0}
                                            </a>
                                            {/if}
                                        </div>


                                        <div class="col-sm-1-custom">
                                            <a rel="nofollow" href="javascript:void(0)"
                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews','','',{$spmgsnipreviewid_product|escape:'htmlall':'UTF-8'})"
                                               id="clear-rating-reviews" class="reset-items-block display-none">
                                                <i class="fa fa-refresh"></i>{l s='Reset' mod='spmgsnipreview'}
                                            </a>
                                        </div>



                                    </div>

                                </div>
                                <div class="clear"></div>
                            </div>



                            <!-- filters and ratings -->
                        {/if}

                        {if $spmgsnipreviewis_search == 1}
                            <h3 class="search-result-item">{l s='Results for' mod='spmgsnipreview'} <b>"{$spmgsnipreviewsearch|escape:'quotes':'UTF-8'}"</b></h3>
                            <br/>
                        {/if}


                        {if $reviews && $spmgsnipreviewis_sortf ==1}
                            <div class="search-result-item float-right">

                                <b>
                                    {l s='Sort by' mod='spmgsnipreview'}:
                                </b>
                                <select id="select_spmgsnipreview_sort" data-action="productpagereviews">
                                    <option value="time_add:desc" selected="selected">--</option>
                                    {if $spmgsnipreviewratings_on == 1}
                                        <option value="rating:asc">Rating: Lowest first</option>
                                        <option value="rating:desc">Rating: Highest first</option>
                                    {/if}
                                    <option value="time_add:asc">Date: Oldest first</option>
                                    <option value="time_add:desc">Date: Newest first</option>
                                </select>

                            </div>
                            <div class="clear"></div>
                            <br/>
                        {/if}




                        {if $reviews}
                            <div class="spr-reviews" id="reviews-list">


                                {include file="module:spmgsnipreview/views/templates/front/list_reviews.tpl"}



                            </div>


                            {*!! no smarty changes |escape:'htmlall':'UTF-8' !!*}
                            <div id="page_navr">{$paging nofilter}</div>
                            {*!! no smarty changes |escape:'htmlall':'UTF-8' !!*}



                        {if $spmgsnipreviewgp > 1}
                        {literal}
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    spmgsnipreview_open_tab();
                                });
                            </script>
                        {/literal}
                        {/if}


                        {else}


                            <div class="advertise-text-review advertise-text-review-text-align" id="no-customers-reviews">
                                {l s='No reviews for the product' mod='spmgsnipreview'}

                                {if $spmgsnipreviewcount_reviews == 0}
                                    <br/><br/>
                                    <a href="javascript:void(0)" class="btn-spmgsnipreview btn-primary-spmgsnipreview" onclick="show_form_review(1)">
                                        <b id="button-addreview-blockreview">
                                            <i class="icon-pencil"></i>&nbsp;{l s='Be the first to write your review !' mod='spmgsnipreview'}
                                        </b>
                                    </a>
                                {/if}
                            </div>




                        {/if}

                    </div>

                </div></div>

            <div class="clear-spmgsnipreview"></div>
            <!-- reviews template -->



        </div>



    {/if}

{/if}







{if $spmgsnipreviewis17 == 1}

    {if $spmgsnipreviewrvis_on == 1}

        {if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}

            {if $spmgsnipreviewhooktodisplay == "product_footer"}

                <div class="clear-spmgsnipreview"></div>

                <div class="{if $spmgsnipreviewis16 == 1}gsniprev-block-16{else}gsniprev-block{/if}">
                    <b class="title-rating-block">
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{l s='Total Rating' mod='spmgsnipreview'}" />&nbsp;{l s='Total Rating' mod='spmgsnipreview'}</b><span class="ratings-block-punct">:</span>
                    <br/><br/>

                    {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">{/if}
                        {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}
                            <meta content="1" itemprop="worstRating">
                            <meta content="{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}" itemprop="ratingCount">
                        {/if}

                        <div class="rating">{$spmgsnipreviewavg_rating|escape:'htmlall':'UTF-8'}</div>
                        <div class="gsniprev-block-reviews-text">
                            <span {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}itemprop="ratingValue"{/if}>{$spmgsnipreviewavg_decimal|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}itemprop="bestRating"{/if}>5</span> - <span id="count_review_block" {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}itemprop="reviewCount"{/if}>{$spmgsnipreviewcount_reviews|escape:'htmlall':'UTF-8'}</span> <span id="reviews_text_block">{$spmgsnipreviewtext_reviews|escape:'htmlall':'UTF-8'}</span>
                        </div>
                        <div class="clear-spmgsnipreview"></div>
                        {if $spmgsnipreviewcount_reviews >0 && $spmgsnipreviewis16_snippet == 1}</div>{/if}
                    <br/>


                    {if $spmgsnipreviewstarratingon == 1}

                        <a href="javascript:void(0)" onclick="$('.gsniprev-rating-block').toggle();" class="view-ratings">{l s='View ratings' mod='spmgsnipreview'}</a>
                        <br/>
                        <div class="gsniprev-rating-block">
                            <table class="gsniprev-rating-block-table">
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        {section name="test" loop=5}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="five-blockreview">{$spmgsnipreviewfive|escape:'htmlall':'UTF-8'}</b></td>
                                </tr>
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        {section name="test" loop=4}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                        {section name="test" loop=1}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="four-blockreview">{$spmgsnipreviewfour|escape:'htmlall':'UTF-8'}</b></td>
                                </tr>
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        {section name="test" loop=3}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                        {section name="test" loop=2}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="three-blockreview">{$spmgsnipreviewthree|escape:'htmlall':'UTF-8'}</b></td>
                                </tr>
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        {section name="test" loop=2}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                        {section name="test" loop=3}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="two-blockreview">{$spmgsnipreviewtwo|escape:'htmlall':'UTF-8'}</b></td>
                                </tr>
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        {section name="test" loop=1}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                        {section name="test" loop=4}
                                            <img alt="{$smarty.section.test.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/section}
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="one-blockreview">{$spmgsnipreviewone|escape:'htmlall':'UTF-8'}</b></td>
                                </tr>
                            </table>
                        </div>

                        <br/>
                    {/if}


                    {if $spmgsnipreviewis_add != 1}
                        <a class="btn-spmgsnipreview btn-primary-spmgsnipreview" href="#idTab777" id="idTab777-my-click" >
        <span>
            <i class="icon-pencil"></i>&nbsp;

            {l s='Add Review' mod='spmgsnipreview'}

        </span>
                        </a>
                    {/if}


                    <a class="btn-spmgsnipreview btn-default-spmgsnipreview" href="#idTab777" >
        <span>
            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="title-rating-one-star" alt="{l s='View Reviews' mod='spmgsnipreview'}"/>
            {l s='View Reviews' mod='spmgsnipreview'}
        </span>
                    </a>




                </div>




            {/if}

        {/if}

    {/if}

    {if isset($spmgsnipreviewproductfooter)}
        {$spmgsnipreviewproductfooter|escape:'htmlall':'UTF-8'}
    {/if}


    {if $spmgsnipreviewpinvis_on == 1 && isset($spmgsnipreview_productFooter) && $spmgsnipreview_productFooter == 'productFooter'}
        <a href="//www.pinterest.com/pin/create/button/?
		url=http://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}
		&media={$product_image|escape:'htmlall':'UTF-8'}
		&description={$meta_description|escape:'htmlall':'UTF-8'}"
           data-pin-do="buttonPin" data-pin-config="{if $spmgsnipreviewpinterestbuttons == 'firston'}above{/if}{if $spmgsnipreviewpinterestbuttons == 'secondon'}beside{/if}{if $spmgsnipreviewpinterestbuttons == 'threeon'}none{/if}">
            <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" alt="Pinterest" />
        </a>
    {/if}

{/if}