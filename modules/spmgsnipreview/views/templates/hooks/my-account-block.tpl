{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}

{if $spmgsnipreviewislogged !=0}
<li>

	<a href="{$spmgsnipreviewaccount_url|escape:'htmlall':'UTF-8'}"
	   title="{l s='Product Reviews' mod='spmgsnipreview'}">
        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'html':'UTF-8'}" alt="{l s='Product Reviews' mod='spmgsnipreview'}" />
       	{l s='Product Reviews' mod='spmgsnipreview'}
	   	</a>
</li>
{/if}

{/if}

{/if}


{if $spmgsnipreviewis_uprof == 1}
{if $spmgsnipreviewislogged !=0}
    <li>

        <a href="{$spmgsnipreviewuacc_url|escape:'htmlall':'UTF-8'}"
           title="{l s='User profile' mod='spmgsnipreview'}">
            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/users/users-logo.png" alt="{l s='User profile' mod='spmgsnipreview'}" />
            {l s='User profile' mod='spmgsnipreview'}
        </a>
    </li>
{/if}
{/if}


{if $spmgsnipreviewis_storerev == 1}
    {if $spmgsnipreviewislogged !=0}
        <li>

            <a href="{$spmgsnipreviewmysr_url|escape:'htmlall':'UTF-8'}"
               title="{l s='Store Reviews' mod='spmgsnipreview'}">
                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/storereviews-logo.png" alt="{l s='Store Reviews' mod='spmgsnipreview'}" />
                {l s='Store Reviews' mod='spmgsnipreview'}
            </a>
        </li>
    {/if}
{/if}