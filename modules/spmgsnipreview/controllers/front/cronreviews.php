<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewCronreviewsModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {

        $name = "spmgsnipreview";
        $token = Tools::getValue('token');

        include_once(_PS_MODULE_DIR_.$name.'/spmgsnipreview.php');
        $obj_main = new spmgsnipreview();

        $_token = $obj_main->getokencron();


        if($_token == $token){


            include_once(_PS_MODULE_DIR_.$name.'/classes/spmgsnipreviewhelp.class.php');
            $obj = new spmgsnipreviewhelp();

            $obj->generateGoogleReviews();


        } else {
            echo 'Error: Access denien! Invalid token!';
        }
        exit;
    }

}