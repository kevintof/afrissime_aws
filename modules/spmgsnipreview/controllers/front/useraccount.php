<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewuseraccountModuleFrontController extends ModuleFrontController
{
    public $php_self;
	public function init()
	{

		parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();
    }

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
        $name_module = 'spmgsnipreview';
        $this->php_self = 'module-'.$name_module.'-useraccount';


        parent::initContent();



        $is_uprof = Configuration::get($name_module.'is_uprof');

        if (!$is_uprof)
            Tools::redirect('index.php');

        $cookie = Context::getContext()->cookie;

        $id_customer = isset($cookie->id_customer)?$cookie->id_customer:0;
        if (!$id_customer)
            Tools::redirect('authentication.php');


        include_once(_PS_MODULE_DIR_.$name_module.'/classes/userprofileg.class.php');
        $obj = new userprofileg();

        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        $_data_translate = $obj_spmgsnipreview->translateCustom();

        $obj_spmgsnipreview->setSEOUrls();


        include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $data_urls = $obj_spmgsnipreviewhelp->getSEOURLs();
        $my_account = $data_urls['my_account'];


        $info_customer = $obj->getCustomerInfo();
        $avatar_thumb = $info_customer['avatar_thumb'];
        $exist_avatar = $info_customer['exist_avatar'];
        $is_show = $info_customer['is_show'];


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $_data_translate['meta_title_myaccount'];
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = $_data_translate['meta_description_myaccount'];
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $_data_translate['meta_keywords_myaccount'];
        }

        $this->context->smarty->assign('meta_title' , $_data_translate['meta_title_myaccount']);
        $this->context->smarty->assign('meta_description' , $_data_translate['meta_description_myaccount']);
        $this->context->smarty->assign('meta_keywords' , $_data_translate['meta_keywords_myaccount']);

        $this->context->smarty->assign(array(
            $name_module.'avatar_thumb' => $avatar_thumb,
            $name_module.'exist_avatar' => $exist_avatar,
            $name_module.'is_show' => $is_show,
            $name_module.'my_account'=>$my_account,

            $name_module.'ava_msg8'=>$_data_translate['ava_msg8'],
            $name_module.'ava_msg9'=>$_data_translate['ava_msg9'],

        ));






        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:' . $name_module . '/views/templates/front/useraccount17.tpl');
        }else {
            $this->setTemplate('useraccount.tpl');
        }


    }
}