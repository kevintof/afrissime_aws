<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewRsstestimonialsModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {

        $_name = "spmgsnipreview";

        include_once(_PS_MODULE_DIR_.$_name.'/classes/storereviews.class.php');
        $shopreviews = new storereviews();



        $_http_host = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;

        $cookie = Context::getContext()->cookie;

        $id_lang = (int)$cookie->id_lang;
        $data_language = $shopreviews->getfacebooklib($id_lang);
        $rss_title =  '<![CDATA['.Configuration::get('PS_SHOP_NAME').']]>';
        $rss_description =  '<![CDATA['.Configuration::get('PS_SHOP_NAME').' Testimonials]]>';

        if (Configuration::get('PS_SSL_ENABLED') == 1)
            $url = "https://";
        else
            $url = "http://";

        $site = $_SERVER['HTTP_HOST'];

        // Lets build the page
        //$rootURL = $url.$site."/feeds/";
        $latestBuild = date("r");

        // Lets define the the type of doc we're creating.
        header('Content-Type:text/xml; charset=utf-8');
        $createXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";


        if(Configuration::get($_name.'rssontestim') == 1){

            $createXML .= "<rss version=\"0.92\">\n";
            $createXML .= "<channel>
	<title>".$rss_title."</title>
	<link>$url$site</link>
	<description>".$rss_description."</description>
	<lastBuildDate>$latestBuild</lastBuildDate>
	<docs>http://backend.userland.com/rss092</docs>
	<language>".$data_language['rss_language_iso']."</language>
	<image>
			<title>".$rss_title."</title>
			<url>".$_http_host."img/logo.jpg</url>
			<link>$url$site</link>
	</image>
";

            $data_rss_items = $shopreviews->getItemsForRSS();


            if(!empty($data_rss_items['items'])) {
                foreach ($data_rss_items['items'] as $_item) {
                    $page = $_item['page'];
                    $description = $_item['seo_description'];
                    $title = $_item['title'];
                    $pubdate = $_item['pubdate'];
                    $createXML .= $shopreviews->createRSSFile($title, $description, $page, $pubdate);
                }

            }
            $createXML .= "</channel>\n </rss>";
        // Finish it up
        }

        echo $createXML;
        exit;

    }


}
