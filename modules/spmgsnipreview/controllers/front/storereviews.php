<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewstorereviewsModuleFrontController extends ModuleFrontController
{

    public $php_self;
	
	public function init()
	{

        parent::init();
	}
	
	public function setMedia()
	{

        parent::setMedia();


        $name_module = 'spmgsnipreview';

        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();


        if(Configuration::get($name_module.'is_files'.$_prefix)){


            $this->addJqueryUi(array('ui.widget'));

            //$this->context->controller->addJqueryUI(array('ui.widget'));
            $this->context->controller->addJS(__PS_BASE_URI__.'modules/'.$name_module.'/views/js/jquery.fileupload.js');
            $this->context->controller->addJS(__PS_BASE_URI__.'modules/'.$name_module.'/views/js/jquery.fileupload-process.js');
            $this->context->controller->addJS(__PS_BASE_URI__.'modules/'.$name_module.'/views/js/jquery.fileupload-validate.js');


            $this->context->controller->addJs(__PS_BASE_URI__.'modules/'.$name_module.'/views/js/main-fileupload-storereviews.js');

            $this->context->controller->addCSS(__PS_BASE_URI__.'modules/'.$name_module.'/views/css/font-custom.min.css');



            if(version_compare(_PS_VERSION_, '1.7', '>')) {
                $this->addJqueryPlugin(array('fancybox'));
            }

        }


        if(Configuration::get($name_module.'d_eff_shop'.$_prefix) != "disable_all_effects") {

            $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/js/wow.js');
            $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/css/animate.css');
        }


    }


	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{

        $name_module = 'spmgsnipreview';
        $this->php_self = 'module-'.$name_module.'-storereviews';

		parent::initContent();



        $cookie = Context::getContext()->cookie;
        $id_lang = (int)$cookie->id_lang;


        $is_storerev = Configuration::get($name_module.'is_storerev');
        if (!$is_storerev)
            Tools::redirect('index.php');

        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        $_data_translate = $obj_spmgsnipreview->translateItems();

        $_data_translate_custom = $obj_spmgsnipreview->translateCustom();




        $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();

        $obj_spmgsnipreview->basicSettingsHook();

        $obj_spmgsnipreview->setStarsImagesSetting();

        $obj_spmgsnipreview->setSEOUrls();

        $obj_spmgsnipreview->setStoreReviewsSettings();

        $obj_spmgsnipreview->setuserprofilegSettings();


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $_data_translate['meta_title_testimonials_all'];
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = $_data_translate['meta_description_testimonials_all'];
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $_data_translate['meta_keywords_testimonials_all'];
        }


        $this->context->smarty->assign('meta_title' , $_data_translate['meta_title_testimonials_all']);
        $this->context->smarty->assign('meta_description' , $_data_translate['meta_description_testimonials_all']);
        $this->context->smarty->assign('meta_keywords' , $_data_translate['meta_keywords_testimonials_all']);



        $this->context->smarty->assign(
            array(
                 $name_module.'msg1' => $_data_translate['msg1'],
                 $name_module.'msg1_2' => $_data_translate['msg1_2'],
                 $name_module.'msg2' => $_data_translate['msg2'],
                 $name_module.'msg3' => $_data_translate['msg3'],
                 $name_module.'msg4' => $_data_translate['msg4'],
                 $name_module.'msg5' => $_data_translate['msg5'],
                 $name_module.'msg6' => $_data_translate['msg6'],
                 $name_module.'msg7' => $_data_translate['msg7'],
                 $name_module.'msg8' => $_data_translate['msg8'],
                 $name_module.'msg9' => $_data_translate['msg9'],
                 $name_module.'msg10' => $_data_translate['msg10'],
                 $name_module.'ptc_msg13_1'=>$_data_translate_custom['ptc_msg13_1'],
                 $name_module.'ptc_msg13_2'=>$_data_translate_custom['ptc_msg13_2'],
            )
        );



        include_once(_PS_MODULE_DIR_.$name_module.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();





        $step = Configuration::get($name_module.'perpage'.$_prefix);
        $p = (int)Tools::getValue('p'.$_prefix);


        $start = (int)(($p - 1)*$step);
        if($start<0)
            $start = 0;


        $frat = Tools::getValue('frat'.$_prefix);


        $search = Tools::getValue("search".$_prefix);
        $is_search = 0;

        ### search ###
        if(Tools::strlen($search)>0){
            $is_search = 1;

        }
        $this->context->smarty->assign($name_module.'is_search'.$_prefix, $is_search);
        $this->context->smarty->assign($name_module.'search'.$_prefix, $search);



        $_data = $obj_storereviews->getTestimonials(array('start'=>$start,'step'=>$step,'frat'=>$frat,'is_search'=>$is_search,
                                                        'search'=>$search));


        $paging = $obj_storereviews->PageNav17($start,$_data['count_all_reviews'],$step, array('frat'=>$frat,'is_search'=>$is_search,
                                                                                            'search'=>$search,'prefix'=>$name_module.$_prefix, 'action'=>'allstorereviews')
                                            );


        $id_customer = isset($cookie->id_customer)?$cookie->id_customer:0;
        $name_customer = '';
        $email_customer = '';
        $avatar = '';
        if($id_customer) {
            $customer_data = $obj_storereviews->getInfoAboutCustomer(array('id_customer' => $id_customer, 'is_full' => 1));
            $name_customer = $customer_data['customer_name'];
            $email_customer = $customer_data['email'];

            $data_avatar = $obj_storereviews->getAvatarForCustomer(array('id_customer' => $id_customer));
            $avatar = $data_avatar['avatar'];
        }


        $is_buy = $obj_storereviews->checkProductBought(array('id_customer'=>$id_customer));



        $this->context->smarty->assign($name_module.'frat'.$_prefix, $frat);

        $data_rating = $obj_storereviews->getCountRatingForItem();
        $this->context->smarty->assign($name_module.'one'.$_prefix, $data_rating['one']);
        $this->context->smarty->assign($name_module.'two'.$_prefix, $data_rating['two']);
        $this->context->smarty->assign($name_module.'three'.$_prefix, $data_rating['three']);
        $this->context->smarty->assign($name_module.'four'.$_prefix, $data_rating['four']);
        $this->context->smarty->assign($name_module.'five'.$_prefix, $data_rating['five']);


        $id_shop = Context::getContext()->shop->id;
        $this->context->smarty->assign(array(
            $name_module.'criterions' => $obj_storereviews->getReviewCriteria(array('id_lang'=>$id_lang,'id_shop'=>$id_shop)),
        ));


        $this->context->smarty->assign($name_module.'d_eff_shop'.$_prefix, Configuration::get($name_module.'d_eff_shop'.$_prefix));

        $this->context->smarty->assign(
            array(

                'reviews'.$_prefix => $_data['reviews'],
                'count_all_reviews'.$_prefix => $_data['count_all_reviews'],
                'paging'.$_prefix => $paging,

                $name_module.'page_text' => $_data_translate['page'],

                'shop_name_snippet'.$_prefix=>Configuration::get('PS_SHOP_NAME'),

                $name_module.'name_c'.$_prefix => $name_customer,
                $name_module.'email_c'.$_prefix => $email_customer,
                $name_module.'c_avatar'.$_prefix => $avatar,

                $name_module.'is_buy'.$_prefix => $is_buy,
                $name_module.'id_customer'.$_prefix=>$id_customer
            )
        );


        // gdpr
        $this->context->smarty->assign(array('id_module' => $obj_spmgsnipreview->getIdModule()));
        // gdpr



        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:' . $name_module . '/views/templates/front/storereviews17.tpl');
        }else {
            $this->setTemplate('storereviews.tpl');
        }


    }
}