<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewUploadshopModuleFrontController extends ModuleFrontController
{

    public function postProcess()
    {
        header("Access-Control-Allow-Origin: *");
        $HTTP_X_REQUESTED_WITH = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : '';
        if ($HTTP_X_REQUESTED_WITH != 'XMLHttpRequest') {
            exit;
        }


        $name_module = 'spmgsnipreview';
        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();


        $token = Tools::getValue('token');
        $token_orig = $obj_spmgsnipreview->getokencron();
        if($token_orig !=$token)
            die('Invalid token.');


        ob_start();




        $status = 'success';
        $message = '';

        $is_error = 0;
        $name_file = '';
        $size_file = '';

        include_once(_PS_MODULE_DIR_.$name_module.'/classes/storereviews.class.php');
        $obj = new storereviews();



        $action = Tools::getValue('actionshopr');

        switch ($action) {
            case 'add':
                $files = $_FILES['files'];
                $data_files_return = $obj->uploadTmpFile(array('files'=>$files));
                $is_error = $data_files_return['is_error'];
                $status = $data_files_return['status'];
                $message = $data_files_return['message'];
                $size_file = $data_files_return['size_file'];
                $name_file = $data_files_return['name_file'];

                break;
            case 'del':
                $name = Tools::getValue('name');
                $obj->deleteTmpFile(array('name'=>$name));

                break;
            case 'deletefile':


                if($obj_spmgsnipreview->is_demo){
                    $status = 'error';
                    $message = 'Feature disabled on the demo mode!';
                } else {

                    $id = Tools::getValue('item_id');
                    $obj->deleteFile(array('id'=>$id));
                }
            break;


        }


        ## only for backoffice / add or delete image ##
        $action_del_admin = Tools::getValue('action');

        switch ($action_del_admin) {
            case 'add':
                $files = $_FILES['files'];
                $data_files_return = $obj->uploadTmpFile(array('files'=>$files));
                $is_error = $data_files_return['is_error'];
                $status = $data_files_return['status'];
                $message = $data_files_return['message'];
                $size_file = $data_files_return['size_file'];
                $name_file = $data_files_return['name_file'];

            break;
            case 'del':
                $name = Tools::getValue('name');
                $obj->deleteTmpFile(array('name'=>$name));

            break;
            case 'deletefile':
                include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                $obj_spmgsnipreview = new spmgsnipreview();




                if($obj_spmgsnipreview->is_demo){
                    $status = 'error';
                    $message = 'Feature disabled on the demo mode!';
                } else {

                    $id = Tools::getValue('item_id');

                    $obj->deleteFile(array('id'=>$id));
                }
            break;

        }

        ## only for backoffice / add or delete image ##



        $response = new stdClass();
        ob_get_clean();
        $response->status = $status;
        $response->message = $message;

        if($is_error == 0 && $action == 'add'){
            $response->params = array('size' => $size_file,'name'=>$name_file);
        }

        if($is_error == 0 && $action_del_admin == 'add'){
            $response->params = array('size' => $size_file,'name'=>$name_file);
        }



        echo json_encode($response);
        exit;

    }

}