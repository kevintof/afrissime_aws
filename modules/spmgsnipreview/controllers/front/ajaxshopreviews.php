<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewAjaxshopreviewsModuleFrontController extends ModuleFrontController
{

    public function postProcess()
    {

        header("Access-Control-Allow-Origin: *");
        $HTTP_X_REQUESTED_WITH = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : '';
        if ($HTTP_X_REQUESTED_WITH != 'XMLHttpRequest') {
            exit;
        }


        $name_module = 'spmgsnipreview';
        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $objspmgsnipreview = new spmgsnipreview();

        $token = Tools::getValue('token');
        $token_orig = $objspmgsnipreview->getokencron();
        if($token_orig !=$token)
            die('Invalid token.');



        ob_start();
        $status = 'success';
        $message = '';

        $_html_page_nav = '';
        $_html = '';




        $smarty = Context::getContext()->smarty;
        $cookie = Context::getContext()->cookie;


        include_once(_PS_MODULE_DIR_.$name_module.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();

        $action = Tools::getValue('action');




        switch ($action){

            case 'addreview':
                $_html = '';
                $error_type = 0;
                $voucher_html = '';


                $id_customer = isset($cookie->id_customer)?$cookie->id_customer:0;

                $ok_captcha = 1;
                if(!$id_customer) {
                    $codeCaptcha = Tools::getValue('captcha');

                    $cookie = new Cookie($name_module);
                    $code = $cookie->secure_code_testim;



                    $is_captcha = Configuration::get($name_module . 'is_captchati');
                    if ($is_captcha == 1) {
                        if ($code == $codeCaptcha)
                            $ok_captcha = 1;
                        else
                            $ok_captcha = 0;
                    }
                }



                if($ok_captcha == 1){

                    ### ratings ###


                    $id_lang = $objspmgsnipreview->getIdLang();

                    include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmgsnipreviewhelp.class.php');
                    $obj = new spmgsnipreviewhelp();

                    $rating = array();


                        $criterions = $obj_shopreviews->getReviewCriteria(array('id_lang'=>$id_lang,'id_shop'=>$obj->getIdShop()));


                        if(!empty($criterions)) {
                            if (sizeof($criterions) > 0) {

                                foreach ($criterions as $criterion) {
                                    $id_criterion = $criterion['id_spmgsnipreview_review_criterion'];

                                    $rating_criterion = Tools::getValue('rat_rel' . $id_criterion);

                                    if ($rating_criterion)
                                        $rating[$id_criterion] = $rating_criterion;
                                }

                            }
                        }



                        if(sizeof($rating)==0){
                            $rating[0] = Tools::getValue('rat_rel');
                        }


                    ### ratings ###


                    $name = strip_tags(trim(Tools::getValue('name-review')));

                    $country = strip_tags(trim(Tools::getValue('country-review')));
                    $city = strip_tags(trim(Tools::getValue('city-review')));

                    $email = trim(Tools::getValue('email-review'));
                    //$rating = Tools::getValue('rat_rel');
                    $web = strip_tags(str_replace("http://","",trim(Tools::getValue('web-review'))));
                    $web = strip_tags(str_replace("https://","",$web));
                    $text_review = trim(Tools::getValue('text-review'));
                    $company = strip_tags(trim(Tools::getValue('company-review')));
                    $address = strip_tags(trim(Tools::getValue('address-review')));

                    if(!Validate::isEmail($email)) {
                        $error_type = 2;
                        $status = 'error';
                    }

                    $_prefix = $objspmgsnipreview->getPrefixShopReviews();
                    $data_check_is_bought = $obj_shopreviews->checkProductBoughtByEmail(array('email' => $email));
                    if(!$data_check_is_bought && Configuration::get($name_module.'whocanadd'.$_prefix) == 'buy'){
                        $error_type = 10;
                        $status = 'error';
                    }



                    if($error_type == 0 && Tools::strlen($name)==0){
                        $error_type = 1;
                        $status = 'error';
                    }

                    if($error_type == 0 && Tools::strlen($text_review)==0){
                        $error_type = 3;
                        $status = 'error';
                    }

                    $files = Tools::fileAttachment('avatar-review');
                    if(!empty($files['name']))
                    {
                        if(!$files['error'])
                        {
                            $type_one = $files['mime'];
                            $ext = explode("/",$type_one);

                            if(strpos('_'.$type_one,'image')<1)
                            {
                                $error_type = 8;
                                $status = 'error';

                            }elseif(!in_array($ext[1],array('png','x-png','gif','jpg','jpeg','pjpeg'))) {
                                $error_type = 9;
                                $status = 'error';
                            }

                            // check mimy type //
                            $tmp_name = $files['tmp_name'];

                            include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmgsnipreviewhelp.class.php');
                            $obj = new spmgsnipreviewhelp();

                            $mimeType = $obj->processCheckMimeType($tmp_name);

                            if (!$mimeType) {

                                $error_type = 8;
                                $status = 'error';

                            }
                            // check mimy type //
                        }
                    }

                    $filesrev = Tools::getValue('filesrev');

                    if($error_type == 0){
                        $post_images = Tools::getValue('post_images');
                        //insert review
                        $_data = array('name' => $name,
                            'email' => $email,
                            'web' => $web,
                            'text_review' => $text_review,
                            'company' => $company,
                            'address' => $address,
                            'rating' =>$rating,
                            'city'=>$city,
                            'country'=>$country,
                            'post_images' => $post_images,
                            'filesrev'=>$filesrev,
                        );
                        $data_voucher = $obj_shopreviews->saveTestimonial($_data);



                        // create voucher for review

                        $data_permissions_vouchers = $obj_shopreviews->getGroupPermissionsForVouchers();
                        $is_show_voucher = $data_permissions_vouchers['is_show_voucher'];



                        if(Configuration::get($name_module.'vis_on'.$_prefix) == 1 && $is_show_voucher == 1){
                            include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                            $objspmgsnipreview = new spmgsnipreview();
                            $data_translate = $objspmgsnipreview->translateCustom();

                            $smarty->assign(
                                array(
                                    $name_module.'firsttext' => $data_translate['firsttext'],
                                    $name_module.'discountvalue' => $data_translate['discountvalue'],
                                    $name_module.'secondtext' => $data_translate['secondtext'],
                                    $name_module.'voucher_code' => $data_voucher['voucher_code'],
                                    $name_module.'threetext' => $data_translate['threetext'],
                                    $name_module.'date_until' => $data_voucher['date_until'],

                                )
                            );

                            ob_start();

                                echo $objspmgsnipreview->renderReviewCoupon();

                            $voucher_html = ob_get_clean();



                        }
                        // create voucher for review

                    }

                } else {
                    $_html = '';

                    // invalid security code (captcha)
                    $error_type = 4;
                    $status = 'error';
                }


                break;

            case 'active':
                $id = (int)Tools::getValue('id');
                $value = (int)Tools::getValue('value');
                if($value == 0){
                    $value = 1;
                } else {
                    $value = 0;
                }
                $type_action = Tools::getValue('type_action');

                switch($type_action){
                    case 'testimonial':
                        $obj_shopreviews->setPublsh(array('id'=>$id,'active'=>$value));
                        break;


                }



                break;
            case 'deleteimg':
                include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                $obj_spmgsnipreview = new spmgsnipreview();

                if($obj_spmgsnipreview->is_demo){
                    $status = 'error';
                    $message = 'Feature disabled on the demo mode!';
                } else {
                    $item_id = Tools::getValue('item_id');
                    $id_customer = Tools::getValue('id_customer');
                    $obj_shopreviews->deleteAvatar(array('id' => $item_id,'id_customer'=>$id_customer));
                }
                break;

            case 'importoldorders':
                include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                $objspmgsnipreview = new spmgsnipreview();

                $data_translate = $objspmgsnipreview->translateItems();

                $start_date = Tools::getValue('start_date');
                $end_date = date('Y-m-d H:i:s');

                $is_error = 0;
                $msg_error  = '';
                if(strtotime($start_date) > strtotime($end_date)){

                    $is_error = 1;
                    $msg_error = $data_translate['orders_date_start_more_end'];

                }

                if(!$start_date){
                    $is_error = 1;
                    $msg_error = $data_translate['orders_date_empty'];
                }


                if(!$is_error){

                    include_once(_PS_MODULE_DIR_.$name_module.'/classes/featureshelptestim.class.php');
                    $obj_featureshelp = new featureshelptestim();

                    $date_importoldorders = $obj_featureshelp->importOldOrders(array('start_date'=>$start_date, 'end_date'=>$end_date));

                    if($date_importoldorders) {

                        $smarty->assign($name_module . 'msg', $data_translate['orders_date_ok1'].' <b>'.(int)$date_importoldorders.'</b> '.$data_translate['orders_date_ok2']);


                            echo $objspmgsnipreview->renderSuccess();


                    } else {
                        $is_error = 1;
                        $msg_error =  $data_translate['orders_date_not_exists'];
                    }

                }


                if($is_error) {
                    $smarty->assign($name_module . 'msg', $msg_error);


                        echo $objspmgsnipreview->renderError();


                }

                break;
            case 'change-reminder':
                $current_id_customer = isset($cookie->id_customer)?$cookie->id_customer:0;

                if($current_id_customer) {
                    $reminder_status = (int)Tools::getValue('reminder_status');
                    $data = array('id_customer'=>$current_id_customer,'reminder_status'=>$reminder_status);
                    $obj_shopreviews->updateReminderForCustomer($data);
                }else {
                    include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                    $objspmgsnipreview = new spmgsnipreview();

                    $data_translate = $objspmgsnipreview->translateCustom();
                    $message = $data_translate['error_login'];
                    $status = 'error';

                }
                break;
            case 'reminder-send':
                include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                $objspmgsnipreview = new spmgsnipreview();

                $data_translate = $objspmgsnipreview->translateItems();

                $id = (int)Tools::getValue('id');
                $type = Tools::getValue('type');

                $is_error = 0;

                include_once(_PS_MODULE_DIR_.$name_module.'/classes/featureshelptestim.class.php');
                $obj_featureshelp = new featureshelptestim();

                ob_start();

                $status_order = $obj_featureshelp->getOrderStatus(array('order_id'=>$id));

                $reminder_link_settings = '<a href="javascript:void(0)"
                onclick="tabs_custom(115)" style="text-decoration:underline">
                '.$data_translate['customer_reminder_settings'].'</a>';


                $_prefix = $objspmgsnipreview->getPrefixShopReviews();
                $orderstatuses = Configuration::get($name_module.'orderstatuses'.$_prefix);
                $orderstatuses = explode(",",$orderstatuses);
                if(in_array($status_order,$orderstatuses)) {

                    $data_check_errors = $obj_featureshelp->getCronTaskDelayForReminder(array('type'=>$type,'id_order'=>$id));
                    $type_error_reminder = $data_check_errors['type_error'];

                    if($type_error_reminder == 0) {
                        // send tasks
                        $obj_featureshelp->sendCronTab(array('order_id' => $id));
                    } else {

                        // handler errors
                        $html_err_txt = '';
                        switch($type_error_reminder){
                            case 1:
                                $delay = (int)Configuration::get($name_module . 'delay' . $_prefix);
                                $html_err_txt = $data_translate['customer_reminder_error1_1'].'&nbsp;<b>'.$delay.'</b>&nbsp;'.$data_translate['customer_reminder_error1_2'].
                                    '<br/><br/>'.$data_translate['configure_reminder_delay_first'].': '.$reminder_link_settings;
                                break;
                            case 2:
                                $delaysec = (int)Configuration::get($name_module . 'delaysec' . $_prefix);
                                $html_err_txt = $data_translate['customer_reminder_error1_1'].'&nbsp;<b>'.$delaysec.'</b>&nbsp;'.$data_translate['customer_reminder_error2_2'].
                                    '<br/><br/>'.$data_translate['configure_reminder_delay_second'].': '.$reminder_link_settings;
                                break;
                            case 3:
                                $html_err_txt = $data_translate['review_reminder_second']. '<br/><br/>'. $reminder_link_settings;
                                break;
                            case 4:
                                $html_err_txt = $data_translate['review_reminder']. '<br/><br/>'. $reminder_link_settings;
                                break;
                            case 5:
                                $html_err_txt = $data_translate['review_reminder_customer_txt'].'<br/><br/>'.$data_translate['review_reminder_customer']. '<br/><br/>'. $reminder_link_settings;
                                break;
                        }

                        echo $html_err_txt;

                        $is_error = 1;
                        // handler errors
                    }

                } else {
                    $txt_accepted_order_statuses = '';

                    $txt_accepted_order_statuses .= '<b>'.$data_translate['accepted_order_statuses'].'</b>:<br/>';
                    $accepted_order_statuses = $obj_featureshelp->getAcceptedOrderStatuses();


                    foreach($accepted_order_statuses as $accepted_order_status){
                        $payment_order  = $accepted_order_status['name'];
                        $txt_accepted_order_statuses .= "<br/>".$payment_order;

                    }

                    $txt_accepted_order_statuses .= '<br/><br/>'.$data_translate['configure_order_statuses'].': '.$reminder_link_settings;

                    echo $txt_accepted_order_statuses;

                    $is_error = 1;
                }

                $html_resutlt = ob_get_clean();

                $smarty->assign($name_module . 'msg', $html_resutlt);

                if($is_error == 0) {
                        echo $objspmgsnipreview->renderSuccess();


                } else {

                        echo $objspmgsnipreview->renderError();

                }




                break;
            case 'allstorereviews':

                include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                $objspmgsnipreview = new spmgsnipreview();


                $objspmgsnipreview->setStoreReviewsSettings();
                $objspmgsnipreview->setSEOUrls();
                $objspmgsnipreview->setStarsImagesSetting();
                $objspmgsnipreview->setuserprofilegSettings();


                $_prefix = $objspmgsnipreview->getPrefixShopReviews();



                $step = Configuration::get($name_module.'perpage'.$_prefix);


                $start = (int) Tools::getValue('page');
                if($start<0)
                    $start = 0;


                $frat = Tools::getValue('frat'.$_prefix);


                $search = Tools::getValue("search".$_prefix);
                $is_search = 0;

                ### search ###
                if(Tools::strlen($search)>0){
                    $is_search = 1;

                }
                $smarty->assign($name_module.'is_search'.$_prefix, $is_search);
                $smarty->assign($name_module.'search'.$_prefix, $search);


                $sort_condition = Tools::getValue('sort_condition');
                $is_sort = Tools::getValue('is_sort');



                $_data = $obj_shopreviews->getTestimonials(
                                                            array(
                                                                  'start'=>$start,'step'=>$step,'frat'=>$frat,'is_search'=>$is_search,
                                                                  'search'=>$search, 'sort_condition'=>$sort_condition,'is_sort'=>$is_sort,
                                                                 )
                                                          );


                $paging = $obj_shopreviews->PageNav17($start,$_data['count_all_reviews'],$step, array('frat'=>$frat,'is_search'=>$is_search,'search'=>$search,
                        'search'=>$search,'prefix'=>$name_module.$_prefix, 'action'=>'allstorereviews', 'sort_condition'=>$sort_condition,'is_sort'=>$is_sort,)
                );

                $_html_page_nav = $paging;


                $id_customer = isset($cookie->id_customer)?$cookie->id_customer:0;
                $name_customer = '';
                $email_customer = '';
                $avatar = '';
                if($id_customer) {
                    $customer_data = $obj_shopreviews->getInfoAboutCustomer(array('id_customer' => $id_customer, 'is_full' => 1));
                    $name_customer = $customer_data['customer_name'];
                    $email_customer = $customer_data['email'];

                    $data_avatar = $obj_shopreviews->getAvatarForCustomer(array('id_customer' => $id_customer));
                    $avatar = $data_avatar['avatar'];
                }


                $is_buy = $obj_shopreviews->checkProductBought(array('id_customer'=>$id_customer));

                $smarty->assign($name_module.'d_eff_shop'.$_prefix, Configuration::get($name_module.'d_eff_shop'.$_prefix));



                $smarty->assign(
                    array(

                        'reviews'.$_prefix => $_data['reviews'],
                        'count_all_reviews'.$_prefix => $_data['count_all_reviews'],
                        'paging'.$_prefix => $paging,

                        'shop_name_snippet'.$_prefix=>Configuration::get('PS_SHOP_NAME'),

                        $name_module.'name_c'.$_prefix => $name_customer,
                        $name_module.'email_c'.$_prefix => $email_customer,
                        $name_module.'c_avatar'.$_prefix => $avatar,

                        $name_module.'is_buy'.$_prefix => $is_buy,
                        $name_module.'id_customer'.$_prefix=>$id_customer
                    )
                );


                ob_start();

                echo $objspmgsnipreview->renderListStoreReviews();

                $_html = ob_get_clean();


            break;

            case 'userstorereviews':

                ## store reviews by customer ##

                $user_id = (int)Tools::getValue('uid');

                include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                $obj_spmgsnipreview = new spmgsnipreview();


                $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();

                $obj_spmgsnipreview->setStoreReviewsSettings();

                $obj_spmgsnipreview->setSEOUrls();
                $obj_spmgsnipreview->settingsHooks();


                include_once(_PS_MODULE_DIR_.$name_module.'/classes/storereviews.class.php');
                $obj_storereviews = new storereviews();


                $step = Configuration::get($name_module.'perpageu'.$_prefix);


                $start = (int) Tools::getValue('page');
                if($start<0)
                    $start = 0;


                $frat = Tools::getValue('frat'.$_prefix);


                $search = Tools::getValue("search".$_prefix);
                $is_search = 0;

                ### search ###
                if(Tools::strlen($search)>0){
                    $is_search = 1;

                }

                $smarty->assign($name_module.'is_search'.$_prefix, $is_search);
                $smarty->assign($name_module.'search'.$_prefix, $search);

                $sort_condition = Tools::getValue('sort_condition');
                $is_sort = Tools::getValue('is_sort');


                $_data = $obj_storereviews->getTestimonials(
                                                            array(
                                                                  'id_customer'=>$user_id,'start'=>$start,'step'=>$step,'frat'=>$frat,'is_search'=>$is_search,
                                                                  'search'=>$search, 'sort_condition'=>$sort_condition,'is_sort'=>$is_sort,
                                                                )
                                                            );



                $paging = $obj_storereviews->PageNav17($start,$_data['count_all_reviews'],$step,
                    array('frat'=>$frat,'is_search'=>$is_search,'search'=>$search,'prefix'=>$name_module.$_prefix, 'action'=>'userstorereviews','id_customer'=>$user_id,
                        'sort_condition'=>$sort_condition,'is_sort'=>$is_sort,)
                );

                $_html_page_nav = $paging;


                $smarty->assign($name_module.'d_eff_shop'.$_prefix, Configuration::get($name_module.'d_eff_shop_u'.$_prefix));



                $smarty->assign(
                    array(

                        'reviews'.$_prefix => $_data['reviews'],
                        'count_all_reviews'.$_prefix => $_data['count_all_reviews'],
                        $name_module.'paging'.$_prefix => $paging,
                        'shop_name_snippet'.$_prefix=>Configuration::get('PS_SHOP_NAME'),

                        $name_module.'id_customer'.$_prefix=>$user_id
                    )
                );


                ob_start();

                echo $obj_spmgsnipreview->renderListStoreReviews();

                $_html = ob_get_clean();

                ## store reviews by customer ##

            break;


            case 'mystorereviews':
                $cookie = Context::getContext()->cookie;


                $id_customer = isset($cookie->id_customer)?$cookie->id_customer:0;
                if (!$id_customer){
                    $status = 'error';
                    $message = 'You must be as registered customer!';
                } else {

                    include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
                    $obj_spmgsnipreview = new spmgsnipreview();

                    $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();

                    include_once(_PS_MODULE_DIR_.$name_module.'/classes/storereviews.class.php');
                    $obj_storereviews = new storereviews();


                    include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmgsnipreviewhelp.class.php');
                    $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

                    $id_lang = (int)$cookie->id_lang;
                    $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs(array('id_lang'=>$id_lang));

                    $rev_url = $data_seo_url['store_reviews_account_url'];



                    $obj_spmgsnipreview->settingsHooks();



                    $step = (int)$obj_storereviews->getStepForMyStoreReviews();

                    $start = (int) Tools::getValue('page');
                    if($start<0)
                        $start = 0;

                    $data_my_reviews = $obj_storereviews->getTestimonials(array('start'=>$start,'step'=>$step,'id_customer' => $id_customer,'is_my'=>1));



                    $paging = $obj_storereviews->PageNav17($start,$data_my_reviews['count_all_reviews'],$step, array('prefix'=>$name_module.$_prefix,'action'=>'mystorereviews'));

                    $_html_page_nav = $paging;

                    $smarty->assign($name_module.'d_eff_shop_my'.$_prefix, Configuration::get($name_module.'d_eff_shop_my'.$_prefix));

                    $smarty->assign(array(
                        $name_module.'my_reviews' => $data_my_reviews['reviews'],
                        $name_module.'paging' => $paging,
                        $name_module.'rev_url' => $rev_url,


                    ));


                    ob_start();

                    echo $obj_spmgsnipreview->renderListMyStoreReviews();

                    $_html = ob_get_clean();

                }


                break;

            default:
                $status = 'error';
                $message = 'Unknown parameters!';
                break;
        }


        $response = new stdClass();
        $content = ob_get_clean();
        $response->status = $status;
        $response->message = $message;
        if($action == "addreview") {
            $response->params = array('content' => $_html, 'voucher_html' => $voucher_html,
                'error_type' => $error_type
            );
        }elseif($action == "allstorereviews" || $action == "mystorereviews" || $action == "userstorereviews"){
            $response->params = array('content' => $_html, 'page_nav' => $_html_page_nav );
        }elseif($action == "reminder-send") {
            $response->params = array('content' => $content,
                'is_error' => $is_error
            );
        }else {
            $response->params = array('content' => $content);
        }
        echo json_encode($response);
        exit;

    }

}