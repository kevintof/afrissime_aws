** Message from online shop {shop_name} **

Dear {firstname} {lastname},

Thank you for placing the order (n°{orderid}) in our shop.

In order to help us constantly improve our offer, and to help other customers in their purchase decision process, we invite you to rate the products you have purchased on our shop once you have received them. Simply follow the link below each product below.

{products_text}

Thanks again !

-The {shop_name} Team