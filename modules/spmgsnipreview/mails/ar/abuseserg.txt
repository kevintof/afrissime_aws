Someone send abuse on the review via shop : {shop_name}

{review_data}

See review here: {rev_url}

The customer who reported this abuse: {customer}

Abuse text: {abuse_text}

Once you seen the notice, you can go to the moderation tool to your PrestaShop back office and either:

1) Edit or disable review, if review is abusive

2) Delete alert associated with the review

Your {shop_name} team