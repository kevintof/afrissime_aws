Dear {customer}

Here is your review :

Product: {product_name}

Rating: {rating} / 5

Title: {title}

Review: {review}

See your review here: {product_link}

Here is the shop owner reply:

{admin_response}

You can modify your review in your account {my_account} -> section "Reviews"


Your {shop_name} team