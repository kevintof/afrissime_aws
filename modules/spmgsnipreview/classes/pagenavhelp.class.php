<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class pagenavhelp extends Module {


    private $_name = 'spmgsnipreview';

    public function pagenav($start,$count,$step, $_data =null )
    {

        $custom_parameters = '';

        $prefix = isset($_data['prefix'])?$_data['prefix']:'';
        $action = isset($_data['action'])?$_data['action']:'';

        $is_rating_store = isset($_data['frat'])?$_data['frat']:0;
        $is_search = isset($_data['is_search'])?$_data['is_search']:0;
        $id_customer = isset($_data['id_customer'])?$_data['id_customer']:0;
        $id_product = isset($_data['id_product'])?$_data['id_product']:0;

        $is_sort = isset($_data['is_sort'])?$_data['is_sort']:0;


        $custom_parameter_product = $id_product?', '.$id_product.'':'';



        $custom_parameter = $id_customer?', '.$id_customer.'':$custom_parameter_product;


        if($id_customer && !$is_search && !$is_rating_store){
            $custom_parameters = ', \'\', \'\', '.$id_customer.'';
        }


        if($id_product && !$is_search && !$is_rating_store){
            $custom_parameters = ', \'\', \'\', '.$id_product.'';
        }


        if($is_rating_store){
            $custom_parameters = ', '.$is_rating_store.', \'\' '.$custom_parameter.'';
        }


        if($is_rating_store && $is_search){
            $custom_parameters = ', '.$is_rating_store.', 1 '.$custom_parameter.'';
        }

        if(!$is_rating_store && $is_search){
            $custom_parameters = ', \'\', 1 '.$custom_parameter.'';
        }

        if($is_sort){

            $custom_parameter_product_sort = Tools::strlen($custom_parameter_product) > 0 ? $custom_parameter_product : ($id_customer?', '.$id_customer.'':',\'\'');

            $custom_parameters = ', \'\', \'\' '.$custom_parameter_product_sort.', 1';
        }


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/'.__FUNCTION__.'.phtml');
        $res = ob_get_clean();

        return $res;
    }
}