<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2019 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
**/

class WeCashUpCallbackModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        $trans_token = '';
        $trans_uid = '';
        $merchant_secret = Configuration::get('WECASHUP_SKEY');
        $received_transaction_merchant_secret = null;/*create an empty received_transaction_merchant_secret*/
        $received_transaction_uid = null;/*create an empty received_transaction_uid*/
        $received_transaction_status  = null;/*create an empty received_transaction_status*/
        //$received_transaction_details = null;/*create an empty received_transaction_details | provider_name*/
        $received_transaction_token = null;/*create an empty received_transaction_token*/
      //  $received_transaction_type = null;/*create an empty  received_transaction_type | PAYMENT, REFUND, TRANSFER*/
        $received_transaction_amount = null;/*create an empty  received_transaction_amount*/
       // $received_transaction_currency = null;/*reate an empty  received_transaction_currency*/
        $authenticated = 'false'; /*create an authentification boolean and initialize it at false*/
        $execution_mode = 'TEST'; /*Payment Mode*/
        if (Tools::getIsset(Tools::getValue('merchant_secret'))) {
            $received_transaction_merchant_secret = (string)Tools::getValue('merchant_secret');
        }
        if (Tools::getIsset(Tools::getValue('transaction_uid'))) {
            $received_transaction_uid = (string)Tools::getValue('transaction_uid');
        }
        if (Tools::getIsset(Tools::getValue('transaction_status'))) {
            $received_transaction_status = (string)Tools::getValue('transaction_status');
        }
        /*if (Tools::getIsset(Tools::getValue('transaction_details'))) {
            $received_transaction_details = Tools::getValue('transaction_details');
        }*/
        if (Tools::getIsset(Tools::getValue('transaction_token'))) {
            $received_transaction_token = (string)Tools::getValue('transaction_token');
        }
        /*if (Tools::getIsset(Tools::getValue('transaction_type'))) {
            $received_transaction_type = Tools::getValue('transaction_type');
        }*/
        if (Tools::getIsset(Tools::getValue('transaction_amount'))) {
            $received_transaction_amount = Tools::getValue('transaction_amount');
        }
        /*if (Tools::getIsset(Tools::getValue('transaction_receiver_currency'))) {
            $received_transaction_currency = Tools::getValue('transaction_receiver_currency');
        }*/
        if (Tools::getIsset(Tools::getValue('transaction_receiver_execution_mode'))) {
            $execution_mode = Tools::getValue('transaction_receiver_execution_mode');
        }

        $_sql = 'SELECT * FROM '._DB_PREFIX_.'wecashup_data';
        $_sql .= ' WHERE transaction_uid="'.psql($received_transaction_uid).'"';
        $_sql .= ' AND transaction_token="'.psql($received_transaction_token).'"';

        $theOrder = Db::getInstance()->getRow($_sql);
        $trans_uid = $theOrder['transaction_uid'];
        $trans_token = $theOrder['transaction_token'];

        if ($received_transaction_merchant_secret != null &&
        $received_transaction_merchant_secret == $merchant_secret) {
            if ($trans_uid != '') {
                $database_transaction_uid = $trans_uid;
            } else {
                $database_transaction_uid = '';
            }

            if ($trans_token != '') {
                $database_transaction_token = $trans_token;
            } else {
                $database_transaction_token = '';
            }

            if ($received_transaction_uid != null && $received_transaction_uid == $database_transaction_uid) {
                if ($received_transaction_token  != null &&
                $received_transaction_token == $database_transaction_token) {
                    $authenticated = 'true';
                }
            }
        }
        $objOrder = new Order($theOrder['order_id']);
        $history = new OrderHistory();
        $history->id_order = (int)$objOrder->id;

        if ($authenticated == 'true') {
            if ($received_transaction_status == "PAID") {
                if ($received_transaction_amount == $theOrder['transaction_amount']) {
                    $history->changeIdOrderState(2, (int)($objOrder->id));
                }
            } else {
                $history->changeIdOrderState(6, (int)($objOrder->id));
            }

            $objOrder->payment = $objOrder->payment.' - '.$execution_mode;
            $objOrder->update();
        }
    }
}
