<?php
/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2019 PrestaShop SA
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
**/

class WeCashUpValidationModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {
        parent::initContent();
        $this->setTemplate('module:wecashup/views/templates/front/wecashup_display.tpl');
    }

    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        $cart = $this->context->cart;
        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 ||
        $cart->id_address_invoice == 0 || !$this->module->active) {
            Tools::redirect('index.php?controller=order&step=1');
        }
        // Check that this payment option is still available in case the customer
        // changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'wecashup') {
                $authorized = true;
                break;
            }
        }
        if (!$authorized) {
            die($this->module->l('This payment method is not available.', 'validation'));
        }
        $this->context->smarty->assign(array(
            'params' => $_REQUEST,
        ));
        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }
        $currency = $this->context->currency;
        $total = (float) $cart->getOrderTotal(true, Cart::BOTH);
        $merchant_uid = Configuration::get('WECASHUP_MUID');
        $merchant_public_key = Configuration::get('WECASHUP_PKEY');
        $merchant_secret = Configuration::get('WECASHUP_SKEY');
        $transaction_uid = ''; /*create an empty transaction_uid*/
        $transaction_token = ''; /*create an empty transaction_token*/
        $transaction_provider_name = ''; /*create an empty transaction_provider_name*/
        $transaction_confirmation_code = ''; /*create an empty confirmation code*/

        if (Tools::getIsset('transaction_uid')) {
            /*Get the transaction_uid posted by the payment box*/
            $transaction_uid = Tools::getValue('transaction_uid');
        }
        if (Tools::getIsset('transaction_token')) {
            /*Get the transaction_token posted by the payment box*/
            $transaction_token = Tools::getValue('transaction_token');
        }
        if (Tools::getIsset('transaction_provider_name')) {
            /*Get the transaction_provider_name posted by the payment box*/
            $transaction_provider_name = Tools::getValue('transaction_provider_name');
        }
        if (Tools::getIsset('transaction_confirmation_code')) {
            /*Get the transaction_confirmation_code posted by the payment box*/
            $transaction_confirmation_code = Tools::getValue('transaction_confirmation_code');
        }
        /*Building the url where to send the transaction confirmation data*/
        $url = 'https://www.wecashup.com/api/v2.0/merchants/' . $merchant_uid;
        $url .= '/transactions/' . $transaction_uid . '/?merchant_public_key=' . $merchant_public_key;


        $fields = array(
            'merchant_secret' => urlencode($merchant_secret),
            'transaction_token' => urlencode($transaction_token),
            'transaction_uid' => urlencode($transaction_uid),
            'transaction_confirmation_code' => urlencode($transaction_confirmation_code),
            'transaction_provider_name' => urlencode($transaction_provider_name),
            '_method' => urlencode('PATCH')
        );

        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($server_output, true);

        $location = _PS_BASE_URL_ . __PS_BASE_URI__ . 'order';
        if ($data['response_status'] == "success") {
            $mailVars = array(
                '{WECASHUP_MCURR}' => Configuration::get('WECASHUP_MCURR'),
                '{WECASHUP_CPAY}' => Configuration::get('WECASHUP_CPAY'),
                '{WECASHUP_TELECOM}' => Configuration::get('WECASHUP_TELECOM'),
                '{WECASHUP_MWALLET}' => Configuration::get('WECASHUP_MWALLET'),
                '{WECASHUP_SPAY}' => Configuration::get('WECASHUP_SPAY'),
            );

            $_a = (int) $cart->id;
            $_b = Configuration::get('PS_OS_PREPARATION');
            $_d = $this->module->displayName;
            $_g = (int) $currency->id;
            $_i = $customer->secure_key;
            $this->module->validateOrder($_a, $_b, $total, $_d, null, $mailVars, $_g, false, $_i);
            $data = array(
                'user_id' => $this->context->customer->id,
                'order_id' => Order::getIdByCartId($cart->id),
                'transaction_uid' => $transaction_uid,
                'transaction_token' => $transaction_token,
                'transaction_provider_name' => $transaction_provider_name,
                'transaction_confirmation_code' => $transaction_confirmation_code,
                'status' => 1
            );

            Db::getInstance()->insert('wecashup_data', $data);
            $location = _PS_BASE_URL_.__PS_BASE_URI__.'order-confirmation?id_cart='.$cart->id;
            $location .= '&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder;
            $location .= '&key=' . $customer->secure_key;
        } else {
            $location = _PS_BASE_URL_ . __PS_BASE_URI__ . 'order';
        }

        echo '<script>top.window.location = "' . $location . '"</script>';
        return null;
    }
}
