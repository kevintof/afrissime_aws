<?php

/**
* 2018 PushAssist (c)
*
* NOTE - You must have a PushAssist account to use this!
*
*  @author    PushAssist
*  @copyright PushAssist
*  @version   1.0
*  @license   https://pushassist.com
*/


if (!defined('_PS_VERSION_'))
  exit;

class pushassist extends Module
{
	public function __construct()
  {
    $this->name = 'pushassist';
    $this->tab = 'front_office_features';
    $this->version = '1.0';
    $this->author = 'PushAssist';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.4', 'max' => '1.7');
     
    parent::__construct();
 
    $this->displayName = $this->l('PushAssist');
    $this->description = $this->l('Installing the PushAssist module will automatically insert the PushAssist Smart Code on your PrestaShop website. To get started, you just have to copy the PushAssist Registered WebSite Sub Domain and paste it in this module\'s configuration.');
    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
 
    if (!Configuration::get('PUSHASSIST_NAME'))
      $this->warning = $this->l('No name provided');
  }

public function install()
{
  if (Shop::isFeatureActive())
    Shop::setContext(Shop::CONTEXT_ALL);
 
  return parent::install() &&
    $this->registerHook('header') &&
    Configuration::updateValue('PUSHASSIST_NAME', '');
  }


public function uninstall()
{
  return parent::uninstall() && Configuration::deleteByName('PUSHASSIST_NAME');
}

public function hookDisplayHeader()
{  
  $this->context->smarty->assign(
      array(
          'my_module_name' => Configuration::get('PUSHASSIST_NAME'),          
      )
  );
  return $this->display(__FILE__, 'pushassist.tpl');
}  

public function getContent()
{
    $output = null;
 
  if (Tools::isSubmit('submit'.$this->name))
    {
        $my_module_name = strval(Tools::getValue('PUSHASSIST_NAME'));						
        if (!empty($my_module_name))
        {
			Configuration::updateValue('PUSHASSIST_NAME', $my_module_name);
            $output .= $this->displayConfirmation($this->l('Settings updated'));
        }
        else
        {
			Configuration::updateValue('PUSHASSIST_NAME', $my_module_name);
			$output .= $this->displayError($this->l('Invalid value'));
        }
    } 
    return $output.$this->displayForm();
}

public function displayForm()
{
    // Get default language
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
     
    // Init Fields form array
    $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Settings'),
        ),
        'input' => array(
            array(
                'type' => 'text',
                'label' => $this->l('PushAssist WebSite Sub Domain'),
                'name' => 'PUSHASSIST_NAME',
                'size' => 100,
                'required' => true
            )
        ),
        'submit' => array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-left'
        )
    );
     
    $helper = new HelperForm();
     
    // Module, token and currentIndex
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
     
    // Language
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;
     
    // Title and toolbar
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'submit'.$this->name;
    $helper->toolbar_btn = array(
        'save' =>
        array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        )
    );
     
    // Load current value
    $helper->fields_value['PUSHASSIST_NAME'] = Configuration::get('PUSHASSIST_NAME');
     
    return $helper->generateForm($fields_form);
}

}
?>