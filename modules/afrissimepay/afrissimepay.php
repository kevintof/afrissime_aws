<?php



/**

 * 2007-2017 PrestaShop

 *

 * NOTICE OF LICENSE

 *

 * This source file is subject to the Academic Free License (AFL 3.0)

 * that is bundled with this package in the file LICENSE.txt.

 * It is also available through the world-wide-web at this URL:

 * http://opensource.org/licenses/afl-3.0.php

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to license@prestashop.com so we can send you a copy immediately.

 *

 * DISCLAIMER

 *

 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer

 * versions in the future. If you wish to customize PrestaShop for your

 * needs please refer to http://www.prestashop.com for more information.

 *

 *  @author    Hennes Hervé <contact@h-hennes.fr>

 *  @copyright 2013-2017 Hennes Hervé

 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

 *  http://www.h-hennes.fr/blog/

 */



use PrestaShop\PrestaShop\Core\Payment\PaymentOption;



if (!defined('_PS_VERSION_')) {

    exit;

}



class AfrissimePay extends PaymentModule

{

    protected $_html;



    public function __construct()

    {

        $this->author    = 'Hjibidar';

        $this->name      = 'afrissimepay';

        $this->tab       = 'payment_gateways';

        $this->version   = '0.1.0';

        $this->bootstrap = true;

        parent::__construct();



        $this->displayName = $this->l('Afrissime Pay');

        $this->description = $this->l('Moyen de paiement mobile via Paygateglobal');

    }



    public function install()

    {

        if (!parent::install() 

            || !$this->registerHook('paymentOptions')

            || !$this->registerHook('paymentReturn')

            ) {

            return false;

        }

        return true;

    }



    /**

     * Affichage du paiement dans le checkout

     * PS 17 

     * @param type $params

     * @return type

     */

    public function hookPaymentOptions($params) {



        if (!$this->active) {

            return;

        }
        /*
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }
		*/
        $apiPayement = new PaymentOption();

        $apiPayement->setModuleName($this->name)

                ->setCallToActionText($this->l('Paiment par Flooz et Tmoney'))

                //Définition d'un formulaire personnalisé

                ->setAction($this->context->link->getModuleLink($this->name, 'api', array(), true))

                ->setInputs([

                'auth_token' => [

                'name' =>'token',

                'type' =>'hidden',

                'value' =>Configuration::get('PAYMENT_API_KEY'),

                ],

                'url' => [

                'name' =>'token',

                'type' =>'hidden',

                'value' =>Configuration::get('PAYMENT_API_URL_SUCESS'),

                ],

                'amount' => [

                'name' =>'token',

                'type' =>'hidden',

                'value' =>$this->context->cart->getOrderTotal(true, Cart::BOTH),

                ],

                'identifier' => [

                'name' =>'token',

                'type' =>'hidden',

                'value' =>"ZUZIO",

                ],

                ])

                ->setForm($this->fetch('module:afrissimepay/views/templates/hook/payment_api_form.tpl'))

                ->setAdditionalInformation($this->fetch('module:afrissimepay/views/templates/hook/displayPaymentApi.tpl'));

                //->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/payment.png'));

        return [$apiPayement];

    }



    /**

     * Information de paiement api

     * @return array

     */

    public function getPaymentApiVars()

    {

        return  [

             'payment_key' => Configuration::get('PAYMENT_API_KEY'),

             'payment_url' => Configuration::get('PAYMENT_API_URL'),

             'success_url' => Configuration::get('PAYMENT_API_URL_SUCESS'),

             'error_url' => Configuration::get('PAYMENT_API_URL_ERROR'),

             'id_cart' => $this->context->cart->id,

             'cart_total' =>  $this->context->cart->getOrderTotal(true, Cart::BOTH),

             'id_customer' => $this->context->cart->id_customer,

        ];

    }

    

    /**

     * Affichage du message de confirmation de la commande

     * @param type $params

     * @return type

     */

    public function hookDisplayPaymentReturn($params) 

    {

        if (!$this->active) {

            return;

        }

        

        $this->smarty->assign(

            $this->getTemplateVars()

            );

        return $this->fetch('module:afrissimepay/views/templates/hook/payment_return.tpl');

    }



    /**

     * Configuration admin du module

     */

    public function getContent()

    {

        $this->_html .=$this->postProcess();

        $this->_html .= $this->renderForm();



        return $this->_html;



    }



    /**

     * Traitement de la configuration BO

     * @return type

     */

    public function postProcess()

    {

        if ( Tools::isSubmit('SubmitPaymentConfiguration'))

        {

            Configuration::updateValue('PAYMENT_API_URL', Tools::getValue('PAYMENT_API_URL'));

            Configuration::updateValue('PAYMENT_API_KEY', Tools::getValue('PAYMENT_API_KEY'));

            Configuration::updateValue('PAYMENT_API_URL_SUCESS', Tools::getValue('PAYMENT_API_URL_SUCESS'));

            Configuration::updateValue('PAYMENT_API_URL_ERROR', Tools::getValue('PAYMENT_API_URL_ERROR'));

        }

        return $this->displayConfirmation($this->l('Configuration updated with success'));

    }



     /**

     * Formulaire de configuration admin

     */

    public function renderForm()

    {

        $fields_form = [

            'form' => [

                'legend' => [

                    'title' => $this->l('Payment Configuration'),

                    'icon' => 'icon-cogs'

                ],

                'description' => $this->l('Sample configuration form'),


                'input' => [
                    [

                        'type' => 'text',

                        'label' => $this->l('Clé API'),

                        'name' => 'PAYMENT_API_KEY',

                        'required' => true,

                        'empty_message' => $this->l('Please fill the payment api key'),

                   ],

                   [

                        'type' => 'text',

                        'label' => $this->l('Payment api url'),

                        'name' => 'PAYMENT_API_URL',

                        'required' => true,

                        'empty_message' => $this->l('Please fill the payment api url'),

                   ],

                   [

                        'type' => 'text',

                        'label' => $this->l('Payment api success url'),

                        'name' => 'PAYMENT_API_URL_SUCESS',

                        'required' => true,

                        'empty_message' => $this->l('Please fill the payment api success url'),

                    ],

                    [

                        'type' => 'text',

                        'label' => $this->l('Payment api error url'),

                        'name' => 'PAYMENT_API_URL_ERROR',

                        'required' => true,

                        'empty_message' => $this->l('Please fill the payment api error url'),

                    ],

                ],

                'submit' => [

                    'title' => $this->l('Save'),

                    'class' => 'button btn btn-default pull-right',

                ],

            ],

            ];



        $helper = new HelperForm();

        $helper->show_toolbar = false;

        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');

        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

        $helper->id = 'afrissimepay';

        $helper->identifier = 'afrissimepay';

        $helper->submit_action = 'SubmitPaymentConfiguration';

        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;

        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = [

            'fields_value' => $this->getConfigFieldsValues(),

            'languages' => $this->context->controller->getLanguages(),

            'id_language' => $this->context->language->id

        ];



        return $helper->generateForm(array($fields_form));

    }



    /**

     * Récupération des variables de configuration du formulaire admin

     */

    public function getConfigFieldsValues()

    {

        return [

            'PAYMENT_API_URL' => Tools::getValue('PAYMENT_API_URL', Configuration::get('PAYMENT_API_URL')),
            'PAYMENT_API_KEY' => Tools::getValue('PAYMENT_API_KEY', Configuration::get('PAYMENT_API_KEY')),

            'PAYMENT_API_URL_SUCESS' => Tools::getValue('PAYMENT_API_URL_SUCESS', Configuration::get('PAYMENT_API_URL_SUCESS')),

            'PAYMENT_API_URL_ERROR' => Tools::getValue('PAYMENT_API_URL_ERROR', Configuration::get('PAYMENT_API_URL_ERROR')),

        ];

    }





    /**

     * Récupération des informations du template

     * @return array

     */

    public function getTemplateVars()

    {

        return [

            'shop_name' => $this->context->shop->name,

            'custom_var' => $this->l('My custom var value'),

            'payment_details' => $this->l('custom details'),

        ];

    }



}