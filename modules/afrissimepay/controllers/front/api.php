<?php



/**

 * 2007-2016 PrestaShop

 *

 * NOTICE OF LICENSE

 *

 * This source file is subject to the Academic Free License (AFL 3.0)

 * that is bundled with this package in the file LICENSE.txt.

 * It is also available through the world-wide-web at this URL:

 * http://opensource.org/licenses/afl-3.0.php

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to license@prestashop.com so we can send you a copy immediately.

 *

 * DISCLAIMER

 *

 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer

 * versions in the future. If you wish to customize PrestaShop for your

 * needs please refer to http://www.prestashop.com for more information.

 *

 *  @author     Hervé Jibidar <support@afrissime.com>

 *  @copyright 2013-2016 Hervé Jibidar

 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

 *  http://www.afrissime.com

 */

class afrissimepayapiModuleFrontController extends ModuleFrontController {

     public function initContent()

    {

        parent::initContent();

        //Vérification générales 

        $cart = $this->context->cart;



        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active) {

            Tools::redirect('index.php?controller=order&step=1');

        }

        $currency = $this->context->currency;

        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        // Check if customer exists
        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');

        $this->process_payment($customer);

    }
    private function post_paygateglobal_api($url, $data = [], $header = [])
    {
        $strGetField = http_build_query($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $strGetField);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($header, [
            'Content-Type: application/x-www-form-urlencoded;charset=utf-8',
            'Content-Length: ' . mb_strlen($strPostField)
        ]));

        return curl_exec($ch);
    }
    private function process_payment($customer) {
        $commande = urlencode(json_encode($this->get_paygategloabal_args($customer)));

        $ch = curl_init();
        $api_key = Configuration::get('PAYMENT_API_KEY');

        $url = 'https://paygateglobal.com/v1/page';
        $result = post_paygateglobal_api($url, $commande, [
            "API_KEY: ".$api_key,
            //"API_SECRET: ".$api_secret
        ]);
        die($result);
        $jsonResponse = json_decode($result, true);
            // Validate order
            $cart = $this->context->cart;
            $currency = $this->context->currency;
            $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
            $extra_vars = array(
                '{total_to_pay}' => Tools::displayPrice($total)
            );
            $this->module->validateOrder($cart->id, Configuration::get('PS_OS_PayPlus_PAYMENT'), $total,
                $this->module->displayName, NULL, $extra_vars, (int)$currency->id, false, $cart->secure_key);

            Tools::redirectLink($response_decoded->response_text);
            die();
    }
    private function get_paygategloabal_args($customer) {
        $cart = $this->context->cart;
        $order_cart_id = $cart->id;
        $order_total_amount = $cart->getOrderTotal(true, Cart::BOTH);
        $ttx = $order_total_amount - $cart->getOrderTotal(false);
        $order_total_tax_amount = $ttx < 0 ? : $ttx;
        $order_cart_secure_key = $cart->secure_key;
        $order_items = $cart->getProducts(true);
        $order_total_shipping_amount = $cart->getOrderTotal(false, Cart::ONLY_SHIPPING);
        $order_return_url = $this->context->link->getPageLink('order-confirmation', null, null, 'key='.$cart->secure_key.'&id_cart='.$cart->id.'&id_module='.$this->module->id);

        $items = $order_items;
        $paygateglobal_items = array();
        foreach ($items as $item) {
            $paygateglobal_items[] = array(
                "name" => $item['name'],
                "quantity" => $item['cart_quantity'],
                "unit_price" => number_format((float)$item['price'], 2, '.', ''),
                "total_price" => number_format((float)$item['total'], 2, '.', ''),
                "description" => strip_tags($item['description_short'])
            );
        }

        if($order_total_shipping_amount > 0){
            $paygateglobal_items[] = array(
                "name" => "Frais de livraison",
                "quantity" => 1,
                "unit_price" => $order_total_shipping_amount,
                "total_price" => $order_total_shipping_amount,
                "description" => ""
            );
        }

        $paygateglobal_args = array(
            "invoice" => array(
                "items" => $payplus_items,
                "taxes" => array(),
                "total_amount" => $order_total_amount,
                "description" => "Paiement de " . $order_total_amount . " FCFA pour article(s) achetés sur " . Configuration::get('PS_SHOP_NAME')
            ), "store" => array(
                "name" => Configuration::get('PS_SHOP_NAME'),
                "website_url" => Tools::getHttpHost( true ).__PS_BASE_URI__
            ), "actions" => array(
                "cancel_url" => Tools::getHttpHost( true ).__PS_BASE_URI__,
                "callback_url" => $this->context->link->getModuleLink('afrissimepay', 'validationipn'),
                "return_url" => $order_return_url
            ), "custom_data" => array(
                "cart_id" => $order_cart_id,
                "cart_secure_key" => $order_cart_secure_key,
                "Taxes" => $order_total_tax_amount,
            )
        );

        return $paygateglobal_args;
    }
}

