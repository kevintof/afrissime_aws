<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Arthur Cai
 * @copyright 2018 Adeptmind
 * @license   LICENSE
 */

include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/product_util.php';
include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/shop.php';

class AdeptInstallController extends ModuleAdminController
{
    public function initContent()
    {
        try {
            $this->module->sendCallback();
            AdeptProductUtil::install($this->context);
        } catch (Exception $e) {
            AdeptShop::reportException('PRESTASHOP_SCRAPE_FAILED', $e);
        }
    }
}
