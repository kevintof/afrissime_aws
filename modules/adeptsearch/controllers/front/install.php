<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Arthur Cai
 * @copyright 2018 Adeptmind
 * @license   LICENSE
 */

include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/util.php';
include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/product_util.php';
include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/shop.php';

class AdeptSearchInstallModuleFrontController extends ModuleFrontController
{
    public function checkAccess()
    {
        try {
            $nonce = Tools::getValue('nonce');
            if (!$nonce) {
                return false;
            }
            $shop_id = Configuration::get('ADEPTMIND_SHOP_UUID');
            $result = AdeptUtil::callPlatformManagerPost(
                '/shops/'.$shop_id.'/verify',
                array('nonce' => $nonce)
            );
            return AdeptUtil::idx(json_decode($result, true), 'valid');
        } catch (Exception $e) {
            AdeptShop::reportException('PRESTASHOP_SCRAPE_FRONT_FAILED', $e);
            return false;
        }
    }

    public function initContent()
    {
        try {
            $this->module->sendCallback();
            $is_delta_rescrape = Tools::getValue('is_delta_rescrape', 0);
            if ($is_delta_rescrape === 'true' || (int) $is_delta_rescrape) {
                AdeptProductUtil::deltaRescrape($this->context);
            } else {
                $start_index = (int) Tools::getValue('start', 0);
                AdeptProductUtil::install($this->context, $start_index);
            }
        } catch (Exception $e) {
            AdeptShop::reportException('PRESTASHOP_SCRAPE_FRONT_FAILED', $e);
        }
    }
}
