<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Arthur Cai
 * @copyright 2018 Adeptmind
 * @license   LICENSE
 */

class AdeptUtil
{
    const API_DOMAIN = 'api.adeptmind.ai';

    public static function callPlatformManagerGet($path, $params = array())
    {
        $token = Configuration::get('ADEPTMIND_SHOP_TOKEN');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt(
            $curl,
            CURLOPT_URL,
            sprintf(
                'https://%s/platform-ext%s?%s',
                self::API_DOMAIN,
                $path,
                http_build_query($params)
            )
        );
        if ($token) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        }
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        $result = curl_exec($curl);
        if (curl_error($curl)) {
            throw new Exception(curl_error($curl));
        }
        return $result;
    }

    private static function callPlatformManagerImpl($setup, $path, $params)
    {
        $token = Configuration::get('ADEPTMIND_SHOP_TOKEN');
        $curl = curl_init();
        $setup($curl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt(
            $curl,
            CURLOPT_URL,
            sprintf('https://%s/platform-ext%s', self::API_DOMAIN, $path)
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, Tools::jsonEncode($params));
        $headers = array('Content-Type: application/json');
        if ($token) {
            $headers[] = 'Authorization: Bearer '.$token;
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($curl);
        if (curl_error($curl)) {
            throw new Exception(curl_error($curl));
        }
        return $result;
    }

    public static function callPlatformManagerPost($path, $params = array())
    {
        return self::callPlatformManagerImpl(
            function ($curl) {
                curl_setopt($curl, CURLOPT_POST, true);
            },
            $path,
            $params
        );
    }

    public static function callPlatformManagerPut($path, $params = array())
    {
        return self::callPlatformManagerImpl(
            function ($curl) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            },
            $path,
            $params
        );
    }

    public static function callPlatformManagerDelete($path, $params = array())
    {
        return self::callPlatformManagerImpl(
            function ($curl) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            },
            $path,
            $params
        );
    }

    public static function getSlug($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = Tools::strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public static function snakeToCamelCase($str)
    {
        $camel = str_replace(' ', '', ucwords(str_replace('_', ' ', $str)));
        $camel[0] = Tools::strtolower($camel[0]);
        return $camel;
    }

    public static function idx($arr, $key, $default = null)
    {
        return array_key_exists($key, $arr) ? $arr[$key] : $default;
    }

    public static function getMemoryUsage()
    {
        if (function_exists('memory_get_usage')) {
            return memory_get_usage();
        }
        return null;
    }

    public static function getLoadAverage()
    {
        if (function_exists('sys_getloadavg')) {
            return sys_getloadavg();
        }
        return null;
    }

    public static function getMemoryLimit()
    {
        $memory_limit = ini_get('memory_limit');
        if (!$memory_limit) {
            return null;
        }
        return self::convertShortNumber($memory_limit);
    }

    public static function convertShortNumber($val)
    {
        $val = trim($val);
        $val = trim($val, 'bB');
        $last = Tools::strtolower($val[Tools::strlen($val)-1]);
        switch ($last) {
            // The 'G' modifier is available since PHP 5.1.0
            // fucking linter
            case 'g':
                $val *= 1024;
            // fucking linter
            case 'm':
                $val *= 1024;
            // fucking linter
            case 'k':
                $val *= 1024;
        }

        return $val;
    }

    public static function stripTags($string)
    {
        if (empty($string)) {
            return $string;
        }

        // ----- remove HTML TAGs -----
        $string = preg_replace('/<[^>]*>/', ' ', $string);

        // ----- remove control characters -----
        $string = str_replace("\r", '', $string);    // --- replace with empty space
        $string = str_replace("\n", ' ', $string);   // --- replace with space
        $string = str_replace("\t", ' ', $string);   // --- replace with space

        $string = str_replace("\\n", ' ', $string);

        // ----- remove multiple spaces -----
        $string = trim(preg_replace('/ {2,}/', ' ', $string));

        return $string;
    }

    public static function getShopId()
    {
        $shop_id = Configuration::get('ADEPTMIND_SHOP_UUID');
        if (!$shop_id) {
            throw new Exception('Shop ID not set');
        }
        return $shop_id;
    }

    public static function getImportProcessId()
    {
        $import_process_id = Configuration::get('ADEPTMIND_IMPORT_PROCESS_ID');
        if (!$import_process_id) {
            throw new Exception('Import process ID not set');
        }
        return $import_process_id;
    }
}
