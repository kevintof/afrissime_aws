<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Arthur Cai
 * @copyright 2018 Adeptmind
 * @license   LICENSE
 */

include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/product_fetcher.php';
include_once _PS_MODULE_DIR_ . 'adeptsearch/sql/AdeptSearchRescrapeQueue.php';

class AdeptProductUtil
{
    private static function processAttributes($attribute_combinations)
    {
        $attributes_by_id = array();
        $colors_by_attribute = array();
        $color_infos = array();
        foreach ($attribute_combinations as $attribute) {
            $id_product_attribute = $attribute['id_product_attribute'];
            $group_name = $attribute['group_name'];
            $attribute_name = $attribute['attribute_name'];
            $id_attribute = $attribute['id_attribute'];
            if (!isset($attributes_by_id[$id_product_attribute])) {
                $attributes_by_id[$id_product_attribute] = array(
                    'price' => $attribute['price'],
                    'quantity' => $attribute['quantity'],
                    'fields' => array($group_name => $attribute_name),
                );
            } else {
                $attributes_by_id[$id_product_attribute]['fields'][$group_name] = $attribute_name;
            }
            if ($attribute['is_color_group']) {
                $colors_by_attribute[$id_product_attribute] = $id_attribute;
                if (!isset($color_infos[$id_attribute])) {
                    $color_infos[$id_attribute] = array(
                        'color' => $attribute_name,
                        'model_id' => $id_attribute,
                        'color_hex' => array((new Attribute($id_attribute))->color)
                    );
                }
            }
        }

        $default_color = array('color' => '', 'model_id' => '', 'color_hex' => array());
        $final_attributes = array();
        foreach ($attributes_by_id as $id_product_attribute => $attribute) {
            $color = AdeptUtil::idx($colors_by_attribute, $id_product_attribute, '');
            if (!isset($final_attributes[$color])) {
                $final_attributes[$color] = array_merge(
                    AdeptUtil::idx($color_infos, $color, $default_color),
                    array('variants' => array($attribute))
                );
            } else {
                $final_attributes[$color]['variants'][] = $attribute;
            }
        }
        return $final_attributes;
    }

    private static function getCategoryNames($product_id, $lang)
    {
        return array_map(
            function ($category) {
                return $category['name'];
            },
            Product::getProductCategoriesFull($product_id, $lang)
        );
    }

    private static function getTopology($product_id, $lang)
    {
        $default_category = new Category($product_id, $lang);
        $topologies = array($default_category->name);
        foreach ($default_category->getAllParents($lang) as $parent_category) {
            if ($parent_category->id_parent != 0 && !$parent_category->is_root_category) {
                $topologies[] = $parent_category->name;
            }
        }
        return $topologies;
    }

    private static function processProduct($product_arr, $context)
    {
        $lang = $context->language->id;
        $product = new Product($product_arr['id_product']);
        $product_arr['id'] = $product->id;
        $product_arr['category_names'] = self::getCategoryNames($product->id, $lang);
        $product_arr['topologies'] = self::getTopology($product->id_category_default, $lang);
        $product_arr['attributes'] = self::processAttributes(
            $product->getAttributeCombinations($lang)
        );
        $product_arr['tags'] = $product->getTags($lang);
        $link_rewrite = $product_arr['link_rewrite'];
        $product_arr['image'] = $context->link->getImageLink(
            $link_rewrite,
            Product::getCover($product->id)['id_image']
        );
        $product_arr['images'] = array();
        foreach ($product->getImages($lang) as $image) {
            $product_arr['images'][] = $context->link->getImageLink(
                $link_rewrite,
                $image['id_image']
            );
        }
        $product_arr['description'] = AdeptUtil::stripTags($product_arr['description']);
        return $product_arr;
    }

    public static function processProductsData($products, $context)
    {
        $processed_products = array();
        foreach ($products as $product) {
            $processed_products[] = self::processProduct($product, $context);
        }
        return $processed_products;
    }

    public static function getTotalProductCount()
    {
        $rq = Db::getInstance()->executeS(
            'SELECT COUNT(*) AS `count` FROM `'._DB_PREFIX_.'product` p '.
            Shop::addSqlAssociation('product', 'p').
            'WHERE product_shop.`visibility` IN ("both", "search")'.
            'AND product_shop.`active` = 1'
        );
        return AdeptUtil::idx(AdeptUtil::idx($rq, 0), 'count');
    }

    private static function emptyRescrapeQueue()
    {
        // The config update must come before calling emptyQueue()
        Configuration::updateValue('ADEPTMIND_WAITING_FOR_DELTA_RESCRAPE', false);
        Configuration::updateValue('ADEPTMIND_LAST_DELTA_RESCRAPE_TIME', date('Y-m-d H:i:s'));
        return AdeptSearchRescrapeQueue::emptyQueue();
    }

    public static function install($context, $start = 0)
    {
        $shop_id = AdeptUtil::getShopId();
        $import_process_id = AdeptUtil::getImportProcessId();
        self::emptyRescrapeQueue();
        self::scrapeImpl($shop_id, $import_process_id, $context, array('start' => $start));
    }

    public static function deltaRescrape($context)
    {
        $last_delta_rescrape_time = Configuration::get('ADEPTMIND_LAST_DELTA_RESCRAPE_TIME');
        $shop_id = AdeptUtil::getShopId();
        $import_process_id = AdeptUtil::getImportProcessId();
        $products_to_delete = array();
        $product_ids_to_rescrape = array();
        foreach (self::emptyRescrapeQueue() as $queue_item) {
            if ($queue_item['is_deleted']) {
                $products_to_delete[] = array('product_id' => $queue_item['id_product']);
            } else {
                $product_ids_to_rescrape[] = $queue_item['id_product'];
            }
        }
        self::scrapeImpl(
            $shop_id,
            $import_process_id,
            $context,
            array(
                'is_delta_rescrape' => true,
                'platform_manager_method' => 'AdeptUtil::callPlatformManagerPut',
                'product_ids' => $product_ids_to_rescrape,
                'deleted_products' => $products_to_delete,
                'since' => empty($last_delta_rescrape_time)
                    ? null
                    : strtotime($last_delta_rescrape_time),
                'full_rescrape_limit' => 2,
            )
        );
    }

    private static function getProductFetcher($context, $options)
    {
        return new AdeptProductFetcher('AdeptProductUtil::processProductsData', $context, $options);
    }

    private static function scrapeImpl($shop_id, $import_process_id, $context, $options = array())
    {
        $start = AdeptUtil::idx($options, 'start', 0);
        $deleted_products = AdeptUtil::idx($options, 'deleted_products', array());
        $total_product_count = self::getTotalProductCount();
        AdeptShop::trackEvent(
            'PRODUCT_SEND_START',
            array( 'total_product_count' => $total_product_count )
        );
        $memory_limit = AdeptUtil::getMemoryLimit();
        $options = array_merge($options, array('total_product_count' => $total_product_count));
        $product_fetcher = self::getProductFetcher($context, $options);
        $full_rescrape_limit = AdeptUtil::idx($options, 'full_rescrape_limit');
        if ($full_rescrape_limit && $product_fetcher->hasAtLeastNPages($full_rescrape_limit)) {
            unset(
                $options['platform_manager_method'],
                $options['product_ids'],
                $options['since'],
                $options['is_delta_rescrape']
            );
            $product_fetcher = self::getProductFetcher($context, $options);
        }
        foreach ($product_fetcher as $products) {
            $start += count($products);
            try {
                $memory_usage = AdeptUtil::getMemoryUsage();
                $result = call_user_func(
                    $product_fetcher->getPlatformManagerMethod(),
                    sprintf('/shops/%s/catalog/products', $shop_id),
                    array(
                        'shopId' => $shop_id,
                        'platformId' => 'prestashop',
                        'products' => $products,
                        'processId' => $import_process_id,
                        'index' => $start,
                        'terminate' => $memory_usage && ($memory_usage > $memory_limit * 0.9),
                    )
                );
                $result_arr = json_decode($result, true);
                if (is_array($result_arr) && AdeptUtil::idx($result_arr, 'terminate')) {
                    AdeptShop::trackEvent('PRODUCT_SEND_BREAK', array());
                    return;
                }
            } catch (Exception $e) {
                AdeptShop::trackEvent('PRODUCT_SEND_FAIL', $e);
            }
        }
        if (count($deleted_products)) {
            self::sendDeletedProducts($shop_id, $deleted_products);
        }
        self::sync($shop_id, AdeptUtil::idx($options, 'is_delta_rescrape', false));
    }

    private static function sendDeletedProducts($shop_id, $deleted_products)
    {
        return AdeptUtil::callPlatformManagerDelete(
            sprintf('/shops/%s/catalog/products', $shop_id),
            array('platformId' => 'prestashop', 'products' => $deleted_products)
        );
    }

    private static function sync($shop_id, $is_delta_rescrape = false)
    {
        AdeptShop::trackEvent('PRODUCT_SEND_SYNC', array());
        AdeptUtil::callPlatformManagerPut(
            sprintf('/shops/%s/catalog/sync', $shop_id),
            array('is_delta_rescrape' => $is_delta_rescrape)
        );
        AdeptShop::trackEvent('PRODUCT_SEND_COMPLETE', array());
    }

    public static function updateProduct($product, $is_delete = false)
    {
        $shop_id = Configuration::get('ADEPTMIND_SHOP_UUID');
        AdeptSearchRescrapeQueue::insert(
            $product->id,
            $is_delete ||
                ($product->visibility !== 'both' && $product->visibility === 'search') ||
                !$product->active
        );
        if (!Configuration::get('ADEPTMIND_WAITING_FOR_DELTA_RESCRAPE')) {
            AdeptUtil::callPlatformManagerPost(
                sprintf('/shops/%s/catalog/schedulerescrape', $shop_id)
            );
            Configuration::updateValue('ADEPTMIND_WAITING_FOR_DELTA_RESCRAPE', true);
        }
    }
}
