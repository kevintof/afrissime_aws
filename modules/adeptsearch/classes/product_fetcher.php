<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Arthur Cai
 * @copyright 2018 Adeptmind
 * @license   LICENSE
 */

include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/util.php';
include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/shop.php';

class AdeptProductFetcher implements Iterator
{
    const MIN_DB_BATCH = 100;
    const MAX_DB_CALLS = 100;
    const MAX_PAYLOAD_SIZE = 1000000;

    private $dbIndex;
    private $buffer;
    private $queue;
    private $processor;
    private $context;
    private $returnedProducts;
    private $hasMoreProducts;
    private $batchSize;
    private $start;
    private $since;
    private $productIds;
    private $platformManagerMethod;

    public function __construct($processor, $context, $options = array())
    {
        $this->start = AdeptUtil::idx($options, 'start', 0);
        $this->dbIndex = $this->start;
        $this->buffer = array();
        $this->queue = array();
        $this->processor = $processor;
        $this->context = $context;
        $this->returnedProducts = 0;
        $this->hasMoreProducts = true;
        $this->batchSize = (int) max(
            self::MIN_DB_BATCH,
            (float) AdeptUtil::idx($options, 'total_product_count', 10000) / self::MAX_DB_CALLS
        );
        $this->since = AdeptUtil::idx($options, 'since');
        $product_ids = AdeptUtil::idx($options, 'product_ids', array());
        $this->productIds = array_combine($product_ids, $product_ids);
        $this->platformManagerMethod = AdeptUtil::idx(
            $options,
            'platform_manager_method',
            'AdeptUtil::callPlatformManagerPost'
        );
    }

    public function current()
    {
        return $this->buffer;
    }

    public function key()
    {
        return $this->returnedProducts;
    }

    public function next()
    {
        $this->returnedProducts += count($this->buffer);
        $this->buffer = array();

        while ($this->hasMoreProducts && Tools::strlen(Tools::jsonEncode($this->queue)) < self::MAX_PAYLOAD_SIZE) {
            list($products, $has_more_products, $new_index) = $this->fetchProducts($this->dbIndex);
            $this->dbIndex = $new_index;
            $this->hasMoreProducts = $has_more_products;
            $this->queue = array_merge($this->queue, $products);
        }
        for ($end = count($this->queue); $end > 0; $end--) {
            $product_slice = array_slice($this->queue, 0, $end);
            if ($end === 1 ||  Tools::strlen(Tools::jsonEncode($product_slice)) < self::MAX_PAYLOAD_SIZE) {
                $this->queue = array_slice($this->queue, $end);
                $this->buffer = $product_slice;
                break;
            }
        }
    }

    public function rewind()
    {
        $this->dbIndex = $this->start;
        $this->buffer = array();
        $this->queue = array();
        $this->returnedProducts = 0;
        $this->hasMoreProducts = true;
        $this->next();
    }

    public function valid()
    {
        return $this->hasMoreProducts || !empty($this->buffer);
    }

    private function fetchProducts($index)
    {
        $lang = $this->context->language->id;
        $raw_products = Db::getInstance()->executeS(
            'SELECT p.*, product_shop.*, pl.* , m.`name` AS manufacturer_name, s.`name` AS supplier_name
            FROM `'._DB_PREFIX_.'product` p
            '.Shop::addSqlAssociation('product', 'p').'
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON '.'
            (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
            LEFT JOIN `'._DB_PREFIX_.'supplier` s ON (s.`id_supplier` = p.`id_supplier`)
            WHERE pl.`id_lang` = '.(int)$lang.'
            AND product_shop.`visibility` IN ("both", "search")
            AND product_shop.`active` = 1
            ORDER BY pl.`name` asc
            LIMIT '.(int)$index.','.(int)($this->batchSize + 1)
        );
        if (($index - $this->start) % ($this->batchSize * 10) === 0) {
            AdeptShop::trackEvent(
                'PRODUCT_SEND_PROGRESS',
                array(
                    'current_index' => $index,
                    'memory_usage' => AdeptUtil::getMemoryUsage(),
                    'load_average' => AdeptUtil::getLoadAverage(),
                )
            );
        }
        $has_more_products = count($raw_products) > $this->batchSize;
        $products = call_user_func(
            $this->processor,
            Product::getProductsProperties(
                $lang,
                $this->filterProducts(array_slice($raw_products, 0, $this->batchSize))
            ),
            $this->context
        );
        return array($products, $has_more_products, $index + $this->batchSize);
    }

    private function filterProducts($products)
    {
        $filtered_products = array();
        foreach ($products as $product) {
            if ($product['visibility'] !== 'both' && $product['visibility'] !== 'search') {
                continue;
            }
            if (!empty($this->since) && !(
                strtotime($product['date_upd']) >= $this->since ||
                array_key_exists($product['id_product'], $this->productIds)
            )) {
                continue;
            }
            $filtered_products[] = $product;
        }
        return $filtered_products;
    }

    public function hasAtLeastNPages($pages)
    {
        foreach ($this as $_) {
            $pages--;
            if ($pages <= 0) {
                return true;
            }
        }
        return false;
    }

    public function getPlatformManagerMethod()
    {
        return $this->platformManagerMethod;
    }
}
