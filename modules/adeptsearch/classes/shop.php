<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Ted Sczelecki
 * @copyright 2018 Adeptmind
 * @license   LICENSE
 */

class AdeptShop
{
    private static $shop = null;
    private static $customizations = null;
    
    private static $defaultCustomizations = array(
        'button_background' => '#333333',
        'button_color' => '#eeeeee',
        'field_background' => '#ffffff',
        'field_color' => '#000000',
        'field_border_color' => '#333333',
    );
    
    public static function getShop()
    {
        if (self::$shop == null) {
            $result = AdeptUtil::callPlatformManagerGet(
                '/shops',
                array(
                    'shop_id' => Configuration::get('ADEPTMIND_SHOP_UUID')
                )
            );
            self::$shop = json_decode($result, true)[0];
        }
        
        return self::$shop;
    }
    
    public static function getCustomizations()
    {
        if (self::$shop == null) {
            self::getShop();
        }
        
        if (self::$customizations == null) {
            try {
                $result = AdeptUtil::callPlatformManagerGet(
                    sprintf('/shops/%s/configs', self::$shop['shop_id']),
                    array()
                );
                $settings = json_decode($result, true);
                self::$customizations = array_replace(
                    self::$defaultCustomizations,
                    isset($settings['search-box']) ? $settings['search-box'] : array(),
                    isset($settings['general']) ? $settings['general'] : array()
                );
            } catch (Exception $e) {
                return self::$defaultCustomizations;
            }
        }
        return self::$customizations;
    }

    public static function trackEvent($event_key, $event_data)
    {
        if (self::$shop == null) {
            self::getShop();
        }
        return AdeptUtil::callPlatformManagerPost(
            sprintf('/shops/%s/trackevent', self::$shop['shop_id']),
            array('key' => $event_key, 'data' => $event_data)
        );
    }

    private static function getShopToken()
    {
        if (!self::$shop || !array_key_exists('shopToken', self::$shop)) {
            throw new Exception('No shop token');
        }
        return self::$shop['shopToken'];
    }

    private static function getShopPublicToken()
    {
        if (!self::$shop || !array_key_exists('publicToken', self::$shop)) {
            throw new Exception('No public shop token');
        }
        return self::$shop['publicToken'];
    }

    private static function getShopUUID()
    {
        if (!self::$shop || !array_key_exists('shop_id', self::$shop)) {
            throw new Exception('No shop UUID');
        }
        return self::$shop['shop_id'];
    }

    private static function getImportProcessID()
    {
        if (!self::$shop || !array_key_exists('platformConfigs', self::$shop)) {
            throw new Exception('No platform configs');
        }
        $process_id_obj = current(array_filter(self::$shop['platformConfigs'], function ($config) {
            return array_key_exists('key', $config) &&
                array_key_exists('value', $config) &&
                $config['key'] === 'process_id';
        }));
        if (!$process_id_obj) {
            throw new Exception('No import process ID');
        }
        return $process_id_obj['value'];
    }

    public static function install($data)
    {
        $result = AdeptUtil::callPlatformManagerGet('/shops/install', $data);
        self::$shop = json_decode($result, true);
        Configuration::updateValue('ADEPTMIND_SHOP_TOKEN', self::getShopToken());
        Configuration::updateValue('ADEPTMIND_SHOP_PUBLIC_TOKEN', self::getShopPublicToken());
        Configuration::updateValue('ADEPTMIND_SHOP_UUID', self::getShopUUID());
        Configuration::updateValue('ADEPTMIND_IMPORT_PROCESS_ID', self::getImportProcessID());
        self::trackEvent(
            'PRESTASHOP_INSTALL_SUCCESS',
            array(
                'module_version' => AdeptUtil::idx($data, 'module_version'),
                'ps_version' => _PS_VERSION_,
                'php_version' => phpversion(),
                'os_name' => php_uname(),
                'memory_limit' => AdeptUtil::getMemoryLimit(),
                'total_product_count' => AdeptProductUtil::getTotalProductCount(),
            )
        );
        return true;
    }

    public static function reportException($key, $exception)
    {
        try {
            self::trackEvent($key, array(
                'error' => $exception->getMessage()."\n".$exception->getTraceAsString()
            ));
        } catch (Exception $e) {
        }
    }
}
