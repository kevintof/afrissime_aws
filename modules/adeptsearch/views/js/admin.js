/*!
 * Adeptmind search admin for prestashop
 * https://github.com/adeptmind adept-platform-prestashop-module.git
 *
 * Copyright (C) 2018 Adeptmind
 */
(function(window){
  window.addEventListener('DOMContentLoaded', function () {
    window.adeptmind.platform.onMessage = function(event, data) {
      switch(event) {
        case AdeptPlatformAdapter.EVENTS.ON_HEIGHT_SET:
          break;
        default:
          Toastify({
            text: data.message,
            duration: 30000,
            newWindow: true,
            close: true,
            gravity: "top", // `top` or `bottom`
            positionLeft: false, // `true` or `false`
            backgroundColor: "linear-gradient(to right, #4BB543, #188210)",
          }).showToast();
          break;
      }
    };
  });
})(window);
