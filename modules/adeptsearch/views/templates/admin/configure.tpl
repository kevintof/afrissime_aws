{*!
* Adeptmind search admin for prestashop
* https://github.com/adeptmind adept-platform-prestashop-module.git
*
* Copyright (C) 2018 Adeptmind
*}

<link rel='stylesheet' href='{$ADEPT_BASE_URI}modules/adeptsearch/views/css/main.css'>

<script>
  window.adeptmind = window.adeptmind || {};
  window.adeptmind.app = window.adeptmind.app || {};
  window.adeptmind.app.config = window.adeptmind.app.config || {};
  window.adeptmind.platform = window.adeptmind.platform || {};

  window.adeptmind.app.config = {
    useMemoryRouter: true,
    showSaveButton: true
  };

  window.adeptmind.platform.onTerms = function() {
    if (!ADEPT_INSTALL_SCRIPT_URL) {
      console.error('ADEPT_INSTALL_SCRIPT_URL not defined');
      return;
    }
    const xhr = new XMLHttpRequest();
    xhr.open('POST', ADEPT_INSTALL_SCRIPT_URL);
    xhr.send();
  }
</script>

<div id='root'></div>

<script src='{$ADEPT_BASE_URI}modules/adeptsearch/views/js/app/runtime.js'></script>
<script src='{$ADEPT_BASE_URI}modules/adeptsearch/views/js/app/1.chunk.js'></script>
<script src='{$ADEPT_BASE_URI}modules/adeptsearch/views/js/app/main.chunk.js'></script>
