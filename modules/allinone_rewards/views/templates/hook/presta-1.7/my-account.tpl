{*
* All-in-one Rewards Module
*
* @category  Prestashop
* @category  Module
* @author    Yann BONNAILLIE - ByWEB
* @copyright 2012-2019 Yann BONNAILLIE - ByWEB (http://www.prestaplugins.com)
* @license   Commercial license see license.txt
* Support by mail  : contact@prestaplugins.com
* Support on forum : Patanock
* Support on Skype : Patanock13
*}
<!-- MODULE allinone_rewards -->
<li><a href="{url entity='module' name='allinone_rewards' controller='rewards'}" title="{l s='My rewards account' mod='allinone_rewards'}">{l s='My rewards account' mod='allinone_rewards'}</a></li>
<!-- END : MODULE allinone_rewards -->