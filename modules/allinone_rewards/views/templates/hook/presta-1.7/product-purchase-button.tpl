{*
* All-in-one Rewards Module
*
* @category  Prestashop
* @category  Module
* @author    Yann BONNAILLIE - ByWEB
* @copyright 2012-2019 Yann BONNAILLIE - ByWEB (http://www.prestaplugins.com)
* @license   Commercial license see license.txt
* Support by mail  : contact@prestaplugins.com
* Support on forum : Patanock
* Support on Skype : Patanock13
*}

<!-- MODULE allinone_rewards -->
<div id="aior_product_button">
	<button id="aior_add_to_cart" class="btn btn-primary">{l s='Buy with my rewards' mod='allinone_rewards'}</button>
	<span id="aior_add_to_cart_price"></span>
	<span id="aior_add_to_cart_available_after"></span>
</div>
<!-- END : MODULE allinone_rewards -->