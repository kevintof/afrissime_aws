<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Ps_Languageselector extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'ps_languageselector';
        $this->author = 'PrestaShop';
        $this->version = '2.0.2';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->trans('Language selector block', array(), 'Modules.Languageselector.Admin');
        $this->description = $this->trans('Adds a block allowing customers to select a language for your store\'s content.', array(), 'Modules.Languageselector.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:ps_languageselector/ps_languageselector.tpl';
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        $languages = Language::getLanguages(true, $this->context->shop->id);

        if (1 < count($languages)) {
            
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

            return $this->fetch($this->templateFile);
        }

        return false;
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $languages = Language::getLanguages(true, $this->context->shop->id);

        foreach ($languages as &$lang) {
            $lang['name_simple'] = $this->getNameSimple($lang['name']);
        }
        /*ajout du code pour afficher le code iso du pays du visiteur*/
        /*fin code*/
        //var_dump($this->context->cookie);
        
        if(isset($_GET['id_country']) && !empty($_GET['id_country'])){
            $pays_visiteur =Country::getIsoById($_GET['id_country']);
            $this->context->cookie->__set('pays_visiteur' , $pays_visiteur);
            $this->context->cookie->iso_code_country=$pays_visiteur;
            //$this->context->cookie->__set('id_country' , $_GET['id_country']);
        }
        /*
        if(isset($_GET['id_lang']) && !empty($_GET['id_lang'])){
            $this->context->cookie->__set('id_lang' , $_GET['id_lang']);
            $this->context->language->id=$_GET['id_lang'];
        }
        */
        return array(
            'languages' => $languages,
            'pays_visiteur'=>$this->context->cookie->iso_code_country,
            'country' => Country::getCountries($this->context->language->id, true),
            'current_language' => array(
                'id_lang' => $this->context->language->id,
                'name' => $this->context->language->name,
                'name_simple' => $this->getNameSimple($this->context->language->name)
            )
        );
    }
    /*ajout du code pour la vérification de la geolocalisation du visiteur*/
    public static function checkFreeGeoCountry()
    {
        $geolocation = self::returnUserCountry();
        if ($geolocation != false) {
            $visitor_country = Country::getByIso($geolocation);
            if ($visitor_country != false) {
                if ($visitor_country == 33) {
                    return true;
                }
            }
        }
        return false;
    }
    /*code pour retourner le code de l'utilisateur*/
    public static function returnUserCountry()
    {
        $record = false;
        if (!in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1'))) {
            /* Check if Maxmind Database exists */
            if (@filemtime(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_)) {
                $reader = new GeoIp2\Database\Reader(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_);
                try {
                    $record = $reader->city(Tools::getRemoteAddr());
                    //$record = $reader->city('31.60.47.127');
                } catch (\GeoIp2\Exception\AddressNotFoundException $e) {
                    $record = null;
                }

                if (isset($record->country->isoCode)) {
                    return $record->country->isoCode;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
    /*fin code*/

    private function getNameSimple($name)
    {
        return preg_replace('/\s\(.*\)$/', '', $name);
    }
}
