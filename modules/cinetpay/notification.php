<?php
/**
 * CinetPay V1-Payment Module version 1.2.0 (rev 1) for Prestashop 1.5-1.7.
 *
 * Copyright (C) 2012-2015 Cinetcore and contributors
 * Support contact : support@cinetpay.com
 * Author link : http://www.cinetcore.ci/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author    Cinetcore <infos@cinetcore.ci>
 * @copyright 2012-2015 Cinetcore and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @category  payment
 * @package   cinetpay
 * @version   1.2.0 (rev 1)
 */

require_once dirname(dirname(dirname(__FILE__))) . '/config/config.inc.php';
require_once(dirname(__FILE__) . '../../../init.php');
include(dirname(__FILE__) . '/cinetpay.php');

// TODO : validation du panier
//if ((Tools::isSubmit('cart->id') == false) || (Tools::isSubmit('secure_key') == false))
//  return false;

/**
 * If the order has been validated we try to retrieve it
 */
//$order_id = Order::getOrderByCartId((int)$cart->id);

$context = Context::getContext();
$cinetpay = new Cinetpay();
$cinetpay->logger->logInfo("#######################[New IPN]###########################");

if (!defined('_PS_VERSION_')) {
    $cinetpay->logger->logInfo("Erreur de chargement des fonctions prestashop");
    return false;
}
//TODO: recuperation des parametres
$cpm_trans_id = Tools::getValue('cpm_trans_id');
$cpm_amount = Tools::getValue('cpm_amount');
$cpm_currency = Tools::getValue('cpm_currency');
$cpm_site_id = Tools::getValue('cpm_site_id');
$cpm_language = Tools::getValue('cpm_language');
$cpm_version = Tools::getValue('cpm_version');
$cpm_payment_config = Tools::getValue('cpm_payment_config');
$cpm_page_action = Tools::getValue('cpm_page_action');
$cpm_custom = Tools::getValue('cpm_custom');
$payment_method = Tools::getValue('payment_method');
$signature = Tools::getValue('signature');
$cel_phone_num = Tools::getValue('cel_phone_num');
$cpm_phone_prefixe = Tools::getValue('cpm_phone_prefixe');

if (empty($cpm_trans_id)) {
    $cinetpay->logger->logInfo("Appel direct de l'IPN");
    return false;
}
if (empty($cpm_amount) || $cpm_amount == 0) {
    $cinetpay->logger->logInfo("Montant Incorrect");
    return false;
}

//TODO: creation du panier avec la reference envoyé en get par mtn serveur

$cart = new Cart($cpm_custom);
$cart_id = $cart->id;
if (version_compare(_PS_VERSION_, '1.7', '<')) {
    // TODO : verification et validation du panier
    if ($cart->getTotalCart($cart->id) == 0) {
        $cinetpay->logger->logInfo("Panier Absent");
        return false;
    }
}
$cinetpay->logger->logInfo("Server call process starts for cart #$cart_id.");

/* cart errors */
if (!Validate::isLoadedObject($cart)) {
    $cinetpay->logger->logError("Cart #$cart_id not found in database.");
    die('<span style="display:none">KO-' . Tools::getValue('cpm_custom') . "=Impossible de retrouver la commande\n</span>");
} elseif ($cart->nbProducts() <= 0) {
    $cinetpay->logger->logError("Cart #$cart_id was emptied before redirection.");
    die('<span style="display:none">KO-' . Tools::getValue('cpm_custom') . "=Le panier a été vidé avant la redirection\n</span>");
}


//$cart = new Cart((int)$cart->id);
$customer = new Customer((int)$cart->id_customer);

/**
 * Since it's an example we are validating the order right here,
 * You should not do it this way in your own module.
 */
/* envoie des donnée en post pour validation */

/* creation et obtention du token */

/* $cart = $this->context->cart; */
if ($cart->OrderExists() == false) {
    $cinetpay->logger->logError("treatment for : cart #$cart_id.");
    $currency_id = (int)$cart->id_currency;
    $currency_from = new Currency((int)$currency_id);

    $NewOrderTotal = $cart->getOrderTotal();
    $Montant = $NewOrderTotal . ' ' . $currency_from->iso_code;

// Fin validation du panier
    $OTotal_To_XOF = Cinetpay::convertPriceFull($NewOrderTotal, $currency_from->iso_code, 'XOF');

// real paid total on platform
    if (!Cinetpay::verifiedAmountPaid($OTotal_To_XOF, $cpm_amount)) {
        $cinetpay->logger->logError("Error1: amount posted [#$cpm_amount] not equals initial amount [#$OTotal_To_XOF]. Order is in a failed status, cart #$cart_id.");
        return false;
    }

    $postdata = array(
        'cpm_trans_id' => urldecode($cpm_trans_id),
        'cpm_site_id' => urldecode(Configuration::get('CINETPAY_SITE_ID')),
        'apikey' => urldecode(Configuration::get('CINETPAY_APIKEY'))
    );

    $cinetpay->logger->logInfo("Check payment via WS for cart #$cart_id##$Montant");

    $result = $cinetpay->callCinetpayWsMethod($postdata, Configuration::get('CINETPAY_URL_VALIDATION'));
    if ($result == false) {
        $cinetpay->logger->logInfo("WS calling is down for cart #$cart_id.");
        return false;
    } else {
        $cinetpay->logger->logInfo("WS calling is OK #$cart_id.");

        $decodeText = html_entity_decode(trim($result));
        $array_flux_json = Tools::jsonDecode($decodeText, true);

        $module_name = 'cinetpay';
        $customer = new Customer((int)$cart->id_customer);

        $cpm_error_message = $array_flux_json['transaction']['cpm_error_message'];
        $cpm_result = $array_flux_json['transaction']['cpm_result'];
        $cpm_payid = $array_flux_json['transaction']['cpm_payid'];
        $cpm_payment_date = $array_flux_json['transaction']['cpm_payment_date'];
        $cpm_payment_time = $array_flux_json['transaction']['cpm_payment_time'];
        $created_at = $array_flux_json['transaction']['created_at'];
        $cpm_trans_status = $array_flux_json['transaction']['cpm_trans_status'];
        $cpm_designation = $array_flux_json['transaction']['cpm_designation'];
        $buyer_name = $array_flux_json['transaction']['buyer_name'];
        $cpm_amount_cp = (float)$array_flux_json['transaction']['cpm_amount'];

        // real paid total on platform
        if (number_format($cpm_amount_cp, 2) != number_format($cpm_amount, 2)) {
            $cinetpay->logger->logError("Fraude detected: amount paid [#$cpm_amount_cp] not equals posted amount [#$cpm_amount]. Order is in a failed status, cart #$cart_id.");
            return false;
        }

        if (!Cinetpay::verifiedAmountPaid($OTotal_To_XOF, $cpm_amount_cp)) {
            $cinetpay->logger->logError("Fraude detected: amount paid [#$cpm_amount_cp] not equals initial amount [#$OTotal_To_XOF]. Order is in a failed status, cart #$cart_id.");
            return false;
        }

        $query = 'INSERT INTO `' . _DB_PREFIX_ . 'cinetpay` (cpm_trans_id, cpm_amount, cpm_currency, cpm_site_id, cpm_language, cpm_version, cpm_payment_config, cpm_page_action, cpm_custom, payment_method, signature, cel_phone_num, cpm_phone_prefixe) VALUES ("' . pSQL($cpm_trans_id) . '","' . pSQL($cpm_amount) . '","' . pSQL($cpm_currency) . '","' . pSQL($cpm_site_id) . '","' . pSQL($cpm_language) . '","' . pSQL($cpm_version) . '","' . pSQL($cpm_payment_config) . '","' . pSQL($cpm_page_action) . '","' . pSQL($cpm_custom) . '","' . pSQL($payment_method) . '","' . pSQL($signature) . '","' . pSQL($cel_phone_num) . '","' . pSQL($cpm_phone_prefixe) . '")';
        $resInsert = Db::getInstance()->execute($query);
        if ($resInsert === false) {
            $cinetpay->logger->logInfo("SQL: $query ## INSERTION KO");
        } else {
            $cinetpay->logger->logInfo("SQL: $query ## INSERTION OK");
        }

        $updateQuery = 'UPDATE `' . _DB_PREFIX_ . 'cinetpay` SET cpm_error_message="' . pSQL($cpm_error_message) . '",cpm_result="' . pSQL($cpm_result) . '",cpm_payid="' . pSQL($cpm_payid) . '",cpm_payment_date="' . pSQL($cpm_payment_date) . '",cpm_payment_time="' . pSQL($cpm_payment_time) . '",created_at="' . pSQL($created_at) . '",cpm_trans_status="' . pSQL($cpm_trans_status) . '",cpm_designation="' . pSQL($cpm_designation) . '",buyer_name="' . pSQL($buyer_name) . '" WHERE signature="' . pSQL($signature) . '"';

        $resInsert = Db::getInstance()->execute($updateQuery);
        if ($resInsert === false) {
            $cinetpay->logger->logInfo("SQL: $updateQuery ## UPDATE KO");
        } else {
            $cinetpay->logger->logInfo("SQL: $updateQuery ## UPDATE OK");
        }

        // traitement de la validation du panier selon le retour client
        //fin traitement validation du panier

        if (($array_flux_json['transaction']['cpm_result'] == "00")) {
            $payment_status = Configuration::get('_CINETPAY_ACCEPTED_STATUS_'); // Default value for a payment that succeed.
            $message = "Identifiant du paiement: " . $array_flux_json['transaction']['cpm_payid']; // You can add a comment directly into the order so the merchant will see it in the BO.

            $cinetpay->logger->logInfo("Payment accepted for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
            $cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
        } else {
            $cinetpay->logger->logInfo("Ordering must retrieve after payment failed, taken into account #$cart_id.");
            if (($array_flux_json['transaction']['cpm_result'] == "602")) {
                $payment_status = Configuration::get('_CINETPAY_INSUFFISANT_STATUS_'); // Default value for a payment that refused.
                $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                $cinetpay->logger->logInfo("Payment refused for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                //$cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
            } elseif (($array_flux_json['transaction']['cpm_result'] == "604")) {
                $payment_status = Configuration::get('_CINETPAY_OTP_STATUS_'); // Default value for a payment that refused.
                $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                $cinetpay->logger->logInfo("Payment refused for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                //$cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
            } elseif (($array_flux_json['transaction']['cpm_result'] == "627")) {
                $cinetpay->logger->logInfo("Ordering cancelled after payment failed, taken into account #$cart_id.");
                $payment_status = Configuration::get('_CINETPAY_TRANSACTION_CANCEL_'); // Default value for a payment that refused.
                $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                $cinetpay->logger->logInfo("Payment refused for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                $cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
            } elseif (($array_flux_json['transaction']['cpm_result'] == "623")) {
                $payment_status = Configuration::get('_CINETPAY_WAITING_PAYMENT_'); // Default value for a payment that refused.
                $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                $cinetpay->logger->logInfo("Payment pending for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                //$cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
            } else {
                $payment_status = Configuration::get('_CINETPAY_INDISPO_STATUS_'); // Default value for a payment that refused.
                $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                $cinetpay->logger->logInfo("Payment refused for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                //$cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
            }
        }
    }
} else {
    $cinetpay->logger->logInfo("Ordering already taken into account #$cart_id.");
}
exit();
/*
  }else{ // mauvais type de requete
  return false;
  }
 */
