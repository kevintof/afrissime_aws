<?php
/**
 * CinetPay V1-Payment Module version 1.2.0 (rev 1) for Prestashop 1.5-1.7.
 *
 * Copyright (C) 2012-2015 Cinetcore and contributors
 * Support contact : support@cinetpay.com
 * Author link : http://www.cinetcore.ci/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author    Cinetcore <infos@cinetcore.ci>
 * @copyright 2012-2015 Cinetcore and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @category  payment
 * @package   cinetpay
 * @version   1.2.0 (rev 1)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

require_once _PS_MODULE_DIR_ . 'cinetpay/classes/CinetpayFileLogger.php';

class Cinetpay extends PaymentModule
{
    public $logger = null;
    /* module logger */
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'cinetpay';
        $this->tab = 'payments_gateways';
        $this->version = '1.2.0';
        $this->author = 'CINETCORE';
        $this->module_key = '3a9d597e87422bfd8bd670d3bae7f29a';

        // check version compatibility
        $minor = Tools::substr(_PS_VERSION_, strrpos(_PS_VERSION_, '.') + 1);
        $replace = (int)$minor + 1;
        $version = substr_replace(_PS_VERSION_, (string)$replace, Tools::strlen(_PS_VERSION_) - Tools::strlen($minor));
        $this->ps_versions_compliancy = array('min' => '1.5.0.0', 'max' => $version);

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         *
         * $this->bootstrap              = true;
         */

        parent::__construct();

        $this->displayName = $this->l('CinetPay');
        $this->description = $this->l('CinetPay est un module prestashop vous permettant de faire des achats à partir de votre telephone mobile Orange Money, MTN Mobile Money et Moov Flooz en Côte d\'ivoire');

        $this->confirmUninstall = $this->l('Etes vous sûr de desinstaller ce module');


        $deviseArray = $currency_array = array();
        $currency_array = Currency::getCurrencies(false, 1);
        foreach ($currency_array as $arr) {
            $deviseArray[] = (string)$arr['iso_code'];
        }
        $this->limited_currencies = $deviseArray;

        //$this->logger = new CinetpayFileLogger(Configuration::get('CINETPAY_ENABLE_LOGS') != 'NON');
        //$this->logger->setFilename(_PS_ROOT_DIR_ . '/log/' . date('Y_m') . '_cinetpay.log');
        $this->logger = new CinetpayFileLogger(false);
    }

    public static function convertPriceFull($amount_from, $currency_from, $currency_to)
    {
        $currency_from = Tools::strtoupper($currency_from);
        if ($currency_from == 'FCFA' || $currency_from == 'F CFA' || $currency_from == 'CFA') {
            $currency_from = 'XOF';
        }
        $url = "http://quote.yahoo.com/d/quotes.csv?s=" . $currency_from . $currency_to . "=X" . "&f=l1&e=.csv";
        $handle = fopen($url, "r");
        $exchange_rate = fread($handle, 2000);
        fclose($handle);

        $amount_to = $amount_from * $exchange_rate;
        return round($amount_to);
    }

    public static function verifiedAmountPaid($amount_shop, $amount_cinetpay)
    {
        $diff_abs = (number_format($amount_shop, 2) - number_format($amount_cinetpay, 2));
        $diff_abs = abs($diff_abs);
        if ($diff_abs <= 50) {
            return true;
        } else {
            return false;
        }
    }

    public static function isEqual($str1, $str2)
    {
        $n1 = Tools::strlen($str1);
        if (Tools::strlen($str2) != $n1) {
            return false;
        }
        for ($i = 0, $diff = 0; $i != $n1; ++$i) {
            $diff |= ord($str1[$i]) ^ ord($str2[$i]);
        }
        return !$diff;
    }

    /**
     * PHP is not yet a sufficiently advanced technology to be indistinguishable from magic...
     * so don't use magic_quotes, they mess up with the gateway response analysis.
     *
     * @param array $potentially_quoted_data
     * @return mixed
     */
    public static function uncharm($potentially_quoted_data)
    {
        if (get_magic_quotes_gpc()) {
            $sane = array();
            foreach ($potentially_quoted_data as $k => $v) {
                $sane_key = Tools::stripslashes($k);
                $sane_value = is_array($v) ? self::uncharm($v) : Tools::stripslashes($v);
                $sane[$sane_key] = $sane_value;
            }
        } else {
            $sane = $potentially_quoted_data;
        }

        return $sane;
    }

    /**
     * PHP is not yet a sufficiently advanced technology to be indistinguishable from magic...
     * so don't use magic_quotes, they mess up with the gateway response analysis.
     *
     * @param string $potentially_quoted_data
     * @return string
     */
    public static function charm($potentially_unquoted_data)
    {
        return mysql_real_escape_string($potentially_unquoted_data);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            // incompatible version of PrestaShop
            return false;
        }

        if (extension_loaded('curl') == false) {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }
        Configuration::updateValue('CINETPAY_LIVE_MODE', 'OUI');
        /* enregistrement des parametre de base du module cinetpay */
        Configuration::updateValue('CINETPAY_CURRENCY', 'CFA');
        Configuration::updateValue('CINETPAY_PAYMENT_CONFIG', 'SINGLE');
        Configuration::updateValue('CINETPAY_PAGE_ACTION', 'PAYMENT');
        Configuration::updateValue('CINETPAY_VERSION', 'V1');
        Configuration::updateValue('CINETPAY_LANGUAGE', 'fr');
        /* les  : $protocol_content.Tools::getHttpHost().__PS_BASE_URI__.(!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : '').'/modules/cinetpay/notification.php' */

        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            try {
                if (!file_exists(__PS_BASE_URI__ . 'log')) {
                    mkdir(__PS_BASE_URI__ . 'log', 0775, true);
                }
            } catch (Exception $e) {
            }
            Configuration::updateValue(
                'CINETPAY_NOTIFY_URL',
                Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/cinetpay/notification.php'
            );
            Configuration::updateValue(
                'CINETPAY_RETURN_URL',
                Tools::getHttpHost(true) . __PS_BASE_URI__ . 'index.php?controller=history'
            );
            Configuration::updateValue(
                'CINETPAY_CANCEL_URL',
                Tools::getHttpHost(true) . __PS_BASE_URI__ . 'index.php?controller=history'
            );
        } else {
            Configuration::updateValue(
                'CINETPAY_NOTIFY_URL',
                Tools::getHttpHost(true) . __PS_BASE_URI__ . (!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : '') . 'modules/cinetpay/notification.php'
            );
            Configuration::updateValue(
                'CINETPAY_RETURN_URL',
                Tools::getHttpHost(true) . __PS_BASE_URI__ . (!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : '')
            );
            Configuration::updateValue(
                'CINETPAY_CANCEL_URL',
                Tools::getHttpHost(true) . __PS_BASE_URI__ . (!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : '')
            );
        }

        ###BEGIN_CINETPAY_CUSTOM_STATUT###
        // CinetPay payment Insufficient funds order status
        if (!Configuration::get('_CINETPAY_INSUFFISANT_STATUS_')) {
            // create a Insufficient funds order status
            $lang = array(
                'en' => '[CinetPay] Insufficient funds',
                'fr' => '[CinetPay] Insuffisance de fonds',
            );

            $name = array();
            foreach (Language::getLanguages(true) as $language) {
                $name[$language['id_lang']] = key_exists($language['iso_code'], $lang) ? $lang[$language['iso_code']] : $lang['en'];
            }

            $cinetpay_state = new OrderState();
            $cinetpay_state->name = $name;
            $cinetpay_state->invoice = false;
            $cinetpay_state->send_email = false;
            $cinetpay_state->module_name = $this->name;
            $cinetpay_state->color = '#F2FF00';
            $cinetpay_state->unremovable = true;
            $cinetpay_state->hidden = false;
            $cinetpay_state->logable = false;
            $cinetpay_state->delivery = false;
            $cinetpay_state->shipped = false;
            $cinetpay_state->paid = false;
            $cinetpay_state->template = 'payment_error';

            if (!$cinetpay_state->save() || !Configuration::updateValue('_CINETPAY_INSUFFISANT_STATUS_', $cinetpay_state->id)) {
                return false;
            }
        }

        // CinetPay payment secret code error order status
        if (!Configuration::get('_CINETPAY_OTP_STATUS_')) {
            // create a secret code error order status
            $lang = array(
                'en' => '[CinetPay] Secret code error',
                'fr' => '[CinetPay] Code secret incorrect',
            );

            $name = array();
            foreach (Language::getLanguages(true) as $language) {
                $name[$language['id_lang']] = key_exists($language['iso_code'], $lang) ? $lang[$language['iso_code']] : $lang['en'];
            }

            $cinetpay_state = new OrderState();
            $cinetpay_state->name = $name;
            $cinetpay_state->invoice = false;
            $cinetpay_state->send_email = false;
            $cinetpay_state->module_name = $this->name;
            $cinetpay_state->color = '#00F2FF';
            $cinetpay_state->unremovable = true;
            $cinetpay_state->hidden = false;
            $cinetpay_state->logable = false;
            $cinetpay_state->delivery = false;
            $cinetpay_state->shipped = false;
            $cinetpay_state->paid = false;
            $cinetpay_state->template = 'payment_error';

            if (!$cinetpay_state->save() || !Configuration::updateValue('_CINETPAY_OTP_STATUS_', $cinetpay_state->id)) {
                return false;
            }
        }

        // CinetPay payment Transaction cancel order status
        if (!Configuration::get('_CINETPAY_TRANSACTION_CANCEL_')) {
            // create a transaction cancel order status
            $lang = array(
                'en' => '[CinetPay] Transaction cancel',
                'fr' => '[CinetPay] Transaction annulée',
            );

            $name = array();
            foreach (Language::getLanguages(true) as $language) {
                $name[$language['id_lang']] = key_exists($language['iso_code'], $lang) ? $lang[$language['iso_code']] : $lang['en'];
            }

            $cinetpay_state = new OrderState();
            $cinetpay_state->name = $name;
            $cinetpay_state->invoice = false;
            $cinetpay_state->send_email = false;
            $cinetpay_state->module_name = $this->name;
            $cinetpay_state->color = '#FF00F2';
            $cinetpay_state->unremovable = true;
            $cinetpay_state->hidden = false;
            $cinetpay_state->logable = false;
            $cinetpay_state->delivery = false;
            $cinetpay_state->shipped = false;
            $cinetpay_state->paid = false;
            $cinetpay_state->template = 'order_canceled';

            if (!$cinetpay_state->save() || !Configuration::updateValue('_CINETPAY_TRANSACTION_CANCEL_', $cinetpay_state->id)) {
                return false;
            }
        }

        // CinetPay payment  order status
        if (!Configuration::get('_CINETPAY_INDISPO_STATUS_')) {
            // create a unavailable service order status
            $lang = array(
                'en' => '[CinetPay] Unavailable service',
                'fr' => '[CinetPay] Service indisponible',
            );

            $name = array();
            foreach (Language::getLanguages(true) as $language) {
                $name[$language['id_lang']] = key_exists($language['iso_code'], $lang) ? $lang[$language['iso_code']] : $lang['en'];
            }

            $cinetpay_state = new OrderState();
            $cinetpay_state->name = $name;
            $cinetpay_state->invoice = false;
            $cinetpay_state->send_email = false;
            $cinetpay_state->module_name = $this->name;
            $cinetpay_state->color = '#FF0D00';
            $cinetpay_state->unremovable = true;
            $cinetpay_state->hidden = false;
            $cinetpay_state->logable = false;
            $cinetpay_state->delivery = false;
            $cinetpay_state->shipped = false;
            $cinetpay_state->paid = false;
            $cinetpay_state->template = 'payment_error';

            if (!$cinetpay_state->save() || !Configuration::updateValue('_CINETPAY_INDISPO_STATUS_', $cinetpay_state->id)) {
                return false;
            }
        }

        if (!Configuration::get('_CINETPAY_ACCEPTED_STATUS_')) {
            // create a pending order status
            $lang = array(
                'en' => '[CinetPay] Payment accepted',
                'fr' => '[CinetPay] Paiement accepté',
            );

            $name = array();
            foreach (Language::getLanguages(true) as $language) {
                $name[$language['id_lang']] = key_exists($language['iso_code'], $lang) ? $lang[$language['iso_code']] : $lang['en'];
            }

            $oos_state = new OrderState();
            $oos_state->name = $name;
            $oos_state->invoice = true;
            $oos_state->send_email = true;
            $oos_state->module_name = $this->name;
            $oos_state->color = '#00FF8C';
            $oos_state->unremovable = true;
            $oos_state->hidden = false;
            $oos_state->logable = false;
            $oos_state->delivery = false;
            $oos_state->shipped = false;
            $oos_state->paid = true;
            $oos_state->template = 'payment';

            if (!$oos_state->save() || !Configuration::updateValue('_CINETPAY_ACCEPTED_STATUS_', $oos_state->id)) {
                return false;
            }

            // add small icon to status
            @copy(_PS_MODULE_DIR_ . 'cinetpay/views/img/os_oos.gif', _PS_IMG_DIR_ . 'os/' . Configuration::get('_CINETPAY_ACCEPTED_STATUS_') . '.gif');
        }
        ###END_CINETPAY_CUSTOM_STATUT###

        $query = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'cinetpay` (
			`id_cinetpay` int(11) NOT NULL AUTO_INCREMENT,
			`cpm_trans_id` TEXT,
			`cpm_amount` FLOAT,
			`cpm_currency` CHAR(10),
			`cpm_site_id` VARCHAR(25),
			`cpm_language` CHAR(5),
			`cpm_version` CHAR(5),
			`cpm_payment_config` VARCHAR(25),
			`cpm_page_action` VARCHAR(25),
			`cpm_custom` TEXT,
			`payment_method` VARCHAR(25),
			`signature` TEXT,
			`cel_phone_num` VARCHAR(50),
			`cpm_phone_prefixe` VARCHAR(10),
			`cpm_result` VARCHAR(25),
			`cpm_error_message` TEXT,
			`cpm_payid` VARCHAR(250),
			`cpm_payment_date` DATE,
			`cpm_payment_time` VARCHAR(25),
			`created_at` DATETIME,
			`cpm_trans_status` VARCHAR(25),
			`cpm_designation` VARCHAR(250),
			`buyer_name` VARCHAR(200),
			PRIMARY KEY  (`id_cinetpay`)
		) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

        if (Db::getInstance()->execute($query) == false) {
            return false;
        }

        if (!parent::install() || !$this->registerHook('header') || !$this->registerHook('paymentReturn')
            || !$this->registerHook('backOfficeHeader')
            || !$this->registerHook('actionPaymentConfirmation')
            || !$this->registerHook('displayPaymentReturn')
        ) {
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            if (!$this->registerHook('payment')) {
                return false;
            }
        } else {
            if (!$this->registerHook('paymentOptions')) {
                return false;
            }
        }
        return true;
    }

    public function uninstall()
    {
        $result = true;
        // delete all obsolete payzen params but custom order states
        $result &= Db::getInstance()->execute(
            'DELETE FROM `' . _DB_PREFIX_ . "configuration` WHERE `name` LIKE 'CINETPAY_%'"
        );

        $result1 = true;
        // delete all obsolete payzen params but custom order states
        $result1 &= Db::getInstance()->execute(
            'DROP TABLE `' . _DB_PREFIX_ . "cinetpay`"
        );

        return $result && $result1 && parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitCinetpayModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/template_1.tpl');

        return $output . $this->renderForm();
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }

        if (Tools::getValue('CINETPAY_LIVE_MODE') === 'NON') {
            Configuration::updateValue('CINETPAY_URL_SIGN', 'http://api.sandbox.cinetpay.com/v1/?method=getSignatureByPost');
            Configuration::updateValue('CINETPAY_URL_PAIEMENT', 'http://secure.sandbox.cinetpay.com');
            Configuration::updateValue('CINETPAY_URL_VALIDATION', 'http://api.sandbox.cinetpay.com/v1/?method=checkPayStatus');
            Configuration::updateValue('CINETPAY_LIVE_MODE', 'NON');
            /* parametre mode test */
        } else {
            /* parametre mode reel */
            Configuration::updateValue('CINETPAY_URL_SIGN', 'https://api.cinetpay.com/v1/?method=getSignatureByPost');
            Configuration::updateValue('CINETPAY_URL_PAIEMENT', 'https://secure.cinetpay.com');
            Configuration::updateValue('CINETPAY_URL_VALIDATION', 'https://api.cinetpay.com/v1/?method=checkPayStatus');
            Configuration::updateValue('CINETPAY_LIVE_MODE', 'OUI');
        }
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'CINETPAY_LIVE_MODE' => Configuration::get('CINETPAY_LIVE_MODE', 'OUI'),
            'CINETPAY_ENABLE_LOGS' => Configuration::get('CINETPAY_ENABLE_LOGS', 'OUI'),
            'CINETPAY_SITE_ID' => Configuration::get('CINETPAY_SITE_ID', '123456'),
            'CINETPAY_APIKEY' => Configuration::get('CINETPAY_APIKEY', 'azertyuiopqsdfghjklmwxcvbsdfg')
        );
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCinetpayModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        //*************************************************** AJOUT PLATFORME
        //****************** id_lang 3 = de // id_lang 5 = fr
        $list_plateform = array();
        $list_plateform[0]['id'] = 1;
        $list_plateform[0]['value'] = 'OUI';
        $list_plateform[0]['label'] = 'OUI';

        $list_plateform[1]['id'] = 2;
        $list_plateform[1]['value'] = 'NON';
        $list_plateform[1]['label'] = 'NON';
        //*************************************************** AJOUT PLATFORME

        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Passer en production ?'),
                        'name' => 'CINETPAY_LIVE_MODE',
                        'required' => true,
                        'class' => 't',
                        'values' => $list_plateform
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Entrer l\'ID de votre site disponible dans le backoffice'),
                        'name' => 'CINETPAY_SITE_ID',
                        'label' => $this->l('Site id'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Entrer votre apikey disponible dans le backoffice'),
                        'name' => 'CINETPAY_APIKEY',
                        'label' => $this->l('Apikey'),
                    ),
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Activer les logs ?'),
                        'name' => 'CINETPAY_ENABLE_LOGS',
                        'required' => true,
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 'OUI',
                                'label' => $this->l('OUI')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 'NON',
                                'label' => $this->l('NON')
                            )
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    // constant time string compare

    /**
     * This method is used to render the payment button,
     * Take care if the button should be displayed or not.
     */
    public function hookPayment($params)
    {
        $currency_id = $params['cart']->id_currency;
        $currency = new Currency((int)$currency_id);

        if (in_array($currency->iso_code, $this->limited_currencies) == false) {
            return false;
        }

        $this->smarty->assign('module_dir', $this->_path);

        return $this->display(__FILE__, 'views/templates/hook/payment.tpl');
    }

    /**
     * Payment function, display payment buttons/forms for all sub-modules in PrestaShop 1.7+.
     */
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        $payment_options = array(
            $this->getExternalPaymentOption(),
        );

        return $payment_options;
    }

    public function getExternalPaymentOption()
    {
        $externalOption = new PaymentOption();
        $externalOption->setCallToActionText($this->l('Payer avec '))
            ->setAction($this->context->link->getModuleLink($this->name, 'redirect', array(), true))
            /*->setInputs([
                'token' => [
                    'name' =>'token',
                    'type' =>'hidden',
                    'value' =>'12345689',
                ],
            ])
            ->setAdditionalInformation($this->context->smarty->fetch('module:paymentexample/views/templates/front/payment_infos.tpl'))
            */
            ->setLogo(Media::getMediaPath(_PS_MODULE_DIR_ . $this->name . '/logo.png'));

        return $externalOption;
    }

    /**
     * This hook is used to display the order confirmation page.
     */
    public function hookPaymentReturn($params)
    {
        $order = isset($params['order']) ? $params['order'] : $params['objOrder'];
        if (!$this->active || $order->module != $this->name) {
            return;
        }

        $params['total_to_pay'] = empty($params['total_to_pay']) ?
            Tools::displayPrice($order->getOrdersTotalPaid(), new Currency($order->id_currency), false) :
            $params['total_to_pay'];

        if ($order->getCurrentOrderState()->id != Configuration::get('PS_OS_ERROR')) {
            $this->smarty->assign('status', 'ok');
        }

        $this->smarty->assign(array(
            'id_order' => $order->id,
            'reference' => $order->reference,
            'params' => $params,
            'total' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
        ));

        return $this->display(__FILE__, 'views/templates/hook/confirmation.tpl');
    }

    public function callCinetpayWsMethod($params, $url)
    {
        try {
            // Build Http query using params
            $query = http_build_query($params);
            // Create Http context details
            $options = array(
                'http' => array(
                    'header' => "Content-Type: application/x-www-form-urlencoded\r\n" .
                        "Content-Length: " . Tools::strlen($query) . "\r\n" .
                        "User-Agent:MyAgent/1.0\r\n",
                    'method' => "POST",
                    'content' => $query,
                ),
            );
            // Create context resource for our request
            $context = stream_context_create($options);
            // Read page rendered as result of your POST request
            $result = Tools::file_get_contents($url, false, $context);
        } catch (Exception $e) {
            $this->_errors[] = $this->l($e->getMessage());
            return false;
        }

        return trim($result);
    }
}
