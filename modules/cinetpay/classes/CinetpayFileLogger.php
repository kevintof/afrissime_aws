<?php
/**
 * CinetPay V1-Payment Module version 1.2.0 (rev 1) for Prestashop 1.5-1.7.
 *
 * Copyright (C) 2012-2015 Cinetcore and contributors
 * Support contact : support@cinetpay.com
 * Author link : http://www.cinetcore.ci/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author    Cinetcore <infos@cinetcore.ci>
 * @copyright 2012-2015 Cinetcore and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @category  payment
 * @package   cinetpay
 * @version   1.2.0 (rev 1)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Extend logger class to implement logging disable and avoid to check before every log operation
 */
class CinetpayFileLogger extends FileLogger
{
    protected $logs_enabled = false;

    public function __construct($logs_enabled, $level = self::INFO)
    {
        $this->logs_enabled = $logs_enabled;
        parent::__construct($level);
    }

    /**
     * log message only if logs are enabled
     *
     * @param string message
     * @param level
     */
    public function log($message, $level = self::DEBUG)
    {
        if (!$this->logs_enabled) {
            return;
        }

        parent::log($message, $level);
    }
}
