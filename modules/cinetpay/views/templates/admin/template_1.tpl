{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<div class="row cinetpay-header">
		<img width="30px" src="{$module_dir|escape:'html':'UTF-8'}views/img/logo.png" class="col-xs-6 col-md-2 text-center" id="payment-logo" />
		<div class="col-xs-6 col-md-5 text-center">
			<h4>{l s='Paiement mobile en ligne' mod='cinetpay'}</h4>
			<h4>{l s='Rapide - Sécurisé - Fiables' mod='cinetpay'}</h4>
		</div>
		<div class="col-xs-12 col-md-4 text-center">
			<a href="https://www.cinetpay.com/"  class="btn btn-primary" id="create-account-btn">{l s='creer un compte maintenant!' mod='cinetpay'}</a><br />
			{l s='Avez vous deja un compte?' mod='cinetpay'}<a href="#" onclick="javascript:return false;"> {l s='Log in' mod='cinetpay'}</a>
		</div>
	</div>

	<hr />
	
	<div class="cinetpay-content">
		<div class="row">
			<div class="col-md-6">
				<h5>{l s='Pourquoi utilise cinetpay' mod='cinetpay'}</h5>
				<dl>
					Pas besoin de créer un compte pour payer <br/>
                    Effectuez des paiements avec CinetPay sans créer de compte, <br/>
                    juste besoin de vous munir de votre téléphone Mobile, <br/>
                    votre numéro de téléphone et votre code secret. 
				</dl>
			</div>
			
			<div class="col-md-6">
				<h5>{l s='les avantages cinetpay' mod='cinetpay'}</h5>
				<ul>
					<!-- <li>{l s='C est une Solution de paiement simple, securisé et fiable' mod='cinetpay'}</li>
					<li>{l s='Vous avez besoin juste d\'un numero de telephone' mod='cinetpay'}</li>
					<li>{l s='facturation récurrente' mod='cinetpay'}</li>
					<li>{l s='Support clientèle 24/7/365' mod='cinetpay'}</li> -->
					Acceptez les paiements en ligne. Il n'y a que des avantages<br/>
                    Acceptez les paiements avec Orange Money, MTN Mobile Money et Moov Flooz. Que vous disposiez d'un site Internet ou non,<br/>
                    vous pouvez accepter les paiements avec CinetPay.<br/>
                    Acceptez les paiements avec CinetPay vous permet de :<br/>
                    Eviter les problèmes de monnaie, éviter les problèmes de faux billets, éviter les problèmes de cambriolage, sécuriser vos ventes et votre business. Acceptez les paiements en ligne | Acceptez les paiements avec notre TPE Mobile.
				</ul>
				<br />
				<em class="text-muted small">
					
				</em>
			</div>
		</div>

		<hr />
		
		<div class="row">
			<div class="col-md-12">
				<h4>{l s='Paiement accepté en côte d\'ivoire avec MTN, ORANGE et FLOOZ' mod='cinetpay'}</h4>
				
				<div class="row">
					<img width="" height="" src="{$module_dir|escape:'html':'UTF-8'}views/img/cinetpay_icon.png" class="col-md-3" id="payment-logo" />
					<div class="col-md-3">
					<br/>
					</div>
					<div class="col-md-6">
						<h6 class="text-branded"><a href="mailto:support@cinetpay.com">{l s='Contactez-nous à l\'adresse [support@cinetpay.com]' mod='cinetpay'}</a></h6>
						<p class="text-branded">{l s='Si vous avez besoin de plus d\'informations!' mod='cinetpay'}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>