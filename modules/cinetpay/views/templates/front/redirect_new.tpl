{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!doctype html>
<html lang="{$language.iso_code|escape:'html':'UTF-8'}">
<head>
    {block name='head'}
        {include file="_partials/head.tpl"}
    {/block}
</head>
<style>
    .cinetpay-button {
        white-space: nowrap;
    }

    .cinetpay-button .field-error {
        border: 1px solid #FF0000;
    }

    .cinetpay-button .hide {
        display: none;
    }

    .cinetpay-button .error-box {
        background: #FFFFFF;
        border: 1px solid #DADADA;
        border-radius: 5px;
        padding: 8px;
        display: inline-block;
    }

    .cinetpay-button button {
        white-space: nowrap;
        overflow: hidden;
        border-radius: 13px;
        font-family: "Arial", bold, italic;
        font-weight: bold;
        font-style: italic;
        border: 1px solid #2ECC71;
        color: #000000;
        background: #2ECC71;
        position: relative;
        text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
        cursor: pointer;
        z-index: 0;
    }

    .cinetpay-button button:before {
        content: " ";
        position: absolute;
        width: 100%;
        height: 100%;
        border-radius: 11px;
        top: 0;
        left: 0;
        background: #2ECC71;
        background: -webkit-linear-gradient(top, #28B463 0%, #28B463 80%, #FFF8FC 100%);
        background: -moz-linear-gradient(top, #28B463 0%, #28B463 80%, #FFF8FC 100%);
        background: -ms-linear-gradient(top, #28B463 0%, #28B463 80%, #FFF8FC 100%);
        background: linear-gradient(top, #28B463 0%, #28B463 80%, #FFF8FC 100%);
        z-index: -2;
    }

    .cinetpay-button button:after {
        content: " ";
        position: absolute;
        width: 98%;
        height: 60%;
        border-radius: 40px 40px 38px 38px;
        top: 0;
        left: 0;
        background: -webkit-linear-gradient(top, #fefefe 0%, #28B463 100%);
        background: -moz-linear-gradient(top, #fefefe 0%, #28B463 100%);
        background: -ms-linear-gradient(top, #fefefe 0%, #28B463 100%);
        background: linear-gradient(top, #fefefe 0%, #28B463 100%);
        z-index: -1;
        -webkit-transform: translateX(1%);
        -moz-transform: translateX(1%);
        -ms-transform: translateX(1%);
        transform: translateX(1%);
    }

    .cinetpay-button button.small {
        padding: 3px 15px;
        font-size: 12px;
    }

    .cinetpay-button button.large {
        padding: 4px 19px;
        font-size: 14px;
    }
</style>
<body id="checkout" class="{$page.body_classes|classnames}">
{hook h='displayAfterBodyOpeningTag'}

<header id="header">
    {block name='header'}
        {include file="checkout/_partials/header.tpl"}
    {/block}
</header>

<section id="wrapper">
    <div class="container">

        {block name='content'}
            <section id="content">
                <div class="row">
                    <div class="col-md-8">
                        <div>
                            <h3>{l s='Recapitulatif de votre commande' mod='cinetpay'}
                                <!-- <img style="float:right;" src="{$module_dir|escape:'html':'UTF-8'}" alt="{l s='Payer avec CinetPay' mod='cinetpay'}" width="" height="100px" /> --> </h3>
                            <ul class="alert alert-success">
                                <li>
                                    {*l s='This action should be used to redirect your customer to the website of your payment processor' mod='cinetpay'*}

                                </li>
                                <!-- affichage du formulaire de validation  -->
                                <div>
                                    <form id="cinetpay_form" class="form-horizontal cinetpay-button" role="form"
                                          action="{$url_paiement|escape:'htmlall':'UTF-8'}" method="POST">
                                        <div class="box cheque-box">
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="email">Montant :</label>
                                                <div class="col-sm-10">
                                                    <input readonly style="margin:4px;" name="cpm_amount" type="text"
                                                           class="form-control"
                                                           id="SubscriberID" placeholder="Entrer votre numero"
                                                           value="{$cpm_amount|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Devise:</label>
                                                <div class="col-sm-10">
                                                    <input readonly style="margin:4px;" type="text" name="cpm_currency"
                                                           class="form-control"
                                                           id="Reference"
                                                           value="{$cpm_currency|escape:'htmlall':'UTF-8'}"
                                                           placeholder="Votre Mot de passe">
                                                </div>
                                            </div>
                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">Site id:</label>
                                                <div class="col-sm-10">
                                                    <input name="cpm_site_id" type="text" class="form-control"
                                                           id="Balance"
                                                           value="{$cpm_site_id|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>
                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">Transid</label>
                                                <div class="col-sm-10">
                                                    <input name="cpm_trans_id" type="text" class="form-control"
                                                           id="Token"
                                                           value="{$cpm_trans_id|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>
                                            <!-- champs caché -->
                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">trans date:</label>
                                                <div class="col-sm-10">
                                                    <input name="cpm_trans_date" type="text" class="form-control"
                                                           id="User"
                                                           value="{$cpm_trans_date|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>
                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">Paiemment
                                                    config:</label>
                                                <div class="col-sm-10">
                                                    <input name="cpm_payment_config" type="text" class="form-control"
                                                           id="Password"
                                                           value="{$cpm_payment_config|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">cpm_page_action:</label>
                                                <div class="col-sm-10">
                                                    <input name="cpm_page_action" type="text" class="form-control"
                                                           id="ServiceCode"
                                                           value="{$cpm_page_action|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">cpm_version:</label>
                                                <div class="col-sm-10">
                                                    <input style="margin:4px;" name="cpm_version" type="text"
                                                           class="form-control"
                                                           id="TextMessage"
                                                           value="{$cpm_version|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">Designation:</label>
                                                <div class="col-sm-10">
                                                    <input name="cpm_language" type="text" class="form-control"
                                                           id="ImmediateReply"
                                                           value="{$cpm_language|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Designation:</label>
                                                <div class="col-sm-10">
                                                    <input readonly style="margin:4px;" name="cpm_designation"
                                                           type="text" class="form-control"
                                                           id="ImmediateReply"
                                                           value="{$cpm_designation|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">cpm_custom:</label>
                                                <div class="col-sm-10">
                                                    <input name="cpm_custom" type="text" class="form-control"
                                                           id="ImmediateReply"
                                                           value="{$cpm_custom|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">apikey:</label>
                                                <div class="col-sm-10">
                                                    <input name="apikey" type="text" class="form-control"
                                                           id="ImmediateReply"
                                                           value="{$apikey|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">signature:</label>
                                                <div class="col-sm-10">
                                                    <input name="signature" type="text" class="form-control"
                                                           id="ImmediateReply"
                                                           value="{$signature|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>
                                            <!-- envoie des urls -->
                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">url
                                                    notification:</label>
                                                <div class="col-sm-10">
                                                    <input name="notify_url" type="text" class="form-control"
                                                           id="ImmediateReply"
                                                           value="{$notify_url|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">url redirection:</label>
                                                <div class="col-sm-10">
                                                    <input name="return_url" type="text" class="form-control"
                                                           id="ImmediateReply"
                                                           value="{$return_url|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none;">
                                                <label class="control-label col-sm-2" for="pwd">url
                                                    d'annulation:</label>
                                                <div class="col-sm-10">
                                                    <input name="cancel_url" type="text" class="form-control"
                                                           id="ImmediateReply"
                                                           value="{$cancel_url|escape:'htmlall':'UTF-8'}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button style="margin:4px;" name="valider" class="cinetpay-button"
                                                            type="submit">Acheter
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </ul>

                            <!-- <div class="alert alert-warning">
		{l s='You can redirect your customer with an error message' mod='cinetpay'}:
		<a href="{$link->getModuleLink('cinetpay', 'redirect', ['action' => 'error'], true)|escape:'htmlall':'UTF-8'}" title="{l s='Look at the error' mod='cinetpay'}">
			<strong>{l s='Look at the error message' mod='cinetpay'}</strong>
		</a>
	</div>

	<div class="alert alert-success">
		{l s='You can also redirect your customer to the confirmation page' mod='cinetpay'}:
		<a href="{$link->getModuleLink('cinetpay', 'confirmation', ['cart_id' => $cart_id, 'secure_key' => $secure_key], true)|escape:'htmlall':'UTF-8'}" title="{l s='Confirm' mod='cinetpay'}">
			<strong>{l s='Go to the confirmation page' mod='cinetpay'}</strong>
		</a>
	</div> -->
                        </div>
                    </div>

                    <div class="col-md-4">
                    </div>
                </div>
            </section>
        {/block}
    </div>
</section>

<footer id="footer">
    {block name='footer'}
        {include file="checkout/_partials/footer.tpl"}
    {/block}
</footer>

{block name='javascript_bottom'}
    {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
{/block}

</body>
</html>

