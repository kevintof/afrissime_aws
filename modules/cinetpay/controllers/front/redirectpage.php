<?php
/**
 * CinetPay V1-Payment Module version 1.2.0 (rev 1) for Prestashop 1.5-1.7.
 *
 * Copyright (C) 2012-2015 Cinetcore and contributors
 * Support contact : support@cinetpay.com
 * Author link : http://www.cinetcore.ci/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author    Cinetcore <infos@cinetcore.ci>
 * @copyright 2012-2015 Cinetcore and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @category  payment
 * @package   cinetpay
 * @version   1.2.0 (rev 1)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include('../../config/config.inc.php');

class CinetpayRedirectpageModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();

        $this->display_column_left  = false;
        $this->display_column_right = false;
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        include('../../header.php');
        /* inclusion du fichier template */
        $this->context->smarty->display(dirname(__FILE__).'/redirect.tpl');
        include(dirname(__FILE__).'/../../footer.php');
    }
}
