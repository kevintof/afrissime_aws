<?php
/**
 * CinetPay V1-Payment Module version 1.2.0 (rev 1) for Prestashop 1.5-1.7.
 *
 * Copyright (C) 2012-2015 Cinetcore and contributors
 * Support contact : support@cinetpay.com
 * Author link : http://www.cinetcore.ci/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author    Cinetcore <infos@cinetcore.ci>
 * @copyright 2012-2015 Cinetcore and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @category  payment
 * @package   cinetpay
 * @version   1.2.0 (rev 1)
 */

class CinetpayRedirectModuleFrontController extends ModuleFrontController
{

    /**
     * Do whatever you have to before redirecting the customer on the website of your payment processor.
     */
    public function postProcess()
    {
        /**
         * Oops, an error occured.
         */
        if (Tools::getValue('action') == 'error') {
            return $this->displayError('An error occurred while trying to redirect the customer');
        } else {
            /* Stockage de id card et memofisation en base de donnée */
            Configuration::updateValue('idpanier', 'fr');

            /* creation et obtention du token */

            error_reporting(0);

            /* $cart = $this->context->cart; */

            $currency_id   = Context::getContext()->cart->id_currency;
            $currency_from = new Currency((int) $currency_id);

            $NewOrderTotal  = $this->convertPriceFull(Context::getContext()->cart->getOrderTotal(), $currency_from->iso_code, 'XOF');
            $cpm_trans_id = $this->generateTransId();
            
            $SecureData = $postdata   = array(
                'cpm_amount' => $NewOrderTotal,
                'cpm_currency' => Configuration::get('CINETPAY_CURRENCY'),
                'cpm_site_id' => Configuration::get('CINETPAY_SITE_ID'),
                'cpm_trans_id' => $cpm_trans_id,
                'cpm_trans_date' => Context::getContext()->cart->date_add,
                'cpm_payment_config' => Configuration::get('CINETPAY_PAYMENT_CONFIG'),
                'cpm_page_action' => Configuration::get('CINETPAY_PAGE_ACTION'),
                'cpm_version' => Configuration::get('CINETPAY_VERSION'),
                'cpm_language' => Configuration::get('CINETPAY_LANGUAGE'),
                'cpm_designation' => 'Achat sur '.Configuration::get('PS_SHOP_NAME'),
                'cpm_custom' => Context::getContext()->cart->id,
                'apikey' => Configuration::get('CINETPAY_APIKEY')
            );

            $result = false;
            $result = $this->callCinetpayWsMethod($postdata, Configuration::get('CINETPAY_URL_SIGN'));

            if ($result === false) {
                return $this->displayError("Un probleme est survenu lors de l'appel du WS !");
            } else {
                $signature = Tools::jsonDecode($result, true);
                if (is_array($signature)) {
                    if (!isset($signature['status'])) {
                        $message = 'La plateforme CINETPAY est temporairement indisponible.';
                    } else {
                        $message = 'Une erreur est survenue, Code: '.$signature['status']['code'].', Message: '.$signature['status']['message'];
                    }

                    return $this->displayError($message);
                }
            }

            /* fin de la generation du token */
            $SecureData['signature']    = $signature;
            $SecureData['notify_url']   = Configuration::get('CINETPAY_NOTIFY_URL');
            $SecureData['return_url']   = Configuration::get('CINETPAY_RETURN_URL');
            $SecureData['cancel_url']   = Configuration::get('CINETPAY_CANCEL_URL');
            $SecureData['url_paiement'] = Configuration::get('CINETPAY_URL_PAIEMENT');
            $SecureData['module_dir']   = Tools::getHttpHost(true).__PS_BASE_URI__.(!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : '').'modules/cinetpay/views/img/cinetpay_icon.png';
            $SecureData['bouton_pay']   = Tools::getHttpHost(true).__PS_BASE_URI__.(!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : '').'modules/cinetpay/views/img/boutonpay.jpg';

            $this->context->smarty->assign($SecureData);

            if (version_compare(_PS_VERSION_, '1.7', '>=')) {
                $this->setTemplate('module:cinetpay/views/templates/front/redirect_new.tpl');
            } else {
                return $this->setTemplate('redirect.tpl');
            }
        }
    }

    protected function displayError($message, $description = false)
    {
        /**
         * Create the breadcrumb for your ModuleFrontController.
         */
        $this->context->smarty->assign(
            'path',
            '
			<a href="'.$this->context->link->getPageLink('order', null, null, 'step=3').'">'.$this->module->l('Payment').'</a>
			<span class="navigation-pipe">&gt;</span>'.$this->module->l('Error')
        );

        /**
         * Set error message and description for the template.
         */
        array_push($this->errors, $this->module->l($message), $description);

        return $this->setTemplate('error.tpl');
    }

    protected function callCinetpayWsMethod($params, $url)
    {
        try {
            // Build Http query using params
            $query   = http_build_query($params);
            // Create Http context details
            $options = array(
                'http' => array(
                    'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Content-Length: ".Tools::strlen($query)."\r\n".
                    "User-Agent:MyAgent/1.0\r\n",
                    'method' => "POST",
                    'content' => $query,
                ),
            );
            // Create context resource for our request
            $context = stream_context_create($options);
            // Read page rendered as result of your POST request
            $result  = Tools::file_get_contents($url, false, $context);
        } catch (Exception $e) {
            return false;
        }

        return trim($result);
    }

    protected function convertPriceFull($amount_from, $currency_from, $currency_to)
    {
        $currency_from = Tools::strtoupper($currency_from);
        if ($currency_from == 'FCFA' || $currency_from == 'F CFA' || $currency_from == 'CFA') {
            $currency_from = 'XOF';
        }
        $url           = "http://quote.yahoo.com/d/quotes.csv?s=".$currency_from.$currency_to."=X"."&f=l1&e=.csv";
        $handle        = fopen($url, "r");
        $exchange_rate = fread($handle, 2000);
        fclose($handle);

        $amount_to = $amount_from * $exchange_rate;
        return round($amount_to);
    }
    
    protected function generateTransId()
    {
        $timestamp = time();
        sleep(1);
        list($usec, $sec) = explode(' ', microtime()); // microseconds, php4 compatible
        $temp = ($timestamp + $usec - strtotime('today 00:00')) * 10;
        $temp = sprintf('%06d', $temp);
        $temp = $temp.rand(1, 99);
        return $temp;
    }
}
