<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2015 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class CinetpayConfirmationModuleFrontController extends ModuleFrontController
{
    public $display_column_left = false;

    private $current_cart;

    public function postProcess()
    {
        if ((Tools::isSubmit('cpm_trans_id') == false) || (Tools::isSubmit('secure_key') == false)) {
            Tools::redirectLink('index.php');
        }

        $cart_id = Tools::getValue('cpm_trans_id');
        $this->current_cart = new Cart((int)$cart_id);

        $this->module->logger->logInfo("User return to shop process starts for cart #$cart_id.");

        // cart errors
        if (!Validate::isLoadedObject($this->current_cart) || $this->current_cart->nbProducts() <= 0) {
            $this->module->logger->logWarning("Cart is empty, redirect to home page. Cart ID: $cart_id.");
            Tools::redirectLink('index.php');
        }

        if ($this->current_cart->id_customer == 0 || $this->current_cart->id_address_delivery == 0
            || $this->current_cart->id_address_invoice == 0 || !$this->module->active
        ) {
            $this->module->logger->logWarning("No address selected for customer or module disabled, redirect to checkout first page. Cart ID: $cart_id.");
            Tools::redirect('index.php?controller=order&step=1');
        }

        $customer = new Customer($this->current_cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            $this->module->logger->logWarning("Customer not logged in, redirect to checkout first page. Cart ID: $cart_id.");
            Tools::redirect('index.php?controller=order&step=1');
        }

        $this->processPaymentReturn();
    }

    private function processPaymentReturn()
    {
        $cinetpay = new Cinetpay();
        $cinetpay->logger->logInfo("#######################[processPaymentReturn]###########################");
        //debut traitement
        /*
          $method = $_SERVER['REQUEST_METHOD'];
          if($method=='POST')
          { */
        //orderExists()
        //TODO: recuperation des parametres
        $cpm_trans_id = Tools::getValue('cpm_trans_id');
        $cpm_amount = Tools::getValue('cpm_amount');
        $cpm_currency = Tools::getValue('cpm_currency');
        $cpm_site_id = Tools::getValue('cpm_site_id');
        $cpm_language = Tools::getValue('cpm_language');
        $cpm_version = Tools::getValue('cpm_version');
        $cpm_payment_config = Tools::getValue('cpm_payment_config');
        $cpm_page_action = Tools::getValue('cpm_page_action');
        $cpm_custom = Tools::getValue('cpm_custom');
        $payment_method = Tools::getValue('payment_method');
        $signature = Tools::getValue('signature');
        $cel_phone_num = Tools::getValue('cel_phone_num');
        $cpm_phone_prefixe = Tools::getValue('cpm_phone_prefixe');

        if (empty($cpm_amount) || $cpm_amount == 0) {
            return false;
        }

        //TODO: creation du panier avec la reference envoyé en get par mtn serveur

        $cart = new Cart($this->current_cart->id);
        $cart_id = $this->current_cart->id;

        // real paid total on platform
        if (number_format($cart->getOrderTotal(), 2) != number_format($cpm_amount, 2)) {
            $cinetpay->logger->logError("Error1: amount paid [#$cpm_amount] not equals initial amount. Order is in a failed status, cart #$cart_id.");
            Tools::redirectLink('index.php');
        }

        if ($cart->OrderExists() == false) {
            //$cart = new Cart((int)$cart->id);

            /**
             * Since it's an example we are validating the order right here,
             * You should not do it this way in your own module.
             */
            /* envoie des donnée en post pour validation */

            /* creation et obtention du token */

            /* $cart = $this->context->cart; */

            $currency_id = (int)$cart->id_currency;
            $currency_from = new Currency((int)$currency_id);

            $NewOrderTotal = $cart->getOrderTotal();
            $Montant = $NewOrderTotal . ' ' . $currency_from->iso_code;

            $postdata = array(
                'cpm_trans_id' => urldecode($cpm_trans_id),
                'cpm_site_id' => urldecode(Configuration::get('CINETPAY_SITE_ID')),
                'apikey' => urldecode(Configuration::get('CINETPAY_APIKEY'))
            );

            $cinetpay->logger->logInfo("Check payment via WS for cart #$cart_id##$Montant");

            $result = $cinetpay->callCinetpayWsMethod($postdata, Configuration::get('CINETPAY_URL_VALIDATION'));

            if ($result === false) {
                $cinetpay->logger->logInfo("WS calling is down for cart #$cart_id.");
                Tools::redirectLink('index.php');
            } else {
                $cinetpay->logger->logInfo("WS calling is OK #$cart_id.");

                $decodeText = html_entity_decode(trim($result));
                $array_flux_json = Tools::jsonDecode($decodeText, true);

                $module_name = 'cinetpay';

                $cpm_error_message = $array_flux_json['transaction']['cpm_error_message'];
                $cpm_result = $array_flux_json['transaction']['cpm_result'];
                $cpm_payid = $array_flux_json['transaction']['cpm_payid'];
                $cpm_payment_date = $array_flux_json['transaction']['cpm_payment_date'];
                $cpm_payment_time = $array_flux_json['transaction']['cpm_payment_time'];
                $created_at = $array_flux_json['transaction']['created_at'];
                $cpm_trans_status = $array_flux_json['transaction']['cpm_trans_status'];
                $cpm_designation = $array_flux_json['transaction']['cpm_designation'];
                $buyer_name = $array_flux_json['transaction']['buyer_name'];
                $cpm_amount_cp = (float)$array_flux_json['transaction']['cpm_amount'];

                // real paid total on platform
                if (number_format($cpm_amount_cp, 2) != number_format($cpm_amount, 2)) {
                    $cinetpay->logger->logError("Fraude detected: amount paid [#$cpm_amount_cp] not equals posted amount [#$cpm_amount]. Order is in a failed status, cart #$cart_id.");
                    Tools::redirectLink('index.php');
                }

                if (number_format($cart->getOrderTotal(), 2) != number_format($cpm_amount_cp, 2)) {
                    $cinetpay->logger->logError("Fraude detected: amount paid [#$cpm_amount_cp] not equals initial amount. Order is in a failed status, cart #$cart_id.");
                    Tools::redirectLink('index.php');
                }

                $query = 'INSERT INTO `' . _DB_PREFIX_ . 'cinetpay` (cpm_trans_id, cpm_amount, cpm_currency, cpm_site_id, cpm_language, cpm_version, cpm_payment_config, cpm_page_action, cpm_custom, payment_method, signature, cel_phone_num, cpm_phone_prefixe) VALUES ("' . pSQL($cpm_trans_id) . '","' . pSQL($cpm_amount) . '","' . pSQL($cpm_currency) . '","' . pSQL($cpm_site_id) . '","' . pSQL($cpm_language) . '","' . pSQL($cpm_version) . '","' . pSQL($cpm_payment_config) . '","' . pSQL($cpm_page_action) . '","' . pSQL($cpm_custom) . '","' . pSQL($payment_method) . '","' . pSQL($signature) . '","' . pSQL($cel_phone_num) . '","' . pSQL($cpm_phone_prefixe) . '")';

                $resInsert = Db::getInstance()->execute($query);
                if ($resInsert === false) {
                    $cinetpay->logger->logInfo("SQL: $query ## INSERTION KO");
                } else {
                    $cinetpay->logger->logInfo("SQL: $query ## INSERTION OK");
                }

                $updateQuery = 'UPDATE `' . _DB_PREFIX_ . 'cinetpay` SET cpm_error_message="' . pSQL($cpm_error_message) . '",cpm_result="' . pSQL($cpm_result) . '",cpm_payid="' . pSQL($cpm_payid) . '",cpm_payment_date="' . pSQL($cpm_payment_date) . '",cpm_payment_time="' . pSQL($cpm_payment_time) . '",created_at="' . pSQL($created_at) . '",cpm_trans_status="' . pSQL($cpm_trans_status) . '",cpm_designation="' . pSQL($cpm_designation) . '",buyer_name="' . pSQL($buyer_name) . '" WHERE signature="' . pSQL($signature) . '"';

                $resInsert = Db::getInstance()->execute($updateQuery);
                if ($resInsert === false) {
                    $cinetpay->logger->logInfo("SQL: $updateQuery ## UPDATE KO");
                } else {
                    $cinetpay->logger->logInfo("SQL: $updateQuery ## UPDATE OK");
                }

                // traitement de la validation du panier selon le retour client
                //fin traitement validation du panier

                if (($array_flux_json['transaction']['cpm_result'] == "00")) {
                    $payment_status = Configuration::get('_CINETPAY_ACCEPTED_STATUS_'); // Default value for a payment that succeed.
                    $message = "Identifiant du paiement: " . $array_flux_json['transaction']['cpm_payid']; // You can add a comment directly into the order so the merchant will see it in the BO.

                    $cinetpay->logger->logInfo("Payment accepted for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                    $cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
                } elseif (($array_flux_json['transaction']['cpm_result'] == "602")) {
                    $payment_status = Configuration::get('_CINETPAY_INSUFFISANT_STATUS_'); // Default value for a payment that refused.
                    $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                    $cinetpay->logger->logInfo("Payment refused for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                    $cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
                } elseif (($array_flux_json['transaction']['cpm_result'] == "604")) {
                    $payment_status = Configuration::get('_CINETPAY_OTP_STATUS_'); // Default value for a payment that refused.
                    $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                    $cinetpay->logger->logInfo("Payment refused for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                    $cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
                } elseif (($array_flux_json['transaction']['cpm_result'] == "627")) {
                    $payment_status = Configuration::get('_CINETPAY_TRANSACTION_CANCEL_'); // Default value for a payment that refused.
                    $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                    $cinetpay->logger->logInfo("Payment refused for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                    $cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
                } elseif (($array_flux_json['transaction']['cpm_result'] == "623")) {
                    $payment_status = Configuration::get('_CINETPAY_WAITING_PAYMENT_'); // Default value for a payment that refused.
                    $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                    $cinetpay->logger->logInfo("Payment pending for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                    $cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
                } else {
                    $payment_status = Configuration::get('_CINETPAY_INDISPO_STATUS_'); // Default value for a payment that refused.
                    $message = "##$cpm_result##$cpm_error_message##"; // You can add a comment directly into the order so the merchant will see it in the BO.

                    $cinetpay->logger->logInfo("Payment refused for cart #$cart_id. ##$cpm_result##$cpm_error_message##");
                    $cinetpay->validateOrder($cart->id, $payment_status, $cpm_amount_cp, $module_name, $message, array(), $currency_id, false, $cart->secure_key);
                }
                Tools::redirect('index.php?controller=history');
            }
        } else {
            // message d'erreur la commande est deja prise en compte
            $cinetpay->logger->logInfo("Ordering is already taken into account #$cart_id.");
            Tools::redirect('index.php?controller=history');
        }
    }
}
