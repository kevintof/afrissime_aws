{**
* 2013 - 2018 HiPresta
*
* MODULE Facebook Connect
*
* @author    HiPresta <suren.mikaelyan@gmail.com>
* @copyright HiPresta 2018
* @license   Addons PrestaShop license limitation
* @link      http://www.hipresta.com
*
* NOTICE OF LICENSE
*
* Don't use this module on several shops. The license provided by PrestaShop Addons
* for all its modules is valid only once for a single shop.
*}

<script type="text/javascript">
    {literal}
        var psv = {/literal}{$psv|floatval}{literal};
        var id_lang = {/literal}{$id_lang|intval}{literal};
        var hi_sc_fb_admin_controller_dir = '{/literal}{$hi_sc_fb_admin_controller_dir|escape:'htmlall':'UTF-8'}{literal}';
    {/literal}
</script>