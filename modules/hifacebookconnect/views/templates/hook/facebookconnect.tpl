{**
* 2013 - 2018 HiPresta
*
* MODULE Facebook Connect
*
* @author    HiPresta <suren.mikaelyan@gmail.com>
* @copyright HiPresta 2018
* @license   Addons PrestaShop license limitation
* @link      http://www.hipresta.com
*
* NOTICE OF LICENSE
*
* Don't use this module on several shops. The license provided by PrestaShop Addons
* for all its modules is valid only once for a single shop.
*}

<div id="fb-root"></div>
<p style="margin: 0 auto;width: 208px;">
	<div style="margin-bottom: 3px; overflow: hidden; text-align: center; color: rgb(51, 51, 51); font-weight: bold;">ou</div>
	<div style="overflow:hidden; margin:0 auto; width:208px">
		<a onclick="fb_login();" class="hisc-button hisc-fb-button onclick-btn">
			<span class="hisc-button-text">
				<span>{l s='Sign in with Facebook' mod='hifacebookconnect'}</span>
			</span>
			<span class="hisc-button-icon">
				<span></span>
			</span>
		</a>
	</div>	
</p>



