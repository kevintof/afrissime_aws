<?php
/**
 * OLARK
 *
 * @category  Prestashop
 * @category  Module
 * @author    Mediacom87 <support@mediacom87.net>
 * @copyright Mediacom87
 * @license   commercial license see in the module
 */

if (!defined('_TB_VERSION_')
    && !defined('_PS_VERSION_')) {
    exit;
}

include_once dirname(__FILE__).'/class/mediacom87.php';

/**
 * blockolark class.
 *
 * @extends Module
 */
class BlockOlark extends Module
{
    public $smarty;
    public $context;
    public $controller;
    private $errors = array();
    protected $config_form = false;

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->name = 'blockolark';
        $this->tab = 'advertising_marketing';
        $this->version = '1.5.0';
        $this->author = 'Mediacom87';
        $this->need_instance = 0;
        $this->module_key = '';
        $this->addons_id = ''; //
        $this->ps_versions_compliancy = array('min' => '1.5.0.0', 'max' => '1.7.99.99');

        /* boostrap */
        if (version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
            $this->bootstrap = true;
        } else {
            $this->bootstrap = false;
        }

        parent::__construct();

        $this->displayName = $this->l('Olark LiveChat');
        $this->description = $this->l('Integrate your Olark Livechat script on your shop.');

        $this->confirmUninstall = $this->l('If you uninstall this add-on you will lose all of its configuration. Are you sure you want to uninstall it?');

        $this->mediacom87 = new BlockOlarkClass($this);

        $this->conf = unserialize(Configuration::get($this->name));
        $this->tpl_path = _PS_ROOT_DIR_.'/modules/'.$this->name;
    }

    /**
     * install function.
     *
     * @access public
     * @return void
     */
    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('footer')
            || !$this->defaultConf()) {
            return false;
        }

        return true;
    }

    /**
     * uninstall function.
     *
     * @access public
     * @return void
     */
    public function uninstall()
    {
        if (!parent::uninstall()
            || !Configuration::deleteByName($this->name)) {
            return false;
        }

        return true;
    }

    /**
     * defaultConf function.
     *
     * @access public
     * @return void
     */
    public function defaultConf()
    {
        $conf = array();

        $conf['olarkid'] = '';

        if (!Configuration::updateValue($this->name, serialize($conf))) {
            return false;
        }

        return true;
    }

    /**
     * postProcess function.
     *
     * @access private
     * @return void
     */
    private function postProcess()
    {
        if (Tools::isSubmit('saveconf')) {
            $this->conf['olarkid'] = trim(Tools::getValue('olarkid'));
        }

        if (Configuration::updateValue($this->name, serialize($this->conf))) {
            return true;
        } else {
            $this->errors[] = $this->l('Error during saving settings');
            return false;
        }
    }

    /**
     * getContent function.
     *
     * @access public
     * @return void
     */
    public function getContent($tab = 'AdminModules')
    {
        $this->errors = array();
        $output = '';

        $form_url = AdminController::$currentIndex
            .'&amp;configure='.$this->name
            .'&amp;token='.Tools::getAdminToken($tab.(int)Tab::getIdFromClassName($tab).(int)$this->context->cookie->id_employee);

        if (Tools::isSubmit('saveconf')) {
            $this->postProcess();

            if (!count($this->errors)) {
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        $this->context->smarty->assign(array(
                'form_url' => $form_url,
                'tpl_path' => _PS_ROOT_DIR_.'/modules/'.$this->name,
                'img_path' => $this->_path.'views/img/',
                'description' => $this->description,
                'author' => $this->author,
                'name' => $this->name,
                'version' => $this->version,
                'ps_version' => _PS_VERSION_,
                'iso_code' => $this->mediacom87->isoCode(),
                'iso_domain' => $this->mediacom87->isoCode(true),
                'languages' => Language::getLanguages(false),
                'id_active_lang' => (int) $this->context->language->id,
                'link' => $this->context->link,
                'countries' => Country::getCountries((int) $this->context->language->id),
                'affiliateurl' => 'https://bit.ly/2sp41Fi',
                'paypalurl' => 'https://www.paypal.me/jeckyl/',
                'config' => $this->conf
            ));

        if (count($this->errors)) {
            $output .= $this->displayError(implode('<br />', $this->errors));
        }

        $this->context->controller->addJS(array(
                $this->_path.'libraries/js/riotcompiler.min.js',
                $this->_path.'libraries/js/pageloader.js',
                $this->_path.'views/js/back.js'
            ));

        $this->context->controller->addCSS($this->_path.'views/css/back.css');

        $output .= $this->display(__FILE__, 'views/templates/admin/admin.tpl');
        $output .= $this->display(__FILE__, 'libraries/prestui/ps-tags.tpl');

        return $output;
    }

    /**
     * hookFooter function.
     *
     * @access public
     * @return void
     */
    public function hookFooter()
    {
        if ($this->active) {
            $this->smarty->assign('conf', $this->conf);
            return $this->display(__FILE__, 'hook.tpl');
        }
    }
}
