{*

 * @package Jms Ajax Search

 * @version 1.1

 * @Copyright (C) 2009 - 2015 Joommasters.

 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

 * @Website: http://www.joommasters.com

*}



<div id="jms_ajax_search" class="btn-group compact-hidden default">	

	<div class="search-wrapper">

		<form method="get" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" id="searchbox">

			<input type="hidden" name="controller" value="search" />

			<input type="hidden" name="orderby" value="position" />

			<input type="hidden" name="orderway" value="desc" />
			<div class="input-group">
				<div class="input-group-btn" style="position: relative;top: -4px;">
		          <a style="height: 35px;" id="categories_dropdown" href="#" type="button" class="btn btn-default dropdown-toggle" data-target="#categories" data-toggle="dropdown">
		             Categories : <i>Toutes</i> <span class="caret"></span>
		          </a>
		          <ul id="categories" class="dropdown-menu">
		            <li onclick="setCategoryId('0', 'Toutes')">
		                <button type="button" class="categoryBtn btn-link">
		                Toutes
		                </button>
		            </li>
		            {foreach from=$rawCategories item=rawCategorie key=k}
		              {foreach from=$rawCategorie item=Categorie key=k2}
		                <li onclick="setCategoryId('{$Categorie.infos.id_category}', '{$Categorie.infos.name}')">
		                  <button type="button" class="categoryBtn btn-link">{$Categorie.infos.name}</button>
		                </li>
		              {/foreach}
		            {/foreach}
		          </ul>
		        </div><!-- /btn-group -->
				<input type="text" id="ajax_search" name="search_query" placeholder="{if $language.language_code=='fr'}Que recherchez vous?{else} What are you looking for ?{/if}" class="form-control" />	

			<span class="icon-magnifier ic2"></span>
			</div><!-- /input-group -->	

		</form>

		<div id="search_result">

		</div>

	</div>	

</div>
<script>
  var toggle = true;
  document.getElementById("categories_dropdown").addEventListener('click', function () {
    console.log("      document.getElementById(\"categories\").style.display = \"block\" ;\n") ;
    if(toggle) {
      document.getElementById("categories").style.display = "block" ;
      toggle = false ;
    }
    else {
      document.getElementById("categories").style.display = "none" ;
      toggle = true ;
    }
  }) ;
  function setCategoryId(id, category_name) {
    if(id !== "undefined")
    {
      $("#id_category").val(id) ;
      $("#categories_dropdown").find('i').text(category_name) ;
    }
  }
</script>
