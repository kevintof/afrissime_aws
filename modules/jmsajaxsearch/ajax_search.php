<?php
/**
* 2007-2014 PrestaShop
*
* Jms Ajax Search
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2014 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

require_once('../../config/config.inc.php');
require_once('../../init.php');
require_once(dirname(__FILE__).'/jmsajaxsearch.php');

$context = Context::getContext();
$languages = Language::getLanguages(false);
$search_key = Tools::getValue('search_key');
$category_id = Tools::getValue('category') ;

if (!$context) { $context = Context::getContext(); }
$currency = $context->currency;
$result_products = array();
$ajaxsearch = new JmsAjaxsearch();
$products = array();
$link = $context->link;
$count = 0;
$areEnoughProduct = true ;
$categorisedSearch = false ;
$simpleMessage = "" ;
$improvedSearch = false ;
$lang = $context->language->id ;

if (Tools::strlen($search_key) >= 3)
{
    // Rechercher en fonction d'une catégorie si possible
    if (isset($category_id) && intval($category_id) > 0)
    {
        $category = new Category($category_id, $lang, $context->shop->id) ;
        try {
            $products = $category->getProducts($lang, 1, 5);
        }
        catch (PrestaShopDatabaseException $e) {
            $product = false ;
        }
        $categorisedSearch = true ;
    }
    // Sinon en fonction du mot clé
    else {
        $products = Product::searchByName($lang, $search_key);
        $categorisedSearch = false ;
    }
    // Y-a-t-il assez de produit ?
    $areEnoughProduct = (is_bool($products) || is_int($products)) ? false : count($products) > 0 ;
    // Si on a cherché en fonction d'une catégorie, on reprend avec les mots clés uniquement
    if(!$areEnoughProduct)
    {
        $improvedSearch = true ;
        if($categorisedSearch) {
            $products = Product::searchByName($lang, $search_key);
        }
        else {
            $products = Product::getProducts($lang, 1, 10, 'name', 'ASC') ;
        }
    }

    $areEnoughProduct = (is_bool($products) || is_int($products)) ? false : count($products) > 0 ;
    // S'il n'y a toujours pas de produits on cherche top 10
    if(!$areEnoughProduct)
    {
        $improvedSearch = true ;
        $products = Product::getProducts($lang, 1, 10, 'name', 'ASC') ;
    }

    $total_products = count($products) ;
    if ($total_products)
    {
        for ($i = 0; $i < $total_products; $i++)
        {
            if (($products[$i]['name']) && ($products[$i]['active']))
            {
                $images = Image::getImages($context->language->id, $products[$i]['id_product']);
                $product = new Product($products[$i]['id_product']);
                if ((int)Configuration::get('JMS_AJAXSEARCH_DESC_COUNT') < Tools::strlen($product->description_short[$context->language->id]))
                {
                    $pos = strpos($product->description_short[$context->language->id], ' ', (int)Configuration::get('JMS_AJAXSEARCH_DESC_COUNT'));
                    $products[$i]['desc'] = Tools::substr($product->description_short[$context->language->id], 0, $pos);
                } else {
                    $products[$i]['desc'] = $product->description_short[$context->language->id];
                }

                $products[$i]['link'] = $link->getProductLink(
                    $products[$i]['id_product'], $product->link_rewrite[1], $product->id_category_default, $product->ean13
                );
                $products[$i]['link_rewrite'] = $product->link_rewrite[1];
                $products[$i]['id_image'] = $images[0]['id_image'];
//                $products[$i]['price'] = Tools::displayPrice(
//                    Tools::convertPrice($products[$i]['price'], $currency), $currency
//                );
                if ($count < Configuration::get('JMS_AJAXSEARCH_COUNT'))
                {
                    $result_products[] = $products[$i];
                    $count ++;
                } else {
                    break;
                }
            }
        }
    }
    $products = $result_products;

    $context->smarty->assign(array(
        'show_image' => Configuration::get('JMS_AJAXSEARCH_SHOW_IMAGE'),
        'show_desc' => Configuration::get('JMS_AJAXSEARCH_SHOW_DESC'),
        'show_price' => false,
        'link' => $context->link,
        'products' => $products,
        'no_text'   => $ajaxsearch->no_products,
        'improved_search'=>$improvedSearch
    ));
    $context->smarty->display(dirname(__FILE__).'/views/templates/hook/search-ajax.tpl');
}
else {
    echo '<div class="result_div">'.$ajaxsearch->morecharacter.'</div>';
}
