<?php
/**
 * Cron module
 *
 * @author    Samdha <contact@samdha.net>
 * @copyright Samdha
 * @license   commercial license see license.txt
 * @category  Prestashop
 * @category  Module
 * @link      http://www.gnu.org/licenses/lgpl.html logo license
 * @link      http://www.icon-king.com/ - David Vignoni - logo author
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include(_PS_MODULE_DIR_.'cron/autoloader.php');
spl_autoload_register('sdCronAutoload');

class Cron extends Samdha_Cron_Main
{
    public function __construct()
    {
        $this->author = 'Samdha';
        $this->tab = 'administration';
        $this->version = '2.0.2';
        $this->module_key = '';
        $this->name = 'cron';

        parent::__construct();

        $this->displayName = $this->l('Crontab for Prestashop');
        $this->description = $this->l('Allows modules to schedule jobs to run automatically at a certain time or date.');

        $this->description .= ' <div style="overflow:hidden;width:364px;height:45px;display:block"><iframe src="https://adsense.mediacom87.biz" width="728" height="90" marginheight="0" marginwidth="0" scrolling="no" style="border:none;-ms-zoom: 0.5;-moz-transform: scale(0.5);-moz-transform-origin: 0 0;-o-transform: scale(0.5);-o-transform-origin: 0 0;-webkit-transform: scale(0.5);-webkit-transform-origin: 0 0;"></iframe></div>';
    }
}
