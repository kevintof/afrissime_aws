<?php
/**
* 2007-2017 PrestaShop
*
* Jms Page Builder
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

if (!defined('_PS_VERSION_')) {
    exit;
}
include_once(_PS_MODULE_DIR_.'jmspagebuilder/addons/addonbase.php');
include_once(_PS_MODULE_DIR_.'jmspagebuilder/lib/instagramphp/instagram.php');
class JmsAddonInstagram extends JmsAddonBase
{
    public function __construct()
    {
        $this->modulename = 'jmspagebuilder';
        $this->addonname = 'instagram';
        $this->addontitle = 'Instagram';
        $this->addondesc = 'Show latest instagram images';
        $this->overwrite_tpl = '';
        $this->context = Context::getContext();
    }
    public function getInputs()
    {
        $inputs = array(
            array(
                'type' => 'text',
                'name' => 'username',
                'lang' => '0',
                'label' => $this->l('Instagram Username'),
                'desc' => 'Enter Instagram username',
                'default' => 'luvdragon3'
            ),
            array(
                'type' => 'text',
                'name' => 'access_token',
                'lang' => '0',
                'label' => $this->l('Access Token'),
                'desc' => 'Instagram API Access Token',
                'default' => '3234980746.7c96d22.e489ece15408429b99daa9a71355b2d7'
            ),
            array(
                'type' => 'text',
                'name' => 'instagram_to_display',
                'lang' => '0',
                'label' => $this->l('Number Images Instagram To Display'),
                'desc' => 'Number Images Instagram To Display',
                'default' => 5
            ),
            array(
                'type' => 'text',
                'name' => 'title',
                'label' => $this->l('Title'),
                'lang' => '1',
                'desc' => 'Enter text which will be used as addon title. Leave blank if no title is needed.',
                'default' => 'Product Carousel'
            ),
            array(
                'type' => 'text',
                'name' => 'desc',
                'label' => $this->l('Description'),
                'lang' => '1',
                'desc' => 'Enter text which will be used as addon description. Leave blank if no description is needed.',
                'default' => 'easy to load product multi rows and columns'
            ),
            array(
                'type' => 'text',
                'name' => 'rows',
                'label' => $this->l('Number of Rows'),
                'lang' => '0',
                'desc' => 'Number of Rows (Or Number of Product per Column)',
                'default' => 2
            ),
            array(
                'type' => 'text',
                'name' => 'cols',
                'label' => $this->l('Number of Columns'),
                'lang' => '0',
                'desc' => 'Number of Columns (Or Number of Product per Row) ( > 1199px )',
                'default' => 4
            ),
            array(
                'type' => 'text',
                'name' => 'cols_md',
                'label' => $this->l('Number of Columns On Medium Device'),
                'lang' => '0',
                'desc' => 'Number of Columns (Or Number of Product per Row) On Medium Device ( > 991px )',
                'default' => 3
            ),
            array(
                'type' => 'text',
                'name' => 'cols_sm',
                'label' => $this->l('Number of Columns On Tablet'),
                'lang' => '0',
                'desc' => 'Number of Columns (Or Number of Product per Row) On Tablet( >= 768px )',
                'default' => 2
            ),
            array(
                'type' => 'text',
                'name' => 'cols_xs',
                'label' => $this->l('Number of Columns On Mobile'),
                'lang' => '0',
                'desc' => 'Number of Columns (Or Number of Product per Row) On Mobile( >= 320px )',
                'default' => 2
            ),
            array(
                'type' => 'switch',
                'name' => 'navigation',
                'label' => $this->l('Show Navigation'),
                'lang' => '0',
                'desc' => 'Enanble/Disable Navigation',
                'default' => '1'
            ),
            array(
                'type' => 'switch',
                'name' => 'pagination',
                'label' => $this->l('Show Pagination'),
                'lang' => '0',
                'desc' => 'Enanble/Disable Pagination',
                'default' => '0'
            ),
            array(
                'type' => 'switch',
                'name' => 'autoplay',
                'label' => $this->l('Auto Play'),
                'lang' => '0',
                'desc' => 'Enanble/Disable Auto Play',
                'default' => '0'
            ),
            array(
                'type' => 'text',
                'name' => 'overwrite_tpl',
                'label' => $this->l('Overwrite Tpl File'),
                'lang' => '0',
                'desc' => 'Use When you want overwrite template file'
            )
        );
        return $inputs;
    }

    public function getInstaImages($fields, $insta) {
        $rows = $fields[5]->value;
        $cols = $fields[6]->value;
        $total_config = $fields[2]->value;
        $i = 0; 
        $list_img2 ='';
        $images_arr = array();
        if ($total_config > count($insta)) {
            $total_config = count($insta);
        }
        for( $i=0; $i<$total_config; $i++ ) {
            $images_arr [$i]['url']=$insta[$i]['images']['standard_resolution']['url'];
            $images_arr [$i]['link']=$insta[$i]["link"];
        }
        return JmsProductHelper::sliceProducts($images_arr, $rows, $cols, $total_config);
    }

    public function returnValue($addon)
    {
        $this->context = Context::getContext();       
        $id_lang = $this->context->language->id;
        $username = $addon->fields[0]->value; 
        $access_token = $addon->fields[1]->value; 
        $insta = new InstaWCD();
        $insta->username = $username;
        $insta->access_token = $access_token;
        $ins_media = $insta->userMedia();  
        $insta = $ins_media['data'];
        $insta_images = $this->getInstaImages($addon->fields, $insta);       
        $addon_tpl_dir = $this->loadTplDir();
        $this->context->smarty->assign(
            array(       
                'addon_title' => $addon->fields[3]->value->$id_lang,
                'addon_desc' => $addon->fields[4]->value->$id_lang,
                'insta_images' => $insta_images,
                'insta' => $insta,
                'cols'  => $addon->fields[6]->value,
                'cols_md'   => $addon->fields[7]->value,
                'cols_sm'   => $addon->fields[8]->value,
                'cols_xs'   => $addon->fields[9]->value,
                'navigation' => $addon->fields[10]->value,
                'pagination' => $addon->fields[11]->value,
                'autoplay' => $addon->fields[12]->value,
                'addon_tpl_dir' => $addon_tpl_dir
            )
        );
        $this->overwrite_tpl = $addon->fields[count($addon->fields)-1]->value;
        $template_path = $this->loadTplPath();
        return $this->context->smarty->fetch($template_path);
    }
}
