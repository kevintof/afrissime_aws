#User Guide Blog link:
======================
http://webkul.com/blog/prestashop-bitpay-payment-gateway/

#Support:
==========
Find us our support policy – https://store.webkul.com/support.html/

#Refund:
=========
Find us our refund policy – https://store.webkul.com/refund-policy.html/