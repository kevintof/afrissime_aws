<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{psbitpay}prestashop>psbitpay_f2cecf927cd6237de5c1ee1946f86d5a'] = 'скорост на транзакция';
$_MODULE['<{psbitpay}prestashop>psbitpay_655d20c1ca69519ca647684edbb2db35'] = 'Високо';
$_MODULE['<{psbitpay}prestashop>psbitpay_87f8a6ab85c9ced3702b4ea641ad4bb5'] = 'среда';
$_MODULE['<{psbitpay}prestashop>psbitpay_28d0edd045e05cf5af64e35ae0c4c6ef'] = 'ниско';
$_MODULE['<{psbitpay}prestashop>psbitpay_c9cc8cce247e49bae79f15173ce97354'] = 'Запази';
$_MODULE['<{psbitpay}prestashop>psbitpay_c888438d14855d7d96a2724ee9c306bd'] = 'Настройките обновени';
$_MODULE['<{psbitpay}prestashop>psbitpay_66bb2ffd41cdbbd80e238aeb32afb6c7'] = 'се изисква API ключ.';
$_MODULE['<{psbitpay}prestashop>bitpay_error_c02034b1cff4fde56cb3eadc40466f8f'] = 'Съобщение за грешка';
$_MODULE['<{psbitpay}prestashop>admin-order_61b0226145fd2be137cfcdec78ccef3d'] = 'BitPay подробности за транзакцията';
$_MODULE['<{psbitpay}prestashop>admin-order_232f4b53f1b82ae84b71b38eb70958e1'] = 'Номер на транзакцията';
$_MODULE['<{psbitpay}prestashop>admin-order_ec53a8c4f07baed5d8825072c89799be'] = 'Статус';
$_MODULE['<{psbitpay}prestashop>admin-order_7cf626ac15afd59a810ec768f6d1b767'] = 'фактура URL';
$_MODULE['<{psbitpay}prestashop>admin-order_e0010a0a1a3259ab5c06a19bad532851'] = 'платен';
$_MODULE['<{psbitpay}prestashop>admin-order_eaf44b0d5410d22d5290eab1f78d336d'] = 'Валутен код';
$_MODULE['<{psbitpay}prestashop>admin-order_8b7eb5344d33968d1774640a33192eee'] = 'потвърждение Детайли';
$_MODULE['<{psbitpay}prestashop>admin-order_8883640bd8db1d07a68ad7657e6e6896'] = 'Потвърждение: ';
$_MODULE['<{psbitpay}prestashop>admin-order_62d0dc07c9b3a0b82cd305c900556ed8'] = 'Потвърждение на времето: ';
$_MODULE['<{psbitpay}prestashop>admin-order_551d6065dcf92622b1f1c22a123ff8aa'] = 'Статус потвърждение: ';
$_MODULE['<{psbitpay}prestashop>payment_return_ea9b81cc3e7df4b06eaaec7613ed58b6'] = 'Поръчката Ви% и е пълна с';
$_MODULE['<{psbitpay}prestashop>payment_return_b8af13ea9c8fe890c9979a1fa8dbde22'] = 'препратка';
$_MODULE['<{psbitpay}prestashop>payment_return_d79cf3f429596f77db95c65074663a54'] = 'Идентификационен номер на поръчката';
$_MODULE['<{psbitpay}prestashop>payment_return_0db71da7150c27142eef9d22b843b4a9'] = 'За всякакви въпроси или за повече информация, моля свържете се с нашия';
$_MODULE['<{psbitpay}prestashop>payment_return_decce112a9e64363c997b04aa71b7cb8'] = 'отдел \"Обслужване на клиенти.';
$_MODULE['<{psbitpay}prestashop>payment_return_66c7d67e54eabef06ee24a93593d1154'] = 'Благодаря за вашата покупка.';
$_MODULE['<{psbitpay}prestashop>payment_return_1630286a4f4b2fd351968213c6d3079e'] = 'Вашата поръчка е в очакване за валидиране плащане от BitPay. Вие ще получите уведомлението след всяка актуализация ред.';
$_MODULE['<{psbitpay}prestashop>payment_return_b34c9c89c904d9915a31520b2572797f'] = 'За всяка заявка се колебайте да се свържете с';
$_MODULE['<{psbitpay}prestashop>payment_return_9064bc3da6a1f111ad77a3c681632708'] = 'отдел \"Обслужване на клиенти';
$_MODULE['<{psbitpay}prestashop>payment_return_55a838516b6f95567800eb12a9f4e0d8'] = 'по всяко време.';
$_MODULE['<{psbitpay}prestashop>payment_return_46d82042b6339d95b044b9b1de14db92'] = 'Вашата поръчка е Референция:';
$_MODULE['<{psbitpay}prestashop>payment_return_9285c655c0b8a63b17e56cbf8a06cd4d'] = 'Идентификационният номер на поръчката:';
