<?php
/**
* 2010-2016 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2016 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class PsBitPayPaymentModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    private $APIKey;
    private $TransactionSpeed;
    private $SandboxStatus;

    public function __construct()
    {
        parent::__construct();
        $this->APIKey = Configuration::get('WK_BITPAY_API_KEY');
        $this->TransactionSpeed = Configuration::get('WK_BITPAY_API_SPEED');
        $this->SandboxStatus = Configuration::get('WK_BITPAY_SANDBOX');
    }

    public function initContent()
    {
        $this->display_column_left = false;
        $this->display_column_right = false;

        parent::initContent();
        if ($this->context->customer->id) {
            $cartTotal = $this->context->cart->getOrderTotal(true);
            if ($this->module->active && $cartTotal) {
                $idCart = $this->context->cart->id;
                if (isset($this->context->cart->id_shop)) {
                    $idShop = $this->context->cart->id_shop;
                } else {
                    $idShop = 0;
                }

                $objCurrency = new Currency($this->context->cart->id_currency);
                $objAddress = new Address($this->context->cart->id_address_delivery);
                $secureData = md5($idCart.$this->APIKey.$idShop);
                $bodyparams = array(
                    'buyerName' => $this->context->customer->firstname.' '.$this->context->customer->lastname,
                    'buyerAddress1' => $objAddress->address1,
                    'buyerAddress2' => $objAddress->address2,
                    'buyerCity' => $objAddress->city,
                    'buyerState' => State::getNameById($objAddress->id_state),
                    'buyerZip' => $objAddress->postcode,
                    'buyerPhone' => $objAddress->phone,
                    'buyerEmail' => $this->context->customer->email,
                    'buyerCountry' => Country::getNameById($this->context->cart->id_lang, $objAddress->id_country),
                    'transactionSpeed' => $this->TransactionSpeed,
                    'fullNotifications' => 'true',
                    'price' => $cartTotal,
                    'currency' => $objCurrency->iso_code,
                    'redirectURL' => $this->context->link->getPageLink(
                        'order-confirmation.php',
                        null,
                        null,
                        array(
                            'id_cart' => (int)$idCart,
                            'key' => $this->context->customer->secure_key,
                            'id_module' => $this->module->id
                        )
                    ),
                    'notificationURL' => $this->context->link->getModuleLink(
                        'psbitpay',
                        'validation',
                        array(
                            'idCart' => (int)$idCart,
                            'idShop' => (int)$idShop,
                            'secureData' => $secureData
                        ),
                        Configuration::get('PS_SSL_ENABLED')
                    )
                );

                $bodyparams = Tools::jsonEncode($bodyparams);
                try {
                    //create request and add headers
                    $length = Tools::strlen($bodyparams);
                    $apiKey = base64_encode($this->APIKey);
                    $headers = array(
                        'Content-Type: application/json',
                        'Content-Length: ' . $length,
                        'Authorization: Basic ' . $apiKey
                    );

                    $sandbox = '';
                    if ($this->SandboxStatus == 1) {
                        $sandbox = 'test.';
                    }
                    $url = 'https://'.$sandbox.'bitpay.com/api/invoice/';

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyparams);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    $response = Tools::jsonDecode(curl_exec($ch), true);

                    if ($response) {
                        if (array_key_exists('url', $response)) {
                            Tools::redirect($response['url']);
                        } elseif (array_key_exists('error', $response)) {
                            $this->context->smarty->assign('errorMessage', $response['error']['message']);
                            $this->setTemplate('module:psbitpay/views/templates/front/bitpay_error.tpl');
                        } else {
                            $this->context->smarty->assign(
                                'errorMessage',
                                $this->module->l(
                                    'Invoice url not found in API. Please contact our customer service department.',
                                    'payment'
                                )
                            );
                            $this->setTemplate('module:psbitpay/views/templates/front/bitpay_error.tpl');
                        }
                    } else {
                        $this->context->smarty->assign(
                            'errorMessage',
                            $this->module->l(
                                'No response found from API. Please contact our customer service department.',
                                'payment'
                            )
                        );
                        $this->setTemplate('module:psbitpay/views/templates/front/bitpay_error.tpl');
                    }
                } catch (Exception $e) {
                    $this->context->smarty->assign('errorMessage', $e->getMessage());
                    $this->setTemplate('module:psbitpay/views/templates/front/bitpay_error.tpl');
                }
            }
        } else {
            Tools::redirect($this->context->link->getPageLink('my-account'));
        }
    }
}
