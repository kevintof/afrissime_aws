<?php
/**
* 2010-2016 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2016 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

/* You can debug the data by the DEBUG true in the module ipn.log file*/
define('DEBUG', false);
define('LOG_FILE', _PS_MODULE_DIR_.'psbitpay/ipn.log');
class PsBitPayValidationModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    private $APIKey;

    public function __construct()
    {
        parent::__construct();
        $this->APIKey = Configuration::get('WK_BITPAY_API_KEY');
    }

    public function initContent()
    {
        $this->psbitpay = new PsBitPay();
        if ($this->psbitpay->active) {
            $isValid = $this->validateIpnData();
            if ($isValid) {
                $this->createBitPayOrder();
            }
        }
        /* send back "OK" to api */
        die('OK');
    }

    public function validateIpnData()
    {
        $secureData = Tools::getValue('secureData');
        $idCart = Tools::getValue('idCart');
        $idShop = Tools::getValue('idShop');

        if ($secureData && $idCart && $idShop) {
            $actualData = md5($idCart.$this->APIKey.$idShop);
            if ($actualData == $secureData) {
                return true;
            }
        }

        return false;
    }

    public function createBitPayOrder()
    {
        $idShop = Tools::getValue('idShop');
        $idCart = Tools::getValue('idCart');
        $context = Context::getContext();

        // Setup context shop
        $shop = new Shop((int) $idShop);
        $context->shop = $shop;

        // Setup context cart
        $cart = new Cart((int) $idCart);
        $context->cart = $cart;

        if (Validate::isLoadedObject($cart)) {
            $paymentDetails = $this->getPaymentDetails();
            if ($paymentDetails) {
                $currency = new Currency((int) Currency::getIdByIsoCode($paymentDetails['currency']));
                if (!Validate::isLoadedObject($currency) || $currency->id != $cart->id_currency) {
                    if (DEBUG == true) {
                        error_log(
                            date('[Y-m-d H:i e] ').'Invalid Currency Code: '.$paymentDetails['currency'].PHP_EOL,
                            3,
                            LOG_FILE
                        );
                    }
                } else {
                    /* set context currency to the order currency */
                    $context->currency = $currency;
                    $paymentStatus = Tools::strtoupper($paymentDetails['status']);

                    if ($paymentStatus != 'NEW'
                    && $paymentStatus != 'EXPIRED'
                    && $paymentStatus != 'INVALID'
                    && $paymentStatus != 'FALSE') {
                        //check payment status
                        if ($paymentStatus == 'COMPLETE') {
                            $orderStatus = (int) Configuration::get('PS_OS_PAYMENT');
                        } elseif ($paymentStatus == 'PAID'
                        || $paymentStatus == 'CONFIRMED'
                        || $paymentStatus == 'COMPLETED'
                        || $paymentStatus == 'PAIDOVER'
                        || $paymentStatus == 'PAIDLATE'
                        || $paymentStatus != 'PAIDPARTIAL') {
                            if (Configuration::get('WK_BITPAY_OS')) {
                                $orderStatus = (int) Configuration::get('WK_BITPAY_OS');
                            } else {
                                $orderStatus = (int) Configuration::get('PS_OS_PAYMENT');
                            }
                        } else {
                            $orderStatus = (int) Configuration::get('PS_OS_ERROR');
                        }

                        if ($cart->OrderExists()) {
                            $this->addConfirmationDetails($paymentDetails);
                            $order = new Order((int) Order::getOrderByCartId($cart->id));
                            $currentOrderState = (int) $order->getCurrentState();
                            if ($currentOrderState != $orderStatus) {
                                $objHistory = new OrderHistory();
                                $objHistory->id_order = (int) $order->id;
                                $objHistory->changeIdOrderState((int) $orderStatus, $order, true);
                                $objHistory->addWithemail(true);
                            }
                        } elseif (array_key_exists('price', $paymentDetails)) {
                            $customer = new Customer((int) $cart->id_customer);
                            $context->customer = $customer;

                            if ($this->psbitpay->validateOrder(
                                (int) $cart->id,
                                (int) $orderStatus,
                                $paymentDetails['price'],
                                $this->psbitpay->displayName,
                                null,
                                array(),
                                null,
                                false,
                                $customer->secure_key,
                                $shop
                            )) {
                                $this->addTransactionDetails($paymentDetails);
                            }
                        }
                    }
                }
            } else {
                if (DEBUG == true) {
                    error_log(date('[Y-m-d H:i e] ').'Payment Details not find'.PHP_EOL, 3, LOG_FILE);
                }
            }
        } else {
            if (DEBUG == true) {
                error_log(date('[Y-m-d H:i e] ').'Invalid Cart ID: '.$idCart.PHP_EOL, 3, LOG_FILE);
            }
        }
    }

    public function getPaymentDetails()
    {
        $response = fopen('php://input', 'r');
        $response = fgets($response);
        $response = Tools::jsonDecode($response, true);
        if ($response) {
            return $response;
        } else {
            return false;
        }
    }

    public function addTransactionDetails($paymentDetails)
    {
        $objTransaction = new PsBitPayTransaction();
        $objTransaction->transaction_id = $paymentDetails['id'];
        $objTransaction->status = $paymentDetails['status'];
        $objTransaction->invoice_url = $paymentDetails['url'];
        $objTransaction->currency_code = $paymentDetails['currency'];
        $objTransaction->price_paid = $paymentDetails['price'];
        $objTransaction->id_cart = Tools::getValue('idCart');
        $objTransaction->save();
    }

    public function addConfirmationDetails($paymentDetails)
    {
        $transactionId = $paymentDetails['id'];
        if ($transactionId) {
            $id = PsBitPayTransaction::getIdByTransactionId($transactionId);
            if ($id) {
                $objConfirmation = new PsBitPayConfirmations();
                $objConfirmation->id_transaction = $id;
                $objConfirmation->confirmation_info = Tools::jsonEncode($paymentDetails);
                $objConfirmation->confirmations = PsBitPayConfirmations::getConfirmationsByTransactionId($id);
                $objConfirmation->save();
            }
        }
    }
}
