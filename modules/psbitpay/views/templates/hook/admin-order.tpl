{**
* 2010-2016 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2016 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{if isset($bitpayTransaction) && $bitpayTransaction}
	<div class="panel">
		<div class="panel-heading">
			<i class="icon-bitcoin"></i>
			{l s='BitPay Transaction Details' mod='psbitpay'}
		</div>
		<table class="table">
			<tr>
				<td>{l s='Transaction Id' mod='psbitpay'}</td>
				<td>{$bitpayTransaction.transaction_id}</td>
			</tr>
			<tr>
				<td>{l s='Status' mod='psbitpay'}</td>
				<td>{$bitpayTransaction.status}</td>
			</tr>
			<tr>
				<td>{l s='Invoice URL' mod='psbitpay'}</td>
				<td>
					<a href="{$bitpayTransaction.invoice_url}" target="_blank">{$bitpayTransaction.invoice_url}</a>
				</td>
			</tr>
			<tr>
				<td>{l s='Paid' mod='psbitpay'}</td>
				<td>{$bitpayTransaction.price_paid}</td>
			</tr>
			<tr>
				<td>{l s='Currency Code' mod='psbitpay'}</td>
				<td>{$bitpayTransaction.currency_code}</td>
			</tr>
			{if isset($confirmationsDetails)}
				<tr>
					<td>{l s='Confirmation Details' mod='psbitpay'}</td>
					<td>
						{foreach $confirmationsDetails as $confirmation}
						<p>
							<b>{l s='Confirmation: ' mod='psbitpay'}</b>{$confirmation['confirmations']}
							<b>{l s='Confirmation Time: ' mod='psbitpay'}</b>{$confirmation['date_add']}
							{if isset($confirmation['confirmation_info']->status)}
								<b>{l s='Confirmation Status: ' mod='psbitpay'}</b>
								{$confirmation['confirmation_info']->status}
							{/if}
						</p>
						{/foreach}
					</td>
				</tr>
			{/if}
		</table>
	</div>
{/if}