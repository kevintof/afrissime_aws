CREATE TABLE IF NOT EXISTS `PREFIX_psbitpay_transaction` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `transaction_id` varchar(255) character set utf8 NOT NULL,
  `status` varchar(255) character set utf8 NOT NULL,
  `invoice_url` text(1000),
  `currency_code`  varchar(3) NOT NULL,
  `price_paid` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `id_cart` int(11) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_psbitpay_confirmations` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `id_transaction` int(11) unsigned NOT NULL,
  `confirmation_info` text(2000),
  `confirmations` int(11) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;