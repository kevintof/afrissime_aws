<?php
/**
* 2010-2016 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2016 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class PsBitPayTransaction extends ObjectModel
{
    public $transaction_id;
    public $status;
    public $invoice_url;
    public $currency_code;
    public $price_paid;
    public $id_cart;
    public $date_add;

    public static $definition = array(
        'table' => 'psbitpay_transaction',
        'primary' => 'id',
        'fields' => array(
            'transaction_id' => array('type' => self::TYPE_STRING),
            'status' => array('type' => self::TYPE_STRING),
            'invoice_url' => array('type' => self::TYPE_STRING),
            'currency_code' => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 3),
            'price_paid' => array('type' => self::TYPE_FLOAT),
            'id_cart' => array('type' => self::TYPE_INT),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat')
        )
    );

    public static function getIdByTransactionId($transactionId)
    {
        return Db::getInstance()->getValue(
            'SELECT `id` FROM `'._DB_PREFIX_.'psbitpay_transaction`
			WHERE `transaction_id` = \''.pSQL($transactionId).'\''
        );
    }

    public static function getTransactionByIdCart($idCart)
    {
        return Db::getInstance()->getRow(
            'SELECT * FROM `'._DB_PREFIX_.'psbitpay_transaction`
			WHERE `id_cart` = '.(int) $idCart
        );
    }
}
