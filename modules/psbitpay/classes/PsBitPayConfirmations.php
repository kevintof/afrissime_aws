<?php
/**
* 2010-2016 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2016 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class PsBitPayConfirmations extends ObjectModel
{
    public $id_transaction;
    public $confirmation_info;
    public $confirmation;
    public $date_add;

    public static $definition = array(
        'table' => 'psbitpay_confirmations',
        'primary' => 'id',
        'fields' => array(
            'id_transaction' => array('type' => self::TYPE_INT),
            'confirmation_info' => array('type' => self::TYPE_STRING),
            'confirmations' => array('type' => self::TYPE_INT),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat')
        )
    );

    public static function getConfirmationsByTransactionId($idTransaction)
    {
        $count = Db::getInstance()->getValue(
            'SELECT count(`id`) FROM `'._DB_PREFIX_.'psbitpay_confirmations`
			WHERE `id_transaction` = '.(int) $idTransaction
        );

        if ($count) {
            return ($count + 1);
        } else {
            return 1;
        }
    }

    public static function getConfirmationDetailsByTransactionId($idTransaction)
    {
        return Db::getInstance()->executeS(
            'SELECT * FROM `'._DB_PREFIX_.'psbitpay_confirmations`
			WHERE `id_transaction` = '.(int) $idTransaction
        );
    }
}
