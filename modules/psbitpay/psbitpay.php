<?php
/**
* 2010-2016 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2016 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

include_once dirname(__FILE__).'/classes/PsBitPayTransaction.php';
include_once dirname(__FILE__).'/classes/PsBitPayConfirmations.php';
class PsBitPay extends PaymentModule
{
    const INSTALL_SQL_FILE = 'install.sql';
    private $htmlData = '';
    public function __construct()
    {
        $this->name = 'psbitpay';
        $this->tab = 'payments_gateways';
        $this->version = '4.1.0';
        $this->module_key = '6107157d496ead5d4b7787ad43e8f79e';
        $this->author = 'Webkul';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Bitpay Payment Gateway');
        $this->description = $this->l('Accept BitCoins by Bitpay Payment Gateway.');
    }

    public function renderForm()
    {
        $fields_form = array();
        $fields_form['form'] = array(
            'legend' => array(
                'title' => $this->l('Bitpay Settings'),
                'icon' => 'icon-cogs',
            ),
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Sandbox Mode'),
                    'name' => 'WK_BITPAY_SANDBOX',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                        ),
                    ),
                    'required' => true,
                    'hint' => $this->l('Bitpay mode'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('BitPay API KEY'),
                    'name' => 'WK_BITPAY_API_KEY',
                    'required' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Transaction Speed'),
                    'name' => 'WK_BITPAY_API_SPEED',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'id' => 'high',
                                'name' => $this->l('High'),
                            ),
                            array(
                                'id' => 'medium',
                                'name' => $this->l('Medium'),
                            ),
                            array(
                                'id' => 'low',
                                'name' => $this->l('Low'),
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name',
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            ),

        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
        '&configure='.$this->name.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'WK_BITPAY_SANDBOX' => Tools::getValue('WK_BITPAY_SANDBOX', Configuration::get('WK_BITPAY_SANDBOX')),
            'WK_BITPAY_API_KEY' => Tools::getValue('WK_BITPAY_API_KEY', Configuration::get('WK_BITPAY_API_KEY')),
            'WK_BITPAY_API_SPEED' => Tools::getValue('WK_BITPAY_API_SPEED', Configuration::get('WK_BITPAY_API_SPEED')),
        );
    }

    private function postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('WK_BITPAY_SANDBOX', Tools::getValue('WK_BITPAY_SANDBOX'));
            Configuration::updateValue('WK_BITPAY_API_KEY', Tools::getValue('WK_BITPAY_API_KEY'));
            Configuration::updateValue('WK_BITPAY_API_SPEED', Tools::getValue('WK_BITPAY_API_SPEED'));
            $this->htmlData .= $this->displayConfirmation($this->l('Settings updated'));
        }
    }

    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            if (Tools::getValue('WK_BITPAY_API_KEY') == '') {
                $this->context->controller->errors[] = $this->l('API KEY is required.');
            } else {
                $this->postProcess();
            }
        } else {
            $this->htmlData .= '<br />';
        }
        $this->htmlData .= $this->renderForm();

        return $this->htmlData;
    }

    public function install()
    {
        /* The cURL PHP extension must be enabled to use this module */
        if (!function_exists('curl_version')) {
            $this->_errors[] = $this->l('Sorry, this module requires the cURL PHP Extension (http://www.php.net/curl),
            which is not enabled on your server. Please ask your hosting provider for assistance.');

            return false;
        }

        if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) {
            return (false);
        } elseif (!$sql = Tools::file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) {
            return (false);
        }
        $sql = str_replace(array(
            'PREFIX_',
            'ENGINE_TYPE',
        ), array(
            _DB_PREFIX_,
            _MYSQL_ENGINE_,
        ), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", $sql);
        foreach ($sql as $query) {
            if ($query) {
                if (!Db::getInstance()->execute(trim($query))) {
                    return false;
                }
            }
        }

        if (Configuration::get('WK_BITPAY_SANDBOX') != 1) {
            Configuration::updateValue('WK_BITPAY_SANDBOX', 1);
        }

        if (!parent::install()
            || !$this->checkAndCreateOrderStatus()
            || !$this->registerHook('paymentOptions')
            || !$this->registerHook('paymentReturn')
            || !$this->registerHook('adminOrder')
            ) {
            return false;
        }

        return true;
    }

    public function checkAndCreateOrderStatus()
    {
        $idOrderStatus = Configuration::get('WK_BITPAY_OS');
        if ($idOrderStatus) {
            $statusInfo = Db::getInstance()->getRow(
                'SELECT * FROM `'._DB_PREFIX_.'order_state` WHERE `id_order_state` = '.(int) $idOrderStatus
            );
            if (!$statusInfo) {
                $this->createOrderStatus();
            }
        } else {
            $this->createOrderStatus();
        }

        return true;
    }

    public function createOrderStatus()
    {
        $orderState = new OrderState();
        foreach (Language::getLanguages(true) as $lang) {
            $orderState->name[$lang['id_lang']] = 'Pending payment validation from BitPay';
        }
        $orderState->color = '#4169E1';
        $orderState->save();
        if ($orderState->id) {
            Configuration::updateValue('WK_BITPAY_OS', $orderState->id);
            // Copy Logo
            copy(dirname(__FILE__).'/logo.png', _PS_ORDER_STATE_IMG_DIR_. (int)$orderState->id.'.gif');
        }
    }

    public function hookAdminOrder($params)
    {
        $idCart = Cart::getCartIdByOrderId($params['id_order']);
        $bitpayTransaction = PsBitPayTransaction::getTransactionByIdCart($idCart);
        if ($bitpayTransaction) {
            $this->context->smarty->assign('bitpayTransaction', $bitpayTransaction);
            $confirmationsDetails = PsBitPayConfirmations::getConfirmationDetailsByTransactionId(
                $bitpayTransaction['id']
            );
            if ($confirmationsDetails) {
                foreach ($confirmationsDetails as &$confirmation) {
                    $confirmation['confirmation_info'] = Tools::jsonDecode($confirmation['confirmation_info']);
                }
                $this->context->smarty->assign('confirmationsDetails', $confirmationsDetails);
            }

            return $this->display(__FILE__, 'admin-order.tpl');
        }
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active) {
            return;
        }

        if (isset($params['order'])) {
            $this->smarty->assign(array(
                'id_order' => $params['order']->id,
                'valid' => $params['order']->valid,
            ));
        }

        if (isset($params['order']->reference) && !empty($params['order']->reference)) {
            $this->smarty->assign('reference', $params['order']->reference);
        }

        return $this->fetch('module:psbitpay/views/templates/hook/payment_return.tpl');
    }

    public function hookPaymentOptions()
    {
        if (!$this->active) {
            return;
        }

        // payment option will not display untill bitpay settings not filled
        if (Configuration::get('WK_BITPAY_API_KEY')) {
            $payment_options = array($this->getBitPayPaymentOption());

            return $payment_options;
        }
    }

    public function getBitPayPaymentOption()
    {
        $paymentOption = new PaymentOption();
        $paymentOption->setCallToActionText($this->trans('Pay with Bitcoin'))
                       ->setAction($this->context->link->getModuleLink($this->name, 'payment', array(), true));

        return $paymentOption;
    }

    public function uninstall()
    {
        if (!parent::uninstall()
            || !$this->dropTable()
            || !$this->deleteConfigVariable()) {
            return false;
        }

        return true;
    }

    public function dropTable()
    {
        return Db::getInstance()->execute(
            'DROP TABLE IF EXISTS
            `'._DB_PREFIX_.'psbitpay_transaction`,
            `'._DB_PREFIX_.'psbitpay_confirmations`'
        );
    }

    public function deleteConfigVariable()
    {
        $config_key = array('WK_BITPAY_SANDBOX', 'WK_BITPAY_API_KEY', 'WK_BITPAY_API_SPEED');

        foreach ($config_key as $key) {
            if (!Configuration::deleteByName($key)) {
                return false;
            }
        }

        return true;
    }
}
