<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class CacheMiddleware
{
    public function __invoke($request, $response, $next)
    {

        $get_headers = array('lang', 'currency', 'email', 'password', 'order', 'column', 'cart', 'shop', 'type');

        $md5 = '';

        foreach($get_headers as $get_header) {
            $md5 =  md5($md5 . json_encode($request->getHeader($get_header)));
        }

        $uri = $request->getUri();
        $params = json_encode($request->getParams());

        $md5 =  md5($md5 . $uri . $params);

        $cache_file = _PS_MODULE_DIR_  . 'prestapp/api/cache/' . $md5 . '.json';

        if(file_exists($cache_file) ) {
            $json_results = json_decode(file_get_contents($cache_file));
            $response = sendOk($response, $json_results);
            return $response;
        } else {
            $response = $next($request, $response);
            $fp = fopen(_PS_MODULE_DIR_  . 'prestapp/api/cache/' . $md5 . '.json', 'w');
            $json_results = $response->getBody()->__toString();
            $p = json_encode(json_decode($json_results)->data);
            file_put_contents($cache_file, $p);
            return $response;
        }
    }
}