<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class TokenMiddleware
{
    public function __invoke($request, $response, $next)
    {
        $exceptions = array(
            '/modules/payments/paypal/validate',
            '/modules/payments/paypal/approve',
            '/modules/payments/paypal/cancel' 
        );

        $route = $request->getAttribute('route');

        if ($route) {
            $routeName = $route->getName();
        }

        if ('OPTIONS' == $_SERVER['REQUEST_METHOD'] || in_array($routeName, $exceptions)) {
            $response = $next($request, $response);
        } else {
            //get the PrestApp module token
            $token = Configuration::get('PRESTAPP_API_KEY');

            //get the sent token in "Authorization" header
            $sent_token = $request->getHeaderLine('Authorization');

            //Checking the token sent in the request
            if (!$sent_token || $sent_token !== $token) {
                $data = array(
                    'sent_token' => $sent_token,
                    'message' => 'Token is invalid',
                );

                //sent unauthorized message
                $response = sendUnauthorized($response, $data);
            } else {
                //continue
                $response = $next($request, $response);
            }
        }

        return $response;
    }
}
