<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class AuthenticationMiddleware
{
    public function __invoke($request, $response, $next)
    {

        //get email and password sent in headers
        $email = $request->getHeaderLine('email');
        $decrypted_password = $request->getHeaderLine('password');
        $type = $request->getHeaderLine('type');

        // $iv = $request->getHeaderLine("iv");
        // $secure_key = Configuration::get("PRESTAPP_SECURE_KEY");
        // $algorythm = "aes-256-cbc";
        // $options = OPENSSL_ZERO_PADDING;
        // $decrypted_password = openssl_decrypt($hashed_password, $algorythm, $secure_key, $options, $iv);

        //if email and password are provided
        if ($email && $decrypted_password) {
            //we check the validity of both
            if (!Validate::isEmail($email)) {
                $data = array(
                    'message' => 'Entered email is incorrect',
                );
                $response = sendUnauthorized($response, $data);
            } elseif (!Validate::isPasswd($decrypted_password)) {
                $data = array(
                    'message' => 'Entered password is incorrect',
                );
                $response = sendUnauthorized($response, $data);
            } else {
                //if entered credentials seems ok, we check if an account match with it
                $customer = PrestAppCustomerController::login($email, $decrypted_password, $type == 'facebook' ? true : false);
                if ('INACTIVE' === $customer) {
                    $data = array(
                        'message' => 'Your account isn\'t available at this time',
                    );
                    $response = sendUnauthorized($response, $data);
                } elseif ('NOTFOUND' === $customer) {
                    $data = array(
                        'message' => 'Authentication failed or user not found',
                    );
                    $response = sendUnauthorized($response, $data);
                } else {
                    //user found and authenticated
                    $customer->logged = 1;
                    $request = $request->withAttribute('customer', $customer);
                    $context = createContext($request);
                    $request = $request->withAttribute('context', $context);
                    $response = $next($request, $response);
                }
            }
        } else {
            //no credentials provided
            $customer = new Customer();
            $customer->id = 0;
            $request = $request->withAttribute('customer', $customer);
            $context = createContext($request);
            $request = $request->withAttribute('context', $context);
            $response = $next($request, $response);
        }
        return $response;
    }
}
