<?php

$status = [];
$isError = false;

// Check PHP version
if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);
    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

if (PHP_VERSION_ID < 50600) {
    $isError = true;
    $status['php'] = "PHP_VERSION_NOT_SUPPORTED";
}

// Check PrestaShop Version
include_once dirname(__FILE__) . '/../../../config/config.inc.php';
if (version_compare(_PS_VERSION_, '1.5.6.3', '<')) {
    $isError = true;
    $status['prestashop'] = "PRESTASHOP_VERSION_LOW";
} elseif (version_compare(_PS_VERSION_, '1.7.9', '>')) {
    $isError = true;
    $status['prestashop'] = "PRESTASHOP_VERSION_HIGH";
}

//check if the server is apache or other
$status['server_software'] = strpos($_SERVER['SERVER_SOFTWARE'], 'apache') !== false ? 'apache' : 'other';

if ($status['server_software'] === 'apache') {
    // Check mod_rewrite && mod_headers
    if (function_exists('apache_get_modules')) {
        if (!in_array('mod_rewrite', apache_get_modules())) {
            $isError = true;
            $status['mod_rewrite'] = "MOD_REWRITE_DISABLED";
        }
        if (!in_array('mod_headers', apache_get_modules())) {
            $isError = true;
            $status['mod_headers'] = "MOD_HEADERS_DISABLED";
        }
    } else {
        if (getenv('HTTP_MOD_REWRITE') != 'On') {
            if (!isset($_SERVER['REDIRECT_HTTP_MOD_REWRITE'])) {
                $isError = true;
                $status['mod_rewrite'] = "MOD_REWRITE_DISABLED";
            }
        }
        if (getenv('HTTP_MOD_HEADERS') != 'On') {
            if (!isset($_SERVER['REDIRECT_HTTP_MOD_HEADERS'])) {
                $isError = true;
                $status['mod_headers'] = "MOD_HEADERS_DISABLED";
            }
        }
    }
}

// Return errors
if ($isError == true) {
    $status['overall'] = 'BAD_CONFIGURATION';
    http_response_code(500);
    echo json_encode($status);
    exit();
}

ob_start('fatal_error_handler');

//slim dependencies
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

//prestashop dependencies
require '../vendor/autoload.php';
include_once '../../../config/settings.inc.php';
include_once '../../../config/defines.inc.php';
include_once '../../../config/config.inc.php';
include_once '../../../init.php';

//api helpers
include_once 'helpers/response.php';
include_once 'helpers/utils.php';

//api controllers
include_once 'controllers/PrestAppCustomerController.php';
include_once 'controllers/PrestAppProductController.php';
include_once 'controllers/PrestAppCartController.php';
include_once 'controllers/PrestAppImageController.php';
include_once 'controllers/PrestAppAddressController.php';
include_once 'controllers/PrestAppOrderController.php';
include_once 'controllers/PrestAppLanguageController.php';
include_once 'controllers/PrestAppCurrencyController.php';
include_once 'controllers/PrestAppContactController.php';
include_once 'controllers/PrestAppCreditController.php';
include_once 'controllers/PrestAppCouponController.php';
include_once 'controllers/PrestAppSearchController.php';
include_once 'controllers/PrestAppCMSController.php';
include_once 'controllers/PrestAppShippingController.php';
include_once 'controllers/PrestAppCustomController.php';

//api
include_once 'controllers/modules/PrestAppSoliberteModuleController.php';
include_once 'controllers/modules/PrestAppMondialrelayModuleController.php';
include_once 'controllers/modules/PrestAppBankwireModuleController.php';
include_once 'controllers/modules/PrestAppChequeModuleController.php';
include_once 'controllers/modules/PrestAppPsWirepaymentModuleController.php';
include_once 'controllers/modules/PrestAppPsCheckpaymentModuleController.php';
include_once 'controllers/modules/PrestAppCmcicModuleController.php';
include_once 'controllers/modules/PrestAppPayzenModuleController.php';
include_once 'controllers/modules/PrestAppSystempayModuleController.php';
include_once 'controllers/modules/PrestAppEpaymentModuleController.php';
include_once 'controllers/modules/PrestAppPayboxModuleController.php';
include_once 'controllers/modules/PrestAppPayplugModuleController.php';
include_once 'controllers/modules/PrestAppSogecommerceModuleController.php';
include_once 'controllers/modules/PrestAppPaypalModuleController.php';
include_once 'controllers/modules/PrestAppSoflexibiliteModuleController.php';
include_once 'controllers/modules/PrestAppChronopostModuleController.php';
include_once 'controllers/modules/PrestAppSymplModuleController.php';
include_once 'controllers/modules/PrestAppBbcarrierModuleController.php';
include_once 'controllers/modules/PrestAppPaylineModuleController.php';
include_once 'controllers/modules/PrestAppEcrelaypointModuleController.php';
include_once 'controllers/modules/PrestAppColissimoModuleController.php';
include_once 'controllers/modules/PrestAppAlipayModuleController.php';
include_once 'controllers/modules/PrestAppDhlexpressModuleController.php';

//customization
include_once 'controllers/customization/PrestAppAdvancedPackCustomizationController.php';
include_once 'controllers/customization/PrestAppJcAccountGroupCustomizationController.php';
include_once 'controllers/customization/PrestAppAllInOneRewardsCustomizationController.php';
include_once 'controllers/customization/PrestAppDoofinderCustomizationController.php';
include_once 'controllers/customization/PrestAppPrivatesalesCustomizationController.php';
include_once 'controllers/customization/PrestAppBlocklayeredCustomizationController.php';
include_once 'controllers/customization/PrestAppFacebookpsconnectCustomizationController.php';
include_once 'controllers/customization/PrestAppAgepopupCustomizationController.php';
include_once 'controllers/customization/PrestAppPrestadriveCustomizationController.php';
include_once 'controllers/customization/PrestAppFidepiCustomizationController.php';
include_once 'controllers/customization/PrestAppPasswordsecurityCustomizationController.php';
include_once 'controllers/customization/PrestAppOnlinechatCustomizationController.php';
include_once 'controllers/customization/PrestAppMinpurchaseCustomizationController.php';
include_once 'controllers/customization/PrestAppNdksteppingpackCustomizationController.php';

//api middlewares
include_once 'middlewares/token.php';
include_once 'middlewares/authentication.php';

//slim configuration
$config = [
    'settings' => [
        'displayErrorDetails' => true,
        'determineRouteBeforeAppMiddleware' => true,
    ],
];

//inscance of slim framework
$app = new \Slim\App($config);

//call the global token Middleware to securise every routes with the token
$app->add(new TokenMiddleware());
$app->add(new AuthenticationMiddleware());

//index route providing informations
$app->get('/', function (Request $request, Response $response) {
    //get instance of the prestapp module to get the version
    $prestapp = Module::getInstanceByName('prestapp');

    //get context
    $context = $request->getAttribute('context');

    $data = array(
        'version' => $prestapp->version,
        'shop_name' => $context->shop->name,
    );

    if ($context->customer) {
        $data['customer_id'] = (int) $context->customer->id;
    }

    $response = sendOk($response, $data);

    return $response;
});

//route providing informations
$app->get('/phpinfo', function (Request $request, Response $response) {
    phpinfo();
});

//routes
include_once 'routes/authentication.php';
include_once 'routes/cart.php';
include_once 'routes/category.php';
include_once 'routes/product.php';
include_once 'routes/image.php';
include_once 'routes/address.php';
include_once 'routes/shipping.php';
include_once 'routes/payment.php';
include_once 'routes/language.php';
include_once 'routes/currency.php';
include_once 'routes/order.php';
include_once 'routes/contact.php';
include_once 'routes/credit.php';
include_once 'routes/coupon.php';
include_once 'routes/customer.php';
include_once 'routes/search.php';
include_once 'routes/configuration.php';
include_once 'routes/cms.php';
include_once 'routes/custom.php';
include_once 'routes/seo.php';

include_once 'routes/modules/soliberte.php';
include_once 'routes/modules/mondialrelay.php';
include_once 'routes/modules/bankwire.php';
include_once 'routes/modules/cheque.php';
include_once 'routes/modules/ps_wirepayment.php';
include_once 'routes/modules/ps_checkpayment.php';
include_once 'routes/modules/paypal.php';
include_once 'routes/modules/cmcicpaiement.php';
include_once 'routes/modules/payzen.php';
include_once 'routes/modules/systempay.php';
include_once 'routes/modules/epayment.php';
include_once 'routes/modules/paybox.php';
include_once 'routes/modules/payplug.php';
include_once 'routes/modules/sogecommerce.php';
include_once 'routes/modules/soflexibilite.php';
include_once 'routes/modules/chronopost.php';
include_once 'routes/modules/sympl.php';
include_once 'routes/modules/bbcarrier.php';
include_once 'routes/modules/payline.php';
include_once 'routes/modules/ecrelaypoint.php';
include_once 'routes/modules/colissimo.php';
include_once 'routes/modules/alipay.php';
include_once 'routes/modules/dhlexpress.php';

include_once 'routes/customization/custom-modules.php';
include_once 'routes/customization/allinonerewards.php';
include_once 'routes/customization/doofinder.php';
include_once 'routes/customization/privatesales.php';
include_once 'routes/customization/facebookpsconnect.php';
include_once 'routes/customization/blocklayered.php';
include_once 'routes/customization/prestadrive.php';
include_once 'routes/customization/onlinechat.php';
include_once 'routes/customization/ndksteppingpack.php';

$app->add(function ($req, $res, $next) {
    error_reporting(0);
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Max-Age', '86400')
        ->withHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, lang, currency, email, password, order, column, cart, shop, type')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->run();

function fatal_error_handler($buffer)
{
    $error = error_get_last();
    if ($error['type'] == 1) {
        // type, message, file, line
        $newBuffer = '<html><header><title>Fatal Error </title></header>
                        <style>
                            .error_content{
                                background: ghostwhite;
                                vertical-align: middle;
                                margin:0 auto;
                                padding:10px;
                                width:50%;
                            }
                            .error_content label{color: red;font-family: Georgia;font-size: 16pt;font-style: italic;}
                            .error_content ul li{ background: none repeat scroll 0 0 FloralWhite;
                                        border: 1px solid AliceBlue;
                                        display: block;
                                        font-family: monospace;
                                        padding: 2%;
                                        text-align: left;
                            }
                            </style>
                        <body style="text-align: center;">
                            <div class="error_content">
                                <label >Fatal Error </label>
                                <ul>
                                    <li><b>Line</b> ' . $error['line'] . '</li>
                                    <li><b>Message</b> ' . $error['message'] . '</li>
                                    <li><b>File</b> ' . $error['file'] . '</li>
                                </ul>
                            </div>
                        </body></html>';

        return $newBuffer;
    }

    return $buffer;
}
