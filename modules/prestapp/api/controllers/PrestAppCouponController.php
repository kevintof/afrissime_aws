<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppCouponController
{
    public static function getCoupons($context)
    {
        $cart_rules = CartRule::getCustomerCartRules($context->language->id, $context->customer->id, true, false, true);

        $nb_cart_rules = count($cart_rules);

        foreach ($cart_rules as $key => &$discount) {
            if ($discount['quantity_for_user'] === 0) {
                unset($cart_rules[$key]);
            }

            $discount['value'] = Tools::convertPriceFull(
                $discount['value'],
                new Currency((int) $discount['reduction_currency']),
                new Currency((int) $context->cart->id_currency)
            );

            if ((int) $discount['gift_product'] !== 0) {
                $product = new Product((int) $discount['gift_product'], false, (int) $context->language->id);
                if (!Validate::isLoadedObject($product) || !$product->isAssociatedToShop() || !$product->active) {
                    unset($cart_rules[$key]);
                }
                if (Combination::isFeatureActive() && $discount['gift_product_attribute'] != 0) {
                    $attributes = $product->getAttributeCombinationsById((int) $discount['gift_product_attribute'], (int) $context->language->id);
                    $giftAttributes = array();
                    foreach ($attributes as $attribute) {
                        $giftAttributes[] = $attribute['group_name'] . ' : ' . $attribute['attribute_name'];
                    }
                    $discount['gift_product_attributes'] = implode(', ', $giftAttributes);

                    $image = $product->getCombinationImageById($discount['gift_product_attribute'], $context->language->id);
                    if ($image) {
                        $link = new Link();
                        $imageTypes = ImageType::getImagesTypes();
                        $discount['gift_product_image'] = $link->getImageLink($product->link_rewrite, $image['id_image'], $imageType['name']);
                    }
                } else {
                    $images = $product->getImages($context->language->id);
                    foreach ($images as $image) {
                        if ((int) $image['cover']) {
                            $img->is_default = true;
                            $link = new Link();
                            $imageTypes = ImageType::getImagesTypes();
                            $discount['gift_product_image'] = $link->getImageLink($product->link_rewrite, $image['id_image'], $imageType['name']);
                        }
                    }
                }
                $discount['gift_product_name'] = $product->name;
                $discount['gift_product_link'] = $context->link->getProductLink(
                    $product,
                    $product->link_rewrite,
                    $product->category,
                    $product->ean13,
                    $context->language->id,
                    $context->shop->id,
                    $discount['gift_product_attribute'],
                    false,
                    false,
                    true
                );
            }
        }

        return array(
            'nb_cart_rules' => (int) $nb_cart_rules,
            'cart_rules' => $cart_rules,
        );
    }
}
