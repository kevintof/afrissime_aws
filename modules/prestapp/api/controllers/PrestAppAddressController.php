<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppAddressController
{
    /**
     * getCustomerAddresses - Return the addresses list for a customer.
     *
     * @param int     $id_cart
     * @param Context $context
     *
     * @return array Address
     */
    public static function getCustomerAddresses($context)
    {
        if (Validate::isLoadedObject($context->customer)) {
            return $context->customer->getAddresses((int) $context->language->id);
        } else {
            return 'NOT_CONNECTED';
        }
    }

    /**
     * getAddress - Return the address from its id.
     *
     * @param int     $id_address
     * @param Context $context
     *
     * @return Address
     */
    public static function getAddress($id_address, $context)
    {
        $address = new Address($id_address);
        $errors = array();

        //if it's an update, check if id provided is correct
        if (($id_address && !Validate::isLoadedObject($address))) {
            $errors['address'] = 'Address not found';

            return $errors;
        }

        //check is user is authenticated
        if (!Validate::isLoadedObject($context->customer)) {
            $errors['customer'] = 'Customer is not authenticated';

            return $errors;
        }

        //check if user has permissions
        if ($id_address && ((int) $address->id_customer !== (int) $context->customer->id)) {
            $errors['customer'] = 'Customer is unauthorized to get this address which is not belongs to him';

            return $errors;
        } else {
            $address->id_customer = (int) $context->customer->id;
        }

        return $address;
    }

    /**
     * getCustomerDefaultAddress - Return the default customer address.
     *
     * @param Context $context
     *
     * @return Address
     */
    public static function getCustomerDefaultAddress($context)
    {
        if (Validate::isLoadedObject($context->customer)) {
            $first_address = Address::getFirstCustomerAddressId((int) $context->customer->id);

            return new Address($first_address);
        } else {
            return 'NOT_CONNECTED';
        }
    }

    /**
     * createOrUpdateAddress - Create a new address or update it if exists for the customer.
     *
     * @param Context $context
     *
     * @return Adress
     */
    public static function createOrUpdateAddress($lastname, $firstname, $company, $address1, $address2, $city, $postcode, $phone, $mobile, $id_country, $id_state, $other, $alias, $dni, $vat_number, $context, $id_address = null)
    {
        $address = new Address($id_address);

        $errors = array();

        //if it's an update, check if id provided is correct
        if (($id_address && !Validate::isLoadedObject($address))) {
            $errors['address'] = 'Address not found';

            return $errors;
        }

        //check is user is authenticated
        if (!Validate::isLoadedObject($context->customer)) {
            $errors['customer'] = 'Customer is not anthenticated';

            return $errors;
        }

        //check if user has permissions
        if ($id_address && ((int) $address->id_customer !== (int) $context->customer->id)) {
            $errors['customer'] = 'Customer is unauthorized to update this address which is not belongs to him';

            return $errors;
        } else {
            $address->id_customer = (int) $context->customer->id;
        }

        if (!$firstname) {
            $errors['firstname'] = 'The field firstname is required';
        } else {
            $address->firstname = $firstname;
        }

        if (!$lastname) {
            $errors['lastname'] = 'The field lastname is required';
        } else {
            $address->lastname = $lastname;
        }

        if (!$address1) {
            $errors['address1'] = 'The field address1 is required';
        } else {
            $address->address1 = $address1;
        }

        if (!$city) {
            $errors['city'] = 'The field city is required';
        } else {
            $address->city = $city;
        }

        // Check phone
        if (Configuration::get('PS_ONE_PHONE_AT_LEAST') && !$phone && !$mobile) {
            $errors['phone'] = 'You need to provide at least 1 phone number';
        } else {
            $address->phone = $phone;
            $address->phone_mobile = $mobile;
        }

        // Check country
        if (!($country = new Country($id_country)) || !Validate::isLoadedObject($country)) {
            $errors['country'] = 'Country cannot be found';
        } else {
            $address->id_country = $country->id;
        }

        if ((int) $country->contains_states && !(int) $id_state) {
            $errors['state'] = 'State is required for this country';
        } else {
            $address->id_state = (int) $id_state;
        }

        if (!$country->active) {
            $errors['country'] = 'Country is not active';
        }

        /* Check zip code format */
        if ($country->zip_code_format && !$country->checkZipCode($postcode)) {
            $errors['postcode'] = 'Postcode is invalid';
        } elseif (empty($postcode) && $country->need_zip_code) {
            $errors['postcode'] = 'Postcode is required';
        } elseif ($postcode && !Validate::isPostCode($postcode)) {
            $errors['postcode'] = 'Postcode is invalid';
        } else {
            $address->postcode = $postcode;
        }

        // Check country DNI
        if ($country->isNeedDni() && (!$dni || !Validate::isDniLite($dni))) {
            $errors['dni'] = 'Dni is required';
        } elseif (!$country->isNeedDni()) {
            $address->dni = null;
        } else {
            $address->dni = $dni;
        }

        if (Address::aliasExist($alias, $id_address, (int) $context->customer->id)) {
            $errors['alias'] = 'This alias is already used';
        } elseif (!$alias) {
            $errors['alias'] = 'The filed alias is required';
        } else {
            $address->alias = $alias;
        }

        $errors = array_merge($errors, $address->validateFieldsRequiredDatabase());

        if (count($errors) > 0) {
            return $errors;
        }

        if ($address2) {
            $address->address2 = $address2;
        }

        if ($company) {
            $address->company = $company;
        }

        if ($other) {
            $address->other = $other;
        }

        if ($vat_number) {
            $address->vat_number = $vat_number;
        }

        $address_old = new Address($address->id);
        $address->id = null;
        if (Validate::isLoadedObject($address_old)) {
            if (Customer::customerHasAddress($context->customer->id, (int) $address_old->id)) {
                if ($address_old->isUsed()) {
                    $address_old->delete();
                } else {
                    $address->id = (int) $address_old->id;
                    $address->date_add = $address_old->date_add;
                }
            }
        } else {
            $address_old = null;
        }

        // Save address
        if ($result = $address->save()) {

            if (isset($address_old) && $address_old->isUsed()) {
                $context->cart->updateAddressId($address_old->id, $address->id);
            } else {
                $context->cart->autosetProductAddress();
            }

            $context->cart->update();

            return $address;
        }

        $errors['error'] = 'Cannot create address';

        return $errors;
    }

    public static function deleteAddress($id_address, $context)
    {
        $address = new Address($id_address);

        $errors = array();

        //if it's an update, check if id provided is correct
        if (($id_address && !Validate::isLoadedObject($address))) {
            $errors['address'] = 'Address not found';

            return $errors;
        }

        //check is user is authenticated
        if (!Validate::isLoadedObject($context->customer)) {
            $errors['customer'] = 'Customer is not anthenticated';

            return $errors;
        }

        //check if user has permissions
        if ($id_address && ((int) $address->id_customer !== (int) $context->customer->id)) {
            $errors['customer'] = 'Customer is unauthorized to delete this address which is not belongs to him';

            return $errors;
        } else {
            if (count($errors) > 0) {
                return $errors;
            } elseif ($address->delete()) {
                return true;
            }
        }

        return false;
    }

    /**
     * getCountries - Get available countries available to register.
     *
     * @param Context $context
     *
     * @return array Country
     */
    public static function getCountries($context)
    {
        // Generate countries list
        if (Configuration::get('PS_RESTRICT_DELIVERED_COUNTRIES')) {
            $countries = Carrier::getDeliveredCountries((int) $context->language->id, true, true);
        } else {
            $countries = Country::getCountries((int) $context->language->id, true);
        }

        return $countries;
    }

    //fix for ps < 1.6.0.10 Tools::getCountry(); doesnt exists
    public static function getCountry()
    {
        $id_country = (int) Tools::getValue('id_country');
        if (!$id_country && isset($address, $address->id_country) && $address->id_country) {
            $id_country = (int) $address->id_country;
        } elseif (Configuration::get('PS_DETECT_COUNTRY') && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            preg_match('#(?<=-)\w\w|\w\w(?!-)#', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $array);
            if (is_array($array) && isset($array[0]) && Validate::isLanguageIsoCode($array[0])) {
                $id_country = (int) Country::getByIso($array[0], true);
            }
        }
        if (!isset($id_country) || !$id_country) {
            $id_country = (int) Configuration::get('PS_COUNTRY_DEFAULT');
        }
        return (int) $id_country;
    }
}
