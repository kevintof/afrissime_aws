<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'sogecommerce/sogecommerce.php')) {
    require_once _PS_MODULE_DIR_ . 'sogecommerce/sogecommerce.php';

    class PrestAppSogecommerceModuleController
    {
        protected $sogecommerce;

        public function __construct()
        {
            $this->sogecommerce = Module::getInstanceByName('sogecommerce');
        }

        public function displayPayment($context)
        {
            $cart = $context->cart;
            $html = '';
            $activatedPayment = array();

            $standard = new SogecommerceStandardPayment();
            if ($standard->isAvailable($cart)) {
                $activatedPayment['standard'] = array();
                $request = $standard->prepareRequest($cart);
                $activatedPayment['standard']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['standard']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['standard']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $standard->getLogo();
                $activatedPayment['standard']['sogecommerce_title'] = $request->get('order_info');
            }

            $multi = new SogecommerceMultiPayment();
            if ($multi->isAvailable($cart)) {
                $activatedPayment['multi'] = array();
                $request = $multi->prepareRequest($cart);
                $activatedPayment['multi']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['multi']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['multi']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $multi->getLogo();
                $activatedPayment['multi']['sogecommerce_title'] = $request->get('order_info');
            }

            $oney = new SogecommerceOneyPayment();
            if ($oney->isAvailable($cart)) {
                $activatedPayment['oney'] = array();
                $request = $oney->prepareRequest($cart);
                $activatedPayment['oney']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['oney']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['oney']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $oney->getLogo();
                $activatedPayment['oney']['sogecommerce_title'] = $request->get('order_info');
            }

            $fullcb = new SogecommerceFullcbPayment();
            if ($fullcb->isAvailable($cart)) {
                $activatedPayment['fullcb'] = array();
                $request = $fullcb->prepareRequest($cart);
                $activatedPayment['fullcb']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['fullcb']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['fullcb']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $fullcb->getLogo();
                $activatedPayment['fullcb']['sogecommerce_title'] = $request->get('order_info');
            }

            $ancv = new SogecommerceAncvPayment();
            if ($ancv->isAvailable($cart)) {
                $activatedPayment['ancv'] = array();
                $request = $fullcb->prepareRequest($cart);
                $activatedPayment['ancv']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['ancv']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['ancv']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $ancv->getLogo();
                $activatedPayment['ancv']['sogecommerce_title'] = $request->get('order_info');
            }

            $sepa = new SogecommerceSepaPayment();
            if ($sepa->isAvailable($cart)) {
                $activatedPayment['sepa'] = array();
                $request = $sepa->prepareRequest($cart);
                $activatedPayment['sepa']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['sepa']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['sepa']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $sepa->getLogo();
                $activatedPayment['sepa']['sogecommerce_title'] = $request->get('order_info');
            }

            $sofort = new SogecommerceSofortPayment();
            if ($sofort->isAvailable($cart)) {
                $activatedPayment['sofort'] = array();
                $request = $sofort->prepareRequest($cart);
                $activatedPayment['sofort']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['sofort']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['sofort']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $sofort->getLogo();
                $activatedPayment['sofort']['sogecommerce_title'] = $request->get('order_info');
            }

            $paypal = new SogecommercePaypalPayment();
            if ($paypal->isAvailable($cart)) {
                $activatedPayment['paypal'] = array();
                $request = $paypal->prepareRequest($cart);
                $activatedPayment['paypal']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['paypal']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['paypal']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $paypal->getLogo();
                $activatedPayment['paypal']['sogecommerce_title'] = $request->get('order_info');
            }

            $choozeo = new SogecommerceChoozeoPayment();
            if ($choozeo->isAvailable($cart)) {
                $activatedPayment['choozeo'] = array();
                $request = $choozeo->prepareRequest($cart);
                $activatedPayment['choozeo']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['choozeo']['sogecommerce_url'] = $request->get('platform_url');
                $activatedPayment['choozeo']['sogecommerce_logo'] = _MODULE_DIR_ . 'sogecommerce/views/img/' . $choozeo->getLogo();
                $activatedPayment['choozeo']['sogecommerce_title'] = $request->get('order_info');
            }

            // grouped others needs additionnal configuration
            // check sogecommerce.php in the dedicated module around line 1000

            // $other = new SogecommerceOtherPayment();
            // if ($other->isAvailable($cart)) {
            //     $activatedPayment['other'] = array();
            //     $request = $other->prepareRequest($cart);
            //     $activatedPayment['other']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false /* data escape will be done in redirect template */);;
            //     $activatedPayment['other']['sogecommerce_url'] = $request->get('platform_url');
            //     $activatedPayment['other']['sogecommerce_logo'] = _MODULE_DIR_.'sogecommerce/views/img/'.$other->getLogo();
            //     $activatedPayment['other']['sogecommerce_title'] = $request->get('order_info');
            // }

            // $grouped_other = new SogecommerceGroupedOtherPayment();
            // if ($grouped_other->isAvailable($cart)) {
            //     $activatedPayment['grouped_other'] = array();
            //     $request = $grouped_other->prepareRequest($cart);
            //     $activatedPayment['grouped_other']['sogecommerce_params'] = $request->getRequestFieldsArray(false, false /* data escape will be done in redirect template */);;
            //     $activatedPayment['grouped_other']['sogecommerce_url'] = $request->get('platform_url');
            //     $activatedPayment['grouped_other']['sogecommerce_logo'] = _MODULE_DIR_.'sogecommerce/views/img/'.$grouped_other->getLogo();
            //     $activatedPayment['grouped_other']['sogecommerce_title'] = $request->get('order_info');
            // }

            return $activatedPayment;
        }
    }
}
