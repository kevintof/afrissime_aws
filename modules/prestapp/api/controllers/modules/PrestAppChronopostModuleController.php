<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . '/chronopost/chronopost.php')) {
    require _PS_MODULE_DIR_ . '/chronopost/chronopost.php';

    class PrestAppChronopostModuleController extends Chronopost
    {
        public function _construct()
        {
            $this->chronopost = Module::getInstanceByName('chronopost');
        }

        public function saveDeliveryPoint()
        {
            header('Content-type: text/plain');
            include_once '../../../config/config.inc.php';

            if (!Tools::getIsset('relaisID')
                || !Tools::getIsset('cartID')) {
                die('Parameter Error');
            }

            $cart = new Cart((int) Tools::getValue('cartID'));

            if ($cart->id_customer != (int) Context::getContext()->customer->id) {
                die('KO');
            }

            Db::getInstance()->execute('INSERT INTO `' . _DB_PREFIX_ . 'chrono_cart_relais` VALUES (
                    ' . (int) Tools::getValue('cartID') . ', "' . pSQL(Tools::getValue('relaisID')) . '") ON DUPLICATE
                    KEY UPDATE id_pr="' . pSQL(Tools::getValue('relaisID')) . '"');

            return 'OK';
        }

        private function _getNextDay($day)
        {
            if ($day == -1) {
                return null;
            }
            $date = new DateTime();

            switch ($day) {
                case 0:
                    $date->modify('next Sunday');
                    break;
                case 1:
                    $date->modify('next Monday');
                    break;
                case 2:
                    $date->modify('next Tuesday');
                    break;
                case 3:
                    $date->modify('next Wednesday');
                    break;
                case 4:
                    $date->modify('next Thursday');
                    break;
                case 5:
                    $date->modify('next Friday');
                    break;
                case 6:
                    $date->modify('next Saturday');
                    break;
            }

            $date->modify('+ ' . Configuration::get('CHRONOPOST_RDV_HOUR_ON') . ' hours ' . Configuration::get('CHRONOPOST_RDV_MINUTE_ON') . ' minutes');

            return $date;
        }

        public function hookExtraCarrier($context)
        {
            $params = $context->cart;

            $address = new Address($params->id_address_delivery);
            $country = new Country($address->id_country);

            $chronorelais_carrier = Carrier::getCarrierByReference(Configuration::get('CHRONOPOST_CHRONORELAIS_ID'));
            $relaiseurope_carrier = Carrier::getCarrierByReference(Configuration::get('CHRONOPOST_RELAISEUROPE_ID'));
            $relaisdom_carrier = Carrier::getCarrierByReference(Configuration::get('CHRONOPOST_RELAISDOM_ID'));
            $rdv_carrier = Carrier::getCarrierByReference(Configuration::get('CHRONOPOST_CHRONORDV_ID'));

            $chronorelais_id = $chronorelais_carrier ? $chronorelais_carrier->id : -1;
            $relaiseurope_id = $relaiseurope_carrier ? $relaiseurope_carrier->id : -1;
            $relaisdom_id = $relaisdom_carrier ? $relaisdom_carrier->id : -1;
            $rdv_id = $rdv_carrier ? $rdv_carrier->id : -1;

            return array(
                'module_uri' => __PS_BASE_URI__ . 'modules/' . $this->name,
                'cust_codePostal' => $address->postcode,
                'cust_firstname' => $address->firstname,
                'cust_lastname' => $address->lastname,
                'cartID' => $params->id,
                'CHRONORELAIS_ID' => $chronorelais_id,
                'CHRONORELAIS_ID_INT' => (string) Cart::intifier($chronorelais_id),
                'RELAISEUROPE_ID' => $relaiseurope_id,
                'RELAISEUROPE_ID_INT' => (string) Cart::intifier($relaiseurope_id),
                'RELAISDOM_ID' => $relaisdom_id,
                'RELAISDOM_ID_INT' => (string) Cart::intifier($relaisdom_id),
                'cust_address' => $address->address1 . ' ' . $address->address2 . ' ' . $address->postcode . ' ' . $address->city,
                'cust_address_clean' => $address->address1 . ' ' . $address->address2 . ' ',
                'cust_city' => $address->city,
                'cust_country' => Country::getIsoById($address->id_country),
                'map_enabled' => Configuration::get('CHRONOPOST_MAP_ENABLED'),
                'map_apikey' => Configuration::get('CHRONOPOST_MAP_APIKEY'),
            );

            // TODO allow for either one to be activated
            // Currently chronordv needs chronorelais's JS, hence ChronoRelais is always included

            // call WS !
            // include_once(_PS_MODULE_DIR_.'chronopost/libraries/CreneauWS.php');
            // $query = new searchDeliverySlot();
            // $query->callerTool = 'RDVPRE';
            // $query->productType = 'RDV'; // normal product
            // $query->recipientZipCode = $address->postcode;
            // $query->shipperAdress1 = Configuration::get('CHRONOPOST_SHIPPER_ADDRESS');
            // $query->shipperAdress2 = Configuration::get('CHRONOPOST_SHIPPER_ADDRESS2');
            // $query->shipperCity = Configuration::get('CHRONOPOST_SHIPPER_CITY');
            // $query->shipperCountry = Configuration::get('CHRONOPOST_SHIPPER_COUNTRY');
            // $query->shipperName = Configuration::get('CHRONOPOST_SHIPPER_NAME');
            // $query->shipperName2 = Configuration::get('CHRONOPOST_SHIPPER_NAME2');
            // $query->shipperZipCode = Configuration::get('CHRONOPOST_SHIPPER_ZIPCODE');
            // $query->recipientAdress1 = $address->address1;
            // $query->recipientAdress2 = $address->address2;
            // $query->recipientCity = $address->city;
            // $query->recipientCountry = 'FR';
            // $query->recipientZipCode = $address->postcode;
            // $query->weight = 1;

            // // Calculate earliest possible shipping date
            // $date = $this->_getNextDay(
            //     Configuration::get('CHRONOPOST_RDV_DAY_ON'),
            //     Configuration::get('CHRONOPOST_RDV_HOUR_ON'),
            //     Configuration::get('CHRONOPOST_RDV_MINUTE_ON')
            // );

            // if ($date == null) {
            //     $date = new DateTime();
            //     $date->modify('+ '.(int)Configuration::get('CHRONOPOST_RDV_DELAY').' days');
            // }

            // $query->dateBegin = $date->format('Y-m-d\TH:i:s');
            // $date->modify('+ 7 days');
            // $query->dateEnd = $date->format('Y-m-d\TH:i:s');

            // // Calculate next closing period
            // $close_start = $this->_getNextDay(
            //     Configuration::get('CHRONOPOST_RDV_DAY_CLOSE_ST'),
            //     Configuration::get('CHRONOPOST_RDV_HOUR_CLOSE_ST'),
            //     Configuration::get('CHRONOPOST_RDV_MINUTE_CLOSE_ST')
            // );
            // $close_end = $this->_getNextDay(
            //     Configuration::get('CHRONOPOST_RDV_DAY_CLOSE_END'),
            //     Configuration::get('CHRONOPOST_RDV_HOUR_CLOSE_END'),
            //     Configuration::get('CHRONOPOST_RDV_MINUTE_CLOSE_END')
            // );

            // if ($close_start != null && $close_end != null && $close_start != $close_end) {
            //     $query->customerDeliverySlotClosed = $close_start->format('Y-m-d\TH:i:s\Z')
            //         .'/'.$close_end->format('Y-m-d\TH:i:s\Z');
            // }

            // $ws = new CreneauWS();

            // $header = array();
            // $header[] = new SoapHeader(
            //     'http://cxf.soap.ws.creneau.chronopost.fr/',
            //     'accountNumber',
            //     Configuration::get('CHRONOPOST_GENERAL_ACCOUNT'),
            //     false
            // );
            // $header[] = new SoapHeader(
            //     'http://cxf.soap.ws.creneau.chronopost.fr/',
            //     'password',
            //     Configuration::get('CHRONOPOST_GENERAL_PASSWORD'),
            //     false
            // );
            // $ws->__setSoapHeaders($header);

            // $res = $ws->searchDeliverySlot($query);

            // if (!$res->return->slotList) {
            //     return $r;
            // }

            // // group by hour then days
            // $ordered_slots = array();
            // $days = array();

            // foreach ($res->return->slotList as $slot) {
            //     if ($slot->startHour < 10) {
            //         $slot->startHour = '0'.$slot->startHour;
            //     }
            //     $hour_idx = $slot->startHour.'H - '.$slot->endHour.'H';
            //     $when = new DateTime($slot->deliveryDate);
            //     $day_idx = $when->format("d/m/Y");
            //     if (!array_key_exists($hour_idx, $ordered_slots)) {
            //         $ordered_slots[$hour_idx] = array();
            //     }
            //     if (!in_array($day_idx, $days)) {
            //         $days[] = $day_idx;
            //     }

            //     $tariffLevel = Tools::substr($slot->tariffLevel, 1);
            //     $price = Chronopost::getRDVCost($params->id, $tariffLevel);

            //     $slot->tariffLevel = $tariffLevel;
            //     $slot->price = Tools::displayPrice($price);
            //     $slot->fee = $price;
            //     $slot->enable = Configuration::get('CHRONOPOST_RDV_STATE'.$tariffLevel);

            //     $ordered_slots[$hour_idx][$day_idx] = $slot;
            // }

            // ksort($ordered_slots);

            // // $this->context->smarty->assign(
            //     return array(
            //         'rdv_ordered_slots' => $ordered_slots,
            //         'rdv_days' => $days,
            //         'rdv_carrierID' => $rdv_id,
            //         'rdv_carrierIntID' => (string)Cart::intifier($rdv_id),
            //         'rdv_transactionID' => (string)$res->return->transactionID
            //     );
            // );

            // return $r.$this->context->smarty->fetch(dirname(__FILE__).'/views/templates/hook/chronordv.tpl');
        }

        public function getPoints()
        {
            //require _PS_BASE_URL_.'config/config.inc.php';
            require _PS_MODULE_DIR_ . '/chronopost/libraries/PointRelaisServiceWSService.php';
            require _PS_MODULE_DIR_ . '/chronopost/async/getPointRelais2.php';
        }
    }
}
