<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'alipay/alipay.php')) {
    require_once _PS_MODULE_DIR_ . 'alipay/alipay.php';

    class PrestAppAlipayModuleController
    {
        protected $alipay;

        public function __construct()
        {
            $this->alipay = Module::getInstanceByName('alipay');
        }

        public function displayPayment($context)
        {

            include_once(_PS_MODULE_DIR_.'alipay/api/loader.php');

            $currency_id = $context->cart->id_currency;
            $currency = new Currency((int)$currency_id);
            $cart = new Cart($context->cart->id);
            if (!ValidateCore::isLoadedObject($cart)) {
                return false;
            }
            if (in_array($currency->iso_code, $this->alipay->limited_currencies) == false) {
                return false;
            }
            $service = Configuration::get('ALIPAY_SERVICE_PAYMENT');
            $credentials = AlipayTools::getCredentials($service, false);

            $alipayapi = new AlipayApi($credentials);
            $alipayapi->setReturnUrl($this->alipay->getReturnUrl($cart->secure_key, $cart->id));
            $alipayapi->setNotifyUrl($this->alipay->getNotifyUrl($cart->secure_key, $cart->id));
            $alipayapi->setCharset('UTF-8');
            date_default_timezone_set('Asia/Hong_Kong');
            $payment_request = new PaymentRequest();
            $payment_request->setCurrency($currency->iso_code);
            $payment_request->setPartnerTransactionId(date('YmdHis').$cart->id);
            $payment_request->setGoodsDescription($this->alipay->getGoodsDescription());
            $payment_request->setGoodsName($this->alipay->getGoodsName($cart->id));
            $payment_request->setOrderGmtCreate(date('Y-m-d H:i:s'));
            $payment_request->setOrderValidTime(21600);
            $payment_request->setTotalFee($cart->getOrderTotal());
            $payment_request->setProductCode('NEW_OVERSEAS_SELLER');
            $alipayapi->prepareRequest($payment_request);
            $url = $alipayapi->createUrl();

            return array(
                'url' => $url
            );
        }
    }
}