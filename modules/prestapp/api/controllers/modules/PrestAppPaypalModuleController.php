<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'paypal/paypal.php')) {

    class PrestAppPaypalModuleController
    {
        protected $paypal;

        public function __construct()
        {
            $this->paypal = Module::getInstanceByName('paypal');
        }

        public function displayPayment(Context $context)
        {
            $cart = $context->cart;

            if (!Validate::isLoadedObject($cart)) {
                return false;
            }

            $amount = $cart->getOrderTotal();

            return array(
                'active' => $this->paypal->active,
                'currencyIso' => $context->currency->iso_code,
                'total' => (float) ($cart->getOrderTotal(true, Cart::BOTH)),
                'shopName' => Configuration::get('PS_SHOP_NAME'),
            );
        }

        public function validateOrder($context, $transaction_id, $total_paid, $currency, $payment_date, $payment_status)
        {
            $cart = $context->cart;

            if (!Validate::isLoadedObject($cart)) {
                return false;
            }

            if (0 === $cart->id_customer or 0 === $cart->id_address_delivery or 0 === $cart->id_address_invoice or !$this->paypal->active) {
                return false;
            }

            $authorized = false;
            foreach (PrestAppCartController::getPaymentModulesList($context) as $module) {
                if ('paypal' === $module['name']) {
                    $authorized = true;
                    break;
                }
            }
            if (!$authorized) {
                return false;
            }

            $customer = new Customer((int) $cart->id_customer);

            if (!Validate::isLoadedObject($customer)) {
                return false;
            }

            $transaction = array();

            if ($transaction_id) {
                $transaction['transaction_id'] = $transaction_id;
                $transaction['id_transaction'] = $transaction_id;
            }

            if ($total_paid) {
                $transaction['total_paid'] = $total_paid;
            }

            if ($currency) {
                $transaction['currency'] = $currency;
            }

            if ($payment_date) {
                $transaction['payment_date'] = $payment_date;
            }

            if ($payment_status) {
                $transaction['payment_status'] = Tools::ucfirst(Tools::strtolower($payment_status));
            }

            $summary = $cart->getSummaryDetails($context->language->id);
            if (isset($summary['total_shipping'])) {
                $transaction['shipping'] = $summary['total_shipping'];
            }

            $currency = $context->currency;
            $total = (float) ($cart->getOrderTotal(true, Cart::BOTH));

            $this->paypal->validateOrder($cart->id, Configuration::get('PS_OS_PAYMENT'), $total, $this->paypal->displayName, null, $transaction, (int) $currency->id, false, $customer->secure_key);

            $order = new Order($this->paypal->currentOrder);

            return $order;
        }

        public function getPaymentUrl($context)
        {

            $cart = $context->cart;

            if (!Validate::isLoadedObject($cart)) {
                return false;
            }

            $currencyIso = $context->currency->iso_code;
            $total = (float) ($cart->getOrderTotal(true, Cart::BOTH));
            $shopName = Configuration::get('PS_SHOP_NAME');

            $access_token = $this->getPaypalAccessToken();

            if (!$access_token) {
                return false;
            }

            $curl = curl_init();

            $token_response = json_decode($response);

            $paypal_url = "https://api.paypal.com/v2/checkout/orders";

            $params = array(
                "intent" => "CAPTURE",
                "purchase_units" => array(
                    array(
                        "amount" => array(
                            "value" => strval($total),
                            "currency_code" => $currencyIso,
                            "description" => $shopName,
                        ),
                    ),
                ),
                "application_context" => array(
                    "user_action" => "PAY_NOW",
                    "return_url" => _PS_BASE_URL_ . "/modules/prestapp/api/modules/payments/paypal/approve",
                    "cancel_url" => _PS_BASE_URL_ . "/modules/prestapp/api/modules/payments/paypal/cancel",
                ),
            );

            $params = json_encode($params);

            curl_setopt_array($curl, array(
                CURLOPT_URL => $paypal_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $params,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "Authorization: Bearer " . $access_token,
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            $parsed_response = json_decode($response);

            $is_error = false;

            if (curl_errno($curl)) {
                $is_error = true;
            }

            if (isset($parsed_response->error)) {
                $is_error = true;
            }

            curl_close($curl);

            if ($is_error) {
                return false;
            } else {
                $order_response = json_decode($response, true);
                $key = array_search("approve", array_column($order_response['links'], 'rel'));

                if ($key) {
                    return $order_response['links'][$key]['href'];
                } else {
                    return false;
                }
            }
        }

        public function approvePayment($context, $token, $payer_id)
        {
            $access_token = $this->getPaypalAccessToken();

            if (!$access_token) {
                return false;
            }

            $curl = curl_init();

            $order_detail_response = json_decode($response);

            $paypal_url = "https://api.paypal.com/v2/checkout/orders/" . $token . "/capture";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $paypal_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "Authorization: Bearer " . $access_token,
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            $response = json_decode($response);

            $is_error = false;

            if (curl_errno($curl)) {
                $is_error = true;
            }

            if (isset($response->error)) {
                $is_error = true;
            }

            curl_close($curl);

            if ($is_error) {
                return false;
            } else {

                $data = array();

                if (count($response->purchase_units > 0)) {
                    if ($response->purchase_units[0]->payments) {
                        if (count($response->purchase_units[0]->payments->captures) > 0) {
                            $data['transaction_id'] = $response->purchase_units[0]->payments->captures[0]->id;
                            $data['total_paid'] = $response->purchase_units[0]->payments->captures[0]->amount->value;
                            $data['currency'] = $response->purchase_units[0]->payments->captures[0]->amount->currency_code;
                            $data['payment_date'] = $response->purchase_units[0]->payments->captures[0]->create_time;
                            $data['payment_status'] = $response->purchase_units[0]->payments->captures[0]->status;
                            return $data;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }

        public function getPaypalAccessToken()
        {
            $curl = curl_init();

            $paypal_cliend_id = Configuration::get('PRESTAPP_PAYPAL_CLIENT_ID');
            $paypal_secret = Configuration::get('PRESTAPP_PAYPAL_SECRET_KEY');

            $paypal_url = 'https://api.paypal.com/v1/oauth2/token';

            curl_setopt_array($curl, array(
                CURLOPT_URL => $paypal_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "grant_type=client_credentials",
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json",
                    "Accept-Language: en_US",
                    "Content-Type: application/x-www-form-urlencoded",
                ),
                CURLOPT_USERPWD => $paypal_cliend_id . ":" . $paypal_secret,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            $response = json_decode($response);

            $is_error = false;

            if (curl_errno($curl)) {
                $is_error = true;
            }

            if (isset($response->error)) {
                $is_error = true;
            }

            curl_close($curl);

            if ($is_error) {
                return false;
            } else {
                return $response->access_token;
            }
        }
    }
}
