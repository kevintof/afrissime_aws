<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . '/mondialrelay/mondialrelay.php')) {
    require _PS_MODULE_DIR_ . '/mondialrelay/mondialrelay.php';

    class PrestAppMondialrelayModuleController extends Mondialrelay
    {
        public function _construct()
        {

        }

        public function displayCarrier($context)
        {
            if (!MondialRelay::isAccountSet()) {
                return false;
            }

            $mondialrelay = new MondialRelay();

            $id_carrier = false;
            $preSelectedRelay = $mondialrelay->getRelayPointSelected($context->cart->id);
            $carriersList = MondialRelay::_getCarriers();
            $address = new Address($context->cart->id_address_delivery);
            $country = new Country($address->id_country);
            $id_zone = Address::getZoneById((int) ($address->id));

            /*  Check if the defined carrier are ok */
            foreach ($carriersList as $k => $row) {
                /*  For now works only with single shipping (>= 1.5 compatibility) */
                if (method_exists($context->cart, 'carrierIsSelected')) {
                    if ($context->cart->carrierIsSelected($row['id_carrier'], (int) ($address->id))) {
                        $id_carrier = $row['id_carrier'];
                    }

                }

                if ($carriersList[$k]['dlv_mode'] != 'LD1' && $carriersList[$k]['dlv_mode'] != 'LDS' && $carriersList[$k]['dlv_mode'] != 'HOM') {
                    $carriersList[$k]['display_points'] = true;
                } else {
                    $carriersList[$k]['display_points'] = false;
                }

                /*  Temporary carrier for some test */
                $carrier = new Carrier((int) ($row['id_carrier']));
                if ((($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT) && $carrier->getMaxDeliveryPriceByWeight($id_zone) === false) || (($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE) && $carrier->getMaxDeliveryPriceByPrice($id_zone) === false)) {
                    unset($carriersList[$k]);
                } else if ($row['range_behavior']) {
                    /*  Get id zone */
                    $id_zone = (isset($context->cart->id_address_delivery) && $context->cart->id_address_delivery) ?
                    Address::getZoneById((int) $context->cart->id_address_delivery) :
                    (int) $context->country->id_zone;

                    if (($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT && (!Carrier::checkDeliveryPriceByWeight($row['id_carrier'], $context->cart->getTotalWeight(), $id_zone))) ||
                        ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE &&
                            (!Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $context->cart->getOrderTotal(true, MondialRelay::BOTH_WITHOUT_SHIPPING), $id_zone, $context->cart->id_currency) ||
                                !Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $context->cart->getOrderTotal(true, MondialRelay::BOTH_WITHOUT_SHIPPING), $id_zone, $context->cart->id_currency)))) {
                        unset($carriersList[$k]);
                    }

                }
            }

            $carrier = null;
            if ($id_carrier && ($method = MondialRelay::getMethodByIdCarrier($id_carrier))) {
                $carrier = new Carrier((int) $id_carrier);
                /*  Add dynamically a new field */
                $carrier->id_mr_method = $method['id_mr_method'];
                $carrier->mr_dlv_mode = $method['dlv_mode'];
            }

            if (Configuration::get('PS_SSL_ENABLED') || (!empty($_SERVER['HTTPS']) && Tools::strtolower($_SERVER['HTTPS']) != 'off')) {
                $ssl = 'true';
            } else {
                $ssl = 'false';
            }

            return array(
                'address' => $address,
                'account_shop' => $this->account_shop,
                'country' => $country,
                'ssl' => $ssl,
                'MR_Data' => array(
                    'carrier_list' => $carriersList,
                    'carrier' => $carrier,
                    'PS_VERSION' => _PS_VERSION_,
                    'pre_selected_relay' => isset($preSelectedRelay['MR_selected_num']) ? $preSelectedRelay['MR_selected_num'] : -1,
                ),
            );
        }

        public function getPoints($context, $id_carrier)
        {
            try {
                /** Get start points only */
                require _PS_MODULE_DIR_ . '/mondialrelay/classes/MRGetRelayPoint.php';
                require _PS_MODULE_DIR_ . '/mondialrelay/classes/MRRelayDetail.php';

                $mondialrelay = new MondialRelay();
                $cart = new Cart($context->cart->id);
                $params = array(
                    'id_address_delivery' => $context->cart->id_address_delivery,
                    'id_carrier' => $id_carrier,
                    'weight' => $cart->getTotalWeight(),
                );
                $rpoints = new MRGetRelayPoint($params, $mondialrelay);
                $rpoints->init();
                $rpoints->send();
                $result = $rpoints->getResult();

                return $result;
            } catch (Exception $e) {
                return false;
            }
        }

        public function saveDeliveryPoint($context, $id_carrier, $relay_point, $carrier_method)
        {
            try {
                require _PS_MODULE_DIR_ . '/mondialrelay/classes/MRManagement.php';

                $mondialrelay = new MondialRelay();

                $cart = new Cart($context->cart->id);
                $customer = new Customer($context->customer->id);

                $params = array(
                    'relayPointInfo' => json_decode($relay_point, true),
                    'id_carrier' => $id_carrier,
                    'id_mr_method' => $carrier_method,
                    'id_cart' => $cart->id,
                    'id_customer' => $customer->id,
                );

                $MRManagement = new MRManagement($params, $mondialrelay);
                $MRManagement->addSelectedCarrierToDB();
                return true;
            } catch (Exception $e) {
                return false;
            }
        }

        public function getTrackingInformations($id_carrier, $id_cart)
        {
            if (!Mondialrelay::isMondialRelayCarrier($id_carrier)) {
                return false;
            } else {
                $sql = '
                    SELECT s.`MR_Selected_LgAdr1`, s.`MR_Selected_LgAdr2`, s.`MR_Selected_LgAdr3`, s.`MR_Selected_LgAdr4`,
                    s.`MR_Selected_CP`, s.`MR_Selected_Ville`, s.`MR_Selected_Pays`, s.`MR_Selected_Num`, s.`url_suivi`, s.`exp_number`, m.dlv_mode
                    FROM `' . _DB_PREFIX_ . 'mr_selected` s
                    INNER JOIN  ' . _DB_PREFIX_ . 'mr_method m ON m.id_mr_method = s.id_method
                    WHERE s.`id_cart` = ' . (int) $id_cart;
                $res = Db::getInstance()->getRow($sql);

                if (!$res) {
                    return false;
                } else {
                    return array(
                        'tracking_id' => $res['exp_number'],
                        'tracking_url' => $res['url_suivi'],
                    );
                }
            }
        }
    }
}
