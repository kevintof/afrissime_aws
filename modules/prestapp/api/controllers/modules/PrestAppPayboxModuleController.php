<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'paybox/paybox.php')) {
    require_once _PS_MODULE_DIR_ . 'paybox/paybox.php';

    class PrestAppPayboxModuleController
    {
        protected $paybox;

        public function __construct()
        {
            $this->paybox = Module::getInstanceByName('paybox');
        }

        public function displayPayment($context)
        {
            $cart = $context->cart;

            if (!$this->paybox->active) {
	            return false;
	        }

	        if (!$this->paybox->checkCurrency($cart)) {
	            return false;
            }

            $this->paybox->loadAsset(true);

            $this->paybox->setPaymentValues();

            $payment_options = array();
            
            // 1.7 Update - One Payment
	        if ((int)$this->paybox->pbx->configs['PBX_FULL_INTEGRATION'] === 0 && (int)$this->paybox->pbx->configs['PBX_SIMPLE_ADVANCED'] === 1) {
                if ($this->paybox->pbx->configs['PBX_MULTIPLE_1X_PAYMENTS'] == 1 && $this->paybox->pbx_total >= ($this->paybox->pbx->configs['PBX_1X_MINIMUM'] * 100)) {
                    $payment_options[] = $this->paybox->paymentOne($this->paybox->pbx->configs, $this->paybox->pbx->account, $this->paybox->pbx_total, $this->paybox->pbx_devise, $this->paybox->cart->id, $this->paybox->pbx_porteur, $this->paybox->essential->retour, $this->paybox->date, $this->paybox->essential->lang, $this->paybox->essential->urls);
                }
            }
            
            /* Direct payment */
            if ((int)$this->paybox->pbx->configs['PBX_FULL_INTEGRATION'] === 1) {
                $payment_options[] = $this->getEmbeddedPaymentOption();
            }


            $paybox_wallet = new PayboxWallet($this->paybox->customer->id);

            if ((int)$this->paybox->pbx->configs['PBX_SOLUTION'] === 2 && (int)$this->paybox->pbx->configs['PBX_WALLET'] === 1 && (int)$this->paybox->pbx->configs['PBX_FULL_INTEGRATION'] === 1 && $paybox_wallet->parameters['id_paybox_wallet'] != 0) {
                $payment_options[] = $this->paybox->getEmbeddedPaymentOptionWallet();
            }

            /* 1.7 Update
            /* Contracts - Show specific card types */
            if ((int)$this->paybox->pbx->configs['PBX_FULL_INTEGRATION'] === 0 && (int)$this->paybox->pbx->configs['PBX_SIMPLE_ADVANCED'] === 0)
            {
                $results = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'paybox_contracts` WHERE active = 1');
                foreach ($results as $contract) {
                    $payment_options[] = $this->paybox->paymentContracts($this->paybox->pbx->configs, $this->paybox->pbx->account, $this->paybox->pbx_total, $this->paybox->pbx_devise, $this->paybox->cart->id, $this->paybox->pbx_porteur, $this->paybox->essential->retour, $this->paybox->date, $this->paybox->essential->lang, $this->paybox->essential->urls, $contract);
                }
            }

            /* Payments NX times */
			if ($this->paybox->pbx->configs['PBX_MULTIPLE_2X_PAYMENTS'] == 1 && $this->paybox->pbx_total >= ($this->paybox->pbx->configs['PBX_2X_MINIMUM'] * 100)) {
                $payment_options[] = $this->paybox->paymentTwo($this->paybox->pbx->configs, $this->paybox->pbx->account, $this->paybox->pbx_total, $this->paybox->pbx_devise, $this->paybox->cart->id, $this->paybox->pbx_porteur, $this->paybox->essential->retour, $this->paybox->date, $this->paybox->essential->lang, $this->paybox->essential->urls);
            }
            if ($this->paybox->pbx->configs['PBX_MULTIPLE_3X_PAYMENTS'] == 1 && $this->paybox->pbx_total >= ($this->paybox->pbx->configs['PBX_3X_MINIMUM'] * 100)) {
                $payment_options[] = $this->paybox->paymentThree($this->paybox->pbx->configs, $this->paybox->pbx->account, $this->paybox->pbx_total, $this->paybox->pbx_devise, $this->paybox->cart->id, $this->paybox->pbx_porteur, $this->paybox->essential->retour, $this->paybox->date, $this->paybox->essential->lang, $this->paybox->essential->urls);
            }

            if ($this->paybox->pbx->configs['PBX_MULTIPLE_4X_PAYMENTS'] == 1 && $this->paybox->pbx_total >= ($this->paybox->pbx->configs['PBX_4X_MINIMUM'] * 100)) {
                $payment_options[] = $this->paybox->paymentFour($this->paybox->pbx->configs, $this->paybox->pbx->account, $this->paybox->pbx_total, $this->paybox->pbx_devise, $this->paybox->cart->id, $this->paybox->pbx_porteur, $this->paybox->essential->retour, $this->paybox->date, $this->paybox->essential->lang, $this->paybox->essential->urls);
            }
            
            if ($this->paybox->pbx->configs['PBX_ENABLE_SUBSCRIPTIONS'] == 1) {
                $payment_options[] = $this->paybox->paymentSubscription($this->paybox->pbx->configs, $this->paybox->pbx->account, $this->paybox->pbx_total, $this->paybox->pbx_devise, $this->paybox->cart->id, $this->paybox->pbx_porteur, $this->paybox->essential->retour, $this->paybox->date, $this->paybox->essential->lang, $this->paybox->essential->urls);
            }

            $finals = [];

            foreach($payment_options as $option) {
                $payment;
                $payment['logo'] = $option->getLogo();
                $payment['text'] = $option->getCallToActionText();
                $payment['additionalInformation'] = $option->getAdditionalInformation();
                $payment['action'] = $option->getAction();
                $payment['is_active'] = true;
                $payment['is_url'] = true;
                $payment['url_present'] = true;

                $payment['inputs'] = array();

                foreach($option->getInputs() as $input) {
                    $payment['inputs'][$input['name']] = strval($input['value']);
                }
				
				$gateway = $this->getGateway();
				$demo_mode = (int)Configuration::get('PBX_DEMO_MODE');
				
				if ((int)$demo_mode === 0) {
					$payment['action'] = 'https://preprod-tpeweb.'.$gateway.'/cgi/ChoixPaiementMobile.cgi';
				} else {
					$serveurs = array('tpeweb.'.$gateway, //serveur primaire
									'tpeweb1.'.$gateway); //serveur secondaire
					
					$serveur_ok = '';
					foreach($serveurs as $serveur)
					{
						$doc = new DOMDocument();
						$doc->loadHTMLFile('https://'.$serveur.'/load.html');
						$server_status = '';
						$element = $doc->getElementById('server_status');
						if($element) {
							$server_status = $element->textContent;
						}
						if($server_status == 'OK')
						{
							$serveur_ok = $serveur;
							break;
						}
					}
					if(!$serveur_ok) {
						$payment['action'] = 'Error with Paybox server';
					}

					$payment['action'] = 'https://'.$serveur_ok.'/cgi/ChoixPaiementMobile.cgi';
				}

                array_push($finals, $payment);
            }
            
            return $finals;
        }
		
		public function getGateway()
		{
			$gateway = Configuration::get('PBX_GATEWAY');
			if ($gateway === 'paybox')
				return "paybox.com";
			return "e-transactions.fr";
		}
    }
}