<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppChequeModuleController
{
    protected $cheque;

    public function __construct()
    {
        $this->cheque = Module::getInstanceByName('cheque');
    }

    public function displayPayment()
    {
        return array(
            'name' => $this->cheque->chequeName,
            'address' => $this->cheque->address,
        );
    }

    public function validateOrder($context)
    {
        $cart = $context->cart;

        if (!Validate::isLoadedObject($cart)) {
            return false;
        }

        if (0 === $cart->id_customer || 0 === $cart->id_address_delivery || 0 === $cart->id_address_invoice || !$this->cheque->active) {
            return false;
        }

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (PrestAppCartController::getPaymentModulesList($context) as $module) {
            if ('cheque' === $module['name']) {
                $authorized = true;
                break;
            }
        }
        if (!$authorized) {
            return false;
        }

        $customer = new Customer((int) $cart->id_customer);

        if (!Validate::isLoadedObject($customer)) {
            return false;
        }

        $currency = $context->currency;
        $total = (float) ($cart->getOrderTotal(true, Cart::BOTH));

        $this->cheque->validateOrder($cart->id, Configuration::get('PS_OS_CHEQUE'), $total, $this->cheque->displayName, null, array(), (int) $currency->id, false, $customer->secure_key);

        $order = new Order($this->cheque->currentOrder);

        return $order;
    }
}
