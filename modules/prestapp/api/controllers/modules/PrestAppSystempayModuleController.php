<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'systempay/systempay.php')) {
    require_once _PS_MODULE_DIR_ . 'systempay/systempay.php';
    require_once _PS_MODULE_DIR_ . 'systempay/classes/SystempayApi.php';

    class PrestAppSystempayModuleController
    {
        protected $systempay;

        public function __construct()
        {
            $this->systempay = Module::getInstanceByName('systempay');
        }

        public function displayPayment($context)
        {

            if (!$this->systempay->active) {
                return false;
            }

            // currency support
            if (!$this->checkCurrency($context)) {
                return false;
            }

            $cart = $context->cart;
            $html = '';
            $activatedPayment = array();

            $standard = new SystempayStandardPayment();
            if ($standard->isAvailable($cart)) {
                // $standardObject = array();
                // $request = $standard->prepareRequest($cart);
                // $standardObject['systempay_params'] = $request->getRequestFieldsArray(false, false);
                // $standardObject['systempay_url'] = $request->get('platform_url');
                // $standardObject['systempay_logo'] = _PS_BASE_URL_ . _MODULE_DIR_.'systempay/views/img/'.$standard->getLogo();
                // $standardObject['systempay_title'] = $request->get('order_info');

                // array_push($activatedPayment, $standardObject);

                $vars = $standard->getTplVars($cart);
                $standardObject = array();
                $standardObject['systempay_logo'] = _PS_BASE_URL_ . $vars['systempay_logo'];
                $standardObject['systempay_title'] = $vars['systempay_title'];
                $standardObject['systempay_params_link'] = 'standard';
                array_push($activatedPayment, $standardObject);
            }

            $multi = new SystempayMultiPayment();
            if ($multi->isAvailable($cart)) {
                $vars = $multi->getTplVars($cart);
                $multiObject = array();
                $multiObject['systempay_logo'] = _PS_BASE_URL_ . $vars['systempay_logo'];
                $multiObject['systempay_title'] = $vars['systempay_title'];
                $multiObject['systempay_params_link'] = 'multi';
                $multiObject['systempay_multi_options'] = array();
                $multiObject['systempay_avail_cards'] = array();

                $multiObject['systempay_multi_options']['param'] = 'systempay_opt';
                $multiObject['systempay_multi_options']['text'] = $this->systempay->l('Choose your payment option');
                $multiObject['systempay_multi_options']['values'] = array();
                foreach ($vars['systempay_multi_options'] as $key => $value) {
                    array_push($multiObject['systempay_multi_options']['values'], array(
                        "opt_id" => $key,
                        "opt_value" => $value['localized_label'],
                    ));
                }

                $multiObject['systempay_avail_cards']['param'] = 'systempay_card_type';
                $multiObject['systempay_avail_cards']['text'] = $this->systempay->l('Choose your payment mean');
                $multiObject['systempay_avail_cards']['values'] = array();
                foreach ($vars['systempay_avail_cards'] as $key => $value) {
                    array_push($multiObject['systempay_avail_cards']['values'], array(
                        "opt_id" => $key,
                        "opt_value" => $value,
                    ));
                }

                array_push($activatedPayment, $multiObject);
            }

            return $activatedPayment;
        }

        public function getPaymentParams($attribute, $data, $context)
        {

            $cart = $context->cart;

            $params = array();

            if ($attribute == 'standard') {
                $standard = new SystempayStandardPayment();
                $request = $standard->prepareRequest($cart);
                $params['systempay_params'] = $request->getRequestFieldsArray(false, false);
                $params['systempay_url'] = $request->get('platform_url');
            }

            if ($attribute == 'multi') {
                $multi = new SystempayMultiPayment();
                $request = $multi->prepareRequest($cart, $data);
                $params['systempay_params'] = $request->getRequestFieldsArray(false, false);
                $params['systempay_url'] = $request->get('platform_url');
            }

            return $params;
        }

        private function checkCurrency($context)
        {
            $cart = $context->cart;

            $cart_currency = new Currency((int) $cart->id_currency);
            $currencies = $this->systempay->getCurrency((int) $cart->id_currency);

            if (!is_array($currencies) || empty($currencies)) {
                return false;
            }

            foreach ($currencies as $currency) {
                if ($cart_currency->id == $currency['id_currency']) {
                    // cart currency is allowed for this module
                    return SystempayApi::findCurrencyByAlphaCode($cart_currency->iso_code) != null;
                }
            }

            return false;
        }
    }
}
