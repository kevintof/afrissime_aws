<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'payplug/payplug.php')) {
    require_once _PS_MODULE_DIR_ . 'payplug/payplug.php';

    class PrestAppPayplugModuleController
    {
        protected $payplug;

        public function __construct()
        {
            $this->payplug = Module::getInstanceByName('payplug');
        }

        public function getData()
        {
            $data = new StdClass();
            $data->name = 'payplug';
            $data->cards_active = $this->isCardActive();
            return $data;
        }

        public function isCardActive()
        {

            if (!$this->payplug->active) {
                return false;
            } else if (PayplugBackward::getConfiguration('PAYPLUG_SHOW') == 0) {
                return false;
            } else {
                return true;
            }
        }

        public function displayPayment($context)
        {
            $is_active = true;

            if (!$this->payplug->active) {
                $is_active = false;
            } else if (PayplugBackward::getConfiguration('PAYPLUG_SHOW') == 0) {
                $is_active = false;
            } else if (!$this->payplug->checkCurrency($context->cart)) {
                $is_active = false;
            } else if (!$this->payplug->checkAmount($context->cart)) {
                $is_active = false;
            }

            $payplug_cards = $this->payplug->getCardsByCustomer($context->customer->id);
            $one_click = (int) PayplugBackward::getConfiguration('PAYPLUG_ONE_CLICK');

            $data = array(
                'is_active' => $is_active,
                'is_url' => true,
            );

            if (!empty($payplug_cards) && $one_click == 1) {
                $data['cards'] = $payplug_cards;
                $data['one_click'] = true;
            }

            return $data;
        }

        public function getPaymentUrl($context, $id_card)
        {

            $payplug_url = $this->processPayment($context, $id_card);

            //////////////////////////////////////////
            if ($payplug_url === false || (isset($payplug_url['result']) && $payplug_url['result'] === false)) {
                return false;
            } else if (isset($payplug_url['result']) && $payplug_url['result'] !== false) {
                if ($payplug_url['result'] === 'new_card') {
                    return array(
                        'type' => 'payment',
                        'url' => $payplug_url['payment_url'],
                    );
                } else if ($payplug_url['result'] === true) {
                    return array(
                        'type' => 'validation',
                        'url' => $payplug_url['validation_url'],
                    );
                }
            } else {
                return array(
                    'type' => 'payment',
                    'url' => $payplug_url,
                );
            }
        }

        public function processPayment($context, $id_card)
        {
            $cart = new Cart((int) $context->cart->id);
            if (!Validate::isLoadedObject($cart)) {
                return false;
            }

            $one_click = (int) PayplugBackward::getConfiguration('PAYPLUG_ONE_CLICK');
            $embedded_mode = (int) PayplugBackward::getConfiguration('PAYPLUG_EMBEDDED_MODE');

            $current_card = null;
            if ($id_card != null && $id_card != 'new_card') {
                $current_card = $this->payplug->getCardId(
                    (int) $cart->id_customer,
                    $id_card,
                    (int) PayplugBackward::getConfiguration('PAYPLUG_COMPANY_ID')
                );
            }

            //currency
            $result_currency = array();
            if (version_compare(_PS_VERSION_, '1.3', '<')) {
                $result_currency['iso_code'] = Tools::setCurrency()->iso_code;
            } elseif (version_compare(_PS_VERSION_, '1.5', '<')) {
                $result_currency['iso_code'] = Currency::getCurrent()->iso_code;
            } else {
                $currency = $cart->id_currency;
                $result_currency = Currency::getCurrency($currency);
            }
            $supported_currencies = explode(';', PayplugBackward::getConfiguration('PAYPLUG_CURRENCIES'));

            if (!in_array($result_currency['iso_code'], $supported_currencies)) {
                return false;
            }

            $currency = $result_currency['iso_code'];

            //amount
            if (version_compare(_PS_VERSION_, '1.4', '<')) {
                $amount = $cart->getOrderTotal(true, 3);
            } else {
                $amount = $cart->getOrderTotal(true, Cart::BOTH);
            }

            $amount = intval(round(($amount * 100), PHP_ROUND_HALF_UP));
            $current_amounts = Payplug::getAmountsByCurrency($currency);
            $current_min_amount = $current_amounts['min_amount'];
            $current_max_amount = $current_amounts['max_amount'];

            if ($amount < $current_min_amount || $amount > $current_max_amount) {
                return false;
            }

            //customer
            $customer = new Customer((int) $cart->id_customer);
            //$address_invoice = new Address((int)$cart->id_address_invoice);
            $address_delivery = new Address((int) $cart->id_address_delivery);
            //$country = new Country((int)$address_invoice->id_country);
            $country = new Country((int) $address_delivery->id_country);
            $country_iso_code = $this->payplug->getIsoCodeByCountryId((int) $country->id);

            $payment_customer = array(
                'first_name' => !empty($customer->firstname) ? $customer->firstname : null,
                'last_name' => !empty($customer->lastname) ? $customer->lastname : null,
                'email' => $customer->email,
                'address1' => !empty($address_delivery->address1) ? $address_delivery->address1 : null,
                'address2' => !empty($address_delivery->address2) ? $address_delivery->address2 : null,
                'postcode' => !empty($address_delivery->postcode) ? $address_delivery->postcode : null,
                'city' => !empty($address_delivery->city) ? $address_delivery->city : null,
                'country' => $country_iso_code,
            );

            //hosted payment
            $return_url = PayplugBackward::getModuleLink(
                $this->payplug->name,
                'validation',
                array('ps' => 1, 'cartid' => (int) $cart->id),
                true
            );
            $cancel_url = PayplugBackward::getModuleLink(
                $this->payplug->name,
                'validation',
                array('ps' => 2, 'cartid' => (int) $cart->id),
                true
            );
            if ($one_click != 1 || ($one_click == 1 && ($id_card == null || $id_card == 'new_card'))) {
                $hosted_payment = array(
                    'return_url' => $return_url,
                    'cancel_url' => $cancel_url,
                );
            }

            //notification
            $notification_url = PayplugBackward::getModuleLink($this->payplug->name, 'ipn', array(), true);

            //payment method
            if ($one_click == 1 && $current_card != null && $id_card != 'new_card') {
                $payment_method = $current_card;
            }

            //force 3ds
            $force_3ds = false;

            //save card
            $allow_save_card = false;
            if ($one_click == 1) {
                $allow_save_card = true;
            }

            //meta data
            $ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
            $baseurl = $ssl ? Tools::getShopDomainSsl(true) : Tools::getShopDomain(true);
            $metadata = array(
                'customer_id' => (int) $customer->id,
                'cart_id' => (int) $cart->id,
                'website' => $baseurl,
            );

            //payment
            $payment_tab = array(
                'amount' => $amount,
                'currency' => $currency,
                'customer' => array(
                    'email' => $payment_customer['email'],
                    'first_name' => $payment_customer['first_name'],
                    'last_name' => $payment_customer['last_name'],
                    'address1' => $payment_customer['address1'],
                    'address2' => $payment_customer['address2'],
                    'postcode' => $payment_customer['postcode'],
                    'city' => $payment_customer['city'],
                    'country' => $payment_customer['country'],
                ),
                'notification_url' => $notification_url,
                'force_3ds' => $force_3ds,
                'metadata' => array(
                    'ID Client' => $metadata['customer_id'],
                    'ID Cart' => $metadata['cart_id'],
                    'Website' => $metadata['website'],
                ),
            );

            if ($one_click == 1 && $current_card != null && $id_card != 'new_card') {
                $payment_tab['payment_method'] = $payment_method;
                $payment_tab['allow_save_card'] = false;
            } else {
                $payment_tab['hosted_payment'] = array(
                    'return_url' => $hosted_payment['return_url'],
                    'cancel_url' => $hosted_payment['cancel_url'],
                );
                $payment_tab['allow_save_card'] = $allow_save_card;
            }

            try {
                if (PayplugBackward::getConfiguration('PAYPLUG_DEBUG_MODE')) {
                    $log = new MyLogPHP(_PS_MODULE_DIR_ . $this->payplug->name . '/log/prepare_payment.csv');
                    $log->info('Starting payment.');
                    foreach ($payment_tab as $key => $value) {
                        if (is_array($value)) {
                            foreach ($value as $n_key => $n_value) {
                                $log->info($n_key . ' : ' . $n_value);
                            }
                        } else {
                            $log->info($key . ' : ' . $value);
                        }
                    }
                }
                $payment = \Payplug\Payment::create($payment_tab);
            } catch (Exception $e) {
                $data = array(
                    'result' => false,
                    'response' => $e->__toString(),
                );
                if (version_compare(_PS_VERSION_, '1.7', '<')) {
                    return $data;
                } else {
                    return ($data);
                }
            }

            $this->payplug->storePayment($payment->id, (int) $cart->id);
            if ($one_click == 1 && $current_card != null && $id_card != 'new_card') {

                $data = array(
                    'result' => true,
                    'validation_url' => $return_url,
                );
                if (version_compare(_PS_VERSION_, '1.7', '<')) {
                    return $data;
                } else {
                    return ($data);
                }
            } elseif (($one_click == 1 && $id_card == 'new_card') || ($one_click != 1 && $id_card == 'new_card')) {
                $data = array(
                    'result' => 'new_card',
                    'embedded_mode' => (int) $embedded_mode,
                    'payment_url' => $payment->hosted_payment->payment_url,
                );
                return $data;
            } else {
                $payment_url = $payment->hosted_payment->payment_url;

                return $payment_url;
            }
        }

        public function getCardsList($context)
        {

            $customer = $context->customer;
            $payplug_cards = $this->payplug->getCardsByCustomer($customer->id);

            $data = $payplug_cards ? $payplug_cards : array();

            return $data;
        }

        public function deleteCard($context, $id_card)
        {
            $valid_key = Payplug::setAPIKey();
            return $this->payplug->deleteCard($context->customer->id, $id_card, $valid_key);
        }
    }
}
