<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . '/soflexibilite/soflexibilite.php')) {
    require _PS_MODULE_DIR_ . '/soflexibilite/soflexibilite.php';

    class PrestAppSoflexibiliteModuleController extends SoFlexibilite
    {

        protected $soflexibilite;

        public function _construct()
        {
            $this->soflexibilite = Module::getInstanceByName('soflexibilite');
        }

        public function getCarriersFeesForAvailableCountries($available_countries)
        {
            // private function getShippingCostByMethod(Carrier $carrier, Cart $cart, $id_zone)
            $carriers_fees = array();

            foreach ($this->id_carrier_so as $id_carrier) {
                foreach ($available_countries as $iso_code) {
                    $carriers_fees[$id_carrier][$iso_code] = $this->getShippingCostByMethod(
                        new Carrier((int) $id_carrier),
                        $this->context->cart,
                        Country::getIdZone((int) Country::getByIso($iso_code))
                    );
                }

                $carriers_fees[$id_carrier] = array_filter(
                    $carriers_fees[$id_carrier],
                    array('SoFlexibiliteTools', 'arrayFilterNullOrLowerThanEqualZero')
                );
            }

            return Tools::jsonEncode($carriers_fees);
        }

        public function getShippingCostByMethod(Carrier $carrier, Cart $cart, $id_zone)
        {
            return $carrier->getShippingMethod() === Carrier::SHIPPING_METHOD_WEIGHT ?
            $carrier->getDeliveryPriceByWeight($cart->getTotalWeight(), $id_zone) :
            $carrier->getDeliveryPriceByPrice($cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone);
        }

        public function pointList()
        {
            $soflexibilite = Module::getInstanceByName('soflexibilite');

            if (version_compare($soflexibilite->version, '4', '>=') === true) {
                header('Access-Control-Allow-Origin: *');
                require _PS_MODULE_DIR_ . '/soflexibilite/functions/get_delivery_points.php';
            } else {
                header('Access-Control-Allow-Origin: *');
                require _PS_MODULE_DIR_ . '/soflexibilite/functions/point_list.php';
            }
        }

        public function saveInfoCommande($context) 
        {
            $soflexibilite = Module::getInstanceByName('soflexibilite');

            if (version_compare($soflexibilite->version, '4', '>=') === true) {
                $result = $this->saveInfoCommande4($context);
            } else {
                $result = $this->saveInfoCommande3($context);
            }

            return $result;
        }

        public function saveInfoCommande4($context)
        {

            ob_start();

            $message = '';

            try {
                $customer = new Customer(Tools::getValue('id_customer'));
                $cart = new Cart(Tools::getValue('id_cart'));
                $delivery_info = SoFlexibiliteDeliveryInfo::getByIdCartAndIdCustomer($cart->id, $customer->id);
            } catch (PrestaShopException $exception) {
                return false;
            }

            if (!Validate::isLoadedObject($customer) || !Validate::isLoadedObject($cart)) {
                return false;
            }

            $ceemail = Tools::getValue('ceemail');
            $cephonenumber = Tools::getValue('cephonenumber');
            $cedoorcode1 = Tools::getValue('cedoorcode1');
            $cedoorcode2 = Tools::getValue('cedoorcode2');

            if (Tools::getValue('ceemail') || Tools::getValue('ceemail') == "") {
                $delivery_info->ceemail = Tools::getValue('ceemail');
            }

            if (Tools::getValue('cephonenumber') || Tools::getValue('cephonenumber') == "") {
                $delivery_info->cephonenumber = Tools::getValue('cephonenumber');
            }

            if (Tools::getValue('cedoorcode1') || Tools::getValue('cedoorcode1') == "") {
                $delivery_info->cedoorcode1 = Tools::getValue('cedoorcode1');
            }

            if (Tools::getValue('cedoorcode2') || Tools::getValue('cedoorcode2') == "") {
                $delivery_info->cedoorcode2 = Tools::getValue('cedoorcode2');
            }

            $message = ob_get_clean();

            $result = $delivery_info->save();

            if ($result) {
                $output = $result;
            } else {
                $output = null;
            }

            $json = array(
                'output' => $output,
                'response' => '',
                'message' => $message,
            );

            return Tools::jsonEncode($json);
        }

        public function saveInfoCommande3($context)
        {
            ob_start();

            $id_cart = (int) $context->cart->id;
            $id_customer = (int) $context->customer->id;

            $customer_address = new Address($context->cart->id_address_delivery);

            $country_iso = null;

            if ($customer_address && $customer_address->id_country) {
                $country_iso = Country::getIsoById($customer_address->id_country);
            }

            $phone = null;
            $info = null;
            $status = true;

            $update_required = false;

            $soDelivery = new SoFlexibiliteDelivery();
            $soDelivery->id_cart = (int) $id_cart;
            $soDelivery->id_customer = $id_customer ? (int) $id_customer : (int) $this->context->customer->id;
            $soDelivery->loadDelivery();

            $phone = trim(Tools::getValue('phone'));
            $info = trim(Tools::getValue('infoDelivery'));
            $email = trim(Tools::getValue('email'));
            $delivery_mode = Tools::strtoupper(trim(Tools::getValue('delivery_mode')));

            if ($phone && $soDelivery->telephone != $phone) {
                $soDelivery->telephone = pSQL(str_replace(' ', '', $phone));
                $update_required = true;
            }

            if ($info && $soDelivery->informations != $info) {
                $soDelivery->informations = pSQL($info);
                $update_required = true;
            }

            if ($email && $soDelivery->email != $email) {
                $soDelivery->email = pSQL($email);
                $update_required = true;
            }

            if ($delivery_mode && $soDelivery->type != $delivery_mode) {
                $soDelivery->type = pSQL($delivery_mode);
                $update_required = true;
            }

            if ($country_iso && $soDelivery->country != $country_iso) {
                $soDelivery->country = pSQL($country_iso);
                $update_required = true;
            }

            if ($update_required) {
                $status = $soDelivery->saveDelivery() ? true : false;
            }

            $result = ob_get_clean();

            if ($result) {
                $output = $result;
            } else {
                $output = null;
            }

            $json = array(
                'output' => $output,
                'status' => $status,
                'phone' => $phone,
                'mail' => $email,
                'info' => $info,
            );

            return Tools::jsonEncode($json);
        }

        public function saveDeliveryPoint()
        {
            $soflexibilite = Module::getInstanceByName('soflexibilite');

            if (version_compare($soflexibilite->version, '4', '>=') === true) {
                header('Access-Control-Allow-Origin: *');
                require _PS_MODULE_DIR_ . '/soflexibilite/functions/save_delivery_point.php';
            } else {
                header('Access-Control-Allow-Origin: *');
                require _PS_MODULE_DIR_ . '/soflexibilite/functions/saveDeliveryPoint.php';
            }
        }

        public function displayCarrier($context)
        {

            $soflexibilite = Module::getInstanceByName('soflexibilite');

            if (version_compare($soflexibilite->version, '4', '>=') === true) {
                $result = $this->displayCarrier4($context);
            } else {
                $result = $this->displayCarrier3($context);
            }

            return $result;
        }

        private function displayCarrier4($context) {

            $address = new Address($context->cart->id_address_delivery);
            $carrier_modes = array('DOM', 'DOS', 'A2P', 'BPR');

            $carrier_id_refs = array_filter(array_map('intval', Configuration::getMultiple(
                $this->carrier_conf_keys,
                null,
                null,
                null
            )));

            foreach($carrier_id_refs as &$carrier_id) {
                $carrier = Carrier::getCarrierByReference($carrier_id);
                if (isset($carrier) && $carrier->id) {
                    $carrier_id = $carrier->id;
                }
            }

            if (!Validate::isLoadedObject($address)) {
                return false;
            }

            $country_iso = Tools::strtoupper(Country::getIsoById($address->id_country));

            $response_array = array(
                'address_to_search' => sprintf(
                    '%s, %s %s, %s',
                    $address->address1,
                    $address->postcode,
                    $address->city,
                    $address->country
                ),
                'sf_address_company' => Tools::strtoupper($address->company),
                'sf_address_street' => Tools::strtoupper($address->address1).',',
                'sf_address_postcode_city' => Tools::strtoupper(sprintf(
                    '%s %s',
                    $address->postcode,
                    $address->city
                )),
                'sf_address_country_iso' => $country_iso,
                'sf_customer_name' => Tools::strtoupper(sprintf(
                    '%s %s',
                    $context->customer->lastname,
                    $context->customer->firstname
                )),
                'id_address_delivery' => $context->cart->id_address_delivery,
                'id_customer' => $context->cart->id_customer,
                'id_cart' => $context->cart->id,
                'sf_customer_email' => $context->customer->email,
                'sf_customer_phone_mobile' => SoFlexibiliteTools::formatPhoneNumber($address->phone, $country_iso),
                'sf_otp' => SoFlexibiliteOTP::createOTPStatic($context->cart->id_customer),
                'version' => 4
            );

            if (isset($carrier_id_refs) && is_array($carrier_id_refs)) {
                foreach($carrier_id_refs as $key => $value) {
                    $response_array[$key] = $value;
                }
            }

            return $response_array;
        }

        private function displayCarrier3($context) {
            if ($this->ps15x) {
                $customerAddresses = $context->customer->getAddresses((int) $context->language->id);
                $id_delivery_address = (int) $context->cart->id_address_delivery;
            } else {
                $customerAddresses = Db::getInstance()->executeS(
                    'SELECT * FROM ' . _DB_PREFIX_ . 'address WHERE id_customer=' . (int) $context->customer->id
                );
                $id_delivery_address = (int) $context->cart->id_address_delivery;
            }

            $mobile = '';
            $inputs = array();

            foreach ($customerAddresses as $address) {
                if ((int) $address['id_address'] === $id_delivery_address) {
                    $inputs[] = array(
                        'id_address' => $address['id_address'],
                        'alias' => $address['alias'],
                        'company' => $address['company'],
                        'firstname' => $address['firstname'],
                        'lastname' => $address['lastname'],
                        'address1' => $address['address1'],
                        'address2' => $address['address2'],
                        'postcode' => $address['postcode'],
                        'city' => $address['city'],
                        'phone' => $address['phone_mobile'],
                        'country' => Country::getNameById($this->id_lang, $address['id_country']),
                        'abdefault' => $address['id_address'] === (int) $context->cart->id_address_delivery ? '1' : '0',
                    );

                    if (isset($address['phone_mobile']) && $address['phone_mobile']) {
                        $mobile = $address['phone_mobile'];
                    }

                    // Modify delay for european countries other than France
                    $country_iso = Tools::strtoupper(Country::getIsoById((int) $address['id_country']));
                    if (!in_array($country_iso, array('FR'))) {
                        try {
                            $delivery_option_list = array();
                            if (isset($params['delivery_option_list']) && $params['delivery_option_list']) {
                                $delivery_option_list = $params['delivery_option_list'];
                            }

                            foreach ($delivery_option_list as $id_address => $carrier_list_raw) {
                                foreach ($carrier_list_raw as $key => $carrier_list) {
                                    foreach (array_keys($carrier_list['carrier_list']) as $id_carrier) {
                                        if ($id_carrier == $this->id_carrier_so['SOFLEXIBILITE_BPR_ID']) {
                                            $delay = &$delivery_option_list[$id_address][$key]['carrier_list']
                                            [$id_carrier]['instance']->delay;
                                            $delay[$this->id_lang] =
                                            $this->l('Be delivered among one of the post-office available in Europe.');
                                            $name = &$delivery_option_list[$id_address][$key]['carrier_list']
                                            [$id_carrier]['instance']->name;
                                            $name = $this->l('Colissimo – In post-office');
                                        } elseif ($id_carrier == $this->id_carrier_so['SOFLEXIBILITE_A2P_ID']) {
                                            $delay = &$delivery_option_list[$id_address][$key]['carrier_list']
                                            [$id_carrier]['instance']->delay;
                                            $delay[$this->id_lang] =
                                            $this->l('Be delivered among one of the pickup available in Europe.');
                                            $name = &$delivery_option_list[$id_address][$key]['carrier_list']
                                            [$id_carrier]['instance']->name;
                                            $name = $this->l('Colissimo – In PickUp relay');
                                        }
                                    }
                                }
                            }
                        } catch (Exception $e) {
                            if (self::$debug) {
                                printf('SoFlexibilite ERROR on line %s : %s', $e->getLine(), $e->getMessage());
                            }
                        }
                    }
                }
            }

            $available_countries = implode('|', SoFlexibiliteTools::arrayColumn(
                Country::getCountries(1, true, false, false),
                'iso_code'
            ));

            /* Check if went to cart previously, changed page, then came back --> Load delivery_mode for "mon domicile" */
            $soDelivery = new SoFlexibiliteDelivery((int) $context->cart->id, (int) $context->cart->id_customer);
            $soDelivery->loadDelivery();

            if (!is_null($soDelivery->type) && Tools::strlen($soDelivery->type) && ('DOM' === $soDelivery->type || 'DOS' === $soDelivery->type)) {
                $soflexibilite_delivery_mode = $soDelivery->type;
            } else {
                $soflexibilite_delivery_mode = Configuration::get('SOFLEXIBILITE_HOME_MODE');
            }

            /* !Check if went to cart previously */

            $force_dos_delivery = false;
            $cart_price = 0;

            if ($context->cart->id) {
                $cart_price = Cart::getTotalCart((int) $context->cart->id, true);
            }

            if (Configuration::get('SOFLEXIBILITE_FORCE_DOS') && Configuration::get('SOFLEXIBILITE_FORCE_DOS_VALUE') <= $cart_price) {
                $force_dos_delivery = true;
            }

            if (class_exists('SoFlexibiliteTools')) {
                return array('soflexibilite_dom_id' => (int) $this->id_carrier_so['SOFLEXIBILITE_DOM_ID'],
                    'soflexibilite_dos_id' => (int) $this->id_carrier_so['SOFLEXIBILITE_DOS_ID'],
                    'soflexibilite_bpr_id' => (int) $this->id_carrier_so['SOFLEXIBILITE_BPR_ID'],
                    'soflexibilite_a2p_id' => (int) $this->id_carrier_so['SOFLEXIBILITE_A2P_ID'],
                    'soflexibilite_cust_id' => $id_delivery_address,
                    'inputs' => $inputs,
                    'soflexibilite_mobile' => $mobile,
                    'soflexibilite_point_list_url' => _PS_BASE_URL_ . _MODULE_DIR_ . 'prestapp/api/modules/carriers/soflexibilite/points',
                    'getCustomerAddress' => $this->getCustomerAddress,
                    'saveDeliveryPoint' => _PS_BASE_URL_ . _MODULE_DIR_ . 'prestapp/api/modules/carriers/soflexibilite/save-delivery-point',
                    'initSoflexibiliteEnv' => $this->url . '/functions/initSoflexibiliteEnv.php',
                    'checkPointSelectedOPC' => $this->initSoflexibiliteEnv_url,
                    'saveInfoCommande' => _PS_BASE_URL_ . _MODULE_DIR_ . 'prestapp/api/modules/carriers/soflexibilite/save-info-commande',
                    'protocol' => $this->protocol,
                    'soflexibilite_cust_mail' => $context->customer->email,
                    'soflexibilite_cart_id' => $context->cart->id,
                    'lang' => Language::getIsoById($this->id_lang),
                    'signature' => (self::HOME_DELIVERY_DOS === self::$home_delivery_mode) ? 1 : 0,
                    'ps15x' => $this->ps15x,
                    'so_url' => $this->url,
                    'images_url' => $this->url . '/views/img/',
                    'soflexibilite_bpr_active' => (int) SoFlexibiliteTools::carrierIsEnabled($this->id_carrier_so['SOFLEXIBILITE_BPR_ID']),
                    'soflexibilite_a2p_active' => (int) SoFlexibiliteTools::carrierIsEnabled($this->id_carrier_so['SOFLEXIBILITE_A2P_ID']),
                    'soflexibilite_user_home_mode' => (bool) Configuration::get('SOFLEXIBILITE_USER_HOME_MODE'),
                    'soflexibilite_only_map' => Configuration::get('SOFLEXIBILITE_ONLY_MAP'),
                    'soflexibilite_map_style' => Configuration::get('SOFLEXIBILITE_MAP_STYLE'),
                    'soflexibilite_available_countries' => $available_countries,
                    'soflexibilite_carrier_fee_countries' => $this->getCarriersFeesForAvailableCountries(explode('|', $available_countries)),
                    'soflexibilite_google_map_key' => Configuration::get('SOFLEXIBILITE_GMAP_KEY'),
                );
            } else {
                return false;
            }
        }
    }
}
