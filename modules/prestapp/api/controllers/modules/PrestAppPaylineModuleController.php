<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'payline/payline.php')) {
    require_once _PS_MODULE_DIR_ . 'payline/payline.php';

    class PrestAppPaylineModuleController extends ModuleCore
    {
        protected $payline;

        public function __construct()
        {
            $this->payline = Module::getInstanceByName('payline');
        }

        public function displayPayment($context)
        {

            ////////////////////////////////
            // GET ALL CONFIGURATION DATA //
            //  CODE FROM PAYLINE MODULE  //
            //       DO NOT MODIFY        //
            ////////////////////////////////

            $customer = new Customer(intval($context->customer->id));

            $paymentFrom = '<form action="'.'/modules/payline/'.'redirect.php" method="post" name="WebPaymentPayline" id="WebPaymentPayline" class="payline-form">';
            $paymentFrom .= '<input type="hidden" name="contractNumber" id="contractNumber" value="" />';
            $paymentFrom .= '<input type="hidden" name="type" id="type" value="" />';
            $paymentFrom .= '<input type="hidden" name="mode" id="mode" value="" /></form>';

            //We retrieve all contract list
            $contracts = $this->getPaylineContracts();

            $cards = array();
            $cardsNX = array();
            $directDebit = array();
            if (Configuration::get('PAYLINE_DIRDEBIT_ENABLE') && Configuration::get('PAYLINE_DIRDEBIT_CONTRACT') != '') {
                $directDebit[] = array('contract' => Configuration::get('PAYLINE_DIRDEBIT_CONTRACT'), 'type' => 'SDD', 'logo' => 'http://demo.payline.com/~product/logos/p_logo_sdd.png');
            }

            if ($contracts) {
                $cardAuthorizeByWalletNx = explode(',',Configuration::get('PAYLINE_AUTORIZE_WALLET_CARD'));
                foreach($contracts as $contract){
                    if($contract['primary']){
                        if (in_array($contract['type'],array('3XCB_L','3XCB'))) {
                            $orderTotal = round($context->cart->getOrderTotal()*100);
                            if ($orderTotal > 10000 && $orderTotal < 100000){
                                $cards[] = array('contract' => $contract['contract'], 'type' => $contract['type'], 'label' => $contract['label'], 'logo' => $contract['logo']);
                            } else {
                                // $this->log("Cart total = $orderTotal => contract ".$contract['contract']." (type ".$contract['type'].") is not shown in cash web payment");
                            }
                        } else {
                            $cards[] = array('contract' => $contract['contract'], 'type' => $contract['type'], 'label' => $contract['label'], 'logo' => $contract['logo']);
                        }
                        if(in_array($contract['type'],$cardAuthorizeByWalletNx)){
                            $cardsNX[] = array('contract' => $contract['contract'], 'type' => $contract['type'], 'logo' => $contract['logo'], 'label' => $contract['label']);
                        }
                    }
                }
            }

            $cardData = Configuration::get('PAYLINE_WALLET_ENABLE') ? $this->getMyCards($customer->id) : '';

            $paylineRecurring = false;
            if(Configuration::get('PAYLINE_RECURRING_ENABLE') && $context->cart->getOrderTotal() > Configuration::get('PAYLINE_RECURRING_TRIGGER')){
                $paylineRecurring = true;
            }

            $forceSSL = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
            $sBaseUrl = ($forceSSL ? 'https://' . $context->shop->domain_ssl : 'http://' . $context->shop->domain) . $context->shop->getBaseURI();

            $payline_data = array(
                'sBaseUrl'			=> $sBaseUrl,
				'cards'				=> $cards,
				'cardsNX'			=> $cardsNX,
				'directDebitContract'=> $directDebit,
				'cardData' 			=> $cardData,
				'payline' 			=> $paymentFrom,
				'paylineWebcash' 	=> Configuration::get('PAYLINE_WEB_CASH_ENABLE'),
				'paylineWidget' 	=> 0,
				'paylineRecurring'  => $paylineRecurring, //CB/VISA/MASTERCARD/AMEX
				'paylineDirect' 	=> Configuration::get('PAYLINE_DIRECT_ENABLE'), //CB/VISA/MASTERCARD/AMEX
				'paylineWallet' 	=> Configuration::get('PAYLINE_WALLET_ENABLE'), //CB/VISA/MASTERCARD/AMEX
				'paylineSubscribe' 	=> Configuration::get('PAYLINE_SUBSCRIBE_ENABLE'), //CB/VISA/MASTERCARD/AMEX
				'paylineDirDebit' 	=> Configuration::get('PAYLINE_DIRDEBIT_ENABLE'), // SDD
				'paylineDirDebitNb'	=> Configuration::get('PAYLINE_DIRDEBIT_NUMBER'), // SDD
				'paylineProduction' => (int)Configuration::get('PAYLINE_PRODUCTION')
            );

            $payline_config = array();

            foreach (array_keys($this->aDefaultConfig) as $sConfigKeyName) {
                if (in_array($sConfigKeyName, $this->aMultilangConfig)){
                    $payline_config[$sConfigKeyName] = Configuration::get($sConfigKeyName, $context->language->id);
                } else {
                    $payline_config[$sConfigKeyName] = Configuration::hasKey($sConfigKeyName) ? Configuration::get($sConfigKeyName) : $this->aDefaultConfig[$sConfigKeyName];
                }
            }

            $this->removePaylineGifts($context->cart);

            $orderTotal	= $context->cart->getOrderTotal();

            if (Configuration::get('PAYLINE_SUBSCRIBE_ENABLE') && Configuration::get('PAYLINE_SUBSCRIBE_GIFT_ACTIVE')){
                $lastOrderTotal = $orderTotal;
                $orderTotal = $orderTotal - $this->getSubscribeReductionAmount($context->cart);
            }

            if($paylineRecurring){
                $nxAmount = 0;
                $firstNxAmount = 0;
                if(Configuration::get('PAYLINE_RECURRING_FIRST_WEIGHT')>0){
                    $firstNxAmount = round($context->cart->getOrderTotal()*Configuration::get('PAYLINE_RECURRING_FIRST_WEIGHT'));
                    $nxAmount = round(($context->cart->getOrderTotal()*100-$firstNxAmount)/(Configuration::get('PAYLINE_RECURRING_NUMBER')-1));
                    $delta = $context->cart->getOrderTotal()*100-($firstNxAmount+($nxAmount*(Configuration::get('PAYLINE_RECURRING_NUMBER')-1)));
                    // $this->payline->log('Echeancier Nx',array('firstNxAmount'=>$firstNxAmount,'nxAmount'=>$nxAmount,'delta'=>$delta));
                    $firstNxAmount += $delta;
                } else {
                    $nxAmount = round($context->cart->getOrderTotal()*100/Configuration::get('PAYLINE_RECURRING_NUMBER'));
                    $firstNxAmount 	= $context->cart->getOrderTotal()*100 - ($nxAmount * (Configuration::get('PAYLINE_RECURRING_NUMBER')-1));
                }
                $nxAmount = $nxAmount/100;
                $firstNxAmount = $firstNxAmount/100;

                $payline_recurring_data = array(
                    'nxAmount' => $nxAmount,
                    'firstNxAmount' => $firstNxAmount,
                    'nxNumber' => Configuration::get('PAYLINE_RECURRING_NUMBER') - 1
                );
            }

            ////////////////////////////////
            //            END             //
            // GET ALL CONFIGURATION DATA //
            //  CODE FROM PAYLINE MODULE  //
            //       DO NOT MODIFY        //
            ////////////////////////////////

            $data = array();

            if ($payline_data['cards'] && $payline_data['paylineWebcash']) {
                foreach($payline_data['cards'] as $card) {
                    $type = array(
                        'payline_logo' => $card['logo'],
                        'payline_title' => $card['label'],
                        'payline_url' => null,
                        'is_active' => true,
                        'is_url' => true,
                        'payline_params' => array(
                            'contractNumber' => $card['contract'],
                            'type' => $card['type'],
                            'mode' => 'webCash'
                        )
                    );

                    array_push($data, $type);
                }
            }

            if ($payline_data['paylineRecurring']) {
                foreach($payline_data['cardsNX'] as $card) {
                    $type = array(
                        'payline_logo' => $card['logo'],
                        'payline_title' => $payline_config['PAYLINE_RECURRING_TITLE'] . ' - ' . $card['label'],
                        'payline_url' => null,
                        'is_active' => true,
                        'is_url' => true,
                        'payline_params' => array(
                            'contractNumber' => $card['contract'],
                            'type' => $card['type'],
                            'mode' => 'recurring'
                        )
                    );
                    array_push($data, $type);
                }
            }

            return $data;
        }

        public function redirectToPaymentPage($params, $context) {
            $this->payline->context = $context;
            $this->payline->context->cookie->id_customer = $context->customer->id;
            // return $this->payline->redirectToPaymentPage($params);

            $doWebPaymentRequest = $this->doWebPaymentRequest($params);

            foreach ($doWebPaymentRequest as $k=>$v){
                // $this->payline->log('request ' . $k, $v);
            }

            if (isset($doWebPaymentRequest['order']['amount']) && $doWebPaymentRequest['order']['amount'] == 0) {
                // $this->payline->log('ERROR: Order amount = 0');
                return false;
            } else if (isset($doWebPaymentRequest['payment']['amount']) && $doWebPaymentRequest['payment']['amount'] == 0) {
                return false;
            }
            $result = $this->parseWsResult($this->payline->getPaylineSDK()->doWebPayment($doWebPaymentRequest));

            if(isset($result) && $result['result']['code'] == '00000'){
                $this->saveToken((int)$this->payline->context->cart->id, $result['token']);
                return array(
                    'type' => 'payment',
                    'url' => $result['redirectURL'],
                );
            }
            elseif(isset($result)) {
                return false;
            }
        }

        protected function saveToken($nCartId, $sToken) {
            $sQuery = 'REPLACE INTO `'._DB_PREFIX_.'payline_token`
                        (`id_cart`, `token`)
                        VALUES ('. (int)$nCartId . ', "'.pSql($sToken) .'")';
    
            Db::getInstance()->Execute($sQuery);
        }

        protected function parseWsResult($mWsResult){

            $aResult = null;
    
            if ($mWsResult instanceof pl_result){
    
                $aResult = array('result'=>(array)$mWsResult);
    
            } else {
    
                $aResult = $mWsResult;
    
            } // if
    
            // $this->log('Webservice return', isset($aResult['result']) ? $aResult['result'] : var_export($aResult, true));
    
            return $aResult;
    
        }

        private function doWebPaymentRequest($paylineVars){

            $invoiceAddress			= new Address($this->payline->context->cart->id_address_invoice);
            $deliveryAddress 		= new Address($this->payline->context->cart->id_address_delivery);
            $cust 					= new Customer(intval($this->payline->context->cookie->id_customer));
            $pays 					= new Country($invoiceAddress->id_country);
            $paysLivraison			= new Country($deliveryAddress->id_country);
        
            $currency 	 	 = new Currency(intval($this->payline->context->cart->id_currency));
        
            $orderTotalWhitoutTaxes			= round($this->payline->context->cart->getOrderTotal(false)*100);
            $orderTotal						= round($this->payline->context->cart->getOrderTotal()*100);
            $taxes							= $orderTotal-$orderTotalWhitoutTaxes;
        
            $newOrderId = $this->payline->l('Cart') . (int)$this->payline->context->cart->id;
        
            $doWebPaymentRequest = array('version' => Payline::API_VERSION);
        
            $cardAuthoriseWallet = explode(',',Configuration::get('PAYLINE_AUTORIZE_WALLET_CARD'));
        
            //Log data
            // $this->payline->log('DoWebPayment', array('Payment mode'=>$paylineVars['mode'], 'Num Cart' => $newOrderId, 'Num Contract' => $paylineVars['contractNumber']));
            //Retrieve payment mode
            $bActivateWalletPayment = false;
        
            switch($paylineVars['mode'])
            {
                case 'webCash' :
                    $payment_mode = Configuration::get('PAYLINE_WEB_CASH_MODE');
                    $payment_action = Configuration::get('PAYLINE_WEB_CASH_ACTION');
                    if(Configuration::get('PAYLINE_WEB_CASH_BY_WALLET') && (in_array($paylineVars['type'],$cardAuthoriseWallet) || in_array(Configuration::get('PAYLINE_WEB_CASH_UX'),array(Payline::WIDGET_COLUMN,Payline::WIDGET_TAB)) ) ){
                        $bActivateWalletPayment = true;
                    }
                break;
        
                case 'recurring' :
                    $payment_mode = Configuration::get('PAYLINE_RECURRING_MODE');
                    $payment_action = Configuration::get('PAYLINE_RECURRING_ACTION');
        
                    if(Configuration::get('PAYLINE_RECURRING_BY_WALLET') && in_array($paylineVars['type'],$cardAuthoriseWallet)){
                        $bActivateWalletPayment = true;
        
                    }
                    
                    $nUnitAmount = 0;
                    $nFirstAmount = 0;
                    if(Configuration::get('PAYLINE_RECURRING_FIRST_WEIGHT')>0){
                        $nFirstAmount = round($orderTotal*Configuration::get('PAYLINE_RECURRING_FIRST_WEIGHT')/100);
                        $nUnitAmount = round(($orderTotal-$nFirstAmount)/(Configuration::get('PAYLINE_RECURRING_NUMBER')-1));
                        $delta = $orderTotal-($nFirstAmount+($nUnitAmount*(Configuration::get('PAYLINE_RECURRING_NUMBER')-1)));
                        $nFirstAmount += $delta;
                    }else{
                        $nUnitAmount = round($orderTotal/Configuration::get('PAYLINE_RECURRING_NUMBER'));
                        $nFirstAmount = $orderTotal - ($nUnitAmount * (Configuration::get('PAYLINE_RECURRING_NUMBER')-1));
                    }
                    
                    $doWebPaymentRequest['recurring']['amount'] 		= $nUnitAmount;
                    $doWebPaymentRequest['recurring']['firstAmount'] 	= $nFirstAmount;
                    $doWebPaymentRequest['recurring']['billingCycle']	= Configuration::get('PAYLINE_RECURRING_PERIODICITY');
                    $doWebPaymentRequest['recurring']['billingLeft']	= Configuration::get('PAYLINE_RECURRING_NUMBER');
                    $doWebPaymentRequest['recurring']['billingDay']		= '';
                    $doWebPaymentRequest['recurring']['startDate']		= '';
        
        
                    // $this->log('paymentRequest', $doWebPaymentRequest['recurring']);
        
        
                    break;
        
                case 'subscribe' :
        
                    $payment_mode = Configuration::get('PAYLINE_SUBSCRIBE_MODE');
                    $payment_action = Configuration::get('PAYLINE_SUBSCRIBE_ACTION');
                    $bActivateWalletPayment = true;
        
                    if (Configuration::get('PAYLINE_SUBSCRIBE_GIFT_ACTIVE')){
        
                        // removing anothers gifts
                        $this->removePaylineGifts($this->payline->context->cart);
        
                        // creating unique gift voucher
                        $nCartRuleId = $this->createCartRule($this->payline->context->cart);
        
                        if ($nCartRuleId){
        
                            $this->payline->context->cart->addCartRule($nCartRuleId);
        
                        } // if
        
                    } // if
        
                    list($firstAmount, $nUnitAmount, $orderTotalWhitoutTaxes, $taxes) = $this->getSubscribtionAmounts($this->payline->context->cart, Configuration::get('PAYLINE_SUBSCRIBE_NUMBER'));
                    
            $cfgStartDate = (int)Configuration::get('PAYLINE_SUBSCRIBE_START_DATE'); // 1 => à la commande / 2 => après 1 période / 3 => après 2 périodes
                    $cfgSubscribeDay = (int)Configuration::get('PAYLINE_SUBSCRIBE_DAY');
                    $iWaitPeriod = $cfgStartDate-1; // on attend 0, 1 ou 2 périodes avant de débuter l'échéancier
                    $iPeriodicity = (int)Configuration::get('PAYLINE_SUBSCRIBE_PERIODICITY'); // voir $aPeriodicityOptions
                    
                    switch ($iPeriodicity){
                        case 10 : // Daily
                            $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d day', $iWaitPeriod)));
                            break;
                        case 20 : // Weekly
                            $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d week', $iWaitPeriod)));
                            break;
                        case 30 : // Bimonthly
                            $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d week', $iWaitPeriod*2)));
                            break;
                        case 40 : // Monthly
                            if($cfgSubscribeDay == 0){ // PAYLINE_SUBSCRIBE_DAY=0 => les prélèvements mensuels ont lieu le même jour que celui de la commande initiale 
                                $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d month', $iWaitPeriod)));
                            }else{ // les prélèvements ont lieu le PAYLINE_SUBSCRIBE_DAY de chaque mois
                                $iWaitPeriod = date('j') <= $cfgSubscribeDay ? $iWaitPeriod : $iWaitPeriod+1; // si le jour de prélèvement est passé, on décale au mois suivant
                                $sStartDate = date('d/m/Y', strtotime(sprintf(date('Y').'/'.date('n').'/'.$cfgSubscribeDay.' + %d month', $iWaitPeriod))); // on calcule le décalage à partir de la date au format Y/n/j que strtotime comprend  
                            }
                            break;
                        case 50 : // Two quaterly
                            $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d month', $iWaitPeriod*2)));
                            break;
                        case 60 : // Quaterly
                            $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d month', $iWaitPeriod*3)));
                            break;
                        case 70 : // Semiannual
                            $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d month', $iWaitPeriod*6)));
                            break;
                        case 80 : // Annual
                            $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d year', $iWaitPeriod)));
                            break;
                        case 90 : // Biannual
                            $sStartDate  = date('d/m/Y', strtotime(sprintf('now + %d year', $iWaitPeriod*2)));
                            break;
                    }
                    $doWebPaymentRequest['recurring']['amount'] 		= $nUnitAmount;
                    $doWebPaymentRequest['recurring']['firstAmount'] 	= $firstAmount;
                    $doWebPaymentRequest['recurring']['billingCycle']	= Configuration::get('PAYLINE_SUBSCRIBE_PERIODICITY');
                    $doWebPaymentRequest['recurring']['billingLeft']	= Configuration::get('PAYLINE_SUBSCRIBE_NUMBER') > 1 ? Configuration::get('PAYLINE_SUBSCRIBE_NUMBER') : null; // PAYLINE_SUBSCRIBE_NUMBER=1 => no limit => billingLeft is null
                    $doWebPaymentRequest['recurring']['billingDay']		= Configuration::get('PAYLINE_SUBSCRIBE_DAY') ? Configuration::get('PAYLINE_SUBSCRIBE_DAY') : date('d');
                    $doWebPaymentRequest['recurring']['startDate']		= $sStartDate;
        
                    // $this->log('paymentRequest', $doWebPaymentRequest['recurring']);
        
        
                break;
                
                case 'directdebit' :
                    $payment_mode = Configuration::get('PAYLINE_DIRDEBIT_MODE');
                    $payment_action = Configuration::get('PAYLINE_DIRDEBIT_ACTION');
                    $bActivateWalletPayment = true;
                break;
        
            }
        
            if ($paylineVars['mode'] == 'recurring' || $paylineVars['mode'] == 'subscribe'){
        
                    if(Configuration::get('PAYLINE_SUBSCRIBE_NUMBER') >1){ // number of installment is known
                    $doWebPaymentRequest['payment']['amount'] 		= $doWebPaymentRequest['recurring']['firstAmount'] + ($doWebPaymentRequest['recurring']['billingLeft'] - 1) * $doWebPaymentRequest['recurring']['amount'];
                }else{ // no limit => payment.amount is set to null
                    $doWebPaymentRequest['payment']['amount'] 		= null;
                }
        
            } else {
        
                $doWebPaymentRequest['payment']['amount'] 		= $orderTotal;
        
            } // if
        
            $doWebPaymentRequest['payment']['currency'] 		= $currency->iso_code_num;
            $doWebPaymentRequest['payment']['contractNumber']	= $paylineVars['contractNumber'];
            $doWebPaymentRequest['payment']['mode']				= $payment_mode;
            $doWebPaymentRequest['payment']['action']			= $payment_action;
        
            // ORDER
            $doWebPaymentRequest['order']['ref'] 					= $newOrderId;
            $doWebPaymentRequest['order']['country'] 				= $pays->iso_code;
            $doWebPaymentRequest['order']['taxes'] 					= $taxes;
            $doWebPaymentRequest['order']['amount'] 				= $orderTotalWhitoutTaxes;
            $doWebPaymentRequest['order']['date'] 					= date('d/m/Y H:i');
            $doWebPaymentRequest['order']['currency'] 				= $doWebPaymentRequest['payment']['currency'];
            $doWebPaymentRequest['order']['deliveryMode']			= 4; // transporteur privé
            $doWebPaymentRequest['order']['deliveryTime']			= 1; // standard
            $doWebPaymentRequest['order']['deliveryExpectedDelay']	= 7; // 7 jours par défaut
            $doWebPaymentRequest['order']['deliveryExpectedDate']	= date('d/m/Y', strtotime('now + 7 day'));
            
            
        
            $this->addCartProducts($this->payline->context->cart);
        
            // PRIVATE DATA
            $this->setPrivateData('idOrder', $newOrderId);
            $this->setPrivateData('idCart', $this->payline->context->cart->id);
            $this->setPrivateData('idCust', $cust->id);
        
            // CONTRACTS
            if(isset($paylineVars['widget']) || $paylineVars['type'] == 'widget'){ // all primary payment means are displayed in widget
                $allContracts = $this->getPaylineContracts();
                $pContracts = array();
                foreach($allContracts as $contract){
                    if($contract["primary"]){
                        // $this->log($contract["contract"].' added in primary widget contract');
                        $pContracts[] = $contract["contract"];
                    } // if
                } // foreach
                $doWebPaymentRequest['contracts'] = $pContracts;
            }else{ // payment mean is chose in store => only one choice on payment page
                $doWebPaymentRequest['contracts'] = array($paylineVars['contractNumber']);
            }
            $doWebPaymentRequest['secondContracts'] = $this->getSecondaryCardList($paylineVars['mode']);
        
            // BUYER
            $doWebPaymentRequest['buyer']['title'] 	    = ($cust->id_gender == 2) ? 'Mme' : 'M'; // 2=Mme, 1=M
            $doWebPaymentRequest['buyer']['lastName'] 	= $cust->lastname;
            $doWebPaymentRequest['buyer']['firstName'] 	= $cust->firstname;
            $doWebPaymentRequest['buyer']['email'] 		= $cust->email;
            $doWebPaymentRequest['buyer']['customerId']	= $cust->email;
            $doWebPaymentRequest['buyer']['walletId'] 	= $bActivateWalletPayment ? $this->getOrGenWalletId($cust->id) : '';		
        
            // ADDRESSES
            $AddressBill = $this->getAddress($invoiceAddress);
            $doWebPaymentRequest['billingAddress']['name']		= $AddressBill['name'];
            $doWebPaymentRequest['billingAddress']['title']		= $doWebPaymentRequest['buyer']['title'];
            $doWebPaymentRequest['billingAddress']['firstName']	= $AddressBill['firstname'];
            $doWebPaymentRequest['billingAddress']['lastName']	= $AddressBill['lastname'];
            $doWebPaymentRequest['billingAddress']['street1']	= $AddressBill['street'];
            $doWebPaymentRequest['billingAddress']['street2']	= $AddressBill['street2'];
            $doWebPaymentRequest['billingAddress']['cityName']	= $AddressBill['cityName'];
            $doWebPaymentRequest['billingAddress']['zipCode']	= $AddressBill['zipCode'];
            $doWebPaymentRequest['billingAddress']['state']	    = $AddressBill['state'];
            $doWebPaymentRequest['billingAddress']['country']	= $pays->iso_code;
            $doWebPaymentRequest['billingAddress']['phone']		= $AddressBill['phone'];
            
            $AddressDelivery = $this->getAddress($deliveryAddress);
            $doWebPaymentRequest['shippingAddress']['name']		= $AddressDelivery['name'];
            $doWebPaymentRequest['shippingAddress']['title']	= $doWebPaymentRequest['buyer']['title'];
            $doWebPaymentRequest['shippingAddress']['firstName']= $AddressDelivery['firstname'];
            $doWebPaymentRequest['shippingAddress']['lastName']	= $AddressDelivery['lastname'];
            $doWebPaymentRequest['shippingAddress']['street1']	= $AddressDelivery['street'];
            $doWebPaymentRequest['shippingAddress']['street2']	= $AddressDelivery['street2'];
            $doWebPaymentRequest['shippingAddress']['cityName']	= $AddressDelivery['cityName'];
            $doWebPaymentRequest['shippingAddress']['zipCode']	= $AddressDelivery['zipCode'];
            $doWebPaymentRequest['shippingAddress']['state']	= $AddressDelivery['state'];
            $doWebPaymentRequest['shippingAddress']['country']	= $paysLivraison->iso_code;
            $doWebPaymentRequest['shippingAddress']['phone']	= $AddressDelivery['phone'];
            
            $doWebPaymentRequest['buyer']['mobilePhone']= !empty($AddressBill['mobilePhone']) ? $AddressBill['mobilePhone'] : $AddressDelivery['mobilePhone'];
        
            // URLs
            $this->setPaylineSDKUrls($paylineVars['mode']);
        
            return $doWebPaymentRequest;
        }

        protected function getAddress($oAddress){	    
            $state = "";
            if($oAddress->id_state){
                $addressState = new State($oAddress->id_state);
                $state = $addressState->iso_code; // ISo state code is required by Paypal
            }
            return array(
                'name'           => $oAddress->alias, 
                'firstname'      => $oAddress->firstname,
                'lastname'       => $oAddress->lastname,
                'street'         => $oAddress->address1,
                'street2'        => $oAddress->address2,
                'cityName'       => $oAddress->city,
                'zipCode'        => $oAddress->postcode,
                'country'        => $oAddress->country,
                'state'          => $state,
                'phone'          => str_replace(array(' ','.','(',')','-'), '', $oAddress->phone),
                'mobilePhone'    => str_replace(array(' ','.','(',')','-'), '', $oAddress->phone_mobile)
            );
        }

        protected function getSecondaryCardList($sMode){

            $aResult = array();
    
            $bRecurring = ($sMode == Payline::MODE_RECURRING || $sMode == Payline::MODE_SUBSCRIBE || $sMode == Payline::MODE_WALLET);
    
            foreach ($this->getPaylineContractsSecondary($bRecurring) as  $aCard){
    
                $aResult[] = $aCard['contract'];
    
            } // foreach
    
            return $aResult;
    
        }

        protected function getPaylineContractsSecondary($recurring=false){

            $aResult = array();
    
            $aContracts = $this->getPaylineContracts();
    
            $aCards = explode(',',Configuration::get('PAYLINE_AUTORIZE_WALLET_CARD'));
    
            foreach ($aContracts as $aContract){
    
                if ($aContract['secondary'] == 1){
    
                    if(!$recurring ||  (in_array($aContract['type'], $aCards))){
    
                        $aResult[] = $aContract;
    
                    } // if
    
                } // if
    
            } // foreach
    
            return $aResult;
    
        }

        protected function setPaylineSDKUrls($sMode){

            $sKey = isset($this->aUrlsConfig[$sMode]) ? $sMode : Payline::MODE_WEBCASH;
    
            $oPaylineSDK = $this->payline->getPaylineSDK();
            foreach ($this->aUrlsConfig[$sKey] as $sPropertyName => $mConfig) {
                if (!is_array($mConfig) && strpos($mConfig, '%%URL%%') !== false || is_array($mConfig) && strpos($mConfig[0], '%%URL%%') !== false) {
                    $sQuery = $mConfig[1];
                    $forceSSL = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
                    $sBaseUrl = ($forceSSL ? 'https://' . $this->payline->context->shop->domain_ssl : 'http://' . $this->payline->context->shop->domain) . $this->payline->context->shop->getBaseURI();
                    if (is_array($mConfig))
                        $oPaylineSDK->{$sPropertyName} = str_replace('%%URL%%', $sBaseUrl, $mConfig[0]) . $sQuery;
                    else
                        $oPaylineSDK->{$sPropertyName} = str_replace('%%URL%%', $sBaseUrl, $mConfig);
                } else {
                    $sQuery = '';
                    $sConfigVarName = $mConfig;
                    if (is_array($mConfig)) {
                        $sConfigVarName = $mConfig[0];
                        $sQuery = $mConfig[1];
                    }
                    $oPaylineSDK->{$sPropertyName} = ($sConfigVarName ? Configuration::get($sConfigVarName) . $sQuery : '');
                }
    
            } // foreach
    
            return $sMode === $sKey;
    
        } // setPaylineSDKUrls

        protected function setPrivateData($sKey, $mValue){
            $this->payline->getPaylineSDK()->setPrivate(array('key' => $sKey, 'value' => $mValue));
        }

        protected function addCartProducts($oCart){

            foreach($oCart->getProducts() as $aProductDetail) {
    
                if(round($aProductDetail['price']*100) > 0){
                    
    
                    $this->payline->getPaylineSDK()->setItem(
                            array(
                                'ref'		=> $aProductDetail['reference'],
                                'price'		=> round($aProductDetail['price']*100),
                                'quantity'	=> $aProductDetail['cart_quantity'],
                                'comment'	=> $aProductDetail['name'],
                                //'category'	=> preg_replace('/[^a-zA-Z0-9]/','',$aProductDetail['category']),
                                'category'	=> 8, // Bijouterie
                                'brand'		=> $aProductDetail['id_manufacturer']
                            ));
    
                } // if
    
            } // foreach
    
        } // addCartProducts

        protected function getPaylineContracts($nIdShopGroup = -1, $nIdShop = -1){

            if ($nIdShop === -1){
    
                $nIdShop = Shop::getContextShopID(true);
    
            }
    
            if ($nIdShopGroup === -1){
    
                $nIdShopGroup = Shop::getContextShopGroupID(true);
    
            }
    
            foreach (array(array(0, $nIdShop),array($nIdShopGroup, 0),array(0, 0)) as $aIds){
    
                $sQuery = '
                SELECT * FROM `'._DB_PREFIX_.'payline_card`
                WHERE `contract` <> "" ' . $this->getSqlShopRestriction($aIds[0], $aIds[1])
                . ' ORDER BY `primary`, `position_primary`, `secondary`, `position_secondary`';
    
                $aResult = Db::getInstance()->ExecuteS($sQuery);
    
                if (!empty($aResult) && is_array($aResult)){
                    foreach ($aResult as $sKey => $aRow)
                        $aResult[$sKey]['id_card'] = $aRow['type'].'-'.$aRow['contract'];
    
                    return $aResult;
    
                }
    
            }
    
            return array();
    
        }

        protected function getSqlShopRestriction($nIdShopGroup = -1, $nIdShop = -1){

            if ($nIdShopGroup === -1){
    
                $nIdShopGroup = Shop::getContextShopGroupID(true);
    
            } // if
    
            if ($nIdShop === -1){
    
                $nIdShop = Shop::getContextShopID(true);
    
            } // if
    
            if ($nIdShop) {
    
                return ' AND id_shop = '.(int)$nIdShop;
    
            } elseif ($nIdShopGroup) {
    
                return ' AND id_shop_group = '.(int)$nIdShopGroup.' AND id_shop=0';
    
            } else {
    
                return ' AND id_shop_group=0 AND id_shop=0';
    
            } // if
    
        }

        protected $aDefaultConfig = array(
            'PAYLINE_AUTORIZE_WALLET_CARD'			=> 'CB,VISA,MASTERCARD,AMEX',
            'PAYLINE_MERCHANT_ID'					=> '',
            'PAYLINE_ACCESS_KEY'					=> '',
            'PAYLINE_POS'							=> '',
            'PAYLINE_CONTRACT_NUMBER'				=> '',
            'PAYLINE_CONTRACT_LIST'					=> '',
            'PAYLINE_PROXY_HOST'					=> '',
            'PAYLINE_PROXY_PORT'					=> '',
            'PAYLINE_PROXY_LOGIN'					=> '',
            'PAYLINE_PROXY_PASSWORD'				=> '',
            'PAYLINE_PRODUCTION'					=> 0,
            'PAYLINE_SECURITY_MODE'					=> 'SSL',
            'PAYLINE_NB_DAYS_DIFFERED'				=> '0',
            'PAYLINE_DEBUG_MODE'					=> 'FALSE',
            'PAYLINE_WEB_CASH_TPL_URL'				=> '',
            'PAYLINE_WEB_CASH_CUSTOM_CODE'			=> '',
            'PAYLINE_WEB_CASH_ENABLE'				=> 0,
            'PAYLINE_WEB_CASH_MODE'					=> 'CPT',
            'PAYLINE_WEB_CASH_ACTION'				=> '101',
            'PAYLINE_WEB_CASH_VALIDATION'			=> '',
            'PAYLINE_WEB_CASH_BY_WALLET'			=> '',
            'PAYLINE_WEB_CASH_UX'         			=> '',
            'PAYLINE_RECURRING_TPL_URL'				=> '',
            'PAYLINE_RECURRING_CUSTOM_CODE'			=> '',
            'PAYLINE_RECURRING_ENABLE'				=> 0,
            'PAYLINE_RECURRING_ACTION'				=> '101',
            'PAYLINE_RECURRING_BY_WALLET'			=> '',
            'PAYLINE_RECURRING_NUMBER'				=> '2',
            'PAYLINE_RECURRING_PERIODICITY'			=> '10',
            'PAYLINE_RECURRING_FIRST_WEIGHT'		=> '0',
            'PAYLINE_RECURRING_TRIGGER'				=> '0',
            'PAYLINE_RECURRING_MODE'				=> 'NX',
            'PAYLINE_DIRECT_ENABLE'					=> 0,
            'PAYLINE_DIRECT_ACTION'					=> '101',
            'PAYLINE_DIRECT_VALIDATION'				=> '',
            'PAYLINE_WALLET_ENABLE'					=> 0,
            'PAYLINE_WALLET_ACTION'					=> '101',
            'PAYLINE_WALLET_VALIDATION'				=> '',
            'PAYLINE_WALLET_PERSONNAL_DATA'			=> '',
            'PAYLINE_WALLET_PAYMENT_DATA'			=> '',
            'PAYLINE_WALLET_CUSTOM_CODE'			=> '',
            'PAYLINE_WALLET_TPL_URL'				=> '',
            'PAYLINE_SUBSCRIBE_ENABLE'				=> 0,
            'PAYLINE_SUBSCRIBE_ACTION'				=> '101',
            'PAYLINE_SUBSCRIBE_START_DATE'			=> '1',
            'PAYLINE_SUBSCRIBE_GIFT_ACTIVE'			=> 0,
            'PAYLINE_SUBSCRIBE_AMOUNT_GIFT'			=> '0',
            'PAYLINE_SUBSCRIBE_TYPE_GIFT'			=> 'amount',
            'PAYLINE_SUBSCRIBE_PERIODICITY'			=> '30',
            'PAYLINE_SUBSCRIBE_NUMBER'				=> '2',
            'PAYLINE_SUBSCRIBE_DAY'					=> '1',
            'PAYLINE_SUBSCRIBE_MODE'				=> 'REC',
            'PAYLINE_SUBSCRIBE_NUMBER_PENDING'		=> '0',
            'PAYLINE_DIRDEBIT_ENABLE'				=> 0,
            'PAYLINE_DIRDEBIT_ACTION'				=> '101',
            'PAYLINE_DIRDEBIT_START_DATE'			=> 0,
            'PAYLINE_DIRDEBIT_GIFT_ACTIVE'			=> 0,
            'PAYLINE_DIRDEBIT_AMOUNT_GIFT'			=> '0',
            'PAYLINE_DIRDEBIT_PERIODICITY'			=> '30',
            'PAYLINE_DIRDEBIT_NUMBER'				=> '2',
            'PAYLINE_DIRDEBIT_CONTRACT'				=> '',
            'PAYLINE_DIRDEBIT_DAY'					=> '1',
            'PAYLINE_DIRDEBIT_MODE'					=> 'CPT',
            'PAYLINE_WALLET_TITLE'					=> array(),
            'PAYLINE_WALLET_SUBTITLE'				=> array(),
            'PAYLINE_DIRECT_TITLE'					=> array(),
            'PAYLINE_DIRECT_SUBTITLE'				=> array(),
            'PAYLINE_RECURRING_TITLE'				=> array(),
            'PAYLINE_RECURRING_SUBTITLE'			=> array(),
            'PAYLINE_SUBSCRIBE_TITLE'				=> array(),
            'PAYLINE_SUBSCRIBE_SUBTITLE'			=> array(),
            'PAYLINE_DIRDEBIT_TITLE'				=> array(),
            'PAYLINE_DIRDEBIT_SUBTITLE'				=> array(),
            // Exclusive and product list
            'PAYLINE_SUBSCRIBE_EXCLUSIVE'			=> 0,
            'PAYLINE_DIRDEBIT_EXCLUSIVE'			=> 0,
            'PAYLINE_SUBSCRIBE_PLIST'				=> '',
            'PAYLINE_DIRDEBIT_PLIST'				=> '',
        );

        protected $aMultilangConfig =  array('PAYLINE_WALLET_TITLE',  'PAYLINE_WALLET_SUBTITLE', 'PAYLINE_DIRECT_TITLE', 'PAYLINE_DIRECT_SUBTITLE', 'PAYLINE_RECURRING_TITLE',
			'PAYLINE_RECURRING_SUBTITLE', 'PAYLINE_SUBSCRIBE_TITLE', 'PAYLINE_SUBSCRIBE_SUBTITLE', 'PAYLINE_DIRDEBIT_TITLE', 'PAYLINE_DIRDEBIT_SUBTITLE');
    
        protected $aUrlsConfig = array(
            Payline::MODE_WEBCASH => array(
                'returnURL' 				=> '%%URL%%modules/payline/validation.php',
                'cancelURL' 				=> '%%URL%%modules/payline/validation.php',
                'notificationURL' 			=> '%%URL%%modules/payline/notification.php',
                'customPaymentPageCode' 	=> 'PAYLINE_WEB_CASH_CUSTOM_CODE',
                'customPaymentTemplateURL'	=> 'PAYLINE_WEB_CASH_TPL_URL'
            ),
            Payline::MODE_RECURRING => array(
                'returnURL' 				=> '%%URL%%modules/payline/validation_nx.php',
                'cancelURL' 				=> '%%URL%%modules/payline/validation.php',
                'notificationURL' 			=> '%%URL%%modules/payline/notification.php',
                'customPaymentPageCode' 	=> 'PAYLINE_RECURRING_CUSTOM_CODE',
                'customPaymentTemplateURL' 	=> 'PAYLINE_RECURRING_TPL_URL'
            ),
            Payline::MODE_SUBSCRIBE => array(
                'returnURL' 				=> '%%URL%%modules/payline/validation_subscribe.php',
                'cancelURL' 				=> '%%URL%%modules/payline/validation.php',
                'notificationURL' 			=> '%%URL%%modules/payline/notification.php',
                'customPaymentPageCode' 	=> '',
                'customPaymentTemplateURL' 	=> ''
            ),
            Payline::MODE_DIRECT_DEBIT => array(
                'returnURL' 				=> '%%URL%%modules/payline/validation.php',
                'cancelURL' 				=> '%%URL%%modules/payline/validation.php',
                'notificationURL' 			=> '%%URL%%modules/payline/notification.php',
                'customPaymentPageCode' 	=> '',
                'customPaymentTemplateURL' 	=> ''
            ),
            Payline::MODE_DIRECT => array(
                'returnURL' 				=> '%%URL%%modules/payline/validation.php',
                'cancelURL' 				=> '%%URL%%modules/payline/validation.php',
                'notificationURL' 			=> '%%URL%%modules/payline/notification.php',
                'customPaymentPageCode' 	=> '',
                'customPaymentTemplateURL' 	=> ''
            ),
            Payline::MODE_WALLET => array(
                'returnURL' 				=> array('%%URL%%modules/payline/validation.php', '?walletInterface=true'),
                'cancelURL' 				=> array('%%URL%%modules/payline/validation.php', '?walletInterface=true'),
                'notificationURL' 			=> array('%%URL%%modules/payline/notification.php', '?walletInterface=true'),
                'customPaymentPageCode' 	=> 'PAYLINE_WALLET_CUSTOM_CODE',
                'customPaymentTemplateURL' 	=> 'PAYLINE_WALLET_TPL_URL'
            )
        );
        
            protected function removePaylineGifts($oCart){

            foreach ($oCart->getCartRules() as $aRule){
    
                if (Tools::substr($aRule['obj']->code, 0, 8) === 'PAYLINE-') {
    
                    $oCart->removeCartRule((int)$aRule['id_cart_rule']);
    
                    $aRule['obj']->active = false;
    
                    $aRule['obj']->update();
    
                } // if
    
            } // foreach
    
        }   

        protected function getSubscribeReductionAmount($oCart){

            $fResult = 0;
    
    
            if (Configuration::get('PAYLINE_SUBSCRIBE_AMOUNT_GIFT')){
    
                $fReductionRaw = Configuration::get('PAYLINE_SUBSCRIBE_AMOUNT_GIFT');
    
                switch(Configuration::get('PAYLINE_SUBSCRIBE_TYPE_GIFT')){
    
                    case 'amount':
    
                        $fOrderTotal = $oCart->getOrderTotal();
    
                        $fResult = min($fOrderTotal,  $fReductionRaw);
    
                    break;
    
                    case 'percent':
    
                        $fOrderTotal = $oCart->getOrderTotal() - $oCart->getOrderTotal(true, Cart::ONLY_SHIPPING);
    
                        $fResult = min($fOrderTotal, ($fOrderTotal * $fReductionRaw)/100);
    
                    break;
    
                } // switch
    
            } // if
    
            return $fResult;
    
        }
    }
}