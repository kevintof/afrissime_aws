<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'sympl/sympl.php')) {
    require_once _PS_MODULE_DIR_ . 'sympl/sympl.php';

    class PrestAppSymplModuleController
    {
        protected $sympl;

        public function __construct()
        {
            $this->sympl = Module::getInstanceByName('sympl');
        }

        public function displayCarrier($context)
        {
            $address_id = $context->cart->id_address_delivery;

            $weight = 0;
            foreach ($context->cart->getProducts() as $product) {
                $weight += ($product['weight'] ? $product['weight'] : 0.3) * $product['quantity'];
            }

            $address = Db::getInstance()->getRow(
                'SELECT a.address1, a.city, a.postcode, a.id_country FROM `' . _DB_PREFIX_ . 'address` a'
                . ' WHERE a.id_address = ' . (int) ($address_id)
            );

            $country = Db::getInstance()->getRow(
                'SELECT `iso_code` FROM `' . _DB_PREFIX_ . 'country` WHERE `id_country` = ' . (int) ($address['id_country'])
            );

            $sympl_offers = Db::getInstance()->executeS("SELECT id_carrier, code FROM `" . _DB_PREFIX_ . "sympl_offer`");

            $pickupPointsOffers = array('STD_PRO_PICKUP_POINT', 'EXP_PRO_PICKUP_POINT');
            $pickup_point_offers = array();
            foreach ($sympl_offers as $sympl_offer) {
                if (in_array($sympl_offer['code'], $pickupPointsOffers)) {
                    $pickup_point_offers[] = $sympl_offer;
                }
            }

            $account_number = Configuration::get('SYMPL_ACCOUNT_NUMBER');

            foreach ($pickup_point_offers as $pickup_point_offer) {
                $pickup_points_info[] = array(
                    'address' => $address['address1'],
                    'postcode' => $address['postcode'],
                    'city' => $address['city'],
                    'country' => $country['iso_code'],
                    'protocol' => isset($_SERVER['HTTPS']) ? 'https' : 'http',
                    'hostname' => $_SERVER['HTTP_HOST'],
                    'reference' => $pickup_point_offer['id_carrier'],
                    'account_number' => $account_number,
                    'offer_code' => $pickup_point_offer['code'],
                    'radio_button_id' => 'delivery_option_' . $pickup_point_offer['id_carrier'],
                    'weight' => $weight,
                );
            }

            $offers = array();

            foreach ($sympl_offers as $k => $v) {
                $offers[$v['code']] = $v['id_carrier'];
            }

            return array(
                'sympl_offers' => $offers,
                'pickup_points_info' => $pickup_points_info,
                'sympl_token' => Configuration::get('SYMPL_UNIQUE_TOKEN'),
            );
        }

        public function saveExtraInfoCommande($context)
        {
            $id_cart = $context->cart->id;
            $data_to_insert = array('id_cart' => $id_cart);
            $update_type = Tools::getValue('updateType');

            if ($update_type === 'user') {
                $dest_country = Db::getInstance()->getRow(
                    'SELECT country.iso_code FROM `' . _DB_PREFIX_ . 'cart` cart'
                    . ' JOIN `' . _DB_PREFIX_ . 'address` address ON cart.id_address_delivery = address.id_address'
                    . ' JOIN `' . _DB_PREFIX_ . 'country` country ON address.id_country = country.id_country'
                    . ' WHERE cart.id_cart = ' . (int) ($id_cart)
                );

                $phone_number = SymplValidate::validatePhoneNumber(Tools::getValue('phoneNumber'), $dest_country['iso_code']);
                $email = Tools::getValue('email');

                if (!$phone_number) {
                    return false;
                }

                if (!Validate::isEmail($email)) {
                    return false;
                }

                $data_to_insert['recipient_email'] = pSQL($email);
                $data_to_insert['recipient_phone_number'] = pSQL($phone_number);
            } elseif ($update_type === 'pickup_point') {
                $pickup_point_data = Tools::getValue('pickupPointData');
                $pickup_point = Tools::jsonDecode($pickup_point_data);

                if (empty($pickup_point_data) || !$pickup_point || !$pickup_point->name || !$pickup_point->street
                    || !$pickup_point->postalCode || !$pickup_point->city || !$pickup_point->countryCode
                    || !$pickup_point->offerCode) {
                    return false;
                }

                $data_to_insert['pickup_point_data'] = pSQL($pickup_point_data);
            } else {
                return false;
            }

            $sympl_cart_extra_info = Db::getInstance()->getRow(
                'SELECT id_sympl_cart_extra_info FROM `' . _DB_PREFIX_ . 'sympl_cart_extra_info`'
                . ' WHERE id_cart = ' . (int) ($id_cart)
            );

            if (empty($sympl_cart_extra_info)) {
                Db::getInstance()->insert('sympl_cart_extra_info', $data_to_insert);
            } else {
                Db::getInstance()->update(
                    'sympl_cart_extra_info',
                    $data_to_insert,
                    'id_sympl_cart_extra_info = ' . (int) ($sympl_cart_extra_info['id_sympl_cart_extra_info'])
                );
            }

            return true;
        }
    }
}
