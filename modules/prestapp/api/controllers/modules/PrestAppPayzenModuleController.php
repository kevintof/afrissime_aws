<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'payzen/payzen.php')) {
    require_once _PS_MODULE_DIR_ . 'payzen/payzen.php';

    class PrestAppPayzenModuleController
    {
        protected $payzen;

        public function __construct()
        {
            $this->payzen = Module::getInstanceByName('payzen');
        }

        public function displayPayment($context)
        {
            $cart = $context->cart;
            $html = '';
            $activatedPayment = array();

            $standard = new PayzenStandardPayment();
            if ($standard->isAvailable($cart)) {
                $activatedPayment['standard'] = array();

                $request = $standard->prepareRequest($cart);
                $activatedPayment['standard']['payzen_empty_cart'] = Configuration::get('PAYZEN_CART_MANAGEMENT') != PayzenTools::KEEP_CART;
                $activatedPayment['standard']['payzen_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['standard']['payzen_url'] = $request->get('platform_url');
                $activatedPayment['standard']['payzen_logo'] = _MODULE_DIR_ . 'payzen/views/img/' . $standard->getLogo();
                $activatedPayment['standard']['payzen_title'] = $request->get('order_info');
            }

            $multi = new PayzenMultiPayment();
            if ($multi->isAvailable($cart)) {
                $activatedPayment['multi'] = array();

                $request = $multi->prepareRequest($cart, $activatedPayment);
                $activatedPayment['multi']['payzen_empty_cart'] = Configuration::get('PAYZEN_CART_MANAGEMENT') != PayzenTools::KEEP_CART;
                $activatedPayment['multi']['payzen_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['multi']['payzen_url'] = $request->get('platform_url');
                $activatedPayment['multi']['payzen_logo'] = _MODULE_DIR_ . 'payzen/views/img/' . $multi->getLogo();
                $activatedPayment['multi']['payzen_title'] = $request->get('order_info');
            }

            $oney = new PayzenOneyPayment();
            if ($oney->isAvailable($cart)) {
                $activatedPayment['oney'] = array();

                $request = $oney->prepareRequest($cart, $activatedPayment);
                $activatedPayment['oney']['payzen_empty_cart'] = Configuration::get('PAYZEN_CART_MANAGEMENT') != PayzenTools::KEEP_CART;
                $activatedPayment['oney']['payzen_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['oney']['payzen_url'] = $request->get('platform_url');
                $activatedPayment['oney']['payzen_logo'] = _MODULE_DIR_ . 'payzen/views/img/' . $oney->getLogo();
                $activatedPayment['oney']['payzen_title'] = $request->get('order_info');
            }

            $ancv = new PayzenAncvPayment();
            if ($ancv->isAvailable($cart)) {
                $activatedPayment['ancv'] = array();

                $request = $ancv->prepareRequest($cart, $activatedPayment);
                $activatedPayment['ancv']['payzen_empty_cart'] = Configuration::get('PAYZEN_CART_MANAGEMENT') != PayzenTools::KEEP_CART;
                $activatedPayment['ancv']['payzen_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['ancv']['payzen_url'] = $request->get('platform_url');
                $activatedPayment['ancv']['payzen_logo'] = _MODULE_DIR_ . 'payzen/views/img/' . $ancv->getLogo();
                $activatedPayment['ancv']['payzen_title'] = $request->get('order_info');
            }

            $sepa = new PayzenSepaPayment();
            if ($sepa->isAvailable($cart)) {
                $activatedPayment['sepa'] = array();

                $request = $sepa->prepareRequest($cart, $activatedPayment);
                $activatedPayment['sepa']['payzen_empty_cart'] = Configuration::get('PAYZEN_CART_MANAGEMENT') != PayzenTools::KEEP_CART;
                $activatedPayment['sepa']['payzen_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['sepa']['payzen_url'] = $request->get('platform_url');
                $activatedPayment['sepa']['payzen_logo'] = _MODULE_DIR_ . 'payzen/views/img/' . $sepa->getLogo();
                $activatedPayment['sepa']['payzen_title'] = $request->get('order_info');
            }

            $sofort = new PayzenSofortPayment();
            if ($sofort->isAvailable($cart)) {
                $activatedPayment['sofort'] = array();

                $request = $sofort->prepareRequest($cart, $activatedPayment);
                $activatedPayment['sofort']['payzen_empty_cart'] = Configuration::get('PAYZEN_CART_MANAGEMENT') != PayzenTools::KEEP_CART;
                $activatedPayment['sofort']['payzen_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['sofort']['payzen_url'] = $request->get('platform_url');
                $activatedPayment['sofort']['payzen_logo'] = _MODULE_DIR_ . 'payzen/views/img/' . $sofort->getLogo();
                $activatedPayment['sofort']['payzen_title'] = $request->get('order_info');
            }

            $paypal = new PayzenPaypalPayment();
            if ($paypal->isAvailable($cart)) {
                $activatedPayment['paypal'] = array();

                $request = $paypal->prepareRequest($cart, $activatedPayment);
                $activatedPayment['paypal']['payzen_empty_cart'] = Configuration::get('PAYZEN_CART_MANAGEMENT') != PayzenTools::KEEP_CART;
                $activatedPayment['paypal']['payzen_params'] = $request->getRequestFieldsArray(false, false/* data escape will be done in redirect template */);
                $activatedPayment['paypal']['payzen_url'] = $request->get('platform_url');
                $activatedPayment['paypal']['payzen_logo'] = _MODULE_DIR_ . 'payzen/views/img/' . $paypal->getLogo();
                $activatedPayment['paypal']['payzen_title'] = $request->get('order_info');
            }
            return $activatedPayment;
        }
    }
}
