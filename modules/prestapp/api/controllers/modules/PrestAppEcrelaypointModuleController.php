<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'ecrelaypoint/ecrelaypoint.php')) {
    require_once _PS_MODULE_DIR_ . 'ecrelaypoint/ecrelaypoint.php';

    class PrestAppEcrelaypointModuleController
    {
        protected $ecrelaypoint;

        public function __construct()
        {
            $this->ecrelaypoint = Module::getInstanceByName('ecrelaypoint');
        }

        public function getData() 
        {
            $week = array($this->ecrelaypoint->l('Monday'),$this->ecrelaypoint->l('Tuesday'), $this->ecrelaypoint->l('Wenesday'), $this->ecrelaypoint->l('Thursday'), $this->ecrelaypoint->l('Friday'), $this->ecrelaypoint->l('Saturday'), $this->ecrelaypoint->l('Sunday'));

            return array(
                'active' => true,
                'week' => $week,
                'token' => Configuration::get('ECRELAYPOINT_TOKEN')
            );
        }

        public function saveDeliveryPoint($id_pr, $id_cart, $tokenecrelaypoint, $context) 
        {
            if ($tokenecrelaypoint != Configuration::get('ECRELAYPOINT_TOKEN')) {
                return false;
            } else {
                $shop_id = (int)$context->shop->id;

                $sql = 'DELETE FROM '._DB_PREFIX_.'ecrelaypoint_temp WHERE id_cart = '.(int)$id_cart.' and id_shop='.(int)$context->shop->id;

                Db::getInstance()->execute($sql);

                if (!Db::getInstance()->insert('ecrelaypoint_temp', array(
                        'id_relaypoint' => (int)$id_pr,
                        'id_cart' => (int)$id_cart,
                        'id_shop' => (int)$shop_id
                    ))) {
                    return false;
                } else {
                    return true;
                }
            }
        }

        public function getPoints($context)
        {
            include_once _PS_MODULE_DIR_ . '/ecrelaypoint/class/relaypoint.php';

            $rp = RelayPoint::getRelayPoints();

            if (Configuration::get('ECRELAYPOINT_STOCKA', null, $context->shop->id_shop_group, (int)$context->shop->id)) {
                if (!Configuration::get('ECRELAYPOINT_DIFFWA', null, $context->shop->id_shop_group, (int)$context->shop->id)) {
                    foreach ($rp as $key => $value) {
                        if (!$this->checkWh($value['id_entrepot'], $context->cart->id)) {
                            unset($rp[$key]);
                        }
                    }
                }
            }

            return $rp;
        }

        private function checkWh($idwarehouse, $idcart)
        {
            $cartproduct = Db::getInstance()->executeS('
                SELECT `id_product_attribute` FROM '._DB_PREFIX_.'cart_product
                WHERE `id_cart` = '.(int)$idcart.'');
            $res = true;
            foreach ($cartproduct as $key => $value) {
                if (!$this->ecrelaypoint->productIsPresentInStockQt($value['id_product_attribute'], $idwarehouse)) {
                    $res = false;
                }
            }
            return $res;
        }

    }
}