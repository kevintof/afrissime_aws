<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'colissimo/colissimo.php')) {
    require_once _PS_MODULE_DIR_ . 'colissimo/colissimo.php';

    class PrestAppColissimoModuleController
    {
        protected $colissimo;

        public function __construct()
        {
            $this->colissimo = Module::getInstanceByName('colissimo');
        }

        public function displayCarrier($context)
        {
            $carriers = ColissimoService::getAll();
            $moduleCarrierIds = Configuration::getMultiple(
                array(
                    'COLISSIMO_CARRIER_AVEC_SIGNATURE',
                    'COLISSIMO_CARRIER_SANS_SIGNATURE',
                    'COLISSIMO_CARRIER_RELAIS'
                )
            );

            $association = array();

            foreach ($moduleCarrierIds as $key => $value) {
                $carrier = ColissimoCarrier::getCarrierByReference((int) $moduleCarrierIds[$key]);
                $association[$key] = $carrier->id;
            }

            $cart = new Cart($context->cart->id);

            $deliveryAddr = new Address((int) $cart->id_address_delivery);

            $delivery_addr = array(
                'address'     => $deliveryAddr->address1,
                'zipcode'     => $deliveryAddr->postcode,
                'city'        => $deliveryAddr->city,
                'iso_country' => Country::getIsoById($deliveryAddr->id_country)
            );

            $association['token'] = $this->colissimo->getWidgetToken();
            $association['dyPreparationTime'] = Configuration::get('COLISSIMO_ORDER_PREPARATION_TIME');
            $association['addresse'] = $delivery_addr['address'];
            $association['codePostal'] = $delivery_addr['zipcode'];
            $association['ville'] = $delivery_addr['city'];
            $association['pays'] = $delivery_addr['iso_country'];
            $association['mobile_phone'] = ColissimoCartPickupPoint::getMobilePhoneByCartId((int) $cart->id) == "false" ? false : ColissimoCartPickupPoint::getMobilePhoneByCartId((int) $cart->id);

            return $association;
        }

        public function saveInfoCommande($params, $context)
        {

            if ($params['delivery_mode'] == 'A2P') {
                $mobilePhone = $params['phone'];
                if (false === strpos($mobilePhone, '_') && $mobilePhone) {
                    $mobilePhone = str_replace(array('(', '_', ')'), '', $mobilePhone);
                    try {
                        ColissimoCartPickupPoint::updateMobilePhoneByCartId($context->cart->id, $mobilePhone);
                        return true;
                    } catch (Exception $e) {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }

        public function saveDeliveryPoint($params)
        {
            $infoPoint = array(
                'colissimo_id' => $params['infoPoint']['identifiant'],
                'company_name' => $params['infoPoint']['nom'],
                'address1' => $params['infoPoint']['adresse1'],
                'address2' => $params['infoPoint']['adresse2'],
                'address3' => $params['infoPoint']['adresse3'],
                'city' => $params['infoPoint']['localite'],
                'zipcode' => $params['infoPoint']['codePostal'],
                'country' => $params['infoPoint']['libellePays'],
                'iso_country' => $params['infoPoint']['codePays'],
                'product_code' => $params['infoPoint']['typeDePoint'],
                'network' => $params['infoPoint']['reseau']
            );

            $data = $infoPoint;
            $colissimoId = $data['colissimo_id'];
            $pickupPoint = ColissimoPickupPoint::getPickupPointByIdColissimo($colissimoId);
            $pickupPoint->hydrate(array_map('pSQL', $data));
            $mobilePhone = str_replace(array('(', '_', ')'), '', $params['mobilePhone']);
            $needMobileValidation = Configuration::get('PS_ORDER_PROCESS_TYPE');
            try {
                $pickupPoint->save();
                ColissimoCartPickupPoint::updateCartPickupPoint(
                    (int) $params['id_cart'],
                    (int) $pickupPoint->id,
                    $mobilePhone
                );
                return true;
            } catch (Exception $e) {
                return false;
            }
        }

        public function getPoints($params)
        {
            $curl = curl_init();

            $url = 'https://ws.colissimo.fr/widget-point-retrait/GetPointsRetrait.rest';
            $query = '';

            if (isset($params)) {
                if (is_array($params) && count($params) > 0) {
                    $url .= '?';
                }

                foreach($params as $key => $value) {
                    $query .= urlencode($key) . '=' . urlencode($value) . '&';
                }

                $url = $url . $query;
            }

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return false;
            } else {
                $points = json_decode($response);
                return $points;
            }
        }
    }
}