<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'cmcicpaiement/cmcicpaiement.php')) {
    require_once _PS_MODULE_DIR_ . 'cmcicpaiement/cmcicpaiement.php';

    class PrestAppCmcicModuleController
    {
        protected $cmcicpaiement;

        public function __construct()
        {
            $this->cmcicpaiement = new CMCICPaiement();
        }

        public function displayPayment(Context $context)
        {
            $language = $context->language;
            $currency = $context->currency;
            $customer = $context->customer;
            $cart = $context->cart;

            if (!$this->cmcicpaiement->includeConf()) {
                return false;
            }

            // CMCIC server only understands "EN" for english language
            if ('GB' === Tools::strtoupper($language->iso_code)) {
                $cmcic = new CmCicTpe('EN');
            } else {
                $cmcic = new CmCicTpe(Tools::strtoupper($language->iso_code));
            }

            $hmac = new CmCicHmac($cmcic);
            $cmcic_date = date('d/m/Y:H:i:s');
            $cmcic_amount = $cart->getOrderTotal();
            $cmcic_alias = '';

            $cmcic_currency = Tools::strtoupper($currency->iso_code);
            $cmcic_reference = $cart->id . rand(10, 99);
            $cmcic_email = $customer->email;
            $cmcic_textelibre = '[' . $customer->id . '] ' . $customer->email;

            if (1 === (int) Configuration::get('CMCIC_EXPRESS')) {
                $cmcic_alias = $customer->id;
            }

            $hmac_plain = sprintf(
                CMCIC_CGI1_FIELDS,
                $cmcic->s_numero,
                $cmcic_date,
                $cmcic_amount,
                $cmcic_currency,
                $cmcic_reference,
                $cmcic_textelibre,
                $cmcic->s_version,
                $cmcic->s_langue,
                $cmcic->s_code_societe,
                $cmcic_email,
                $cmcic_alias,
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            );

            $hmac_cipher = $hmac->computeHmac($hmac_plain);

            $data = array(
                'cmcic' => $cmcic,
                'cmcic_date' => $cmcic_date,
                'cmcic_montant' => $cmcic_amount . $cmcic_currency,
                'cmcic_reference' => $cmcic_reference,
                'cmcic_textelibre' => $cmcic_textelibre,
                'cmcic_email' => $cmcic_email,
                'express_option' => (int) Configuration::get('CMCIC_EXPRESS'),
                'cmcic_alias' => $cmcic_alias,
                'hmac' => $hmac_cipher,
                'cmcicpaiement_form' => 'cmcicpaiement_form1',
                'cmcic_picture' => 'views/img/cmcicpaiement_paiement.png',
                'cmcic_text' => $this->cmcicpaiement->l('Pay by credit card with CM-CIC paiement'),
            );

            return $data;
        }
    }
}
