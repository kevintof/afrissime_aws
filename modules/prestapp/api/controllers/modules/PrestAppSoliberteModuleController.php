<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . '/soliberte/soliberte.php')) {
    require _PS_MODULE_DIR_ . '/soliberte/soliberte.php';

    class PrestAppSoliberteModuleController extends SoLiberte
    {
        public function _construct()
        {
            // parent::_construct();
        }

        public function getPoints()
        {
            header('Access-Control-Allow-Origin: *');
            require _PS_MODULE_DIR_ . '/soliberte/functions/point_list.php';
        }

        public function saveInfoCommande()
        {
            header('Access-Control-Allow-Origin: *');
            require _PS_MODULE_DIR_ . '/soliberte/functions/saveInfoCommande.php';
        }

        public function saveDeliveryPoint()
        {
            header('Access-Control-Allow-Origin: *');
            require _PS_MODULE_DIR_ . '/soliberte/functions/saveDeliveryPoint.php';
        }

        public function displayCarrier($context)
        {
            if ($this->ps15x) {
                $customerAddresses = $context->customer->getAddresses((int) $context->language->id);
                $id_delivery_address = (int) $context->cart->id_address_delivery;
            } else {
                $customerAddresses = Db::getInstance()->executeS(
                    'SELECT * FROM ' . _DB_PREFIX_ . 'address WHERE id_customer=' . (int) $context->customer->id
                );
                $id_delivery_address = (int) $context->cart->id_address_delivery;
            }

            $mobile = '';
            $inputs = array();

            foreach ($customerAddresses as $address) {
                if ((int) $address['id_address'] === $id_delivery_address) {
                    $inputs[] = array(
                        'id_address' => $address['id_address'],
                        'alias' => $address['alias'],
                        'company' => $address['company'],
                        'firstname' => $address['firstname'],
                        'lastname' => $address['lastname'],
                        'address1' => $address['address1'],
                        'address2' => $address['address2'],
                        'postcode' => $address['postcode'],
                        'city' => $address['city'],
                        'phone' => $address['phone_mobile'],
                        'country' => Country::getNameById($this->id_lang, $address['id_country']),
                        'abdefault' => $address['id_address'] === (int) $context->cart->id_address_delivery ? '1' : '0',
                    );

                    if (isset($address['phone_mobile'])) {
                        $phone_mobile = '/^((\+|00)33\s?|0)[67](\s?\d{2}){4}$/i';
                        $phone_mobile_be = '/^((\+|00)32\s?|0)4(60|[789]\d)(\s?\d{2}){3}$/i';

                        if (preg_match($phone_mobile, $address['phone_mobile']) || preg_match($phone_mobile_be, $address['phone_mobile'])) {
                            $mobile = $address['phone_mobile'];
                        }
                    }
                }
            }

            /* Check if went to cart previously, changed page, then came back --> Load delivery_mode for "mon domicile" */
            $soDelivery = new SoLiberteDelivery((int) $context->cart->id, (int) $context->cart->id_customer);
            $soDelivery->loadDelivery();

            if (!is_null($soDelivery->type) && Tools::strlen($soDelivery->type) && ('DOM' === $soDelivery->type || 'DOS' === $soDelivery->type)) {
                $soliberte_delivery_mode = $soDelivery->type;
            } else {
                $soliberte_delivery_mode = Configuration::get('SOLIBERTE_HOME_MODE');
            }
            /* !Check if went to cart previously */

            $force_dos_delivery = false;
            $cart_price = 0;

            if ($context->cart->id) {
                $cart_price = Cart::getTotalCart((int) $context->cart->id, true);
            }

            if (Configuration::get('SOLIBERTE_FORCE_DOS') && Configuration::get('SOLIBERTE_FORCE_DOS_VALUE') <= $cart_price) {
                $force_dos_delivery = true;
            }

            if (class_exists('SoLiberteTools')) {
                return array('soliberte_dom_id' => (int) $this->id_carrier_so['SOLIBERTE_DOM_ID'],
                    'soliberte_dos_id' => (int) $this->id_carrier_so['SOLIBERTE_DOS_ID'],
                    'soliberte_bpr_id' => (int) $this->id_carrier_so['SOLIBERTE_BPR_ID'],
                    'soliberte_a2p_id' => (int) $this->id_carrier_so['SOLIBERTE_A2P_ID'],
                    'soliberte_cust_id' => $id_delivery_address,
                    'inputs' => $inputs,
                    'soliberte_mobile' => $mobile,
                    'soliberte_point_list_url' => $this->point_list_url,
                    'getCustomerAddress' => $this->getCustomerAddress,
                    'saveDeliveryPoint' => $this->saveDelivery_url,
                    'saveInfoCommande' => $this->saveInfoCommande,
                    'protocol' => $this->protocol,
                    'soliberte_cust_mail' => $context->customer->email,
                    'soliberte_cart_id' => $context->cart->id,
                    'lang' => Language::getIsoById($this->id_lang),
                    'signature' => (self::HOME_DELIVERY_DOS === self::$home_delivery_mode) ? 1 : 0,
                    'ps15x' => $this->ps15x,
                    'so_url' => $this->url,
                    'images_url' => $this->url . '/views/img/',
                    'soliberte_bpr_active' => (int) SoLiberteTools::carrierIsEnabled($this->id_carrier_so['SOLIBERTE_BPR_ID']),
                    'soliberte_a2p_active' => (int) SoLiberteTools::carrierIsEnabled($this->id_carrier_so['SOLIBERTE_A2P_ID']),
                    'soliberte_delivery_mode' => $soliberte_delivery_mode,
                    'soliberte_user_home_mode' => (bool) Configuration::get('SOLIBERTE_USER_HOME_MODE'),
                    'soliberte_force_dos' => $force_dos_delivery,
                    'soliberte_only_map' => Configuration::get('SOLIBERTE_ONLY_MAP'),
                    'soliberte_map_style' => Configuration::get('SOLIBERTE_MAP_STYLE'),
                );
            } else {
                return false;
            }
        }
    }
}
