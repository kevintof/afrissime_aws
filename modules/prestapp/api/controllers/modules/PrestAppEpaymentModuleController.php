<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (file_exists(_PS_MODULE_DIR_ . 'epayment/epayment.php')) {
    require_once _PS_MODULE_DIR_ . 'epayment/epayment.php';

    class PrestAppEpaymentModuleController
    {
        protected $epayment;

        public function __construct()
        {
            $this->epayment = Module::getInstanceByName('epayment');
        }

        public function displayPayment($context)
        {

            $methods = $this->epayment->getHelper()->getActivePaymentMethods();

            $recurringCards = array();
            $cards = array();
            foreach ($methods as $method) {

                $params = array(
                    'a' => 'r',
                    'method' => $method['id_card'],
                );

                $params = http_build_query($params);

                $epayment_params = $this->buildParams($method, $context);
                $epayment_url = $this->getUrl($epayment_params);

                $card = array(
                    'id' => $method['id_card'],
                    'payment' => $method['type_payment'],
                    'card' => $method['type_card'],
                    'epayment_title' => $this->epayment->l('Pay by') . ' ' . $method['label'],
                    'epayment_url' => $epayment_url,
                    'epayment_logo' => _PS_BASE_URL_SSL_ . $this->epayment->getMethodImageUrl($method['type_card']),
                    'epayment_params' => $epayment_params,
                );
                $cards[] = $card;

                if ($this->epayment->getConfig()->isRecurringCard($method)) {
                    $recurringCards[] = $card;
                }

                $cardTypes[] = $method['type_card'];
            }

            return $cards;
        }

        private function buildParams($method, $context)
        {
            $type = 'standard';
            $cart = $context->cart;

            $values = $this->epayment->getHelper()->buildSystemParams($cart, $method, $type);

            return $values;
        }

        private function getUrl($values)
        {
            if ($values['PBX_TYPEPAIEMENT'] == 'KWIXO') {
                $urls = $this->epayment->getConfig()->getKwixoUrls();
            } elseif ($values['PBX_TYPECARTE'] == 'ANCV') {
                $urls = $this->epayment->getConfig()->getPHPUrls();
            } elseif ($this->epayment->getHelper()->isMobile()) {
                $urls = $this->epayment->getConfig()->getMobileUrls();
            } else {
                $urls = $this->epayment->getConfig()->getSystemUrls();
            }

            $url = $this->epayment->getHelper()->checkUrls($urls);

            return $url;
        }
    }
}
