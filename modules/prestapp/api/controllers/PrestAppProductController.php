<?php

/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppProductController
{
    public static function searchProducts($expr, $p, $n, $column, $order, $populate, $context)
    {

        if ('' == $column) {
            $column = 'position';
        }

        if ('' == $order) {
            $order = 'DESC';
        }

        $search = Search::find($context->language->id, $expr, $p, $n, $column, $order, false, false, $context);

        $products = array();
        foreach ($search['result'] as $product) {
            $product = self::getProduct((int) $product['id_product'], $populate, $context);
            if ($product && $product != 404 && $product != 403) {
                array_push($products, $product);
            }
        }

        $data['products'] = $products;
        $data['total'] = $search['total'];

        return $data;
    }

    public static function getNewProducts($populate, $context)
    {
        if (!Configuration::get('NEW_PRODUCTS_NBR')) {
            return array();
        }

        $newProducts = false;
        // if (Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) {
        $newProducts = Product::getNewProducts((int) $context->language->id, 0, (int) Configuration::get('NEW_PRODUCTS_NBR'));
        // }

        if (!$newProducts && Configuration::get('PS_BLOCK_NEWPRODUCTS_DISPLAY')) {
            return array();
        }

        $new_products = array();

        if ($newProducts) {
            foreach ($newProducts as $product) {
                $product = self::getProduct((int) $product['id_product'], $populate, $context);
                if ($product && $product != 404 && $product != 403) {
                    array_push($new_products, $product);
                }
            }
        }

        return $new_products;
    }

    public static function getBestSellersProducts($populate, $context)
    {
        if (Configuration::get('PS_CATALOG_MODE')) {
            return false;
        }

        if (!($bestSellers = ProductSale::getBestSalesLight((int) $context->language->id, 0, (int) Configuration::get('PS_BLOCK_BESTSELLERS_TO_DISPLAY')))) {
            return Configuration::get('PS_BLOCK_BESTSELLERS_DISPLAY') ? array() : false;
        }

        $bestsellers_products = array();

        if ($bestSellers) {
            foreach ($bestSellers as $product) {
                $product = self::getProduct((int) $product['id_product'], $populate, $context);
                if ($product && $product != 404 && $product != 403) {
                    array_push($bestsellers_products, $product);
                }
            }
        }

        return $bestsellers_products;
    }

    public static function getFeaturedProducts($populate, $context)
    {
        $category = new Category((int) Configuration::get('HOME_FEATURED_CAT'), (int) $context->language->id);
        $nb = (int) Configuration::get('HOME_FEATURED_NBR');

        $featured_products = array();

        if (Configuration::get('HOME_FEATURED_RANDOMIZE')) {
            $featured = $category->getProducts((int) $context->language->id, 1, ($nb ? $nb : 8), null, null, false, true, true, ($nb ? $nb : 8));
        } else {
            $featured = $category->getProducts((int) $context->language->id, 1, ($nb ? $nb : 8), 'position');
        }
        foreach ($featured as $product) {
            $product = self::getProduct((int) $product['id_product'], $populate, $context);
            if ($product && $product != 404 && $product != 403) {
                array_push($featured_products, $product);
            }
        }

        return $featured_products;
    }

    public static function getAllProducts($populate, $context)
    {
        $all_products = array();

        $products = Product::getProducts((int) $context->language->id, 0, null, 'id_product', 'ASC');

        foreach ($products as $product) {
            $product = self::getProduct((int) $product['id_product'], $populate, $context);
            if ($product && $product != 404 && $product != 403) {
                array_push($all_products, $product);
            }
        }

        return $all_products;
    }

    public static function getSalesProducts($count, $populate, $context)
    {
        $sales_products = array();

        $products = Product::getPricesDrop((int) $context->language->id, 0, $count);

        if (!is_array($products)) {
            $products = array();
        }

        foreach ($products as $product) {
            $product = self::getProduct((int) $product['id_product'], $populate, $context);
            if ($product && $product != 404 && $product != 403) {
                array_push($sales_products, $product);
            }
        }

        return $sales_products;
    }

    public static function getProductFromCategory($id_category, $p, $n, $column, $order, $populate, $context)
    {
        $category = new Category($id_category);

        if ('' == $column) {
            $column = 'position';
        }

        if ('' == $order) {
            $order = 'ASC';
        }
        $products = $category->getProducts($context->language->id, $p, $n, $column, $order, false, $active = true, false, 1, $check_access = true, $context);
        $productsInCategory = array();
        foreach ($products as $product) {
            $product = self::getProduct((int) $product['id_product'], $populate, $context);
            if ($product && $product != 404 && $product != 403) {
                array_push($productsInCategory, $product);
            }
        }

        return $productsInCategory;
    }

    public static function getLastSeenProducts($populate, $context, $id_array)
    {
        $last_seen_products = array();

        foreach ($id_array as $product) {
            $product = self::getProduct((int) $product, $populate, $context);
            if ($product && $product != 404 && $product != 403) {
                array_push($last_seen_products, $product);
            }
        }

        return $last_seen_products;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // getProduct                                                                                           //
    // Get the product from the id and context provided                                                     //
    // $id_product int : product id of the product to get                                                   //
    // $populate array of string of attributes you want to populate :                                       //
    // ["stock", "price", "description", "combinations", "images", "features", "customizations", "attachments"] //
    // $context object : context of prestashop                                                              //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function getProduct($id_product, $populate, $context)
    {
        //getting the product if an ID is provided
        if ($id_product) {
            $product = new Product($id_product, true, (int) $context->language->id, $context->shop->id, $context);
        }

        //check if the product exists and is active
        if (!Validate::isLoadedObject($product) || !$product->active) {
            return 404;
            //and if the customer is allowed to see it (group restriction for exemple)
        } elseif (!$product->checkAccess(isset($context->customer) && $context->customer->id ? (int) $context->customer->id : 0)) {
            return 403;
            //we will get all information we need and return an array
            // } elseif ($product->cache_is_pack == 1) {
            //     return;
        } elseif (self::moduleAdvancedPackIsPack($id_product)) {
            return;
        } else {
            if (isset($populate)) {
                //populate array
                $populate = json_decode($populate);
                if (!is_array($populate)) {
                    $populate = array();
                }
            } else {
                $populate = array();
            }

            //set utils variables (context)
            $id_customer = (isset($context->customer) ? (int) $context->customer->id : 0);
            $id_group = $id_customer ? (int) Customer::getDefaultGroupId($id_customer) : (int) Group::getCurrent()->id;
            $id_country = $id_customer ? (int) Customer::getCurrentCountry($id_customer) : (int) getCountry();
            $id_lang_default = Configuration::get('PS_LANG_DEFAULT', null, $context->shop->id_shop_group, $context->shop->id);
            $id_lang = (isset($context->language) ? (int) $context->language->id : $id_lang_default);
            $id_currency = (int) $context->currency->id;
            $id_shop = (int) $context->shop->id;

            if ('1' === Group::getPriceDisplayMethod($id_group)) {
                $taxEnabled = false;
            } elseif (Configuration::get('PS_TAX') && !Configuration::get('AEUC_LABEL_TAX_INC_EXC')) {
                $taxEnabled = true;
            } else {
                $taxEnabled = true;
            }

            //get the amount of reduction for the current group
            $group_reduction = GroupReduction::getValueForProduct($id_product, $id_group);
            if (false === $group_reduction) {
                $group_reduction = Group::getReduction((int) $id_customer) / 100;
            }

            $data = array();
            $data['id_product'] = $product->id;

            //////////////////////
            // description part //
            //////////////////////
            if (in_array('description', $populate)) {
                $data['description'] = array(
                    'name' => strip_tags($product->name),
                    'full' => $product->description,
                    'short' => strip_tags($product->description_short),
                    'short_html' => $product->description_short,
                );
            }

            ////////////////////////////
            // manufacturer logo part //
            ////////////////////////////
            if (in_array('manufacturer', $populate)) {
                if ($product->id_manufacturer) {
                    $data['manufacturer_img'] = _PS_BASE_URL_ . '/img/m/' . $product->id_manufacturer . '.jpg';
                } else {
                    $data['manufacturer_img'] = false;
                }
            }

            ////////////////
            // price part //
            ////////////////
            if (in_array('price', $populate)) {

                //get tax from current cart
                $address = new Address((int) $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
                $tax = (float) $product->getTaxesRate($address);
                $ecotax_rate = (float) Tax::getProductEcotaxRate($address->id);
                if (PS_TAX_INC === Product::$_taxCalculationMethod && (int) Configuration::get('PS_TAX')) {
                    $ecotax_tax_amount = Tools::ps_round($product->ecotax * (1 + $ecotax_rate / 100), 2);
                } else {
                    $ecotax_tax_amount = Tools::ps_round($product->ecotax, 2);
                }

                $displayPrice = Product::getPriceStatic((int) $id_product, $taxEnabled, null, 6, null, false, true, 1, false, $id_customer, null, null);
                $priceBeforeDiscount = Product::getPriceStatic($product->id, $taxEnabled, null, 6, null, false, false, 1, true, $id_customer);
                $discountAmout = $priceBeforeDiscount - $displayPrice;
                $specific_price = SpecificPrice::getSpecificPrice($product->id, $context->shop->id, $context->currency->id, null, $id_group, 1, null, $id_customer);

                if (!$specific_price['reduction_tax'] && $taxEnabled) {
                    $reductionAmount = Tools::ps_round($specific_price['reduction'] * (1 + ($tax / 100)), 2);
                } else if ($specific_price['reduction_tax'] && !$taxEnabled) {
                    $reductionAmount = Tools::ps_round($specific_price['reduction'] / (1 + ($tax / 100)), 2);
                } else {
                    $reductionAmount = Tools::ps_round((float) $specific_price['reduction'], 2);
                }

                $data['price'] = array(
                    'displayPrice' => Tools::ps_round($displayPrice, 2),
                    'beforeReduction' => Tools::ps_round($priceBeforeDiscount, 2),
                    'ecotaxIncl' => $ecotax_tax_amount,
                    'taxIncl' => $taxEnabled,
                    'groupReduction' => $group_reduction,
                );

                if (Module::isEnabled('groupinc')) {
                    if (count($product->specificPrice) > 0 && $product->specificPrice['reduction'] > 0) {
                        $data['price']['discountAmount'] = Tools::ps_round($discountAmout, 2);
                        $data['price']['reductionAmount'] = $product->specificPrice['reduction'];
                        $data['price']['reductionType'] = $product->specificPrice['reduction_type'];
                        $data['price']['endDate'] = $product->specificPrice['to'];
                    }
                } else {
                    if (count($specific_price) > 0 && $product->specificPrice['reduction'] > 0) {
                        $data['price']['discountAmount'] = Tools::ps_round($discountAmout, 2);
                        $data['price']['reductionAmount'] = $reductionAmount;
                        $data['price']['reductionType'] = $specific_price['reduction_type'];
                        $data['price']['endDate'] = $specific_price['to'];
                    }
                }
            }

            //////////////////
            // combinations //
            //////////////////
            if (in_array('combinations', $populate)) {
                $attributes_groups = $product->getAttributesGroups($id_lang);

                $groups = array();
                if (is_array($attributes_groups) && $attributes_groups) {
                    foreach ($attributes_groups as $k => $row) {
                        // Color management
                        if (isset($row['is_color_group']) && $row['is_color_group'] && (isset($row['attribute_color']) && $row['attribute_color']) || (file_exists(_PS_COL_IMG_DIR_ . $row['id_attribute'] . '.jpg'))) {
                            $colors[$row['id_attribute']]['value'] = $row['attribute_color'];
                            $colors[$row['id_attribute']]['name'] = $row['attribute_name'];
                            if (!isset($colors[$row['id_attribute']]['attributes_quantity'])) {
                                $colors[$row['id_attribute']]['attributes_quantity'] = 0;
                            }
                            $colors[$row['id_attribute']]['attributes_quantity'] += (int) $row['quantity'];
                        }
                        if (!isset($groups[$row['id_attribute_group']])) {
                            $groups[$row['id_attribute_group']] = array(
                                'group_name' => $row['group_name'],
                                'name' => $row['public_group_name'],
                                'group_type' => $row['group_type'],
                                'default' => -1,
                            );
                        }

                        $groups[$row['id_attribute_group']]['attributes'][$row['id_attribute']] = $row['attribute_name'];
                        if ($row['default_on'] && $groups[$row['id_attribute_group']]['default'] === -1) {
                            $groups[$row['id_attribute_group']]['default'] = (int) $row['id_attribute'];
                        }
                        if (!isset($groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']])) {
                            $groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] = 0;
                        }
                        $groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] += (int) $row['quantity'];

                        $combinations[$row['id_product_attribute']]['attributes_values'][$row['id_attribute_group']] = $row['attribute_name'];
                        $combinations[$row['id_product_attribute']]['attributes'][] = (int) $row['id_attribute'];

                        // $combinations[$row['id_product_attribute']]['price'] = (float)Tools::convertPriceFull($row['price'], null, $context->currency, false);

                        // Call getPriceStatic in order to set $combination_specific_price
                        if (!isset($combination_prices_set[(int) $row['id_product_attribute']])) {
                            Product::getPriceStatic((int) $id_product, $taxEnabled, $row['id_product_attribute'], 6, null, false, true, 1, false, $id_customer, null, null, $combination_specific_price);
                            $combination_prices_set[(int) $row['id_product_attribute']] = true;
                            $combinations[$row['id_product_attribute']]['discount'] = array();
                            $combinations[$row['id_product_attribute']]['discount'] = $combination_specific_price;

                            if (Module::isEnabled('groupinc')) {
                                $priceDisplay = Product::getTaxCalculationMethod((int) $id_customer);
                                if ($priceDisplay == 0) {
                                    $product_price = Product::getPriceStatic((int) $product->id, true, $row['id_product_attribute'], 6, null, false, true, 1);
                                    $old_price = Product::getPriceStatic((int) $product->id, true, $row['id_product_attribute'], 6, null, false, false, 1);
                                } else {
                                    $product_price = Product::getPriceStatic((int) $product->id, false, $row['id_product_attribute'], 6, null, false, true, 1);
                                    $old_price = Product::getPriceStatic((int) $product->id, false, $row['id_product_attribute'], 6, null, false, false, 1);
                                }

                                $combinations[$row['id_product_attribute']]['price']['displayPrice'] = Tools::ps_round($product_price, 2);
                                // $combinations[$row['id_product_attribute']]['price']['reductionAmount'] = Tools::ps_round($old_price - $product_price, 2);
                                $combinations[$row['id_product_attribute']]['price']['reductionType'] = $combination_specific_price['reduction_type'];
                                $combinations[$row['id_product_attribute']]['price']['totalDiscountAmount'] = Tools::ps_round($old_price - $product_price, 2);
                                $combinations[$row['id_product_attribute']]['price']['beforeReduction'] = Tools::ps_round($old_price, 2);
                                $combinations[$row['id_product_attribute']]['price']['endDate'] = $combination_specific_price['to'];

                                if (!$combination_specific_price['reduction_tax'] && $taxEnabled) {
                                    $reductionAmount = Tools::ps_round($combination_specific_price['reduction'] * (1 + ($tax / 100)), 2);
                                } else {
                                    $reductionAmount = Tools::ps_round($combination_specific_price['reduction'], 2);
                                }

                                $combinations[$row['id_product_attribute']]['price']['reductionAmount'] = $reductionAmount;

                            } else {
                                $combination_price = (float) $product->base_price + (float) $product->ecotax;

                                //check if an impact on the base price exists for this combinaison
                                if (0 !== (float) $row['price']) {
                                    $combination_price = $combination_price + (float) $row['price'];
                                }

                                //apply the group reduction if exists
                                $combination_price = $combination_price - ($group_reduction * $combination_price);

                                $combinationPriceBeforeDiscount = $combination_price;

                                if ('percentage' === $combination_specific_price['reduction_type']) {
                                    $combination_price = $combination_price - ($combination_specific_price['reduction'] * $combination_price);
                                } elseif ('amount' === $combination_specific_price['reduction_type']) {
                                    if ($combination_specific_price['reduction_tax'] && $taxEnabled) {
                                        $combination_price = $combination_price - ($combination_specific_price['reduction'] / (1 + ($tax / 100)));
                                    } else {
                                        $combination_price = $combination_price - $combination_specific_price['reduction'];
                                    }
                                }

                                $combinationDiscountAmount = $combinationPriceBeforeDiscount - $combination_price;

                                if ($taxEnabled) {
                                    $combination_price = $combination_price * ($tax / 100 + 1);
                                    $combinationPriceBeforeDiscount = $combinationPriceBeforeDiscount * ($tax / 100 + 1);
                                    // $combinationDiscountAmount = $combinationDiscountAmount * ($tax/100 + 1);
                                }

                                $combinations[$row['id_product_attribute']]['price']['displayPrice'] = Tools::ps_round($combination_price, 2);

                                if (!$combination_specific_price['reduction_tax'] && $taxEnabled) {
                                    $reductionAmount = Tools::ps_round($combination_specific_price['reduction'] * (1 + ($tax / 100)), 2);
                                    $combinationDiscountAmount = $combinationDiscountAmount * (1 + ($tax / 100));
                                } else {
                                    $reductionAmount = Tools::ps_round($combination_specific_price['reduction'], 2);
                                }

                                if ($combination_specific_price > 0 && $combination_specific_price['reduction'] > 0) {
                                    $combinations[$row['id_product_attribute']]['price']['reductionAmount'] = $reductionAmount;
                                    $combinations[$row['id_product_attribute']]['price']['reductionType'] = $combination_specific_price['reduction_type'];
                                    $combinations[$row['id_product_attribute']]['price']['totalDiscountAmount'] = $combinationDiscountAmount;
                                    $combinations[$row['id_product_attribute']]['price']['beforeReduction'] = $combinationPriceBeforeDiscount;
                                    $combinations[$row['id_product_attribute']]['price']['endDate'] = $combination_specific_price['to'];
                                } else {
                                    $combinations[$row['id_product_attribute']]['price']['reductionAmount'] = $reductionAmount;
                                    $combinations[$row['id_product_attribute']]['price']['reductionType'] = null;
                                    $combinations[$row['id_product_attribute']]['price']['totalDiscountAmount'] = $combinationDiscountAmount;
                                    $combinations[$row['id_product_attribute']]['price']['beforeReduction'] = $combinationPriceBeforeDiscount;
                                    $combinations[$row['id_product_attribute']]['price']['endDate'] = null;
                                }
                            }
                        }

                        $combinations[$row['id_product_attribute']]['ecotax'] = (float) $row['ecotax'];
                        $combinations[$row['id_product_attribute']]['weight'] = (float) $row['weight'];
                        $combinations[$row['id_product_attribute']]['quantity'] = (int) $row['quantity'];
                        $combinations[$row['id_product_attribute']]['reference'] = $row['reference'];
                        $combinations[$row['id_product_attribute']]['unit_impact'] = Tools::convertPriceFull($row['unit_price_impact'], null, $context->currency, false);
                        $combinations[$row['id_product_attribute']]['minimal_quantity'] = $row['minimal_quantity'];

                        if ('0000-00-00' !== $row['available_date'] && Validate::isDate($row['available_date'])) {
                            $combinations[$row['id_product_attribute']]['available_date'] = $row['available_date'];
                            $combinations[$row['id_product_attribute']]['date_formatted'] = Tools::displayDate($row['available_date']);
                        } else {
                            $combinations[$row['id_product_attribute']]['available_date'] = $combinations[$row['id_product_attribute']]['date_formatted'] = '';
                        }
                    }

                    // wash attributes list (if some attributes are unavailables and if allowed to wash it)
                    if (!Product::isAvailableWhenOutOfStock($product->out_of_stock) && 0 === Configuration::get('PS_DISP_UNAVAILABLE_ATTR')) {
                        foreach ($groups as &$group) {
                            foreach ($group['attributes_quantity'] as $key => &$quantity) {
                                if ($quantity <= 0) {
                                    unset($group['attributes'][$key]);
                                }
                            }
                        }

                        foreach ($colors as $key => $color) {
                            if ($color['attributes_quantity'] <= 0) {
                                unset($colors[$key]);
                            }
                        }
                    }

                    $combinations_default_attributes = array();

                    if (isset($combinations)) {
                        foreach ($combinations as $id_product_attribute => $comb) {
                            $attribute_list = array();
                            foreach ($comb['attributes'] as $id_attribute) {
                                array_push($attribute_list, (int) $id_attribute);
                            }

                            $combinations[$id_product_attribute]['list'] = $attribute_list;

                            $combinations_images = self::getCombinationImagesById($id_product_attribute, $id_lang);
                            $combinations_images_ids = array();

                            if (isset($combinations_images) && $combinations_images != false) {

                                foreach ($combinations_images as $image) {
                                    array_push($combinations_images_ids, (int) $image['id_image']);
                                }
                            }

                            $combinations[$id_product_attribute]['images_list'] = $combinations_images_ids;
                        }
                    }
                }

                $default_attributes = array();
                $data['combinations']['groups'] = array();

                //format groups response
                foreach ($groups as $key => $value) {
                    $group = $value;
                    $group['id_group'] = $key;
                    array_push($default_attributes, $group['default']);

                    $attributes = array();
                    foreach ($group['attributes'] as $key => $value) {
                        $item = new stdClass();
                        $item->id_attribute = $key;
                        $item->value = $value;
                        array_push($attributes, $item);
                    }

                    $group['attributes'] = $attributes;
                    unset($group['attributes_quantity']);
                    array_push($data['combinations']['groups'], $group);
                }

                $data['combinations']['possibilities'] = array();

                if (isset($combinations)) {
                    foreach ($combinations as $id_combination => $combination) {
                        $formatted_possibility = (object) array();
                        $formatted_possibility->id_possibility = $id_combination;

                        $possibility_attributes = array();
                        //format groups response

                        foreach ($combination['attributes_values'] as $id_group => $value) {
                            $item = new stdClass();
                            $item->id_group = $id_group;
                            $item->value = $value;
                            array_push($possibility_attributes, $item);
                        }

                        $formatted_possibility->attributes = $possibility_attributes;
                        $formatted_possibility->attributes_list = $combination['list'];
                        $formatted_possibility->quantity = $combination['quantity'];
                        $formatted_possibility->price = $combination['price'];
                        $formatted_possibility->images_list = $combination['images_list'];

                        if ($formatted_possibility->attributes_list === $default_attributes) {
                            $formatted_possibility->is_default = true;
                        } else {
                            $formatted_possibility->is_default = false;
                        }

                        array_push($data['combinations']['possibilities'], $formatted_possibility);
                    }
                }
            }

            ////////////
            // images //
            ////////////
            if (in_array('images', $populate)) {
                //all products images
                $images = $product->getImages($id_lang);
                $default_image_id = null;
                $imageTypes = ImageType::getImagesTypes();
                $data['images'] = array();
                $link = new Link();

                foreach ($images as $image) {
                    $img = new stdClass();
                    $img->id_image = (int) $image['id_image'];

                    if ((int) $image['cover']) {
                        $img->is_default = true;
                    } else {
                        $img->is_default = false;
                    }

                    $img->types = array();

                    foreach ($imageTypes as $imageType) {
                        if ($imageType['products']) {
                            $type = new stdClass();
                            $type->url = $link->getImageLink($product->link_rewrite, $image['id_image'], $imageType['name']);
                            $type->height = (int) $imageType['height'];
                            $type->width = (int) $imageType['width'];
                            array_push($img->types, (object) array($imageType['name'] => $type));
                        }
                    }

                    array_push($data['images'], $img);
                }
            }

            ////////////
            // stock //
            ///////////

            if (in_array('stock', $populate)) {
                $data['stock'] = array(
                    'stock_management' => (bool) Configuration::get('PS_STOCK_MANAGEMENT'),
                    'quantities' => $product->quantity,
                    'isOutOfStockAllowed' => $product->isAvailableWhenOutOfStock((int) $product->out_of_stock),
                    'displayLastQuantitiesThreshold' => (int) Configuration::get('PS_LAST_QTIES'),
                );
            }

            ////////////
            // pack  //
            ///////////

            if (Pack::isPack((int) $product->id)) {
                $data['is_pack'] = true;
                $items = Pack::isPack($product->id) ? Pack::getItemTable($product->id, $context->language->id, false) : array();
                $data['pack_resume_items'] = array();
                $data['pack_full_items'] = array();

                foreach ($items as $item) {

                    $resume_product = null;

                    if (!array_search($item['id_product'], array_column($data['pack_full_items'], 'id_product'))) {
                        $full_product = self::getProduct((int) $item['id_product'], json_encode($populate), $context);
                        array_push($data['pack_full_items'], $full_product);
                    }

                    $resume_product['id_product'] = (int) $item['id_product'];
                    if (isset($item['id_product_attribute'])) {
                        $resume_product['id_possibility'] = (int) $item['id_product_attribute'];
                    }
                    $resume_product['pack_quantity'] = (int) $item['pack_quantity'];

                    array_push($data['pack_resume_items'], $resume_product);
                }

                if (!Pack::isInStock((int) $product->id)) {
                    $data['stock']['quantities'] = 0;
                }
            }

            //////////////
            // features //
            //////////////
            if (in_array('features', $populate)) {
                $data['features'] = Product::getFrontFeaturesStatic($id_lang, $id_product);
            }

            ////////////////////
            // customizations //
            ////////////////////
            if (in_array('customizations', $populate)) {
                $data['customizations'] = $product->getCustomizationFields($id_lang, $id_shop);
            }

            ////////////////
            // attachments//
            ////////////////
            if (in_array('attachments', $populate)) {
                $data['attachments'] = $product->getAttachments($id_lang);
            }

            ////////////////
            // categories //
            ////////////////
            $p_categories = $product->getCategories();
            $data['categories'] = $p_categories;

            ////////////////////
            //  privatesales  //
            ////////////////////
            if (in_array('privatesales', $populate)) {
                if (Module::isInstalled('privatesales') && Module::isEnabled('privatesales')) {
                    $id_category = (int) $product->id_category_default;
                    $privatesale = SaleCore::loadSaleFromCategoryId($id_category, $id_lang);
                    if ($privatesale) {
                        $category = new Category($data['privatesale']->id_category, $context->language->id);
                        $privatesale->sub_categories = $category->getSubCategories($context->language->id, true);
                        $data['privatesale'] = $privatesale;
                    }
                }
            }

            ////////////////////
            //    agepopop    //
            ////////////////////

            if (class_exists('PrestAppAgepopupCustomizationController')) {
                $agepopup = new PrestAppAgepopupCustomizationController();
                $data['agepopup'] = $agepopup->getDataProduct($product->id, $context);
            }

            ///////////////////////
            //    prestadrive    //
            ///////////////////////

            if (class_exists('PrestAppPrestadriveCustomizationController')) {
                $prestadrive = new PrestAppPrestadriveCustomizationController();
                $data['prestadrive'] = $prestadrive->getData($context, $product->id);
            }

            return $data;
        }
    }

    private static function getCombinationImagesById($id_product_attribute, $id_lang)
    {
        if (!Combination::isFeatureActive() || !$id_product_attribute) {
            return false;
        }

        $result = Db::getInstance()->executeS(
            '
                SELECT pai.`id_image`, pai.`id_product_attribute`, il.`legend`
                FROM `' . _DB_PREFIX_ . 'product_attribute_image` pai
                LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (il.`id_image` = pai.`id_image`)
                LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_image` = pai.`id_image`)
                WHERE pai.`id_product_attribute` = ' . (int) $id_product_attribute . ' AND il.`id_lang` = ' . (int) $id_lang . ' ORDER by i.`position`'
        );

        if (!$result) {
            return false;
        }

        return $result;
    }

    private static function moduleAdvancedPackIsPack($id_product)
    {
        if (class_exists('PrestAppAdvancedPackCustomizationController')) {
            $prestapp_advancedpack_customization_controller = new PrestAppAdvancedPackCustomizationController;
            if ($prestapp_advancedpack_customization_controller::isAdvancedPack($id_product)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

//fix 1.0.6 - add retro compatibility with the function in Tools class
function getCountry($address = null)
{
    $id_country = (int) Tools::getValue('id_country');
    if ($id_country && Validate::isInt($id_country)) {
        return (int) $id_country;
    } elseif (!$id_country && isset($address) && isset($address->id_country) && $address->id_country) {
        $id_country = (int) $address->id_country;
    } elseif (Configuration::get('PS_DETECT_COUNTRY') && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        preg_match('#(?<=-)\w\w|\w\w(?!-)#', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $array);
        if (is_array($array) && isset($array[0]) && Validate::isLanguageIsoCode($array[0])) {
            $id_country = (int) Country::getByIso($array[0], true);
        }
    }
    if (!isset($id_country) || !$id_country) {
        $id_country = (int) Configuration::get('PS_COUNTRY_DEFAULT');
    }
    return (int) $id_country;
}
