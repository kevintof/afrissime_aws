<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppOrderController
{
    /**
     * getOrderByOrderId - Return the order object from its id.
     *
     * @param Context $context
     *
     * @return array
     */
    public static function getOrderByOrderId($id_order, $context)
    {
        $order = new Order($id_order);

        if (!Validate::isLoadedObject($order)) {
            return 'ORDER_NOT_FOUND';
        } elseif ($order->id_customer && null == $context->customer) {
            return 'NOT_AUTHORIZED';
        } elseif ($order->id_customer && $order->id_customer != $context->customer->id) {
            return 'NOT_AUTHORIZED';
        } else {
            $ProductDetailObject = new OrderDetail();
            $CarrierDetailObject = new Carrier($order->id_carrier, (int) $context->language->id);
            $AddressDeliveryDetailObject = new Address($order->id_address_delivery);
            $AddressInvoiceDetailObject = new Address($order->id_address_invoice);
            $CurrencyDetailObject = new Currency($order->id_currency);
            $product_detail = $ProductDetailObject->getList($id_order);
            $order->products = $product_detail;
            $order->carrier = $CarrierDetailObject;
            $order->currency = $CurrencyDetailObject;
            $order->address_delivery = $AddressDeliveryDetailObject;
            $order->address_invoice = $AddressInvoiceDetailObject;
            $order->cart_rules = $order->getCartRules();
            $order->state = $order->getCurrentStateFull((int) $context->language->id);
            $order->history = $order->getHistory((int) $context->language->id, false, true);

            // if (class_exists('PrestAppMondialrelayModuleController')) {
            //     $mondialrelay = new PrestAppMondialrelayModuleController();
            //     $mondialrelay_trackinginformations = $mondialrelay->getTrackingInformations($order->id_carrier, $order->id_cart);

            //     if ($mondialrelay_trackinginformations) {
            //         $order->tracking_informations = $mondialrelay_trackinginformations;
            //     }
            // }

            if (!isset($order->tracking_informations)) {
                $order_shipping = $order->getShipping();

                if ($order_shipping) {
                    if (count($order_shipping) > 0) {
                        $order->tracking_informations = array(
                            'tracking_id' => $order_shipping[0]['tracking_number'],
                            'tracking_url' => $order_shipping[0]['url'],
                        );
                    }
                }
            }

            return $order;
        }
    }

    /**
     * getOrderByCartId - Return the order object from its cart id.
     *
     * @param Context $context
     *
     * @return array
     */
    public static function getOrderByCartId($id_cart, $context)
    {
        $order_id = Order::getOrderByCartId((int) ($id_cart));
        $order = new Order($order_id);

        if (!Validate::isLoadedObject($order)) {
            return 'ORDER_NOT_FOUND';
        } elseif ($order->id_customer && null == $context->customer) {
            return 'NOT_AUTHORIZED';
        } elseif ($order->id_customer && $order->id_customer != $context->customer->id) {
            return 'NOT_AUTHORIZED';
        } else {
            $ProductDetailObject = new OrderDetail();
            $product_detail = $ProductDetailObject->getList($order_id);
            $order->products = $product_detail;
            $order->address_delivery = new Address($order->id_address_delivery);
            $order->address_invoice = new Address($order->id_address_invoice);
            $order->carrier = new Carrier($order->id_carrier, (int) $context->language->id);
            $order->currency = new Currency($order->id_currency);
            $order->cart_rules = $order->getCartRules();
            $order->state = $order->getCurrentStateFull((int) $context->language->id);

            return $order;
        }
    }

    public static function getAnalyticsData($order, $context)
    {
        $parameters = Configuration::getMultiple(array('PS_LANG_DEFAULT'));
        $deliveryAddress = new Address(intval($order->id_address_delivery));
        $conversion_rate = 1;
        if ($order->id_currency != Configuration::get('PS_CURRENCY_DEFAULT')) {
            $currency = new Currency(intval($order->id_currency));
            $conversion_rate = floatval($currency->conversion_rate);
        }

        $trans = array(
            'id' => intval($order->id), // order ID - required
            'store' => htmlentities(Configuration::get('PS_SHOP_NAME')), // affiliation or store name
            'revenue' => Tools::ps_round(floatval($order->total_paid) / floatval($conversion_rate), 2), // total - required
            'tax' => '0', // tax
            'shipping' => Tools::ps_round(floatval($order->total_shipping) / floatval($conversion_rate), 2), // shipping
            'city' => addslashes($deliveryAddress->city), // city
            'state' => '', // state or province
            'country' => addslashes($deliveryAddress->country), // country
        );

        $products = $order->getProducts();
        foreach ($products as $product) {
            $category = Db::getInstance()->getRow('
                SELECT name FROM `' . _DB_PREFIX_ . 'category_lang` , ' . _DB_PREFIX_ . 'product
                WHERE `id_product` = ' . intval($product['product_id']) . ' AND `id_category_default` = `id_category`
                AND `id_lang` = ' . intval($parameters['PS_LANG_DEFAULT']));

            $items[] = array(
                'id' => intval($order->id), // order ID - required
                'sku' => addslashes($product['product_id']), // SKU/code - required
                'name' => addslashes($product['product_name']), // product name
                'category' => addslashes($category['name']), // category or variation
                'price' => Tools::ps_round(floatval($product['product_price_wt']) / floatval($conversion_rate), 2), // unit price - required
                'quantity' => addslashes(intval($product['product_quantity'])), //quantity - required
            );
        }

        return array(
            'transaction' => $trans,
            'items' => $items,
        );
    }

    /**
     * getOrderHistory - Return the list of orders.
     *
     * @param Context $context
     *
     * @return array
     */
    public static function getOrderHistory($context)
    {
        if ($orders = Order::getCustomerOrders($context->customer->id)) {
            foreach ($orders as &$order) {
                $myOrder = new Order((int) $order['id_order']);
                if (Validate::isLoadedObject($myOrder)) {
                    $order['virtual'] = $myOrder->isVirtual(false);
                }
            }
        }

        return $orders;
    }

    /**
     * getCurrency - Return currency from id.
     *
     * @param Context $context
     *
     * @return array
     */
    public static function getCurrency($id, $context)
    {
        $currency = new Currency($id, $context->language->id, $context->shop->id);

        return $currency;
    }

    public static function logOrder($id_order, $context)
    {

        if (isset($id_order) && (int) $id_order) {
            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'prestapp_orders` (
                `id_prestapp_order` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `id_ps_order` INT( 11 ) UNSIGNED NOT NULL,
                `date_add` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id_prestapp_order`)
              ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

            if (Db::getInstance()->Execute($sql)) {
                $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'prestapp_orders` (
                    `id_ps_order`
                    ) values (' . (int) ($id_order) . ')';

                Db::getInstance()->Execute($sql);
            }
        }

        return;
    }

    public static function getOrderCountByCustomerID($id_customer)
    {
        if ($orders = Order::getCustomerOrders($id_customer)) {
            return count($orders);
        } else {
            return 0;
        }
    }
}
