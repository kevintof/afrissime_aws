<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('allinone_rewards') && Module::isEnabled('allinone_rewards')) {

    include_once _PS_MODULE_DIR_ . 'allinone_rewards/allinone_rewards.php';
    // include_once(_PS_MODULE_DIR_.'allinone_rewards/models/RewardsSpon.php');

    class PrestAppAllInOneRewardsCustomizationController extends allinone_rewards
    {
        public function _construct()
        {

        }

        public function getData()
        {
            $data = new StdClass();
            $data->name = 'all_in_one_rewards';
            $data->sponsorship_active = $this->isSponsorshipActive();
            return $data;
        }

        public function getCustomerData($context)
        {
            $rewards_sponsorship = new allinone_rewards();

            $id_template = (int)MyConf::getIdTemplate('sponsorship', $context->customer->id);
            $orderQuantityS = (int) MyConf::get('RSPONSORSHIP_ORDER_QUANTITY_S', null, $id_template);
            $stats = $context->customer->getStats();
            $canSendInvitations = false;
            if ((int) ($stats['nb_orders']) >= $orderQuantityS) {
                $canSendInvitations = true;
            }

            $data['rules'] = MyConf::get('RSPONSORSHIP_RULES_TXT', $context->language->id, (int) MyConf::getIdTemplate('sponsorship', $context->customer->id));
            $data['orderQuantityS'] = $orderQuantityS;
            $data['canSendInvitations'] = $canSendInvitations;
            $data['pendingFriends'] = RewardsSponsorshipModel::getSponsorFriends((int) $context->customer->id, 'pending');
            $data['subscribeFriends'] = RewardsSponsorshipModel::getSponsorFriends((int) $context->customer->id, 'subscribed');
            $data['discount_gc'] = $rewards_sponsorship->getDiscountReadyForDisplay((int) MyConf::get('RSPONSORSHIP_DISCOUNT_TYPE_GC', null, $id_template), (int) MyConf::get('RSPONSORSHIP_FREESHIPPING_GC', null, $id_template), (float) MyConf::get('RSPONSORSHIP_VOUCHER_VALUE_GC_' . (int) $context->currency->id, null, $id_template));

            return $data;
        }

        public function getCustomerCredits($context)
        {
            $rewards_sponsorship = new allinone_rewards();

            $id_template = (int) MyConf::getIdTemplate('core', $context->customer->id);

            $totals = RewardsModel::getAllTotalsByCustomer((int) $context->customer->id);
            $totalGlobal = isset($totals['total']) ? (float) $totals['total'] : 0;
            $totalConverted = isset($totals[RewardsStateModel::getConvertId()]) ? (float) $totals[RewardsStateModel::getConvertId()] : 0;
            $totalAvailable = isset($totals[RewardsStateModel::getValidationId()]) ? (float) $totals[RewardsStateModel::getValidationId()] : 0;
            $totalPending = (isset($totals[RewardsStateModel::getDefaultId()]) ? (float) $totals[RewardsStateModel::getDefaultId()] : 0) + (isset($totals[RewardsStateModel::getReturnPeriodId()]) ? $totals[RewardsStateModel::getReturnPeriodId()] : 0);
            $totalWaitingPayment = isset($totals[RewardsStateModel::getWaitingPaymentId()]) ? (float) $totals[RewardsStateModel::getWaitingPaymentId()] : 0;
            $totalPaid = isset($totals[RewardsStateModel::getPaidId()]) ? (float) $totals[RewardsStateModel::getPaidId()] : 0;
            $totalForPaymentDefaultCurrency = round($totalAvailable * MyConf::get('REWARDS_PAYMENT_RATIO', null, $id_template) / 100, 2);
            $currency = Currency::getCurrency((int) $this->context->currency->id);
            $totalAvailableUserCurrency = Tools::convertPrice($totalAvailable, $currency);
            $voucherMininum = (float) MyConf::get('REWARDS_VOUCHER_MIN_VALUE_' . (int) $context->currency->id, null, $id_template) > 0 ? (float) MyConf::get('REWARDS_VOUCHER_MIN_VALUE_' . (int) $context->currency->id, null, $id_template) : 0;
            $paymentMininum = (float) MyConf::get('REWARDS_PAYMENT_MIN_VALUE_' . (int) $context->currency->id, null, $id_template) > 0 ? (float) MyConf::get('REWARDS_PAYMENT_MIN_VALUE_' . (int) $context->currency->id, null, $id_template) : 0;

            $voucherAllowed = RewardsModel::isCustomerAllowedForVoucher((int) $context->customer->id);
            $paymentAllowed = RewardsModel::isCustomerAllowedForPayment((int) $context->customer->id);

            if ($voucherAllowed && Tools::getValue('transform-credits') == 'true' && $totalAvailableUserCurrency >= $voucherMininum) {
                RewardsModel::createDiscount($totalAvailable);
            }

            $link = $context->link->getModuleLink('allinone_rewards', 'rewards', array(), true);
            $rewards = RewardsModel::getAllByIdCustomer((int) $context->customer->id);
            $displayrewards = RewardsModel::getAllByIdCustomer((int) $context->customer->id, false, false, true, ((int) (Tools::getValue('n')) > 0 ? (int) (Tools::getValue('n')) : 10), ((int) (Tools::getValue('p')) > 0 ? (int) (Tools::getValue('p')) : 1), $context->currency->id);

            $data['return_days'] = (Configuration::get('REWARDS_WAIT_RETURN_PERIOD') && Configuration::get('PS_ORDER_RETURN') && (int) Configuration::get('PS_ORDER_RETURN_NB_DAYS') > 0) ? (int) Configuration::get('PS_ORDER_RETURN_NB_DAYS') : 0;
            $data['rewards_duration'] = (int) Configuration::get('REWARDS_DURATION');
            $data['rewards'] = $rewards;
            $data['displayrewards'] = $displayrewards;
            $data['pagination_link'] = $link . (strpos($link, '?') !== false ? '&' : '?');
            $data['totalGlobal'] = round(Tools::convertPrice($totalGlobal, $currency), 2);
            $data['totalConverted'] = round(Tools::convertPrice($totalConverted, $currency), 2);
            $data['totalAvailable'] = round(Tools::convertPrice($totalAvailable, $currency), 2);
            $data['totalPending'] = round(Tools::convertPrice($totalPending, $currency), 2);
            $data['totalWaitingPayment'] = round(Tools::convertPrice($totalWaitingPayment, $currency), 2);
            $data['totalPaid'] = round(Tools::convertPrice($totalPaid, $currency), 2);
            $data['totalForPaymentDefaultCurrency'] = $totalForPaymentDefaultCurrency;
            $data['payment_currency'] = Configuration::get('PS_CURRENCY_DEFAULT');
            $data['voucher_min'] = $voucherAllowed ? $voucherMininum : 0;
            $data['voucher_allowed'] = $voucherAllowed;
            $data['voucher_button_allowed'] = $voucherAllowed && $totalAvailableUserCurrency >= $voucherMininum && $totalAvailableUserCurrency > 0;
            $data['payment_min'] = $paymentAllowed ? $paymentMininum : 0;
            $data['payment_allowed'] = $paymentAllowed;
            $data['payment_button_allowed'] = $paymentAllowed && $totalAvailableUserCurrency >= $paymentMininum && $totalForPaymentDefaultCurrency > 0;
            $data['payment_txt'] = MyConf::get('REWARDS_PAYMENT_TXT', (int) $context->language->id, $id_template);
            $data['general_txt'] = MyConf::get('REWARDS_GENERAL_TXT', (int) $context->language->id, $id_template);
            $data['payment_invoice'] = (int) MyConf::get('REWARDS_PAYMENT_INVOICE', null, $id_template);
            $data['page'] = ((int) (Tools::getValue('p')) > 0 ? (int) (Tools::getValue('p')) : 1);
            $data['nbpagination'] = ((int) (Tools::getValue('n') > 0) ? (int) (Tools::getValue('n')) : 10);
            $data['nArray'] = array(10, 20, 50);
            $data['max_page'] = floor(sizeof($rewards) / ((int) (Tools::getValue('n') > 0) ? (int) (Tools::getValue('n')) : 10));

            return $data;
        }

        public function sendInvitation($emails, $context)
        {

            $rewards_sponsorship = new allinone_rewards();

            $emails = json_decode($emails);

            if ($emails) {

                $is_error = false;

                foreach ($emails as $key => $friendEmail) {
                    if (empty($friendEmail) || !Validate::isEmail($friendEmail)) {
                        $is_error = true;
                    }
                }

                if ($is_error) {
                    return 'INVALID';
                } else {

                    $mails_exists = [];

                    foreach ($emails as $key => $friendEmail) {
                        if (RewardsSponsorshipModel::isEmailExists($friendEmail) || Customer::customerExists($friendEmail)) {
                            array_push($mails_exists, $friendEmail);
                        } else {
                            $sponsorship = new RewardsSponsorshipModel();
                            $sponsorship->id_sponsor = (int) $context->customer->id;
                            $sponsorship->channel = 1;
                            $sponsorship->email = $friendEmail;

                            if ($sponsorship->save()) {

                                $discount_gc = $rewards_sponsorship->getDiscountReadyForDisplay((int) MyConf::get('RSPONSORSHIP_DISCOUNT_TYPE_GC', null, $id_template), (int) MyConf::get('RSPONSORSHIP_FREESHIPPING_GC', null, $id_template), (float) MyConf::get('RSPONSORSHIP_VOUCHER_VALUE_GC_' . (int) $context->currency->id, null, $id_template));
                                $id_template = (int) MyConf::getIdTemplate('sponsorship', $context->customer->id);
                                $template = 'sponsorship-invitation-novoucher';

                                if ((int) MyConf::get('RSPONSORSHIP_DISCOUNT_GC', null, $id_template) == 1) {

                                    if ((int) MyConf::get('RSPONSORSHIP_DISCOUNT_TYPE_GC', null, $id_template) != 0) {
                                        $template = 'sponsorship-invitation';
                                    } else {
                                        $template = 'sponsorship-invitation-freeshipping';
                                    }
                                }

                                $vars = array(

                                    '{email}' => $context->customer->email,

                                    '{lastname}' => $context->customer->lastname,

                                    '{firstname}' => $context->customer->firstname,

                                    '{email_friend}' => $friendEmail,

                                    '{link}' => $context->link->getPageLink('index', true, $context->language->id, 's=' . $sponsorship->getSponsorshipMailLink()),

                                    '{nb_discount}' => (int) MyConf::get('RSPONSORSHIP_QUANTITY_GC', null, $id_template),

                                    '{discount}' => $discount_gc);

                                $rewards_sponsorship->sendMail((int) $context->language->id, $template, $rewards_sponsorship->getL('invitation'), $vars, $friendEmail, $friendFirstName . ' ' . $friendLastName);
                            }
                        }
                    }

                    $data['exists'] = $mails_exists;
                    $data['status'] = 'OK';

                    return $data;
                }
            } else {
                return 'UNKNOWN';
            }
        }

        public function revive($id_sponsorship, $context)
        {
            $rewards_sponsorship = new allinone_rewards();
            $sponsorship = new RewardsSponsorshipModel((int) $id_sponsorship);

            $discount_gc = $rewards_sponsorship->getDiscountReadyForDisplay((int) MyConf::get('RSPONSORSHIP_DISCOUNT_TYPE_GC', null, $id_template), (int) MyConf::get('RSPONSORSHIP_FREESHIPPING_GC', null, $id_template), (float) MyConf::get('RSPONSORSHIP_VOUCHER_VALUE_GC_' . (int) $context->currency->id, null, $id_template));
            $id_template = (int) MyConf::getIdTemplate('sponsorship', $context->customer->id);
            $template = 'sponsorship-invitation-novoucher';

            if ((int) MyConf::get('RSPONSORSHIP_DISCOUNT_GC', null, $id_template) == 1) {

                if ((int) MyConf::get('RSPONSORSHIP_DISCOUNT_TYPE_GC', null, $id_template) != 0) {
                    $template = 'sponsorship-invitation';
                } else {
                    $template = 'sponsorship-invitation-freeshipping';
                }
            }

            $vars = array(

                '{email}' => $context->customer->email,

                '{lastname}' => $context->customer->lastname,

                '{firstname}' => $context->customer->firstname,

                '{email_friend}' => $sponsorship->email,

                '{link}' => $context->link->getPageLink('index', true, $context->language->id, 's=' . $sponsorship->getSponsorshipMailLink()),

                '{nb_discount}' => (int) MyConf::get('RSPONSORSHIP_QUANTITY_GC', null, $id_template),

                '{discount}' => $discount_gc,

            );

            $sponsorship->save();
            $rewards_sponsorship->sendMail((int) $context->language->id, $template, $rewards_sponsorship->getL('invitation'), $vars, $sponsorship->email, $sponsorship->firstname . ' ' . $sponsorship->lastname);

            return 'OK';
        }

        public function askVoucher($context)
        {

            $id_template = (int) MyConf::getIdTemplate('core', $context->customer->id);

            $totals = RewardsModel::getAllTotalsByCustomer((int) $context->customer->id);
            $currency = Currency::getCurrency((int) $context->currency->id);
            $totalAvailable = isset($totals[RewardsStateModel::getValidationId()]) ? (float) $totals[RewardsStateModel::getValidationId()] : 0;
            $voucherAllowed = RewardsModel::isCustomerAllowedForVoucher((int) $context->customer->id);
            $totalAvailableUserCurrency = Tools::convertPrice($totalAvailable, $currency);
            $voucherMininum = (float) MyConf::get('REWARDS_VOUCHER_MIN_VALUE_' . (int) $context->currency->id, null, $id_template) > 0 ? (float) MyConf::get('REWARDS_VOUCHER_MIN_VALUE_' . (int) $context->currency->id, null, $id_template) : 0;

            if ($voucherAllowed && $totalAvailableUserCurrency >= $voucherMininum && $totalAvailableUserCurrency > 0) {
                RewardsModel::createDiscount($totalAvailable);
                return 'OK';
            } else {
                return 'ERROR';
            }
        }

        public function isSponsorshipActive()
        {
			$allinone_rewards = new allinone_rewards();
            $rewards_sponsorship = new RewardsSponsorshipPlugin($allinone_rewards);
            return $rewards_sponsorship->isActive();
        }
    }
}
