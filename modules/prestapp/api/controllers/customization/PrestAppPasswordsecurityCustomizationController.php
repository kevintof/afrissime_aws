<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('passwordsecurity') && Module::isEnabled('passwordsecurity')) {

    class PrestAppPasswordsecurityCustomizationController
    {
        protected $passwordsecurity;

        public function __construct()
        {
            $this->passwordsecurity = Module::getInstanceByName('passwordsecurity');
        }

        public function getByEmail($email, $passwd = null, $ignore_guest = true)
        {

            if (!Validate::isEmail($email) || ($passwd && !Validate::isPasswd($passwd))) {
                return false;
            }

            $result = Db::getInstance()->getRow('
            SELECT *
            FROM `' . _DB_PREFIX_ . 'customer`
            WHERE `email` = \'' . pSQL($email) . '\'
            ' . Shop::addSqlRestriction(Shop::SHARE_CUSTOMER) . '
            AND `deleted` = 0
            ' . ($ignore_guest ? ' AND `is_guest` = 0' : ''));

            if (!$result) {
                return false;
            }

            if (isset($passwd)) {
                if (strpos($result['passwd'], ':') !== false) {
                    list($hash, $salt) = explode(':', $result['passwd']);
                } else {
                    $salt = '';
                    $hash = $result['passwd'];
                }

                // md5 or sha1?
                if (Tools::strlen($hash) === 32) {
                    $newHash = md5($salt . _COOKIE_KEY_ . $passwd);
                } else {
                    $newHash = sha1($salt . _COOKIE_KEY_ . $passwd);
                }
                if ($newHash != $hash) {
                    return false;
                }

                // Convert password to sha1
                if (Tools::strlen($hash) === 32) {
                    $salt = mt_rand(100000000, 999999999);
                    Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'customer`
                    SET passwd = "' . pSql(sha1($salt . _COOKIE_KEY_ . $passwd) . ':' . $salt) . '"
                    WHERE id_customer = ' . (int) $result['id_customer']);
                }
            }

            foreach ($result as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->{$key} = $value;
                }
            }

            return $result;
        }
    }
}
