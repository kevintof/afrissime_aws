<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('agepopup') && Module::isEnabled('agepopup')) {

    class PrestAppAgepopupCustomizationController
    {
        protected $agepopup;

        public function __construct()
        {
            $this->agepopup = Module::getInstanceByName('agepopup');
        }

        public function getDataCategory($id_category, $context)
        {
            $id_shop = (int) $context->shop->id;
            $agecontrol = AgepopupClass::getByIdShop($id_shop);
            $agecontrol = new AgepopupClass((int) $agecontrol->id, $context->language->id);
            $catarray = Configuration::get('age_cat');
            $replace = array('a:1:{', '}', ':', '\n');
            $result = str_replace($replace, '', trim($catarray));
            $finish = str_replace(';', ',', trim($result));
            $idcat = $id_category;
            $regcat = 'i' . $idcat . ',';
            $display = strpos($finish, $regcat);

            if ($display === false) {
                $showcat = 0;
            } else {
                $showcat = 1;
            }

            if (!Validate::isLoadedObject($context->customer) || $agecontrol->age_login == '1') {
                if ($agecontrol->age_all == '0' && $showcat == '1') {
                    return array(
                        'title' => $agecontrol->age_title,
                        'text' => html_entity_decode($agecontrol->age_text),
                    );
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        public function getDataProduct($id_product, $context)
        {

            $product = new Product($id_product, true, $context->language->id, $context->shop->id);
            $idcat = $product->id_category_default;
            $catarray = Configuration::get('age_cat');
            $replace = array('a:1:{', '}', ':', '\n');
            $result = str_replace($replace, '', trim($catarray));
            $finish = str_replace(';', ',', trim($result));
            $regcat = 'i' . $idcat . ',';
            $display = strpos($finish, $regcat);

            if ($display === false) {
                $showcat = 0;
            } else {
                $showcat = 1;
            }

            $id_shop = (int) $context->shop->id;
            $agecontrol = AgepopupClass::getByIdShop($id_shop);
            $agecontrol = new AgepopupClass((int) $agecontrol->id, $context->language->id);

            if (!Validate::isLoadedObject($context->customer) || $agecontrol->age_login == '1') {
                if ($agecontrol->age_all == '0' && $showcat == '1') {
                    return array(
                        'title' => $agecontrol->age_title,
                        'text' => html_entity_decode($agecontrol->age_text),
                    );
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}
