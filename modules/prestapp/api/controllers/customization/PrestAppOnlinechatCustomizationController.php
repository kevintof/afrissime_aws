<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('onlinechat') && Module::isEnabled('onlinechat')) {

    class PrestAppOnlinechatCustomizationController
    {
        protected $onlinechat;

        public function __construct()
        {
            $this->onlinechat = Module::getInstanceByName('onlinechat');
        }

        public function getData()
        {
            $data = new StdClass();
            $data->name = 'onlinechat';
            $data->onlinechat_active = true;

            return $data;
        }

        public function getStatus($context)
        {

            $customer = null;
            if (isset($context->customer->lastname) && isset($context->customer->firstname)
                && isset($context->customer->email) && isset($context->customer->id)
                && isset($context->cart->id_guest)) {
                $username = Tools::strtoupper($context->customer->lastname) . ' ';
                $username .= Tools::ucfirst($context->customer->firstname);
                $customer = array(
                    "id_customer" => $context->customer->id,
                    "id_guest" => $context->cart->id_guest,
                    "name" => $username,
                    "email" => $context->customer->email,
                );
            }

            $config_vars = $this->getConfigFormValuesFiltered(
                $context->cart->id_shop,
                $context->cart->id_shop_group,
                $context->cart->id_lang
            );

            $status = 'deactivated';

            if (Ochat::isEmployeeConnected()) {
                $max_window = Configuration::get(
                    'OCHAT_MAX_ONLINE_CHAT_WINDOW_IN_FO',
                    null,
                    $context->cart->id_shop_group,
                    $context->cart->id_shop
                );

                if (JsonHandler::hasOpenThread($context->cart->id_customer, $context->cart->id_guest)) {
                    $status = 'online';
                } elseif (JsonHandler::getNbOpenThread() < $max_window) {
                    $status = 'online';
                } else {
                    if ($config_vars['OCHAT_IF_OFFLINE_ACTIVATED']) {
                        $status = 'offline';
                    }
                }
            } else {
                if ($config_vars['OCHAT_IF_OFFLINE_ACTIVATED']) {
                    $status = 'offline';
                }
            }

            return array(
                'status' => $status,
                'vars' => $config_vars,
            );
        }

        public function sendOfflineMessage($email, $name, $phone, $message, $context)
        {

            $form = array();

            if ($name) {
                $form['OCHAT_FORM_NAME'] = $name;
            }

            if ($email) {
                $form['OCHAT_FORM_EMAIL'] = $email;
            }

            if ($phone) {
                $form['OCHAT_FORM_PHONE'] = $phone;
            }

            if ($message) {
                $form['OCHAT_FORM_MESSAGE'] = $message;
            }

            $form_data = $this->onlinechat->formToDataTemplate($form);

            $from = null;

            if (isset($form['OCHAT_FORM_EMAIL'])) {
                $from = $form['OCHAT_FORM_EMAIL'];
            }

            $mail_sent = $this->onlinechat->sendMail($from, $context->cart->id_lang, $form_data);

            return $mail_sent == 1 ? true : false;
        }

        protected function getConfigFormValuesFiltered($id_shop, $id_shop_group, $id_lang)
        {
            return array(
                'OCHAT_INSTALL' => Configuration::get('OCHAT_INSTALL', null, $id_shop_group, $id_shop),
                'OCHAT_OPERATOR_NAME' => Configuration::get('OCHAT_OPERATOR_NAME', null, $id_shop_group, $id_shop),
                'OCHAT_POSITION' => Configuration::get('OCHAT_POSITION', null, $id_shop_group, $id_shop),
                'OCHAT_BIP_SOUND' => Configuration::get('OCHAT_BIP_SOUND', null, $id_shop_group, $id_shop),
                'OCHAT_BIP_VOLUME' => Configuration::get('OCHAT_BIP_VOLUME', null, $id_shop_group, $id_shop),
                'OCHAT_EMAIL_TO_NOTIFY' => Configuration::get('OCHAT_EMAIL_TO_NOTIFY', null, $id_shop_group, $id_shop),
                'OCHAT_ONLINE_HEADER_TEXT' => Configuration::get(
                    'OCHAT_ONLINE_HEADER_TEXT',
                    $id_lang,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_ONLINE_WELCOME_TEXT' => Configuration::get(
                    'OCHAT_ONLINE_WELCOME_TEXT',
                    $id_lang,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_OFFLINE_HEADER_TEXT' => Configuration::get(
                    'OCHAT_OFFLINE_HEADER_TEXT',
                    $id_lang,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_OFFLINE_WELCOME_TEXT' => Configuration::get(
                    'OCHAT_OFFLINE_WELCOME_TEXT',
                    $id_lang,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_FORM_NAME' => Configuration::get('OCHAT_FORM_NAME', null, $id_shop_group, $id_shop),
                'OCHAT_FORM_EMAIL' => Configuration::get('OCHAT_FORM_EMAIL', null, $id_shop_group, $id_shop),
                'OCHAT_FORM_PHONE' => Configuration::get('OCHAT_FORM_PHONE', null, $id_shop_group, $id_shop),
                'OCHAT_FORM_MESSAGE' => Configuration::get('OCHAT_FORM_MESSAGE', null, $id_shop_group, $id_shop),
                'OCHAT_IF_OFFLINE_ACTIVATED' => Configuration::get(
                    'OCHAT_IF_OFFLINE_ACTIVATED',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_MAX_ONLINE_CHAT_WINDOW_IN_FO' => Configuration::get(
                    'OCHAT_MAX_ONLINE_CHAT_WINDOW_IN_FO',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_TIME_DELAY' => Configuration::get('OCHAT_TIME_DELAY', null, $id_shop_group, $id_shop),
                'OCHAT_OFFLINE_HEADER_BG_COLOR' => Configuration::get(
                    'OCHAT_OFFLINE_HEADER_BG_COLOR',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_OFFLINE_HEADER_TEXT_COLOR' => Configuration::get(
                    'OCHAT_OFFLINE_HEADER_TEXT_COLOR',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_ONLINE_HEADER_BG_COLOR' => Configuration::get(
                    'OCHAT_ONLINE_HEADER_BG_COLOR',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_ONLINE_HEADER_TEXT_COLOR' => Configuration::get(
                    'OCHAT_ONLINE_HEADER_TEXT_COLOR',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_OPERATOR_CHAT_BOX_BG_COLOR' => Configuration::get(
                    'OCHAT_OPERATOR_CHAT_BOX_BG_COLOR',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_OPERATOR_CHAT_BOX_TEXT_COLOR' => Configuration::get(
                    'OCHAT_OPERATOR_CHAT_BOX_TEXT_COLOR',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_CLIENT_CHAT_BOX_BG_COLOR' => Configuration::get(
                    'OCHAT_CLIENT_CHAT_BOX_BG_COLOR',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
                'OCHAT_CLIENT_CHAT_BOX_TEXT_COLOR' => Configuration::get(
                    'OCHAT_CLIENT_CHAT_BOX_TEXT_COLOR',
                    null,
                    $id_shop_group,
                    $id_shop
                ),
            );
        }

        public function checkBeforeCreateThread($context)
        {
            $id_shop = (int) $context->shop->id;
            return $this->onlinechat->checkBeforeCreateThread($id_shop);
        }

        public function createThread($content, $context)
        {

            $params = array(
                'id_shop' => (int) $context->shop->id,
                'id_lang' => $context->language->id,
                'id_employee' => 0,
            );

            if (isset($context->customer) && isset($context->customer->id) && $context->customer->id) {
                $params['id_customer'] = $context->customer->id;
                $params['customer_name'] = $context->customer->firstname . ' ' . $context->customer->lastname;
            } else {
                $params['id_customer'] = null;
                $params['customer_name'] = "";
            }

            if (isset($context->cart) && isset($context->cart->id_guest)) {
                $params['id_guest'] = (int) $context->cart->id_guest;
            } else {
                $params['id_guest'] = 0;
            }

            if (!class_exists('JsonHandler')) {
                include_once dirname(__FILE__) . '/libraries/JsonHandler.php';
            }

            $jh = new JsonHandler();

            $thread = $jh->createThread($params['id_lang'], $params['id_shop'], $params['id_customer'], $params['id_guest'], $params['id_employee'], $params['customer_name']);

            if ($thread) {
                $decoded = json_decode($thread, true);

                $posted = $this->postMessage($decoded['thread'], $content, $context);

                return array(
                    'thread' => $decoded['thread'],
                    'message' => json_decode($posted),
                );
            }

            return array(
                'thread' => $thread,
            );
        }

        public function postMessage($thread, $content, $context)
        {
            if (!class_exists('JsonHandler')) {
                include_once dirname(__FILE__) . '/libraries/JsonHandler.php';
            }

            $jh = new JsonHandler();

            if (isset($context->cart) && isset($context->cart->id_guest)) {
                $id_guest = (int) $context->cart->id_guest;
            } else {
                $id_guest = 0;
            }

            if ($context->customer == null || $context->customer->id == null) {
                $message = array(
                    'message_content' => $content,
                    'id_guest' => $id_guest,
                );
            } else {
                $message = array(
                    'message_content' => $content,
                    'id_customer' => $context->customer->id,
                );
            }

            $posted = $jh->postMessage($thread, $message);

            return $posted;
        }

        public function getEmployeesLastMessages($thread, $message_key, $context)
        {
            if (!class_exists('JsonHandler')) {
                include_once dirname(__FILE__) . '/libraries/JsonHandler.php';
            }

            $jh = new JsonHandler();

            if (isset($context->cart) && isset($context->cart->id_guest)) {
                $id_guest = (int) $context->cart->id_guest;
            } else {
                $id_guest = 0;
            }

            if ($context->customer == null || $context->customer->id == null) {
                $id_customer = null;
            } else {
                $id_customer = $context->customer->id;
            }

            $employeesMessages = $jh->getEmployeeLastMessages($thread, $message_key, $id_customer, $id_guest);

            return $employeesMessages;
        }
    }
}
