<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('prestadrive') && Module::isEnabled('prestadrive')) {

    class PrestAppPrestadriveCustomizationController
    {
        protected $prestadrive;

        public function __construct()
        {
            $this->prestadrive = Module::getInstanceByName('prestadrive');
        }

        public function getData($context, $id_product)
        {
            $get_values = $this->getConfigFormValues();
            $selectyourcarrier = $get_values['selectyourcarrier'];

            if (preg_match('#,[0-9]+,#', $selectyourcarrier, $matches, PREG_OFFSET_CAPTURE)) {

                $conf_values = array();

                foreach (array_keys($get_values) as $key) {
                    if (preg_match('#^conf[a-z]+#', $key)) {
                        ${$key} = $get_values[$key];
                        $conf_values[$key] = ${$key};
                    }
                }
                $daydrivenow = date("w");
                $daydrivenowheure = date("Hm");

                foreach (array_keys($get_values) as $key) {

                    if (Tools::strlen(($get_values[$key])) > 4) {
                        ${$key} = $get_values[$key];

                        if (preg_match('#^drive([a-z]+)#', $key, $matchesdrive, PREG_OFFSET_CAPTURE)) {

                            $posttimevalue = array();
                            if ($matchesdrive[1][0] == 'lundi') {
                                $daydrive = 1;
                            } elseif ($matchesdrive[1][0] == 'mardi') {
                                $daydrive = 2;
                            } elseif ($matchesdrive[1][0] == 'mercredi') {
                                $daydrive = 3;
                            } elseif ($matchesdrive[1][0] == 'jeudi') {
                                $daydrive = 4;
                            } elseif ($matchesdrive[1][0] == 'vendredi') {
                                $daydrive = 5;
                            } elseif ($matchesdrive[1][0] == 'samedi') {
                                $daydrive = 6;
                            } elseif ($matchesdrive[1][0] == 'dimanche') {
                                $daydrive = 0;
                            }

                            if (preg_match('#((.*;.*))#', ${$key}, $matches, PREG_OFFSET_CAPTURE)) {

                                $multihour = explode(";", $matches[0][0]);
                                $calcultimevalues = array();
                                foreach ($multihour as $key_ => $multihour_) {
                                    $calcultimevalues[] = $this->calcultime($multihour[$key_], $confintervalfordrive);

                                }

                                foreach ($calcultimevalues as $calcultimevalues_) {

                                    foreach ($calcultimevalues_ as $calcultimevalues__) {

                                        if ($daydrivenow == $daydrive && ceil(str_replace(":", "", $calcultimevalues__)) > ceil($daydrivenowheure)) {
                                            $posttimevalue[] = $calcultimevalues__;

                                        } else if ($daydrivenow != $daydrive) {
                                            $posttimevalue[] = $calcultimevalues__;
                                        }
                                    }
                                }

                            } else {

                                $calcultimevalues = $this->calcultime(${$key}, $confintervalfordrive);

                                foreach ($calcultimevalues as $calcultimevalues_) {

                                    if ($daydrivenow == $daydrive && ceil(str_replace(":", "", $calcultimevalues_)) > ceil($daydrivenowheure)) {
                                        $posttimevalue[] = $calcultimevalues_;

                                    } else if ($daydrivenow != $daydrive) {
                                        $posttimevalue[] = $calcultimevalues_;
                                    }
                                }

                            }
                            $aposttimevalue[$daydrive] = $posttimevalue;
                            $posttimevalue = array();
                        }
                    }
                }

                $values = array();

                $customer_mail = $context->customer->email;
                $customer_passwd = $context->customer->passwd;
                $customer_secure_key = $context->customer->secure_key;
                $customer_logged = $context->customer->logged;
                $securedrive = md5('6s' . $customer_mail . $customer_passwd . $customer_secure_key . $customer_logged);

                $values['wdrive'] = $get_values['wdrive'];
                $values['wselect'] = $get_values['wselect'];
                $values['wdate'] = $get_values['wdate'];
                $values['aposttimevalue'] = $aposttimevalue;
                $values['prestadrive_controlcarrier'] = Configuration::get('prestadrive_controlcarrier');
                $values['prestadrive_forcedrive'] = Configuration::get('prestadrive_forcedrive');
                $values['customer_mail'] = $customer_mail;
                $values['customer_passwd'] = $customer_passwd;
                $values['customer_secure_key'] = $customer_secure_key;
                $values['customer_logged'] = $customer_logged;
                $values['securedrive'] = $securedrive;
                $values['carriers'] = explode(",", $selectyourcarrier);

                if ($id_product) {
                    $values['add_to_cart'] = $this->getSupplierState($context, $id_product);
                }

                return $values;
            }
        }

        public function getSupplierState($context, $id_product)
        {
            if ($id_product) {
                $product = new Product($id_product);

                $req = "SELECT etat FROM " . _DB_PREFIX_ . "supplier_etat WHERE id_supplier = " . $product->id_supplier;
                $res = Db::getInstance()->getRow($req);

                $supplier_state = $res['etat'];

                if (!$supplier_state) {
                    return false;
                }

                $cart_values = $this->getCartValues($context->cart->id);

                if ($cart_values) {
                    if ($cart_values['jour_drive_time'] == 'Dimanche') {
                        $j = 0;
                    } else if ($cart_values['jour_drive_time'] == 'Lundi') {
                        $j = 1;
                    } else if ($cart_values['jour_drive_time'] == 'Mardi') {
                        $j = 2;
                    } else if ($cart_values['jour_drive_time'] == 'Mercredi') {
                        $j = 3;
                    } else if ($cart_values['jour_drive_time'] == 'Jeudi') {
                        $j = 4;
                    } else if ($cart_values['jour_drive_time'] == 'Vendredi') {
                        $j = 5;
                    } else if ($cart_values['jour_drive_time'] == 'Samedi') {
                        $j = 6;
                    }

                    $req_date_full = "SELECT day0,day1,day2,day3,day4,day5,day6 FROM " . _DB_PREFIX_ . "supplier_etat WHERE id_supplier = " . $product->id_supplier;
                    $res_date_full = Db::getInstance()->getRow($req_date_full);

                    $req_date = "SELECT day" . $j . " FROM " . _DB_PREFIX_ . "supplier_etat WHERE id_supplier = " . $product->id_supplier;
                    $res_date = Db::getInstance()->getRow($req_date);
                    $horaire = $res_date['day' . $j];

                    list($horaire_deb, $horaire_fin) = explode("-", $horaire);
                    list($horaire_deb_hh, $horaire_deb_mm) = explode(":", $horaire_deb);
                    list($horaire_fin_hh, $horaire_fin_mm) = explode(":", $horaire_fin);
                    list($horaire_drive_hh, $horaire_drive_mm) = explode(":", $cart_values['hour_drive_time']);

                    //On convertis les horaires en datetime pour les calcules
                    $datetime_deb = mktime($horaire_deb_hh, $horaire_deb_mm, 0, 01, 01, 2001);
                    $datetime_fin = mktime($horaire_fin_hh, $horaire_fin_mm, 0, 01, 01, 2001);
                    $datetime_drive = mktime($horaire_drive_hh, $horaire_drive_mm, 0, 01, 01, 2001);

                    if ($datetime_drive >= $datetime_deb && $datetime_drive <= $datetime_fin) {
                        $shop_state = true;
                    } else {
                        $shop_state = false;
                    }

                    if (!$shop_state) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }

        public function validate($carrier_id, $product_id, $tokendrive, $timeselect, $dateday, $date, $context)
        {

            $customer_mail = $context->customer->email;
            $customer_passwd = $context->customer->passwd;
            $customer_secure_key = $context->customer->secure_key;
            $customer_logged = $context->customer->logged;

            $securedrive = md5('6s' . $customer_mail . $customer_passwd . $customer_secure_key . $customer_logged);

            if ($tokendrive == $securedrive) {
                $selectyourcarrier = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "selectedcarrier"');
                if (preg_match('#,' . $carrier_id . ',#', $selectyourcarrier['data_value'])) {

                    $datedrivebrut = $date . ' ' . $timeselect;
                    $dateTime = new DateTime($datedrivebrut);
                    $datedrive = $dateTime->format('Y-m-d H:i:s');

                    $id_prestashop_order = 0;
                    $id_prestashop_cart = $context->cart->id;
                    $user_checksum = $context->customer->id . $id_prestashop_cart . $context->customer->secure_key;
                    $id_fournisseur = 0;
                    $id_boutique = $context->customer->id_shop;
                    $id_carrier = $carrier_id;
                    $id_client = $context->customer->id;
                    $id_drive_level = 0;
                    $complet_drive_time = $datedrive;
                    $hour_drive_time = $timeselect;
                    $sms_drive = '';
                    $mail_drive = $customer_mail;
                    $jour_drive_time = "";
                    $date_add = date('Y-m-d H:i:s');

                    $id_one = 0;
                    $id_two = 0;
                    $id_tre = 0;
                    $id_for = 0;
                    $id_fiv = 0;
                    $id_six = 0;
                    $id_sev = 0;
                    $val_one = '';
                    $val_two = '';
                    $val_tre = '';
                    $val_for = '';
                    $val_fiv = '';
                    $val_six = '';
                    $val_sev = '';

                    if ($dateday == 0) {
                        $jour_drive_time = "Dimanche";
                    } else if ($dateday == 1) {
                        $jour_drive_time = "Lundi";
                    } else if ($dateday == 2) {
                        $jour_drive_time = "Mardi";
                    } else if ($dateday == 3) {
                        $jour_drive_time = "Mercredi";
                    } else if ($dateday == 4) {
                        $jour_drive_time = "Jeudi";
                    } else if ($dateday == 5) {
                        $jour_drive_time = "Vendredi";
                    } else if ($dateday == 6) {
                        $jour_drive_time = "Samedi";
                    }

                    Db::getInstance()->delete('prestadrive_order', 'id_prestashop_order = ' . $id_prestashop_order);

                    Db::getInstance()->insert('prestadrive_order', array(
                        'user_checksum' => pSQL($user_checksum),
                        'id_prestashop_cart' => (int) $id_prestashop_cart,
                        'id_prestashop_order' => (int) $id_prestashop_order,
                        'id_fournisseur' => (int) $id_fournisseur,
                        'id_boutique' => (int) $id_boutique,
                        'id_carrier' => (int) $id_carrier,
                        'id_client' => (int) $id_client,
                        'id_drive_level' => (int) $id_drive_level,
                        'complet_drive_time' => $complet_drive_time,
                        'hour_drive_time' => pSQL($hour_drive_time),
                        'jour_drive_time' => pSQL($jour_drive_time),
                        'sms_drive' => pSQL($sms_drive),
                        'mail_drive' => pSQL($mail_drive),
                        'date_add' => $date_add,
                        'id_one' => (int) $id_one,
                        'id_two' => (int) $id_two,
                        'id_tre' => (int) $id_tre,
                        'id_for' => (int) $id_for,
                        'id_fiv' => (int) $id_fiv,
                        'id_six' => (int) $id_six,
                        'id_sev' => (int) $id_sev,
                        'val_one' => pSQL($val_one),
                        'val_two' => pSQL($val_two),
                        'val_tre' => pSQL($val_tre),
                        'val_for' => pSQL($val_for),
                        'val_fiv' => pSQL($val_fiv),
                        'val_six' => pSQL($val_six),
                        'val_sev' => pSQL($val_sev),
                    ));

                    if ($product_id) {
                        $supplier_state = $this->getSupplierState($context, $product_id);

                        return array(
                            'add_to_cart' => $supplier_state,
                        );
                    } else {
                        return $this->getCartValues($id_prestashop_cart);
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        public function edit($cart_id, $context)
        {
            $cart = new Cart($cart_id);
            return ($cart->delete());
        }

        public function getCartValues($id_cart)
        {
            $cart_values = Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'prestadrive_order WHERE id_prestashop_cart = ' . $id_cart . '');
            if ($cart_values) {
                return $cart_values;
            } else {
                return false;
            }
        }

        public function isEnabledForCarrier($carrier_id)
        {
            $selectyourcarrier = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "selectedcarrier"');
            if (preg_match('#,' . $carrier_id . ',#', $selectyourcarrier['data_value'])) {
                return true;
            } else {
                return false;
            }
        }

        protected function getConfigFormValues()
        {
            // recuperation des valeurs en base
            $wdrive = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "wdrive"');
            $wselect = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "wselect"');
            $wdate = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "wdate"');
            $drivelundi = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "drivelundi"');
            $drivemardi = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "drivemardi"');
            $drivemercredi = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "drivemercredi"');
            $drivejeudi = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "drivejeudi"');
            $drivevendredi = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "drivevendredi"');
            $drivesamedi = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "drivesamedi"');
            $drivedimanche = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "drivedimanche"');
            $confintervalfordrive = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "confintervalfordrive"');
            $selectyourcarrier = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "selectedcarrier"');

            return array(
                'prestadrive_forcedrive_name' => Configuration::get('prestadrive_forcedrive'),
                'prestadrive_controlcarrier_name' => Configuration::get('prestadrive_controlcarrier'),
                'wdrive' => pSQL($wdrive['data_value']),
                'wselect' => pSQL($wselect['data_value']),
                'wdate' => pSQL($wdate['data_value']),
                'drivelundi' => pSQL($drivelundi['data_value']),
                'drivemardi' => pSQL($drivemardi['data_value']),
                'drivemercredi' => pSQL($drivemercredi['data_value']),
                'drivejeudi' => pSQL($drivejeudi['data_value']),
                'drivevendredi' => pSQL($drivevendredi['data_value']),
                'drivesamedi' => pSQL($drivesamedi['data_value']),
                'drivedimanche' => pSQL($drivedimanche['data_value']),
                'confintervalfordrive' => pSQL($confintervalfordrive['data_value']),
                'selectyourcarrier' => pSQL($selectyourcarrier['data_value']),

            );
        }

        protected function calcultime($hours, $confintervalfordrive)
        {
            //d($hours);
            $gethour = explode("-", $hours);
            $starthour = strtotime($gethour[0]);
            $endhour = strtotime($gethour[1]);
            //transformation en minutes
            $totalhour = (int) ($endhour - $starthour) / 60;
            $calctime = $starthour;

            $returnalltimetpl[] = date('H:i', $calctime);

            $driveevery = strtotime($confintervalfordrive);
            // conversion en minutes
            $driveevery = date('i', $driveevery);
            $countdriveevery = (int) ($totalhour) / (int) $driveevery;

            for ($ii = 0; $ii < $countdriveevery; $ii++) {
                $calctime = $calctime + $driveevery * 60;
                $returnalltimetpl[] = date('H:i', $calctime);
            }

            return $returnalltimetpl;
        }
    }
}
