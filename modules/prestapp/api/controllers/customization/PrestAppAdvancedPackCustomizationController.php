<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

//https://addons.prestashop.com/fr/ventes-croisees-packs-produits/1015-advanced-pack-5-vendez-des-lots-de-produits.html
if (Module::isInstalled('pm_advancedpack') && Module::isEnabled('pm_advancedpack')) {
    class PrestAppAdvancedPackCustomizationController extends pm_advancedpack
    {
        public function _construct()
        {
        }

        public function isAdvancedPack($id_product)
        {
            return AdvancedPack::getPackContent($id_product) ? true : false;
        }
    }
}
