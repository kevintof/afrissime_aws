<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('fidepi') && Module::isEnabled('fidepi')) {

    class PrestAppFidepiCustomizationController
    {
        protected $fidepi;

        public function __construct()
        {
            $this->fidepi = Module::getInstanceByName('fidepi');
        }

        public function getData()
        {
            $data = new StdClass();
            $data->name = 'fidepi';
            $data->fidepi_active = true;

            return $data;
        }

        public function updateCard($context)
        {
            $id_customer = $context->customer->id;
            Context::getContext()->cookie->__set('id_customer', $id_customer);
            $this->fidepi->hookActionCustomerIdentityFormSubmit(null);
        }
    }
}
