<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('facebookpsconnect') && Module::isEnabled('facebookpsconnect')) {

    class PrestAppFacebookpsconnectCustomizationController
    {
        protected $facebookpsconnect;

        public function __construct()
        {
            $this->facebookpsconnect = Module::getInstanceByName('facebookpsconnect');

            include_once _PS_MODULE_DIR_ . '/facebookpsconnect/lib/connectors/base-connector_class.php';
            include_once _PS_MODULE_DIR_ . '/facebookpsconnect/facebookpsconnect.php';
            include_once _PS_MODULE_DIR_ . '/facebookpsconnect/lib/module-tools_class.php';
            include_once _PS_MODULE_DIR_ . '/facebookpsconnect/lib/connectors/facebook-connect_class.php';
        }

        public function facebookConnect($access_token, $code, $context)
        {

            $fbConnect = new BT_FacebookConnect(array());

            if ($access_token && $code) {
                $oFBUser = BT_FPCModuleTools::jsonDecode(BT_FPCModuleTools::fileGetContent('https://graph.facebook.com/me/?access_token=' . $access_token . '&fields=' . $fbConnect->sFieldList));

                if (!empty($oFBUser->id) && !empty($oFBUser->email)) {
                    // set create status
                    $bCreateStatus = true;
                    $bCreatePs = false;
                    $bCreateSocial = false;

                    BT_BaseConnector::$sName = 'facebook';

                    // set FB data
                    $fbConnect->oUser->id = $oFBUser->id;
                    $fbConnect->oUser->customerId = 0;
                    $fbConnect->oUser->first_name = $oFBUser->first_name;
                    $fbConnect->oUser->last_name = $oFBUser->last_name;
                    $fbConnect->oUser->email = $oFBUser->email;

                    // set birthday
                    if (!empty($oFBUser->birthday)) {
                        $aBirthday = explode('/', $oFBUser->birthday);

                        // format date for PS customer table
                        $fbConnect->oUser->birthday = $aBirthday[2] . '-' . $aBirthday[0] . '-' . $aBirthday[1];
                    }

                    // set gender
                    if (!empty($oFBUser->gender)) {
                        // get gender ID from PS
                        $fbConnect->oUser->gender = BT_BaseConnector::getGender($oFBUser->gender);
                    }

                    $bCreateSocial = !BT_BaseConnector::existSocialAccount($fbConnect->oUser->id);
                    $bCreatePs = !BT_BaseConnector::existPsAccount($fbConnect->oUser->email);

                    if (empty($bCreateSocial)) {

                        if (!empty($bCreatePs)) {
                            $iCustomerId = $this->getCustomerId($fbConnect->oUser->id);
                            if (BT_BaseConnector::existPsAccount($iCustomerId, 'id')) {
                                $bCreateSocial = false;
                                $bCreatePs = false;
                            } else {
                                $this->deleteSocialAccount($fbConnect->oUser->id);
                                $bCreateSocial = true;
                            }
                        }
                    } elseif (!$bCreatePs) {
                        $fbConnect->oUser->customerId = Customer::customerExists($fbConnect->oUser->email, true);
                    }

                    if (!empty($bCreatePs) || !empty($bCreateSocial)) {
                        $bCreateStatus = $this->createCustomer($fbConnect->oUser, $bCreatePs, $bCreateSocial, $this->sConnectorName);
                    }

                    if ($bCreateStatus) {
                        $customer_id = $this->getCustomerId($fbConnect->oUser->id);
                        if ($customer_id) {
                            $customer = new Customer($customer_id);
                            return $customer;
                        } else {
                            return false;
                        }
                    }
                } else {
                    return false;
                }

            } else {
                return false;
            }
        }

        protected function createCustomer($oUser, $bCreatePs, $bCreateSocial, $sSocialName)
        {

            $bResult = true;
            $iDateAdd = time();
            $sPassword = mt_rand();

            if ($bCreatePs) {
                if (!empty($oUser->email) && !empty($oUser->first_name)) {
                    // set default values
                    $iGender = empty($oUser->gender) ? $this::getGender('') : $oUser->gender;

                    // throw exception if default customer group empty
                    if (empty(FacebookPsConnect::$conf['FBPSC_DEFAULT_CUSTOMER_GROUP'])) {
                        throw new BT_ConnectorException(FacebookPsConnect::$oModule->l('Internal server error => default customer group is empty',
                            'base-connector_class'), 518);
                    } else {
                        $iDefaultGroup = FacebookPsConnect::$conf['FBPSC_DEFAULT_CUSTOMER_GROUP'];
                    }

                    // set password
                    $sPasswordGen = Tools::encrypt($sPassword);

                    // date
                    $sLastPwdGen = time();
                    // secure key
                    $sSecureKey = md5(uniqid(rand(), true));

                    $oCustomer = new Customer();

                    // Set the customer information
                    $oCustomer->lastname = $oUser->last_name;
                    $oCustomer->firstname = $oUser->first_name;
                    $oCustomer->id_shop = FacebookPsConnect::$iShopId;
                    $oCustomer->id_gender = $iGender;
                    $oCustomer->id_default_group = $iDefaultGroup;
                    $oCustomer->email = $oUser->email;
                    $oCustomer->passwd = $sPasswordGen;
                    $oCustomer->last_passwd_gen = $sLastPwdGen;
                    $oCustomer->date_add = $iDateAdd;
                    $oCustomer->date_upd = $iDateAdd;
                    $oCustomer->newsletter = 1;
                    $oCustomer->website = FacebookConnect;
                    $oCustomer->ip_registration_newsletter = FacebookConnect;

                    if (empty($oCustomer->optin)) {
                        $oCustomer->optin = 0;
                    }

                    if (!empty($oUser->birthday)) {
                        $oCustomer->birthday = $oUser->birthday;
                    }

                    // Add the customer
                    $bResult = $oCustomer->add();

                    $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'prestapp_customers` (
                        `id_prestapp_customer` int(11) unsigned NOT NULL AUTO_INCREMENT,
                        `id_ps_customer` INT( 11 ) UNSIGNED NOT NULL,
                        `date_add` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id_prestapp_customer`)
                      ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';
            
                    if(Db::getInstance()->Execute($sql)) {
                        $sql = 'INSERT INTO `'._DB_PREFIX_.'prestapp_customers` (
                            `id_ps_customer`
                            ) values ('.(int)($oCustomer->id).')';
    
                        Db::getInstance()->Execute($sql);
                    }

                    $oUser->customerId = $oCustomer->id;

                }
            }

            if (!empty($oUser->customerId)) {
                // use case - update customer address or birthday
                if (empty($bCreatePs)) {
                    // update birthday
                    if (!empty($oUser->birthday)) {
                        $this->updateCustomerBirthDay($oUser->customerId, $oUser->birthday);
                    }
                    // update gender
                    if (!empty($oUser->gender)) {
                        $this->updateCustomerGender($oUser->customerId, $oUser->gender);
                    }
                }

                // update address
                if (!empty($oUser->address)) {
                    $this->updateCustomerAddress($oUser->customerId, $oUser->address);
                }
                // use case - create PS account
                if (!empty($bCreateSocial)) {
                    if (!empty($oUser->id) && !empty($oUser->email)) {

                        $sQuery = 'INSERT INTO `' . _DB_PREFIX_ . bqSQL(Tools::strtolower(_FPC_MODULE_NAME)) . '_connect` SET '
                        . ' CNT_CUST_ID = ' . (int) $oUser->customerId . ', '
                        . ' CNT_SHOP_ID = ' . (int) FacebookPsConnect::$iShopId . ', '
                        . ' CNT_CUST_SOCIAL_ID = "' . pSQL($oUser->id) . '", '
                        . ' CNT_CUST_TYPE = "' . pSQL('facebook') . '",'
                        . ' CTN_DATE_ADD = FROM_UNIXTIME(' . (int) $iDateAdd . '), CTN_DATE_UPD = FROM_UNIXTIME(' . (int) $iDateAdd . ') ';

                        // exec query
                        $bResult = Db::getInstance()->Execute($sQuery);

                        // use case - customer creation succeed
                        if ($bResult) {
                            // exec transaction
                            Db::getInstance()->Execute('COMMIT');

                            //generate the voucher code
                            if (!empty(FacebookPsConnect::$conf['FBPSC_ENABLE_VOUCHER']) && !empty($oUser->customerId)) {
                                BT_FpcVoucher::generateVoucher($sSocialName, $oUser->customerId);
                            }

                            // send an e-mail with password
                            if (!empty(FacebookPsConnect::$conf['FBPSC_ENABLE_VOUCHER'])
                                && (FacebookPsConnect::$conf['FBPSC_VOUCHER_DISPLAY_MODE'] == 'email' || FacebookPsConnect::$conf['FBPSC_VOUCHER_DISPLAY_MODE'] == 'both')) {

                                require_once _FPC_PATH_LIB_VOUCHER . 'voucher-dao_class.php';

                                //get the voucher information for the current customer
                                $aVoucher = BT_FpcVoucherDao::getvoucherAssocInfo((int) $oUser->customerId);

                                //set the notification building with the voucher information
                                $this->sendCustomerNotification(FacebookPsConnect::$iCurrentLang, $oUser->email,
                                    $sPassword, $oUser->first_name, $oUser->last_name, 'facebook', true,
                                    $aVoucher);
                            } else {
                                $this->sendCustomerNotification(FacebookPsConnect::$iCurrentLang, $oUser->email,
                                    $sPassword, $oUser->first_name, $oUser->last_name, 'facebook', false,
                                    null);
                            }
                        } else {
                            Db::getInstance()->Execute('ROLLBACK');
                            $bResult = false;
                        }
                    } else {
                        Db::getInstance()->Execute('ROLLBACK');
                        $bResult = false;
                    }
                }
            }

            return $bResult;
        }

        protected static function getGender($sGender)
        {

            $iGender = 0;

            if ($sGender == 'male') {
                $iGender = 0;
            } elseif ($sGender == 'female') {
                $iGender = 1;
            } // default case - male
            else {
                $iGender = 0;
            }

            $sQuery = 'SELECT id_gender '
            . ' FROM ' . _DB_PREFIX_ . 'gender'
            . ' WHERE `type` = ' . (int) $iGender;

            $aGender = Db::getInstance()->getRow($sQuery);
            $iGender = !empty($aGender['id_gender']) ? $aGender['id_gender'] : 1;

            unset($aGender);

            return $iGender;
        }

        protected function updateCustomerBirthDay($iCustomerId, $sBirthday)
        {
            // query
            $sQuery = 'UPDATE ' . _DB_PREFIX_ . 'customer SET birthday = "' . pSQL($sBirthday) . '"'
            . ' WHERE `active` = 1 AND `id_customer` = ' . (int) $iCustomerId
                . ' AND `deleted` = 0 AND `is_guest` =  0';

            // execute
            return Db::getInstance()->Execute($sQuery);
        }

        protected function updateCustomerGender($iCustomerId, $iCustomerGender)
        {

            $bResult = true;

            // query
            $sQuery = 'SELECT id_gender as id'
            . ' FROM ' . _DB_PREFIX_ . 'customer'
            . ' WHERE `id_customer` = ' . (int) $iCustomerId;

            $aGenderExists = Db::getInstance()->getValue($sQuery);

            // use case - empty id for 1.5 or id equal unknown gender for under 1.5
            if ((version_compare(_PS_VERSION_, '1.5', '>')
                && empty($aGenderExists['id']))
                || (version_compare(_PS_VERSION_, '1.5', '<')
                    && $aGenderExists['id'] == 9)
            ) {
                // query
                $sQuery = 'UPDATE ' . _DB_PREFIX_ . 'customer SET id_gender = ' . (int) $iCustomerGender
                . ' WHERE `active` = 1 AND `id_customer` = ' . (int) $iCustomerId
                    . ' AND `deleted` = 0  AND `is_guest` =  0';

                // execute
                $bResult = Db::getInstance()->Execute($sQuery);
            }

            return $bResult;
        }

        protected function updateCustomerAddress($iCustomerId, stdClass $oAddress)
        {
            $bInsert = true;

            // query
            $sQuery = 'SELECT count(*) as nb'
            . ' FROM ' . _DB_PREFIX_ . 'address'
            . ' WHERE `id_customer` = ' . (int) $iCustomerId;

            $aExists = Db::getInstance()->ExecuteS($sQuery);

            if (empty($aExists[0]['nb'])) {
                // define query
                $sQuery = 'INSERT INTO `' . _DB_PREFIX_ . 'address` SET'
                . ' id_country = ' . (int) $oAddress->id_country . ', '
                . ' id_state = ' . (int) $oAddress->id_state . ', '
                . ' id_customer = ' . (int) $iCustomerId . ', '
                . ' id_manufacturer = ' . (int) $oAddress->id_manufacturer . ', '
                . ' id_supplier = ' . (int) $oAddress->id_supplier . ', '
                . ' id_warehouse = ' . (int) $oAddress->id_warehouse . ','
                . ' alias = "' . pSQL($oAddress->alias) . '", '
                . ' company = "' . pSQL($oAddress->company) . '", '
                . ' lastname = "' . pSQL($oAddress->lastname) . '", '
                . ' firstname = "' . pSQL($oAddress->firstname) . '", '
                . ' address1 = "' . pSQL($oAddress->address1) . '", '
                . ' address2 = "' . pSQL($oAddress->address2) . '", '
                . ' postcode = "' . pSQL($oAddress->postcode) . '", '
                . ' city = "' . pSQL($oAddress->city) . '", '
                . ' other = "' . pSQL($oAddress->other) . '", '
                . ' phone = "' . pSQL($oAddress->phone) . '", '
                . ' phone_mobile = "' . pSQL($oAddress->phone_mobile) . '", '
                . ' vat_number = "' . pSQL($oAddress->vat_number) . '",'
                . ' dni = "' . pSQL($oAddress->dni) . '",'
                . ' date_add = "' . pSQL($oAddress->date_add) . '", '
                . ' date_upd = "' . pSQL($oAddress->date_upd) . '", '
                . ' active = ' . (int) $oAddress->active . ', '
                . ' deleted = ' . (int) $oAddress->deleted;

                // exec query
                $bInsert = Db::getInstance()->Execute($sQuery);
            }

            return $bInsert;
        }

        protected static function sendCustomerNotification(
            $iIsoLang,
            $sEmail,
            $sPassword,
            $sFirstName,
            $sLastName,
            $sSocialName,
            $bVoucherCode,
            $aVoucherInfo = null
        ) {
            // set params
            if (!empty($iIsoLang)
                && !empty($sEmail)
                && !empty($sPassword)
                && !empty($sFirstName)
                && !empty($sLastName)
            ) {
                require_once _FPC_PATH_LIB . 'mail-send_class.php';

                $aParams = array();

                $aParams['iso'] = 'fr';
                $aParams['isoId'] = $iIsoLang;
                $aParams['email'] = $sEmail;
                $aParams['password'] = $sPassword;
                $aParams['firstname'] = $sFirstName;
                $aParams['lastname'] = $sLastName;
                $aParams['social'] = $sSocialName;

                if (!empty($bVoucherCode)) {

                    // assign specific voucher information
                    if (!empty($aVoucherInfo)) {
                        $aParams['voucherCode'] = $aVoucherInfo['code'];
                        $aParams['voucherUntilDate'] = $aVoucherInfo['date_to'];

                        //build the information for the voucher
                        if ($aVoucherInfo['reduction_percent'] != 0.0) {
                            $aParams['voucherInfoText'] = $aVoucherInfo['reduction_percent'] . ' %';
                        } else {
                            //get currency sign
                            if (!empty($aVoucherInfo['reduction_currency'])) {

                                $oCurrency = new Currency((int) $aVoucherInfo['reduction_currency']);

                                if (is_object($oCurrency)) {
                                    //get tax included / excluded tax
                                    $sTaxText = empty($aVoucherInfo['reduction_tax']) ? FacebookPsConnect::$oModule->l('Tax excluded',
                                        'base-connector_class') : FacebookPsConnect::$oModule->l('Tax included',
                                        'base-connector_class');

                                    $aParams['voucherInfoText'] = $aVoucherInfo['reduction_amount'] . $oCurrency->sign . ' ' . $sTaxText;
                                }
                                unset($oCurrency);
                            }
                        }
                    }

                    // Use case notification for Voucher code
                    BT_FpcMailSend::create()->run('customerAccountNotificationWithVoucher', $aParams);
                } else {
                    // Use case for simple notification
                    BT_FpcMailSend::create()->run('customerAccountNotification', $aParams);
                }

            }
        }

        protected function getCustomerId($iSocialId)
        {
            // query
            $sQuery = 'SELECT CNT_CUST_ID as id'
            . ' FROM ' . _DB_PREFIX_ . bqSQL(Tools::strtolower(_FPC_MODULE_NAME)) . '_connect'
            . ' WHERE `CNT_CUST_SOCIAL_ID` = "' . pSQL($iSocialId) . '" AND CNT_CUST_TYPE = "' . pSQL('facebook') . '"';

            // execute
            $aResult = Db::getInstance()->getRow($sQuery);

            return !empty($aResult['id']) ? $aResult['id'] : 0;
        }

        protected function deleteSocialAccount($iSocialId)
        {
            // query
            $sQuery = 'DELETE FROM ' . _DB_PREFIX_ . bqSQL(Tools::strtolower(_FPC_MODULE_NAME)) . '_connect '
            . ' WHERE CNT_CUST_SOCIAL_ID = "' . pSQL($iSocialId) . '"'
            . ' AND CNT_CUST_TYPE = "' . pSQL('facebook') . '"';

            // execute
            return Db::getInstance()->Execute($sQuery);
        }
    }

}
