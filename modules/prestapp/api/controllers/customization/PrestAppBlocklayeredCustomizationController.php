<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('blocklayered') && Module::isEnabled('blocklayered')) {

    class PrestAppBlocklayeredCustomizationController
    {
        protected $blocklayered;

        public function __construct($p, $n, $column, $order)
        {
            $_POST['p'] = $p;
            $_POST['orderby'] = Tools::strtolower($column);
            $_POST['orderway'] = Tools::strtolower($order);
            $_POST['n'] = $n;
            $this->blocklayered = Module::getInstanceByName('blocklayered');
        }

        public function getFilters($id_category, $filters)
        {

            $_POST['id_category'] = $id_category;

            $blocklayered = Module::getInstanceByName('blocklayered');

            if ($filter_block = $this->blocklayered->getFilterBlock($selected_filters)) {
                if ($filter_block['nbr_filterBlocks'] == 0) {
                    return false;
                }

                return $filter_block;
            } else {
                return false;
            }
        }

        public static function getAvailableFilters($context, $filters, $id_category)
        {

            $selected_filters = json_decode($filters, true);

            if (!$selected_filters) {
                $selected_filters = array(
                    'category' => array(
                        $id_category,
                    ),
                );
            }

            $_POST['id_category'] = $id_category;

            $blocklayered = Module::getInstanceByName('blocklayered');
            $filter_block = $blocklayered->getFilterBlock($selected_filters);

            return $filter_block;
        }

        public function getProducts($context, $populate, $filters, $id_category)
        {

            $selected_filters = json_decode($filters, true);

            if (!$selected_filters) {
                $selected_filters = array(
                    'category' => array(
                        $id_category,
                    ),
                );
            }

            $_POST['id_category'] = $id_category;

            $blocklayered = Module::getInstanceByName('blocklayered');

            $filter_block = $blocklayered->getFilterBlock($selected_filters);

            $products = $blocklayered->getProductByFilters($selected_filters);
            $products = Product::getProductsProperties($context->language->id, $products);

            foreach ($products as $product) {
                if ($product['id_product_attribute'] && isset($product['product_attribute_minimal_quantity'])) {
                    $product['minimal_quantity'] = $product['product_attribute_minimal_quantity'];
                }

            }

            $productsInLayer = array();
            foreach ($products as $product) {
                $product = PrestAppProductController::getProduct((int) $product['id_product'], $populate, $context);
                if ($product && $product != 404 && $product != 403) {
                    array_push($productsInLayer, $product);
                }
            }

            $data = array(
                'layer' => $filter_block,
                'products' => $productsInLayer,
            );

            return $data;
        }
    }
}
