<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('ndk_steppingpack') && Module::isEnabled('ndk_steppingpack')) {

    class PrestAppNdksteppingpackCustomizationController
    {
        protected $ndksteppingpack;

        public function __construct()
        {
            $this->ndksteppingpack = Module::getInstanceByName('ndk_steppingpack');
        }

        public function getData()
        {
            $data = new StdClass();
            $data->name = 'ndksteppingpack';
            $data->ndksteppingpack_active = true;

            return $data;
        }

        public function getPacks($context)
        {
            $packs_id = null;
			$id_category = null;

            $packs = NdkSpack::getPacks($packs_id, false);
            $is_https = (Configuration::get('PS_SSL_ENABLED') == 1 && Configuration::get('PS_SSL_ENABLED_EVERYWHERE') == 1 ? true : false);

            foreach($packs as $key => $value) {

                $img_url = '';

                if (isset($is_https) && $is_https) {
                    $img_url = $img_url . _PS_BASE_URL_SSL_;
                } else {
                    $img_url = $img_url . _PS_BASE_URL_;
                }

                $img_url = $img_url . '/img/scenes/ndksp/thumbs/' . $value['id_ndk_steppingpack'] . '-home_default.jpg';

                $packs[$key]['img_url'] = $img_url;
            }
            
            return $packs;
        }

        public function getPackData($pack_id, $context)
        {
            $pack = new NdkSpack($pack_id, $context->language->id);

            if($pack->fixed_price > 0 && $pack->type == 1)
			{
				$pack->fixed_price = Product::getPriceStatic((int)$pack->id_pack_prod);
            }
            
            $steps = $pack->getPAckSteps();
            $stepsLite = $pack->getPAckStepsLite();
            
            if($pack->id_cart_rule > 0) {
				$cart_rule = new CartRule($pack->id_cart_rule, $context->language->id);
            } else {
				$cart_rule = false;
            }

            if (!isset($pack->id)) {
                $pack = null;
                $steps = null;
                $cart_rule = null;
            }

            return array(
                'pack' => $pack,
                'steps' => $steps,
                'stepsLite' => $stepsLite,
                'cart_rule' => $cart_rule
            );
        }

        public function getStepProducts($step_id, $populate, $context) {
            $step = new ndkSpackStep((int)$step_id);
            $step_products = $step->getProducts();

            $products = array();

            foreach ($step_products as $product) {
                $product = PrestAppProductController::getProduct((int) $product['id_product'], $populate, $context);
                if ($product && $product != 404 && $product != 403) {
                    array_push($products, $product);
                }
            }

            return $products;
        }

        public function checkCart($id_pack, $stepsLite, $create_pack, $context) {

            require_once _PS_MODULE_DIR_.'ndk_steppingpack/models/ndkSpack.php';
            require_once _PS_MODULE_DIR_.'ndk_steppingpack/models/ndkSpackStep.php';

            $saved_currency = $context->currency->id;
            $context->currency->id = (int)Configuration::get('PS_CURRENCY_DEFAULT');

            $languages = Language::getLanguages();
            $id_lang = $context->language->id;
            $steps = json_decode($stepsLite, true);
            $results = array();
            $h = -1;
            $i = 0;
            $j = 1;
            $finished = false;

            $step_todo = false;
            $step_cando = false;
            $results[0]['step_todo'] = 0;
            $results[0]['step_cando'] = 0;
            $virtualPack = array();

            foreach($steps as $step)
            {
                $stepObj = new NdkSpackStep((int)$step['id']);
                $pids = $stepObj->getProducts(true);
                $ids = array();
                foreach($pids as $pid) {
                    $ids[] = $pid['id_product'];
                }
                $prodsIds = implode(',', $ids);

                $count_quantities = 0;
                $count = Db::getInstance()->getRow(
                'SELECT COUNT(*) as count   
                FROM `'._DB_PREFIX_.'cart_product` cp 
                WHERE cp.`id_product` IN ('.pSQL($prodsIds).') AND id_cart = '.(int)$context->cart->id);
                
                $id_products = Db::getInstance()->executeS(
                'SELECT id_product, quantity    
                FROM `'._DB_PREFIX_.'cart_product` cp 
                WHERE cp.`id_product` IN ('.pSQL($prodsIds).') AND id_cart = '.(int)$context->cart->id.' GROUP BY id_product');
                
                $count_sql = Db::getInstance()->executeS(
                'SELECT id_product, quantity    
                FROM `'._DB_PREFIX_.'cart_product` cp 
                WHERE cp.`id_product` IN ('.pSQL($prodsIds).') AND id_cart = '.(int)$context->cart->id);
                
                $prodIds = array();
                $prodIds[] = 0;
                $products = '';
    
                if (count($products) > 0) {
                    $results[$i]['products'] = array();
                }

                foreach($id_products as $products) {
                    $prodIds[] = $products['id_product'];
                    $cart = new Cart((int)$context->cart->id);
                    $products = $cart->getProducts(false, $products['id_product']);
                    array_push($results[$i]['products'], $products[0]);
                    $virtualPack[$steps[$i]['id']]['name'] =  $steps[$i]['name'];
                    $virtualPack[$steps[$i]['id']]['products'][] = $products;
                    $virtualPack[$steps[$i]['id']]['maximum'] = $steps[$i]['maximum'];
                    $virtualPack[$steps[$i]['id']]['optionnal'] = $steps[$i]['optionnal'];  
                }

                $parsed_products = array();

                foreach ($results[$i]['products'] as &$product) {
                    $id_image = explode('-', $product['id_image']);
                    $image = new Image($id_image[1]);
                    $urls = PrestAppImageController::getImageUrl((int) $image->id, $context);
                    $product['image'] = $urls;
                }

                if (count($parsed_products) > 0) {
                    $results[$i]['products'] = $parsed_products;
                }

                foreach($count_sql as $row) {
                    $count_quantities += $row['quantity'];
                }

                if(isset($steps[$j])) {
                    $results[$i]['id'] = (int)$steps[$j]['id'];
                } else {
                    $results[$i]['id'] = 999;
                }
                
                if($h > -1) {
                    $results[$i]['prev_step'] = (int)$steps[$h]['id'];
                } else {
                    $results[$i]['prev_step'] = (int)$steps[$i]['id'];
                }
                    
                $results[$i]['id_step'] = (int)$steps[$i]['id'];
                $results[$i]['count'] = $count_quantities;
                $results[$i]['id_step'] = (int)$steps[$i]['id'];
                $results[$i]['position'] = (int)$steps[$i]['position'];
                
                $results[$i]['disable_it'] = 0;

                if((!$step_cando || $step_cando == false) && $step['minimum'] == 0)
                {
                    $results[0]['step_cando'] = $steps[$i]['id'];
                    $results[0]['optionnal'] = $steps[$i]['optionnal'] ;
                    $step_cando = true;
                }
                
                if ( ($count_quantities >= $step['maximum'] ) && $step['maximum'] > 0)
                {
                    $results[$i]['disable_it'] = 1;
                }
                else
                {
                    $results[$i]['disable_it'] = 0;
                }
                
                if($count_quantities >= $step['minimum']) 
                {
                    $results[$i]['status'] = 1;
                    $finished = true;
                }
                    
                else
                { 
                    $results[$i]['status'] = 0;
                    if(!$step_todo || $step_todo == false)
                    {
                        $results[0]['step_todo'] = $steps[$i]['id'];
                        $step_todo = true;
                    }
                    
                    $finished = false;
                }
                $h++;
                $i++;
                $j++;
            }

            if ($finished && $create_pack) {
                $pack = new NdkSpack($id_pack, $id_lang);
                if($pack->type == 1 && $pack->id_pack_prod > 0) {
                    $packProd = new Product($pack->id_pack_prod, $id_lang);
                    $pack_price = 0;
                    $opt_price = 0;
                    $newWeight = 0;
                    $newPackProd = NdkSpack::processDuplicatePack($packProd->id);
                    $newPackProd->reference = Tools::str2url('Pack-'.$packProd->id.'-Cart-'.(int)$context->cart->id);
                    $newPackProd->supplier_reference = Tools::str2url('myndkcustomprodPack');
                    $newPackProd->cache_is_pack = 1;
                    $newPackProd->pack_stock_type = 1;

                    Db::getInstance()->delete('ndk_steppingpack_order', 'id_cart = '.(int)$context->cart->id.' AND id_virtual_pack = '.(int)$newPackProd->id);
                
                    Db::getInstance()->delete('pack', 'id_product_pack = '.(int)$newPackProd->id);

                    $fields_nb = 0;
                    foreach($virtualPack as $vpack)
                    {
                        $labels = array();
                        foreach ($languages as $language) {
                            $labels[$language['id_lang']][0]['name']  = $vpack['name'];
                        }
                        $index = $this->createLabel($context, $languages, 1, $newPackProd->id, $labels);

                        $row = '';
                        $incart = 0;
                        
                        $pack_available_quantity = 0;
                        $last_quantity_encountred = 999999999999;

                        foreach($vpack['products'] as $prod)
                        {
                            foreach($prod as $prodItem)
                            {
                                $maxP = $prodItem['cart_quantity'];

                                if( ($vpack['maximum'] > 0) && (($prodItem['cart_quantity'] + $incart) > $vpack['maximum']) ) {
                                    $maxP = (int)$vpack['maximum'] - $incart;
                                }

                                if($prodItem['id_product_attribute'] > 0)
                                {
                                    $p = new Product((int)$prodItem['id_product']);
                                    $accessorycombNames = $p->getAttributesResume($context->language->id);
                                    foreach($accessorycombNames as $comb)
                                    {
                                        if($comb['id_product_attribute'] == $prodItem['id_product_attribute']) {
                                            $accessorycombName = $comb['attribute_designation'];
                                        }
                                    }
                                }

                                $row .= $maxP.'X '.$prodItem['name'].($prodItem['id_product_attribute'] > 0 ? ' | '.$accessorycombName : '').'; ';

                                Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ndk_steppingpack_order` (`id_cart`, `id_product`, `id_product_attribute`, `quantity`,`id_virtual_pack`)
                                VALUES ('.(int)$context->cart->id.', '.(int)$prodItem['id_product'].', '.(int)$prodItem['id_product_attribute'].', '.(int)$maxP.', '.(int)$newPackProd->id.')');
                                
                                $pack_price+= (float)($prodItem['price']*$maxP);
						
                                if($vpack['optionnal'] == 1) {
                                    $opt_price+= (float)($prodItem['price']*$maxP);
                                }
                                
                                $newWeight += (float)$prodItem['weight'];
                                Pack::addItem((int)$newPackProd->id, (int)$prodItem['id_product'], (int)$maxP, (int)$prodItem['id_product_attribute']);

                                if($prodItem['id_customization'] > 0) {
                                    $this->copyCustomisations((int)$prodItem['id_customization'], (int)$newPackProd->id, $cart);
                                }

                                if($maxP == $prodItem['cart_quantity']) {
                                    $context->cart->deleteProduct((int)$prodItem['id_product'], (int)$prodItem['id_product_attribute']);
                                } else {
							        $context->cart->updateQty((int)$maxP, (int)$prodItem['id_product'], (int)$prodItem['id_product_attribute'], null, 'down');
                                }   
                                
                                $incart += $maxP;	
                                $prod_available = StockAvailable::getQuantityAvailableByProduct($prodItem['id_product'], $prodItem['id_product_attribute'])/$maxP;

                                if($prod_available < $last_quantity_encountred) 
                                {
                                    $pack_available_quantity = $prod_available;
                                    $last_quantity_encountred = $prod_available;
                                }
                            }
                        }

                        $context->cart->addTextFieldToProduct($newPackProd->id, $index, 1, $row);
				        $fields_nb++;
                    }

                    $pack_price += (float)$opt_price;
                    $newPackProd->text_fields = $fields_nb;
                    $newPackProd->minimal_quantity = 1;
                    $newPackProd->quantity = 100;
                    $newPackProd->weight = (float)$newWeight;
                    $newPackProd->save();

                    if($pack->fixed_price == 0)
                    {
                        $newPackProd->price = $pack_price;
                        $newPackProd->save();

                        if($pack->reduction_amount > 0 || $pack->reduction_percent > 0 )
                        {
                            if($pack->reduction_percent > 0) {
                                $reduc = $pack->reduction_percent/100;
                            } else {
                                $reduc = $pack->reduction_amount;
                            }

                            $specific_price = new SpecificPrice();
                            $specific_price->id_product = $newPackProd->id;
                            $specific_price->id_product_attribute = 0;
                            $specific_price->id_shop = (int)$context->shop->id;
                            $specific_price->id_currency = (int)$context->currency->id;
                            $specific_price->id_shop_group = 0;
                            $specific_price->id_specific_price_rule = 0;
                            $specific_price->id_cart = 0;
                            $specific_price->id_country = 0;
                            $specific_price->id_group = 0;
                            $specific_price->id_customer = 0;
                            $specific_price->price = -1;
                            $specific_price->from_quantity = 1;
                            $specific_price->reduction_tax = 1;
                            $specific_price->reduction_type = ($pack->reduction_amount > 0 ? 'amount' : 'percentage');
                            $specific_price->reduction = $reduc;
                            $specific_price->from = '0000-00-00 00:00:00';
                            $specific_price->to = '0000-00-00 00:00:00';
                            $specific_price->save();
                        }
                    } else {
                        $newPackProd->price = $newPackProd->price + (float)$opt_price;
                        $newPackProd->available_for_order = 1;
                        $newPackProd->save();
                    }

                    if($pack_available_quantity < 1) {
                        $pack_available_quantity = 1;
                    }

                    Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'stock_available` SET `quantity` = '.(int)$pack_available_quantity.' WHERE id_product = '.(int)$newPackProd->id);

                    foreach($vpack['products'] as $prod)
                    {
                        foreach($prod as $prodItem)
                        {
                            $context->cart->updateQty((int)$prodItem['cart_quantity'], (int)$prodItem['id_product'], (int)$prodItem['id_product_attribute'], null, 'down');
                        }
                    }

                    NdkSpack::duplicateGroupReductionCache($pack->id_pack_prod, $newPackProd->id);
		            $context->cart->updateQty(1, $newPackProd->id);
                }
            }

            return $results;
        }

        public function copyCustomisations($id_customization, $id_product, $cart)
        {
            $result = Db::getInstance()->executeS('SELECT `type`, `index`, `value` FROM `'._DB_PREFIX_.'customized_data` WHERE `id_customization` = '.(int)$id_customization);
            
            foreach($result as $row)
            {
                $cart->addTextFieldToProduct($id_product, $row['index'], $row['type'], $row['value']);
            }
            Db::getInstance()->delete('customization', 'id_customization = '.(int)$id_customization);
        }

        public function createLabel($context, $languages, $type, $id_product, $labels, $required = 0)
        {
            $count = 0;
            $id_customization_field = 0;
                if($labels[(int) $context->language->id][0]['name'] !='')
                {
                //on recherche un champs existant
                $result = Db::getInstance()->executeS('
                    SELECT cf.`id_product`, cfl.id_customization_field 
                    FROM `'._DB_PREFIX_.'customization_field` cf
                    NATURAL JOIN `'._DB_PREFIX_.'customization_field_lang` cfl
                    WHERE cf.`id_product` = '.(int)$id_product. ' AND cfl.`id_lang` = '.(int) $context->language->id.' AND cfl.name = \''.pSQL($labels[(int) $context->language->id][0]['name']).'\' 
                    ORDER BY cf.`id_customization_field`');
                $count += sizeof($result);
                }
            
            
            if($count == 0)
            {
                // Label insertion
                if (!Db::getInstance()->execute('
                INSERT INTO `'._DB_PREFIX_.'customization_field` (`id_product`, `type`, `required`)
                VALUES ('.(int)$id_product.', '.(int)$type.', '.(int)$required.')') ||
                !$id_customization_field = (int)Db::getInstance()->Insert_ID())
                return false;
        
                // Multilingual label name creation
                $values = '';
        
                foreach (Shop::getContextListShopID() as $id_shop)
                    foreach($languages as $language)
                    $values .= '('.(int)$id_customization_field.', '.(int) $language['id_lang'].', '.(int)$id_shop.', \''.pSQL($labels[(int) $context->language->id][0]['name']).'\'), ';
        
                $values = rtrim($values, ', ');
                if (!Db::getInstance()->execute('
                INSERT INTO `'._DB_PREFIX_.'customization_field_lang` (`id_customization_field` ,`id_lang`, `id_shop`, `name`)
                VALUES '.$values))
                return false;
        
                // Set cache of feature detachable to true
                Configuration::updateGlobalValue('PS_CUSTOMIZATION_FEATURE_ACTIVE', '1');
            }
            else{
                if($result)
                $id_customization_field = $result[0]['id_customization_field'];
                Db::getInstance()->execute('
                UPDATE `'._DB_PREFIX_.'customization_field` SET `required` = '.(int)$required.' WHERE id_customization_field = '.(int)$id_customization_field);
            }

            return (int)$id_customization_field;
        }
    }
}