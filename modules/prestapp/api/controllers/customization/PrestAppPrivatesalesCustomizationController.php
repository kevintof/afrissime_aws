<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('privatesales') && Module::isEnabled('privatesales')) {

    class PrestAppPrivatesalesCustomizationController
    {
        protected $privatesales;

        public function __construct()
        {
            $this->privatesales = Module::getInstanceByName('privatesales');
        }

        public function getData()
        {
            $data = new StdClass();
            $data->name = 'privatesales';
            $data->privatesales_active = true;

            return $data;
        }

        public function getSales($context)
        {

            $bestsales = SaleCore::getBestSales(3, $context->language->id, $context->shop->id);
            $currentsales = SaleCore::getSales("current");
            $newsales = SaleCore::getSales("new");
            $lastdaysales = SaleCore::getSales("lastday");
            $futuresales = SaleCore::getSales("future", Configuration::get('PRIVATESALES_FUTURELIMIT'));

            $best = array();
            $current = array();
            $new = array();
            $lastday = array();
            $future = array();

            foreach ($bestsales as $value) {
                $category = new Category($value->id_category, $context->language->id);
                $value->sub_categories = $category->getSubCategories($context->language->id, true);
                array_push($best, $value);
            }

            foreach ($currentsales as $value) {
                $category = new Category($value->id_category, $context->language->id);
                $value->sub_categories = $category->getSubCategories($context->language->id, true);
                array_push($current, $value);
            }

            foreach ($newsales as $value) {
                $category = new Category($value->id_category, $context->language->id);
                $value->sub_categories = $category->getSubCategories($context->language->id, true);
                array_push($new, $value);
            }

            foreach ($lastdaysales as $value) {
                $category = new Category($value->id_category, $context->language->id);
                $value->sub_categories = $category->getSubCategories($context->language->id, true);
                array_push($lastday, $value);
            }

            foreach ($futuresales as $value) {
                $category = new Category($value->id_category, $context->language->id);
                $value->sub_categories = $category->getSubCategories($context->language->id, true);
                array_push($future, $value);
            }

            $data = array(
                'bestsales' => $best,
                'currentsales' => $current,
                'newsales' => $new,
                'lastdaysales' => $lastday,
                'futuresales' => $future,
                'has_order' => $this->ndiagaVerify($context->customer->id), //override pour ajout variable front has_order
                'link_img' => __PS_BASE_URI__ . 'modules/privatesales/img/',
                'link_mod_img' => _PS_MODULE_DIR_ . 'modules/privatesales/img/',
            );
            return $data;
        }

        public function ndiagaVerify($id_customer)
        {

            $order = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
                SELECT o.id_order
                FROM ' . _DB_PREFIX_ . 'orders o
                LEFT JOIN ' . _DB_PREFIX_ . 'order_detail od ON (od.id_order = o.id_order)
                WHERE o.id_customer=' . (int) $id_customer);

            return $order['id_order'];
        }
    }
}
