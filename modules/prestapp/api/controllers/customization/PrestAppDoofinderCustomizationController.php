<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('doofinder') && Module::isEnabled('doofinder')) {

    class PrestAppDoofinderCustomizationController
    {
        protected $doofinder;

        public function __construct()
        {
            $this->doofinder = Module::getInstanceByName('doofinder');
        }

        public function getData()
        {
            $data = new StdClass();
            $data->name = 'doofinder';
            $data->doofinder_active = true;
            // $data->doofinder_facets = Configuration::get('DF_OWSEARCHFAC') ? true : false;
            $data->doofinder_facets = true;

            return $data;
        }

        public function dooSearch($string, $page = 1, $page_size = 12, $timeout = 8000, $filters = null, $return_facets = false, $hashid = false, $apikey = false, $context = false)
        {
            require _PS_MODULE_DIR_ . '/doofinder/lib/doofinder_api.php';

            //maybe it will do not work if there are more than one language or if the shop id isnt "1".
            //in fact, in this function, the context is used to determine theses variables.
            //the solution is to copy the function and change Context:: with $context->

            $lang_iso = Tools::strtoupper($context->language->iso_code);

            $hash_id = Configuration::get('DF_HASHID_' . $lang_iso, null);
            $api_key = Configuration::get('DF_API_KEY');

            if (!$hash_id || !$api_key) {
                if (!$hash_id && $hashid) {
                    $hash_id = $hashid;
                }
                if (!$api_key && $apikey) {
                    $api_key = $apikey;
                }
                $doofinder_results = $this->searchOnApi($string, $page, $page_size, $timeout, $filters, $return_facets, $hash_id, $api_key);
            } else {
                $doofinder_results = $this->doofinder->searchOnApi($string, $page, $page_size, $timeout, $filters, $return_facets);
            }

            if (!$page) {$page = 1;}
            if (!$page_size) {$page_size = 12;}
            if (!$timeout) {$timeout = 8000;}
            if (!$return_facets) {$return_facets = false;}

            $data['products'] = [];
            $data['page'] = $page;
            $data['page_size'] = $page_size;
            $data['total'] = 0;
            $data['facets'] = $doofinder_results['facets'];

            $dfApi = new DoofinderApi($hash_id, $api_key, false, array('apiVersion' => '5'));

            $data['facets_details'] = json_decode($dfApi->getOptions());

            if ($doofinder_results['result']) {
                foreach ($doofinder_results['result'] as $product) {
                    $populate = '["price","description","images","stock"]';
                    $product = PrestAppProductController::getProduct((int) $product['id_product'], $populate, $context);
                    array_push($data['products'], $product);
                }
                $data['total'] = $doofinder_results['total'];
                return $data;
            } else {
                return $data;
            }
        }

        public function searchOnApi(
            $string,
            $page = 1,
            $page_size = 12,
            $timeout = 8000,
            $filters = null,
            $return_facets = false,
            $hash_id,
            $api_key
        ) {
            $query_name = Tools::getValue('df_query_name', false);
            $debug = Configuration::get('DF_DEBUG');
            if (isset($debug) && $debug) {
                $this->debug('Search On API Start');
            }
            $show_variations = Configuration::get('DF_SHOW_PRODUCT_VARIATIONS');
            if ((int) $show_variations !== 1) {
                $show_variations = false;
            }

            if ($hash_id && $api_key) {
                $fail = false;

                try {
                    if (!class_exists('DoofinderApi')) {
                        include_once dirname(__FILE__) . '/lib/doofinder_api.php';
                    }

                    $df = new DoofinderApi($hash_id, $api_key, false, array('apiVersion' => '5'));
                    $queryParams = array('rpp' => $page_size, // results per page
                        'timeout' => $timeout,
                        'types' => array(
                            'product',
                        ), 'transformer' => 'basic');
                    if ($query_name) {
                        $queryParams['query_name'] = $query_name;
                    }
                    if (!empty($filters)) {
                        $queryParams['filter'] = $filters;
                    }

                    $dfResults = $df->query($string, $page, $queryParams);
                } catch (Exception $e) {

                    $fail = true;
                }

                if ($fail || !$dfResults->isOk()) {
                    return false;
                }

                $dfResultsArray = $dfResults->getResults();
                $product_pool_attributes = array();
                $product_pool_ids = array();
                $customexplodeattr = Configuration::get('DF_CUSTOMEXPLODEATTR');
                foreach ($dfResultsArray as $entry) {
                    if ($entry['type'] == 'product') {
                        if (!empty($customexplodeattr) && strpos($entry['id'], $customexplodeattr) !== false) {
                            $id_products = explode($customexplodeattr, $entry['id']);
                            $product_pool_attributes[] = $id_products[1];
                            $product_pool_ids[] = (int) pSQL($id_products[0]);
                        }
                        if (strpos($entry['id'], 'VAR-') === false) {
                            $product_pool_ids[] = (int) pSQL($entry['id']);
                        } else {
                            $id_product_attribute = str_replace('VAR-', '', $entry['id']);
                            if (!in_array($id_product_attribute, $product_pool_attributes)) {
                                $product_pool_attributes[] = (int) pSQL($id_product_attribute);
                            }
                            $id_product = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue(
                                'SELECT id_product FROM ' . _DB_PREFIX_ . 'product_attribute'
                                . ' WHERE id_product_attribute = ' . (int) pSQL($id_product_attribute)
                            );
                            $product_pool_ids[] = ((!empty($id_product)) ? (int) pSQL($id_product) : 0);
                        }
                    }
                }
                $product_pool = implode(', ', $product_pool_ids);

                // To avoid SQL errors.
                if ($product_pool == "") {
                    $product_pool = "0";
                }

                if (isset($debug) && $debug) {
                    $this->debug("Product Pool: $product_pool");
                }

                $product_pool_attributes = implode(',', $product_pool_attributes);

                $context = Context::getContext();
                // Avoids SQL Error
                if ($product_pool_attributes == "") {
                    $product_pool_attributes = "0";
                }

                if (isset($debug) && $debug) {
                    $this->debug("Product Pool Attributes: $product_pool_attributes");
                }
                $db = Db::getInstance(_PS_USE_SQL_SLAVE_);
                $id_lang = $context->language->id;
                $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock,
					IFNULL(stock.quantity, 0) as quantity,
					pl.`description_short`, pl.`available_now`,
					pl.`available_later`, pl.`link_rewrite`, pl.`name`,
					' . (Combination::isFeatureActive() && $show_variations ?
                    ' IF(pai.`id_image` IS NULL OR pai.`id_image` = 0, MAX(image_shop.`id_image`),pai.`id_image`)'
                    . ' id_image, ' : 'i.id_image, ') . '
					il.`legend`, m.`name` manufacturer_name '
                . (Combination::isFeatureActive() ? (($show_variations) ?
                    ', MAX(product_attribute_shop.`id_product_attribute`) id_product_attribute' :
                    ', product_attribute_shop.`id_product_attribute` id_product_attribute') : '') . ',
					DATEDIFF(
						p.`date_add`,
						DATE_SUB(
							NOW(),
							INTERVAL ' . (Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT'))
                    ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20) . ' DAY
						)
					) > 0 new' . (Combination::isFeatureActive() ?
                    ', MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity' : '') . '
					FROM ' . _DB_PREFIX_ . 'product p
					' . Shop::addSqlAssociation('product', 'p') . '
					INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
						p.`id_product` = pl.`id_product`
						AND pl.`id_lang` = ' . (int) pSQL($id_lang) . Shop::addSqlRestrictionOnLang('pl') . ') '
                . (Combination::isFeatureActive() ? ' LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa
						ON (p.`id_product` = pa.`id_product`)
						' . Shop::addSqlAssociation('product_attribute', 'pa', false, (($show_variations) ? '' :
                    ' product_attribute_shop.default_on = 1')) . '
						' . Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :
                    Product::sqlStock('p', 'product', false, Context::getContext()->shop)) . '
					LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
					LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product` '
                . ((Combination::isFeatureActive() && $show_variations) ? '' : 'AND i.cover=1') . ') '
                . ((Combination::isFeatureActive() && $show_variations) ?
                    ' LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_image` pai'
                    . ' ON (pai.`id_product_attribute` = product_attribute_shop.`id_product_attribute`) ' : ' ')
                . Shop::addSqlAssociation('image', 'i', false, 'i.cover=1') . '
					LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il'
                . ' ON (i.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) pSQL($id_lang) . ') '
                . ' WHERE p.`id_product` IN (' . pSQL($product_pool) . ') ' .
                (($show_variations) ? ' AND (product_attribute_shop.`id_product_attribute` IS NULL'
                    . ' OR product_attribute_shop.`id_product_attribute`'
                    . ' IN (' . pSQL($product_pool_attributes) . ')) ' : '') .
                ' GROUP BY product_shop.id_product '
                . (($show_variations) ? ' ,  product_attribute_shop.`id_product_attribute` ' : '') .
                ' ORDER BY FIELD (p.`id_product`,' . pSQL($product_pool) . ') '
                    . (($show_variations) ? ' , FIELD (product_attribute_shop.`id_product_attribute`,'
                    . pSQL($product_pool_attributes) . ')' : '');
                if (isset($debug) && $debug) {
                    $this->debug("SQL: $sql");
                }

                $result = $db->executeS($sql);

                if (!$result) {
                    return false;
                } else {
                    if (version_compare(_PS_VERSION_, '1.7', '<') === true) {
                        $result_properties = Product::getProductsProperties((int) $id_lang, $result);
                        // To print the id and links in the javascript so I can register the clicks
                        $this->productLinks = array();

                        foreach ($result_properties as $rp) {
                            $this->productLinks[$rp['link']] = $rp['id_product'];
                        }
                    } else {
                        $result_properties = $result;
                    }
                }
                $this->searchBanner = $dfResults->getBanner();

                if ($return_facets) {
                    return array(
                        'doofinder_results' => $dfResultsArray,
                        'total' => $dfResults->getProperty('total'),
                        'result' => $result_properties,
                        'facets' => $dfResults->getFacets(),
                        'filters' => $df->getFilters(),
                        'df_query_name' => $dfResults->getProperty('query_name'),
                    );
                }
                return array(
                    'doofinder_results' => $dfResultsArray,
                    'total' => $dfResults->getProperty('total'),
                    'result' => $result_properties,
                    'df_query_name' => $dfResults->getProperty('query_name'),
                );
            } else {
                return false;
            }
        }
    }
}
