<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (Module::isInstalled('jcaccountgroup') && Module::isEnabled('jcaccountgroup')) {

    include_once _PS_MODULE_DIR_ . 'jcaccountgroup/jcaccountgroup.php';

    class PrestAppJcAccountGroupCustomizationController extends jcaccountgroup
    {
        public function _construct()
        {
        }

        public static function isJcAccountGroup($lang, $id_group = 0)
        {
            $jcags = array();
            $groups = Group::getGroups($lang);
            foreach ($groups as $group) {
                if ($id_group == 0 || $id_group == $group['id_group']) {
                    $jcag = JCAccountGroupClass::getByGroup($group['id_group']);
                    if ($jcag->active) {
                        $jcag = array(
                            'id' => $jcag->id_group,
                        );
                        $jcags[] = $jcag;
                    }
                }
            }
            return $jcags;
        }
    }
}
