<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppCurrencyController
{
    /**
     * getActiveLanguages - Return the languages list.
     *
     * @return array Language
     */
    public static function getActiveCurrencies()
    {
        $currencies = Currency::getCurrencies();

        $defaultCurrency = Currency::getDefaultCurrency();

        foreach ($currencies as &$currency) {
            if ((int) $currency['id_currency'] == $defaultCurrency->id) {
                $currency['default'] = true;
            } else {
                $currency['default'] = false;
            }
        }

        return $currencies;
    }
}
