<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppContactController
{
    /**
     * getActiveLanguages - Return the languages list.
     *
     * @return array Language
     */
    public static function sendContactForm($context, $message, $email, $id_contact, $id_order, $id_product)
    {
        $errors = array();

        if (!$email || !Validate::isEmail($email)) {
            $errors['email'] = 'Invalid email address.';
        } elseif (!$message) {
            $errors['message'] = 'The message cannot be blank.';
        } elseif (!Validate::isCleanHtml($message)) {
            $errors['messageFormat'] = 'Invalid message';
        } elseif (!$id_contact || !(Validate::isLoadedObject($contact = new Contact($id_contact, $context->language->id)))) {
            $errors['id_contact'] = 'Please select a subject from the list provided.';
        } else {
            $customer = $context->customer;
            if (!$customer->id) {
                $customer = new Customer();
                $customer->getByEmail($email);
            }

            $id_order = getOrder($id_order);

            if (!((
                ($id_customer_thread = (int) Tools::getValue('id_customer_thread'))
                && (int) Db::getInstance()->getValue('
                    SELECT cm.id_customer_thread FROM ' . _DB_PREFIX_ . 'customer_thread cm
                    WHERE cm.id_customer_thread = ' . (int) $id_customer_thread . ' AND cm.id_shop = ' . (int) $context->shop->id . ' AND token = \'' . pSQL(Tools::getValue('token')) . '\'')
            ) || (
                $id_customer_thread = CustomerThread::getIdCustomerThreadByEmailAndIdOrder($email, $id_order)
            ))) {
                $fields = Db::getInstance()->executeS('
					SELECT cm.id_customer_thread, cm.id_contact, cm.id_customer, cm.id_order, cm.id_product, cm.email
					FROM ' . _DB_PREFIX_ . 'customer_thread cm
					WHERE email = \'' . pSQL($email) . '\' AND cm.id_shop = ' . (int) $context->shop->id . ' AND (' .
                    ($customer->id ? 'id_customer = ' . (int) $customer->id . ' OR ' : '') . '
						id_order = ' . (int) $id_order . ')');
                $score = 0;
                foreach ($fields as $key => $row) {
                    $tmp = 0;
                    if ((int) $row['id_customer'] && $row['id_customer'] != $customer->id && $row['email'] != $email) {
                        continue;
                    }
                    if ($row['id_order'] != 0 && $id_order != $row['id_order']) {
                        continue;
                    }
                    if ($row['email'] == $email) {
                        $tmp += 4;
                    }
                    if ($row['id_contact'] == $id_contact) {
                        $tmp++;
                    }
                    if (Tools::getValue('id_product') != 0 && $row['id_product'] == Tools::getValue('id_product')) {
                        $tmp += 2;
                    }
                    if ($tmp >= 5 && $tmp >= $score) {
                        $score = $tmp;
                        $id_customer_thread = $row['id_customer_thread'];
                    }
                }
            }
            $old_message = Db::getInstance()->getValue('
                SELECT cm.message FROM ' . _DB_PREFIX_ . 'customer_message cm
                LEFT JOIN ' . _DB_PREFIX_ . 'customer_thread cc on (cm.id_customer_thread = cc.id_customer_thread)
                WHERE cc.id_customer_thread = ' . (int) $id_customer_thread . ' AND cc.id_shop = ' . (int) $context->shop->id . '
                ORDER BY cm.date_add DESC');
            if ($old_message == $message) {
                $context->smarty->assign('alreadySent', 1);
                $contact->email = '';
                $contact->customer_service = 0;
            }

            if ($contact->customer_service) {
                if ((int) $id_customer_thread) {
                    $ct = new CustomerThread($id_customer_thread);
                    $ct->status = 'open';
                    $ct->id_lang = (int) $context->language->id;
                    $ct->id_contact = (int) $id_contact;
                    $ct->id_order = (int) $id_order;
                    if ($id_product = (int) Tools::getValue('id_product')) {
                        $ct->id_product = $id_product;
                    }
                    $ct->update();
                } else {
                    $ct = new CustomerThread();
                    if (isset($customer->id)) {
                        $ct->id_customer = (int) $customer->id;
                    }
                    $ct->id_shop = (int) $context->shop->id;
                    $ct->id_order = (int) $id_order;
                    if ($id_product = (int) Tools::getValue('id_product')) {
                        $ct->id_product = $id_product;
                    }
                    $ct->id_contact = (int) $id_contact;
                    $ct->id_lang = (int) $context->language->id;
                    $ct->email = $email;
                    $ct->status = 'open';
                    $ct->token = Tools::passwdGen(12);
                    $ct->add();
                }

                if ($ct->id) {
                    $cm = new CustomerMessage();
                    $cm->id_customer_thread = $ct->id;
                    $cm->message = $message;
                    if (isset($file_attachment['rename']) && !empty($file_attachment['rename']) && rename($file_attachment['tmp_name'], _PS_UPLOAD_DIR_ . basename($file_attachment['rename']))) {
                        $cm->file_name = $file_attachment['rename'];
                        @chmod(_PS_UPLOAD_DIR_ . basename($file_attachment['rename']), 0664);
                    }
                    $cm->ip_address = (int) ip2long(Tools::getRemoteAddr());
                    $cm->user_agent = $_SERVER['HTTP_USER_AGENT'];
                    if (!$cm->add()) {
                        $errors['unknown'] = 'An error occurred while sending the message.';
                    }
                } else {
                    $errors['unknown'] = 'An error occurred while sending the message.';
                }
            }
        }

        if (count($errors) > 0) {
            return $errors;
        } else {
            return true;
        }
    }

    public static function getContactTypes($context)
    {
        return Contact::getContacts($context->language->id);
    }
}

function getOrder($id)
{
    $id_order = false;
    if (!is_numeric($reference = $id)) {
        $reference = ltrim($reference, '#');
        $orders = Order::getByReference($reference);
        if ($orders) {
            foreach ($orders as $order) {
                $id_order = (int) $order->id;
                break;
            }
        }
    } elseif (Order::getCartIdStatic((int) $id)) {
        $id_order = (int) $id;
    }
    return (int) $id_order;
}
