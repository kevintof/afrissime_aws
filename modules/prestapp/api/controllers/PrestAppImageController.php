<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppImageController
{
    public static function getBestImageUrl($id_product, $id_combination, $context)
    {
        $image = Image::getBestImageAttribute($context->shop->id, $context->language->id, $id_product, $id_combination);
        $imageTypes = ImageType::getImagesTypes();

        $link = new Link();
        $product = new Product($id_product, true, (int) $context->language->id, $context->shop->id, $context);

        $img = new stdClass();
        $img->id_image = (int) $image['id_image'];
        $img->types = array();

        foreach ($imageTypes as $imageType) {
            if ($imageType['products']) {
                $type = new stdClass();
                $type->url = $link->getImageLink($product->link_rewrite, $img->id_image, $imageType['name']);
                $type->height = (int) $imageType['height'];
                $type->width = (int) $imageType['width'];
                array_push($img->types, (object) array($imageType['name'] => $type));
            }
        }

        return $img;
    }

    public static function getImageUrl($id_image, $context)
    {
        $imageTypes = ImageType::getImagesTypes();
        $image = new Image((int) $id_image);

        $product = new Product($image->id_product, true, (int) $context->language->id, $context->shop->id, $context);

        $link = new Link();

        $img = new stdClass();
        $img->id_image = (int) $id_image;
        $img->types = array();

        foreach ($imageTypes as $imageType) {
            if ($imageType['products']) {
                $type = new stdClass();
                $type->url = $link->getImageLink($product->link_rewrite, $img->id_image, $imageType['name']);
                $type->height = (int) $imageType['height'];
                $type->width = (int) $imageType['width'];
                array_push($img->types, (object) array($imageType['name'] => $type));
            }
        }

        return $img;
    }
}
