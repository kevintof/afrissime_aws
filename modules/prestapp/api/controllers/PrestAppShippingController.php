<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppShippingController
{
    /**
     * getUsedCarriers - Return the list of used shipping methods.
     *
     * @return array Address
     */
    public static function getUsedCarriers()
    {
        if (Validate::isLoadedObject($context->customer)) {
            return $context->customer->getAddresses((int) $context->language['id_lang']);
        } else {
            return 'NOT_CONNECTED';
        }
    }

    public static function completeActiveCarriers($carriers, $context) {
        if (class_exists('PrestAppEcrelaypointModuleController')) {
            $ec = (int)Configuration::get('ECRELAYPOINT_TRANSPORTEUR', null, $context->shop->id_shop_group, (int)$context->shop->id);
            if (isset($ec)) {
                if ($ec) {
                    foreach ($carriers as $key => $value) {
                        if ($carriers[$key]['id_carrier'] == $ec) {
                            $carriers[$key]['external_module_name'] = 'ecrelaypoint';
                        }
                    }
                }
            }
        }

        return $carriers;
    }

    public static function completeDeliveryOptionList($carriers, $context) {
        if (class_exists('PrestAppEcrelaypointModuleController')) {
            $ec = (int)Configuration::get('ECRELAYPOINT_TRANSPORTEUR', null, $context->shop->id_shop_group, (int)$context->shop->id);
            if (isset($ec)) {
                if ($ec) {
                    foreach ($carriers as $key => $value) {
                        if($carriers[$key]->id == $ec) {
                            $carriers[$key]->moduleName = 'ecrelaypoint';
                        }
                    }
                }
            }
        }

        return $carriers;
    }
}
