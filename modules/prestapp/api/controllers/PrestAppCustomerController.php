<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use PrestaShop\PrestaShop\Adapter\ServiceLocator;

class PrestAppCustomerController
{
    /**
     * Login function - return the customer object if authentication is ok.
     *
     * @param [string] $email
     * @param [string] $password
     *
     * @return Customer
     */
    public static function login($email, $password, $facebook = false)
    {

        $email = trim($email);
        $password = trim($password);
        $customer = new Customer();
        $ignore_guest = true;

        if ($facebook) {
            $passwd = $password;
        } else {
            $passwd = Tools::encrypt($password);
        }

        if (class_exists('PrestAppPasswordsecurityCustomizationController')) {
            $passwordsecurity = new PrestAppPasswordsecurityCustomizationController();
            $result = $passwordsecurity->getByEmail($email, $password, $ignore_guest);
            if ($result) {
                $customer->id = $result['id_customer'];
                foreach ($result as $key => $value) {
                    if (property_exists($customer, $key)) {
                        $customer->{$key} = $value;
                    }
                }
            }
        } else {
            if (version_compare(_PS_VERSION_, '1.7', '<')) {
                $result = Db::getInstance()->getRow('
                        SELECT *
                        FROM `' . _DB_PREFIX_ . 'customer`
                        WHERE `email` = \'' . pSQL($email) . '\'
                        ' . Shop::addSqlRestriction(Shop::SHARE_CUSTOMER) . '
                        ' . (isset($password) ? 'AND `passwd` = \'' . pSQL($passwd) . '\'' : '') . '
                        AND `deleted` = 0
                        ' . ($ignore_guest ? ' AND `is_guest` = 0' : ''));
                if ($result) {
                    $customer->id = $result['id_customer'];
                    foreach ($result as $key => $value) {
                        if (property_exists($customer, $key)) {
                            $customer->{$key} = $value;
                        }
                    }
                }
            } else {
                $shopGroup = Shop::getGroupFromShop(Shop::getContextShopID(), false);

                $sql = new DbQuery();
                $sql->select('c.`passwd`');
                $sql->from('customer', 'c');
                $sql->where('c.`email` = \'' . pSQL($email) . '\'');
                if (Shop::getContext() == Shop::CONTEXT_SHOP && $shopGroup['share_customer']) {
                    $sql->where('c.`id_shop_group` = ' . (int) Shop::getContextShopGroupID());
                } else {
                    $sql->where('c.`id_shop` IN (' . implode(', ', Shop::getContextListShopID(Shop::SHARE_CUSTOMER)) . ')');
                }

                if ($ignoreGuest) {
                    $sql->where('c.`is_guest` = 0');
                }
                $sql->where('c.`deleted` = 0');

                $passwordHash = Db::getInstance()->getValue($sql);

                try {
                    /** @var \PrestaShop\PrestaShop\Core\Crypto\Hashing $crypto */
                    $crypto = ServiceLocator::get('\\PrestaShop\\PrestaShop\\Core\\Crypto\\Hashing');
                } catch (CoreException $e) {
                    return 'NOTFOUND';
                }

                $shouldCheckPassword = !is_null($password);

                if ($facebook) {
                    if ($shouldCheckPassword && $passwd != $passwordHash) {
                        return 'NOTFOUND';
                    }
                } else {
                    if ($shouldCheckPassword && !$crypto->checkHash($password, $passwordHash)) {
                        return 'NOTFOUND';
                    }
                }

                $sql = new DbQuery();
                $sql->select('c.*');
                $sql->from('customer', 'c');
                $sql->where('c.`email` = \'' . pSQL($email) . '\'');
                if (Shop::getContext() == Shop::CONTEXT_SHOP && $shopGroup['share_customer']) {
                    $sql->where('c.`id_shop_group` = ' . (int) Shop::getContextShopGroupID());
                } else {
                    $sql->where('c.`id_shop` IN (' . implode(', ', Shop::getContextListShopID(Shop::SHARE_CUSTOMER)) . ')');
                }
                if ($ignoreGuest) {
                    $sql->where('c.`is_guest` = 0');
                }
                $sql->where('c.`deleted` = 0');

                $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);

                if (!$result) {
                    return 'NOTFOUND';
                }

                $customer->id = $result['id_customer'];
                foreach ($result as $key => $value) {
                    if (property_exists($customer, $key)) {
                        $customer->{$key} = $value;
                    }
                }

                if (!$facebook) {
                    if ($shouldCheckPassword && !$crypto->isFirstHash($password, $passwordHash)) {
                        $customer->passwd = $crypto->hash($plaintextPassword);
                        $customer->update();
                    }
                }
            }
        }

        if (isset($result['active']) && !$result['active']) {
            return 'INACTIVE';
        } elseif (!$result || !$customer->id) {
            return 'NOTFOUND';
        } else {
            return $customer;
        }
    }

    /**
     * Register function - Register new user.
     *
     * @return Customer
     */
    public static function register($email, $lastname, $firstname, $company, $siret, $phone, $phone_mobile, $newsletter, $years, $months, $days, $is_new_customer, $modules, $context)
    {
        $errors = array();

        Hook::exec('actionBeforeSubmitAccount');

        $create_account = true;

        // Checked the user address in case he changed his email address
        if (Validate::isEmail($email = Tools::getValue('email')) && !empty($email)) {
            if (Customer::customerExists($email)) {
                $errors[] = 'ACCOUNT_ALREADY_EXISTS';
            }
        }

        // Preparing customer
        $customer = new Customer();
        $lastnameAddress = Tools::getValue('lastname');
        $firstnameAddress = Tools::getValue('firstname');
        $_POST['lastname'] = Tools::getValue('customer_lastname', $lastnameAddress);
        $_POST['firstname'] = Tools::getValue('customer_firstname', $firstnameAddress);
        $addresses_types = array('address');

        if (empty(Tools::getValue('lastname'))) {
            $errors[] = 'LASTNAME_REQUIRED';
        }

        if (empty(Tools::getValue('firstname'))) {
            $errors[] = 'FIRSTNAME_REQUIRED';
        }

        if (empty(Tools::getValue('email'))) {
            $errors[] = 'EMAIL_REQUIRED';
        }

        if (empty(Tools::getValue('passwd'))) {
            $errors[] = 'PASSWD_REQUIRED';
        }

        // $error_phone = false;

        // if ((int) Configuration::get('PS_ONE_PHONE_AT_LEAST') && !Tools::getValue('phone') && !Tools::getValue('phone_mobile')) {
        //     $error_phone = true;
        // }

        // if ($error_phone) {
        //     $errors[] = 'AT_LEAST_ONE_PHONE_REQUIRED';
        // }

        if (!count($errors) && !$customer->validateFieldsRequiredDatabase() && !$customer->validateController()) {
            $blocknewsletter = Module::isInstalled('blocknewsletter') && $module_newsletter = Module::getInstanceByName('blocknewsletter');
            if ($blocknewsletter && $module_newsletter->active && !Tools::getValue('newsletter')) {
                require_once _PS_MODULE_DIR_ . 'blocknewsletter/blocknewsletter.php';
                if (is_callable(array($module_newsletter, 'isNewsletterRegistered')) && Blocknewsletter::GUEST_REGISTERED == $module_newsletter->isNewsletterRegistered(Tools::getValue('email'))) {
                    /* Force newsletter registration as customer as already registred as guest */
                    $_POST['newsletter'] = true;
                }
            }

            if (Tools::getValue('newsletter')) {
                $customer->newsletter = true;
                $customer->ip_registration_newsletter = pSQL(Tools::getRemoteAddr());
                $customer->newsletter_date_add = pSQL(date('Y-m-d H:i:s'));
                /** @var Blocknewsletter $module_newsletter */
                if ($blocknewsletter && $module_newsletter->active) {
                    $module_newsletter->confirmSubscription(Tools::getValue('email'));
                }
            }

            $customer->firstname = Tools::ucwords($customer->firstname);
            $customer->birthday = (empty(Tools::getValue('years')) ? '' : (int) Tools::getValue('years') . '-' . (int) Tools::getValue('months') . '-' . (int) Tools::getValue('days'));
            if (!Validate::isBirthDate($customer->birthday)) {
                $errors[] = 'BIRTHDATE_INVALID';
            }

            $customer->company = $company;
            $customer->siret = $siret;

            // New Guest customer
            $customer->is_guest = 0;
            $customer->active = 1;

            if (!count($errors)) {
                if ($customer->add()) {

                    $context->customer = $customer;

                    // Check module JcAccountGroup
                    $group_id = self::moduleJcAccountGroupActivated((int) $context->language->id);
                    if ($group_id) {
                        $_POST['jcaccountgroup'] = $group_id;
                    }
                    // End check module JcAccountGroup

                    // Check module AllInOneRewards
                    $sponsor = self::moduleAllInOneRewardsActivated($modules);
                    if ($sponsor) {
                        $_POST['sponsorship'] = $sponsor;
                    }
                    // End check module AllInOneRewards

                    // Check module Fidepi
                    $card = self::moduleFidepiActivated($modules);
                    if ($card) {
                        $_POST['fidepi_id'] = $card;
                    }
                    // End check module Fidepi

                    Hook::exec('actionCustomerAccountAdd', array(
                        '_POST' => $_POST,
                        'newCustomer' => $customer,
                    ));

                    $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'prestapp_customers` (
                        `id_prestapp_customer` int(11) unsigned NOT NULL AUTO_INCREMENT,
                        `id_ps_customer` INT( 11 ) UNSIGNED NOT NULL,
                        `date_add` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id_prestapp_customer`)
                      ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';
            
                    if(Db::getInstance()->Execute($sql)) {
                        $sql = 'INSERT INTO `'._DB_PREFIX_.'prestapp_customers` (
                            `id_ps_customer`
                            ) values ('.(int)($customer->id).')';
    
                        Db::getInstance()->Execute($sql);
                    }

                    $return = array(
                        'hasError' => !empty($errors),
                        'errors' => $errors,
                    );

                    Hook::exec('actionBeforeAjaxDie', array('controller' => 'AuthController', 'method' => 'POST', 'value' => Tools::jsonEncode($return)));
                    Hook::exec('actionBeforeAjaxDieAuthControllerPOST', array('value' => Tools::jsonEncode($return)));
                } else {
                    $errors[] = 'ERROR';
                }
            }
        }

        $return = array(
            'hasError' => !empty($errors),
            'errors' => $errors,
            'canConnect' => $customer->active ? true : false
        );

        return $return;
    }

    public static function forgotPassword($sentEmail, $context)
    {
        if (!($email = trim($sentEmail)) || !Validate::isEmail($email)) {
            return 'INVALID_EMAIL';
        } else {
            $customer = new Customer();
            $customer->getByemail($email);
            if (!Validate::isLoadedObject($customer)) {
                return 'NO_ACCOUNT';
            } elseif (!$customer->active) {
                return 'CHANGE_UNAVAILABLE';
            } elseif ((strtotime($customer->last_passwd_gen . '+' . ($min_time = (int) Configuration::get('PS_PASSWD_TIME_FRONT')) . ' minutes') - time()) > 0) {
                return 'CHANGE_TIME';
            } else {
                $mail_params = array(
                    '{email}' => $customer->email,
                    '{lastname}' => $customer->lastname,
                    '{firstname}' => $customer->firstname,
                    '{url}' => $context->link->getPageLink('password', true, null, 'token=' . $customer->secure_key . '&id_customer=' . (int) $customer->id),
                );
                if (Mail::Send($context->language->id, 'password_query', Mail::l('Password query confirmation'), $mail_params, $customer->email, $customer->firstname . ' ' . $customer->lastname)) {
                    return 'MAIL_SENT';
                } else {
                    return 'ERROR';
                }
            }
        }
    }

    public static function getCustomerInformations($context)
    {
        $customer = new Customer;
        $customer = $customer->getByEmail($context->customer->email);

        if (class_exists('PrestAppFidepiCustomizationController')) {
            $id_customer = $customer->id;
            $sql_badge = 'SELECT id_badge FROM '._DB_PREFIX_.'fidepi WHERE id_customer='.$id_customer;
            $id_badge = Db::getInstance()->getValue($sql_badge);
            
            if ($id_badge) {
                $customer->fidepi = $id_badge;
            } else {
                $customer->fidepi = null;
            }
        }

        return $customer;
    }

    public static function saveCustomerInformations($firstname, $lastname, $company, $siret, $newpassword, $email, $newsletter, $days, $months, $years, $modules, $context)
    {
        $customer = $context->customer;

        if ($years != null && $years != '' && $months != null && $months != '' && $days != null && $days != '') {
            $customer->birthday = (int) $years . '-' . (int) $months . '-' . (int) $days;
        }

        if ($firstname != null && $firstname != '') {
            $customer->firstname = Tools::ucwords($firstname);
        }

        if ($lastname != null && $lastname != '') {
            $customer->lastname = Tools::ucwords($lastname);
        }

        if ($email != null && $email != '') {
            $customer->email = $email;
        }

        $customer->company = $company;
        $customer->siret = $siret;

        if ($newsletter != null && $newsletter != '') {
            if ((bool) $newsletter) {
                if (!(bool) $customer->newsletter) {
                    if ($module_newsletter = Module::getInstanceByName('blocknewsletter')) {
                        /** @var Blocknewsletter $module_newsletter */
                        if ($module_newsletter->active) {
                            $module_newsletter->confirmSubscription($customer->email);
                            $customer->newsletter = 1;
                        }
                    }
                }
            } else {
                $customer->newsletter = 0;
            }
        } else {
            $customer->newsletter = 0;
        }

        if ($newpassword != null && $newpassword != '') {
            $customer->passwd = Tools::encrypt($newpassword);
        }

        $customer->update();

        // Check module Fidepi
        $card = self::moduleFidepiActivated($modules);
        if ($card) {
            $_POST['fidepi_id'] = $card;
            $fidepi = new PrestAppFidepiCustomizationController();
            $fidepi->updateCard($context);
        }
        // End check module Fidepi

        return $customer;
    }

    private static function moduleJcAccountGroupActivated($id_lang)
    {
        if (class_exists('PrestAppJcAccountGroupCustomizationController')) {
            $prestapp_jcaccountgroup_customization_controller = new PrestAppJcAccountGroupCustomizationController;
            $groups = $prestapp_jcaccountgroup_customization_controller::isJcAccountGroup($id_lang);
            if ($groups && count($groups) > 0) {
                return $groups[0]['id'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static function moduleAllInOneRewardsActivated($modules) {
        if (class_exists('PrestAppAllInOneRewardsCustomizationController')) {
            if ($modules) {
                $key = array_search('all_in_one_rewards', array_column($modules, 'name'));
                if ($key !== false) {
                    if ($modules[$key]['params']['sponsor']) {
                        return $modules[$key]['params']['sponsor'];
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    private static function moduleFidepiActivated($modules) {
        if (class_exists('PrestAppFidepiCustomizationController')) {
            if ($modules) {
                $key = array_search('fidepi', array_column($modules, 'name'));
                if ($key !== false) {
                    if ($modules[$key]['params']['card']) {
                        return $modules[$key]['params']['card'];
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}
