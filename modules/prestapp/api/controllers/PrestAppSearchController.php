<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppSearchController
{
    public static function searchProducts($value)
    {
        $languages = Language::getLanguages();
        if (count($languages) > 0) {
            $languages = array_values($languages);
            $search = Search::find($languages[0]['id_lang'], $value, null, 10, 'position', 'desc', false, false, null);
            $products = array();
            foreach ($search['result'] as $product) {
                $product = array(
                    'id_product' => $product['id_product'],
                    'name' => $product['name'],
                );
                array_push($products, $product);
            }
            return $products;
        } else {
            return 'NO_LANGUAGES';
        }
    }

    public static function searchCategories($value)
    {
        $languages = Language::getLanguages();
        if (count($languages) > 0) {
            $languages = array_values($languages);
            $search = Category::searchByName($languages[0]['id_lang'], $value);
            $categories = array();
            foreach ($search as $category) {
                $category = array(
                    'id_category' => $category['id_category'],
                    'name' => $category['name'],
                );
                array_push($categories, $category);
            }
            return $categories;
        } else {
            return 'NO_LANGUAGES';
        }
    }
}
