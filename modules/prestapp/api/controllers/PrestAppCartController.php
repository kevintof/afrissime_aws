<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

class PrestAppCartController
{
    /**
     * getCartSummary - Return the summary of a cart from its id.
     *
     * @param int     $id_cart
     * @param Context $context
     *
     * @return array
     */
    public static function getCartSummary($id_cart, $delete, $context)
    {
        //get cart from id
        $cart = new Cart($id_cart);

        if (null === $cart->id) {
            return false;
        } elseif ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {
            $time_start = microtime(true);

            if ($delete) {
                $temp_summary = $cart->getSummaryDetails($context->language->id);
                foreach ($temp_summary['products'] as $key => &$product) {
                    if (($product['quantity_available'] <= 0 && !Product::isAvailableWhenOutOfStock($product['out_of_stock'])) || !(int) $product['active']) {
                        $cart->deleteProduct($product['id_product'], $product['id_product_attribute'], $product['id_customization']);
                    }
                }
            }

            $summary = $cart->getSummaryDetails($context->language->id);

            $summary['id_guest'] = $cart->id_guest;

            foreach ($summary['products'] as &$product) {
                $id_image = explode('-', $product['id_image']);

                $image = new Image($id_image[1]);

                $product['is_available_out_of_stock'] = Product::isAvailableWhenOutOfStock($product['out_of_stock']) ? true : false;

                $urls = PrestAppImageController::getImageUrl((int) $image->id, $context);
                $product['image'] = $urls;

                if ($product['price_without_reduction'] == $product['price_with_reduction']) {
                    $product['reduction_type'] = null;
                }
            }

            foreach ($summary['gift_products'] as &$gift_product) {
                $id_image = explode('-', $gift_product['id_image']);

                $image = new Image($id_image[1]);

                $urls = PrestAppImageController::getImageUrl((int) $image->id, $context);
                $gift_product['image'] = $urls;
            }

            $customized_datas = Product::getAllCustomizedDatas($id_cart);

            if ($customized_datas) {
                foreach($summary['products'] as &$product) {
                    if (isset($customized_datas[$product['id_product']]) && $customized_datas[$product['id_product']]) {
                        if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']]) && $customized_datas[$product['id_product']][$product['id_product_attribute']]) {
                            if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']][$product['id_address_delivery']]) && $customized_datas[$product['id_product']][$product['id_product_attribute']][$product['id_address_delivery']]) {
                                if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']][$product['id_address_delivery']][$product['id_customization']]) && $customized_datas[$product['id_product']][$product['id_product_attribute']][$product['id_address_delivery']][$product['id_customization']]) {
                                    $product['customized_data'] = $customized_datas[$product['id_product']][$product['id_product_attribute']][$product['id_address_delivery']][$product['id_customization']];
                                }
                            }
                        }
                    }
                }
            }

            $currency = Currency::getCurrency((int)$context->cart->id_currency);
            $minimal_purchase = Tools::convertPrice((float)Configuration::get('PS_PURCHASE_MINIMUM'), $currency);

            $summary['minimal_purchase'] = $minimal_purchase;

            // var_dump(json_encode($customized_datas));exit();

            // $summary['customized_datas'] = 

            return $summary;
        } else {
            return false;
        }
    }

    /**
     * deleteProductInCart - delete all quantity of a specific product in cart.
     *
     * @param int     $id_cart
     * @param int     $id_product
     * @param int     $id_combination
     * @param int     $id_customization
     * @param Context $context
     *
     * @return bool
     */
    public static function deleteProductInCart($id_cart, $id_product, $context, $id_combination = null, $id_customization = false)
    {
        $cart = new Cart($id_cart);

        if ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {
            $cart->deleteProduct($id_product, $id_combination, $id_customization);

            return $cart;
        } else {
            return false;
        }
    }

    /**
     * FindOrCreateCart - If user is authenticated, we try to get the last cart non orderer, else we create another one.
     *
     * @param Context $context
     *
     * @return Cart or false
     */
    public static function findOrCreateCart($context)
    {
        if ($context->customer && $context->customer->id) {
            $id_cart = Cart::lastNoneOrderedCart($context->customer->id);
        } else {
            $id_cart = null;
        }

        $cart = new Cart($id_cart);

        // check if cart is older than 7 days
        $a = new DateTime($cart->date_upd);
        $b = new DateTime();

        $cart_date_upd = $a->getTimestamp();
        $current_date = $b->getTimestamp();

        $diff = $current_date - $cart_date_upd;

        $max_cart_age = 60 * 60 * 24 * 7;

        // if cart is undefined or older than 7 days, create a new one
        if (null === $cart->id || $diff > $max_cart_age) {
            $cart = new Cart();
            $cart->id_currency = $context->currency->id;
            $cart->id_lang = $context->language->id;

            if ($context->customer) {
                $cart->id_customer = $context->customer->id;
            }

            $cart->save();
        }

        return $cart;
    }

    /**
     * updateProductQuantity - allow to update all quantity in cart (remove and add specific product quantity).
     *
     * @param int     $id_cart
     * @param int     $id_product
     * @param int     $quantity
     * @param int     $id_product_attribute
     * @param bool    $id_customization
     * @param string  $operator             UP|DOWN
     * @param Context $context
     *
     * @return bool
     */
    public static function updateProductQuantity($id_cart, $id_product, $context, $quantity = 1, $id_product_attribute = null, $id_customization = false, $operator = 'up')
    {
        $cart = new Cart($id_cart);

        if ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {
            return $cart->updateQty($quantity, $id_product, $id_product_attribute, $id_customization, $operator);
        } else {
            return false;
        }
    }

    /**
     * claimOwnership - Allow to take the ownership of a cart if its not belongs to someone else.
     *
     * @param int     $id_cart
     * @param Context $context
     *
     * @return bool
     */
    public static function claimOwnership($id_cart, $context)
    {
        $cart = new Cart($id_cart);

        if (null !== $context->customer && !(int) $cart->id_customer) {
            $cart->id_customer = (int) $context->customer->id;
            $cart->save();

            return true;
        } else {
            return false;
        }
    }

    /**
     * addCartRule - add cart rule from the code and check validity before adding it to cart.
     *
     * @param int $id_cart
     * @param int $id_cart_rule
     * @param int $context
     *
     * @return bool
     */
    public static function addCartRule($id_cart, $id_cart_rule, $context)
    {
        $cart_rule = new CartRule($id_cart_rule);
        $cart = new Cart($id_cart);

        if ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {
            if ($cart_rule->checkValidity($context, false, false, true)) {
                return $cart->addCartRule($id_cart_rule);
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * removeCartRule - remove cart rule from its id in cart.
     *
     * @param int     $id_cart
     * @param int     $id_cart_rule
     * @param Context $context
     *
     * @return bool
     */
    public static function removeCartRule($id_cart, $id_cart_rule, $context)
    {
        $cart = new Cart($id_cart);

        if ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {
            return $cart->removeCartRule($id_cart_rule);
        } else {
            return false;
        }
    }

    /**
     * updateAddressDeliveryId Update the delivery address id of the cart.
     *
     * @param int $id_cart        Current cart id to update
     * @param int $id_address     Current address id to change
     * @param int $id_address_new New address id
     */
    public static function updateAddressDeliveryId($id_cart, $id_address, $id_address_new, $context)
    {
        $cart = new Cart($id_cart);

        $new_address = new Address($id_address_new);
        if (!Validate::isLoadedObject($new_address)) {
            return false;
        } elseif (null !== $context->customer && ((int) $new_address->id_customer !== (int) $context->customer->id)) {
            return false;
        }

        if ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {

            $cart->id_address_delivery = $id_address_new;

            if (!$cart->update()) {
                return false;
            } else {
                if (!$cart->isMultiAddressDelivery()) {
                    $cart->setNoMultishipping();
                }
                return true;
            }
        }

        return false;
    }

    /**
     * updateAddressInvoiceId Update the invoice address id of the cart.
     *
     * @param int $id_cart        Current cart id to update
     * @param int $id_address     Current address id to change
     * @param int $id_address_new New address id
     */
    public static function updateAddressInvoiceId($id_cart, $id_address, $id_address_new, $context)
    {
        $cart = new Cart($id_cart);

        $new_address = new Address($id_address_new);
        if (!Validate::isLoadedObject($new_address)) {
            return false;
        } elseif (null !== $context->customer && ((int) $new_address->id_customer !== (int) $context->customer->id)) {
            return false;
        }

        $cart->id_address_invoice = $id_address_new;

        if (!$cart->update()) {
            return false;
        } else {
            return true;
        }

        return false;
    }

    public static function updateAddressDeliveryInvoiceId($id_cart, $id_address_delivery, $id_address_invoice, $id_address_new_delivery, $id_address_new_invoice, $context)
    {

        $cart = new Cart($id_cart);

        $new_address_delivery = new Address($id_address_new_delivery);
        if (!Validate::isLoadedObject($new_address_delivery)) {
            return false;
        } elseif (null !== $context->customer && ((int) $new_address_delivery->id_customer !== (int) $context->customer->id)) {
            return false;
        }
        $new_address_invoice = new Address($id_address_new_invoice);
        if (!Validate::isLoadedObject($new_address_invoice)) {
            return false;
        } elseif (null !== $context->customer && ((int) $new_address_invoice->id_customer !== (int) $context->customer->id)) {
            return false;
        }

        if ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {
            $cart->id_address_delivery = $id_address_new_delivery;
            $cart->id_address_invoice = $id_address_new_invoice;

            if (!$cart->update()) {
                return false;
            } else {
                if (!$cart->isMultiAddressDelivery()) {
                    $cart->setNoMultishipping();
                }
                return true;
            }
        }

        return false;
    }

    /**
     * getDeliveryOptionList Get carriers list available for this cart.
     *
     * @param int $id_cart Current cart id to update
     */
    public static function getDeliveryOptionList($id_cart, $context)
    {
        $cart = new Cart($id_cart);

        if ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {
            $carriers = $cart->getDeliveryOptionList();
            $formattedCarriers = array();


            foreach ($carriers[$context->cart->id_address_delivery] as $key => $carrier) {
                $c = (object) array();
                $c->id = (int) array_values($carrier['carrier_list'])[0]['instance']->id;
                $c->referenceId = (int) array_values($carrier['carrier_list'])[0]['instance']->id_reference;
                $c->name = array_values($carrier['carrier_list'])[0]['instance']->name;
                $c->priceWithTax = $carrier['total_price_with_tax'];
                $c->priceWithoutTax = $carrier['total_price_without_tax'];
                $c->isFree = $carrier['is_free'];
                $c->isGeneric = 0 === (int) array_values($carrier['carrier_list'])[0]['instance']->is_module;
                $c->moduleName = array_values($carrier['carrier_list'])[0]['instance']->external_module_name;
                $c->deliveryOptionId = $key;
                $c->deliveryOptionAddress = (int) $context->cart->id_address_delivery;
                $c->shopLogo = array_values($carrier['carrier_list'])[0]['logo'];

                // delay

                $delay = array_values($carrier['carrier_list'])[0]['instance']->delay;
                if (isset($delay[$context->language->id])) {
                    $delay = $delay[$context->language->id];
                } else {
                    $delay = $delay[(int)Configuration::get('PS_LANG_DEFAULT')];
                }
                
                $c->delay = $delay;

                array_push($formattedCarriers, $c);
            }

            $formattedCarriers = PrestAppShippingController::completeDeliveryOptionList($formattedCarriers, $context);

            return $formattedCarriers;
        }

        return false;
    }

    public static function getExtraCarriers($id_cart, $carriers, $context)
    {

        $carriers_ids = json_decode($carriers);

        if (count($carriers_ids) > 0) {
            $extra_carriers = array();
            foreach ($carriers_ids as $carrier_id) {
                $extra_carriers[$carrier_id] = array();
                if (class_exists('PrestAppPrestadriveCustomizationController')) {
                    $prestadrive = new PrestAppPrestadriveCustomizationController();

                    $extra_carriers[$carrier_id]['prestadrive'] = array(
                        'is_enabled' => $prestadrive->isEnabledForCarrier($carrier_id),
                    );

                    if ($extra_carriers[$carrier_id]['prestadrive']['is_enabled']) {
                        $extra_carriers[$carrier_id]['prestadrive']['cart_data'] = $prestadrive->getCartValues($id_cart);
                        $extra_carriers[$carrier_id]['prestadrive']['params_data'] = $prestadrive->getData($context);
                    }
                }
            }
            return $extra_carriers;
        } else {
            return false;
        }
    }

    /**
     * setDeliveryOption Get carriers list available for this cart.
     *
     * @param int   $id_cart         Current cart id to update
     * @param array $delivery_option Delivery option choose
     */
    public static function setDeliveryOption($id_cart, $delivery_option, $context)
    {
        $cart = new Cart($id_cart);

        if ((null === $context->customer && 0 === (int) $cart->id_customer) || (null !== $context->customer && ((int) $cart->id_customer === (int) $context->customer->id))) {
            $cart->setDeliveryOption($delivery_option);
            if ($cart->save()) {
                return true;
            }
        }

        return false;
    }

    /**
     * getPaymentModulesList Get payment modules list available for this context.
     */
    public static function getPaymentModulesList($context)
    {
        if (isset($context->cart)) {
            $billing = new Address((int) $context->cart->id_address_invoice);
        }

        $use_groups = Group::isFeatureActive();

        $frontend = true;
        $groups = array();
        if (isset($context->employee)) {
            $frontend = false;
        } elseif (isset($context->customer) && $use_groups) {
            $groups = $context->customer->getGroups();
            if (!count($groups)) {
                $groups = array(Configuration::get('PS_UNIDENTIFIED_GROUP'));
            }
        }

        if (Db::getInstance()->getValue('SELECT `id_hook` FROM `' . _DB_PREFIX_ . 'hook` WHERE `name` = \'displayPayment\'')) {
            $hook_payment = 'displayPayment';
        }

        if (version_compare(_PS_VERSION_, '1.6.9', '>')) {
            if (Db::getInstance()->getValue('SELECT `id_hook` FROM `' . _DB_PREFIX_ . 'hook` WHERE `name` = \'paymentOptions\'')) {
                $hook_payment = 'paymentOptions';
            }
        }

        $list = Shop::getContextListShopID();

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT DISTINCT m.`id_module`, h.`id_hook`, m.`name`, hm.`position`
            FROM `' . _DB_PREFIX_ . 'module` m
            ' . ($frontend ? 'LEFT JOIN `' . _DB_PREFIX_ . 'module_country` mc ON (m.`id_module` = mc.`id_module` AND mc.id_shop = ' . (int) $context->shop->id . ')' : '') . '
            ' . ($frontend && $use_groups ? 'INNER JOIN `' . _DB_PREFIX_ . 'module_group` mg ON (m.`id_module` = mg.`id_module` AND mg.id_shop = ' . (int) $context->shop->id . ')' : '') . '
            ' . ($frontend && isset($context->customer) && $use_groups ? 'INNER JOIN `' . _DB_PREFIX_ . 'customer_group` cg on (cg.`id_group` = mg.`id_group`AND cg.`id_customer` = ' . (int) $context->customer->id . ')' : '') . '
            LEFT JOIN `' . _DB_PREFIX_ . 'hook_module` hm ON hm.`id_module` = m.`id_module`
            LEFT JOIN `' . _DB_PREFIX_ . 'hook` h ON hm.`id_hook` = h.`id_hook`
            WHERE h.`name` = \'' . pSQL($hook_payment) . '\'
            ' . (isset($billing) && $frontend ? 'AND mc.id_country = ' . (int) $billing->id_country : '') . '
            AND (SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'module_shop ms WHERE ms.id_module = m.id_module AND ms.id_shop IN(' . implode(', ', $list) . ')) = ' . count($list) . '
            AND hm.id_shop IN(' . implode(', ', $list) . ')
            ' . ((count($groups) && $frontend && $use_groups) ? 'AND (mg.`id_group` IN (' . implode(', ', $groups) . '))' : '') . '
            GROUP BY hm.id_hook, hm.id_module
            ORDER BY hm.`position`, m.`name` DESC');
    }
}
