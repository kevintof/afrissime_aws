<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/category/{id_category:[0-9]+}', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_category = $request->getAttribute('id_category');

    if (Module::isInstalled('privatesales') && Module::isEnabled('privatesales')) {
        $privatesale = SaleCore::loadSaleFromCategoryId($id_category, (int) $context->cookie->id_lang);
        if ($privatesale) {
            $privatesale->link_img = __PS_BASE_URI__ . 'modules/privatesales/img/';
            $privatesale->link_mod_img = _PS_MODULE_DIR_ . 'modules/privatesales/img/';
            $category = new Category($privatesale->id_category, $context->language->id);
            $privatesale->sub_categories = $category->getSubCategories($context->language->id, true);
        }
    }

    $category = new Category($id_category, $context->language->id);

    $childCategories = getSubCategories($id_category, $context);
    $parentCategories = getParentCategories($id_category, $context);

    $data = array(
        'id_category' => (int) $category->id_category,
        'name' => $category->name,
        'description' => $category->description,
        'childCategories' => $childCategories,
        'parentCategories' => $parentCategories,
    );

    if (isset($privatesale) && $privatesale) {
        $data['privatesale'] = $privatesale;
    }

    $response = sendOk($response, $data);
    return $response;
});

//get product by category
$app->get('/products/category/{id_category:[0-9]+}/{page_number:[0-9]+}/{product_per_page:[0-9]+}/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_category = $request->getAttribute('id_category');
    $page_number = $request->getAttribute('page_number');
    $product_per_page = $request->getAttribute('product_per_page');
    $populate = $request->getAttribute('populate');
    $filters = $request->getParam('filters');
    $column = $request->getHeaderLine('column');
    $order = $request->getHeaderLine('order');

    if (Module::isInstalled('privatesales') && Module::isEnabled('privatesales')) {
        $privatesale = SaleCore::loadSaleFromCategoryId($id_category, (int) $context->cookie->id_lang);
        if ($privatesale) {
            $privatesale->link_img = __PS_BASE_URI__ . 'modules/privatesales/img/';
            $privatesale->link_mod_img = _PS_MODULE_DIR_ . 'modules/privatesales/img/';
            $category = new Category($privatesale->id_category, $context->language->id);
            $privatesale->sub_categories = $category->getSubCategories($context->language->id, true);
        }
    }

    $category = new Category($id_category, $context->language->id);

    if (class_exists('PrestAppBlocklayeredCustomizationController')) {
        $blocklayered = new PrestAppBlocklayeredCustomizationController($page_number, $product_per_page, $column, $order);
        $layer_response = $blocklayered->getProducts($context, $populate, $filters, $id_category);
        $layer = $layer_response['layer'];
        $products = $layer_response['products'];
        $filter_nbr_products = Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'cat_filter_restriction', false);
    } else {
        $products = PrestAppProductController::getProductFromCategory($id_category, $page_number, $product_per_page, $column, $order, $populate, $context);
    }

    $nbr_products = $category->getProducts(null, null, null, null, null, true, true, false, 1, true, $context);

    // IMPORTANT - PRODUCT NUMBER IS INCORRECT DUE TO HIDING PACKS ($category->getProducts)
    // CAREFULL - MAY INTERFER WITH PRODUCT PAGES COUNT IF MOFIFIED

    $childCategories = getSubCategories($id_category, $context);
    $parentCategories = getParentCategories($id_category, $context);

    $data = array(
        'id_category' => (int) $category->id_category,
        'name' => $category->name,
        'description' => $category->description,
        'productsNumber' => $nbr_products,
        'products' => $products,
        'childCategories' => $childCategories,
        'parentCategories' => $parentCategories,
    );

    if (isset($privatesale) && $privatesale) {
        $data['privatesale'] = $privatesale;
    }

    if (isset($layer) && $layer) {
        $data['layer'] = $layer;
        if (isset($filter_nbr_products)) {
            $data['layer']['filters_products_number'] = (int) $filter_nbr_products;
        }
    }

    ////////////////////
    //    agepopop    //
    ////////////////////

    if (class_exists('PrestAppAgepopupCustomizationController')) {
        $agepopup = new PrestAppAgepopupCustomizationController();
        $data['agepopup'] = $agepopup->getDataCategory($id_category, $context);
    }

    $response = sendOk($response, $data);

    return $response;
});

$app->get('/categories', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $id_lang = (int) $context->language->id;

    $root_category = Category::getRootCategory();

    $category = (object) array();
    $category->id = $root_category->id;
    $category->name = $root_category->name;
    $category->subcategories = array();

    $first_degree_categories = $root_category->getSubCategories($id_lang, true);

    foreach ($first_degree_categories as $first_degree_category) {
        $first_degree_category = new Category((int) $first_degree_category['id_category'], $id_lang);

        $fdc = (object) array();
        $fdc->id = (int) $first_degree_category->id;
        $fdc->name = $first_degree_category->name;
        if (hasChildren($fdc->id, $id_lang, true, $context->shop->id)) {
            $fdc->subcategories = array();
        }

        $second_degree_categories = $first_degree_category->getSubCategories($id_lang, true);

        foreach ($second_degree_categories as $second_degree_category) {
            $second_degree_category = new Category((int) $second_degree_category['id_category'], $id_lang);

            $sdc = (object) array();
            $sdc->id = (int) $second_degree_category->id;
            $sdc->name = $second_degree_category->name;

            if (hasChildren($sdc->id, $id_lang, true, $context->shop->id)) {
                $sdc->subcategories = array();
            }

            $third_degree_categories = $second_degree_category->getSubCategories($id_lang, true);

            foreach ($third_degree_categories as $third_degree_category) {
                $third_degree_category = new Category((int) $third_degree_category['id_category'], $id_lang);

                $tdc = (object) array();
                $tdc->id = (int) $third_degree_category->id;
                $tdc->name = $third_degree_category->name;

                if (hasChildren($tdc->id, $id_lang, true, $context->shop->id)) {
                    $tdc->subcategories = array();
                }

                $fourth_degree_categories = $third_degree_category->getSubCategories($id_lang, true);

                foreach ($fourth_degree_categories as $fourth_degree_category) {
                    $fourth_degree_category = new Category((int) $fourth_degree_category['id_category'], $id_lang);

                    $fodc = (object) array();
                    $fodc->id = (int) $fourth_degree_category->id;
                    $fodc->name = $fourth_degree_category->name;

                    if (hasChildren($fdc->id, $id_lang, true, $context->shop->id)) {
                        $fodc->subcategories = array();
                    }

                    array_push($tdc->subcategories, $fodc);
                }

                array_push($sdc->subcategories, $tdc);
            }

            array_push($fdc->subcategories, $sdc);
        }

        array_push($category->subcategories, $fdc);
    }

    $data = array(
        'categories' => $category,
    );

    $response = sendOk($response, $data);

    return $response;
});

//fix 1.0.6 - add retro compatibility with the function in Category class
function hasChildren($id_parent, $id_lang, $active = true, $id_shop = false)
{
    if (!Validate::isBool($active)) {
        die(Tools::displayError());
    }

    $cache_id = 'Category::hasChildren_' . (int) $id_parent . '-' . (int) $id_lang . '-' . (bool) $active . '-' . (int) $id_shop;
    if (!Cache::isStored($cache_id)) {
        $query = 'SELECT c.id_category, "" as name
			FROM `' . _DB_PREFIX_ . 'category` c
			LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (c.`id_category` = cl.`id_category`' . Shop::addSqlRestrictionOnLang('cl') . ')
			' . Shop::addSqlAssociation('category', 'c') . '
			WHERE `id_lang` = ' . (int) $id_lang . '
			AND c.`id_parent` = ' . (int) $id_parent . '
			' . ($active ? 'AND `active` = 1' : '') . ' LIMIT 1';
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        Cache::store($cache_id, $result);
        return $result;
    }
    return Cache::retrieve($cache_id);
}

function getSubCategories($id_category, $context)
{
    $category = new Category($id_category, $context->language->id);

    $children = $category->getSubCategories($context->language->id);

    foreach ($children as $key => $child) {
        $child_cat = new Category($children[$key]['id_category'], $context->language->id);
        $children[$key]['nb_products'] = $child_cat->getProducts(null, null, null, null, null, true, true, false, 1, true, $context);

        $children[$key]['childCategories'] = getSubCategories($children[$key]['id_category'], $context);
    }

    return $children;
}

function getParentCategories($id_category, $context)
{
    $category = new Category($id_category, $context->language->id);

    $parents = $category->getParentsCategories($context->language->id);

    $categories = array();

    foreach ($parents as $key => $child) {
        array_push($categories, $parents[$key]['id_category']);
    }

    return $categories;
}
