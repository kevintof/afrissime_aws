<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /customization/onlinechat/status
 */
$app->get('/customization/onlinechat/status', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    if (class_exists('PrestAppOnlinechatCustomizationController')) {
        $onlinechat = new PrestAppOnlinechatCustomizationController();
        $status = $onlinechat->getStatus($context);
        $data = $status;

        $response = sendOk($response, $data);
    } else {
        $data = 'NOT_FOUND';
        $response = sendNotFound($response, $data);
    }

    return $response;
});

$app->post('/customization/onlinechat/message', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $email = $request->getParam('email');
    $name = $request->getParam('name');
    $phone = $request->getParam('phone');
    $message = $request->getParam('message');

    if (class_exists('PrestAppOnlinechatCustomizationController')) {
        $onlinechat = new PrestAppOnlinechatCustomizationController();
        $status = $onlinechat->sendOfflineMessage($email, $name, $phone, $message, $context);
        $data = $status;

        $response = sendOk($response, $data);
    } else {
        $data = 'NOT_FOUND';
        $response = sendNotFound($response, $data);
    }

    return $response;
});

$app->post('/customization/onlinechat/checkBeforeCreateThread', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    if (class_exists('PrestAppOnlinechatCustomizationController')) {
        $onlinechat = new PrestAppOnlinechatCustomizationController();
        $result = $onlinechat->checkBeforeCreateThread($context);
        $data = $result;
        $response = sendOk($response, $data);
    } else {
        $data = 'NOT_FOUND';
        $response = sendNotFound($response, $data);
    }

    return $response;
});

$app->post('/customization/onlinechat/createThread', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $content = $request->getParam('message');

    if (class_exists('PrestAppOnlinechatCustomizationController')) {
        $onlinechat = new PrestAppOnlinechatCustomizationController();
        $result = $onlinechat->createThread($content, $context);
        $data = $result;
        $response = sendOk($response, $data);
    } else {
        $data = 'NOT_FOUND';
        $response = sendNotFound($response, $data);
    }

    return $response;
});

$app->post('/customization/onlinechat/postMessage', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $content = $request->getParam('message');
    $thread = $request->getParam('thread');

    if (class_exists('PrestAppOnlinechatCustomizationController')) {

        $onlinechat = new PrestAppOnlinechatCustomizationController();
        $result = $onlinechat->postMessage($thread, $content, $context);
        $data = array(
            'message' => json_decode($result),
        );
        $response = sendOk($response, $data);
    } else {
        $data = 'NOT_FOUND';
        $response = sendNotFound($response, $data);
    }

    return $response;
});

$app->post('/customization/onlinechat/getEmployeesLastMessages', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $thread = $request->getParam('thread');
    $message_key = $request->getParam('message_key');

    if (class_exists('PrestAppOnlinechatCustomizationController')) {

        $onlinechat = new PrestAppOnlinechatCustomizationController();
        $result = $onlinechat->getEmployeesLastMessages($thread, $message_key, $context);
        $data = json_decode($result);
        $response = sendOk($response, $data);
    } else {
        $data = 'NOT_FOUND';
        $response = sendNotFound($response, $data);
    }

    return $response;
});
