<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * POST /customization/facebookpsconnect/facebook-connect
 */
$app->post('/customization/facebookpsconnect/facebook-connect', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $access_token = $request->getParam('access_token');
    $code = $request->getParam('code');

    if (class_exists('PrestAppFacebookpsconnectCustomizationController')) {
        $facebookpsconnect = new PrestAppFacebookpsconnectCustomizationController();
        $customer = $facebookpsconnect->facebookConnect($access_token, $code, $context);

        if ($customer) {
            $catalog_mode = false;

            if (Configuration::get('PS_CATALOG_MODE')) {
                $catalog_mode = true;
            } else if (Group::isFeatureActive()) {
                $group = new Group($customer->id_default_group);
                $show_prices = $group->show_prices;
                $catalog_mode = $show_prices ? false : true;
            } else {
                $catalog_mode = false;
            }

            $customer->catalog_mode = $catalog_mode;
        }

        $data = array(
            'customer' => $customer,
        );

        $response = sendOk($response, $data);
    } else {
        $data = 'NOT_FOUND';
        $response = sendNotFound($response, $data);
    }

    return $response;
});
