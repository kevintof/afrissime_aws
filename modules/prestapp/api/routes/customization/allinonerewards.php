<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /customization/allinonerewards
 */
$app->get('/customization/allinonerewards', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    if (class_exists('PrestAppAllInOneRewardsCustomizationController')) {
        $all_in_one_rewards = new PrestAppAllInOneRewardsCustomizationController();
        $data = $all_in_one_rewards->getCustomerData($context);
        $response = sendOk($response, $data);
    } else {
        $response = sendServerError($response, 'ERROR');
    }

    return $response;
});

$app->get('/customization/allinonerewards/credits', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    if (class_exists('PrestAppAllInOneRewardsCustomizationController')) {
        $all_in_one_rewards = new PrestAppAllInOneRewardsCustomizationController();
        $data = $all_in_one_rewards->getCustomerCredits($context);
        $response = sendOk($response, $data);
    } else {
        $response = sendServerError($response, 'ERROR');
    }

    return $response;
});

$app->post('/customization/allinonerewards/send', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $emails = $request->getParam('emails');

    if (class_exists('PrestAppAllInOneRewardsCustomizationController')) {
        $all_in_one_rewards = new PrestAppAllInOneRewardsCustomizationController();
        $data = $all_in_one_rewards->sendInvitation($emails, $context);

        if ($data == 'INVALID') {
            $response = sendBadRequest($response, $data);
        } else if ($data == 'UNKNOWN') {
            $response = sendServerError($response, $data);
        } else {
            $response = sendOk($response, $data);
        }
    } else {
        $response = sendServerError($response, 'ERROR');
    }

    return $response;
});

$app->post('/customization/allinonerewards/revive', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_sponsorship = $request->getParam('id_sponsorship');

    if (class_exists('PrestAppAllInOneRewardsCustomizationController')) {
        $all_in_one_rewards = new PrestAppAllInOneRewardsCustomizationController();
        $data = $all_in_one_rewards->revive($id_sponsorship, $context);

        $response = sendOk($response, $data);
    }

    return $response;
});

$app->post('/customization/allinonerewards/voucher', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    if (class_exists('PrestAppAllInOneRewardsCustomizationController')) {
        $all_in_one_rewards = new PrestAppAllInOneRewardsCustomizationController();
        $data = $all_in_one_rewards->askVoucher($context);
        $response = sendOk($response, $data);
    }

    return $response;
});
