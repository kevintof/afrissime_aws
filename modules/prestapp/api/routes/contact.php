<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /contact - send contact form
 */
$app->get('/contact', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $data['orders'] = PrestAppOrderController::getOrderHistory($context);
    $data['contacts'] = PrestAppContactController::getContactTypes($context);

    $response = sendOk($response, $data);

    return $response;
});

/*
 * POST /contact - send contact form
 */
$app->post('/contact', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $message = $request->getParam('message');
    $email = $request->getParam('email');
    $id_contact = $request->getParam('id_contact');
    $id_order = $request->getParam('id_order');
    $id_product = $request->getParam('id_product');

    $form = PrestAppContactController::sendContactForm($context, $message, $email, $id_contact, $id_order, $id_product);

    if ($form === true) {
        $data['message'] = 'Message sent';
        $response = sendOk($response, $data);
    } else {
        $data['errors'] = $form;
        $data['message'] = 'There is an error in your request';
        $response = sendBadRequest($response, $data);
    }

    return $response;
});
