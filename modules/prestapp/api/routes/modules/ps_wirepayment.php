<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon
 * @copyright Copyright (c) 2017 - 2018 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * POST /modules/payments/bankwire/validate
 */
$app->post('/modules/payments/ps_wirepayment/validate', function (Request $request, Response $response) {

    $context = $request->getAttribute('context');

    $ps_wirepayment = new PrestAppPsWirepaymentModuleController();

    $order = $ps_wirepayment->validateOrder($context);

    $data = array(
        'order' => $order,
    );

    $response = sendOk($response, $data);

    return $response;
});

/*
 * GET /modules/payments/bankwire/display
 */
$app->get('/modules/payments/ps_wirepayment/display', function (Request $request, Response $response) {

    $ps_wirepayment = new PrestAppPsWirepaymentModuleController();

    $front = $ps_wirepayment->displayPayment();

    $data = array(
        'data' => $front,
    );

    $response = sendOk($response, $data);

    return $response;
});
