<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /modules/payments/bbcarrier/display
 */
$app->get('/modules/carriers/bbcarrier/display', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    if (Module::isInstalled('bbcarrier') && Module::isEnabled('bbcarrier')) {
        if (class_exists('PrestAppBbcarrierModuleController')) {
            $bbcarrier = new PrestAppBbcarrierModuleController();
            $content = $bbcarrier->displayCarrier($context);
            $data = array(
                'data' => $content,
            );
            $response = sendOk($response, $data);
        } else {
            $data = array();
            $response = sendNotFound($response, $data);
        }
    } else {
        $data = array();
        $response = sendNotFound($response, $data);
    }
    return $response;
});
