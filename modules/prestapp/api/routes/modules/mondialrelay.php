<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/modules/carriers/mondialrelay/display', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $mondialrelay = new PrestAppMondialrelayModuleController();

    $points = $mondialrelay->displayCarrier($context);

    $data = array(
        'points' => $points,
    );

    $response = sendOk($response, $data);

    return $response;
});

$app->post('/modules/carriers/mondialrelay/points', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_carrier = $request->getParam('id_carrier');
    $mondialrelay = new PrestAppMondialrelayModuleController();
    $points = $mondialrelay->getPoints($context, $id_carrier);

    $data = array(
        'points' => $points,
    );

    if (!$points) {
        $response = sendBadRequest($response, $data);
    } else {
        $response = sendOk($response, $data);
    }

    return $response;
});

$app->post('/modules/carriers/mondialrelay/save-delivery-point', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_carrier = $request->getParam('id_carrier');
    $relay_point = $request->getParam('relay_point');
    $carrier_method = $request->getParam('carrier_method');

    $mondialrelay = new PrestAppMondialrelayModuleController();
    $saveDelivery = $mondialrelay->saveDeliveryPoint($context, $id_carrier, $relay_point, $carrier_method);

    $data = array(
        'data' => $saveDelivery,
    );

    if ($saveDelivery) {
        $response = sendOk($response, $data);
    } else {
        $response = sendServerError($response, $data);
    }

    return $response;
});
