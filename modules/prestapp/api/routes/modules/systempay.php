<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /modules/payments/systempay/display
 */
$app->get('/modules/payments/systempay/display', function (Request $request, Response $response) {
    $systempay = new PrestAppSystempayModuleController();
    $context = $request->getAttribute('context');

    $front = $systempay->displayPayment($context);

    $data = array(
        'options' => $front,
        'isMulti' => true,
    );

    $response = sendOk($response, $data);

    return $response;
});

/*
 * GET /modules/payments/systempay/display
 */
$app->post('/modules/payments/systempay/params', function (Request $request, Response $response) {

    $systempay = new PrestAppSystempayModuleController();
    $context = $request->getAttribute('context');
    $attribute = $request->getParam('attribute');
    $systempay_card_type = $request->getParam('systempay_card_type');
    $systempay_opt = $request->getParam('systempay_opt');

    $data = array();

    if ($systempay_card_type && $systempay_opt) {
        $data['opt'] = $systempay_opt;
        $data['card_type'] = $systempay_card_type;
    }

    $front = $systempay->getPaymentParams($attribute, $data, $context);

    // $data = array(
    //     'options' => $front,
    //     'isMulti' => true
    // );

    $data = $front;

    $response = sendOk($response, $data);

    return $response;
});
