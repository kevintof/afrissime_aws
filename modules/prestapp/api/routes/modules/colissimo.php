<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/modules/carriers/colissimo/display', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $colissimo = new PrestAppColissimoModuleController();

    $data = $colissimo->displayCarrier($context);

    $data = array(
        'data' => $data,
    );

    $response = sendOk($response, $data);

    return $response;
});

$app->post('/modules/carriers/colissimo/save-info-commande', function (Request $request, Response $response) {

    $context = $request->getAttribute('context');
    $params = $request->getParams();
    $colissimo = new PrestAppColissimoModuleController();
    $saveInfo = $colissimo->saveInfoCommande($params, $context);

    $data = array(
        'data' => $saveInfo,
    );

    if (!$saveInfo) {
        $response = sendBadRequest($response, $data);
    } else {
        $response = sendOk($response, $data);
    }

    return $response;
});

$app->post('/modules/carriers/colissimo/points', function (Request $request, Response $response) {

    $params = $request->getParams();

    $colissimo = new PrestAppColissimoModuleController();
    $points = $colissimo->getPoints($params);

    $data = array(
        'points' => $points,
    );

    $response = sendOk($response, $data);

    return $response;
});

$app->post('/modules/carriers/colissimo/save-delivery-point', function (Request $request, Response $response) {

    $params = $request->getParams();

    $colissimo = new PrestAppColissimoModuleController();
    $saveDelivery = $colissimo->saveDeliveryPoint($params);

    $data = array(
        'data' => $saveDelivery,
    );

    if (!$saveDelivery) {
        $response = sendBadRequest($response, $data);
    } else {
        $response = sendOk($response, $data);
    }

    return $response;
});