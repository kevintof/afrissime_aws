<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/modules/carriers/ecrelaypoint/display', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $ecrelaypoint = new PrestAppEcrelaypointModuleController();

    $result = $ecrelaypoint->getData($context);

    $data = array(
        'data' => $result,
    );

    $response = sendOk($response, $data);

    return $response;
});

$app->post('/modules/carriers/ecrelaypoint/points', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $ecrelaypoint = new PrestAppEcrelaypointModuleController();
    $points = $ecrelaypoint->getPoints($context);

    if (isset($points)) {
        if (is_array($points) && count($points) > 0) {
            foreach($points as $key => $value) {
                $points[$key]['hours'] = Tools::unSerialize($points[$key]['hours']);
            }
        }
    }

    $data = array(
        'points' => $points,
    );

    $response = sendOk($response, $data);

    return $response;
});

$app->post('/modules/carriers/ecrelaypoint/save-delivery-point', function (Request $request, Response $response) {

    $context = $request->getAttribute('context');

    $id_pr = $request->getParam('id_pr');
    $id_cart = $request->getParam('id_cart');
    $tokenecrelaypoint = $request->getParam('tokenecrelaypoint');

    $ecrelaypoint = new PrestAppEcrelaypointModuleController();
    $saveDelivery = $ecrelaypoint->saveDeliveryPoint($id_pr, $id_cart, $tokenecrelaypoint, $context);

    $data = array(
        'result' => $saveDelivery,
    );

    if (!$saveDelivery) {
        $response = sendBadRequest($response, $data);
    } else {
        $response = sendOk($response, $data);
    }

    return $response;
});