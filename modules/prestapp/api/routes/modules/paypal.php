<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /modules/payments/bankwire/display
 */
$app->get('/modules/payments/paypal/display', function (Request $request, Response $response) {
    $paypal = new PrestAppPaypalModuleController();

    $context = $request->getAttribute('context');

    $front = $paypal->displayPayment($context);

    $data = array(
        'data' => $front,
    );

    $response = sendOk($response, $data);

    return $response;
});

/*
 * POST /modules/payments/paypal/validate
 */
$app->post('/modules/payments/paypal/validate', function (Request $request, Response $response) {

    $context = $request->getAttribute('context');
    $transaction_id = $request->getParam('transaction_id');
    $total_paid = $request->getParam('total_paid');
    $currency = $request->getParam('currency');
    $payment_date = $request->getParam('payment_date');
    $payment_status = $request->getParam('payment_status');

    $paypal = new PrestAppPaypalModuleController();

    $order = $paypal->validateOrder($context, $transaction_id, $total_paid, $currency, $payment_date, $payment_status);

    $data = array(
        'order' => $order,
    );

    $response = sendOk($response, $data);

    return $response;
})->setName('/modules/payments/paypal/validate');

$app->post('/modules/payments/paypal/url', function (Request $request, Response $response) {
    $paypal = new PrestAppPaypalModuleController();
    $context = $request->getAttribute('context');

    $front = $paypal->getPaymentUrl($context);

    if (!$front) {
        $response = sendNotFound($response, 'NOT_FOUND');
    } else {
        $data = array(
            'data' => $front,
        );

        $response = sendOk($response, $data);
    }

    return $response;
});

$app->get('/modules/payments/paypal/approve', function (Request $request, Response $response) {
    $paypal = new PrestAppPaypalModuleController();
    $context = $request->getAttribute('context');

    $token = $request->getParam('token');
    $payer_id = $request->getParam('PayerID');

    $front = $paypal->approvePayment($context, $token, $payer_id);

    $data = array();

    if (!$front) {
        $data['approved'] = false;
        $data['transaction_id'] = false;
    } else {
        $data['approved'] = true;
        if ($front['transaction_id']) {
            $data['token'] = $front['transaction_id'];
        } else {
            $data['token'] = null;
        }

        if ($front['total_paid']) {
            $data['total_paid'] = $front['total_paid'];
        } else {
            $data['total_paid'] = null;
        }

        if ($front['currency']) {
            $data['currency'] = $front['currency'];
        } else {
            $data['currency'] = null;
        }

        if ($front['payment_date']) {
            $data['payment_date'] = $front['payment_date'];
        } else {
            $data['payment_date'] = null;
        }

        if ($front['payment_status']) {
            $data['payment_status'] = $front['payment_status'];
        } else {
            $data['payment_status'] = null;
        }
    }

    $response = sendOk($response, $data);

    return $response;
})->setName('/modules/payments/paypal/approve');

$app->get('/modules/payments/paypal/cancel', function (Request $request, Response $response) {
    $data = array(
        'approved' => false,
    );
    $response = sendOk($response, $data);
    return $response;
})->setName('/modules/payments/paypal/cancel');
