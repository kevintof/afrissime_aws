<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /carts/carriers - GET available carriers for this cart
 */
$app->get('/carts/carriers', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $carriers = PrestAppCartController::getDeliveryOptionList($id_cart, $context);

    $data = array(
        'id_cart' => $id_cart,
    );

    if (!$carriers) {
        $data['message'] = "carriers wasn't found";
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'carriers found';
        $data['carriers'] = $carriers;
        $response = sendOk($response, $data);
    }

    return $response;
});

$app->get('/carts/default-carrier', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $default_carrier = (int)Configuration::get('PS_CARRIER_DEFAULT');

    if ($default_carrier > 0) {
        $data = array(
            'default_carrier' => $default_carrier,
        );
    } else {
        $data = array(
            'default_carrier' => null,
        );
    }

    $response = sendOk($response, $data);

    return $response;
});

$app->get('/carts/extra-carriers', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $carriers = $request->getParam('carriers');
    $id_cart = (int) $context->cart->id;
    $extra_carriers = PrestAppCartController::getExtraCarriers($id_cart, $carriers, $context);

    $data = array(
        'extra_carriers' => $extra_carriers
    );

    $response = sendOk($response, $data);

    return $response;
});

/*
 * GET /carts/payments - GET available payments modules for this cart
 */
$app->get('/carts/payments', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $modules = PrestAppCartController::getPaymentModulesList($context);

    $data['message'] = 'modules found';
    $data['modules'] = $modules;
    $response = sendOk($response, $data);

    return $response;
});

/*
 * GET /carts/:id_cart - Return the summary of a cart from its id
 */
$app->get('/carts/{id_cart}', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $request->getAttribute('id_cart');
    $delete = $request->getParam('delete');

    $summary = PrestAppCartController::getCartSummary($id_cart, $delete, $context);

    if (!$summary) {
        $data = array(
            'id_cart' => $id_cart,
            'message' => 'Cannot found the specified cart',
        );
        $response = sendNotFound($response, $data);
    } else {
        $summary['id_cart'] = $id_cart;

        $data = array(
            'id_cart' => $id_cart,
            'message' => 'Cart summary',
            'summary' => $summary,
        );

        ///////////////////////
        //    prestadrive    //
        ///////////////////////

        if (class_exists('PrestAppPrestadriveCustomizationController')) {
            $prestadrive = new PrestAppPrestadriveCustomizationController();
            $data['summary']['prestadrive'] = $prestadrive->getCartValues($id_cart);
        }

        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * DELETE /carts/product - Remove product from its id from cart
 */
$app->delete('/carts/product', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $id_product = (int) $request->getParam('id_product');
    $id_combination = (int) $request->getParam('id_combination');
    $id_customization = (int) $request->getParam('id_customization');

    $deleted = PrestAppCartController::deleteProductInCart($id_cart, $id_product, $context, $id_combination, $id_customization);

    $data = array(
        'id_cart' => $id_cart,
        'id_product' => $id_product,
        'id_combination' => $id_combination,
        'id_customization' => $id_customization,
    );

    if (!$deleted) {
        $data['message'] = 'Cannot remove this specific product';
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'Product removed or not in cart';
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * PUT /carts - allow to update all quantity in cart (remove and add specific product quantity)
 */
$app->put('/carts/quantity', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $id_product = (int) $request->getParam('id_product');
    $quantity = (int) $request->getParam('quantity');
    $id_combination = (int) $request->getParam('id_combination');
    $operator = $request->getParam('operator'); //up or down
    $id_customization = (int) $request->getParam('id_customization');

    $updated = PrestAppCartController::updateProductQuantity($id_cart, $id_product, $context, $quantity, $id_combination, $id_customization, $operator);

    $data = array(
        'id_cart' => $id_cart,
        'id_product' => $id_product,
        'id_combination' => $id_combination,
        'id_customization' => $id_customization,
        'id_combination' => $id_combination,
        'id_customization' => $id_customization,
    );

    if (!$updated) {
        $data['message'] = 'Cannot remove this specific product';
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'Product removed or is not in cart';
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * PUT /carts/addresses/delivery - Update the delivery address id of the cart
 */
$app->put('/carts/addresses/delivery', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $id_current_address = (int) $request->getParam('id_current_address');
    $id_new_address = (int) $request->getParam('id_new_address');

    $updated = PrestAppCartController::updateAddressDeliveryId($id_cart, $id_current_address, $id_new_address, $context);

    $data = array(
        'id_cart' => $id_cart,
        'id_current_address' => $id_current_address,
        'id_new_address' => $id_new_address,
    );

    if (!$updated) {
        $data['message'] = 'Cannot update address';
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'address updated';
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * PUT /carts/addresses/invoice - Update the invoice address id of the cart
 */
$app->put('/carts/addresses/invoice', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $id_current_address = (int) $request->getParam('id_current_address');
    $id_new_address = (int) $request->getParam('id_new_address');

    $updated = PrestAppCartController::updateAddressInvoiceId($id_cart, $id_current_address, $id_new_address, $context);

    $data = array(
        'id_cart' => $id_cart,
        'id_current_address' => $id_current_address,
        'id_new_address' => $id_new_address,
    );

    if (!$updated) {
        $data['message'] = 'Cannot update address';
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'address updated';
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * PUT /carts/addresses/delivery-invoice - Update the delivery and invoice address id of the cart
 */
$app->put('/carts/addresses/delivery-invoice', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $id_current_delivery_address = (int) $request->getParam('id_current_delivery_address');
    $id_current_invoice_address = (int) $request->getParam('id_current_invoice_address');
    $id_new_delivery_address = (int) $request->getParam('id_new_delivery_address');
    $id_new_invoice_address = (int) $request->getParam('id_new_invoice_address');

    $updated = PrestAppCartController::updateAddressDeliveryInvoiceId($id_cart, $id_current_delivery_address, $id_current_invoice_address, $id_new_delivery_address, $id_new_invoice_address, $context);

    $data = array(
        'id_cart' => $id_cart,
        'id_current_delivery_address' => $id_current_delivery_address,
        'id_current_invoice_address' => $id_current_invoice_address,
        'id_new_delivery_address' => $id_new_delivery_address,
        'id_new_invoice_address' => $id_new_invoice_address,
    );

    if (!$updated) {
        $data['message'] = 'Cannot update address';
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'address updated';
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * POST /carts/rule - add cart rule from the code and check validity before adding it to cart
 */
$app->post('/carts/rule', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $code = $request->getParam('code');
    $id_cart_rule = (int) CartRule::getIdByCode($code);

    $data = array(
        'id_cart' => $id_cart,
        'code' => $code,
        'id_cart_rule' => $id_cart_rule,
    );

    if ($id_cart_rule) {
        $added = PrestAppCartController::addCartRule($id_cart, $id_cart_rule, $context);

        if (!$added) {
            $data['message'] = "Cart rule wasn't added because of validity or rights";
            $response = sendUnauthorized($response, $data);
        } else {
            $data['message'] = 'Cart rule added';
            $response = sendOk($response, $data);
        }
    } else {
        $data['message'] = "Cart rule wasn't added because not exists";
        $response = sendBadRequest($response, $data);
    }

    return $response;
});

/*
 * DELETE /carts/rule - remove cart rule from its id in cart
 */
$app->delete('/carts/rule', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $id_cart_rule = (int) $request->getParam('id_cart_rule');

    $data = array(
        'id_cart' => $id_cart,
        'id_cart_rule' => $id_cart_rule,
    );

    $deleted = PrestAppCartController::removeCartRule($id_cart, $id_cart_rule, $context);

    if (!$deleted) {
        $data['message'] = "Cart rule wasn't removed. Have you the rights to do this ?";
        $response = sendUnauthorized($response, $data);
    } else {
        $data['message'] = 'Cart rule removed';
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * POST /carts/claim - Allow to take the ownership of a cart if its not belongs to someone else
 */
$app->post('/carts/claim', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $request->getParam('id_cart');

    $taken = PrestAppCartController::claimOwnership($id_cart, $context);

    $data = array(
        'id_cart' => $id_cart,
    );

    if (!$taken) {
        $data['message'] = "Ownership wasn't granted";
        $response = sendUnauthorized($response, $data);
    } else {
        $data['message'] = 'Ownership granted';
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * POST /carts - If user is authenticated, we try to get the last cart non orderer, else we create another one
 */
$app->post('/carts', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $full = $request->getParam('full');

    $cart = PrestAppCartController::findOrCreateCart($context);

    $data = array();

    if (!$cart) {
        $data['message'] = "Cart wasn't created and found";
        $response = sendBadRequest($response, $data);
    } else {
        if (!$full) {
            $data['message'] = 'Cart created or found';
            $data['cart'] = $cart;
            $response = sendOk($response, $data);
        } else {
            $delete = $request->getParam('delete');
            $id_cart = $cart->id;
            $summary = PrestAppCartController::getCartSummary($id_cart, $delete, $context);
            
            if (!$summary) {
                $data = array(
                    'id_cart' => $id_cart,
                    'message' => 'Cannot found the specified cart',
                );
                $response = sendNotFound($response, $data);
            } else {
                $summary['id_cart'] = $id_cart;
    
                $data = array(
                    'id_cart' => $id_cart,
                    'message' => 'Cart summary',
                    'cart' => $summary,
                    'is_full' => true
                );

                ///////////////////////
                //    prestadrive    //
                ///////////////////////

                if (class_exists('PrestAppPrestadriveCustomizationController')) {
                    $prestadrive = new PrestAppPrestadriveCustomizationController();
                    $data['summary']['prestadrive'] = $prestadrive->getCartValues($id_cart);
                }

                $response = sendOk($response, $data);
            }
        }
    }

    return $response;
});

/*
 * PUT /carts/delivery - update the delivery option of the cart
 */
$app->put('/carts/delivery', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = (int) $context->cart->id;
    $id_address = (int) $request->getParam('id_address');
    $id_delivery_option = $request->getParam('delivery_option');

    $delivery_option = array();
    $delivery_option[$id_address] = $id_delivery_option;

    $result = PrestAppCartController::setDeliveryOption($id_cart, $delivery_option, $context);

    $data = array(
        'id_cart' => $id_cart,
        'delivery_option' => $delivery_option,
    );

    if (!$result) {
        $data['message'] = "Cart wasn't updated";
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'Cart updated';
        $response = sendOk($response, $data);
    }

    return $response;
});
