<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /customers/addresses - return customer addresses list
 */
$app->get('/customers/addresses', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    //if entered credentials seems ok, we check if an account match with it
    $addresses = PrestAppAddressController::getCustomerAddresses($context);
    $default = PrestAppAddressController::getCustomerDefaultAddress($context);

    if ('NOT_CONNECTED' === $addresses || 'NOT_CONNECTED' === $default) {
        $data = array(
            'message' => 'You must to be connected to get your addresses',
        );

        $response = sendUnauthorized($response, $data);
    } else {
        $data = array(
            'message' => 'addresses list',
        );

        $data['id_customer'] = (int) $context->customer->id;
        $data['addresses'] = $addresses;
        $data['default'] = $default;

        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * GET /addresses/{id_address} - return customer addresses list
 */
$app->get('/addresses/{id_address:[0-9]+}', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_address = $request->getAttribute('id_address');

    //if entered credentials seems ok, we check if an account match with it
    $address = PrestAppAddressController::getAddress($id_address, $context);

    if ('NOT_CONNECTED' === $address) {
        $data = array(
            'message' => 'You must to be connected to get your addresses',
        );

        $response = sendUnauthorized($response, $data);
    } else {
        $data = array(
            'message' => 'address',
        );

        $data['id_customer'] = (int) $context->customer->id;
        $data['address'] = $address;

        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * GET /customers/addresses/default - return customer default address
 */
$app->get('/customers/addresses/default', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    //if entered credentials seems ok, we check if an account match with it
    $address = PrestAppAddressController::getCustomerDefaultAddress($context);

    if ('NOT_CONNECTED' === $address) {
        $data = array(
            'message' => 'You must to be connected to get your default address',
        );

        $response = sendUnauthorized($response, $data);
    } else {
        $data = array(
            'message' => 'default address',
        );

        $data['id_customer'] = (int) $context->customer->id;
        $data['address'] = $address;

        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * GET /countries - return available countries
 */
$app->get('/countries', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    //if entered credentials seems ok, we check if an account match with it
    $countries = PrestAppAddressController::getCountries($context);

    $data = array(
        'message' => 'Available countries',
    );

    $countries_array = array();
    foreach ($countries as $country) {
        array_push($countries_array, $country);
    }

    $data['countries'] = $countries_array;

    //fix for ps < 1.6.0.10 Tools::getCountry(); doesnt exists
    // $data['default'] = Tools::getCountry();
    $data['default'] = PrestAppAddressController::getCountry();

    $response = sendOk($response, $data);

    return $response;
});

/*
 * POST /customers/addresses - create a new address
 */
$app->post('/customers/addresses', function (Request $request, Response $response) {
    $lastname = $request->getParam('lastname');
    $firstname = $request->getParam('firstname');
    $company = $request->getParam('company');
    $address1 = $request->getParam('address1');
    $address2 = $request->getParam('address2');
    $city = $request->getParam('city');
    $postcode = $request->getParam('postcode');
    $phone = $request->getParam('phone');
    $mobile = $request->getParam('mobile');
    $id_country = (int) $request->getParam('id_country');
    $id_state = (int) $request->getParam('id_state');
    $other = $request->getParam('other');
    $alias = $request->getParam('alias');
    $dni = $request->getParam('dni');
    $vat_number = $request->getParam('vat_number');
    $context = $request->getAttribute('context');

    //if entered credentials seems ok, we check if an account match with it
    $address = PrestAppAddressController::createOrUpdateAddress($lastname, $firstname, $company, $address1, $address2, $city, $postcode, $phone, $mobile, $id_country, $id_state, $other, $alias, $dni, $vat_number, $context, null);

    $data = array();

    if (!Validate::isLoadedObject($address)) {
        $data['code'] = $address;
        $data['message'] = 'There is an error in your request';
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'cannot load address';
        $data['address'] = $address;
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * PUT /customers/addresses - update address
 */
$app->post('/customers/addresses/update', function (Request $request, Response $response) {
    $lastname = $request->getParam('lastname');
    $firstname = $request->getParam('firstname');
    $company = $request->getParam('company');
    $address1 = $request->getParam('address1');
    $address2 = $request->getParam('address2');
    $city = $request->getParam('city');
    $postcode = $request->getParam('postcode');
    $phone = $request->getParam('phone');
    $mobile = $request->getParam('mobile');
    $id_country = (int) $request->getParam('id_country');
    $id_state = (int) $request->getParam('id_state');
    $other = $request->getParam('other');
    $alias = $request->getParam('alias');
    $dni = $request->getParam('dni');
    $vat_number = $request->getParam('vat_number');
    $context = $request->getAttribute('context');

    $id_address = (int) $request->getParam('id_address');

    if (!$id_address) {
        $data['message'] = 'id_adress is required';
        $response = sendBadRequest($response, $data);
    }

    //if entered credentials seems ok, we check if an account match with it
    $address = PrestAppAddressController::createOrUpdateAddress($lastname, $firstname, $company, $address1, $address2, $city, $postcode, $phone, $mobile, $id_country, $id_state, $other, $alias, $dni, $vat_number, $context, $id_address);

    $data = array();

    if (!Validate::isLoadedObject($address)) {
        $data['code'] = $address;
        $data['message'] = 'There is an error in your request';
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'created/updated address';
        $data['address'] = $address;
        $response = sendOk($response, $data);
    }

    return $response;
});

/*
 * DELETE /customers/addresses - update address
 */
$app->post('/customers/addresses/delete', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_address = (int) $request->getParam('id_address');

    if (!$id_address) {
        $data['message'] = 'id_adress is required';
        $response = sendBadRequest($response, $data);
    }

    //if entered credentials seems ok, we check if an account match with it
    $deleted = PrestAppAddressController::deleteAddress($id_address, $context);

    $data = array();

    if (true !== $deleted) {
        $data['code'] = $deleted;
        $data['message'] = 'There is an error in your request';
        $response = sendBadRequest($response, $data);
    } else {
        $data['message'] = 'address deleted';
        $response = sendOk($response, $data);
    }

    return $response;
});
