<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /initconf
 */
$app->get('/initconf', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $currencies = PrestAppCurrencyController::getActiveCurrencies();
    $languages = PrestAppLanguageController::getActiveLanguages($context->language->id);
    $b2b = Configuration::get('PS_B2B_ENABLE');

    $catalog_mode = false;

    if (Configuration::get('PS_CATALOG_MODE')) {
        $catalog_mode = true;
    } else if (Group::isFeatureActive()) {
        $catalog_mode = !(bool) Group::getCurrent()->show_prices;
    } else {
        $catalog_mode = false;
    }

    $data = array(
        'currencies' => $currencies,
        'languages' => $languages,
        'b2b' => $b2b == 1 ? true : false,
        'catalog_mode' => $catalog_mode,
        'customization_modules' => array(),
    );

    if (class_exists('PrestAppAllInOneRewardsCustomizationController')) {
        $all_in_one_rewards = new PrestAppAllInOneRewardsCustomizationController();
        array_push($data['customization_modules'], $all_in_one_rewards->getData());
    }

    if (class_exists('PrestAppDoofinderCustomizationController')) {
        $doofinder = new PrestAppDoofinderCustomizationController();
        array_push($data['customization_modules'], $doofinder->getData());
    }

    if (class_exists('PrestAppPrivatesalesCustomizationController')) {
        $privatesales = new PrestAppPrivatesalesCustomizationController();
        array_push($data['customization_modules'], $privatesales->getData());
    }

    if (class_exists('PrestAppPayplugModuleController')) {
        $payplug = new PrestAppPayplugModuleController();
        array_push($data['customization_modules'], $payplug->getData());
    }

    if (class_exists('PrestAppFidepiCustomizationController')) {
        $fidepi = new PrestAppFidepiCustomizationController();
        array_push($data['customization_modules'], $fidepi->getData());
    }

    if (class_exists('PrestAppOnlinechatCustomizationController')) {
        $onlinechat = new PrestAppOnlinechatCustomizationController();
        array_push($data['customization_modules'], $onlinechat->getData());
    }

    if (class_exists('PrestAppMinpurchaseCustomizationController')) {
        $minpurchase = new PrestAppMinpurchaseCustomizationController();
        array_push($data['customization_modules'], $minpurchase->getData());
    }

    if (class_exists('PrestAppNdksteppingpackCustomizationController')) {
        $ndksteppingpack = new PrestAppNdksteppingpackCustomizationController();
        array_push($data['customization_modules'], $ndksteppingpack->getData());
    }

    $response = sendOk($response, $data);

    return $response;
});
