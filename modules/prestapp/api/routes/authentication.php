<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * POST /customers/registration - return customer object when authentication succeed
 */
$app->post('/customers/registration', function (Request $request, Response $response) {
    $email = $request->getParam('email');
    $lastname = $request->getParam('lastname');
    $firstname = $request->getParam('firstname');
    $company = $request->getParam('company');
    $siret = $request->getParam('siret');
    $phone = $request->getParam('phone');
    $phone_mobile = $request->getParam('phone_mobile');
    $newsletter = $request->getParam('newsletter');
    $days = $request->getParam('days');
    $months = $request->getParam('months');
    $years = $request->getParam('years');
    $is_new_customer = $request->getParam('is_new_customer');
    $context = $request->getAttribute('context');

    $modules = [];

    if (class_exists('PrestAppAllInOneRewardsCustomizationController')) {
        $all_in_one_rewards = new PrestAppAllInOneRewardsCustomizationController();
        $sponsorship_active = $all_in_one_rewards->isSponsorshipActive();

        if ($sponsorship_active) {
            $module['name'] = "all_in_one_rewards";
            $module['params']['sponsor'] = $request->getParam('all_in_one_rewards_sponsor');
            array_push($modules, $module);
        }
    }

    if (class_exists('PrestAppFidepiCustomizationController')) {
        $fidepi = new PrestAppFidepiCustomizationController();
        $module['name'] = "fidepi";
        $module['params']['card'] = $request->getParam('fidepi');
        array_push($modules, $module);
    }

    //if entered credentials seems ok, we check if an account match with it
    $registration = PrestAppCustomerController::register($email, $lastname, $firstname, $company, $siret, $phone, $phone_mobile, $newsletter, $years, $months, $days, $is_new_customer, $modules, $context);

    $data = array(
        'message' => 'Registration',
        'registration' => $registration,
    );

    $response = sendOk($response, $data);

    return $response;
});

/*
 * POST /customers/authentication - return customer object when authentication succeed
 */
$app->post('/customers/authentication', function (Request $request, Response $response) {
    $email = $request->getParam('email');
    $decrypted_password = $request->getParam('password');
    $type = $request->getParam('type');

    // $iv = $request->getHeaderLine("iv");
    // $secure_key = Configuration::get("PRESTAPP_SECURE_KEY");
    // $algorythm = "aes-256-cbc";
    // $options = 0;
    // $decrypted_password = openssl_decrypt($hashed_password, $algorythm, $secure_key, $options, $iv);

    //if entered credentials seems ok, we check if an account match with it
    $customer = PrestAppCustomerController::login($email, $decrypted_password, $type == 'facebook' ? true : false);

    if ('INACTIVE' === $customer) {
        $data = array(
            'message' => "Your account isn't available at this time",
        );

        $response = sendUnauthorized($response, $data);
    } elseif ('NOTFOUND' === $customer) {
        $data = array(
            'message' => 'Authentication failed or user not found',
        );

        $response = sendUnauthorized($response, $data);
    } else {
        $data = array(
            'message' => 'Authentication succeed',
        );

        $data['customer'] = array(
            'id' => (int) $customer->id,
            'id_gender' => (int) $customer->id_gender,
            'lastname' => $customer->lastname,
            'firstname' => $customer->firstname,
            'birthday' => $customer->birthday,
            'email' => $customer->email,
            'website' => $customer->website,
            'company' => $customer->company,
            'siret' => $customer->siret,
            'ape' => $customer->ape,
            'id_lang' => (int) $customer->id_lang,
            'id_default_group' => (int) $customer->id_default_group,
            'newsletter' => (int) $customer->newsletter,
            'optin' => (int) $customer->optin,
            'passwd' => $customer->passwd,
        );

        $catalog_mode = false;

        if (Configuration::get('PS_CATALOG_MODE')) {
            $catalog_mode = true;
        } else if (Group::isFeatureActive()) {
            $group = new Group($customer->id_default_group);
            $show_prices = $group->show_prices;
            $catalog_mode = $show_prices ? false : true;
        } else {
            $catalog_mode = false;
        }

        $order_count = PrestAppOrderController::getOrderCountByCustomerID((int) $customer->id);
        $data['customer']['orders_count'] = $order_count;

        $data['customer']['catalog_mode'] = $catalog_mode;

        $response = sendOk($response, $data);
    }

    return $response;
});
