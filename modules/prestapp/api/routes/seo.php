<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/seo', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $page_name = $request->getParam('page_name');
    $id_category = $request->getParam('id_category');
    $id_product = $request->getParam('id_product');
    $id_cms = $request->getParam('id_cms');

    $data = array();

    if (!$page_name) {
        $data['message'] = 'page_name is required';
        $response = sendBadRequest($response, $data);
    }

    if ($page_name == 'index') {
        $link = Tools::getHttpHost(true) . __PS_BASE_URI__;
    }

    if ($page_name == 'product' && !$id_product) {
        $data['message'] = 'id_product is required';
        $response = sendBadRequest($response, $data);
    } else if ($page_name == 'product' && $id_product) {
        $link = new Link();
        $link = $link->getProductLink($id_product);
    }

    if ($page_name == 'category' && !$id_category) {
        $data['message'] = 'id_category is required';
        $response = sendBadRequest($response, $data);
    } else if ($page_name == 'category' && $id_category) {
        $link = new Link();
        $link = $link->getCategoryLink($id_category);
    }

    if ($page_name == 'cms' && !$id_category) {
        $data['message'] = 'id_cms is required';
        $response = sendBadRequest($response, $data);
    } else if ($page_name == 'category' && $id_cms) {
        $link = new Link();
        $link = $link->getCmsLink($id_cms);
    }

    $meta = Meta::getMetaTags((int) $context->language->id, $page_name);

    if (!$meta || !$page_name) {
        $data['message'] = 'There is an error in your request';
        $response = sendBadRequest($response, $data);

    } else {
        $data['meta'] = [
            'meta_title' => $meta['meta_title'],
            'meta_description' => $meta['meta_description'],
            'meta_keywords' => $meta['meta_keywords'],
            'link_rewrite' => $link,
        ];
        $response = sendOk($response, $data);
    }

    return $response;
});
