<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /customer/infos
 */
$app->get('/customers/infos', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $customer = PrestAppCustomerController::getCustomerInformations($context);

    $data = array(
        'customer' => $customer,
    );

    $response = sendOk($response, $data);

    return $response;
});

$app->post('/customers/infos', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $firstname = $request->getParam('firstname');
    $lastname = $request->getParam('lastname');
    $company = $request->getParam('company');
    $siret = $request->getParam('siret');
    $newpassword = $request->getParam('newPassword');
    $email = $request->getParam('email');
    $newsletter = $request->getParam('newsletter');
    $days = $request->getParam('days');
    $months = $request->getParam('months');
    $years = $request->getParam('years');

    $modules = [];

    if (class_exists('PrestAppFidepiCustomizationController')) {
        $fidepi = new PrestAppFidepiCustomizationController();
        $module['name'] = "fidepi";
        $module['params']['card'] = $request->getParam('fidepi');
        array_push($modules, $module);
    }

    $customer = PrestAppCustomerController::saveCustomerInformations($firstname, $lastname, $company, $siret, $newpassword, $email, $newsletter, $days, $months, $years, $modules, $context);

    $data['customer'] = array(
        'id' => (int) $customer->id,
        'id_gender' => (int) $customer->id_gender,
        'lastname' => $customer->lastname,
        'firstname' => $customer->firstname,
        'birthday' => $customer->birthday,
        'email' => $customer->email,
        'website' => $customer->website,
        'company' => $customer->company,
        'siret' => $customer->siret,
        'ape' => $customer->ape,
        'id_lang' => (int) $customer->id_lang,
        'id_default_group' => (int) $customer->id_default_group,
        'newsletter' => (int) $customer->newsletter,
        'optin' => (int) $customer->optin,
    );

    $response = sendOk($response, $data);

    return $response;
});

$app->post('/customers/forgot-password', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $email = $request->getParam('email');

    $result = PrestAppCustomerController::forgotPassword($email, $context);

    $status;
    $data = $result;

    switch ($result) {
        case 'MAIL_SENT':
            $response = sendOk($response, $data);
            break;
        case 'INVALID_EMAIL':
        case 'NO_ACCOUNT':
        case 'CHANGE_UNAVAILABLE':
        case 'CHANGE_TIME':
            $response = sendBadRequest($response, $data);
            break;
        case 'ERROR':
        default:
            $response = sendServerError($response, $data);
            break;
    }

    return $response;
});
