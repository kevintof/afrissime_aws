<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /payments/active - Get active carriers
 */
$app->get('/payments/active', function (Request $request, Response $response) {
    //if entered credentials seems ok, we check if an account match with it
    $paymentModules = PaymentModule::getInstalledPaymentModules();

    $modules = array();

    foreach ($paymentModules as $module) {
        $module_obj = Module::getInstanceById($module['id_module']);
        $module['displayName'] = $module_obj->displayName;
        $m = (object) array();
        $m->id = (int) $module_obj->id;
        $m->moduleName = $module_obj->name;
        $m->displayName = $module_obj->displayName;
        array_push($modules, $m);
    }

    $data = array();

    $data['message'] = 'payment active';
    $data['paymentModules'] = $modules;
    $response = sendOk($response, $data);

    return $response;
});
