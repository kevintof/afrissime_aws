<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/*
 * GET /orders/{id_order} - GET order history
 */
$app->get('/orders/{id_order:[0-9]+}', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_order = $request->getAttribute('id_order');

    $order = PrestAppOrderController::getOrderByOrderId($id_order, $context);

    $data['message'] = 'order found';
    $data['order'] = $order;
    $response = sendOk($response, $data);

    return $response;
});

/*
 * GET /orders/carts/{id_carts} - GET order
 */
$app->get('/orders/carts/{id_cart:[0-9]+}', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_cart = $request->getAttribute('id_cart');

    $order = PrestAppOrderController::getOrderByCartId($id_cart, $context);

    if ($order != 'ORDER_NOT_FOUND' && $order != 'NOT_AUTHORIZED') {
        $analytics_data = PrestAppOrderController::getAnalyticsData($order, $context);
    } else {
        $analytics_data = null;
    }

    $data['message'] = 'order found';
    $data['order'] = $order;
    $data['analytics_data'] = $analytics_data;
    $response = sendOk($response, $data);

    return $response;
});

/*
 * GET /orders/history - GET order history
 */
$app->get('/orders/history', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');

    $orders = PrestAppOrderController::getOrderHistory($context);

    $data['message'] = 'orders found';
    $data['orders'] = $orders;
    $response = sendOk($response, $data);

    return $response;
});

/*
 * GET /currency/{id} - GET order history
 */
$app->get('/currency/{id_currency:[0-9]+}', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_currency = $request->getAttribute('id_currency');

    $currency = PrestAppOrderController::getCurrency($id_currency, $context);

    $data['message'] = 'currency found';
    $data['currency'] = $currency;
    $response = sendOk($response, $data);

    return $response;
});

$app->post('/orders/log', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_order = $request->getParam('id_order');

    $order = PrestAppOrderController::logOrder($id_order, $context);

    $data['message'] = 'Order logged';

    $response = sendOk($response, $data);

    return $response;
});
