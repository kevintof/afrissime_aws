<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

//get featured products
$app->get('/products/featured/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $populate = $request->getAttribute('populate');

    $data = PrestAppProductController::getFeaturedProducts($populate, $context);

    $response = sendOk($response, $data);

    return $response;
});

//get new products
$app->get('/products/new/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $populate = $request->getAttribute('populate');

    $data = PrestAppProductController::getNewProducts($populate, $context);

    $response = sendOk($response, $data);

    return $response;
});

//get best sellers
$app->get('/products/bestsellers/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $populate = $request->getAttribute('populate');

    $data = PrestAppProductController::getBestSellersProducts($populate, $context);

    $response = sendOk($response, $data);

    return $response;
});

//get product by category
$app->get('/products/search/{expr}/{page_number:[0-9]+}/{product_per_page:[0-9]+}/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $expr = $request->getAttribute('expr');
    $page_number = $request->getAttribute('page_number');
    $product_per_page = $request->getAttribute('product_per_page');
    $populate = $request->getAttribute('populate');
    $column = $request->getHeaderLine('column');
    $order = $request->getHeaderLine('order');

    $data = PrestAppProductController::searchProducts($expr, $page_number, $product_per_page, $column, $order, $populate, $context);

    $response = sendOk($response, $data);

    return $response;
});

//get product
$app->get('/products/{id_product:[0-9]+}/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $id_product = $request->getAttribute('id_product');
    $populate = $request->getAttribute('populate');

    $data = PrestAppProductController::getProduct($id_product, $populate, $context);

    $response = sendOk($response, $data);

    return $response;
});

//get all products
$app->get('/products/all/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $populate = $request->getAttribute('populate');

    $data = PrestAppProductController::getAllProducts($populate, $context);

    $response = sendOk($response, $data);

    return $response;
});

//get all sales
$app->get('/products/sales/{count:[0-9]+}/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $populate = $request->getAttribute('populate');
    $count = $request->getAttribute('count');

    $data = PrestAppProductController::getSalesProducts($count, $populate, $context);

    $response = sendOk($response, $data);

    return $response;
});

//get all last seen products
$app->get('/products/lastseen/{id_array}/[{populate}]', function (Request $request, Response $response) {
    $context = $request->getAttribute('context');
    $populate = $request->getAttribute('populate');
    $id_array = $request->getAttribute('id_array');
    $id_array = json_decode($id_array);

    $data = PrestAppProductController::getLastSeenProducts($populate, $context, $id_array);

    $response = sendOk($response, $data);

    return $response;
});
