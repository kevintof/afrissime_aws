<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

function sendUnauthorized($response, $data)
{
    $code = 401;

    $return = array(
        'status' => 'UNAUTHORIZED',
        'code' => $code,
        'data' => $data,
    );

    $newResponse = $response->withJson($return, $code);

    return $newResponse;
}

function sendBadRequest($response, $data)
{
    $code = 400;

    $return = array(
        'status' => 'BADREQUEST',
        'code' => $code,
        'data' => $data,
    );

    $newResponse = $response->withJson($return, $code);

    return $newResponse;
}

function sendNotFound($response, $data)
{
    $code = 404;

    $return = array(
        'status' => 'NOTFOUND',
        'code' => $code,
        'data' => $data,
    );

    $newResponse = $response->withJson($return, $code);

    return $newResponse;
}

function sendForbidden($response, $data)
{
    $code = 403;

    $return = array(
        'status' => 'FORBIDDEN',
        'code' => $code,
        'data' => $data,
    );

    $newResponse = $response->withJson($return, $code);

    return $newResponse;
}

function sendOk($response, $data)
{
    $code = 200;

    $return = array(
        'status' => 'OK',
        'code' => $code,
        'data' => $data,
    );

    $newResponse = $response->withJson($return, $code);

    return $newResponse;
}

function sendServerError($response, $data)
{
    $code = 500;

    $return = array(
        'status' => 'SERVER_ERROR',
        'code' => $code,
        'data' => $data,
    );

    $newResponse = $response->withJson($return, $code);

    return $newResponse;
}
