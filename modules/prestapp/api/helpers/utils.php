<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

function createContext($request)
{
    $context = Context::getContext();

    $context->customer = $request->getAttribute('customer');

    $context->cart = new Cart((int) $request->getHeaderLine('cart'));

    if ($context->customer) {
        $context->cart->id_customer = $context->customer->id;
    }

    if ($request->getHeaderLine('lang')) {
        $context->language = new Language((int) $request->getHeaderLine('lang'));
    }
    if ($request->getHeaderLine('currency')) {
        $context->currency = new Currency((int) $request->getHeaderLine('currency'));
    }
    if ($request->getHeaderLine('shop')) {
        $context->shop = new Shop((int) $request->getHeaderLine('shop'));
    } else {
        $context->shop = new Shop(1);
    }

    return $context;
}
