<?php
/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Prestapp extends PaymentModule
{
    protected $api_url;

    public function __construct()
    {
        $this->name = 'prestapp';
        $this->tab = 'mobile';
        $this->version = '1.2.1';
        $this->author = 'Aquil\'App';
        $this->need_instance = 0;
        $this->module_key = '9341195e16d310293a30a7286ab73c37';
        $this->author_address = '0x3EC17b06Ad9d217884d8CAF70d25cc8454b3E81C';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('PrestApp - Create your own mobile app and mobile first PWA');
        $this->description = $this->l('Increase your business by offering to your customers an optimized mobile and tablet experience. Create your mobile app and/or your Progressive Web App now with PrestApp!');
        $this->api_url = Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/prestapp/api/';

        /* Backward compatibility */
        if (_PS_VERSION_ < '1.5') {
            require _PS_MODULE_DIR_ . $this->name . '/backward_compatibility/backward.php';
        }
    }

    public function install()
    {
        $length = 16;
        $token = bin2hex(openssl_random_pseudo_bytes($length));
        $secret_key = bin2hex(openssl_random_pseudo_bytes($length));

        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if (!parent::install() ||
            !$this->registerHook('displayHeader') ||
            !$this->registerHook('actionOrderStatusUpdate') ||
            !$this->registerHook('actionOrderStatusPostUpdate') ||
            !$this->registerHook('actionValidateOrder') ||
            !Configuration::updateValue('PRESTAPP_API_KEY', 't_' . $token) ||
            !Configuration::updateValue('PRESTAPP_SECRET_KEY', 'sk_' . $secret_key)
        ) {
            return false;
        } else {
            return true;
        }
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    /**
     * Load the configuration form.
     */
    public function getContent()
    {

        $this->context->controller->addJS($this->_path . 'views/js/back.js');
        $protocol = Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://';

        $status = array();
        $isError = false;

        $version = explode('.', PHP_VERSION);
        $php_version = $version[0] . '.' . $version[1];
        $php_version_numeric = $version[0] * 10000 + $version[1] * 100 + $version[2];

        // Check PHP version
        if (!defined('PHP_VERSION_ID')) {
            define('PHP_VERSION_ID', $php_version_numeric);
        }

        if (!function_exists('curl_version')) {
            $isError = true;
            $status['curl'] = "CURL_DISABLED";
        } else {

            $curl = curl_init();

            //send the event and say to our app that the order changed status
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->api_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: " . Configuration::get('PRESTAPP_API_KEY'),
                    "User-Agent: PrestApp PrestaShop Module",
                ),
            ));

            $response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $err = curl_error($curl);
            curl_close($curl);

            if (!isset($response) || $httpcode !== 200 || $err !== "") {
                $status['response_code'] = "NO_CONTACT_WITH_OUR_API";
            }

            //server error
            if ($httpcode == 500) {
                $data = json_decode($response);
                if ($data->mod_rewrite) {
                    $isError = true;
                    $status['mod_rewrite'] = $data->mod_rewrite;
                }
                if ($data->mod_headers) {
                    $isError = true;
                    $status['mod_headers'] = $data->mod_headers;
                }
                if ($data->php) {
                    $isError = true;
                    $status['php'] = $data->php;
                }
                if ($data->prestashop) {
                    $isError = true;
                    $status['prestashop'] = $data->prestashop;
                }
            }
        }

        $prestapp_paypal_error = null;

        if (Tools::isSubmit('submitPrestAppPaypal') && Module::isEnabled('paypal')) {

            $PRESTAPP_PAYPAL_SECRET_KEY = strval(Tools::getValue('PRESTAPP_PAYPAL_SECRET_KEY'));
            $PRESTAPP_PAYPAL_CLIENT_ID = strval(Tools::getValue('PRESTAPP_PAYPAL_CLIENT_ID'));

            if (
                !$PRESTAPP_PAYPAL_SECRET_KEY ||
                empty($PRESTAPP_PAYPAL_SECRET_KEY) ||
                !Validate::isGenericName($PRESTAPP_PAYPAL_SECRET_KEY) ||
                !$PRESTAPP_PAYPAL_CLIENT_ID ||
                empty($PRESTAPP_PAYPAL_CLIENT_ID) ||
                !Validate::isGenericName($PRESTAPP_PAYPAL_CLIENT_ID)
            ) {
                $prestapp_paypal_error = true;
            } else {
                $prestapp_paypal_error = false;
                Configuration::updateValue('PRESTAPP_PAYPAL_SECRET_KEY', $PRESTAPP_PAYPAL_SECRET_KEY);
                Configuration::updateValue('PRESTAPP_PAYPAL_CLIENT_ID', $PRESTAPP_PAYPAL_CLIENT_ID);
            }
        }

        if (Tools::isSubmit('submitGDPR')) {
            $PRESTAPP_CUSTOM_GDPR_TEXT = strval(Tools::getValue('PRESTAPP_CUSTOM_GDPR_TEXT'));
            if (isset($_POST["PRESTAPP_CUSTOM_GDPR"])) {
                $PRESTAPP_CUSTOM_GDPR = strval(Tools::getValue('PRESTAPP_CUSTOM_GDPR'));
            } else {
                $PRESTAPP_CUSTOM_GDPR = "0";
            }

            Configuration::updateValue('PRESTAPP_CUSTOM_GDPR_TEXT', trim($PRESTAPP_CUSTOM_GDPR_TEXT));
            Configuration::updateValue('PRESTAPP_CUSTOM_GDPR', $PRESTAPP_CUSTOM_GDPR);
        }

        if (Tools::isSubmit('submitRedirectionSettings')) {
            Configuration::updateValue('PRESTAPP_REDIRECTION', strval(Tools::getValue('PRESTAPP_REDIRECTION')));
            Configuration::updateValue('PRESTAPP_PWA_URL', strval(Tools::getValue('PRESTAPP_PWA_URL')));
        }

        $this->context->smarty->assign(array(
            'PRESTAPP_API_PROTOCOL' => $protocol,
            'PRESTAPP_API_URL' => Tools::getHttpHost(false),
            'PRESTAPP_API_KEY' => Configuration::get('PRESTAPP_API_KEY', null),
            'PRESTAPP_SECRET_KEY' => Configuration::get('PRESTAPP_SECRET_KEY', null),
            'PRESTAPP_PAYPAL_SECRET_KEY' => Configuration::get('PRESTAPP_PAYPAL_SECRET_KEY', null),
            'PRESTAPP_PAYPAL_CLIENT_ID' => Configuration::get('PRESTAPP_PAYPAL_CLIENT_ID', null),
            'PRESTAPP_CUSTOM_GDPR_TEXT' => Configuration::get('PRESTAPP_CUSTOM_GDPR_TEXT', null),
            'PRESTAPP_CUSTOM_GDPR' => Configuration::get('PRESTAPP_CUSTOM_GDPR', null),
            'PRESTAPP_REDIRECTION' => Configuration::get('PRESTAPP_REDIRECTION', null),
            'PRESTAPP_PWA_URL' => Configuration::get('PRESTAPP_PWA_URL', null),
            'PRESTAPP_PAYPAL_ENABLED' => Module::isEnabled('paypal'),
            'PRESTAPP_PAYPAL_ERROR' => $prestapp_paypal_error,
            'API_URL' => $this->api_url,
            'PHP_VERSION' => $php_version,
            'PRESTASHOP_VERSION' => _PS_VERSION_,
            'IS_ERROR' => $isError,
            'STATUS' => $status,
            'MODULE_VERSION' => $this->version,
        ));

        return ($this->context->smarty->fetch(dirname(__FILE__) . '/views/templates/admin/configuration.tpl'));
    }

    /**
     * Send event to application when order is changing status
     *
     * @return void
     */
    public function hookActionOrderStatusUpdate($params)
    {
        // improve : detect only orders passed with PrestApp
        // the problem is : bank and paypal request directly the shop for the confirmation
        $blackListTriggerStatus = array(1, 3, 4, 5, 7, 10, 13); //no need to trigger request for theses statuses
        $id_status = $params['newOrderStatus']->id;

        if (!in_array($id_status, $blackListTriggerStatus)) {
            $id_order = (int) $params['id_order'];
            $order = new Order($id_order);
            $id_cart = (int) $params['cart']->id;

            $curl = curl_init();

            $url = 'https://mobileapi.prestapp.eu/sendEvent';

            //send the event and say to our app that the order changed status
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => array(
                    "id_cart" => $id_cart,
                    "id_order" => $id_order,
                    "id_status" => $id_status,
                    "webservice_key" => Configuration::get('PRESTAPP_API_KEY'),
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
        }
    }

    public function hookActionOrderStatusPostUpdate($params)
    {

        ///////////////////////
        //    prestadrive    //
        ///////////////////////

        if (Module::isInstalled('prestadrive') && Module::isEnabled('prestadrive')) {
            $user_checksum = $this->context->cookie->id_drive;
            $id_carrier = $params['cart']->id_carrier;
            $selectyourcarrier = Db::getInstance()->getRow('SELECT data_value FROM ' . _DB_PREFIX_ . 'prestadrive_data WHERE data_name = "selectedcarrier"');
            if (preg_match('#,' . $id_carrier . ',#', $selectyourcarrier['data_value'], $matches, PREG_OFFSET_CAPTURE)) {

                $id_prestashop_order = 0;

                $sqluser_checksum = 'SELECT id_prestashop_order FROM ' . _DB_PREFIX_ . 'prestadrive_order WHERE user_checksum = "' . pSQL($user_checksum) . '"';
                if ($rowuser_checksum = Db::getInstance()->getRow($sqluser_checksum)) {
                    $id_prestashop_order = pSQL($rowuser_checksum['id_prestashop_order']);
                }

                if ($id_prestashop_order != 0) {

                    $prestashop_reference = null;
                    $sqlidorder = 'SELECT reference FROM ' . _DB_PREFIX_ . 'orders WHERE id_order = ' . (int) $id_prestashop_order;
                    if ($rowidorder = Db::getInstance()->getRow($sqlidorder)) {
                        $prestashop_reference = pSQL($rowidorder['reference']);
                    }

                    if (Tools::strlen($prestashop_reference) == 9) {
                        Db::getInstance()->update('prestadrive_order', array(
                            'id_drive_level' => 3,
                            'prestashop_reference' => pSQL($prestashop_reference),
                        ), 'user_checksum = "' . pSQL($user_checksum) . '"');
                    }
                }
            }
        }
    }

    public function hookActionValidateOrder($params)
    {

        //////////////////////
        //   ecrelaypoint   //
        //////////////////////

        if (Module::isInstalled('ecrelaypoint') && Module::isEnabled('ecrelaypoint')) {
            $ec_carrier_id = (int) Configuration::get('ECRELAYPOINT_TRANSPORTEUR', null, $this->context->shop->id_shop_group, (int) $this->context->shop->id);
            if (isset($ec_carrier_id)) {
                if ($ec_carrier_id) {
                    if ((int) $params['order']->id_carrier == (int) $ec_carrier_id) {
                        $result = Db::getInstance()->getRow('SELECT max(id_temp), id_relaypoint FROM ' . _DB_PREFIX_ . 'ecrelaypoint_temp WHERE id_cart =' . (int) $params['cart']->id);
                        if (!empty($result['id_relaypoint'])) {
                            $relaypoint = Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'ecrelaypoint WHERE id_relaypoint =' . (int) $result['id_relaypoint']);
                            Db::getInstance()->insert('address', array(
                                'id_country' => (int) $relaypoint['id_country'],
                                'id_state' => (int) $relaypoint['id_state'],
                                'id_customer' => (int) $params['customer']->id, //ajout par ThF
                                'alias' => pSQL($relaypoint['name']),
                                'firstname' => pSQL($params['customer']->firstname),
                                'lastname' => pSQL($params['customer']->lastname),
                                'company' => pSQL($relaypoint['name']), //ajout par ThF
                                'address1' => pSQL($relaypoint['address1']),
                                'address2' => pSQL($relaypoint['address2']),
                                'postcode' => pSQL($relaypoint['postcode']),
                                'city' => pSQL($relaypoint['city']),
                                'phone' => pSQL($relaypoint['phone']),
                            ));
                            $newidaddr = Db::getInstance()->getRow('SELECT id_address FROM ' . _DB_PREFIX_ . 'address order by id_address desc ');

                            $params['order']->id_address_delivery = (int) $newidaddr['id_address'];
                            $params['order']->save();
                        } else {
                            $sql = 'DELETE FROM ' . _DB_PREFIX_ . 'ecrelaypoint_temp WHERE id_cart = ' . (int) $params['cart']->id . ' and id_shop=' . (int) $this->context->shop->id;
                            Db::getInstance()->execute($sql);
                        }
                    } else {
                        $sql = 'DELETE FROM ' . _DB_PREFIX_ . 'ecrelaypoint_temp WHERE id_cart = ' . (int) $params['cart']->id . ' and id_shop=' . (int) $this->context->shop->id;
                        Db::getInstance()->execute($sql);
                    }
                } else {
                    $sql = 'DELETE FROM ' . _DB_PREFIX_ . 'ecrelaypoint_temp WHERE id_cart = ' . (int) $params['cart']->id . ' and id_shop=' . (int) $this->context->shop->id;
                    Db::getInstance()->execute($sql);
                }
            } else {
                $sql = 'DELETE FROM ' . _DB_PREFIX_ . 'ecrelaypoint_temp WHERE id_cart = ' . (int) $params['cart']->id . ' and id_shop=' . (int) $this->context->shop->id;
                Db::getInstance()->execute($sql);
            }
        }
    }

    public function hookDisplayHeader()
    {
        if (Context::getContext()->isMobile() && Configuration::get('PRESTAPP_REDIRECTION') && Configuration::get('PRESTAPP_PWA_URL')) {
            switch (Configuration::get('PRESTAPP_REDIRECTION')) {
                case 'ALWAYS_REDIRECT':
                    $controller = Tools::getValue('controller');
                    $url = Configuration::get('PRESTAPP_PWA_URL');
                    if ($controller === "index") { //redirect to the homepage
                        $url = $url . "/home";
                        Tools::redirect($url);
                    } else if ($controller === "category") {
                        $url = $url . "/category/" . Context::getContext()->controller->getCategory()->id_category;
                        Tools::redirect($url);
                    } else if ($controller === "product") {
                        $url = $url . "/product/" . Context::getContext()->controller->getProduct()->id_product;
                        Tools::redirect($url);
                    }
                    break;
                case 'NO_REDIRECTION':
                    //do nothing
                    break;
                case 'ASK_POPIN':
                    //TODO inject JS to show the popin
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Get status code
     *
     * @return int
     */
    protected function getHttpResponseCode($URL)
    {
        $headers = get_headers($URL);
        return Tools::substr($headers[0], 9, 3);
    }
}
