/**
 * NOTICE OF LICENSE.
 *
 * This source file is subject to a commercial license from Aquil'App.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapp.eu
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Aquil'App.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
 * ...........................................................................
 *
 * @author    Maxime Léon <maxime.leon@prestapp.eu>
 * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
 * @license   Commercial license
 * Support by mail  :  contact@prestapp.eu
 */



$(document).ready(function() {
    if ($('#PRESTAPP_CUSTOM_GDPR').val() === "1") {
        $('#PRESTAPP_CUSTOM_GDPR').attr("checked", true);
        $('#INPUT_TEXT_GDPR').show();
    } else {
        $('#PRESTAPP_CUSTOM_GDPR').prop("checked", false);
        $('#INPUT_TEXT_GDPR').hide();
    }
    $('#PRESTAPP_CUSTOM_GDPR').click(function() {
        if ($('#PRESTAPP_CUSTOM_GDPR').is(":checked")) {
            $('#PRESTAPP_CUSTOM_GDPR').attr("value", "1");
            $('#INPUT_TEXT_GDPR').show();
        } else {
            $('#PRESTAPP_CUSTOM_GDPR').attr("value", "0");
            $('#INPUT_TEXT_GDPR').hide();
        }
    });

    $('#custom_form').submit(function(e) {
        if ($.trim($("#PRESTAPP_CUSTOM_GDPR_TEXT").val()).length === 0 && $('#PRESTAPP_CUSTOM_GDPR').is(":checked")) {
            alert('Vous avez coché la case GDPR mais aucun texte n\'est présent. Veuillez insérer du texte dans la zone prévue à cet effet.');
            e.preventDefault();
        }
    });
});
