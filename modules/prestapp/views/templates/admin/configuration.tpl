{*/**
  * NOTICE OF LICENSE.
  *
  * This source file is subject to a commercial license from Aquil'App.
  * Use, copy, modification or distribution of this source file without written
  * license agreement from the SARL is strictly forbidden.
  * In order to obtain a license, please contact us: contact@prestapp.eu
  * ...........................................................................
  * INFORMATION SUR LA LICENCE D'UTILISATION
  *
  * L'utilisation de ce fichier source est soumise a une licence commerciale
  * concedee par la societe Aquil'App.
  * Toute utilisation, reproduction, modification ou distribution du present
  * fichier source sans contrat de licence ecrit de la part de la Aquil'App est
  * expressement interdite.
  * Pour obtenir une licence, veuillez contacter Aquil'App a l'adresse: contact@prestapp.eu
  * ...........................................................................
  *
  * @author    Maxime Léon <maxime.leon@prestapp.eu>
  * @copyright Copyright (c) 2017 - 2019 Aquil'App 2 Rue D'espalion 44800 Saint-Herblain France
  * @license   Commercial license
  * Support by mail  :  contact@prestapp.eu
  */
  *}

<div class="row">  
  <div class="col-sm-4">
    <div class="panel">
      <div class="panel-heading">
        <i class="icon-th-list"></i> {l s='Requirements' mod='prestapp'}
      </div>

      <div class="form-group">
        <label>
          {l s='PrestApp\'s module version' mod='prestapp'}
        </label>
        <p>{$MODULE_VERSION}</p>
      </div>

      <div class="form-group">
        <label>
          {l s='PHP version' mod='prestapp'}
        </label>
        <p>{l s='Required' mod='prestapp'} : 5.6 {l s='or higher' mod='prestapp'}</p>
        <p>{l s='Yours' mod='prestapp'} : {$PHP_VERSION|escape:'htmlall':'UTF-8'}
          {if isset($STATUS["php"])}
            <i class="icon-times" style="color: red;"></i>
          {else}
            <i class="icon-check" style="color: green;"></i>
          {/if}
        </p>
      </div>


      <div class="form-group">
        <label>
          {l s='PrestaShop version' mod='prestapp'}
        </label>
        <p>{l s='Required' mod='prestapp'} : 1.5.6.3 {l s='or higher' mod='prestapp'}</p>
        <p>{l s='Yours' mod='prestapp'} : {$PRESTASHOP_VERSION|escape:'htmlall':'UTF-8'}
          {if isset($STATUS["prestashop"])}
            <i class="icon-times" style="color: red;"></i>
          {else}
            <i class="icon-check" style="color: green;"></i>
          {/if}
        </p>
      </div>
      
      <div class="form-group">
        <label>
          {l s='Server requirements' mod='prestapp'}
        </label>
        {if isset($STATUS["server_software"]) && $STATUS["server_software"] === "apache"}
          <p>
            {l s='mod_rewrite' mod='prestapp'}
            {if $STATUS["mod_rewrite"]}
              <i class="icon-times" style="color: red;"></i>
            {else}
              <i class="icon-check" style="color: green;"></i>
            {/if}
          </p>
          <p>
            {l s='mod_headers' mod='prestapp'}
            {if isset($STATUS["mod_headers"])}
              <i class="icon-times" style="color: red;"></i>
            {else}
              <i class="icon-check" style="color: green;"></i>
            {/if}
          </p>
        {/if}
        <p>
          {l s='cURL' mod='prestapp'}
          {if isset($STATUS["curl"])}
            <i class="icon-times" style="color: red;"></i>
          {else}
            <i class="icon-check" style="color: green;"></i>
          {/if}
        </p>
        <br/>
        <p>
          {if isset($STATUS["response_code"]) && !$IS_ERROR}
            <div class="alert alert-danger">
              <p><strong>{l s='Sorry!' mod='prestapp'}</strong> {l s='We can not reach the module from outside.' mod='prestapp'} {l s='Please check that ' mod='prestapp'} <a href="{$API_URL|escape:'htmlall':'UTF-8'}" target="_blank">{$API_URL|escape:'htmlall':'UTF-8'}</a> {l s='returns a proper JSON response.' mod='prestapp'}</p>
            </div>
          {else if $IS_ERROR === true}
            <div class="alert alert-danger">
              <p><strong>{l s='Sorry!' mod='prestapp'}</strong> {l s='Please check all requirements' mod='prestapp'}  {l s='Please check that ' mod='prestapp'} <a href="{$API_URL|escape:'htmlall':'UTF-8'}" target="_blank">{$API_URL|escape:'htmlall':'UTF-8'}</a> {l s='returns a proper JSON response.' mod='prestapp'}</p>
            </div>
          {else if !$IS_ERROR}
            <div class="alert alert-success">
              <p><strong>{l s='Good job!' mod='prestapp'}</strong> {l s='Everything seems to be ok, you can now create your app on ' mod='prestapp'} <a href="https://webapp.prestapp.eu/#!/register" target="_blank">PrestApp</a> !</p>
            </div>
          {/if}
        </p>
        <p>
          {if isset($STATUS["server_software"]) && $STATUS["server_software"] !== "apache"}
            <p style="color: orange;">
              {l s='It seems that you do not use Apache on your server, maybe do you use Nginx or IIS ?' mod='prestapp'} <i class="icon-warning" style="color: orange;"></i>
            </p>
          {/if}
        </p>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item active">
        <a class="nav-link active" id="connect-tab" data-toggle="tab" href="#connect" role="tab" aria-controls="connect" aria-selected="true"><i class="icon-info"></i> {l s='Informations' mod='prestapp'}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="options-tab" data-toggle="tab" href="#options" role="tab" aria-controls="options" aria-selected="false"><i class="icon-cogs"></i> {l s='PWA settings' mod='prestapp'}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="modules-tab" data-toggle="tab" href="#modules" role="tab" aria-controls="modules" aria-selected="false"><i class="icon-cubes"></i> {l s='Modules' mod='prestapp'}</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade active in" id="connect" role="tabpanel" aria-labelledby="home-tab">

        <div class="panel">
          <div class="alert alert-info">
            <p>
              <strong>{l s='Welcome !' mod='prestapp'}</strong>
              {l s='You\'ll find here all informations you need to connect your store with our solution. You can copy and paste this data on your PrestApp\'s dashboard.' mod='prestapp'}
            </p>
          </div>
        </div>

        <div class="panel">
          <div class="panel-heading">
            <i class="icon-link"></i> {l s='Connection parameters' mod='prestapp'}
          </div>

          <p>
            <div class="form-group">
              <label>{l s='HTTP Protocol' mod='prestapp'}</label><br>
              {$PRESTAPP_API_PROTOCOL|escape:'htmlall':'UTF-8'}
            </div>
          <p>
          <p>
            <div class="form-group">
              <label>{l s='Your shop URL' mod='prestapp'}</label><br>
              {$PRESTAPP_API_URL|escape:'htmlall':'UTF-8'}
            </div>
          <p>
          <p>
            <div class="form-group">
              <label>{l s='API key' mod='prestapp'}</label><br>
              {$PRESTAPP_API_KEY|escape:'htmlall':'UTF-8'}
            </div>
          <p>
          <p>
            <div class="form-group">
              <label>{l s='Secret key' mod='prestapp'}</label><br>
              {$PRESTAPP_SECRET_KEY|escape:'htmlall':'UTF-8'}
            </div>
          <p>
        </div>
      </div>

      <div class="tab-pane fade" id="modules" role="tabpanel" aria-labelledby="modules-tab">
        <div class="panel">
          <div class="alert alert-info">
            <p>
              {l s='You will find here installed modules that PrestApp supports and needs credentials or informations in order to work' mod='prestapp'}
            </p>
          </div>
        </div>

        {if $PRESTAPP_PAYPAL_ENABLED === true}
          <form id="module_form" class="defaultForm form-horizontal" method="post" enctype="multipart/form-data" novalidate="">
            <input type="hidden" name="submitPrestAppPaypal" value="1">
            <div class="panel" id="fieldset_0">

              <div class="panel-heading">
                <i class="icon-paypal"></i> {l s='Module Paypal' mod='prestapp'}
              </div>

              <div class="alert alert-info">{l s='Please refer to our documentation to know how to get these credentials' mod='prestapp'}</div>
              {if $PRESTAPP_PAYPAL_ERROR === true}
                <div class="alert alert-danger">
                    {l s='An error occured: please check the fields are not empty' mod='prestapp'}
                </div>
              {/if}
              {if $PRESTAPP_PAYPAL_ERROR === false}
                <div class="alert alert-success">
                    {l s='Configuration has been saved' mod='prestapp'}
                </div>
              {/if}
              <div class="form-wrapper">
                <div class="form-group">
                  <label class="control-label col-lg-3"> {l s='PayPal Secret key' mod='prestapp'}</label>
                  <div class="col-lg-9">
                    <input type="text" name="PRESTAPP_PAYPAL_SECRET_KEY" id="PRESTAPP_PAYPAL_SECRET_KEY" value="{$PRESTAPP_PAYPAL_SECRET_KEY|escape:'htmlall':'UTF-8'}" class="fixed-width-xl">
                  </div>
                </div>
              </div>

              <div class="form-wrapper">
                <div class="form-group">
                  <label class="control-label col-lg-3"> {l s='PayPal Client ID' mod='prestapp'}</label>
                  <div class="col-lg-9">
                    <input type="text" name="PRESTAPP_PAYPAL_CLIENT_ID" id="PRESTAPP_PAYPAL_CLIENT_ID" value="{$PRESTAPP_PAYPAL_CLIENT_ID|escape:'htmlall':'UTF-8'}" class="fixed-width-xl">
                  </div>
                </div>
              </div>

              <div class="panel-footer">
                <button type="submit" value="1" name="submitPrestAppPaypal" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save' mod='prestapp'}</button>
              </div>
            </div>
          </form>
        {/if}
      </div>

      <div class="tab-pane fade" id="options" role="tabpanel" aria-labelledby="options-tab">

        <div class="panel">
          <div class="alert alert-info">
            <p>
              {l s='Some useful features for you' mod='prestapp'}
            </p>
          </div>
        </div>


        <div class="panel">
          <div class="panel-heading">
            {l s='GDPR compliance' mod='prestapp'}
          </div>

          <form id="custom_form" class="defaultForm form-horizontal" method="post" enctype="multipart/form-data" novalidate="">
            <input type="hidden" name="submitGDPR" value="1">

            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="PRESTAPP_CUSTOM_GDPR" id="PRESTAPP_CUSTOM_GDPR" value="{$PRESTAPP_CUSTOM_GDPR|escape:'htmlall':'UTF-8'}">
              <label class="form-check-label" for="PRESTAPP_CUSTOM_GDPR"> {l s='Activate the GDPR banner' mod='prestapp'}</label>
            </div>

            <div class="form-wrapper">
              <div class="form-group" id="INPUT_TEXT_GDPR">
                <label class="control-label"> {l s='Banner text' mod='prestapp'}</label>
                <input type="text" name="PRESTAPP_CUSTOM_GDPR_TEXT" id="PRESTAPP_CUSTOM_GDPR_TEXT" value="{$PRESTAPP_CUSTOM_GDPR_TEXT|escape:'htmlall':'UTF-8'}">
              </div>
            </div>

            <div class="panel-footer">
              <button type="submit" value="1" name="submitPrestAppCustom" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save' mod='prestapp'}</button>
            </div>
          </form>
        </div>

        <div class="panel">
          <div class="panel-heading">
            {l s='PWA redirection settings' mod='prestapp'}
          </div>

          <form id="pwa_redirect_form" class="defaultForm form-horizontal" method="post" enctype="multipart/form-data" novalidate="">
            <input type="hidden" name="submitRedirectionSettings" value="1">

            <div class="form-wrapper">
              <div class="form-group">
                <label class="control-label"> {l s='Full PWA url' mod='prestapp'}</label>
                <input type="text" name="PRESTAPP_PWA_URL" id="PRESTAPP_PWA_URL" value="{$PRESTAPP_PWA_URL|escape:'htmlall':'UTF-8'}">
              </div>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" name="PRESTAPP_REDIRECTION" id="noredirection" value="NO_REDIRECTION" {if $PRESTAPP_REDIRECTION === "NO_REDIRECTION"} checked{/if}>
              <label class="form-check-label" for="noredirection">
                {l s='No redirection' mod='prestapp'}
              </label>
            </div>
            <div class="form-check disabled">
              <input class="form-check-input" type="radio" name="PRESTAPP_REDIRECTION" id="alwaysredirect" value="ALWAYS_REDIRECT" {if $PRESTAPP_REDIRECTION === "ALWAYS_REDIRECT"} checked{/if}>
              <label class="form-check-label" for="alwaysredirect">
                {l s='Always redirect mobile traffic' mod='prestapp'}
              </label>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" name="PRESTAPP_REDIRECTION" id="askpopin" value="ASK_POPIN"  {if $PRESTAPP_REDIRECTION === "ASK_POPIN"} checked{/if} disabled>
              <label class="form-check-label" for="askpopin">
                <span class="text-muted">{l s='Ask with a popin' mod='prestapp'} {l s='(soon)' mod='prestapp'}</span>
              </label>
            </div>

            <div class="panel-footer">
              <button type="submit" value="1" name="submitPrestAppCustom" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save' mod='prestapp'}</button>
            </div>
          </form>
        </div>

      </div>

    </div>
  </div>
</div>