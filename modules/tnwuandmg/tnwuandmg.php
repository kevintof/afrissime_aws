<?php
/**
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class TnWUAndMG extends PaymentModule
{
    private $_html = '';
    private $_postErrors = array();
    private $_postMessages = array();
    public $statuses = array('tnwuandmg_wu' => 'WesternUnion', 'tnwuandmg_mg' => 'MoneyGram');

    public function __construct()
    {
        $this->name = 'tnwuandmg';
        $this->tab = 'payments_gateways';
        $this->version = '2.0.1';
        $this->author = 'Terranet';
        $this->module_key = '30639bff4320c65a66396515b80e6d8f';
        $this->controllers = array('payment', 'validation');

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('TN Western Union and MoneyGram Payment');
        $this->description = $this->l('Accept payments using TN Western Union and MoneyGram.');
        $this->confirmUninstall = $this->l('Confirm uninstall of module - TN Western Union and MoneyGram Payment?');
        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);

        if ((!isset($this->checkName) || !isset($this->address) || empty($this->checkName) || empty($this->address))) {
            $this->warning = $this->l('The "Payee" and "Address" fields must be configured before using this module.');
        }
        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $this->warning = $this->l('No currency has been set for this module.');
        }
        if (!$this->existAdminsTr()) {
            $this->warning = $this->l('Account owner and details must be configured in order to use this module correctly.');
        }
    }

    public function existAdminsTr()
    {
        $res = 0;
        $is_exist_table = Db::getInstance()->executeS("SHOW TABLES LIKE '" . _DB_PREFIX_ . pSQL($this->name) . "'");

        if ((bool)$is_exist_table) {
            $users = Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . pSQL($this->name) . ' WHERE `active` = 1');
            if ($users && count($users) > 0) {
                foreach ($users as $user) {
                    if (!empty($user['fullname']) || !empty($user['details']) || !empty($user['vat_number'])) {
                        $res = 1;
                    }
                }
            }
        }

        return $res;
    }

    public function install()
    {
        $this->setStatuses();
        $languages = Language::getLanguages(false);
        foreach ($languages as $language) {
            // check if folder exist, if not create it
            if (!file_exists(dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"])) {
                mkdir(dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"], 0777, true);
            }

            // create email templates
            if (file_exists(dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.html')) {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.html',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.html'
                );
            } elseif (!file_exists(dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.html')) {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/tnwuandmg_mg.html',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.html'
                );
            }

            if (file_exists(dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.html')) {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.html',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.html'
                );
            } elseif (!file_exists(dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.html')) {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/tnwuandmg_wu.html',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.html'
                );
            }

            if (file_exists(dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.txt')) {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.txt',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.txt'
                );
            } else {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/tnwuandmg_wu.txt',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_wu.txt'
                );
            }

            if (file_exists(dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.txt')) {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.txt',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.txt'
                );
            } else {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/tnwuandmg_mg.txt',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/tnwuandmg_mg.txt'
                );
            }

            if (!file_exists(dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/lang.php')) {
                $file_data = "<?php\n";
                $file_data .= "/**\n";
                $file_data .= " * 2016 TN\n";
                $file_data .= " *\n";
                $file_data .= " * NOTICE OF LICENSE\n";
                $file_data .= " *\n";
                $file_data .= " *  @author    TN\n";
                $file_data .= " *  @copyright 2016 TN\n";
                $file_data .= " *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)\n";
                $file_data .= " */\n";
                $file_data .= "global \$_LANGMAIL;\n";
                $file_data .= "?>\n\n";

                $file_data .= Tools::file_get_contents(dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/lang.php');
                file_put_contents(dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/lang1.php', $file_data);

                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/lang1.php',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/lang.php'
                );
                unlink(dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/lang1.php');
            }

            if (!file_exists(dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/index.php')) {
                copy(
                    dirname(dirname(dirname(__file__))) . '/modules/' . $this->name . '/mails/en/index.php',
                    dirname(dirname(dirname(__file__))) . '/mails/' . $language["iso_code"] . '/index.php'
                );
            }
        }

        return parent::install()
        && $this->installDb()
        && $this->installTab()
        && Configuration::updateValue('TNWUANDMG_MAIL', '')
        && Configuration::updateValue('TNWUANDMG_MAIL_ACTIVE', 0)
        && Configuration::updateValue('TNWUANDMG_WESTERUNION_PAY_ONLINE', 'https://www.westernunion.com/us/en/send-money/start.html')
        && Configuration::updateValue('TNWUANDMG_MONEYGRAM_PAY_ONLINE', 'https://secure.moneygram.com')
        && $this->registerHook('customerAccount')
        && $this->registerHook('paymentOptions')
        && $this->registerHook('displayHeader')
        && $this->registerHook('paymentReturn');
    }

    public function hookCustomerAccount($params)
    {
        return $this->display(__FILE__, 'my-account.tpl');
    }

    protected function setStatuses()
    {
        /* Installing & Validation Statuses
         * */
        $statuses_exists = $this->getIdIfExistThisState();
        if ($statuses_exists && is_array($statuses_exists) && count($statuses_exists) > 0) {
            $temp_st = $this->statuses;
            foreach ($statuses_exists as $statuses_exist) {
                if ($statuses_exist && isset($statuses_exist['id_order_state'])
                    && !empty($statuses_exist['id_order_state']) && is_numeric($statuses_exist['id_order_state'])
                    && isset($statuses_exist['template']) && !empty($statuses_exist['template'])
                    && isset($temp_st[$statuses_exist['template']])
                ) {
                    if (!Configuration::getGlobalValue('PS_OS_TN' . $temp_st[$statuses_exist['template']])
                        || Configuration::getGlobalValue('PS_OS_TN' . $temp_st[$statuses_exist['template']]) != $statuses_exist['id_order_state']
                    ) {
                        Configuration::updateGlobalValue('PS_OS_TN' . $temp_st[$statuses_exist['template']], $statuses_exist['id_order_state']);
                    }
                    unset($temp_st[$statuses_exist['template']]);
                }
            }
            if (is_array($temp_st) && count($temp_st) > 0) {
                foreach ($temp_st as $type_status => $status) {
                    $OrderStateId = $this->addOrderState($type_status, $status);
                    if (is_numeric($OrderStateId) && $OrderStateId > 0) {
                        Configuration::updateGlobalValue('PS_OS_TN' . Tools::strtoupper($status), $OrderStateId);
                    }
                }
            }
        } else {
            foreach ($this->statuses as $type_status => $status) {
                $OrderStateId = $this->addOrderState($type_status, $status);
                if (is_numeric($OrderStateId) && $OrderStateId > 0) {
                    Configuration::updateGlobalValue('PS_OS_TN' . Tools::strtoupper($status), $OrderStateId);
                }
            }
        }

        return true;
    }

    public function getIdIfExistThisState()
    {
        return Db::getInstance()->executeS('SELECT DISTINCT os.`id_order_state`, os_lg.`template`
                                                FROM `' . _DB_PREFIX_ . 'order_state` AS os
                                                LEFT JOIN `' . _DB_PREFIX_ . 'order_state_lang` AS os_lg
                                                    ON os.id_order_state = os_lg.id_order_state
                                                WHERE os.`module_name` = "' . pSQL($this->name) . '" ');
    }

    public function addOrderState($type_status = '', $type = '')
    {
        $available_statuses = array('WesternUnion', 'MoneyGram');
        if (empty($type) || empty($type_status) || !in_array($type, $available_statuses)) {
            return '';
        }

        $status = '';
        switch ($type) {
            case 'WesternUnion':
                $status = $this->l('Awaiting Western Union payment');
                break;
            case 'MoneyGram':
                $status = $this->l('Awaiting MoneyGram payment');
                break;
        }

        $languages = Language::getLanguages(false);

        $OrderState = new OrderState();
        $OrderState->name = array();
        $OrderState->invoice = false;
        $OrderState->send_email = true;
        $OrderState->module_name = $this->name;
        $OrderState->color = '#FF8C00';
        $OrderState->unremovable = true;
        $OrderState->hidden = false;
        $OrderState->logable = false;
        $OrderState->delivery = false;
        $OrderState->shipped = false;
        $OrderState->paid = false;
        $OrderState->pdf_delivery = false;
        $OrderState->pdf_invoice = false;
        $OrderState->deleted = false;
        $OrderState->template = array();
        foreach ($languages as $language) {
            $OrderState->name[$language['id_lang']] = $status;
            $OrderState->template[$language['id_lang']] = $type_status;
        }
        $OrderState->add();

        return $OrderState->id;
    }

    public function installDb()
    {
        return (Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . '' . pSQL($this->name) . '` (
			`id_tnwuandmg` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`module` VARCHAR( 255 ) NOT NULL,
			`fullname` VARCHAR( 255 ) NOT NULL,
			`id_customer` VARCHAR( 255 ) NOT NULL,
            `details` TEXT,
            `address` TEXT,
            `vat_number` VARCHAR( 255 ) DEFAULT NULL,
            `active` tinyint(1) NOT NULL DEFAULT "0",
            `id_customer_active` tinyint(1) NOT NULL DEFAULT "0",
            `details_active` tinyint(1) NOT NULL DEFAULT "0",
            `address_active` tinyint(1) NOT NULL DEFAULT "0",
            `vat_number_active` tinyint(1) NOT NULL DEFAULT "0",

			PRIMARY KEY (`id_tnwuandmg`)
		) ENGINE = ' . _MYSQL_ENGINE_ . ' CHARSET=utf8;')
            &&
            Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . '' . pSQL($this->name) . '_send` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `id_order` int(11) DEFAULT NULL,
            `id_cart` int(11) DEFAULT NULL,
            `customer_id` int(11) DEFAULT NULL,
            `firstname` varchar(255) DEFAULT NULL,
            `lastname` varchar(255) DEFAULT NULL,
            `other` text,
            `data` DATETIME DEFAULT NULL,
            `translation_key` varchar(255) DEFAULT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;')
            &&
            Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . '' . pSQL($this->name) . '_orders` (
            `id_tnwuandmg` int(11) DEFAULT NULL,
            `id_cart` int(11) DEFAULT NULL,
            `hash` varchar(255) DEFAULT NULL,
            INDEX (`id_cart`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;')
            &&
            Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . '' . pSQL($this->name) . '_currency` (
            `id_tnwuandmg` int(11) NOT NULL,
            `currency_id` int(11) NOT NULL,
            INDEX (`id_tnwuandmg`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;')
        );
    }

    private function installTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->name = array();
        $tab->class_name = 'AdminTnWuAndMg';

        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'W-Union and M-Gram';
        }

        $tab->id_parent = (int)Tab::getIdFromClassName('AdminParentPayment');
        $tab->module = $this->name;

        return $tab->add();
    }

    private function uninstallTab()
    {
        $id_tab = (int)Tab::getIdFromClassName('AdminTnWuAndMg');
        if ($id_tab) {
            $tab = new Tab($id_tab);
            return $tab->delete();
        }

        return false;
    }

    public function uninstall()
    {
        $this->uninstallTab();

        if (!parent::uninstall()
            && !Configuration::deleteByName('TNWUANDMG_MAIL')
            && !Configuration::deleteByName('TNWUANDMG_MAIL_ACTIVE')
            && !Configuration::deleteByName('TNWUANDMG_WESTERUNION_PAY_ONLINE')
            && !Configuration::deleteByName('TNWUANDMG_MONEYGRAM_PAY_ONLINE')
        ) {
            return false;
        }
        if (!$this->uninstallDB()) {
            return false;
        }

        return true;
    }

    private function uninstallDb()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . '' . pSQL($this->name) . '`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . '' . pSQL($this->name) . '_send`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . '' . pSQL($this->name) . '_currency`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . '' . pSQL($this->name) . '_orders`');
        return true;
    }

    private function _postValidation()
    {
        if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('owner')) {
                $this->_postErrors[] = $this->l('The "Payee" field is required.');
            }
        }
    }

    private function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit') && (!Tools::isSubmit('id_tnwuandmg') || empty(Tools::getValue('id_tnwuandmg')))) {
            $this->postProcessNewData();
        } elseif (Tools::isSubmit('btnSubmit') && Tools::isSubmit('id_tnwuandmg') && !empty(Tools::getValue('id_tnwuandmg'))) {
            $this->postProcessUpdateDetails();
        }
        if (count($this->_postMessages)) {
            foreach ($this->_postMessages as $err) {
                $this->_html .= $this->displayConfirmation($err);
            }
        } elseif (count($this->_postErrors)) {
            foreach ($this->_postErrors as $err) {
                $this->_html .= $this->displayError($err);
            }
        }
    }

    protected function postProcessNewData()
    {
        if ($this->isNewDataValid() == true) {
            $owner = Tools::getValue('owner');
            $id_customer = urlencode(Tools::getValue('id_customer'));
            $details = Tools::getValue('details');
            $vat_number = Tools::getValue('vat_number');
            $address = Tools::getValue('address');
            $type_transfer = Tools::getValue('type_transfer');
            $id_customer_active = Tools::getValue('id_customer_active');
            $details_active = Tools::getValue('details_active');
            $vat_number_active = Tools::getValue('vat_number_active');
            $address_active = Tools::getValue('address_active');

            $currency_selected = array();
            $currencies = Currency::getCurrencies();
            if ($currencies && count($currencies) > 0) {
                foreach ($currencies as $currency) {
                    if (Tools::isSubmit('selected_currency_' . $currency['id_currency'])
                        && Tools::getValue('selected_currency_' . $currency['id_currency'])
                    ) {
                        $currency_selected[] = $currency['id_currency'];
                    }
                }
            }

            $result = Db::getInstance()->getRow('SELECT `id_tnwuandmg` FROM ' . _DB_PREFIX_ . pSQL($this->name) . '
				WHERE `fullname` = \'' . pSQL($owner) . '\' AND `id_customer` = \'' . pSQL($id_customer) . '\' AND `details` = \'' . pSQL($details) . '\'
				AND `vat_number` = \'' . pSQL($vat_number) . '\' AND `module` = \'' . pSQL($type_transfer) . '\'');

            if ($result == false) {
                $query = 'INSERT INTO ' . _DB_PREFIX_ . $this->name . '
                (`fullname`, `id_customer`, `details`, `vat_number`, `address`, `active`,
                `id_customer_active`,
                `details_active`,
                `vat_number_active`,
                `address_active`,
                `module`)
                VALUES
                (\'' . pSQL($owner) . '\', \'' . pSQL($id_customer) . '\', \'' . pSQL($details) . '\', \'' . pSQL($vat_number) . '\',\'' . pSQL($address) . '\', 1,
                ' . (int)$id_customer_active . ',
                ' . (int)$details_active . ',
                ' . (int)$vat_number_active . ',
                ' . (int)$address_active . ',
                \'' . pSQL($type_transfer) . '\')';

                if (Db::getInstance()->execute($query)) {
                    $last_id = Db::getInstance()->Insert_ID();
                    $error = false;
                    if (count($currency_selected) > 0) {
                        foreach ($currency_selected as $currency) {
                            if (Validate::isInt($currency) && $currency > 0) {
                                $query = 'INSERT INTO `' . _DB_PREFIX_ . $this->name . '_currency` (`id_tnwuandmg`, `currency_id`) VALUES (' . (int)$last_id . ',' . (int)$currency . ')';
                                if (!Db::getInstance()->execute($query)) {
                                    $error = true;
                                }
                            }
                        }
                    }

                    if ($error) {
                        $this->_postErrors[] = $this->l('An error happened: this Currency could not be added');
                        return false;
                    }
                    $this->_postMessages[] = $this->l('New customer has been successfully added.');
                    return true;
                }
                $this->_postMessages[] = $this->l('An error happened: this customer could not be added.');
                return true;
            }
            $this->_postMessages[] = $this->l('This customer already exists.');
            return true;
        }

        return false;
    }

    protected function isNewDataValid()
    {
        if ((Tools::isSubmit('owner') == true)) {
            return true;
        }

        return false;
    }

    protected function postProcessUpdateDetails()
    {
        if (Tools::isSubmit('id_tnwuandmg') == false) {
            return false;
        }

        $fullname = Tools::getValue('owner');
        $id_customer = Tools::getValue('id_customer');
        $details = Tools::getValue('details');
        $vat_number = Tools::getValue('vat_number');
        $id_tnwuandmg = (int)Tools::getValue('id_tnwuandmg');
        $address = Tools::getValue('address');
        $id_customer_active = Tools::getValue('id_customer_active');
        $details_active = Tools::getValue('details_active');
        $vat_number_active = Tools::getValue('vat_number_active');
        $address_active = Tools::getValue('address_active');

        $query = 'UPDATE ' . _DB_PREFIX_ . $this->name . '
			SET `fullname` = \'' . pSQL($fullname) . '\',
				`id_customer` = \'' . pSQL($id_customer) . '\',
				`details` = \'' . pSQL($details) . '\',
				`vat_number` = \'' . pSQL($vat_number) . '\',
				`address` = \'' . pSQL($address) . '\',
				`id_customer_active` = ' . (int)$id_customer_active . ',
				`details_active` = ' . (int)$details_active . ',
				`vat_number_active` = ' . (int)$vat_number_active . ',
				`address_active` = ' . (int)$address_active . '
			WHERE `id_tnwuandmg` = \'' . (int)$id_tnwuandmg . '\'';

        if (($result = Db::getInstance()->execute($query)) != false) {
            $currency_selected = array();
            $currencies = Currency::getCurrencies();
            if ($currencies && count($currencies) > 0) {
                foreach ($currencies as $currency) {
                    if (Tools::isSubmit('selected_currency_' . $currency['id_currency'])
                        && Tools::getValue('selected_currency_' . $currency['id_currency'])
                    ) {
                        $currency_selected[] = $currency['id_currency'];
                    }
                }
            }

            $error = false;
            Db::getInstance()->delete($this->name . '_currency', 'id_tnwuandmg = ' . (int)$id_tnwuandmg);
            if (count($currency_selected) > 0) {
                foreach ($currency_selected as $currency) {
                    $query = 'INSERT INTO `' . _DB_PREFIX_ . $this->name . '_currency`
                            (`id_tnwuandmg`, `currency_id`) VALUES (' . (int)$id_tnwuandmg . ',' . (int)$currency . ')';
                    if (!Db::getInstance()->execute($query)) {
                        $error = true;
                    }
                }
            }
            if ($error) {
                $this->_postErrors[] = $this->l('An error happened: this Currency could not be added');
                return false;
            }

            $this->_postMessages[] = $this->l('Contact details has been updated.');

            return true;
        }

        $this->_postErrors[] = $this->l('Contact details has not been updated.');
        return false;
    }

    private function _displayCheck()
    {
        return $this->display(__FILE__, '/views/templates/hook/infos.tpl');
    }

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('changeMailTranslationOrder')) {
            $mail = Tools::getValue('TNWUANDMG_MAIL');
            $mail = htmlspecialchars((string)$mail);

            if (!empty($mail) && Validate::isEmail($mail)) {
                Configuration::updateValue('TNWUANDMG_MAIL', $mail);
            } else {
                if (empty($mail)) {
                    Configuration::updateValue('TNWUANDMG_MAIL', '');
                } else {
                    $this->errors[] = sprintf($this->displayError('Mail "%s" is incorrect!'), $mail);
                }
            }
            if (Tools::isSubmit('TNWUANDMG_MAIL_ACTIVE')) {
                Configuration::updateValue('TNWUANDMG_MAIL_ACTIVE', (int)Tools::getValue('TNWUANDMG_MAIL_ACTIVE'));
            }
            if (Tools::isSubmit('TNWUANDMG_WESTERUNION_PAY_ONLINE')) {
                Configuration::updateValue('TNWUANDMG_WESTERUNION_PAY_ONLINE', Tools::getValue('TNWUANDMG_WESTERUNION_PAY_ONLINE'));
            }
            if (Tools::isSubmit('TNWUANDMG_MONEYGRAM_PAY_ONLINE')) {
                Configuration::updateValue('TNWUANDMG_MONEYGRAM_PAY_ONLINE', Tools::getValue('TNWUANDMG_MONEYGRAM_PAY_ONLINE'));
            }
        }

        if (Tools::isSubmit('btnSubmit')) {
            $this->_postValidation();
            if (!count($this->_postErrors)) {
                $this->_postProcess();
            } else {
                foreach ($this->_postErrors as $err) {
                    $this->_html .= $this->displayError($err);
                }
            }
        } elseif (Tools::isSubmit('deletetnwuandmg') && Tools::isSubmit('id_tnwuandmg')
            && Validate::isInt(Tools::getValue('id_tnwuandmg'))
            && Tools::getValue('id_tnwuandmg') > 0
        ) {
            $this->postProcessDeleteWuAndMgContact((int)Tools::getValue('id_tnwuandmg'));
        } elseif (Tools::isSubmit('statustnwuandmg') && Tools::isSubmit('id_tnwuandmg')
            && Validate::isInt(Tools::getValue('id_tnwuandmg'))
            && Tools::getValue('id_tnwuandmg') > 0
        ) {
            $this->postProcessChangeStatus((int)Tools::getValue('id_tnwuandmg'));
        }

        $this->_html .= $this->_displayCheck();

        if ((bool)Tools::isSubmit('add') === true && Tools::getValue('add') == 1
            || (bool)Tools::isSubmit('updatetnwuandmg') === true && Tools::getValue('id_tnwuandmg') > 0
        ) {
            $this->_html .= $this->renderForm();
        } else {
            $this->_html .= $this->renderList() . $this->renderMailForm();
        }

        return $this->_html;
    }

    protected function renderMailForm()
    {
        $form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Email Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Active'),
                        'name' => 'TNWUANDMG_MAIL_ACTIVE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled'),
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled'),
                            ),
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Email'),
                        'name' => 'TNWUANDMG_MAIL',
                        'col' => 4,
                        'desc' => $this->l('Send notification to this email'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Reference to the  WesterUnion payment'),
                        'name' => 'TNWUANDMG_WESTERUNION_PAY_ONLINE',
                        'col' => 4,
                        'desc' => $this->l('Online link to the payment '),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Reference to the MoneyGram payment'),
                        'name' => 'TNWUANDMG_MONEYGRAM_PAY_ONLINE',
                        'col' => 4,
                        'desc' => $this->l('Online link to the payment '),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            )
        );

        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'changeMailTranslationOrder';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => array(
                'TNWUANDMG_MAIL' => Configuration::get('TNWUANDMG_MAIL'),
                'TNWUANDMG_MAIL_ACTIVE' => Configuration::get('TNWUANDMG_MAIL_ACTIVE'),
                'TNWUANDMG_WESTERUNION_PAY_ONLINE' => Configuration::get('TNWUANDMG_WESTERUNION_PAY_ONLINE'),
                'TNWUANDMG_MONEYGRAM_PAY_ONLINE' => Configuration::get('TNWUANDMG_MONEYGRAM_PAY_ONLINE')
            ),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($form));
    }

    protected function postProcessDeleteWuAndMgContact($id_tnwuandmg)
    {
        if ((bool)$id_tnwuandmg != false) {
            Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . pSQL($this->name) . ' WHERE `id_tnwuandmg` = \'' . (int)$id_tnwuandmg . '\'');
        }

        return Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }

    protected function postProcessChangeStatus($id_tnwuandmg)
    {
        if ((bool)$id_tnwuandmg != false) {
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . pSQL($this->name) . ' SET active = !active WHERE `id_tnwuandmg` = \'' . (int)$id_tnwuandmg . '\'');
        }

        return Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }

    public function renderList()
    {
        $helper = new HelperList();

        $helper->title = $this->l('Contact details');
        $helper->table = $this->name;
        $helper->no_link = true;
        $helper->shopLinkType = '';
        $helper->identifier = 'id_tnwuandmg';
        $helper->actions = array('edit', 'delete');

        $values = $this->getListFormValues();
        $helper->listTotal = count($values);

        $helper->tpl_vars = array('show_filters' => false);

        $helper->toolbar_btn['new'] = array(
            'href' => $this->context->link->getAdminLink('AdminModules', false)
                . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name
                . '&add=1&token=' . Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new contact details'),
        );

        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;

        return $helper->generateList($values, $this->getList());
    }

    public function getList()
    {
        return array(
            'user_fullname' => array(
                'title' => $this->l('Beneficiary name'),
                'type' => 'text',
                'orderby' => false
            ),
            'active' => array(
                'title' => $this->l('Active'),
                'active' => 'status',
                'type' => 'bool',
                'align' => 'center',
                'orderby' => false
            ),
        );
    }

    public function getListValues($with_currency = false)
    {
        if (!$with_currency) {
            $customers = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . $this->name . '`');
        } else {
            if (isset($this->context->cookie->id_currency) && Validate::isInt($this->context->cookie->id_currency)
                && $this->context->cookie->id_currency > 0
            ) {
                $currency = (int)$this->context->cookie->id_currency;
            } else {
                $currency = (int)Context::getContext()->currency->id;
            }

            $customers = Db::getInstance()->executeS('SELECT ctm.* FROM `' . _DB_PREFIX_ . $this->name . '` AS ctm
                    LEFT JOIN `' . _DB_PREFIX_ . $this->name . '_currency` AS cy ON (cy.`id_tnwuandmg` = ctm.`id_tnwuandmg`)
                    WHERE cy.`currency_id` = ' . (int)$currency);
        }

        $customer_sort = array();
        foreach ($customers as $key => $customer) {
            $customer['user_fullname'] = $customer['fullname'];
            $customer_sort[$customer['module']][] = $customer;
        }

        return $customer_sort;
    }

    protected function renderForm()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues()
        );

        $this->fields_form = array();

        return $helper->generateForm($this->_getForm());
    }

    public function getConfigFieldsValues()
    {
        if ((bool)Tools::isSubmit('add') === true && Tools::getValue('add') == 1) {
            return $this->getNewFormValues();
        } elseif ((bool)Tools::isSubmit('updatetnwuandmg') && (bool)Tools::isSubmit('id_tnwuandmg')
            && Validate::isInt(Tools::getValue('id_tnwuandmg')) && Tools::getValue('id_tnwuandmg') > 0
        ) {
            return $this->getUpdateFormValues();
        }
    }

    public function getNewFormValues()
    {
        return array(
            'owner' => Tools::getValue('owner', null),
            'id_customer' => Tools::getValue('id_customer', null),
            'details' => Tools::getValue('details', null),
            'vat_number' => Tools::getValue('vat_number', null),
            'address' => Tools::getValue('address', null),
            'id_customer_active' => Tools::getValue('id_customer_active', 1),
            'details_active' => Tools::getValue('details_active', 1),
            'address_active' => Tools::getValue('address_active', 1),
            'vat_number_active' => Tools::getValue('vat_number_active', 1),
            'id_tnwuandmg' => (int)Tools::getValue('id_tnwuandmg', 0),
            'type_transfer' => Tools::getValue('type_transfer', key($this->statuses))
        );
    }

    public function getUpdateFormValues()
    {
        $contact = Db::getInstance()->getRow('
            SELECT * FROM `' . _DB_PREFIX_ . $this->name . '`
                WHERE `id_tnwuandmg` = \'' . (int)Tools::getValue('id_tnwuandmg') . '\'');

        if ($contact && count($contact) > 0 && (bool)$contact['id_tnwuandmg'] == true) {
            $currency_contact = Db::getInstance()->executeS('
                SELECT `currency_id` FROM `' . _DB_PREFIX_ . $this->name . '_currency`
                    WHERE `id_tnwuandmg` = \'' . (int)Tools::getValue('id_tnwuandmg') . '\'');

            $currency_selected = array();
            if ($currency_contact && count($currency_contact) > 0) {
                foreach ($currency_contact as $currency) {
                    if (Validate::isInt($currency['currency_id']) && $currency['currency_id'] > 0) {
                        $currency_selected['selected_currency_' . $currency['currency_id']] = $currency['currency_id'];
                    }
                }
            }

            return array_merge(array(
                'owner' => Tools::getValue('owner', $contact['fullname']),
                'id_customer' => Tools::getValue('customer_account', $contact['id_customer']),
                'details' => Tools::getValue('details', $contact['details']),
                'vat_number' => Tools::getValue('vat_number', $contact['vat_number']),
                'address' => Tools::getValue('address', $contact['address']),
                'id_customer_active' => Tools::getValue('id_customer_active', $contact['id_customer_active']),
                'details_active' => Tools::getValue('details_active', $contact['details_active']),
                'address_active' => Tools::getValue('address_active', $contact['address_active']),
                'vat_number_active' => Tools::getValue('vat_number_active', $contact['vat_number_active']),
                'id_tnwuandmg' => (int)Tools::getValue('id_tnwuandmg', $contact['id_tnwuandmg']),
                'type_transfer' => Tools::getValue('type_transfer', $contact['module'])
            ), $currency_selected);
        }
        return self::getNewFormValues();
    }

    public function checkCurrency($id_currency, $owner_selected)
    {
        if (isset($id_currency) && Validate::isInt($id_currency) && $id_currency > 0
            && isset($owner_selected) && !empty($owner_selected)
        ) {
            $currency_contact = Db::getInstance()->executeS('
                SELECT `currency_id` FROM `' . _DB_PREFIX_ . $this->name . '_currency`
                    WHERE `id_tnwuandmg` = \'' . (int)$owner_selected . '\'
                    AND `currency_id` = ' . (int)$id_currency);
            return $currency_contact;
        }
    }

    public function getTransferInformation($uid)
    {
        return Db::getInstance()->getRow('SELECT u.* FROM `' . _DB_PREFIX_ . 'tnwuandmg_orders` AS ord
                                            INNER JOIN `' . _DB_PREFIX_ . 'tnwuandmg` AS u
                                            ON u.id_tnwuandmg = ord.id_tnwuandmg
                                                WHERE ord.id_cart = ' . (int)$uid);
    }

    public function hookPaymentReturn($params)
    {
        $state = $params['order']->getCurrentState();
        if ($state) {
            $transferInformation = array();
            if (isset($params['order']->id_cart) && !empty($params['order']->id_cart) && is_numeric($params['order']->id_cart)) {
                $transferInformation = $this->getTransferInformation((int)$params['order']->id_cart);
            }
            $pay_online_url = '';
            if ($transferInformation['module'] == 'tnwuandmg_wu') {
                $pay_online_url = (Configuration::get('TNWUANDMG_WESTERUNION_PAY_ONLINE', 0) && Validate::isAbsoluteUrl(Configuration::get('TNWUANDMG_WESTERUNION_PAY_ONLINE'))) ? Configuration::get('TNWUANDMG_WESTERUNION_PAY_ONLINE') : '';
            } elseif ($transferInformation['module'] == 'tnwuandmg_mg') {
                $pay_online_url = (Configuration::get('TNWUANDMG_MONEYGRAM_PAY_ONLINE', 0) && Validate::isAbsoluteUrl(Configuration::get('TNWUANDMG_MONEYGRAM_PAY_ONLINE'))) ? Configuration::get('TNWUANDMG_MONEYGRAM_PAY_ONLINE') : '';
            }
            $this->context->smarty->assign(
                array(
                    'total_to_pay' => Tools::displayPrice(
                        $params['order']->getOrdersTotalPaid(),
                        new Currency($params['order']->id_currency),
                        false
                    ),
                    'wuandmgTransferList' => $transferInformation,
                    'status' => 'ok',
                    'id_order' => $params['order']->id,
                    'print_style' => $this->_path . 'views/css/print.css',
                    'modules_dir' => _MODULE_DIR_,
                    'pay_online_url' => $pay_online_url
                )
            );
        } else {
            $this->context->smarty->assign('status', 'failed');
        }

        if ($transferInformation['module'] == 'tnwuandmg_wu') {
            return $this->fetch('module:tnwuandmg/views/templates/hook/payment_return_wu.tpl');
        } elseif ($transferInformation['module'] == 'tnwuandmg_mg') {
            return $this->fetch('module:tnwuandmg/views/templates/hook/payment_return_mg.tpl');
        }

        return $this->display(__FILE__, 'payment_return.tpl');
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return array();
        }

        $owners = $this->getTemplateVars();

        $this->smarty->assign(
            $owners
        );

        $return_owners = array();
        if (!isset($owners['owners']['tnwuandmg_mg']) || count($owners['owners']['tnwuandmg_mg']) == 0
            || !isset($owners['owners']['tnwuandmg_mg'][0]['id_tnwuandmg']) || empty($owners['owners']['tnwuandmg_mg'][0]['id_tnwuandmg'])
        ) {
            $return_owners['tnwuandmg_mg'] = false;
        } else {
            $newOptionMG = new PaymentOption();
            $newOptionMG->setCallToActionText($this->l('Pay by MoneyGram'))
                ->setAction($this->context->link->getModuleLink($this->name, 'validation', array('mod' => 'mg'), true))
                ->setAdditionalInformation($this->fetch('module:tnwuandmg/views/templates/hook/tnwuandmg_intro_mg.tpl'));
            $return_owners['tnwuandmg_mg'] = true;
        }

        if (!isset($owners['owners']['tnwuandmg_wu']) || count($owners['owners']['tnwuandmg_wu']) == 0
            || !isset($owners['owners']['tnwuandmg_wu'][0]['id_tnwuandmg']) || empty($owners['owners']['tnwuandmg_wu'][0]['id_tnwuandmg'])
        ) {
            $return_owners['tnwuandmg_wu'] = false;
        } else {
            $newOptionWU = new PaymentOption();
            $newOptionWU->setCallToActionText($this->l('Pay by Western Union'))
                ->setAction($this->context->link->getModuleLink($this->name, 'validation', array('mod' => 'wu'), true))
                ->setAdditionalInformation($this->fetch('module:tnwuandmg/views/templates/hook/tnwuandmg_intro_wu.tpl'));
            $return_owners['tnwuandmg_wu'] = true;
        }

        $payment_options = array();
        ((isset($return_owners['tnwuandmg_wu']) && $return_owners['tnwuandmg_wu'] == true) ? $payment_options[] = $newOptionWU : '');
        ((isset($return_owners['tnwuandmg_mg']) && $return_owners['tnwuandmg_mg'] == true) ? $payment_options[] = $newOptionMG : '');

        return $payment_options;
    }

    public function getTemplateVars()
    {
        $cart = $this->context->cart;
        $total = Tools::displayPrice($cart->getOrderTotal(true, Cart::BOTH));
        $owners = $this->getListValues(true);

        return array(
            'total' => $total,
            'owners' => $owners,
            'custom_currency' => $this->context->cookie->id_currency,
            'currencies' => Currency::getCurrencies(),
            'id_order' => (int)$cart->id,
            'print_style' => $this->_path . 'views/css/print.css',
            'pay_online_url' => ''
        );
    }

    public function hookdisplayHeader($params)
    {
        $this->context->controller->registerStylesheet('modules-tnwuandmg', 'modules/' . $this->name . '/views/css/print.css', array('media' => 'all', 'priority' => 150));
        $this->context->controller->registerStylesheet('modules-tnwuandmg', 'modules/' . $this->name . '/views/css/front.css', array('media' => 'all', 'priority' => 150));
        $this->context->controller->registerJavascript('modules-tnwuandmg', 'modules/' . $this->name . '/views/js/payment.js', array('position' => 'bottom', 'priority' => 150));
    }

    public function getAdminTransfers($uid = '', $type_module = '')
    {
        if (!empty($uid) && is_numeric($uid)) {
            if (!empty($type_module)) {
                $sql = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL($this->name) . ' WHERE `active` = 1 AND `id_tnwuandmg` = ' . (int)$uid . ' AND module = "' . pSQL($type_module) . '"';
            } else {
                $sql = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL($this->name) . ' WHERE `active` = 1 AND `id_tnwuandmg` = ' . (int)$uid;
            }
        } else {
            if (!empty($type_module)) {
                $sql = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL($this->name) . ' WHERE `active` = 1 AND module = "' . pSQL($type_module) . '"';
            } else {
                $sql = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL($this->name) . ' WHERE `active` = 1 ';
            }
        }

        return Db::getInstance()->executeS($sql);
    }

    public function _getForm($title = 'New Contact Details', $update = false)
    {
        $form = array(
            array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l($title),
                        'icon' => 'icon-plus',
                    ),
                    'input' => array(),
                    'submit' => array('title' => $this->l('Save', 'ContactDetailsForms'), 'type' => 'submit', 'class' => 'btn btn-default pull-right'),
                ),
            ),
        );

        $type_transfer = array();
        if (isset($this->statuses) && is_array($this->statuses) && count($this->statuses)) {
            foreach ($this->statuses as $key_tr => $name_tr) {
                $type_transfer[] = array(
                    'id' => $key_tr,
                    'value' => $key_tr,
                    'label' => $name_tr
                );
            }
            $form[0]['form']['input'][] = array(
                'type' => 'radio',
                'label' => $this->l('Select type Transfer'),
                'name' => 'type_transfer',
                'required' => true,
                'is_bool' => true,
                'values' => $type_transfer
            );
        } else {
            $this->errors[] = $this->displayError('Problems with the status pay!');
        }

        $form[0]['form']['input'][] = array(
            'type' => 'hidden',
            'name' => 'id_tnwuandmg',
        );

        $form[0]['form']['input'][] = array(
            'type' => 'text',
            'name' => 'owner',
            'label' => $this->l('Beneficiary Full Name'),
        );

        $form[0]['form']['input'][] = array(
            'type' => 'switch',
            'label' => $this->l('Show Beneficiary ID'),
            'name' => 'id_customer_active',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => true,
                    'label' => $this->l('Enabled'),
                ),
                array(
                    'id' => 'active_off',
                    'value' => false,
                    'label' => $this->l('Disabled'),
                ),
            ),
        );
        $form[0]['form']['input'][] = array(
            'type' => 'text',
            'name' => 'id_customer',
            'label' => $this->l('Beneficiary ID'),
        );

        $form[0]['form']['input'][] = array(
            'type' => 'switch',
            'label' => $this->l('Show CIF/NF/DNI (VAT number)'),
            'name' => 'vat_number_active',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => true,
                    'label' => $this->l('Enabled'),
                ),
                array(
                    'id' => 'active_off',
                    'value' => false,
                    'label' => $this->l('Disabled'),
                ),
            ),
        );
        $form[0]['form']['input'][] = array(
            'type' => 'textarea',
            'name' => 'vat_number',
            'label' => $this->l('CIF/NF/DNI (VAT number)'),
            'desc' => $this->l('Such as VAT Number, required in some countries (Spain for example) .'),
        );

        $form[0]['form']['input'][] = array(
            'type' => 'switch',
            'label' => $this->l('Show Address'),
            'name' => 'address_active',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => true,
                    'label' => $this->l('Enabled'),
                ),
                array(
                    'id' => 'active_off',
                    'value' => false,
                    'label' => $this->l('Disabled'),
                ),
            ),
        );
        $form[0]['form']['input'][] = array(
            'type' => 'textarea',
            'name' => 'address',
            'label' => $this->l('Address'),
            'desc' => $this->l('Such as country, state, postal code and tel.'),
        );

        $form[0]['form']['input'][] = array(
            'type' => 'switch',
            'label' => $this->l('Show Details'),
            'name' => 'details_active',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => true,
                    'label' => $this->l('Enabled'),
                ),
                array(
                    'id' => 'active_off',
                    'value' => false,
                    'label' => $this->l('Disabled'),
                ),
            ),
        );

        $form[0]['form']['input'][] = array(
            'type' => 'textarea',
            'name' => 'details',
            'label' => $this->l('Details'),
            'desc' => $this->l('Detailed information.'),
        );

        $form[0]['form']['input'][] = array(
            'type' => 'checkbox',
            'label' => 'Selected currencies',
            'name' => 'selected_currency',
            'values' => array(
                'query' => Currency::getCurrencies(),
                'id' => 'id_currency',
                'name' => 'name'
            )
        );

        return $form;
    }

    public function getListFormValues()
    {
        $customers = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . pSQL($this->name) . '`');
        foreach ($customers as $key => &$customer) {
            $customer['user_fullname'] = $customer['fullname'];
            $customer['module'] = $this->statuses[$customer['module']];
        }

        return $customers;
    }

    public static function getCustomerOrdersModule($id_customer, $module = '', $show_hidden_status = false, Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        if (empty($module)) {
            $module = 'tnwuandmg';
        }

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT o.*, (SELECT SUM(od.`product_quantity`) FROM `' . _DB_PREFIX_ . 'order_detail` od WHERE od.`id_order` = o.`id_order`) nb_products
            FROM `' . _DB_PREFIX_ . 'orders` o WHERE o.`id_customer` = ' . (int)$id_customer . Shop::addSqlRestriction(Shop::SHARE_ORDER) . ' AND o.module = "' . pSQL($module) . '" GROUP BY o.`id_order` ORDER BY o.`date_add` DESC');
        if (!$res) {
            return array();
        }

        foreach ($res as $key => $val) {
            $res2 = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(' SELECT os.`id_order_state`,
                osl.`name` AS order_state, os.`invoice`, os.`color` as order_state_color
                    FROM `' . _DB_PREFIX_ . 'order_history` oh
                    LEFT JOIN `' . _DB_PREFIX_ . 'order_state` os ON (os.`id_order_state` = oh.`id_order_state`)
                    INNER JOIN `' . _DB_PREFIX_ . 'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state`
                        AND osl.`id_lang` = ' . (int)$context->language->id . ')
                    WHERE oh.`id_order` = ' . (int)$val['id_order'] . (!$show_hidden_status ? ' AND os.`hidden` != 1' : '') . '
                    AND os.module_name = "' . pSQL($module) . '" ORDER BY oh.`date_add` DESC, oh.`id_order_history` DESC
                LIMIT 1');
            if ($res2) {
                $res[$key] = array_merge($res[$key], $res2[0]);
            }
        }
        return $res;
    }

    public static function getCustomerOrdersModuleValidation($id_customer, $order_id)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT id_order FROM `' . _DB_PREFIX_ . 'orders`
            WHERE id_order = ' . (int)$order_id . '
             AND id_customer = ' . (int)$id_customer);
    }

    public static function getUnCustomerOrdersModuleValidation($order_id)
    {
        $ref = $order_id;
        if (strpos($order_id, '#') == 0) {
            $order_id = str_replace('#', '', $order_id);
        }

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('SELECT `id_order`, `id_cart`, `id_customer` FROM `' . _DB_PREFIX_ . 'orders`
            WHERE id_order = ' . (int)$order_id . ' OR `reference` = "' . pSQL($ref) . '"');
    }

    public static function addCustomerTranslationKey($order_id, $order_cart, $customer_id, $fn, $ls, $other, $tr_key, $uniqReference)
    {
        if (!empty($order_id) && is_numeric($order_id)
            && !empty($customer_id) && is_numeric($customer_id)
            && !empty($fn) && !empty($ls) && !empty($tr_key)
        ) {
            $insert_data = array(
                'id_order' => (int)$order_id,
                'id_cart' => (int)$order_cart,
                'customer_id' => (int)$customer_id,
                'firstname' => pSQL($fn),
                'lastname' => pSQL($ls),
                'other' => pSQL($other),
                'translation_key' => pSQL($tr_key),
                'data' => Date('Y-m-d H:i:s'));
            if (Db::getInstance()->insert('tnwuandmg_send', $insert_data, false, true)) {
                $id = (int)Db::getInstance()->Insert_ID();
                $configuration = Configuration::getMultiple(array('TNWUANDMG_MAIL', 'TNWUANDMG_MAIL_ACTIVE'));
                if ($id > 0 && $configuration['TNWUANDMG_MAIL_ACTIVE'] && !empty($configuration['TNWUANDMG_MAIL_ACTIVE'])) {
                    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
                    $send = Mail::Send(
                        (int)$default_lang,
                        'tnwuandmg_translation',
                        Mail::l('Western Union and Money Gram Payment information', (int)$default_lang),
                        array(
                            '{fname}' => $fn,
                            '{sname}' => $ls,
                            '{customer_id}' => (int)$customer_id,
                            '{order_id}' => (int)$order_id,
                            '{other}' => Tools::nl2br($other),
                            '{translation_key}' => $tr_key,
                            '{uniqReference}' => $uniqReference,
                            '{data}' => Date('Y-m-d H:i:s'),
                        ),
                        $configuration['TNWUANDMG_MAIL'],
                        Mail::l('Western Union and Money Gram Payment information', (int)$default_lang),
                        null,
                        (string)Configuration::get('PS_SHOP_NAME'),
                        null,
                        null,
                        _PS_MODULE_DIR_ . 'tnwuandmg/' . 'mails/'
                    );
                    if ($send) {
                        return true;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public static function getKeyValidatorValues($key)
    {
        $return = array();
        $order = Db::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'tnwuandmg_orders` WHERE `hash` = "' . pSQL($key) . '"');
        if ($order && count($order) > 0 && isset($order['id_cart']) && $order['id_cart'] > 0) {
            $return = Db::getInstance()->getRow('SELECT o.reference, c.firstname, c.lastname FROM `' . _DB_PREFIX_ . 'orders` AS o
                INNER JOIN `' . _DB_PREFIX_ . 'customer` AS c ON c.id_customer = o.id_customer
                WHERE o.`id_cart` = "' . (int)$order['id_cart'] . '"');
        }
        return $return;
    }
}
