[{shop_url}]

Customer Id: {customer_id}
Order id: {order_id}
First name: {fname}
Last name: {sname}
Transaction key: {translation_key}
Data: {data}
Other: {other}


{shop_name} - {shop_url}

{shop_url} powered by PrestaShop™