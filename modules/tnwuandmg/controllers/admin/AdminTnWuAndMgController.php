<?php
/**
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class AdminTnWuAndMgController extends ModuleAdminControllerCore
{
    public $name = 'tnwuandmg';
    public $bootstrap = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('deletetnwuandmg') && Tools::getValue('id')) {
            Db::getInstance()->delete('tnwuandmg_send', 'id = ' . (int)Tools::getValue('id'));
        }
    }

    public function initContent()
    {
        if (!$this->viewAccess()) {
            $this->errors[] = Tools::displayError('You do not have permission to view this.');
            return;
        }

        $this->getLanguages();
        $this->initToolbar();
        $this->initTabModuleList();
        $this->initPageHeaderToolbar();

        $this->content .= $this->renderList() . $this->renderOrdersList();

        $this->context->smarty->assign(array(
            'maintenance_mode' => !(bool)Configuration::get('PS_SHOP_ENABLE'),
            'content' => $this->content,
            'lite_display' => $this->lite_display,
            'url_post' => self::$currentIndex . '&token=' . $this->token,
            'show_page_header_toolbar' => $this->show_page_header_toolbar,
            'page_header_toolbar_title' => $this->page_header_toolbar_title,
            'title' => $this->page_header_toolbar_title,
            'toolbar_btn' => $this->page_header_toolbar_btn,
            'page_header_toolbar_btn' => $this->page_header_toolbar_btn
        ));
    }

    public function renderList()
    {
        $this->fields_list = array();
        $this->fields_list['customer_id'] = array('title' => $this->l('Customer Id'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-xs');
        $this->fields_list['id_order'] = array('title' => $this->l('Order Id'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-xs');
        $this->fields_list['data'] = array('title' => $this->l('Data'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-md');
        $this->fields_list['firstname'] = array('title' => $this->l('Firstname'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-md');
        $this->fields_list['lastname'] = array('title' => $this->l('Lastname'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-md');
        $this->fields_list['translation_key'] = array('title' => $this->l('Transaction key'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-md');
        $this->fields_list['other'] = array('title' => $this->l('Additional information'),
            'type' => 'text',
            'search' => true,
            'orderby' => true);
        $this->fields_list['id_customer'] = array('title' => $this->l('Beneficiary Id / Name'),
            'type' => 'select',
            'list' => $this->getCustomerTransferInformationList(),
            'filter_key' => 'id_tnwuandmg',
            'filter_type' => 'int',
            'search' => true,
            'orderby' => true);
        $this->fields_list['payment'] = array('title' => $this->l('Payment type'),
            'type' => 'select',
            'list' => $this->module->statuses,
            'filter_key' => 'tranzactionptype',
            'search' => true,
            'orderby' => true);

        $helper = new HelperList();
        $helper->module = $this;
        $helper->simple_header = false;
        $helper->title = $this->l('Users transactions');
        $helper->identifier = 'id';
        $helper->actions = array('order', 'delete');

        $helper->show_toolbar = true;
        $helper->shopLinkType = '';
        $helper->listTotal = 0;
        $helper->token = $this->token;
        $helper->table = 'tnwuandmg';
        $helper->table_id = 'module-tnwuandmg';
        $helper->currentIndex = self::$currentIndex;

        return $helper->generateList($this->getListWuandmg(), $this->fields_list);
    }

    public function renderOrdersList()
    {
        $this->fields_list = array();
        $this->fields_list['id_customer'] = array('title' => $this->l('Customer Id'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-xs');
        $this->fields_list['id_order'] = array('title' => $this->l('Order Id'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-xs');
        $this->fields_list['date_add'] = array('title' => $this->l('Data'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-md');
        $this->fields_list['firstname'] = array('title' => $this->l('Firstname'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-md');
        $this->fields_list['lastname'] = array('title' => $this->l('Lastname'),
            'type' => 'text',
            'search' => true,
            'orderby' => true,
            'class' => 'fixed-width-md');
        $this->fields_list['current_state'] = array('title' => $this->l('Order Status'),
            'type' => 'select',
            'color' => 'color',
            'list' => $this->statusesArray(),
            'filter_key' => 'id_order_state',
            'filter_type' => 'int',
            'search' => true,
            'orderby' => true);
        $this->fields_list['id_tnwuandmg'] = array('title' => $this->l('Beneficiary Id / Name'),
            'type' => 'select',
            'list' => $this->getCustomerTransferInformationList(),
            'filter_key' => 'id_tnwuandmg',
            'filter_type' => 'int',
            'search' => true,
            'orderby' => true);
        $this->fields_list['payment'] = array('title' => $this->l('Payment type'),
            'type' => 'select',
            'list' => $this->module->statuses,
            'filter_key' => 'ptype',
            'search' => true,
            'orderby' => true);

        $helper = new HelperList();
        $helper->module = $this;
        $helper->simple_header = false;
        $helper->title = $this->l('The orders list');
        $helper->identifier = 'id_order';
        $helper->actions = array('Orderlist');

        $helper->show_toolbar = true;
        $helper->shopLinkType = '';
        $helper->listTotal = 0;
        $helper->token = $this->token;
        $helper->table = 'tnwuandmg_orders';
        $helper->table_id = 'module-tnwuandmg';
        $helper->currentIndex = self::$currentIndex;

        return $helper->generateList($this->getOrdersListWuandmg(), $this->fields_list);
    }

    public function statusesArray()
    {
        $statuses_array = array();
        $statuses = OrderState::getOrderStates((int)$this->context->language->id);
        $listExistStates = $this->getTransfersStatesList();
        if ($listExistStates && is_array($listExistStates) && count($listExistStates) > 0) {
            foreach ($statuses as $status) {
                if (in_array($status['id_order_state'], $listExistStates)) {
                    $statuses_array[$status['id_order_state']] = $status['name'];
                }
            }
        }

        return $statuses_array;
    }

    public function displayOrderLink($token = '', $id = '')
    {
        if (!empty($id)) {
            $tpl = $this->createTemplate('helpers/list/list_action_edit.tpl');
            if (!array_key_exists('Order', self::$cache_lang)) {
                self::$cache_lang['Order'] = $this->l('Order', 'Helper');
            }

            $order_id = $this->getOrderIdTransferInformationId($id);
            if (!empty($order_id)) {
                $link = new Link;
                $href = $link->getAdminLink('AdminOrders') . '&vieworder&id_order=' . (int)$order_id;
                $tpl->assign(array(
                    'href' => $href,
                    'action' => self::$cache_lang['Order'],
                    'id' => $id,
                ));
                return $tpl->fetch();
            }
        }
        return '';
    }

    public function displayOrderlistLink($token = '', $id = '')
    {
        if (!empty($id)) {
            $tpl = $this->createTemplate('helpers/list/list_action_edit.tpl');
            if (!array_key_exists('Order', self::$cache_lang)) {
                self::$cache_lang['Order'] = $this->l('Order', 'Helper');
            }

            if (!empty($id)) {
                $link = new Link;
                $href = $link->getAdminLink('AdminOrders') . '&vieworder&id_order=' . (int)$id;
                $tpl->assign(array(
                    'href' => $href,
                    'action' => self::$cache_lang['Order'],
                    'id' => $id,
                ));
                return $tpl->fetch();
            }
        }
        return '';
    }

    public function getListWuandmg()
    {
        $filters = array();
        $filters['where'] = array();
        $filters['orderby'] = array();

        if (Tools::isSubmit('submitFilter')) {
            if (Tools::getValue('tnwuandmgFilter_customer_id')) {
                $filters['where'][] = 's.customer_id LIKE "%' . pSQL(Tools::getValue('tnwuandmgFilter_customer_id')) . '%"';
            }
            if (Tools::getValue('tnwuandmgFilter_id_order')) {
                $filters['where'][] = 's.id_order LIKE "%' . pSQL(Tools::getValue('tnwuandmgFilter_id_order')) . '%"';
            }
            if (Tools::getValue('tnwuandmgFilter_data')) {
                $filters['where'][] = 's.data LIKE "%' . pSQL(Tools::getValue('tnwuandmgFilter_data')) . '%"';
            }
            if (Tools::getValue('tnwuandmgFilter_firstname')) {
                $filters['where'][] = 's.firstname LIKE "%' . pSQL(Tools::getValue('tnwuandmgFilter_firstname')) . '%"';
            }
            if (Tools::getValue('tnwuandmgFilter_lastname')) {
                $filters['where'][] = 's.lastname LIKE "%' . pSQL(Tools::getValue('tnwuandmgFilter_lastname')) . '%"';
            }
            if (Tools::getValue('tnwuandmgFilter_translation_key')) {
                $filters['where'][] = 's.translation_key LIKE "%' . pSQL(Tools::getValue('tnwuandmgFilter_translation_key')) . '%"';
            }
            if (Tools::getValue('tnwuandmgFilter_other')) {
                $filters['where'][] = 's.other LIKE "%' . pSQL(Tools::getValue('tnwuandmgFilter_other')) . '%"';
            }
            if (Tools::getValue('tnwuandmgFilter_id_tnwuandmg')) {
                $filters['inner'][] = ' INNER JOIN `' . _DB_PREFIX_ . 'tnwuandmg_orders' . '` AS o ON o.id_cart = s.id_cart';
                $filters['where'][] = 'o.id_tnwuandmg = "' . (int)Tools::getValue('tnwuandmgFilter_id_tnwuandmg') . '"';
            }
            if (Tools::getValue('tnwuandmgFilter_tranzactionptype')
                && isset($this->module->statuses[Tools::getValue('tnwuandmgFilter_tranzactionptype')])
                && !empty($this->module->statuses[Tools::getValue('tnwuandmgFilter_tranzactionptype')])
            ) {
                $filters['inner'][] = ' INNER JOIN `' . _DB_PREFIX_ . 'orders' . '` AS otb ON otb.id_order = s.id_order';
                $filters['where'][] = 'otb.payment = "' . pSQL($this->module->statuses[Tools::getValue('tnwuandmgFilter_tranzactionptype')]) . '"';
            }
        }

        $filter_sort = 'ORDER BY s.data DESC';
        if (Tools::getValue('tnwuandmgOrderby') && Tools::getValue('tnwuandmgOrderway')) {
            $key_validator = array('customer_id', 'id_order', 'firstname', 'lastname', 'translation_key', 'data', 'other');
            $sortby = Tools::getValue('tnwuandmgOrderby');
            $sorteway = Tools::getValue('tnwuandmgOrderway');
            if (in_array($sortby, $key_validator)) {
                $filter_sort = ' ORDER BY s.' . pSQL($sortby);
                $filter_sort .= ($sorteway == 'desc') ? ' DESC' : ' ASC';
            }
        }

        $filter = '';
        if (count($filters['where']) > 0) {
            $filter = ' WHERE ' . join(' AND ', $filters['where']);
        }

        $filter_inner = '';
        if (isset($filters['inner']) && count($filters['inner']) > 0) {
            $filter_inner = join(' ', $filters['inner']);
        }

        $WesterUnionData = Db::getInstance()->executeS('SELECT s.* FROM `' . _DB_PREFIX_ . 'tnwuandmg_send` s ' . $filter_inner . $filter . ' ' . $filter_sort);
        $data = array();
        foreach ($WesterUnionData as $key => $item) {
            $data[$key] = $item;
            if ($item['id_order']) {
                $customerInf = $this->getCustomerTransferInformation($item['id_cart']);
                $data[$key]['id_customer'] = $customerInf['id_tnwuandmg'] . ' - ' . $customerInf['fullname'];
                $data[$key]['payment'] = '';
                if (isset($customerInf['module'])) {
                    switch ($customerInf['module']) {
                        case 'tnwuandmg_mg':
                            $data[$key]['payment'] = $this->l('MoneyGram');
                            break;
                        case 'tnwuandmg_wu':
                            $data[$key]['payment'] = $this->l('WesternUnion');
                            break;
                    }
                }
            }
        }

        return $data;
    }

    public function getOrdersListWuandmg()
    {
        $filters = array();
        $filters['where'] = array();
        $filters['orderby'] = array();

        if (Tools::isSubmit('submitFilter') && Tools::getValue('submitFiltertnwuandmg_orders')) {
            if (Tools::getValue('tnwuandmg_ordersFilter_id_customer')) {
                $filters['where'][] = 'c.id_customer LIKE "%' . pSQL(Tools::getValue('tnwuandmg_ordersFilter_id_customer')) . '%"';
            }
            if (Tools::getValue('tnwuandmg_ordersFilter_id_order')) {
                $filters['where'][] = 'o.id_order LIKE "%' . pSQL(Tools::getValue('tnwuandmg_ordersFilter_id_order')) . '%"';
            }
            if (Tools::getValue('tnwuandmg_ordersFilter_date_add')) {
                $filters['where'][] = 'o.date_add LIKE "%' . pSQL(Tools::getValue('tnwuandmg_ordersFilter_date_add')) . '%"';
            }
            if (Tools::getValue('tnwuandmg_ordersFilter_firstname')) {
                $filters['where'][] = 'c.firstname LIKE "%' . pSQL(Tools::getValue('tnwuandmg_ordersFilter_firstname')) . '%"';
            }
            if (Tools::getValue('tnwuandmg_ordersFilter_lastname')) {
                $filters['where'][] = 'c.lastname LIKE "%' . pSQL(Tools::getValue('tnwuandmg_ordersFilter_lastname')) . '%"';
            }
            if (Tools::getValue('tnwuandmg_ordersFilter_id_order_state')) {
                $filters['where'][] = 'o.current_state = "' . (int)Tools::getValue('tnwuandmg_ordersFilter_id_order_state') . '"';
            }
            if (Tools::getValue('tnwuandmg_ordersFilter_id_tnwuandmg')) {
                $filters['where'][] = 'tno.id_tnwuandmg = "' . (int)Tools::getValue('tnwuandmg_ordersFilter_id_tnwuandmg') . '"';
            }
            if (Tools::getValue('tnwuandmg_ordersFilter_ptype') && isset($this->module->statuses[Tools::getValue('tnwuandmg_ordersFilter_ptype')]) &&
                !empty($this->module->statuses[Tools::getValue('tnwuandmg_ordersFilter_ptype')])
            ) {
                $filters['where'][] = 'o.payment = "' . pSQL($this->module->statuses[Tools::getValue('tnwuandmg_ordersFilter_ptype')]) . '"';
            }
        }

        $filter = '';
        if (count($filters['where']) > 0) {
            $filter = ' WHERE ' . join(' AND ', $filters['where']);
        }

        $filter_sort = ' ORDER BY o.date_add DESC ';
        if (Tools::getValue('tnwuandmg_ordersOrderby') && Tools::getValue('tnwuandmg_ordersOrderway')) {
            $filter_sort = '';
            $key_validator = array('date_add', 'id_customer', 'id_order', 'current_state', 'payment');
            $sortby = Tools::getValue('tnwuandmg_ordersOrderby');
            $sorteway = Tools::getValue('tnwuandmg_ordersOrderway');
            if (in_array($sortby, $key_validator)) {
                $filter_sort = ' ORDER BY o.' . pSQL($sortby);
                $filter_sort .= ($sorteway == 'desc') ? ' desc' : ' asc';
            }

            if (empty($filter_sort)) {
                $key_validator = array('firstname', 'lastname');
                if (in_array($sortby, $key_validator)) {
                    $filter_sort = ' ORDER BY c.' . pSQL($sortby);
                    $filter_sort .= ($sorteway == 'desc') ? ' desc' : ' asc';
                }
            }

            if (empty($filter_sort)) {
                $key_validator = array('id_tnwuandmg');
                if (in_array($sortby, $key_validator)) {
                    $filter_sort = ' ORDER BY tno.' . pSQL($sortby);
                    $filter_sort .= ($sorteway == 'desc') ? ' desc' : ' asc';
                }
            }
        }

        $westeruniondata = Db::getInstance()->executeS('SELECT c.firstname, c.lastname,
            tno.id_tnwuandmg, tno.id_cart,
            o.date_add, o.id_order, o.id_customer, o.id_customer, o.current_state, o.payment
            FROM `' . _DB_PREFIX_ . 'tnwuandmg_orders` AS tno
        INNER JOIN `' . _DB_PREFIX_ . 'orders` AS o ON o.id_cart = tno.id_cart
        INNER JOIN `' . _DB_PREFIX_ . 'customer` AS c ON c.id_customer = o.id_customer
         ' . $filter . $filter_sort);

        $orderSt = OrderState::getOrderStates((int)$this->context->language->id);
        $orderStSort = array();
        foreach ($orderSt as $item) {
            $orderStSort[$item['id_order_state']]['name'] = $item['name'];
            $orderStSort[$item['id_order_state']]['color'] = $item['color'];
        }

        $data = array();
        foreach ($westeruniondata as $key => $item) {
            $data[$key] = $item;
            if ($item['id_cart']) {
                $customerInf = $this->getCustomerTransferInformation($item['id_cart']);
                $data[$key]['id_tnwuandmg'] = $customerInf['id_tnwuandmg'] . ' - ' . $customerInf['fullname'];
                $data[$key]['current_state'] = $orderStSort[$item['current_state']]['name'];
                $data[$key]['color'] = $orderStSort[$item['current_state']]['color'];
            }
        }

        return $data;
    }

    public function getCustomerTransferInformation($id_cart)
    {
        return Db::getInstance()->getRow('SELECT u.* FROM `' . _DB_PREFIX_ . 'tnwuandmg_orders` AS ord
                                            INNER JOIN `' . _DB_PREFIX_ . 'tnwuandmg` AS u
                                            ON u.id_tnwuandmg = ord.id_tnwuandmg
                                                WHERE ord.id_cart = ' . (int)$id_cart);
    }

    public function getCustomerTransferInformationList()
    {
        $result = array();
        $AccountList = Db::getInstance()->executeS('SELECT `id_tnwuandmg`, `fullname` FROM `' . _DB_PREFIX_ . 'tnwuandmg`');
        if ($AccountList && count($AccountList) > 0) {
            foreach ($AccountList as $item) {
                $result[$item['id_tnwuandmg']] = $item['id_tnwuandmg'] . ' - ' . $item['fullname'];
            }
        }

        return $result;
    }

    public function getTransfersStatesList()
    {
        $result = array();
        $States = Db::getInstance()->executeS('SELECT o.current_state FROM `' . _DB_PREFIX_ . 'tnwuandmg_orders` AS ord
                                            INNER JOIN `' . _DB_PREFIX_ . 'orders` AS o
                                            ON o.id_cart = ord.id_cart GROUP BY o.current_state');
        if ($States && count($States) > 0) {
            foreach ($States as $state) {
                if (isset($state['current_state']) && is_numeric($state['current_state']) && $state['current_state'] > 0) {
                    $result[] = $state['current_state'];
                }
            }
        }
        return $result;
    }

    public function getOrderIdTransferInformationId($id)
    {
        return Db::getInstance()->getValue('SELECT `id_order` FROM `' . _DB_PREFIX_ . 'tnwuandmg_send` WHERE id = ' . (int)$id);
    }
}
