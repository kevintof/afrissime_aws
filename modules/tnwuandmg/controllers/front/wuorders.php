<?php
/**
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class TnWUAndMGWuOrdersModuleFrontController extends ModuleFrontController
{
    public $notification = array();
    public $errors = array();
    public $messages = false;

    public function __construct()
    {
        parent::__construct();

        $this->display_column_left = false;
        $this->display_column_right = false;

        $this->context = Context::getContext();
    }

    public function initContent()
    {
        parent::initContent();

        $this->messages = (isset($this->context->cookie->wuandmg) && $this->context->cookie->wuandmg == 'success') ? 1 : 0;
        if ($this->messages) {
            unset($this->context->cookie->wuandmg);
        }

        if (Tools::isSubmit('submitTranslationKey') && Tools::getValue('id_order') && Tools::getValue('id_cart')) {
            $OrderId = (int)Tools::getValue('id_order');
            $OrderCart = (int)Tools::getValue('id_cart');
            $firstname = Tools::getValue('firstname');
            $lastname = Tools::getValue('lastname');
            $other = Tools::getValue('other');
            $translationkey = Tools::getValue('translationkey');
            $uniqReference = Tools::getValue('uniqReference');
            if (is_numeric($OrderId) && TnWUAndMG::getCustomerOrdersModuleValidation($this->context->customer->id, $OrderId)) {
                if (!empty($firstname) && !empty($lastname)) {
                    if (!empty($translationkey)) {
                        if (TnWUAndMG::addCustomerTranslationKey((int)$OrderId, (int)$OrderCart, (int)$this->context->customer->id, $firstname, $lastname, $other, $translationkey, $uniqReference)) {
                            $link = new Link();
                            $this->context->cookie->wuandmg = 'success';
                            Tools::redirect($link->getModuleLink('tnwuandmg', 'wuorders'));
                        } else {
                            $this->errors[] = $this->module->l('Error transfer key!', 'tnwuandmg');
                        }
                    } else {
                        $this->errors[] = $this->module->l('Transaction key is a required!', 'tnwuandmg');
                    }
                } else {
                    $this->errors[] = $this->module->l('Firstname & Lastname is a required!', 'tnwuandmg');
                    if (empty($translationkey)) {
                        $this->errors[] = $this->module->l('Transaction key is a required!', 'tnwuandmg');
                    }
                }
            } else {
                $this->errors[] = $this->module->l('Errors OrderId', 'tnwuandmg');
            }
        }

        if (Tools::isSubmit('unregistered')) {
            $OrderInf = TnWUAndMG::getUnCustomerOrdersModuleValidation(Tools::getValue('order_key'));

            if ($OrderInf && isset($OrderInf['id_order']) && isset($OrderInf['id_cart']) && !empty($OrderInf['id_order'])
                && !empty($OrderInf['id_cart'])
            ) {
                $OrderId = (int)$OrderInf['id_order'];
                $OrderCart = (int)$OrderInf['id_cart'];
                $CustomerId = (int)$OrderInf['id_customer'];
                $firstname = Tools::getValue('firstname');
                $lastname = Tools::getValue('lastname');
                $other = Tools::getValue('other');
                $translationkey = Tools::getValue('translationkey');

                $uniqReference = '';
                if (!empty($OrderId)) {
                    $uniqReference = Order::getUniqReferenceOf($OrderId);
                }

                if (is_numeric($OrderId) && $OrderId > 0
                    && is_numeric($OrderCart) && $OrderCart > 0
                ) {
                    if (!empty($firstname) && !empty($lastname)) {
                        if (!empty($translationkey)) {
                            if (TnWUAndMG::addCustomerTranslationKey((int)$OrderId, (int)$OrderCart, (int)$CustomerId, $firstname, $lastname, $other, $translationkey, $uniqReference)) {
                                $link = new Link();
                                $this->context->cookie->wuandmg = 'success';
                                Tools::redirect($link->getModuleLink('tnwuandmg', 'wuorders'));
                            } else {
                                $this->errors[] = $this->module->l('Error transfer key!', 'tnwuandmg');
                            }
                        } else {
                            $this->errors[] = $this->module->l('Transaction key is a required!', 'tnwuandmg');
                        }
                    } else {
                        $this->errors[] = $this->module->l('Firstname & Lastname is a required!', 'tnwuandmg');
                        if (empty($translationkey)) {
                            $this->errors[] = $this->module->l('Transaction key is a required!', 'tnwuandmg');
                        }
                    }
                } else {
                    $this->errors[] = $this->module->l('Errors OrderId', 'tnwuandmg');
                }
            } else {
                if (!empty(Tools::getValue('order_key'))) {
                    $this->errors[] = $this->module->l('It isn\'t correctly Order Id!', 'tnwuandmg');
                } else {
                    $this->errors[] = $this->module->l('Empty a required fields!', 'tnwuandmg');
                }
            }
        }

        $action = Tools::getValue('action');

        if (!Tools::isSubmit('myajax')) {
            $this->assign();
        } elseif (!empty($action) && method_exists($this, 'ajaxProcess' . Tools::toCamelCase($action))) {
            $this->{'ajaxProcess' . Tools::toCamelCase($action)}();
        } else {
            die(Tools::jsonEncode(array('error' => 'method doesn\'t exist')));
        }
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(array(
            _MODULE_DIR_ . $this->module->name . '/views/css/front.css'
        ));

        $this->addJS(array(
            _MODULE_DIR_ . $this->module->name . '/views/js/front.js'
        ));
        $this->addJqueryPlugin(array('scrollTo', 'footable', 'footable-sort'));
    }

    public function assign()
    {
        $ortderSt = OrderState::getOrderStates((int)$this->context->language->id);
        $ortderStSort = array();
        foreach ($ortderSt as $item) {
            $ortderStSort[$item['id_order_state']]['name'] = $item['name'];
            $ortderStSort[$item['id_order_state']]['color'] = $item['color'];
        }

        if ($orders = TnWUAndMG::getCustomerOrdersModule($this->context->customer->id, 'tnwuandmg')) {
            foreach ($orders as $key => &$order) {
                if ($order['module'] != $this->module->name || $order['payment'] != 'WesternUnion') {
                    unset($orders[$key]);
                } else {
                    $myOrder = new Order((int)$order['id_order']);
                    if (Validate::isLoadedObject($myOrder)) {
                        $order['virtual'] = $myOrder->isVirtual(false);
                        $orders[$key]['state_module'] = (isset($order['id_order_state']) && !empty($order['id_order_state'])) ? $order['id_order_state'] : 0;
                        $orders[$key]['id_order_state'] = (isset($order['current_state']) && !empty($order['current_state'])) ? $order['current_state'] : 0;
                        $orders[$key]['order_state'] = (isset($ortderStSort[$order['current_state']]['name']) && !empty($ortderStSort[$order['current_state']]['name'])) ? $ortderStSort[$order['current_state']]['name'] : '';
                        $orders[$key]['order_state_color'] = (isset($ortderStSort[$order['current_state']]['color']) && !empty($ortderStSort[$order['current_state']]['color'])) ? $ortderStSort[$order['current_state']]['color'] : '';
                        $orders[$key]['total_paid'] = Tools::displayPrice(
                            $myOrder->getOrdersTotalPaid(),
                            new Currency($myOrder->id_currency),
                            false
                        );
                        $orders[$key]['details']['details_url'] = $this->context->link->getPageLink('order-detail', true, null, 'id_order='.(int)$order['id_order']);
                        $orders[$key]['details']['reorder_url'] = HistoryController::getUrlToReorder((int) $order['id_order'], $this->context);
                    }
                }
            }
        }

        $ex = array();
        if (Tools::isSubmit('key') && Tools::getValue('key')) {
            $key = Tools::getValue('key');
            if (!empty($key)) {
                $ex = TnWUAndMG::getKeyValidatorValues($key);
            }
        }

        $this->context->smarty->assign(array(
            'orders' => $orders,
            'invoiceAllowed' => (int)Configuration::get('PS_INVOICE'),
            'reorderingAllowed' => !(bool)Configuration::get('PS_DISALLOW_HISTORY_REORDERING'),
            'errors' => $this->errors,
            'alert_success' => $this->messages,
            'firstName' => ($this->context->customer->logged ? $this->context->customer->firstname : false),
            'lastName' => ($this->context->customer->logged ? $this->context->customer->lastname : false),
            'customer_logged' => ($this->context->customer->logged ? 1 : 0),
            'un_firstName' => (!$this->context->customer->logged && isset($ex['firstname']) && !empty($ex['firstname']) ? $ex['firstname'] : false),
            'un_lastName' => (!$this->context->customer->logged && isset($ex['lastname']) && !empty($ex['lastname']) ? $ex['lastname'] : false),
            'un_key' => (isset($ex['reference']) && !empty($ex['reference']) ? $ex['reference'] : false),
            'post_order_id' => Tools::getValue('order_key'),
            'post_firstname' => Tools::getValue('firstname'),
            'post_lastname' => Tools::getValue('lastname'),
            'post_translationkey' => Tools::getValue('translationkey'),
            'post_other' => Tools::getValue('other'),
        ));

        $this->setTemplate('module:tnwuandmg/views/templates/hook/orders_wu.tpl');
    }
}
