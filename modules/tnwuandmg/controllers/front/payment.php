<?php
/**
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class TnWUAndMGPaymentModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $display_column_left = false;

    public function initContent()
    {
        parent::initContent();
        $cart = $this->context->cart;

        $typePayment = '';
        if (Tools::isSubmit('wu') && Tools::getValue('wu') == 1
            && Tools::isSubmit('transferUid') && Tools::getValue('transferUid')
        ) {
            $typePayment = 'wu';
            $this->context->cookie->owner_selected_wu = (int)Tools::getValue('transferUid');
        }
        if (Tools::isSubmit('mg') && Tools::getValue('mg') == 1
            && Tools::isSubmit('transferUid') && Tools::getValue('transferUid')
        ) {
            $typePayment = 'mg';
            $this->context->cookie->owner_selected_mg = (int)Tools::getValue('transferUid');
        }

        if (!$this->module->checkCurrency((int)$cart->id_currency, (int)Tools::getValue('transferUid'))) {
            die(0);
        }

        if (isset($typePayment) && in_array($typePayment, array('mg', 'wu'))) {
            $this->context->smarty->assign(
                array(
                    'nbProducts' => $cart->nbProducts(),
                    'cust_currency' => $this->context->cookie->id_currency,
                    'currencies' => Currency::getCurrencies(),
                    'total' => Tools::displayPrice(number_format($cart->getOrderTotal(true, 3), 2, '.', '')),
                    'isoCode' => Language::getIsoById((int)$this->context->cookie->id_lang),
                    'typePayment' => $typePayment,
                    'tnwuandmgTransferList' => ((Tools::isSubmit('transferUid') && Tools::getValue('transferUid')) ? $this->module->getAdminTransfers((int)Tools::getValue('transferUid')) : 0),
                )
            );

            die($this->context->smarty->fetch('module:tnwuandmg/views/templates/hook/payment_transfer_return.tpl'));
        }
    }
}
