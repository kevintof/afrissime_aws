<?php
/**
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class TnWUAndMGValidationModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        $context = Context::getContext();
        $cart = $context->cart;

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        $currency = $this->context->currency;
        $total = (float)number_format($cart->getOrderTotal(true, 3), 2, '.', '');

        $validateSendStatus = array('status' => 1, 'inf' => 'default');
        $hash = md5(time() . $cart->id . $total);
        $link = new Link();


        if (Tools::isSubmit('mod') && Tools::getValue('mod') == 'mg' && isset($this->context->cookie->owner_selected_mg)
            && Validate::isInt($this->context->cookie->owner_selected_mg) && !empty($this->context->cookie->owner_selected_mg)
        ) {
            $transferDataUid = (int)$this->context->cookie->owner_selected_mg;
            $validateSendStatus = array(
                'status' => (int)Configuration::get('PS_OS_TNMONEYGRAM'),
                'inf' => 'MoneyGram'
            );
            $tnwuandmg_link_hash = $link->getModuleLink('tnwuandmg', 'mgorders', array('key' => $hash));
        } elseif (Tools::isSubmit('mod') && Tools::getValue('mod') == 'wu' && isset($this->context->cookie->owner_selected_wu)
            && Validate::isInt($this->context->cookie->owner_selected_wu) && !empty($this->context->cookie->owner_selected_wu)
        ) {
            $transferDataUid = (int)$this->context->cookie->owner_selected_wu;
            $validateSendStatus = array(
                'status' => (int)Configuration::get('PS_OS_TNWESTERNUNION'),
                'inf' => 'WesternUnion'
            );
            $tnwuandmg_link_hash = $link->getModuleLink('tnwuandmg', 'wuorders', array('key' => $hash));
        }

        if (!isset($transferDataUid) || empty($transferDataUid) || !Validate::isInt($transferDataUid)) {
            Tools::redirect('index.php?controller=order&step=1');
        }
        $transferData = $this->module->getAdminTransfers($transferDataUid);

        $mailVars = array(
            '{tnwuandmg_owner}' => $transferData[0]['fullname'],
            '{tnwuandmg_id_customer}' => ($transferData[0]['id_customer_active']) ? $transferData[0]['id_customer'] : '___________',
            '{tnwuandmg_address}' => ($transferData[0]['address_active']) ? Tools::nl2br($transferData[0]['address']) : '___________',
            '{tnwuandmg_details}' => ($transferData[0]['details_active']) ? Tools::nl2br($transferData[0]['details']) : '___________',
            '{tnwuandmg_vat}' => ($transferData[0]['vat_number_active']) ? Tools::nl2br($transferData[0]['vat_number']) : '___________',
            '{tnwuandmg_link_hash}' => $tnwuandmg_link_hash
        );

        $this->module->validateOrder($cart->id, (int)$validateSendStatus['status'], $total, $validateSendStatus['inf'], null, $mailVars, $currency->id, false, $customer->secure_key);

        $insert_data = array(
            'id_tnwuandmg' => (int)$transferDataUid,
            'id_cart' => (int)$cart->id,
            'hash' => pSQL($hash)
        );
        Db::getInstance()->insert('tnwuandmg_orders', $insert_data, false, true);

        Tools::redirect('index.php?controller=order-confirmation&id_cart=' . (int)$cart->id . '&id_module=' . (int)$this->module->id . '&id_order=' . $this->module->currentOrder . '&key=' . $customer->secure_key . '&hash_key=' . $hash);
    }
}
