/**
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 *  @author    TN
 *  @copyright 2016 TN
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$(function(){
	$('.tnwuandmg_key').click(function(){
		if($(this).siblings('.tnwuandmg_enter_key_form').length > 0)
		{
			if($(this).siblings('.tnwuandmg_enter_key_form').is(":visible"))
			{
				$(this).siblings('.tnwuandmg_enter_key_form').slideUp();
			}else{
				$(this).siblings('.tnwuandmg_enter_key_form').slideDown();
			}
		}
	})
});