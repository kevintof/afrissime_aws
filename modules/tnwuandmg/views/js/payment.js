/**
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 *  @author    TN
 *  @copyright 2016 TN
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$(function () {
    if ($('.wu_payment').length > 0 && $('.wu_path_transfer').length > 0 && $('.wu_path_transfer').val().length > 0) {
        var wu_url = $('.wu_path_transfer').val();
        transferShowData($('.wu_payment'), wu_url, 'wu');
    }
    if ($('.mg_payment').length > 0 && $('.mg_path_transfer').length > 0 && $('.mg_path_transfer').val().length > 0) {
        var mg_url = $('.mg_path_transfer').val();
        transferShowData($('.mg_payment'), mg_url, 'mg');
    }
});

function transferShowData(sel, path_pay, container_tr, fload) {
    var transferUid = parseInt($(sel).val());
    if (transferUid > 0 && path_pay.length > 0) {
        $.ajax({
            type: "POST",
            headers: {"cache-control": "no-cache"},
            url: path_pay,
            data: {
                ajax: 'true',
                getTransferData: '1',
                transferUid: transferUid
            },
            success: function (msg) {
                $('.transferData.' + container_tr).fadeOut('fast').html('').html(msg).fadeIn();
            }
        });
    }
    return false;
}
