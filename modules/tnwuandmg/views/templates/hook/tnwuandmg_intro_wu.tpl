{*
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

<section>
    {if (isset($owners['tnwuandmg_wu']) && count($owners['tnwuandmg_wu']) == 1)}
        <input name="user_select_transfer" class="wu_payment" type="hidden"
               value="{$owners['tnwuandmg_wu'][0]['id_tnwuandmg']|escape:'htmlall':'UTF-8'}">
        <input type="hidden"
               value="{$link->getModuleLink('tnwuandmg', 'payment', ['wu'=>1], true)|escape:'htmlall':'UTF-8'}"
               class="wu_path_transfer">
    {elseif (isset($owners['tnwuandmg_wu']) && count($owners['tnwuandmg_wu']) > 1)}
        <div>
            <table class="table table-bordered">
                <tr>
                    <td width="30%">
                        <label for="user_select_transfer">
                            {l s='Select Beneficiary:' mod='tnwuandmg'}
                        </label>
                    </td>
                    <td>
                        <input type="hidden"
                               value="{$link->getModuleLink('tnwuandmg', 'payment', ['wu'=>1], true)|escape:'htmlall':'UTF-8'}"
                               class="wu_path_transfer">
                        <select onchange="transferShowData($(this), '{$link->getModuleLink('tnwuandmg', 'payment', ['wu'=>1], true)|escape:'htmlall':'UTF-8'}', 'wu')"
                                name="user_select_transfer"
                                id="user_select_transfer" class="wu_payment">
                            {foreach $owners['tnwuandmg_wu'] as $key => $list_item}
                                <option value="{$list_item['id_tnwuandmg']|escape:'htmlall':'UTF-8'}">
                                    {$list_item['fullname']|escape:'htmlall':'UTF-8'}
                                </option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    {/if}
    <br/>
    <div class="transferData wu">
        {if (isset($owners['tnwuandmg_wu']) && count($owners['tnwuandmg_wu']) == 1)}
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th colspan="2">
                        {l s='Necessary data for this Western Union transfer:' mod='tnwuandmg'}
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="left_item">
                        {l s='Total amout:' mod='tnwuandmg'}
                    </td>
                    <td class="item">
                        <strong>
                            <span class="price">{$total|escape:'htmlall':'UTF-8'}</span>
                            <span id="amount" class="price"></span>
                            {l s='(tax incl.)' mod='tnwuandmg'}
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td class="left_item">
                        {l s='Beneficiary name:' mod='tnwuandmg'}
                    </td>
                    <td>
                        <strong>{if $owners['tnwuandmg_wu'][0]['fullname']}
                                <strong>{$owners['tnwuandmg_wu'][0]['fullname']|escape:'htmlall':'UTF-8'}</strong>{else}___________{/if}
                        </strong>
                    </td>
                </tr>
                {if isset($owners['tnwuandmg_wu'][0]['id_customer_active']) && $owners['tnwuandmg_wu'][0]['id_customer_active']}
                    <tr>
                        <td class="left_item">
                            {l s='Beneficiary Id:' mod='tnwuandmg'}
                        </td>
                        <td>
                            <strong>{if $owners['tnwuandmg_wu'][0]['id_customer']}{$owners['tnwuandmg_wu'][0]['id_customer']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                        </td>
                    </tr>
                {/if}
                {if isset($owners['tnwuandmg_wu'][0]['vat_number_active']) && $owners['tnwuandmg_wu'][0]['vat_number_active']}
                    <tr>
                        <td class="left_item">
                            {l s='Vat number:' mod='tnwuandmg'}
                        </td>
                        <td>
                            <strong>{if $owners['tnwuandmg_wu'][0]['vat_number']}{$owners['tnwuandmg_wu'][0]['vat_number']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                        </td>
                    </tr>
                {/if}
                {if isset($owners['tnwuandmg_wu'][0]['address_active']) && $owners['tnwuandmg_wu'][0]['address_active']}
                    <tr>
                        <td class="left_item">
                            {l s='Address:' mod='tnwuandmg'}
                        </td>
                        <td>
                            <strong>{if $owners['tnwuandmg_wu'][0]['address']}{$owners['tnwuandmg_wu'][0]['address']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                        </td>
                    </tr>
                {/if}
                {if isset($owners['tnwuandmg_wu'][0]['details_active']) && $owners['tnwuandmg_wu'][0]['details_active']}
                    <tr>
                        <td class="left_item">
                            {l s='Details:' mod='tnwuandmg'}
                        </td>
                        <td>
                            <strong>{if $owners['tnwuandmg_wu'][0]['details']}{$owners['tnwuandmg_wu'][0]['details']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                        </td>
                    </tr>
                {/if}
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="2">
                        {l s='Website: ' mod='tnwuandmg' }<a href="http://www.westernunion.com"
                                                                  target="_blank">{l s='www.westernunion.com' mod='tnwuandmg'}</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        {l s='You can leave a message with the transaction key as soon as you make the transfer in your order history. Or you can contact our:' mod='tnwuandmg'}
                        <strong><a href="{$link->getPageLink('contact', true)|escape:'htmlall':'UTF-8'}">{l s='customer support team' mod='tnwuandmg'}</a></strong>.
                    </td>
                </tr>
                <tfoot>
            </table>
        {/if}
    </div>
</section>
