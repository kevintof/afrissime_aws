{*
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

{extends file='customer/page.tpl'}

{block name='page_title'}
    {l s='Order history' mod='tnwuandmg'}
{/block}

{block name='page_content'}
    <h6>
        {l s='Here are the orders you\'ve placed since your account was created.' mod='tnwuandmg'}
    </h6>
    <div class="block-center" id="block-history tnwuandmg-table">
        {if $orders && count($orders)}
            <table id="order-list" class="table table-striped table-bordered table-labeled hidden-sm-down wu-table">
                <thead class="thead-default">
                <tr>
                    <th class="first_item" data-sort-ignore="true">
                        {l s='Order reference' mod='tnwuandmg'}
                    </th>
                    <th class="item">
                        {l s='Date' mod='tnwuandmg'}
                    </th>
                    <th data-hide="phone" class="item">
                        {l s='Total price' mod='tnwuandmg'}
                    </th>
                    <th class="item">
                        {l s='Status' mod='tnwuandmg'}
                    </th>
                    <th data-sort-ignore="true" class="last_item">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$orders item=order name=myLoop}
                    <tr class="{if !empty($un_key) && $un_key==Order::getUniqReferenceOf($order.id_order)}open {/if}{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
                        <td class="history_link bold">
                            {$order.reference|escape:'htmlall':'UTF-8'}
                        </td>
                        <td data-value="{$order.date_add|escape:'htmlall':'UTF-8'|regex_replace:"/[\-\:\ ]/":""}"
                            class="history_date bold">
                            {dateFormat date=$order.date_add full=0}
                        </td>
                        <td class="history_price" data-value="{$order.total_paid|escape:'htmlall':'UTF-8'}">
                    <span class="price">
                        {$order.total_paid|escape:'htmlall':'UTF-8'}
                    </span>
                        </td>
                        <td{if isset($order.order_state)} data-value="{$order.id_order_state|escape:'htmlall':'UTF-8'}"{/if}
                                class="history_state">
                            {if isset($order.order_state)}
                                <span class="label{if isset($order.order_state_color) && Tools::getBrightness($order.order_state_color) > 128} dark{/if}"{if isset($order.order_state_color) && $order.order_state_color} style="background-color:{$order.order_state_color|escape:'html':'UTF-8'}; border-color:{$order.order_state_color|escape:'html':'UTF-8'};"{/if}>
                                {$order.order_state|escape:'html':'UTF-8'}
                            </span>
                            {/if}
                        </td>
                        <td class="history_detail">
                            <a class="btn_block" href="{$order.details.details_url|escape:'htmlall':'UTF-8'}"
                               data-link-action="view-order-details">
                                {l s='Details' mod='tnwuandmg'}
                            </a>
                            {if $order.details.reorder_url}
                                <a class="btn_block" href="{$order.details.reorder_url|escape:'htmlall':'UTF-8'}">
                                    {l s='Reorder' mod='tnwuandmg'}
                                </a>
                            {/if}
                            {if $order.state_module == $order.id_order_state}
                                <a class="btn_block link-button tnwuandmg_key">
                                    <i class="icon-key"></i>
                                    {l s='Enter a transaction key' mod='tnwuandmg'}
                                </a>
                                <div class="tnwuandmg_enter_key_form">
                                    <form method="post" action="">
                                        <div class="required form-group">
                                            <label for="firstname">{l s='First name' mod='tnwuandmg'}
                                                <sup>*</sup></label>
                                            <input class="is_required validate form-control" data-validate="isName"
                                                   type="text" name="firstname" id="firstname"
                                                   value="{if isset($firstName) && $firstName}{$firstName|escape:'htmlall':'UTF-8'}{/if}">
                                        </div>
                                        <div class="required form-group">
                                            <label for="lastname">{l s='Last name' mod='tnwuandmg'}
                                                <sup>*</sup></label>
                                            <input class="is_required validate form-control" data-validate="isName"
                                                   type="text" id="lastname" name="lastname"
                                                   value="{if isset($lastName) && $lastName}{$lastName|escape:'htmlall':'UTF-8'}{/if}">
                                        </div>
                                        <div class="required form-group">
                                            <label for="translationkey">{l s='Transaction key' mod='tnwuandmg'}
                                                <sup>*</sup></label>
                                            <input class="form-control" type="text" id="translationkey"
                                                   name="translationkey" placeholder="0123456789" value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="other">{l s='Additional information' mod='tnwuandmg'}</label>
                                        <textarea class="form-control" id="other" name="other" cols="26"
                                                  rows="3"></textarea>
                                        </div>
                                        <p class="submit2">
                                            <input type="hidden" value="{$order.id_order|intval}" name="id_order">
                                            <input type="hidden" value="{$order.id_cart|intval}" name="id_cart">
                                            <input type="hidden"
                                                   value="{Order::getUniqReferenceOf($order.id_order|intval)}"
                                                   name="uniqReference">
                                            <button type="submit" name="submitTranslationKey" id="submitTranslationKey"
                                                    class="btn btn-default button button-medium">
                                        <span> {l s='Send transaction key' mod='tnwuandmg'} <i
                                                    class="icon-chevron-right right"></i> </span>
                                            </button>
                                        </p>
                                    </form>
                                </div>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
            <div id="block-order-detail" class="unvisible">&nbsp;</div>
        {else}
            {if !$customer_logged}
                <div class="tnwuandmg_enter_key_form">
                    <form method="post" action="">
                        <div class="required form-group">
                            <label for="order_key">{l s='Order ID' mod='tnwuandmg'}<sup>*</sup></label>
                            {if $un_key && !empty($un_key)}
                                <input {if $un_key && !empty($un_key)}disabled{/if}
                                       class="is_required validate form-control" type="text" name="order_key_cash"
                                       id="order_key_cash"
                                       value="{if $un_key && !empty($un_key)}{$un_key|escape:'htmlall':'UTF-8'}{/if}">
                            {else}
                                <input class="is_required validate form-control" type="text" name="order_key"
                                       id="order_key"
                                       value="{if $post_order_id && !empty($post_order_id)}{$post_order_id|escape:'htmlall':'UTF-8'}{/if}">
                            {/if}
                        </div>
                        <div class="required form-group">
                            <label for="firstname">{l s='First name' mod='tnwuandmg'}<sup>*</sup></label>
                            <input class="is_required validate form-control" data-validate="isName" type="text"
                                   name="firstname" id="firstname"
                                   value="{if $post_firstname && !empty($post_firstname)}{$post_firstname|escape:'htmlall':'UTF-8'}{elseif $un_firstName && !empty($un_firstName)}{$un_firstName|escape:'htmlall':'UTF-8'}{/if}">
                        </div>
                        <div class="required form-group">
                            <label for="lastname">{l s='Last name' mod='tnwuandmg'} <sup>*</sup></label>
                            <input class="is_required validate form-control" data-validate="isName" type="text"
                                   id="lastname" name="lastname"
                                   value="{if $post_lastname && !empty($post_lastname)}{$post_lastname|escape:'htmlall':'UTF-8'}{elseif $un_lastName && !empty($un_lastName)}{$un_lastName|escape:'htmlall':'UTF-8'}{/if}">
                        </div>
                        <div class="required form-group">
                            <label for="translationkey">{l s='Transaction key' mod='tnwuandmg'} <sup>*</sup></label>
                            <input class="form-control" type="text" id="translationkey" name="translationkey"
                                   placeholder="0123456789"
                                   value="{if $post_other && !empty($post_other)}{$post_other|escape:'htmlall':'UTF-8'}{/if}">
                        </div>
                        <div class="form-group">
                            <label for="other">{l s='Additional information' mod='tnwuandmg'}</label>
                            <textarea class="form-control" id="other" name="other" cols="26"
                                      rows="3">{if $post_translationkey && !empty($post_translationkey)}{$post_translationkey|escape:'htmlall':'UTF-8'}{/if}</textarea>
                        </div>
                        <p class="submit2">
                            <input type="hidden" value="1" name="unregistered">
                            {if $un_key && !empty($un_key)}
                                <input class="is_required validate form-control" type="hidden" name="order_key"
                                       id="order_key"
                                       value="{$un_key|escape:'htmlall':'UTF-8'}">
                            {/if}
                            <button type="submit" name="submitTranslationKey" id="submitTranslationKey"
                                    class="btn btn-default button button-medium">
                                <span> {l s='Send transaction key' mod='tnwuandmg'} <i
                                            class="icon-chevron-right right"></i> </span>
                            </button>
                        </p>
                    </form>
                </div>
            {else}
                <p class="alert alert-warning">{l s='You have not placed any orders.' mod='tnwuandmg'}</p>
            {/if}
        {/if}
    </div>
{/block}
