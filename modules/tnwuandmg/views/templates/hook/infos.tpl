{*
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

<div class="alert alert-info">
    <img src="../modules/tnwuandmg/logo.png" style="float:left; margin-right:15px;" height="49">
    <p><strong>{l s='This module allows you to accept payments by check.' mod='tnwuandmg'}</strong></p>
    <p>{l s='If the client chooses this payment method, the order status will change to \'Waiting for payment\'.' mod='tnwuandmg'}</p>
    <p>{l s='You will need to manually confirm the order as soon as you receive a check.' mod='tnwuandmg'}</p>
</div>