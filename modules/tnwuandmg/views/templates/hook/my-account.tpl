{*
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

<a class="lnk_tnwuandmg_wu col-lg-4 col-md-6 col-sm-6 col-xs-12" id="returns-link" href="{$link->getModuleLink('tnwuandmg', 'wuorders', array(), true)|escape:'quotes':'UTF-8'}" title="{l s='Westernunion link' mod='tnwuandmg'}">
  <span class="link-item">
		<i class="icon-fllist_wu"></i>
      {l s='Western Union orders' mod='tnwuandmg'}
  </span>
</a>
<a class="lnk_tnwuandmg_mg col-lg-4 col-md-6 col-sm-6 col-xs-12" id="returns-link" href="{$link->getModuleLink('tnwuandmg', 'mgorders', array(), true)|escape:'quotes':'UTF-8'}" title="{l s='MoneyGram link' mod='tnwuandmg'}">
  <span class="link-item">
		<i class="icon-fllist_mg"></i>
      {l s='MoneyGram orders' mod='tnwuandmg'}
  </span>
</a>