{*
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

{if isset($tnwuandmgTransferList) && !empty($tnwuandmgTransferList) && is_array($tnwuandmgTransferList)}
    <table class="table table-bordered">
        <thead>
        <tr>
            <th colspan="2">
                {if (isset($typePayment) && $typePayment == 'wu')}
                    {l s='Necessary data for this Western Union transfer:' mod='tnwuandmg'}
                {elseif (isset($typePayment) && $typePayment == 'mg')}
                    {l s='Necessary data for this MoneyGram transfer:' mod='tnwuandmg'}
                {/if}
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="left_item">
                {l s='Total amout:' mod='tnwuandmg'}
            </td>
            <td class="item">
                <strong>
                    <span class="price">{$total|escape:'htmlall':'UTF-8'}</span>
                    <span id="amount" class="price"></span>
                    {l s='(tax incl.)' mod='tnwuandmg'}
                </strong>
            </td>
        </tr>
        <tr>
            <td class="left_item">
                {l s='Beneficiary name:' mod='tnwuandmg'}
            </td>
            <td>
                <strong>{if $tnwuandmgTransferList[0]['fullname']}<strong>{$tnwuandmgTransferList[0]['fullname']|escape:'htmlall':'UTF-8'}</strong>{else}___________{/if}</strong>
            </td>
        </tr>
        {if isset($tnwuandmgTransferList[0]['id_customer_active']) && $tnwuandmgTransferList[0]['id_customer_active']}
            <tr>
                <td class="left_item">
                    {l s='Beneficiary Id:' mod='tnwuandmg'}
                </td>
                <td>
                    <strong>{if $tnwuandmgTransferList[0]['id_customer']}{$tnwuandmgTransferList[0]['id_customer']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                </td>
            </tr>
        {/if}
        {if isset($tnwuandmgTransferList[0]['vat_number_active']) && $tnwuandmgTransferList[0]['vat_number_active']}
            <tr>
                <td class="left_item">
                    {l s='Vat number:' mod='tnwuandmg'}
                </td>
                <td>
                    <strong>{if $tnwuandmgTransferList[0]['vat_number']}{$tnwuandmgTransferList[0]['vat_number']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                </td>
            </tr>
        {/if}
        {if isset($tnwuandmgTransferList[0]['address_active']) && $tnwuandmgTransferList[0]['address_active']}
            <tr>
                <td class="left_item">
                    {l s='Address:' mod='tnwuandmg'}
                </td>
                <td>
                    <strong>{if $tnwuandmgTransferList[0]['address']}{$tnwuandmgTransferList[0]['address']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                </td>
            </tr>
        {/if}
        {if isset($tnwuandmgTransferList[0]['details_active']) && $tnwuandmgTransferList[0]['details_active']}
            <tr>
                <td class="left_item">
                    {l s='Details:' mod='tnwuandmg'}
                </td>
                <td>
                    <strong>{if $tnwuandmgTransferList[0]['details']}{$tnwuandmgTransferList[0]['details']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                </td>
            </tr>
        {/if}
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2">
                {if (isset($typePayment) && $typePayment == 'wu')}
                    {l s='Website: ' mod='tnwuandmg' }<a href="http://www.westernunion.com" target="_blank">{l s='www.westernunion.com' mod='tnwuandmg'}</a>
                {elseif (isset($typePayment) && $typePayment == 'mg')}
                    {l s='Website: ' mod='tnwuandmg' }<a href="http://global.moneygram.com" target="_blank">{l s='global.moneygram.com' mod='tnwuandmg'}</a>
                {/if}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                {l s='You can leave a message with the transaction key as soon as you make the transfer in your order history. Or you can contact our' mod='tnwuandmg'} <strong><a href="{$link->getPageLink('contact', true)|escape:'htmlall':'UTF-8'}">{l s='customer support team' mod='tnwuandmg'}</a></strong>.
            </td>
        </tr>
        <tfoot>
    </table>
{else}
    <p class="warning">
        {l s='We noticed a problem with your order. If you think this is an error, you can contact our' mod='tnwuandmg'}
        <a href="{$link->getPageLink('contact', true)|escape:'htmlall':'UTF-8'}">{l s='customer support team' mod='tnwuandmg'}</a>.
    </p>
{/if}
