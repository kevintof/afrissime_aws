{*
 * 2016 TN
 *
 * NOTICE OF LICENSE
 *
 * @author    TN
 * @copyright 2016 TN
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

{if $status == 'ok'}
    <div id="transferData" class="transferData">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="2">
                    <img src="{$modules_dir|escape:'html':'UTF-8'}/tnwuandmg/views/img/tnwuandmg.png" alt="">
                    {l s='Your order' mod='tnwuandmg'} {l s='is complete.' mod='tnwuandmg'}
                    <span onclick="PrintElem('#transferData')"
                          class="print_title button btn btn-default button-medium paddingLR10">
                    {l s='Print' mod='tnwuandmg'}
                </span>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="left_item">
                    {l s='Total amout:' mod='tnwuandmg'}
                </td>
                <td class="item">
                    If you have any questions, please contact to our sup <strong><span
                                class="price">{$total_to_pay|escape:'htmlall':'UTF-8'}</span></strong>
                    <span id="amount" class="price"></span>
                    {l s='(tax incl.)' mod='tnwuandmg'}
                </td>
            </tr>
            <tr>
                <td class="left_item">
                    {l s='Beneficiary name:' mod='tnwuandmg'}
                </td>
                <td>
                    <strong>{if $wuandmgTransferList['fullname']}
                            <strong>{$wuandmgTransferList['fullname']}</strong>{else}___________{/if}</strong>
                </td>
            </tr>
            {if isset($wuandmgTransferList['id_customer_active']) && $wuandmgTransferList['id_customer_active']}
                <tr>
                    <td class="left_item">
                        {l s='Beneficiary Id:' mod='tnwuandmg'}
                    </td>
                    <td>
                        <strong>{if $wuandmgTransferList['id_customer']}{$wuandmgTransferList['id_customer']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                    </td>
                </tr>
            {/if}
            {if isset($wuandmgTransferList['vat_number_active']) && $wuandmgTransferList['vat_number_active']}
                <tr>
                    <td class="left_item">
                        {l s='Vat number:' mod='tnwuandmg'}
                    </td>
                    <td>
                        <strong>{if $wuandmgTransferList['vat_number']}{$wuandmgTransferList['vat_number']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                    </td>
                </tr>
            {/if}
            {if isset($wuandmgTransferList['address_active']) && $wuandmgTransferList['address_active']}
                <tr>
                    <td class="left_item">
                        {l s='Address:' mod='tnwuandmg'}
                    </td>
                    <td>
                        <strong>{if $wuandmgTransferList['address']}{$wuandmgTransferList['address']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                    </td>
                </tr>
            {/if}
            {if isset($wuandmgTransferList['details_active']) && $wuandmgTransferList['details_active']}
                <tr>
                    <td class="left_item">
                        {l s='Details:' mod='tnwuandmg'}
                    </td>
                    <td>
                        <strong>{if $wuandmgTransferList['details']}{$wuandmgTransferList['details']|escape:'htmlall':'UTF-8'}{else}___________{/if}</strong>
                    </td>
                </tr>
            {/if}
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2">
                    {l s='Website: ' mod='tnwuandmg' }<a href="http://www.westernunion.com"
                                                              target="_blank">{l s='www.westernunion.com' mod='tnwuandmg'}</a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {l s='You should\'ve received an email with this data (if you haven\'t, than check in spam)' mod='tnwuandmg'}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {l s='If you have any qusetions, please will contact our support: ' mod='tnwuandmg'} <strong><a
                                href="{$link->getPageLink('contact', true)|escape:'htmlall':'UTF-8'}">{l s='Support' mod='tnwuandmg'}</a></strong>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {l s='You can leave message with the transaction key in this section: ' mod='tnwuandmg' }
                    <strong>
                        {if isset($smarty.get.hash_key) && !empty($smarty.get.hash_key)}
                            {assign var=hash_key value = ['key' => $smarty.get.hash_key]}
                        {else}
                            {assign var=hash_key value=array()}
                        {/if}
                        <a class="button btn btn-default button-medium paddingLR10"
                           href="{$link->getModuleLink('tnwuandmg', 'orders', $hash_key, true)|escape:'quotes':'UTF-8'}"
                           target="_blank">{l s='Western Union Orders' mod='tnwuandmg'}</a>
                    </strong>
                </td>
            </tr>

            <tfoot>
        </table>
    </div>
    <script type="text/javascript">
        function PrintElem(elem) {
            Popup($(elem).html());
        }
        function Popup(data) {
            var printWindow = window.open('', 'OrderPrint', 'height=500,width=800');
            printWindow.document.write('<html><head><title>{l s='Print:' mod='tnwuandmg'}</title>');

            {if isset($print_style) && !empty($print_style)}
            printWindow.document.write('<link rel="stylesheet" href="{$print_style|escape:'html':'UTF-8'}" type="text/css" />');
            {/if}

            printWindow.document.write('</head><body >');
            printWindow.document.write(data);
            printWindow.document.write('</body></html>');
            setTimeout(function () {
                printWindow.document.close();
                printWindow.focus();

                printWindow.print();
                printWindow.close();
            }, 800);

            return true;
        }
    </script>
{else}
    <p class="warning">

    </p>
{/if}
