<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_5238bd0d26ee4c221badd6e6c6475412'] = 'Votre commande';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_75fbf512d744977d62599cc3f0ae2bb4'] = 'est effctuée avec succès.';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_13dba24862cf9128167a59100e154c8d'] = 'Imprimer';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_83a9c5e9d34480d0476fcf09a0668bae'] = 'Total :';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_670f1dd225481cfd2f10f486ae00946b'] = 'Si vous avez des questions, n\'hésitez pas à nous contacter';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_1f87346a16cf80c372065de3c54c86d9'] = '(taxes incluses)';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_72f6fc606a2fcdcf3134dd122ef6a8f4'] = 'Nom du bénéficiaire';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_e5e275cbded51476bddd49f4937201f0'] = 'Bénéficiaire ID:';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_a546703d34127ef703d7035b4e451742'] = 'Numéro de TVA :';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_2bf1d5fae1c321d594fdedf05058f709'] = 'Adresse:';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_2babe53026309ed1bc4d26626dbaf9b3'] = 'Détails:';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_f053776fc937f55c38baf3e8d2f7c558'] = 'Site web :';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_6b77b3ecff65af712b6753a5972b2185'] = 'Si vous avez des questions, n\'hésitez pas à nous contacter notre service commercial';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_d78cd942b82023ea5674bf83848aa4e6'] = 'Commandes effectuées avec MoneyGram';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_bafa8208a788220cffd55ddbfb0a47b3'] = 'Paiement en ligne';
$_MODULE['<{tnwuandmg}prestashop>payment_return_mg_a467ca08d81c78b1f5e808168d13320a'] = 'Imprimer:';
$_MODULE['<{tnwuandmg}prestashop>my-account_d716596bd1642ba92e6991dd39e77ae4'] = 'Commandes Western Union';
$_MODULE['<{tnwuandmg}prestashop>my-account_a2725dce47efe872fed121c9bbd761a5'] = 'Commandes MoneyGram';
