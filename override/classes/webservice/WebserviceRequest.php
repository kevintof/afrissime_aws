<?php 
  
  class WebserviceRequest extends WebserviceRequestCore {
    public static function getResources(){
        $resources = parent::getResources();
        $resources['products_sales'] = array('description' => 'products sales', 'class' => 'ProductSale');
        ksort($resources);
        return $resources;
    }

}

?>