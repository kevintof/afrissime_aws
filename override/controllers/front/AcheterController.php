<?php

class AcheterController extends FrontController
{

    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		
        $this->setTemplate('acheter.tpl'); // themes/theme_current/templates/acheter.tpl
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Comment acheter sur Afrissime', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('acheter', true),
        ];

        return $breadcrumb;
    }
}