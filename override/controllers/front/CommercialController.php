<?php

class CommercialController extends FrontController
{

    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		$this->context->smarty->assign(
            array(
            'country' => Country::getCountries($this->context->language->id, true),
            )
        );
        $this->jsDefVars();
        $this->setTemplate('commercial.tpl'); // themes/theme_current/templates/acheter.tpl
    }

    public function postProcess()
    {
        if (Tools::isSubmit('squadRequest')) {
            $firstname = strtolower(trim(Tools::getValue('fname')));
            $lastname = strtolower(trim(Tools::getValue('lname')));
            $email = strtolower(trim(Tools::getValue('email')));
            $password = strtolower(trim(Tools::getValue('pass')));
            $phone = strtolower(trim(Tools::getValue('phone')));
            $city = strtolower(trim(Tools::getValue('city')));
            $paymentModeId = Tools::getValue('payment_mode_id');
            $defaultLang= $this->context->language->id;
            if (Tools::getValue('gender_id')) {
            	$gender_id = Tools::getValue('gender_id');
	        } else {
	            $gender_id = 1;
	        }
	        if (Tools::getValue('birthday')) {
	                $birthday = Tools::getValue('birthday');
	                $selBirthday = explode("/",$birthday);
	                if($defaultLang ==1){
	                    $birthday =$selBirthday[2]."-".$selBirthday[1]."-".$selBirthday[0];
	                }
	                if($defaultLang==2){
	                    $birthday =$selBirthday[0]."-".$selBirthday[1]."-".$selBirthday[2];
	                }
	        } 
	        else{
	           $birthday=""; 
	        }
	        if (Tools::getValue('title')) {
	            $title = Tools::getValue('title');
	        } else {
	            $title = "";
	        }
            
            //$this->validateSquadRegistrationForm($defaultLang);
            //if(empty($this->errors)){
                /*code pour l'inscription du vendeur*/
                //if (empty($this->context->customer->id)) {
               //if(Customer::customerExists($email,false)){

                    $customer = new Customer();
                    $customer->lastname=$lastname;
                    $customer->firstname =$firstname;
                    $customer->id_gender =$gender_id;
                    $customer->birthday =$birthday;
                    $customer->phone =$phone;
                    $customer->seller_title =$title;
                    $pwd=Tools::getValue('pass');
                    $customer->passwd=md5(_COOKIE_KEY_.$pwd);
                    $customer->email=$email;


                    $customer->firstname = Tools::ucwords($customer->firstname);
                    $customer->active = 1;
                    $customer->is_squad = 1;

                        if ($customer->add())
                        {
                            //updateContext($customer);
                            $this->context->customer->id= $customer->id;
                            //Saving customer payment
                            $paymentMode = Tools::getValue('payment_mode_id');
                            if(!empty($paymentMode)){
                                for($i=0;$i<count($paymentMode); $i++){
                                    $customerPayment = new CustomerPayment();
                                    $customerPayment->customer_id = $this->context->customer->id;
                                    $customerPayment->payment_mode_id = $paymentMode[$i];
                                    switch ($paymentMode[$i]) {
                                        case '1':
                                            $customerPayment->bank_name = Tools::getValue('bank_name');
                                            $customerPayment->rib = Tools::getValue('rib');
                                            break;
                                        
                                        case '2':
                                            $customerPayment->name_card = Tools::getValue('name_card');
                                            $customerPayment->num_card = Tools::getValue('num_card');
                                            $customerPayment->card_expiration = Tools::getValue('card_expiration');
                                            break;
                                        case '3':
                                            $customerPayment->num_mobile = Tools::getValue('num_mobile');
                                            break;
                                        case '4':
                                            $customerPayment->num_cash = Tools::getValue('num_cash');
                                            $customerPayment->cash_operator = Tools::getValue('cash_operator');
                                            break;
                                        case '5':
                                            $customerPayment->bitcoin_address = Tools::getValue('bitcoin_address');
                                            $customerPayment->nickname_bitcoin = Tools::getValue('nickname_bitcoin');
                                            break;
                                        case '6':
                                            $customerPayment->wallet_number = Tools::getValue('wallet_number');
                                            break;
                                    }
                                $customerPayment->save();
                                }
                            }
                            Tools::redirect($this->context->link->getPageLink('commercial',array('signup' => 1)));
                        }
                        else {
                            $this->errors[] = $this->getTranslator()->trans('Something wrong while creating seller.', [], 'Shop.Theme.Global');
                        }
                //}
                //}
               
                /*fin code*/

            //}
            
        }
    }
    public function jsDefVars()
    {
        $jsDef = array(
            'path_squaddetails' => $this->context->link->getPageLink('commercial',true),
        );

        Media::addJsDef($jsDef);
    }
    public function displayAjaxValidateSquadForm()
    {
        if (!$this->isTokenValid()) {
            die('Something went wrong!');
        }

        $params = array();
        parse_str(Tools::getValue('formData'), $params);
        if (!empty($params)) {
            Customer::validationSquadFormField($params);
        } else {
            die('1');
        }
    }

    public function validateSquadRegistrationForm()
    {
        $firstname = strtolower(trim(Tools::getValue('fname')));
        $lastname = strtolower(trim(Tools::getValue('lname')));
        $email = strtolower(trim(Tools::getValue('email')));
        $password = strtolower(trim(Tools::getValue('pass')));
        $phone = strtolower(trim(Tools::getValue('phone')));
        $firstname = strtolower(trim(Tools::getValue('fname')));
        $city = strtolower(trim(Tools::getValue('city')));
        $paymentModeId = Tools::getValue('payment_mode_id');
        $defaultLang = strtolower(trim(Tools::getValue('default_lang')));
        if (Tools::getValue('gender_id')) {
            $gender_id = Tools::getValue('gender_id');
        } else {
            $gender_id = 1;
        }
        if (Tools::getValue('birthday')) {
                $birthday = Tools::getValue('birthday');
                $selBirthday = explode("/",$birthday);
                if($defaultLang ==1){
                    $birthday =$selBirthday[2]."-".$selBirthday[1]."-".$selBirthday[0];
                }
                if($defaultLang==2){
                    $birthday =$selBirthday[0]."-".$selBirthday[1]."-".$selBirthday[2];
                }
        } 
        else{
           $birthday=""; 
        }
        if (Tools::getValue('title')) {
            $title = Tools::getValue('title');
        } else {
            $title = "";
        }
        if (!$lastname) {
            $this->errors[] = $this->getTranslator()->trans('Last name is required field.', [], 'Shop.Theme.Global');
        } elseif (!Validate::isName($lastname)) {
            $this->errors[] = $this->getTranslator()->trans('Invalid seller last name.', [], 'Shop.Theme.Global');
        }
        if (!$firstname) {
            $this->errors[] = $this->getTranslator()->trans('First name is required field.', [], 'Shop.Theme.Global'); 
        } elseif (!Validate::isName($firstname)) {
            $this->errors[] = $this->getTranslator()->trans('Invalid seller first name.', [], 'Shop.Theme.Global');
        }
        
        if (!$birthday) {
            $this->errors[] = $this->getTranslator()->trans('Birthdate is required field.', [], 'Shop.Theme.Global');
        } elseif (!Validate::isDate($birthday)) {
            $this->errors[] = $this->getTranslator()->trans('Invalid birthdate format.', [], 'Shop.Theme.Global');
        }
        
        if (!$paymentModeId) {
            $this->errors[] = $this->getTranslator()->trans('Payment mode is required field.', [], 'Shop.Theme.Global');
        }
        
        if (!$gender_id) {
            $this->errors[] = $this->getTranslator()->trans('Gender is required field.', [], 'Shop.Theme.Global'); 
        } 
        if (!$phone) {
            $this->errors[] = $this->getTranslator()->trans('Phone is required field.', [], 'Shop.Theme.Global');
        } elseif (!Validate::isPhoneNumber($phone)) {
            $this->errors[] = $this->getTranslator()->trans('Invalid phone number.', [], 'Shop.Theme.Global'); 
        }
       
        if (!Tools::getValue('terms_and_conditions')) {
            $this->errors[] = $this->getTranslator()->trans('Please agree the terms and condition.', [], 'Shop.Theme.Global');
        }
        //var_dump($this->errors); exit;
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Devenir commercial consultant', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('commercial', true),
        ];

        return $breadcrumb;
    }

}