<?php
//include(dirname(__FILE__).'/config/config.inc.php');
//include_once(dirname(__FILE__).'/classes/Currency.php');
class CurrencyRatesExchangeController extends FrontController
{

    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		if (isset($_GET['secure_key']) AND $_GET['secure_key'] == 'wf7895123')
            Currency::refreshCurrencies();

        $this->setTemplate('currencyratesexchange.tpl'); // themes/theme_current/templates/currencyratesexchange.tpl
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Currency Rates Exchange ', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('currencyratesexchange', true),
        ];

        return $breadcrumb;
    }

}