<?php

class SquadrewardsController extends FrontController
{
    public $php_self = 'squadrewards';
    public $ssl = true;
    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		$this->context->smarty->assign(array(
            'return_days' => (Configuration::get('PS_ORDER_RETURN') && (int)Configuration::get('PS_ORDER_RETURN_NB_DAYS') > 0) ? (int)Configuration::get('PS_ORDER_RETURN_NB_DAYS') : 0,
            'activeTab' => Tools::getValue('page') ? 'history' : '',
            ));
        $this->setTemplate('squadrewards.tpl'); // themes/theme_current/templates/squad_rewards.tpl
    }
    
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Afrissime Squad rewards', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('squadrewards', true),
        ];

        return $breadcrumb;
    }
}