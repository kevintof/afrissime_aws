<?php

class PageshopController extends FrontController
{

    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		
        $this->setTemplate('pageshop.tpl'); // themes/theme_current/templates/pageshop.tpl
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Ouvrir une boutique', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('pageshop', true),
        ];

        return $breadcrumb;
    }
}