<?php

class SquadController extends FrontController
{
    public $php_self = 'squad';
    public $ssl = true;
    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		
        $this->setTemplate('squad.tpl'); // themes/theme_current/templates/pageshop.tpl
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Rejoindre Afrissime Squad', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('squad', true),
        ];

        return $breadcrumb;
    }
}