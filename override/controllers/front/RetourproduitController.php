<?php

class RetourproduitController extends FrontController
{

    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		
        $this->setTemplate('retourproduit.tpl'); // themes/theme_current/templates/retourproduit.tpl
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Comment effectuer un retour produit', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('retourproduit', true),
        ];

        return $breadcrumb;
    }
}