<?php

class VendreController extends FrontController
{

    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		
        $this->setTemplate('vendre.tpl'); // themes/theme_current/templates/pageshop.tpl
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Vendre sur Afrissime', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('vendre', true),
        ];

        return $breadcrumb;
    }
}