<?php
include(dirname(__FILE__).'/../config/config.inc.php');
 
    if (Tools::getIsset('email') && Tools::getIsset('ip')) {
        $mail = Tools::getValue('email');
        $ip = Tools::getValue('ip');
        $date = date('Y-m-d H:i:s');
 
        Db::getInstance()->insert('newsletter', array(
            'id_shop' => 1,
            'id_shop_group' => 1,
            'email' => $mail,
            'newsletter_date_add' => $date,
            'ip_registration_newsletter' => $ip,
            'http_referer' => NULL,
            'active' => 1
        )); 
$to = $mail;
$subject = "Inscription à la newsletter d'Afrissime";
$txt = "Bonjour, vous venez de vous inscrire à notre newsletter. Toute l'équipe d'Afrissime vous remercie pour votre confiance. ";
$headers = "From: support@afrissime.com" . "\r\n";
mail($to,$subject,$txt,$headers);
        echo Tools::jsonEncode('success');
    } else {
            $return = array('result' => 'Ajax Error');
            echo Tools::jsonEncode($return);
    }
?>