<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
use PrestaShop\PrestaShop\Adapter\CoreException;
use PrestaShop\PrestaShop\Adapter\ServiceLocator;

/***
 * Class CustomerCore
 */
class CustomerPaymentCore extends ObjectModel
{
    /** @var int $  id_customer_payment Customer payment ID */
    public $id_customer_payment;

    /** @var int $customer_id Customer ID */
    public $customer_id;

    /** @var int $payment_mode_id payment mode ID */
    public $payment_mode_id;

    /** @var string bank_name */
    public $bank_name;

    /** @var string rib */
    public $rib;

    /** @var string name_card */
    public $name_card;

    /** @var string num_card */
    public $num_card;

    /** @var string card_expiration */
    public $card_expiration;

    /** @var string num_mobile */
    public $num_mobile;

    /** @var int cash_operator */
    public $cash_operator;

    /** @var string wallet_number */
    public $wallet_number;

    /** @var string bitcoin_address */
    public $bitcoin_address;

    /** @var string num_cash */
    public $num_cash;

   /*
    protected $webserviceParameters = array(
        'fields' => array(
            'id_default_group' => array('xlink_resource' => 'groups'),
            'id_lang' => array('xlink_resource' => 'languages'),
            'newsletter_date_add' => array(),
            'ip_registration_newsletter' => array(),
            'last_passwd_gen' => array('setter' => null),
            'secure_key' => array('setter' => null),
            'deleted' => array(),
            'passwd' => array('setter' => 'setWsPasswd'),
        ),
        'associations' => array(
            'groups' => array('resource' => 'group'),
        ),
    );
    */
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'customer_payment_detail',
        'primary' => 'id_customer_payment',
        'fields' => array(
            'bank_name' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 255),
            'payment_mode_id' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'customer_id' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'rib' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 32),
            'name_card' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 255),
            'num_card' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 20),
            'card_expiration' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 3),
            'num_mobile' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 3),
            'cash_operator' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'wallet_number' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 20),
            'bitcoin_address' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 255),
            'num_cash' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 20),
        ),
    );

    protected static $_defaultGroupId = array();
    protected static $_customerHasAddress = array();
    protected static $_customer_groups = array();

    /**
     * CustomerPaymentCore constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }
    
    /**
     * Retrieve customer payment detail by idCustomer.
     *
     * @param string $idCustomer
     *
     * @return array
     */
    public static function getCustomersById($idCustomer)
    {
        $sql = 'SELECT *
                FROM `' . _DB_PREFIX_ . 'customer_payment_detail`
                WHERE `customer_id` = \'' . (int) $idCustomer . '\'.';

        return Db::getInstance()->executeS($sql);
    }

     
}
