[{shop_url}]

Hi {firstname} {lastname},

Your order "{order_name}" is awaiting for payment by Western Union.
Your order has been placed successfully, and will be shipped as soon as payment is received.

Please note that you have selected to pay by Western Union. Please send your wire to:


{tnwuandmg_owner}

{tnwuandmg_id_customer}

{tnwuandmg_vat}

{tnwuandmg_address}

{tnwuandmg_details}


Total amount: {total_paid}

You can change transaction key  for this order: {tnwuandmg_link_hash}


You can review this order and download your invoice from the <a href="{history_url}" style="color:#DB3484; font-weight:bold; text-decoration:none;">"Order history"</a>
				section of your account by clicking <a href="{my_account_url}" style="color:#DB3484; font-weight:bold; text-decoration:none;">"My account"</a> on our Website.

Thank you for shopping at {shop_name}.

{shop_name} - {shop_url}


{shop_url} powered by PrestaShop™
